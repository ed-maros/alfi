# Release 7.2.0
    - Added support for Ubuntu 22.04
    - Removed --workdir option
    - Added --with-java-policy option
