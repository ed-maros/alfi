#!/usr/bin/perl -w
    # edit the above line to point to perl

    # edit this to point toward your installation of javaws
my $WEBSTART = "/usr/lib/icedtea-web/bin/javaws";

# Custom working PATH
$WORKDIR="/home/parallels/Alfi/Models/";
# Fully qualified path to the alfi.jnlp executable
$ALFI_JNLP="/home/parallels/mnt/Sources/Alfi/alfi-head/alfi_offline/alfi.jnlp";

############### do not edit below this line ###############

chdir($WORKDIR) or die "Can't chdir to $WORKDIR $!";


use Cwd ();

    # for "(B" problem in titles under fvwm
my $wm = $ENV{"ALFI__WINDOW_MANAGER"};
if (($wm) && ($wm eq "fvwm")) {
    delete $ENV{"LANG"};
    delete $ENV{"LC_CTYPE"};
}

my $ALFI_UTILITY_DIR = "$ENV{HOME}/.alfi";
if (! (-d $ALFI_UTILITY_DIR)) {
    mkdir($ALFI_UTILITY_DIR, 0755) || die($!);
}

if ( false ) {
my $COMMAND_FILE = "$ALFI_UTILITY_DIR/command_line.txt";
if (-e $COMMAND_FILE) {
    unlink($COMMAND_FILE) || die($!);
}

my $arguments = join(" ", @ARGV);

open(COMMAND_FILE, ">$COMMAND_FILE") || die($!);
print COMMAND_FILE "args " . join(" ", @ARGV) . "\n";
foreach my $key (keys(%ENV)) {
    print COMMAND_FILE "$key $ENV{$key}\n";
}
print COMMAND_FILE "user.dir " . Cwd::cwd();
close(COMMAND_FILE);
}

    # latest release
#$command =
#         "$WEBSTART http://www.ligo.caltech.edu/~e2e/alfi6/download/alfi.jnlp";
#$command =
#         "$WEBSTART ~/Software/alfi_offline/alfi.jnlp";
$command = 
        "env _JAVA_OPTIONS='-Dswing.systemlaf=javax.swing.plaf.metal.MetalLookAndFeel \
        -Dawt.useSystemAAFontSettings=on \
        -Dswing.aatext=true   \
        -Dswing.plaf.metal.controlFont='Sans-16'    \
        -Dswing.plaf.metal.userTextFont='Sans-16'   \
        -Dswing.plaf.metal.menuTextFont='Sans-16'   \
        -Dswing.plaf.metal.subTextFont='Sans-16'    \
        -Dswing.plaf.metal.systemTextFont='Sans-16' \
        -Dswing.plaf.metal.windowTitleFont='Sans-16'' $WEBSTART -property=deployment.security.itw.ignorecertissues=True -nosecurity $ALFI_JNLP";
print( $command );
exec($command);
