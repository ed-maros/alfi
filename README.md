# ALFI

ALFI is a graphical editor for the end-toend simulation software developed by the LIGO labs to
simulate the propigation of light through various medium to help in the
construction of the LIGO interferometers.

Additional information beyond what is contained in this file can be found at the
<a href="https://labcit.ligo.caltech.edu/~e2e/">E2E Home Page</a>.

## Installation

### Obtaining the binaries

<table>
  <tr>
    <th>Operatiing System</th>
    <th>Official Release</th>
    <th>Experimental Release</th>
  </tr>
  <tr>
    <th colspan="3">Fedora</th>
  </tr>
  <tr>
    <td>Fedora 35</td>
    <td></td>
    <td>
      <a href="https://ed-maros.docs.ligo.org/alfi/local-release-fedora35/noarch/alfi-offline-7.2.0-1.fc35.noarch.rpm">
      alfi-offline-7.2.0-1.fc35.noarch.rpm</a>
    </td>
  </tr>
  <tr>
    <td>Fedora 34</td>
    <td></td>
    <td>
      <a href="https://ed-maros.docs.ligo.org/alfi/local-release-fedora34/noarch/alfi-offline-7.2.0-1.fc34.noarch.rpm">
      alfi-offline-7.2.0-1.fc34.noarch.rpm</a>
    </td>
  </tr>
  <tr>
    <td>Fedora 33</td>
    <td></td>
    <td>
      <a href="https://ed-maros.docs.ligo.org/alfi/local-release-fedora33/noarch/alfi-offline-7.2.0-1.fc33.noarch.rpm">
      alfi-offline-7.2.0-1.fc33.noarch.rpm</a>
    </td>
  </tr>
  <tr>
    <th colspan="3">Ubuntu</th>
  </tr>
  <tr>
    <td>Ubuntu 22.04</td>
    <td></td>
    <td>
      <a href="https://ed-maros.docs.ligo.org/alfi/local-release-ubuntu2204/all/alfi-offline_7.2.0-1_all.deb">
      alfi-offline_7.2.0-1_all.deb</a>
    </td>
  </tr>
  <tr>
    <td>Ubuntu 20.04</td>
    <td></td>
    <td>
      <a href="https://ed-maros.docs.ligo.org/alfi/local-release-ubuntu2004/all/alfi-offline_7.2.0-1_all.deb">
      alfi-offline_7.2.0-1_all.deb</a>
    </td>
  </tr>
  <tr>
    <td>Ubuntu 18.04</td>
    <td></td>
    <td>
      <a href="https://ed-maros.docs.ligo.org/alfi/local-release-ubuntu1804/all/alfi-offline_7.2.0-1_all.deb">
      alfi-offline_7.2.0-1_all.deb</a>
    </td>
  </tr>
</table>

### Debian based Linux distributions

    sudo apt-get install [package name]

### Red Hat based Linux distributions

If your system uses `dnf`:

    sudo dnf install [package name]

If your systeem uses `yum`:

    sudo yum install [package name]

### Apple OSX

The software is not natively supported on OSX hardware.
Instead, support is offered by running within a virtual machine either
using VirtualBox or Parallels.
It is important to choose a guest image that is compatable with the host cpu.
For OSX this means using an arm64 image for systems running either an M1 or M2 cpu.
Intel based systems should use an amd64 guest image.

### Microsoft Windows

The software is not natively supported on Microsoft Windows.
Instead, support is offered by running within a virtual machine such as VirtualBox.
It is important to choose a guest image that is compatable with the host cpu.
For Windows this means using an arm64 image for systems running an arm based cpu.
Intel based systems should use an amd64 guest image.

### VirtualBox Setup

### Parallels Setup

## Running

The application is `alfi_ws` and stands for ALFI Web Start.
To get a full listing of options available, use `alfi_ws --help`.
These options should be considered
expert mode settings and used only if explicitly instructed.

You can set the environment variable E2E_PATH to specify the location E2E project.
This can be done in your login shell startup file or from within your current shell
before starting the application.

To use the command, your project directory should already exist.

    mkdir -p ${HOME}/ALFI/myproject

For borne shell and its derivatives,

    E2E_PATH=${HOME}/ALFI/myproject; export E2E_PATH

For C shell and its derivatives,

    setenv E2E_PATH ${HOME}/ALFI/myproject

Now it is time to invoke the editor and start building systems

    alfi_ws

If you get an error or exception complaining about not havinng permissions,
then try starting alfi_ws in the following way.

    alfi_ws --with-java-policy

## Credits

This project was funded by the NSF and lead by Hiroaki Yamamoto.

## License

GPLv2+

See the LICENSE file and COPYING file for details

