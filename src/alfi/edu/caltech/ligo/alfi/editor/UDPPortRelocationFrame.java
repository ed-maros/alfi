package edu.caltech.ligo.alfi.editor;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.filechooser.*;
import javax.swing.border.*;
import javax.swing.tree.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.resource.*;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.widgets.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;

/**    
  *
  * <pre>
  * The UDPPortRelocationFrame.
  * </pre>
  *
  * @author  Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.AlfiFrame
  */
public class UDPPortRelocationFrame extends EditorFrame {

    public static final int RET_CANCEL = 0;
    public static final int RET_OK = 1;
    public static final int RET_HELP = 2;

    private int m_return_status = RET_CANCEL;

    // Menu item strings
    private static final String PORTS = new String("Ports");
    private static final String SAVE_CLOSE = new String("Save");
    private static final String CANCEL_CLOSE = new String("Cancel");

    private static final String HELP_PORT = new String("Help"); 
    private static final String HELP_PORT_POSITION = 
                                            new String("Help with Ports..."); 

    private JMenuItem m_save_button;

 
    //**************************************************************************
    //********* Constructors ***************************************************

    public UDPPortRelocationFrame (AlfiMainFrame _parent,
                                                   UserDefinedPrimitive _udp) {
        super(_parent, _udp, false);
    }

    //**************************************************************************
    //********* methods ********************************************************

    /**
      * Create the ALFIView inside the Frame
      *
      * @see edu.caltech.ligo.alfi.editor.ALFIView
      */
    public void initializeFrame() {
        super.initializeFrame();
        // Set up the menu bar
        JMenu port_menu = createPortMenu();
        JMenu help_menu = createHelpMenu();
        JMenuBar menu_bar = new JMenuBar();
        menu_bar.add(port_menu);
        menu_bar.add(help_menu);
        setJMenuBar(menu_bar);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new RelocationFrameAdapter());
    } 

    /**
      * Creates the Port Menu and its menu items
      */
    protected JMenu createPortMenu () {
        JMenu menu = new JMenu(PORTS);
        menu.setMnemonic('P');
                
        SaveAction save_action = new SaveAction();
        JMenuItem save = menu.add(save_action);
        save.setMnemonic('S');

        CancelAction cancel_action = new CancelAction();
        JMenuItem cancel = menu.add(cancel_action); 
        cancel.setMnemonic('A');

        return menu;
    }

    /**
      * Create the edit menu and its menu items.
      */
    protected JMenu createHelpMenu() {
        JMenu menu = new JMenu(HELP_PORT);
        menu.setMnemonic('H');

        HelpAction help_action = new HelpAction();
        JMenuItem help = menu.add(help_action);
        help.setMnemonic('P');

        return menu;
    }

    private void saveActionPerformed (ActionEvent _event) {
        try {
            System.out.println("Save here...");
            doClose(RET_OK);
        }
        catch (Exception e) { e.printStackTrace(); }
    }    

    private void cancelActionPerformed (ActionEvent _event) {
        try {
            System.out.println("Cancel here...");
            doClose(RET_CANCEL);
        }
        catch (Exception e) { e.printStackTrace(); }
    }    

    private void helpActionPerformed (ActionEvent _event) {
        try {
            System.out.println("Help here...");
        }
        catch (Exception e) { e.printStackTrace(); }
    }    

    private void doClose (int _return_status) {
        m_return_status = _return_status;
        setVisible(false);
    }   


    class RelocationFrameAdapter extends WindowAdapter{

        public void windowActivated (WindowEvent e) {
        }
        
        public void windowClosing (WindowEvent e) {
            prompt_to_save();
        }
    }

    public void prompt_to_save() {
        try {
            System.out.println("prompt to save");
            saveActionPerformed(null);
        }
        catch (Exception ex) { ; }
     }
 
     ////////////////////////////////////////////
    //     Menu Actions     ////////////////////
    ////////////////////////////////////////////


    class SaveAction extends AbstractAction {
        public SaveAction() {
            super(SAVE_CLOSE);
        }
        public void actionPerformed (ActionEvent _event) {
            saveActionPerformed(_event);
        }
    }

    class CancelAction extends AbstractAction {
        public CancelAction() {
            super(CANCEL_CLOSE);
        }
        public void actionPerformed (ActionEvent _event) {
            cancelActionPerformed(_event);
        }
    }

    class HelpAction extends AbstractAction {
        public HelpAction() {
            super(HELP_PORT_POSITION);
        }
        public void actionPerformed (ActionEvent _event) {
            helpActionPerformed(_event);
        }
    }

}
