package edu.caltech.ligo.alfi.editor;

import java.awt.event.*;

    /**
     * This  very simple extension of WindowEvent involves simple changes and
     * actions associated with an editors window.
     */
public class EditorWindowEvent extends WindowEvent {

    //**************************************************************************
    //***** constants **********************************************************

        // action codes
    public static final int EDITOR_WINDOW_HIDING    = WINDOW_LAST + 1;
    public static final int EDITOR_WINDOW_ACTIVATED = WINDOW_LAST + 2;
    public static final int TITLE_CHANGE            = WINDOW_LAST + 3;

    public EditorWindowEvent(EditorFrame _source, int _event_id) {
        super(_source, _event_id);
    }
}
