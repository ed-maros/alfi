package edu.caltech.ligo.alfi.editor;

import java.awt.event.*;

    /**
     * This interface deals with simple changes to the externals of editor
     * windows, specifically, title changes and window closing.
     */
public interface EditorWindowListener {
    public abstract void processEditorWindowEvent(EditorWindowEvent _event);
}
