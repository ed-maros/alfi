package edu.caltech.ligo.alfi.editor;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.Box;

import edu.caltech.ligo.alfi.common.BasicPopupMenu;
import edu.caltech.ligo.alfi.common.Actions;
import edu.caltech.ligo.alfi.common.UIResourceBundle;

/**    
  * <pre>
  * The command popup menu for nodes
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see BasicPopupMenu
  */
public class NodePopupMenu extends BasicPopupMenu { 

    private JMenuItem m_center_ports_checkbox;

    public NodePopupMenu (Actions _actions, UIResourceBundle _resources) {
        super(_actions);

        JMenu edit = createCascadeMenu(EditorActionsIF.EDIT);        
        createMenuItem(edit, EditorActionsIF.CUT);
        createMenuItem(edit, EditorActionsIF.COPY);
        createMenuItem(edit, EditorActionsIF.DUPLICATE);
        createMenuItem(edit, EditorActionsIF.PASTE);
        edit.addSeparator();
        createMenuItem(edit, EditorActionsIF.DELETE);
        edit.addSeparator();
        createMenuItem(edit, EditorActionsIF.EDIT_COMMENT);
        //edit.addSeparator();
        //createMenuItem(edit, EditorActionsIF.EDIT_MACROS);

        addSeparator();

        createMenu(EditorActionsIF.NODE_RENAME);
        createMenu(EditorActionsIF.SETTINGS);
        createMenu(EditorActionsIF.NODE_SWAP);

        addSeparator();
    
        createMenu(EditorActionsIF.VIEW_INTERNAL);
        createMenu(EditorActionsIF.VIEW_BASE_BOX);

        addSeparator();
    
        m_center_ports_checkbox =
                          createCheckBoxMenuItem(EditorActionsIF.CENTER_PORTS);

        JMenu rotate_menu = createCascadeMenu(EditorActionsIF.ROTATE);
        createMenuItem(rotate_menu, EditorActionsIF.ROTATE_NORTH);
        createMenuItem(rotate_menu, EditorActionsIF.ROTATE_EAST);
        createMenuItem(rotate_menu, EditorActionsIF.ROTATE_WEST);
        createMenuItem(rotate_menu, EditorActionsIF.ROTATE_SOUTH);

        JMenu swap_menu = createCascadeMenu(EditorActionsIF.SWAP);
        createMenuItem(swap_menu, EditorActionsIF.SWAP_NONE);
        createMenuItem(swap_menu, EditorActionsIF.SWAP_X);
        createMenuItem(swap_menu, EditorActionsIF.SWAP_Y);

        JMenu symmetry_menu = createCascadeMenu(EditorActionsIF.SYMMETRY);
        createMenuItem(symmetry_menu, EditorActionsIF.SYMMETRY_NONE);
        createMenuItem(symmetry_menu, EditorActionsIF.SYMMETRY_X);
        createMenuItem(symmetry_menu, EditorActionsIF.SYMMETRY_Y);
        createMenuItem(symmetry_menu, EditorActionsIF.SYMMETRY_PLUS_XY);
        createMenuItem(symmetry_menu, EditorActionsIF.SYMMETRY_MINUS_XY);

        addSeparator();

        JMenu debug = createCascadeMenu(EditorActionsIF.DEBUG);
        createMenuItem(debug, EditorActionsIF.LOCATE_FILE);
        createMenuItem(debug, EditorActionsIF.NODE_INFO);
    }

    public JMenuItem getCenterPortsCheckbox() { return m_center_ports_checkbox;}
}

