package edu.caltech.ligo.alfi.editor;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.Box;

import edu.caltech.ligo.alfi.common.BasicPopupMenu;
import edu.caltech.ligo.alfi.common.Actions;
import edu.caltech.ligo.alfi.common.UIResourceBundle;

/**    
  * <pre>
  * The command popup menu for bundlers
  * </pre>
  *
  * @see BasicPopupMenu
  */
public class BundlerPopupMenu extends BasicPopupMenu { 

    public BundlerPopupMenu (Actions _actions) {
        super(_actions);

        createMenu(EditorActionsIF.BUNDLER_DELETE);
        createMenu(EditorActionsIF.BUNDLER_IO_RENAME);
        createMenu(EditorActionsIF.BUNDLER_PRIMARY_IN_TO_SECONDARY);
        createMenu(EditorActionsIF.BUNDLER_PRIMARY_OUT_TO_SECONDARY);
    }
}

