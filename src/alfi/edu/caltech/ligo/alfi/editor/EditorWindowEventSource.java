package edu.caltech.ligo.alfi.editor;

/**
  * This interface defines fields and methods used by sources of 
  * EditorWindowEvent sources.
  */
public interface EditorWindowEventSource {
    public void addEditorWindowListener(EditorWindowListener _listener);
    public void removeEditorWindowListener(EditorWindowListener _listener);
}

