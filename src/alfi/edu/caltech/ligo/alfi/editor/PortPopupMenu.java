package edu.caltech.ligo.alfi.editor;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.Box;

import edu.caltech.ligo.alfi.common.BasicPopupMenu;
import edu.caltech.ligo.alfi.common.Actions;
import edu.caltech.ligo.alfi.common.UIResourceBundle;

/**    
  * <pre>
  * The command popup menu for nodes
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see BasicPopupMenu
  */
public class PortPopupMenu extends BasicPopupMenu { 
    public PortPopupMenu (Actions _actions) {
        super(_actions);

        createMenu(EditorActionsIF.PORT_MODIFY);
        createMenu(EditorActionsIF.PORT_DELETE);
        createMenu(EditorActionsIF.PORT_RENAME);
        createMenu(EditorActionsIF.IMPORT_CHANNELS);
    }
}

