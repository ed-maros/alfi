package edu.caltech.ligo.alfi.editor;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.Box;

import edu.caltech.ligo.alfi.common.BasicPopupMenu;
import edu.caltech.ligo.alfi.common.Actions;
import edu.caltech.ligo.alfi.common.UIResourceBundle;

/**    
  * <pre>
  * The popup menu for comments  
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see BasicPopupMenu
  */
public class HighlightPopupMenu extends BasicPopupMenu { 

    public HighlightPopupMenu (Actions _actions, UIResourceBundle _resources) {
        super(_actions);

        JMenu edit = createCascadeMenu(EditorActionsIF.EDIT);        
        createMenuItem(edit, EditorActionsIF.CUT);
        createMenuItem(edit, EditorActionsIF.COPY);
        createMenuItem(edit, EditorActionsIF.DUPLICATE);
        createMenuItem(edit, EditorActionsIF.PASTE);
        edit.addSeparator();
        createMenuItem(edit, EditorActionsIF.DELETE);
        edit.addSeparator();
        createMenuItem(edit, EditorActionsIF.GROUP);
        createMenuItem(edit, EditorActionsIF.UNGROUP);

        addSeparator();
        JMenu color = createCascadeMenu(EditorActionsIF.HILITE_COLOR_MODIFY);        
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_BLUE);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_CYAN);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_GREEN);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_LIGHT_GRAY);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_MAGENTA);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_ORANGE);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_PINK);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_RED);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_YELLOW);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_DARK_BLUE);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_DARK_CYAN);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_DARK_GREEN);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_DARK_GRAY);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_DARK_MAGENTA);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_DARK_OLIVE_GREEN);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_DARK_ORANGE);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_DARK_RED);
        createMenuItem(color, EditorActionsIF.HILITE_COLOR_DARK_YELLOW);

        createMenu(EditorActionsIF.HILITE_LAYER_MODIFY);
        createMenu(EditorActionsIF.HILITE_SEND_TO_BACK);
        createMenu(EditorActionsIF.HILITE_SEND_TO_FRONT);


    }

}

