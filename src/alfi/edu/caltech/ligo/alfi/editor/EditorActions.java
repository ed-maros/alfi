package edu.caltech.ligo.alfi.editor;

import java.awt.event.ActionEvent;
import javax.swing.JCheckBoxMenuItem;

import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.Alfi;

/**    
  * <pre>
  * The actions for the Alfi Editor Frames
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.BasicAction 
  */
public class EditorActions extends Actions {

    protected EditorFrame m_editor_frame;

    public EditorActions(UIResourceBundle _resources, EditorFrame _frame) {
        super(_resources);
        m_editor_frame = _frame;
    }

    protected void loadActions() {
        loadAction(EditorActionsIF.CUT, new CutEditAction());
        loadAction(EditorActionsIF.COPY, new CopyEditAction());
        loadAction(EditorActionsIF.DUPLICATE, new DuplicateEditAction());
        loadAction(EditorActionsIF.PASTE, new PasteEditAction());
        loadAction(EditorActionsIF.DELETE, new DeleteEditAction());
        loadAction(EditorActionsIF.SELECT_ALL, new SelectAllEditAction());
        loadAction(EditorActionsIF.GROUP, new GroupEditAction());
        loadAction(EditorActionsIF.UNGROUP, new UngroupEditAction());
    
        loadAction(EditorActionsIF.ALIGN_LEFT, new AlignLeft());        
        loadAction(EditorActionsIF.ALIGN_RIGHT, new AlignRight());        
        loadAction(EditorActionsIF.ALIGN_TOP, new AlignTop());        
        loadAction(EditorActionsIF.ALIGN_BOTTOM, new AlignBottom());        
        loadAction(EditorActionsIF.ALIGN_VERTICAL_CENTER, 
                                                     new AlignVerticalCenter());        
        loadAction(EditorActionsIF.ALIGN_HORIZONTAL_CENTER, 
                                                   new AlignHorizontalCenter());        

        loadAction(EditorActionsIF.NEW_BUNDLER, new NewBundlerAction());
        loadAction(EditorActionsIF.NEW_TEXT_COMMENT, 
                                                    new NewTextCommentAction());
        loadAction(EditorActionsIF.NEW_COLORED_BOX, new NewColoredBoxAction());
        loadAction(EditorActionsIF.EDIT_COMMENT, new EditCommentAction());
        loadAction(EditorActionsIF.EDIT_MACROS, new EditMacroAction());
        loadAction(EditorActionsIF.BOX_SETTINGS, new BoxSettingsAction());
        loadAction(EditorActionsIF.VIEW_EXTERNAL, new ExternalViewAction());
        loadAction(EditorActionsIF.VIEW_INTERNAL, new InternalViewAction());
        loadAction(EditorActionsIF.RESET_VIEW, new ResetViewAction());
        loadAction(EditorActionsIF.VALIDATE_BUNDLES,
                                                  new ValidateBundlesAction());
        loadAction(EditorActionsIF.SMOOTH_CONNS, 
                                             new SmoothAllConnectionsAction());
        loadAction(EditorActionsIF.TRIM_CONNS, new TrimConnectionsAction());
        loadAction(EditorActionsIF.VIEW_BASE_BOX, new ViewBaseBoxAction());
        loadAction(EditorActionsIF.SHOW_MAIN, new ShowMainAction());
        loadAction(EditorActionsIF.EXPORT_MODELER_FILE, 
                                                new ExportModelerFileAction());
        loadAction(EditorActionsIF.SAVE, new SaveAction());
        loadAction(EditorActionsIF.SAVE_COPY_AS, new SaveCopyAsAction());
        loadAction(EditorActionsIF.RELOAD, new ReloadAction());
        loadAction(EditorActionsIF.CLOSE, new CloseAction());
        loadAction(EditorActionsIF.PRINT_TO_SCALE, new PrintToScaleAction());
        loadAction(EditorActionsIF.PRINT_FIT_PAGE, new PrintFitPageAction());
        loadAction(EditorActionsIF.LOCATE_FILE, new LocateFileAction());
        loadAction(EditorActionsIF.NODE_INFO, new ShowNodeInfoAction());
        loadAction(EditorActionsIF.NODE_RENAME, new RenameNodeAction());
        loadAction(EditorActionsIF.NODE_DELETE, new DeleteNodeAction());
        loadAction(EditorActionsIF.NODE_DUPLICATE, new DuplicateNodeAction());
        loadAction(EditorActionsIF.NODE_SWAP, new SwapNodesAction());
        loadAction(EditorActionsIF.SETTINGS, new EditNodeSettingsAction());
        loadAction(EditorActionsIF.CENTER_PORTS, new CenterPortsAction());
        loadAction(EditorActionsIF.ROTATE_NORTH, new RotateNorthAction());
        loadAction(EditorActionsIF.ROTATE_SOUTH, new RotateSouthAction());
        loadAction(EditorActionsIF.ROTATE_EAST, new RotateEastAction());
        loadAction(EditorActionsIF.ROTATE_WEST, new RotateWestAction());
        loadAction(EditorActionsIF.SWAP_X, new SwapXAction());
        loadAction(EditorActionsIF.SWAP_Y, new SwapYAction());
        loadAction(EditorActionsIF.SWAP_NONE, new SwapNoneAction());

        loadAction(EditorActionsIF.SYMMETRY_X, new SymmetryXAction());
        loadAction(EditorActionsIF.SYMMETRY_Y, new SymmetryYAction());
        loadAction(EditorActionsIF.SYMMETRY_PLUS_XY,
                                                   new SymmetryPlusXYAction());
        loadAction(EditorActionsIF.SYMMETRY_MINUS_XY,
                                                  new SymmetryMinusXYAction());
        loadAction(EditorActionsIF.SYMMETRY_NONE, new SymmetryNoneAction());

        // Port Popup Menu Actions 
        loadAction(EditorActionsIF.PORT_MODIFY, new ModifyPortAction());
        loadAction(EditorActionsIF.PORT_DELETE, new DeletePortAction());
        loadAction(EditorActionsIF.PORT_RENAME, new RenamePortAction());
        loadAction(EditorActionsIF.IMPORT_CHANNELS, new ImportChannelsAction());

        // Connections Popup Menu Actions 
        loadAction(EditorActionsIF.CONN_DELETE, new DeleteConnectionAction());
        loadAction(EditorActionsIF.CONN_SMOOTH, new SmoothConnectionAction());
        loadAction(EditorActionsIF.CONN_CREATE_JUNCTION, 
                                       new CreateJunctionInConnectionAction());
        loadAction(EditorActionsIF.CONN_CREATE_BUNDLER, 
                                        new CreateBundlerInConnectionAction());
        loadAction(EditorActionsIF.CONN_TRIM, new TrimConnectionAction());
        loadAction(EditorActionsIF.SHOW_BUNDLE_CONTENT,
                                                new ShowBundleContentAction());

        // Junctions Popup Menu Actions
        loadAction(EditorActionsIF.JUNCTION_DELETE, new DeleteJunctionAction());

        // Bundlers Popup Menu Actions
        loadAction(EditorActionsIF.BUNDLER_DELETE, new DeleteBundlerAction());
        loadAction(EditorActionsIF.BUNDLER_IO_RENAME,
                                                  new RenameBundlerIOAction());
        loadAction(EditorActionsIF.BUNDLER_PRIMARY_IN_TO_SECONDARY,
                                     new BundlerPrimaryInToSecondaryAction());
        loadAction(EditorActionsIF.BUNDLER_PRIMARY_OUT_TO_SECONDARY,
                                     new BundlerPrimaryOutToSecondaryAction());

        // TextComment Popup Menu Actions
        loadAction(EditorActionsIF.TEXT_FONT_XSMALL, new FontSizeXSmallAction()); 
        loadAction(EditorActionsIF.TEXT_FONT_SMALL, new FontSizeSmallAction()); 
        loadAction(EditorActionsIF.TEXT_FONT_MEDIUM, new FontSizeMediumAction()); 
        loadAction(EditorActionsIF.TEXT_FONT_LARGE, new FontSizeLargeAction()); 
        loadAction(EditorActionsIF.TEXT_FONT_XLARGE, 
                                                new FontSizeExtraLargeAction()); 
        loadAction(EditorActionsIF.TEXT_FONT_HEADING, 
                                                   new FontSizeHeadingAction()); 
        loadAction(EditorActionsIF.TEXT_VALUE, new ChangeTextAction()); 
        loadAction(EditorActionsIF.TEXT_FONT_BOLD, new ToggleBoldAction());
        loadAction(EditorActionsIF.TEXT_FONT_UNDERLINE,
                                                   new ToggleUnderlineAction());
        loadAction(EditorActionsIF.TEXT_FONT_ITALIC, new ToggleItalicAction());
        loadAction(EditorActionsIF.TEXT_COLOR_BLACK, new BlackTextAction());
        loadAction(EditorActionsIF.TEXT_COLOR_WHITE, new WhiteTextAction()); 
        loadAction(EditorActionsIF.TEXT_COLOR_BLUE, new BlueTextAction());
        loadAction(EditorActionsIF.TEXT_COLOR_CYAN, new CyanTextAction());
        loadAction(EditorActionsIF.TEXT_COLOR_GREEN, new GreenTextAction());
        loadAction(EditorActionsIF.TEXT_COLOR_LIGHT_GRAY, 
                                                     new LightGrayTextAction()); 
        loadAction(EditorActionsIF.TEXT_COLOR_MAGENTA, new MagentaTextAction());
        loadAction(EditorActionsIF.TEXT_COLOR_ORANGE, new OrangeTextAction());
        loadAction(EditorActionsIF.TEXT_COLOR_PINK, new PinkTextAction());
        loadAction(EditorActionsIF.TEXT_COLOR_RED, new RedTextAction());
        loadAction(EditorActionsIF.TEXT_COLOR_YELLOW, new YellowTextAction());
        
        // ColoredBox Popup Menu Actions
        loadAction(EditorActionsIF.HILITE_LAYER_MODIFY, 
                                                       new ModifyHiliteLayer()); 
        loadAction(EditorActionsIF.HILITE_SEND_TO_BACK, 
                                                       new SendHiliteToBack()); 
        loadAction(EditorActionsIF.HILITE_SEND_TO_FRONT, 
                                                       new SendHiliteToFront()); 
        loadAction(EditorActionsIF.HILITE_COLOR_BLUE, 
                                                    new BlueBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_CYAN, 
                                                    new CyanBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_GREEN, 
                                                   new GreenBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_LIGHT_GRAY, 
                                               new LightGrayBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_MAGENTA, 
                                                 new MagentaBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_ORANGE, 
                                                  new OrangeBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_PINK, 
                                                    new PinkBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_RED, new RedBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_YELLOW, 
                                                  new YellowBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_DARK_BLUE, 
                                                new DarkBlueBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_DARK_CYAN, 
                                                new DarkCyanBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_DARK_GREEN, 
                                               new DarkGreenBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_DARK_GRAY, 
                                                new DarkGrayBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_DARK_MAGENTA, 
                                             new DarkMagentaBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_DARK_OLIVE_GREEN, 
                                          new DarkOliveGreenBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_DARK_ORANGE, 
                                              new DarkOrangeBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_DARK_RED, 
                                                 new DarkRedBackgroundAction()); 
        loadAction(EditorActionsIF.HILITE_COLOR_DARK_YELLOW, 
                                              new DarkYellowBackgroundAction()); 
    }

    class CutEditAction extends BasicAction {
        CutEditAction() {
            super(EditorActionsIF.CUT, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.cut();
        }
    }

    class CopyEditAction extends BasicAction {
        CopyEditAction() {
            super(EditorActionsIF.COPY, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.copy();
        }
    }

    class DuplicateEditAction extends BasicAction {
        DuplicateEditAction() {
            super(EditorActionsIF.DUPLICATE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.duplicate();
        }
    }

    class PasteEditAction extends BasicAction {
        PasteEditAction() {
            super(EditorActionsIF.PASTE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.paste(false);
        }
    }

    class DeleteEditAction extends BasicAction {
        DeleteEditAction() {
            super(EditorActionsIF.DELETE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.getView().performDelete();
        }
    }

    class SelectAllEditAction extends BasicAction {
        SelectAllEditAction() {
            super(EditorActionsIF.SELECT_ALL, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.getView().selectAll();
        }
    }

    class GroupEditAction extends BasicAction {
        GroupEditAction() {
            super(EditorActionsIF.GROUP, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.group();
        }
    }

    class UngroupEditAction extends BasicAction {
        UngroupEditAction() {
            super(EditorActionsIF.UNGROUP, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.ungroup();
        }
    }

    class AlignLeft extends BasicAction {
        AlignLeft() {
            super(EditorActionsIF.ALIGN_LEFT, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.getView().alignLeft();
        }
    }

    class AlignRight extends BasicAction {
        AlignRight() {
            super(EditorActionsIF.ALIGN_RIGHT, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.getView().alignRight();
        }
    }

    class AlignTop extends BasicAction {
        AlignTop() {
            super(EditorActionsIF.ALIGN_TOP, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.getView().alignTop();
        }
    }

    class AlignBottom extends BasicAction {
        AlignBottom() {
            super(EditorActionsIF.ALIGN_BOTTOM, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.getView().alignBottom();
        }
    }

    class AlignVerticalCenter extends BasicAction {
        AlignVerticalCenter() {
            super(EditorActionsIF.ALIGN_VERTICAL_CENTER, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.getView().alignVerticalCenter();
        }
    }

    class AlignHorizontalCenter extends BasicAction {
        AlignHorizontalCenter() {
            super(EditorActionsIF.ALIGN_HORIZONTAL_CENTER, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.getView().alignHorizontalCenter();
        }
    }


    class NewBundlerAction extends BasicAction {
        NewBundlerAction() {
            super(EditorActionsIF.NEW_BUNDLER, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.addBundler();
        }
    }

    class NewTextCommentAction extends BasicAction {
        NewTextCommentAction() {
            super(EditorActionsIF.NEW_TEXT_COMMENT, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.addTextComment();
        }
    }

    class NewColoredBoxAction extends BasicAction {
        NewColoredBoxAction() {
            super(EditorActionsIF.NEW_COLORED_BOX, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.addColoredBox();
        }
    }

    class EditCommentAction extends BasicAction {
        EditCommentAction() {
            super(EditorActionsIF.EDIT_COMMENT, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.editComment();
        }
    }

    class EditMacroAction extends BasicAction {
        EditMacroAction() {
            super(EditorActionsIF.EDIT_MACROS, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.editMacros();
        }
    }

    class BoxSettingsAction extends BasicAction {
        BoxSettingsAction() {
            super(EditorActionsIF.BOX_SETTINGS, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.editSettings(m_editor_frame.getNodeInEdit());
        }
    }

    class ExternalViewAction extends BasicAction {
        ExternalViewAction() {
            super(EditorActionsIF.VIEW_EXTERNAL, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.viewExternal();
        }
    }

    class InternalViewAction extends BasicAction {
        InternalViewAction() {
            super(EditorActionsIF.VIEW_INTERNAL, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.viewInternal();
        }
    }

    class ResetViewAction extends BasicAction {
        ResetViewAction() {
            super(EditorActionsIF.RESET_VIEW, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.resetView();
        }
    }

    class ValidateBundlesAction extends BasicAction {
        ValidateBundlesAction() {
            super(EditorActionsIF.VALIDATE_BUNDLES, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.validateBundles();
        }
    }

    class SmoothAllConnectionsAction extends BasicAction {
        SmoothAllConnectionsAction() {
            super(EditorActionsIF.SMOOTH_CONNS, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.smoothConnections(true);
        }
    }

    class TrimConnectionsAction extends BasicAction {
        TrimConnectionsAction() {
            super(EditorActionsIF.TRIM_CONNS, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.trimAllConnectionPaths();
        }
    }

    class ViewBaseBoxAction extends BasicAction {
        ViewBaseBoxAction() {
            super(EditorActionsIF.VIEW_BASE_BOX, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.viewBaseBox();
        }
    }

    class ShowMainAction extends BasicAction {
        ShowMainAction() {
            super(EditorActionsIF.SHOW_MAIN, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.showMainWindow();
        }
    }

    class ExportModelerFileAction extends BasicAction {
        ExportModelerFileAction() {
            super(EditorActionsIF.EXPORT_MODELER_FILE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.exportModelerFile();
        }
    }

    class SaveAction extends BasicAction {
        SaveAction() {
            super(EditorActionsIF.SAVE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.save();
        }
    }

    class SaveCopyAsAction extends BasicAction {
        SaveCopyAsAction() {
            super(EditorActionsIF.SAVE_COPY_AS, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.saveCopyAs();
        }
    }

    class ReloadAction extends BasicAction {
        ReloadAction() {
            super(EditorActionsIF.RELOAD, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.reload();
        }
    }

    class CloseAction extends BasicAction {
        CloseAction() {
            super(EditorActionsIF.CLOSE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setVisible(false);
        }
    }

    class PrintToScaleAction extends BasicAction {
        PrintToScaleAction() {
            super(EditorActionsIF.PRINT_TO_SCALE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.printToScale();
        }
    }

    class PrintFitPageAction extends BasicAction {
        PrintFitPageAction() {
            super(EditorActionsIF.PRINT_FIT_PAGE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.printFitPage();
        }
    }

    class ShowNodeInfoAction extends BasicAction {
        ShowNodeInfoAction() {
            super(EditorActionsIF.NODE_INFO, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.showNodeInfo();
        }
    }

    class LocateFileAction extends BasicAction {
        LocateFileAction() {
            super(EditorActionsIF.LOCATE_FILE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.locateFile();
        }
    }

    class RenameNodeAction extends BasicAction {
        RenameNodeAction() {
            super(EditorActionsIF.NODE_RENAME, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.renameNode();
        }
    }

    class DeleteNodeAction extends BasicAction {
        DeleteNodeAction() {
            super(EditorActionsIF.NODE_DELETE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.deleteMemberNode();
        }
    }

    class DuplicateNodeAction extends BasicAction {
        DuplicateNodeAction() {
            super(EditorActionsIF.NODE_DUPLICATE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.duplicateMemberNode();
        }
    }

    class EditNodeSettingsAction extends BasicAction {
        EditNodeSettingsAction() {
            super(EditorActionsIF.SETTINGS, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.editSettings();
        }
    }

    class CenterPortsAction extends BasicAction {
        CenterPortsAction() {
            super(EditorActionsIF.CENTER_PORTS, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            boolean state = ((JCheckBoxMenuItem) _event.getSource()).getState();
            m_editor_frame.setCenterExternalViewPorts(state);
        }
    }

    class RotateNorthAction extends BasicAction {
        RotateNorthAction() {
            super(EditorActionsIF.ROTATE_NORTH, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.rotateNorth();
        }
    }

    class RotateSouthAction extends BasicAction {
        RotateSouthAction() {
            super(EditorActionsIF.ROTATE_SOUTH, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.rotateSouth();
        }
    }

    class RotateEastAction extends BasicAction {
        RotateEastAction() {
            super(EditorActionsIF.ROTATE_EAST, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.rotateEast();
        }
    }

    class RotateWestAction extends BasicAction {
        RotateWestAction() {
            super(EditorActionsIF.ROTATE_WEST, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.rotateWest();
        }
    }

    class SwapNodesAction extends BasicAction {
        SwapNodesAction() {
            super(EditorActionsIF.NODE_SWAP, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.swapNodes();
        }
    }


    class SwapXAction extends BasicAction {
        SwapXAction() {
            super(EditorActionsIF.SWAP_X, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.swapX();
        }
    }

    class SwapYAction extends BasicAction {
        SwapYAction() {
            super(EditorActionsIF.SWAP_Y, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.swapY();
        }
    }

    class SwapNoneAction extends BasicAction {
        SwapNoneAction() {
            super(EditorActionsIF.SWAP_NONE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.swapNone();
        }
    }

    class SymmetryXAction extends BasicAction {
        SymmetryXAction() {
            super(EditorActionsIF.SYMMETRY_X, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.symmetryX();
        }
    }

    class SymmetryYAction extends BasicAction {
        SymmetryYAction() {
            super(EditorActionsIF.SYMMETRY_Y, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.symmetryY();
        }
    }

    class SymmetryPlusXYAction extends BasicAction {
        SymmetryPlusXYAction() {
            super(EditorActionsIF.SYMMETRY_PLUS_XY, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.symmetryPlusXY();
        }
    }

    class SymmetryMinusXYAction extends BasicAction {
        SymmetryMinusXYAction() {
            super(EditorActionsIF.SYMMETRY_MINUS_XY, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.symmetryMinusXY();
        }
    }

    class SymmetryNoneAction extends BasicAction {
        SymmetryNoneAction() {
            super(EditorActionsIF.SYMMETRY_NONE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.symmetryNone();
        }
    }

    class ModifyPortAction extends BasicAction {
        ModifyPortAction() {
            super(EditorActionsIF.PORT_MODIFY, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.modifyPort();
        }
    }

    class DeletePortAction extends BasicAction {
        DeletePortAction() {
            super(EditorActionsIF.PORT_DELETE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.deletePort();
        }
    }

    class ImportChannelsAction extends BasicAction {
        ImportChannelsAction() {
            super(EditorActionsIF.IMPORT_CHANNELS, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.importChannels();
        }
    }

    class RenamePortAction extends BasicAction {
        RenamePortAction() {
            super(EditorActionsIF.PORT_RENAME, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.renamePort();
        }
    }

    class DeleteConnectionAction extends BasicAction {
        DeleteConnectionAction() {
            super(EditorActionsIF.CONN_DELETE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.deleteConnection();
        }
    }

    class SmoothConnectionAction extends BasicAction {
        SmoothConnectionAction() {
            super(EditorActionsIF.CONN_SMOOTH, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.smoothConnections(false);
        }
    }

    class CreateJunctionInConnectionAction extends BasicAction {
        CreateJunctionInConnectionAction() {
            super(EditorActionsIF.CONN_CREATE_JUNCTION, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.addJunction();
        }
    }

    class CreateBundlerInConnectionAction extends BasicAction {
        CreateBundlerInConnectionAction() {
            super(EditorActionsIF.CONN_CREATE_BUNDLER, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.addBundler();
        }
    }

    class TrimConnectionAction extends BasicAction {
        TrimConnectionAction() {
            super(EditorActionsIF.CONN_TRIM, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.trimConnectionPath();
       }
    }

    class ShowBundleContentAction extends BasicAction {
        ShowBundleContentAction() {
            super(EditorActionsIF.SHOW_BUNDLE_CONTENT, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.showBundleContent();
       }
    }

    class DeleteJunctionAction extends BasicAction {
        DeleteJunctionAction() {
            super(EditorActionsIF.JUNCTION_DELETE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.deleteJunction();
        }
    }

    class DeleteBundlerAction extends BasicAction {
        DeleteBundlerAction() {
            super(EditorActionsIF.BUNDLER_DELETE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.deleteBundler();
        }
    }

    class RenameBundlerIOAction extends BasicAction {
        RenameBundlerIOAction() {
            super(EditorActionsIF.BUNDLER_IO_RENAME, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.renameBundlerIO();
        }
    }

    class BundlerPrimaryInToSecondaryAction extends BasicAction {
        BundlerPrimaryInToSecondaryAction() {
            super(EditorActionsIF.BUNDLER_PRIMARY_IN_TO_SECONDARY,
                                                               getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.bundlerPrimaryToSecondary(true);
        }
    }

    class BundlerPrimaryOutToSecondaryAction extends BasicAction {
        BundlerPrimaryOutToSecondaryAction() {
            super(EditorActionsIF.BUNDLER_PRIMARY_OUT_TO_SECONDARY,
                                                               getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.bundlerPrimaryToSecondary(false);
        }
    }

    class FontSizeXSmallAction extends BasicAction {
        FontSizeXSmallAction() {
            super(EditorActionsIF.TEXT_FONT_XSMALL, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextSize(
                                  AlfiFonts.getFontSize(AlfiFonts.FONT_XSMALL));
        }
    }

    class FontSizeSmallAction extends BasicAction {
        FontSizeSmallAction() {
            super(EditorActionsIF.TEXT_FONT_SMALL, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextSize(
                                   AlfiFonts.getFontSize(AlfiFonts.FONT_SMALL));
        }
    }

    class FontSizeMediumAction extends BasicAction {
        FontSizeMediumAction() {
            super(EditorActionsIF.TEXT_FONT_MEDIUM, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextSize(
                                  AlfiFonts.getFontSize(AlfiFonts.FONT_MEDIUM));
        }
    }

    class FontSizeLargeAction extends BasicAction {
        FontSizeLargeAction() {
            super(EditorActionsIF.TEXT_FONT_LARGE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextSize(
                                   AlfiFonts.getFontSize(AlfiFonts.FONT_LARGE));
        }
    }

    class FontSizeExtraLargeAction extends BasicAction {
        FontSizeExtraLargeAction() {
            super(EditorActionsIF.TEXT_FONT_XLARGE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextSize(
                          AlfiFonts.getFontSize(AlfiFonts.FONT_XLARGE));
        }
    }

    class FontSizeHeadingAction extends BasicAction {
        FontSizeHeadingAction() {
            super(EditorActionsIF.TEXT_FONT_HEADING, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextSize(
                         AlfiFonts.getFontSize(AlfiFonts.FONT_HEADING));
        }
    }

    class ChangeTextAction extends BasicAction {
        ChangeTextAction () {
            super(EditorActionsIF.TEXT_VALUE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextValue();
        }
    }

    class ToggleBoldAction extends BasicAction {
        ToggleBoldAction () {
            super(EditorActionsIF.TEXT_FONT_BOLD, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextBold();
        }
    }

    class ToggleUnderlineAction extends BasicAction {
        ToggleUnderlineAction () {
            super(EditorActionsIF.TEXT_FONT_UNDERLINE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextUnderline();
        }
    }

    class ToggleItalicAction extends BasicAction {
        ToggleItalicAction () {
            super(EditorActionsIF.TEXT_FONT_ITALIC, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextItalic();
        }
    }

    class BlackTextAction extends BasicAction {
        BlackTextAction () {
            super(EditorActionsIF.TEXT_COLOR_BLACK, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_BLACK);
        }
    }

    class WhiteTextAction extends BasicAction {
        WhiteTextAction () {
            super(EditorActionsIF.TEXT_COLOR_WHITE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_WHITE);
        }
    }

    class BlueTextAction extends BasicAction {
        BlueTextAction () {
            super(EditorActionsIF.TEXT_COLOR_BLUE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_BLUE);
        }
    }

    class CyanTextAction extends BasicAction {
        CyanTextAction () {
            super(EditorActionsIF.TEXT_COLOR_CYAN, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_CYAN);
        }
    }

    class GreenTextAction extends BasicAction {
        GreenTextAction () {
            super(EditorActionsIF.TEXT_COLOR_GREEN, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_GREEN);
        }
    }

    class LightGrayTextAction extends BasicAction {
        LightGrayTextAction () {
            super(EditorActionsIF.TEXT_COLOR_LIGHT_GRAY, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_LIGHT_GRAY);
        }
    }

    class MagentaTextAction extends BasicAction {
        MagentaTextAction () {
            super(EditorActionsIF.TEXT_COLOR_MAGENTA, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_MAGENTA);
        }
    }

    class OrangeTextAction extends BasicAction {
        OrangeTextAction () {
            super(EditorActionsIF.TEXT_COLOR_ORANGE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_ORANGE);
        }
    }

    class PinkTextAction extends BasicAction {
        PinkTextAction () {
            super(EditorActionsIF.TEXT_COLOR_PINK, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_PINK);
        }
    }

    class RedTextAction extends BasicAction {
        RedTextAction () {
            super(EditorActionsIF.TEXT_COLOR_RED, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_RED);
        }
    }

    class YellowTextAction extends BasicAction {
        YellowTextAction () {
            super(EditorActionsIF.TEXT_COLOR_YELLOW, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setTextColor(AlfiColors.ALFI_YELLOW);
        }
    }

    class ModifyHiliteLayer extends BasicAction {
        ModifyHiliteLayer () {
            super(EditorActionsIF.HILITE_LAYER_MODIFY, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.switchHighlightLayer();
        }
    }

    class SendHiliteToBack extends BasicAction {
        SendHiliteToBack () {
            super(EditorActionsIF.HILITE_SEND_TO_BACK, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.sendHighlightToBack();
        }
    }

    class SendHiliteToFront extends BasicAction {
        SendHiliteToFront () {
            super(EditorActionsIF.HILITE_SEND_TO_FRONT, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.sendHighlightToFront();
        }
    }

    class BlueBackgroundAction extends BasicAction {
        BlueBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_BLUE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_BLUE);
        }
    }

    class CyanBackgroundAction extends BasicAction {
        CyanBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_CYAN, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_CYAN);
        }
    }

    class GreenBackgroundAction extends BasicAction {
        GreenBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_GREEN, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_GREEN);
        }
    }

    class LightGrayBackgroundAction extends BasicAction {
        LightGrayBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_LIGHT_GRAY, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_LIGHT_GRAY);
        }
    }

    class MagentaBackgroundAction extends BasicAction {
        MagentaBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_MAGENTA, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_MAGENTA);
        }
    }

    class OrangeBackgroundAction extends BasicAction {
        OrangeBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_ORANGE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_ORANGE);
        }
    }

    class PinkBackgroundAction extends BasicAction {
        PinkBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_PINK, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_PINK);
        }
    }

    class RedBackgroundAction extends BasicAction {
        RedBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_RED, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_RED);
        }
    }

    class YellowBackgroundAction extends BasicAction {
        YellowBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_YELLOW, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_YELLOW);
        }
    }

    class DarkBlueBackgroundAction extends BasicAction {
        DarkBlueBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_DARK_BLUE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_DARK_BLUE);
        }
    }

    class DarkCyanBackgroundAction extends BasicAction {
        DarkCyanBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_DARK_CYAN, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_DARK_CYAN);
        }
    }

    class DarkGreenBackgroundAction extends BasicAction {
        DarkGreenBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_DARK_GREEN, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_DARK_GREEN);
        }
    }

    class DarkGrayBackgroundAction extends BasicAction {
        DarkGrayBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_DARK_GRAY, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_DARK_GRAY);
        }
    }

    class DarkMagentaBackgroundAction extends BasicAction {
        DarkMagentaBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_DARK_MAGENTA, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_DARK_MAGENTA);
        }
    }

    class DarkOliveGreenBackgroundAction extends BasicAction {
        DarkOliveGreenBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_DARK_OLIVE_GREEN, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_DARK_OLIVE_GREEN);
        }
    }

    class DarkOrangeBackgroundAction extends BasicAction {
        DarkOrangeBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_DARK_ORANGE, 
                                                        getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(
                                           AlfiColors.ALFI_DARK_ORANGE);
        }
    }

    class DarkRedBackgroundAction extends BasicAction {
       DarkRedBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_DARK_RED, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(AlfiColors.ALFI_DARK_RED);
        }
    }

    class DarkYellowBackgroundAction extends BasicAction {
        DarkYellowBackgroundAction () {
            super(EditorActionsIF.HILITE_COLOR_DARK_YELLOW, 
                                                        getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_editor_frame.setHighlightColor(
                                           AlfiColors.ALFI_DARK_YELLOW);
        }
    }

}
