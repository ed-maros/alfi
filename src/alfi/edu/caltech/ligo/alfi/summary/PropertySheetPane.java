/** 
 *  This PropertySheetPane allows the display of the ALFINode settings
 *
 */
package edu.caltech.ligo.alfi.summary;

import com.l2fprod.common.propertysheet.Property;
import com.l2fprod.common.propertysheet.DefaultProperty;
import com.l2fprod.common.propertysheet.PropertySheet;
import com.l2fprod.common.propertysheet.PropertySheetPanel;
import com.l2fprod.common.propertysheet.PropertySheetTable;
import com.l2fprod.common.propertysheet.PropertySheetTableModel;
import com.l2fprod.common.propertysheet.PropertySheetTableModel.Item;
import com.l2fprod.common.swing.LookAndFeelTweaks;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.*;
import java.util.StringTokenizer;
import java.util.HashSet;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.editor.EditorFrame;


/**
 * PropertySheetPane. <br>
 *  
 */
public class PropertySheetPane extends JPanel implements
                ProxyChangeEventListener,
                NodeContentChangeEventListener,
                ContainedElementModEventListener {

    private static final int HOTSPOT_SIZE = 18;
    private static final String GENERAL_CATEGORY = "General";
    private static final String INPUT_PORT_CATEGORY = "Input Port Default";
    private static final String PARAM_SETTING_CATEGORY = "Parameter Setting";

    private static final String MACRO_PROPERTY = "Macros";
    private static final String LABEL_PROPERTY = "Label";
    private static final String COMMENT_PROPERTY = "Comment";
    private static final String NODE_TYPE_PROPERTY = "Node Type";
    private static final String BASE_NODE_PROPERTY = "Base Node";
    private static final String CONTAINER_NODE_PROPERTY = "Container Node";

    private static final String PRIMITIVE_NODE_TYPE = "Primitive";
    private static final String BOX_NODE_TYPE = "Box";
 
    private ALFINode m_node;
    private PropertySheetPanel m_sheet = null;

    public PropertySheetPane( ALFINode _node ) {
        setLayout(LookAndFeelTweaks.createVerticalPercentLayout());

        m_node = _node;
    
        initializePanelContents();

        setBackground(Color.white);

        m_node.addContainedElementModListener(this);
        m_node.addNodeContentChangeListener(this);

        if (m_node.memberNodeProxy_getAssociated() != null) {
            m_node.memberNodeProxy_getAssociated().addProxyChangeListener(this);
        }
    }

    private void initializePanelContents() {
        JTextArea message = new JTextArea();
        String text = new String ("Node Information for: " + m_node.generateFullNodePathName());
        message.setText(text);
        message.setBackground(Color.white);
        add(message);

        m_sheet = new PropertySheetPanel();
        m_sheet.setMode(PropertySheet.VIEW_AS_CATEGORIES);
        addGeneralInformation(m_sheet);
        addParameterSettings(m_sheet);
        m_sheet.setDescriptionVisible(true);
        m_sheet.setSortingCategories(true);
        m_sheet.setSortingProperties(false);
        m_sheet.setBackground(Color.white);

        add(m_sheet, "*");
        collapseCategories(m_sheet);

    }

    public void refresh() {
        if (m_sheet != null) {
            PropertySheetTable table = m_sheet.getTable();
            PropertySheetTableModel model = table.getSheetModel();
            Property[] properties = model.getProperties();
            for (int i = properties.length - 1; i >= 0; i--) {
                model.removeProperty(properties[i]); 
            }
            m_sheet.removeAll();
            removeAll();
            m_sheet = null;
        }

        initializePanelContents();
        validate();
    }
    
    public void cleanUp() {
        if (m_node.memberNodeProxy_getAssociated() != null) {
            m_node.memberNodeProxy_getAssociated().removeProxyChangeListener(this);
        }
        m_node.removeContainedElementModListener(this);
        m_node.removeNodeContentChangeListener(this);
     }

    // General information
    private void addGeneralInformation (PropertySheetPanel _sheet){ 

        ALFINode root_base_node = 
                m_node.isRootNode() ? m_node : m_node.baseNode_getRoot();

        // Primitive or box?
        DefaultProperty node_type = new NoReadWriteProperty();
        node_type.setDisplayName(NODE_TYPE_PROPERTY);
        String type = m_node.isPrimitive() ? PRIMITIVE_NODE_TYPE 
                                           : BOX_NODE_TYPE;
        node_type.setValue(type);
        node_type.setShortDescription(type);
        node_type.setCategory(GENERAL_CATEGORY);
        _sheet.addProperty(node_type);

        // Base Node Information
        DefaultProperty base_node = new NoReadWriteProperty();
        base_node.setDisplayName(BASE_NODE_PROPERTY);
        base_node.setValue(root_base_node.toString());
        base_node.setShortDescription(root_base_node.toString());
        base_node.setCategory(GENERAL_CATEGORY);
        _sheet.addProperty(base_node);

        // Container Node Information
        if (!m_node.isRootNode()){
            DefaultProperty container = new NoReadWriteProperty();
            container.setDisplayName(CONTAINER_NODE_PROPERTY);
            container.setValue(m_node.containerNode_get().toString());
            container.setShortDescription(m_node.containerNode_get().toString());
            container.setCategory(GENERAL_CATEGORY);
            _sheet.addProperty(container);
        }

        // Comment Information
        if (m_node.comment_getLocal() != null) {
            DefaultProperty comment = new NoReadWriteProperty();
            comment.setDisplayName(COMMENT_PROPERTY);
            comment.setValue(m_node.comment_getLocal());
            String description = new String("<pre>" + m_node.comment_getLocal() + "</pre>");
            comment.setShortDescription(description);
            comment.setCategory(GENERAL_CATEGORY);
            _sheet.addProperty(comment);
        }
        
        DefaultProperty label_type = new NoReadWriteProperty();
        label_type.setDisplayName(LABEL_PROPERTY);
        if (m_node.getLabelType() == ALFINode.LABEL_TYPE_ICON) {
            label_type.setValue(root_base_node.getIconFilePath());
            label_type.setShortDescription(root_base_node.getIconFilePath());
        }
        else {
            label_type.setValue(m_node.getTextLabel());
            label_type.setShortDescription(m_node.getTextLabel());
        }
        label_type.setCategory(GENERAL_CATEGORY);
        _sheet.addProperty(label_type);
    
        StringBuffer buffer = new StringBuffer();

        Macro macros_local = m_node.macro_getLocal();
        if (macros_local != null) {
            buffer.append(macros_local.getValue() + Parser.NL);
        }
        Macro[] macros_inherited = m_node.macros_getInherited();
        for (int ii = 0; ii < macros_inherited.length; ii++) {
            if (ii == 0){
                buffer.append("\\ " + Parser.NL);
            }
            buffer.append(macros_inherited[ii].getValue() + Parser.NL);
        }

        if (buffer.length() > 0) {
            DefaultProperty macro = new NoReadWriteProperty();
            macro.setDisplayName(MACRO_PROPERTY);
            macro.setValue(buffer.toString());
            String description = new String("<pre>" + buffer.toString()+ "</pre>");
            macro.setShortDescription(description);
            macro.setCategory(GENERAL_CATEGORY);
            _sheet.addProperty(macro);
        }
    }

    // Parameter Settings and Input Port Defaults
    private void addParameterSettings (PropertySheetPanel _sheet) {

        HashSet parameters = new HashSet();

        ALFINode[] base_nodes = m_node.baseNodes_getHierarchyPath();

        boolean this_node = true;

        for (int i = base_nodes.length - 1; i >= 0; i--) {
            NodeSettingIF node_setting[] =  base_nodes[i].settings_getLocal();
            for (int j = 0; j < node_setting.length; j++) {
                String name = new String(node_setting[j].getName());
                if (!parameters.contains(name)){
                    parameters.add(name);
                    DefaultProperty setting = new NoReadWriteProperty();
                    setting.setDisplayName(name);
                    setting.setValue(node_setting[j].getValue());
                    String description =
                        new String("<pre>" + 
                                    node_setting[j].getValue() + 
                                    "</pre>");
                    setting.setShortDescription(description);
                    if (this_node) {
                        name = new String("* " +node_setting[j].getName());
                        setting.setDisplayName(name);
                    }
                    if (m_node.isPrimitive() && 
                        !m_node.parameterDeclaration_contains(node_setting[j].getName())){
                        setting.setCategory(INPUT_PORT_CATEGORY);
                    }
                    else {
                        setting.setCategory(PARAM_SETTING_CATEGORY);
                    }
                    _sheet.addProperty(setting);
                }
            }
            this_node = false;
        }

        // Now add the parameter declarations
        NodeSettingDeclarationIF[] declarations = 
                                  base_nodes[0].settingDeclarations_getLocal();

        for (int k = 0; k < declarations.length; k++) {
            String name = new String(declarations[k].getName());
            if (!parameters.contains(name)){
                parameters.add(name);
                DefaultProperty setting = new NoReadWriteProperty();
                setting.setDisplayName(name);
                setting.setValue(declarations[k].getValue());
                String description = new String("<pre>" + 
                                        declarations[k].getValue() + 
                                        "</pre>");
                setting.setShortDescription(description);
                setting.setCategory(PARAM_SETTING_CATEGORY);
                 _sheet.addProperty(setting);
            }
        }
    }


    private void collapseCategories (PropertySheetPanel _sheet){ 

        PropertySheetTable table = _sheet.getTable();
        PropertySheetTableModel model = table.getSheetModel();

        for (int i = 0; i < model.getRowCount(); i++) {
            Item item = model.getPropertySheetElement(i);
            String name = item.getName();
            boolean is_category_visible = true;
            if ( (name.equals(GENERAL_CATEGORY))    ||
                 (name.equals(INPUT_PORT_CATEGORY)) ||
                 (name.equals(PARAM_SETTING_CATEGORY)) ) {
                is_category_visible = getCategorySetting(name);

                if (is_category_visible != item.isVisible()){
                    item.toggle();
                }
            }
            else {
                continue;
            }
        }

        class SheetMouseHandler extends MouseAdapter {
            public void mouseClicked(MouseEvent e) {
                if (e.getSource() == null) return;

                if ( (e.getClickCount()) == 2 && 
                     (SwingUtilities.isLeftMouseButton(e)) ) {
                    PropertySheetTable table = 
                        (PropertySheetTable) e.getComponent();
                    int row = table.rowAtPoint(e.getPoint());
                    int column = table.columnAtPoint(e.getPoint());
                    if (row != -1) {
                        Item item = table.getSheetModel().getPropertySheetElement(row);        
                        int x = e.getX() - getIndent(table, item);
                        if (x > HOTSPOT_SIZE) {
                            processEditAction(item);    
                        }
                    }
                }  
            }

            public void mouseReleased(MouseEvent event) {
                super.mouseReleased(event);
                PropertySheetTable table = 
                    (PropertySheetTable) event.getComponent();
                int row = table.rowAtPoint(event.getPoint());
                int column = table.columnAtPoint(event.getPoint());
                if (row != -1 && column == 0) {
                    // if we clicked on Item, see if we clicked on its hotspot
                    Item item =
                            table.getSheetModel().getPropertySheetElement(row);
                    int x = event.getX() - getIndent(table, item);
                    if (x > 0 && x < HOTSPOT_SIZE) {
                        String name = item.getName();
                        setCategorySetting(name, item.isVisible());    
                    }
                    else {
                    }
                }  
            }
        }

        table.addMouseListener(new SheetMouseHandler());
    }

    private boolean getCategorySetting (String _name) {
        boolean is_category_visible = true;

        if (_name.equals(GENERAL_CATEGORY)) {
            is_category_visible = 
                Alfi.getMainWindow().isGeneralCategoryExpanded();
        }
        else if (_name.equals(INPUT_PORT_CATEGORY)) {
            is_category_visible = 
                Alfi.getMainWindow().isInputDefaultsCategoryExpanded();
        }
        else if (_name.equals(PARAM_SETTING_CATEGORY)) {
            is_category_visible = 
                Alfi.getMainWindow().isParameterCategoryExpanded();
        }
        else {
            // do nothing
        }

        return  is_category_visible;
    }

    private void setCategorySetting (String _name, boolean _is_expanded) {

        if (_name.equals(GENERAL_CATEGORY)) {
            Alfi.getMainWindow().setGeneralCategoryExpanded(_is_expanded);
        }
        else if (_name.equals(INPUT_PORT_CATEGORY)) {
            Alfi.getMainWindow().setInputDefaultsCategoryExpanded(_is_expanded);
        }
        else if (_name.equals(PARAM_SETTING_CATEGORY)) {
             Alfi.getMainWindow().setParameterCategoryExpanded(_is_expanded);
        }
        else {
            //do nothing
        }

    }

    private void processEditAction (Item _item) {
        
        if (_item.isProperty()) {
            Property property = _item.getProperty();
            String category = property.getCategory();
            String name = _item.getName();
            if (category.equals(PARAM_SETTING_CATEGORY)) {
                EditorFrame.editSettings(Alfi.getMainWindow(), m_node);
            }
            else if (name.equals(COMMENT_PROPERTY)) {
                EditorFrame.editComment(Alfi.getMainWindow(), m_node);
            }
            else if (name.equals(MACRO_PROPERTY)) {
                EditorFrame.editMacros(Alfi.getMainWindow(), m_node);
            }
            else if (name.equals(CONTAINER_NODE_PROPERTY)) {
                ALFINode container =  
                   (m_node.isRootNode() ? m_node : m_node.containerNode_get());
                Alfi.getMainWindow().showNodeWindow(container); 
            }
            else if (name.equals(BASE_NODE_PROPERTY)) {
                ALFINode base_node =  
                   (m_node.isRootNode() ? m_node : m_node.baseNode_getRoot());
                Alfi.getMainWindow().showNodeWindow(base_node); 
            }
            else if (name.equals(LABEL_PROPERTY)) {
                ALFINode container =  
                   (m_node.isRootNode() ? m_node : m_node.containerNode_get());
                boolean modifiable = container.isRootNode();
                if (container == m_node) {
                    modifiable = false;
                }
                EditorFrame.renameNode(Alfi.getMainWindow(), m_node,
                    container, modifiable);
            }
        }
        
    }
        
    /**
    * Calculates the required left indent for a given item, given its type and
    * its hierarchy level.
    */
    static int getIndent (PropertySheetTable table, Item item) {
      int indent = 0;
      
      if (item.isProperty()) {
        // it is a property, it has no parent or a category, and no child
        if ((item.getParent() == null || !item.getParent().isProperty())
          && !item.hasToggle()) {
          indent = table.getWantsExtraIndent()?HOTSPOT_SIZE:0;
        } else {
          // it is a property with children
          if (item.hasToggle()) {
            indent = item.getDepth() * HOTSPOT_SIZE;
          } else {          
            indent = (item.getDepth() + 1) * HOTSPOT_SIZE;
          }        
        }
        
        if (table.getSheetModel().getMode() == PropertySheet.VIEW_AS_CATEGORIES
          && table.getWantsExtraIndent()) {
            indent += HOTSPOT_SIZE;
        }
  
        } else {
        // category has no indent
          indent = 0;
        }    
      return indent;
    }
 
    //////////////////////////////////////////////////////////
    //////////  ProxyChangeEventListener methods /////////////

        /** ProxyChangeEventListener method. */
    public void proxyChangePerformed(ProxyChangeEvent _event) {
        refresh();
    }


        /** NodeContentChangeEventListener method. */
    public void nodeContentChanged(NodeContentChangeEvent _event) {
        refresh();
    }

    ///////////////////////////////////////////////////////////////////
    ////////// ContainedElementModEventListener methods ///////////////

        /** ContainedElementModEventListener method. */
    public void containedElementModified(ContainedElementModEvent _event) {
        refresh();
    }



    static class NoReadWriteProperty extends DefaultProperty {
      public void readFromObject(Object object) {
        
      }
      public void writeToObject(Object object) {
      }
    }
 
}
