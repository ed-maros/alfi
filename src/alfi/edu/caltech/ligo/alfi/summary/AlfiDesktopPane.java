package edu.caltech.ligo.alfi.summary;

import java.io.*;
import java.util.*;
import java.net.*;

/*
import java.awt.event.*;
*/

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import javax.swing.*;
import javax.swing.event.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.editor.EditorFrame;
import edu.caltech.ligo.alfi.editor.ALFIView;
import edu.caltech.ligo.alfi.tools.*;

/**    
  * <pre>
  * The desktop pane that displays all the windows tiled and
  * proportional to the full sized window.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class AlfiDesktopPane extends JDesktopPane 
                             implements InternalFrameListener { 

    /** The EditorFrame sizes are divided into units of this number */
    public static final int FRAME_UNITS = 200;

    /** A map of JInternalFrames vs ALFINode */ 
     private HashMap m_frame_vs_node_map; 

    /** A sorted list of opened EditorFrames.  The list is sorted based on 
      * the longest side of the frame
      */ 
    private final List m_opened_frames_list;

    /** A map of JInternalFrames vs ALFINode */ 
    private ALFINode m_node_to_display;

    /** The number of windows displayed */ 
    private int m_window_count;

    /** A map of JInternalFrames vs ALFINode */ 
    private final JDialog m_parent;

    /** Number of rows and columns for the imaginary grid */
    private int m_rows;
    private int m_columns;

    /** The window placement grid.  
      * Track which part of the screen contains a window.
      */
    private int m_window_map[][];
                    
    /** Screen dimensions saved for use in the package */ 
    private final Insets m_insets;
    private final Dimension m_content_pane; 

    ////////////////////////////////////////////////////////////////////////////
    ////////// constructors ////////////////////////////////////////////////////

    public AlfiDesktopPane (JDialog _parent) {
        super();

        putClientProperty("JDesktopPane.dragMode", "outline");

        m_parent = _parent;
        m_window_count = 0;
        m_node_to_display = null;

        m_frame_vs_node_map = new HashMap();
        m_insets = Alfi.getMainWindow().getInsets();
        Dimension content_pane = Toolkit.getDefaultToolkit().getScreenSize(); 
        double window_size_width = content_pane.getWidth() - m_insets.left - m_insets.right;
        double window_size_height = content_pane.getHeight() - m_insets.top - m_insets.bottom;
        m_content_pane = new Dimension();
        m_content_pane.setSize(window_size_width, window_size_height);

        ToolTipManager.sharedInstance().setEnabled(true);

        EditorFrame[] editors = Alfi.getTheNodeCache().getEditSessions();

        int total_window_units = 0;

        m_opened_frames_list = new ArrayList();

        for (int i = 0; i < editors.length; i++) {
            if (editors[i].isShowing()) {
                m_window_count++;
                Dimension window_ratio = calculateWindowRatio(editors[i]);

                // Add the window to a list
                m_opened_frames_list.add(editors[i]);

                total_window_units += (window_ratio.height * window_ratio.width);
            }
        }

        // Sort the list based on the frames' side length... longest to shortest.
        Collections.sort(m_opened_frames_list, new EditorFrameSizeComparator());

        double square_root = Math.sqrt(total_window_units);
        m_rows = ( int )Math.ceil(square_root);

        // For now make,  the grid a square
        m_columns = m_rows;
        initialize(m_rows);
    }

    /*
     * Initialize the JDesktopPnae with _rows and the same number for 
     * columns
     */
        private void initialize (int _rows) {
        initializeWindowMap();

	    Dimension window_size = new Dimension();
        double window_size_width = 
            m_content_pane.getWidth() / ( double )m_columns; 
        double window_size_height = 
            m_content_pane.getHeight() / ( double )m_rows; 
        window_size.setSize(window_size_width, window_size_height);

        createFrames(window_size);
    }

    /*
     * Calculates the ratio between the sides of the EditorFrame.
     */
    private Dimension calculateWindowRatio (EditorFrame _editor) {
        int frame_height = _editor.getHeight();
        int frame_width = _editor.getWidth();
        int height_units = 0;
        int width_units = 0;
        float frame_in_units_of = ( float )FRAME_UNITS;

        width_units = (int) Math.ceil(( double )frame_width/frame_in_units_of); 
        height_units = (int) Math.ceil(( double )frame_height/frame_in_units_of); 

        return new Dimension(width_units, height_units);        
    }

    /*
     * Clears out any previously created JInternalFrames and
     * zeros out the m_window_map.
     */
    private void initializeWindowMap () {
        if (m_window_map != null) {
            m_window_map = null;

            JInternalFrame[] existing_frames = getAllFrames();
            for (int i = 0; i < existing_frames.length; i++) {
                existing_frames[i].dispose();
            }
        }

        m_window_map = new int[m_rows][m_columns];
        for (int i = 0; i < m_rows; i++) {
            for (int j = 0; j < m_columns; j++) {
                m_window_map[i][j] = 0;
            }
        }
    }

    /*
     * Determines the next available location on the screen.
     */
    private Point determineScreenLocation (Dimension _ratio) {
        int width = ( int ) _ratio.getWidth();
        int height = ( int ) _ratio.getHeight();

        int row = -1;
        int column = -1;
        int start_row = 0;
        int start_col = 0;
        int end_row = 0;
        int end_col = 0;


        while ( (row < 0) && ((start_row + height) <= m_rows ) ) {
            end_row = start_row + height - 1;

            start_col = 0;
            while ( (column < 0) && ((start_col + width) <= m_columns) ) {
                end_col = start_col + width - 1;

                boolean skip = false;

                int i = start_row;
                int j = start_col;
                while (i <= end_row && !skip) {
                    j = start_col;
                    while (j <= end_col  && !skip) {
                        if (m_window_map[i][j] != 0) {
                            skip = true;
                        }
                        j++;
                    }
                    i++;
                }
                        
                if (skip) {
                    start_col++;
                }
                else if (j > end_col && i > end_row) {
                    row = start_row;
                    column = start_col;
                    for (int k = start_row; k <= end_row; k++) {
                        for (int l = start_col; l <= end_col; l++) {
                            m_window_map[k][l] = 1;
                        }
                    }
                }
                else {
                    start_col++;
                }
            } // while col

            if (row < 0) { start_row++; }
        } // while row


        return new Point(row, column);
    } 


    /*
     * Creates the frames to be displayed in the DesktopPane.
     */
    private void createFrames (Dimension _window_unit_size) {
        
        int row = 0;
        int column = 0;
        int windows_created = 0;    

        Dimension view_size = new Dimension(_window_unit_size);

        for (int i = m_opened_frames_list.size() - 1; i >= 0; i--) {
            int inset_width = 0;
            int inset_height = 0;

            EditorFrame frame = ( EditorFrame )m_opened_frames_list.get(i);
            ALFIView original_view = frame.getView();

            Dimension window_ratio = calculateWindowRatio(frame);

            double view_width = (_window_unit_size.getWidth() * window_ratio.getWidth())
                                    - m_insets.left - m_insets.right; 
            double view_height = (_window_unit_size.getHeight() * window_ratio.getHeight())
                                    - m_insets.top - m_insets.bottom;
            if (view_width > original_view.getWidth() ||
                view_height > original_view.getHeight() ) {
                view_size.setSize(original_view.getSize());
            }
            else {
                view_size.setSize(view_width, view_height);
            }
            
            Point location = determineScreenLocation(window_ratio);
            int left = ( int )(location.getX() * _window_unit_size.getHeight());
            int top = (int )(location.getY() * _window_unit_size.getWidth());
            location.setLocation(top, left);

            if ((top < 0) && (left < 0)) {
                m_rows++;
                m_columns++;
                // Recursive
                initialize(m_rows);
                return;
            }
            else {
                // We found a valid location 
	            createFrame(frame, location, view_size);
            }
        }
    }


    /*
     * Creates the JInternalFrame for this JDesktopPane
     */
    private void createFrame (EditorFrame _editor, Point _location, 
                              Dimension _view_size) {

        final JInternalFrame frame = new JInternalFrame(_editor.getName(), 
            false, false, false);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addInternalFrameListener(this);

        ALFINode node = _editor.getNodeInEdit();
        final JGoOverview overview = new AlfiOverview(node.generateFullNodePathName());  
        ALFIView original_view = _editor.getView();
        overview.setObserved(original_view);

        // Determine the optimal scale for the overview
        double scale = 1.0d;
        double width_scale = 1.0d;
        double height_scale = 1.0d;

        double view_width = original_view.getWidth(); 
        double view_height = original_view.getHeight();
        double internal_view_width = _view_size.getWidth();  
        double internal_view_height = _view_size.getHeight(); 
        
        if (view_width  > internal_view_width) {
            width_scale = internal_view_width/view_width;
        }
        if (view_height > internal_view_height) { 
            height_scale = internal_view_height/view_height;
        }
        
        scale = Math.min(width_scale, height_scale);
    
        // Adjust the frame, add the m_insets
        double internal_frame_width = (view_width * scale) 
                                        + m_insets.left + m_insets.right;
        double internal_frame_height = (view_height * scale) 
                                        + m_insets.top + m_insets.bottom;
        overview.setScale(scale);

        frame.getContentPane().add(overview, BorderLayout.CENTER);

        Dimension frame_size = new Dimension();
        frame_size.setSize(internal_frame_width,  internal_frame_height);
        frame.setSize(frame_size);
        frame.setLocation(_location);
        
        m_frame_vs_node_map.put(frame, node);
        add(frame);
        frame.show();
        
    }

    /*
     *  Access to the node which has been clicked.
     */
    public ALFINode getNodeToDisplay () {
        return m_node_to_display;
    }

    //////////////////////////////////////////////////////////
    ////////// InternalFrameListener methods//////////////////
    //////////////////////////////////////////////////////////
    public void internalFrameClosing(InternalFrameEvent e) { }

    public void internalFrameClosed(InternalFrameEvent e) { }

    public void internalFrameOpened(InternalFrameEvent e) { }

    public void internalFrameIconified(InternalFrameEvent e) { }

    public void internalFrameDeiconified(InternalFrameEvent e) { }

    public void internalFrameActivated(InternalFrameEvent e) {
        m_node_to_display = 
            ( ALFINode ) m_frame_vs_node_map.get(e.getInternalFrame());
        m_parent.setVisible(false);
    }

    public void internalFrameDeactivated(InternalFrameEvent e) { }


}     

