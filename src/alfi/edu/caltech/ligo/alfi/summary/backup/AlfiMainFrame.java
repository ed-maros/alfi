package edu.caltech.ligo.alfi.summary;

import java.io.*;
import java.util.*;
import java.net.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.editor.EditorFrame;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.widgets.*;

/**    
  * <pre>
  * The outmost frame.
  * </pre>
  * There is only one MainFrame in the application.
  * The MainFrame may have multiple children
  * which are managed by the WindowManager.
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.AlfiFrame
  */
public class AlfiMainFrame extends AlfiFrame 
            implements AlfiMainFrameIF, ActionsIF, TreeSelectionListener {

    private AlfiMenuBar m_menubar;
    private FindBoxFile m_find_box_file = null;

    private JTree m_container_tree_widget;
    private JTree m_inheritance_tree_widget;

        /** kluge.  search on TreeWillExpandListener for explanation. */
    private boolean mb_temporarilly_disallow_tree_expansion;

    private boolean mb_abort_quit;

    public boolean mb_suppress_rename_warning = false;


    ////////////////////////////////////////////////////////////////////////////
    ////////// constructors ////////////////////////////////////////////////////

    public AlfiMainFrame (UIResourceBundle _resources) {
        super(null, _resources.getName(AlfiMainFrameIF.PARENT_FRAME),
                                                                   _resources);
        setVisible(false);

        m_container_tree_widget = null;
        m_inheritance_tree_widget = null;
        mb_temporarilly_disallow_tree_expansion = false;

        initializeFrame();
        setParentFrame(this);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new MainFrameAdapter());

        updateTipDisplayStatus();


        mb_abort_quit = false;
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////// static methods //////////////////////////////////////////////////

        /** Gets the color in which to display member nodes in both editor
          * windows and in the node tree(s).
          */
    public static Color determineNodeDisplayColor(ALFINode _node) {
        Color node_color = Color.black;

        boolean b_is_root_node = _node.isRootNode();

        if (b_is_root_node) {
            node_color = GuiConstants.ROOT_NODE_COLOR;
        }
        else {
            boolean b_is_modified = _node.isModifiedFromDirectBase(false);
            boolean b_is_local =
                  _node.containerNode_get().memberNodeProxy_containsLocal(
                                        _node.memberNodeProxy_getAssociated());
            if (b_is_local) {
                if (b_is_modified) {
                    node_color = GuiConstants.LOCAL_MODIFIED_COLOR;
                }
                else { node_color = GuiConstants.LOCAL_UNMODIFIED_COLOR; }
            }
            else {
                if (b_is_modified) {
                    node_color = GuiConstants.INHERITED_MODIFIED_COLOR;
                }
                else { node_color = GuiConstants.INHERITED_UNMODIFIED_COLOR; }
            }
        }

        return node_color;
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////// instance methods ////////////////////////////////////////////////

    class MainFrameAdapter extends WindowAdapter{

        public void windowActivated (WindowEvent e) {
/*
            WindowManager wm = getWindowManager();
            if (wm != null) {
                wm.windowActivated (e);
            }
*/
        }
        
        public void windowClosing (WindowEvent e) {
            quit();
        }
    }

    protected void cleanUp () throws CanceledOperationException {
        exit();
    }

    public void initializeFrame () {
        super.initializeFrame();

        m_menubar = new AlfiMenuBar(m_actions);
       
        JPanel menuPanel = new JPanel(new BorderLayout(), true);
        menuPanel.add(m_menubar, BorderLayout.NORTH);        
        
        getContentPane().add(menuPanel, BorderLayout.NORTH); 

        // show the container and inheritance trees in the main frame
        addTrees();

        m_container_tree_widget.addTreeSelectionListener(this);
    }

    public void setAllowTreeExpansion(boolean _b_allow_expansion) {
        mb_temporarilly_disallow_tree_expansion = !_b_allow_expansion;
    }

    public void run () {
        resetSize();
        centerFrame();
        setOptionsFromPreviousSession();
        showPrimitiveToolbar(false);
        m_find_box_file = new FindBoxFile(this, Alfi.getTheNodeCache());
        m_find_box_file.setVisible(false);
    }

    public JTree getContainerTreeWidget () { return m_container_tree_widget; }

    protected JMenuBar setDefaultMenuBar () {
        return (JMenuBar) new AlfiMenuBar(m_actions);
    }

    /**
      * Initializes the Menu Bar Actions
      *
      * @see edu.caltech.ligo.alfi.summary.AlfiActions
      */

    protected Actions initializeActions () {
        m_actions = new AlfiActions(getResources(), this);

        BasicActionIF action = (BasicActionIF) m_actions.
            getAction(ActionsIF.NEW_BOX);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.LOAD);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.RELOAD);
        action.setEnabled(false);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.UNLOAD);
        action.setEnabled(false);

        action= (BasicActionIF) m_actions.getAction(ActionsIF.SHOW_PREFERENCES);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.SAVE);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.QUIT);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.FIND);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.PRIMITIVE_TB);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.TIP);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.TIP_INHERITED);
        action.setEnabled(true);

        action=(BasicActionIF)m_actions.getAction(ActionsIF.MACRO_INFO_IN_TREE);
        action.setEnabled(true);

        action=(BasicActionIF)m_actions.getAction(ActionsIF.MACRO_INFO_ON_NODE);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.AUTO_SAVE);
        action.setEnabled(false);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.CONN_ON_TOP);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.RESTORE_WINDOWS);
        action.setEnabled(true);

        action= (BasicActionIF) m_actions.getAction(ActionsIF.TRIM_CONNECTIONS);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(
                                               ActionsIF.PERSISTENT_CLIPBOARD);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(
                                                ActionsIF.AUTO_VALIDATE_NODES);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.VALIDATE_NODES);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.ABOUT);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(ActionsIF.UPDATES);
        action.setEnabled(true);

        return m_actions;
    }
    
    public JPopupMenu getPrimitivesMenu () {
        return Alfi.getPrimitivesMenu();
    }

    public boolean isToolbarSelected () {
        return Alfi.getPrimitivesToolbar().hasButtonSelected();
    }

        /** Inserts a default name for the new box. */
    public void newBox () { newBox("NAME.box"); }

    public void newBox (String _default_file_name) {
        JFileChooser file_chooser = new JFileChooser(); 
        file_chooser.setCurrentDirectory(Alfi.WORKING_DIRECTORY);

            // accept only box files
        final ExtensionFileFilter filter = new ExtensionFileFilter(); 
        filter.addExtension(ALFIFile.BOX_NODE_FILE_SUFFIX);
        filter.setDescription(AlfiConstants.DEFAULT_FILE_DESCRIPTOR);
        file_chooser.setFileFilter(filter);

            // set the default to NAME.box
        try {
            file_chooser.setSelectedFile(
                new File(Alfi.WORKING_DIRECTORY.getCanonicalPath() + "/" +
                                                          _default_file_name));
        }
        catch (IOException ioe) {
            file_chooser.setSelectedFile(new File(_default_file_name));
        }

            // show file chooser dialog
        int result = file_chooser.showOpenDialog(this);

            // if a box file is entered, then create an ALFINode
        if (result == JFileChooser.APPROVE_OPTION) {
            File file = file_chooser.getSelectedFile();
                // check that the file name ends in ".box"
            if (! file.getName().endsWith(ALFIFile.BOX_NODE_FILE_SUFFIX)) {
                String message = "Box file names must end in \"" +
                                ALFIFile.BOX_NODE_FILE_SUFFIX + "\".";
                String title = "Invalid Box File Name";
                JOptionPane.showMessageDialog(this, message, title,
                                          JOptionPane.INFORMATION_MESSAGE);
                newBox(file.getName() + ALFIFile.BOX_NODE_FILE_SUFFIX);
                return;
            }

                // ask user what to do if file already exists
            if (file.exists()) {
                String message =
                    "This file already exists.  Do you wish to overwrite it?";
                String title = "File Exists";
                String[] options = { "Yes, Overwrite File",
                                     "No, Open Existing File", "Cancel" };
                int confirm = JOptionPane.showOptionDialog(this,
                            message, title, JOptionPane.YES_NO_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null, options, null);
                if (confirm == 0) {
                    try {
                        ALFINodeCache node_cache = Alfi.getTheNodeCache();
                        String canonical_path = file.getCanonicalPath();
                        if (node_cache.containsRootNode(canonical_path)) {
                            ALFINode node =
                                        node_cache.getRootNode(canonical_path);
                            unload(node, false); 
                        }
                    }
                    catch(IOException _e) { _e.printStackTrace(); }
                    file.delete();
                }
                else if (confirm == 1) {
                    show(file, null, true);
                    return;
                }
                else { return; }
            }

            ALFIFile alfi_file = null;
            try {
                String canonical_path = file.getCanonicalPath();
                alfi_file = new ALFIFile(canonical_path, true);
            }
            catch (Exception _e) { _e.printStackTrace(); }

            if (alfi_file != null) {
                ALFINode new_node = alfi_file.getRootNode();
                show(new_node, null, true);
            }
        }
        else { return; }
    }
 
    private EditorFrame displayNodeWindow (ALFINode _node) {
        EditorFrame editor = Alfi.getTheNodeCache().getEditSession(_node);
        boolean b_internal_view = true;
        if (editor != null) {
            b_internal_view = editor.isInternalView();
        }
        return displayNodeWindow(_node, null, b_internal_view);
    }

    private EditorFrame displayNodeWindow (ALFINode _node, Point _location) {
        return displayNodeWindow(_node, _location, true);
    }

    private EditorFrame displayNodeWindow (ALFINode _node,
                                     Point _location, boolean _internal_view) {
            // Check if the node already has an edit session open.
        EditorFrame editor = Alfi.getTheNodeCache().getEditSession(_node);
        Point old_location = null;
        if (editor != null) {
                // if the correct view is already up, just bring it to the fore
            if (editor.isInternalView() == _internal_view) {
                if (editor.getState() == Frame.ICONIFIED) {
                    editor.setState(Frame.NORMAL);
                }
                else if (! editor.isShowing()) {
                    editor.show();
                    m_menubar.getWindowMenu().addMenuItem(editor);
                }
                editor.toFront();
                editor.requestFocus();

                    // Kludge for fixing focus problem
                editor.setVisible(false);
                editor.setVisible(true);
                
                return editor;
            }
            else {
                    // save location, close the wrong view, and then proceed
                old_location = editor.getLocation();
                editor.close();
            }
        }

            // disable time stamp mods while openning editor
        final ALFINode node = _node;
        node.timeStamp_enableUpdates(false);
        final EditorFrame new_editor =
                        new EditorFrame(this, node, _internal_view);
        new_editor.hide();

        new_editor.initializeFrame();

        if ((_location == null) || (_location.x < 0) || (_location.y < 0)) {
            _location = old_location;
        }
        if (_location == null) { _location = new Point(50, 50); }

        if (Alfi.WINDOW_MANAGER.equals("fvwm")) {
            _location.translate(Alfi.WINDOW_MANAGER_OFFSET.x,
                                Alfi.WINDOW_MANAGER_OFFSET.y);
        }
        new_editor.setLocation(_location);

        new_editor.getView().getCanvas().setVisible(false);
        new_editor.show();

        java.util.Timer resize_enabler = new java.util.Timer();
        resize_enabler.schedule(
            new TimerTask() {
                public void run() {
                    if (new_editor.isShowing()) {
                        try { Thread.currentThread().sleep(500L); }
                        catch(InterruptedException _ie) { ; }

                        new_editor.getView().initialize();
                        new_editor.initializePreviousScreenPosition();
                        new_editor.updateView();
                        new_editor.getView().getDocument().
                                                      setSuspendUpdates(false);
                        new_editor.getView().getCanvas().setVisible(true);
                        node.timeStamp_enableUpdates(true);

                            // notify main thread below to restart
                        synchronized(new_editor) {
                            new_editor.notifyAll();
                        }

                        cancel();
                    }
                }
            }, 0L, 250L);

            // main thread waits for timer to complete above
        synchronized(new_editor) {
            try { new_editor.wait(); }
            catch(InterruptedException _ie) { ; }
        }

        m_menubar.getWindowMenu().addMenuItem(new_editor);

        return new_editor;
    }

        /* Called by user "Load" in file menu.  This actually loads (if needed)
         * and shows the edit window of a user selected (herein) file.
         */
    public void load() {
        JFileChooser file_chooser = new JFileChooser(Alfi.WORKING_DIRECTORY); 

            // accept only box files
        final ExtensionFileFilter filter = new ExtensionFileFilter(); 
        filter.addExtension(ALFIFile.BOX_NODE_FILE_SUFFIX);
        filter.setDescription(AlfiConstants.DEFAULT_FILE_DESCRIPTOR);
        file_chooser.setFileFilter(filter);

            // show file chooser dialog
        int result = file_chooser.showOpenDialog(this);

            // if a box file is entered, then create an ALFINode
        if (result == JFileChooser.APPROVE_OPTION) {
            File file = file_chooser.getSelectedFile();
            if (file.exists()) { show(file, null, true); }
            else {
                Alfi.warn("The node does not exist.  " +
                                      "Please make another selection.", false);
            }
        }
    }

        /* Convenience method to access load_and_show__engine.  Pass
         * _window_rect as null for default edit window.
         */
    public void show(File _node_file, Rectangle _window_rect,
                                                    boolean _b_internal_view) {
        if (_node_file != null) {
            File[] node_files = { _node_file };
            Rectangle[] window_rects = { _window_rect };
            boolean[] b_internal_views = { _b_internal_view };
            show(node_files, window_rects, b_internal_views);
        }
    }

        /** Convenience method to access load_and_show__engine.  Pass
          * _window_rects as null for default edit windows.  _b_internal_views
          * may be passed as null for all internal view editors.
          */
    public void show(File[] _node_files, Rectangle[] _window_rects,
                                                 boolean[] _b_internal_views) {
        if ((_node_files != null) && (_node_files.length > 0)) {
            if (_window_rects == null) {
                _window_rects = new Rectangle[_node_files.length];
            }
            if (_b_internal_views == null) {
                _b_internal_views = new boolean[_node_files.length];
                for (int i = 0; i < _b_internal_views.length; i++) {
                    _b_internal_views[i] = true;
                }
            }
            HashMap show_map = new HashMap();
            for (int i = 0; i < _node_files.length; i++) {
                Object[] info = { _window_rects[i],
                                  new Boolean(_b_internal_views[i]) };
                show_map.put(_node_files[i], info);
            }

            load_and_show__engine(_node_files, show_map);
        }
    }

        /* Convenience method to access load_and_show__engine.  Pass
         * _window_rect as null for default edit window.
         */
    public void show(ALFINode _node, Rectangle _window_rect,
                                                    boolean _b_internal_view) {
        if (_node != null) {
            ALFINode[] nodes = { _node };
            Rectangle[] window_rects = { _window_rect };
            boolean[] b_internal_views = { _b_internal_view };
            show(nodes, window_rects, b_internal_views);
        }
    }

        /** Convenience method to access load_and_show__engine.  Pass
          * _window_rects as null for default edit windows.  _b_internal_views
          * may be passed as null for all internal view editors.
          */
    public void show(ALFINode[] _nodes, Rectangle[] _window_rects,
                                                 boolean[] _b_internal_views) {
        if ((_nodes != null) && (_nodes.length > 0)) {
                // since all nodes already exist, nothing to load
            File[] load_files = new File[0];

            if (_window_rects == null) {
                _window_rects = new Rectangle[_nodes.length];
            }
            if (_b_internal_views == null) {
                _b_internal_views = new boolean[_nodes.length];
                for (int i = 0; i < _b_internal_views.length; i++) {
                    _b_internal_views[i] = true;
                }
            }
            HashMap show_map = new HashMap();
            for (int i = 0; i < _nodes.length; i++) {
                Object[] info = { _window_rects[i],
                                  new Boolean(_b_internal_views[i]) };
                show_map.put(_nodes[i], info);
            }

            load_and_show__engine(load_files, show_map);
        }
    }

        /** Loads nodes from file (if not already loaded).  _full_path_names
          * should contain the canonical file paths to the root nodes to be
          * loaded.  _show_map is used to display edit windows.  These
          * operations are batched together because they are very frequently
          * done together and, as they are time consuming, this method also
          * deals with the delay for both the user (as in showing a wait cursor)
          * and other operations that can be postponed to speed this operation
          * (as in proxy listener registration.)  _show_map keys may either be
          * ALFINodes or Files (associated with a root node) or Strings (full
          * node names), and the values are Object[2] == { Rectangle, Boolean }
          * where the rectangle indicates the edit window geometry requested,
          * and the boolean: true = internal view, false = external view.
          */
    public void load_and_show__engine(File[] _load_files, HashMap _show_map) {
        if (((_load_files == null) || (_load_files.length < 1)) &&
                             ((_show_map == null) || (_show_map.size() < 1))) {
            return;
        }

        final ProxyListenerRegistrationTool proxy_listener_registrar =
                                           Alfi.getTheProxyListenerRegistrar();
        final ALFINodeCache node_cache = Alfi.getTheNodeCache();
        final Cursor original_cursor = getCursor();
        final Cursor wait_cursor =
                                Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

        setCursor(wait_cursor);
        setAllowTreeExpansion(false);
        proxy_listener_registrar.interrupt(this);

        ArrayList canonical_path_list = new ArrayList();
        for (int i = 0; i < _load_files.length; i++) {
            if (_load_files[i].exists()) {
                try {
                    canonical_path_list.add(_load_files[i].getCanonicalPath());
                }
                catch(IOException e) { e.printStackTrace(); }
            }
        }
        String[] paths = new String[canonical_path_list.size()];
        canonical_path_list.toArray(paths);

        final ALFINode[] loaded_nodes = new ALFINode[paths.length];

        JDialog dialog = new JDialog(this, "Alfi: Loading Nodes", false);
        dialog.setBounds(200, 200, 600, 60);
        JLabel info_text = new JLabel("Loading...");
        Box box = Box.createHorizontalBox();
        box.add(Box.createHorizontalGlue());
        box.add(info_text);
        box.add(Box.createHorizontalGlue());
        dialog.getContentPane().add(box, BorderLayout.CENTER);
        dialog.show();

        for (int i = 0; i < paths.length; i++) {
            if (node_cache.containsRootNode(paths[i])) {
                loaded_nodes[i] = node_cache.getRootNode(paths[i]);

                info_text.setText("Node " + loaded_nodes[i] + " found.");
            }
            else {
                    // while is used here to repeat if E2E_PATH modified by user
                while (true) {
                    ALFIFile alfi_file = null;
                    try {
                        alfi_file = new ALFIFile(paths[i], true, info_text);
                    }
                    catch(AlfiIncludeFileNotFoundException e) {
                            // if this occurs, alfi_file will have been returned
                            // as null, and importantly, no associated ALFINode
                            // will have yet been created for it (Parser code
                            // shows this is true), so there is no chance that
                            // the ALFINodeCache has been "polluted" by a ghost
                            // node. This is important because we want to
                            // continue normally after giving the user a chance
                            // to change the E2E_PATH, which may solve the
                            // current problem.
                        int response = e.showMessage(this);
                        if (response == AlfiIncludeFileNotFoundException.
                                                             MODIFY_E2E_PATH) {
                            showPreferences();

                                // repeat the while loop with new E2E_PATH
                            continue;
                        }
                    }
                    catch(CyclicAlfiFileIncludeException e) {
                        String s1 = TextFormatter.formatMessage(e.getMessage());
                        String s2 = TextFormatter.formatMessage("File " +
                            paths[i] + " is part of and/or associated " +
                            "with this loop and cannot be loaded.");
                        Alfi.warn(s1 + "\n\n" + s2);
                    }

                    if (alfi_file != null) {
                        loaded_nodes[i] = alfi_file.getRootNode();
                    }

                    break;    // only continue above repeats while loop
                }
            }
        }
        dialog.dispose();

            // display nodes associated with objects in the _show_map
        ArrayList nodes_to_show_list = new ArrayList();
        for (Iterator i = _show_map.keySet().iterator(); i.hasNext(); ) {
            Object key = i.next();
            Object[] info = (Object[]) _show_map.get(key);
            Rectangle rect = (info != null) ? (Rectangle) info[0] : null;
            boolean b_internal_view = (info != null) ?
                                     ((Boolean) info[1]).booleanValue() : true;
            Point p = (rect == null) ? null : rect.getLocation();
            if (key instanceof File) {
                try {
                    String canonical_path = ((File) key).getCanonicalPath();
                    ALFINode node = node_cache.getRootNode(canonical_path);
                    if (node != null) {
                        nodes_to_show_list.add(node);
                        displayNodeWindow(node, p, b_internal_view);
                    }
                }
                catch(IOException e) { e.printStackTrace(); }
            }
            else if (key instanceof ALFINode) {
                ALFINode node = (ALFINode) key;
                nodes_to_show_list.add(node);
                displayNodeWindow(node, p, b_internal_view);
            }
            else if (key instanceof String) {    // a full node name (A.B.C)
                ALFINode node = node_cache.findNode((String) key);
                if (node != null) {
                    nodes_to_show_list.add(node);
                    displayNodeWindow(node, p, b_internal_view);
                }
            }
        }
        final ALFINode[] nodes_to_show= new ALFINode[nodes_to_show_list.size()];
        nodes_to_show_list.toArray(nodes_to_show);

            // resume updates and registration after all windows appear
        java.util.Timer timer = new java.util.Timer();
        timer.scheduleAtFixedRate(
            new TimerTask() {
                public void run() {
                    boolean b_all_editors_showing = true;
                    for (int i = 0; i < nodes_to_show.length; i++) {
                        if (nodes_to_show[i] == null) { continue; }

                        EditorFrame editor =
                                    node_cache.getEditSession(nodes_to_show[i]);
                        if ((editor == null) || (! editor.isShowing())) {
                            b_all_editors_showing = false;
                            break;
                        }
                    }
                    if (b_all_editors_showing) {
                       setAllowTreeExpansion(true);
                       getContainerTreeWidget().expandRow(0);
                       setCursor(original_cursor);
                       proxy_listener_registrar.resumeProcessingLoop(this);
                       updateBundlePortContent(loaded_nodes);
                       cancel();
                    }
                    else { setCursor(wait_cursor); }
                }
            }, 250, 250);
    }

    private void updateBundlePortContent(ALFINode[] _loaded_nodes) {
        BundlePortContentInitializer content_initializer =
                               new BundlePortContentInitializer(_loaded_nodes);
        content_initializer.start();
    }

        /** Unloads all nodes.  Returns { String[], HashMap } where the String[]
          * is an array of canonical paths to any user-loaded box files which
          * have been unloaded, and the HashMap is
          * { String => [Rectangle, Boolean] } pairs, which is information about
          * edit windows which were closed: String is the full name of a node,
          * Rectangle is the edit window bounds, and Boolean is true for
          * internal views and false for external.
          */
    public Object[] unloadAll() {
        ALFINodeCache cache = Alfi.getTheNodeCache();
        ALFINode[] root_boxes = cache.getRootBoxNodes_sorted();

        ArrayList nodes_to_reload_list = new ArrayList();
        for (int i = 0; i < root_boxes.length; i++) {
            if (root_boxes[i].getSourceFile().isUserLoadedBox()) {
                nodes_to_reload_list.add(root_boxes[i]);
            }
        }
        String[] files_to_reload = new String[nodes_to_reload_list.size()];
        for (int i = 0; i < files_to_reload.length; i++) {
            files_to_reload[i] = ((ALFINode) nodes_to_reload_list.get(i)).
                                            getSourceFile().getCanonicalPath();
        }

            // for the replacement of editor windows later
        HashMap editor_map = cache.getEditSessionInfo();

            // clear all loaded root boxes, order matters
        for (int i = root_boxes.length - 1; i >= 0; i--) {
            unload(root_boxes[i], false);
        }

            // unload the primitives
        Point toolbar_location = Alfi.clearPrimitivesToolbar();
        cache.clearPrimitives();

        Object[] info = { files_to_reload, editor_map, toolbar_location };
        return info;
    }

        /** Checks for a node selected in the container tree. */
    public void unload (boolean _b_inform_user) {
        unload(getNodeSelectedInTree(), _b_inform_user);
    }

        /** This method is used to remove a root base node from the system.
          * It forces any node which contains the node to
          * first be removed from the system.  The user has a chance to cancel
          * this operation after being informed which nodes will be unloaded.
          */
    public void unload (ALFINode _root_node, boolean _b_inform_user) {
        if ((_root_node != null) && _root_node.isRootNode()) {
            ALFINodeCache cache = Alfi.getTheNodeCache();

            ArrayList unload_list = unload__getContainers(_root_node);
            Collections.reverse(unload_list);

                // inform user about what will happen?
            if (_b_inform_user) {
                if (! unload__informUser(_root_node, unload_list)) { return; }
            }

            for (Iterator i = unload_list.iterator(); i.hasNext(); ) {
                ALFINode container = (ALFINode) i.next();

                    // shut down edit sessions
                EditorFrame[] editors = cache.getEditSessions();
                for (int j = 0; j < editors.length; j++) {
                    ALFINode node_in_edit = editors[j].getNodeInEdit();
                    if ((node_in_edit.containerNode_getRoot() == container) ||
                                                 (node_in_edit == container)) {
                        editors[j].close();
                    }
                }
                    // clear all contents of the node
                container.clear__except_listeners();

                    // remove the node from the cache
                cache.removeNode(container);
            }
        }
    }

        /** unload() utility method.  Informs user what the consequences of
          * the unload operation will be and asks if unload should proceed.
          */
    private boolean unload__informUser(ALFINode _original_node_to_unload,
                                                      ArrayList _unload_list) {
        String message = "In order to unload " + _original_node_to_unload +
            ", all of the following nodes must be unloaded.  Edit windows " +
            "for any of these nodes and/or their contents will be closed.  " +
            "All unsaved changes to any of these nodes will be lost.";
        message = TextFormatter.formatMessage(message);
        message += "\n\nUnload these nodes?\n\n";
        int count = 0;
        for (Iterator i = _unload_list.iterator(); i.hasNext(); count++) {
            message += "\t" + i.next() + "\n";
            if (count >= 10) {
                message += "\t.\n\t.\n\t.";
                break;
            }
        }
        String title = "Node Unload Dialog";
        int response = JOptionPane.showConfirmDialog(this, message,
                title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        return (response == JOptionPane.YES_OPTION);
    }

        /** unload() utility method.  Any node which contains a node derived
          * from a node to be unloaded must itself be unloaded first.
          */
    private ArrayList unload__getContainers(ALFINode _root_container) {
        ArrayList container_list = new ArrayList();

        if (! container_list.contains(_root_container)) {
            container_list.add(_root_container);
        }

        ALFINode[] derived_nodes = _root_container.derivedNodes_getDirect();
        for (int i = 0; i < derived_nodes.length; i++) {
            ALFINode root_container = derived_nodes[i].containerNode_getRoot();
            ArrayList next_gen_container_list =
                                         unload__getContainers(root_container);

            for (Iterator j = next_gen_container_list.iterator(); j.hasNext();){
                ALFINode container = (ALFINode) j.next();
                if (! container_list.contains(container)) {
                    container_list.add(container);
                }
            }
        }

        return container_list;
    }

        /** Checks for a node selected in the container tree. */
    public void reload(boolean _b_inform_user) {
        reload(getNodeSelectedInTree(), _b_inform_user);
    }

        /** Reloads a base node from disk. */
    public void reload(ALFINode _root_node, boolean _b_inform_user) {
        if ((_root_node != null) && _root_node.isRootNode()) {
            ALFINodeCache cache = Alfi.getTheNodeCache();

            ArrayList reload_list = unload__getContainers(_root_node);
            Collections.reverse(reload_list);

                // inform user about what will happen?
            if (_b_inform_user) {
                if (! reload__informUser(_root_node, reload_list)) { return; }
            }

            ArrayList reload_path_list = new ArrayList();
            HashMap reload_editor_map = new HashMap();
            for (Iterator i = reload_list.iterator(); i.hasNext(); ) {
                ALFINode container = (ALFINode) i.next();
                reload_path_list.add(
                                 container.getSourceFile().getCanonicalPath());

                EditorFrame[] editors = cache.getEditSessions();
                for (int j = 0; j < editors.length; j++) {
                    ALFINode node_in_edit = editors[j].getNodeInEdit();
                    if ((node_in_edit.containerNode_getRoot() == container) ||
                                                 (node_in_edit == container)) {
                        String name = node_in_edit.generateFullNodePathName();
                        Point location = editors[j].getLocation();
                        reload_editor_map.put(name, location);
                    }
                }
            }

            boolean b_user_loaded= _root_node.getSourceFile().isUserLoadedBox();

            unload(_root_node, false);

            Collections.reverse(reload_path_list);
            for (Iterator i = reload_path_list.iterator(); i.hasNext(); ) {
                String path = (String) i.next();
                try { new ALFIFile(path, b_user_loaded); }
                catch(AlfiIncludeFileNotFoundException _e) { ; }
                catch(CyclicAlfiFileIncludeException _e) { ; }
            }

            for (Iterator i = reload_editor_map.keySet().iterator();
                                                               i.hasNext(); ) {
                String node_name = (String) i.next();
                Point editor_location = (Point)reload_editor_map.get(node_name);

                ALFINode node = cache.findNode(node_name);

                if (node != null) {
                    displayNodeWindow(node, editor_location);
                }
            }
        }
    }

        /** reload() utility method.  Informs user what the consequences of
          * the reload operation will be and asks if reload should proceed.
          */
    private boolean reload__informUser(ALFINode _original_node_to_reload,
                                                      ArrayList _reload_list) {
        String message = "In order to reload " + _original_node_to_reload +
            ", all of the following nodes must be reloaded.  Edit windows " +
            "for any of these nodes and/or their contents will be closed and " +
            "then re-opened.  All unsaved changes to any of these nodes will " +
            "be lost.";
        message = TextFormatter.formatMessage(message);
        message += "\n\nReload these nodes from disk?\n\n";
        int count = 0;
        for (Iterator i = _reload_list.iterator(); i.hasNext(); count++) {
            message += "\t" + i.next() + "\n";
            if (count >= 10) {
                message += "\t.\n\t.\n\t.";
                break;
            }
        }
        String title = "Node Reload Dialog";
        int response = JOptionPane.showConfirmDialog(this, message,
                title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        return (response == JOptionPane.YES_OPTION);
    }

    public void showPreferences() {
        PreferencesDialog dialog = new PreferencesDialog(this);
        dialog.setLocation(200, 200);
        dialog.show();
    }

    public void save() { save(false); }

    public void forceSaveAll() { save(true); }

    private void save(boolean _b_force_save) {
/*
        if (! validateNodes(null, null, true)) {
            JOptionPane.showMessageDialog(this, "Save All / Quit cancelled.");
            return;
        }
*/

        ALFINode[] root_nodes =
                             Alfi.getTheNodeCache().getRootBoxNodes_unsorted();
        ALFINode clipboard_node = EditorFrame.getClipboard().getClipboardNode();
        for (int i = 0; i < root_nodes.length; i++) {

            if (root_nodes[i] == clipboard_node) { continue; }

            ALFIFile file = root_nodes[i].getSourceFile();
            boolean b_save_file;
            if (_b_force_save) { b_save_file = true; }
            else {
                Date file_last_modified_time = file.getTimeLastModified();
                if (root_nodes[i].hasBeenModifiedSince_includingContainedNodes(
                                                     file_last_modified_time)) {
                    b_save_file = true;
                }
                else { b_save_file = false; }
            }
            if (b_save_file) {
                if (file.createAssociatedJavaFile().canWrite()) {
                    file.write(true);
                }
                else {
                    System.err.println("Cannot write " +
                        file.getCanonicalPath() + ".  File is read-only.");
                }
            }
        }
    }

        /** This method looks at all the loaded box files and checks to see if
          * any of them are old and in need of being rewritten to avoid possible
          * parsing problems later.
          */
    private void checkForDeprecatedFileVersions(ALFINode[] _root_nodes) {
        String[] current_version_parts = Alfi.ms_alfi_version.split("\\.");
        int[] current_version_numbers = new int[current_version_parts.length];
        for (int i = 0; i < current_version_numbers.length; i++) {
            try {
                current_version_numbers[i] =
                                    Integer.parseInt(current_version_parts[i]);
            }
            catch(NumberFormatException _e) { ; }
        }

        ALFINode clipboard_node = EditorFrame.getClipboard().getClipboardNode();
        int response = -1;
        for (int i = 0; i < _root_nodes.length; i++) {
            ALFINode node = _root_nodes[i];
            if (node != clipboard_node) {
                ALFIFile file = node.getSourceFile();
                int[] file_version = file.getParseVersion();
                int primary_dif = current_version_numbers[0] - file_version[0];
                int secondary_dif= current_version_numbers[1] - file_version[1];
                int tertiary_dif = current_version_numbers[2] - file_version[2];

                String warning = null;
                if ((primary_dif > 0) || (secondary_dif > 3)) {
                    warning = "Box file " + file.getCanonicalPath() + " is\n" +
                        "very old.  It is recommended you resave it from\n" +
                        "the original Aramaic.";
                }
                else if (secondary_dif > 1) {
                    warning = "Box file " + file.getCanonicalPath() + " is\n" +
                        "getting old.  You might want to resave it now to\n" +
                        "prevent possible parsing problems in the future.";
                }   

                if (warning != null) {
                        // if "Resave All Remaining" hasn't already been chosen
                    if (response != 2) {
                        warning += "\n\nDo you wish to resave it at this time?";
                        String title = "Resave Old File Dialog";
                        String[] options = { "Yes", "No",
                              "Resave All Remaining", "Ignore All Remaining" };

                        response = JOptionPane.showOptionDialog(this,
                            warning, title, JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null, options, null);
                    }

                    switch (response) {
                      case 0:
                      case 2:
                        file.write(true);
                        break;
                      case 1:
                        break;
                      case 3:
                        return;
                    }
                }   
            }
        }
    }

    public void quit () {
        mb_abort_quit = false;

        quit__writePreferenceFiles();

            // check if anything needs to be saved
        ALFINode[] root_nodes =
                             Alfi.getTheNodeCache().getRootBoxNodes_unsorted();

        checkForDeprecatedFileVersions(root_nodes);

        ALFINode clipboard_node = EditorFrame.getClipboard().getClipboardNode();
        HashMap ignored_changes_map = new HashMap();
        boolean b_ignore_remaining_changes = false;
        for (int i = 0; i < root_nodes.length; i++) {
            ALFINode node = root_nodes[i];

                // ignore saving clipboard node here
            if (node == clipboard_node) { continue; }

            ALFIFile file = node.getSourceFile();
            Date file_last_modified_time = file.getTimeLastModified();
            if (node.hasBeenModifiedSince_includingContainedNodes(
                                                    file_last_modified_time)) {
                String node_name = node.determineLocalName();
                String title = "Unsaved Changes Dialog";
                String message = "Unsaved changes exist in node " + node_name +
                    ".  Do you wish to\n\n" +
                    "  1. Save changes to " + node_name + "?\n" +
                    "  2. Ignore changes to " + node_name + "?\n" +
                    "  3. Save changes to all remaining nodes ?\n" +
                    "  4. Ignore changes to all remaining nodes ?\n" +
                    "  5. Cancel ?  (I.e., do not quit.)";
                String[] options = {
                        "1. Save " + node_name, "2. Ignore " + node_name,
                        "3. Save All", "4. Ignore All", "5. Cancel" };
                String choice = options[0];
                int response = JOptionPane.showOptionDialog(this, message,
                        title, JOptionPane.DEFAULT_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, choice);
                switch(response) {
                  case 0:
                        // save this file only
//                  if (validateNode(node, null, true)) {
                        file.write(true);
                        break;
/*
                    }
                    else {
                        Alfi.warn("Save " + node + " and Quit cancelled.");
                        return;
                    }
*/
                  case 1:
                        // save current time stamp in case of a cancel
                    Date time_stamp = node.getTimeLastModified();
                    ignored_changes_map.put(node, time_stamp);
                        // set time stamp to file's time stamp to ignore
                    node.setTimeLastModified(file.getTimeLastModified());
                    break;
                  case 2:
                        // save changes to all remaining files and then quit
                    save();
                    quit();
                    return;
                  case 3:
                    b_ignore_remaining_changes = true;
                    break;
                  case 4:
                  default:
                        // reset any time stamps which were ignored
                    Iterator j = ignored_changes_map.keySet().iterator();
                    while (j.hasNext()) {
                        ALFINode node_to_reset = (ALFINode) j.next();
                        Date stamp=(Date)ignored_changes_map.get(node_to_reset);
                        node_to_reset.setTimeLastModified(stamp);
                    }
                        // cancel the quit
                    return;
                }
            }

            if (b_ignore_remaining_changes) { break; }
        }

        if (! mb_abort_quit) {
                // either save to or remove the clipboard file
            if (isPersistentClipboard()) {
                clipboard_node.getSourceFile().write(false);
            }
            else { Alfi.CLIPBOARD_FILE.delete(); }

                // determines whether to keep the log file
            Alfi.getMessenger().cleanup();

            closeAllEditors();

                // All inner windows closed successfully.  
                // Attempt to close this (the outer) frame.  
            try {
                super.close();
                System.exit(0);
            }
            catch(Exception ex) { }
        }
        else { mb_abort_quit = false; }    // unneeded, but for completeness
    }

        /** Allows quits to be aborted if requested at some point in the quit
          * procedure.
          */
    public void abortQuit() { mb_abort_quit = true; }

    private void closeAllEditors() {
        EditorFrame[] editors = Alfi.getTheNodeCache().getEditSessions();
        for (int i = 0; i < editors.length; i++) {
            editors[i].close();
        }
    }

    private void quit__writePreferenceFiles() {
            // standard preferences/options
        try {
            FileWriter fw = new FileWriter(Alfi.UNIVERSAL_PREFERENCES_FILE);
            BufferedWriter writer = new BufferedWriter(fw);
            if (writer != null) {
                writer.write("UNIVERSAL_PREFERENCES_WRITTEN_BY " +
                                                         Alfi.ms_alfi_version);
                writer.newLine();

                String e2e_path = new String(Alfi.getE2EPath());
                if (! Alfi.SAVE_WORKING_DIRECTORY_IN_E2E_PATH) {
                    String cwd = Alfi.WORKING_DIRECTORY.getPath();
                    if (e2e_path.startsWith(cwd)) {
                        int length = cwd.length();
                        e2e_path = e2e_path.substring(length + 1);
                    }
                }
                writer.write("E2E_PATH " + e2e_path);
                writer.newLine();
                writer.write("ALFI__WINDOW_MANAGER " + Alfi.WINDOW_MANAGER);
                writer.newLine();
                writer.write("user.dir " + Alfi.WORKING_DIRECTORY);
                writer.newLine();
                writer.write("default.browser " + Alfi.DEFAULT_BROWSER);
                writer.newLine();

                writer.write(ActionsIF.TIP + " " +
                                          getCheckBoxItemState(ActionsIF.TIP));
                writer.newLine();
                writer.write(ActionsIF.TIP_INHERITED + " " +
                                getCheckBoxItemState(ActionsIF.TIP_INHERITED));
                writer.newLine();
                writer.write(ActionsIF.MACRO_INFO_IN_TREE + " " +
                           getCheckBoxItemState(ActionsIF.MACRO_INFO_IN_TREE));
                writer.newLine();
                writer.write(ActionsIF.MACRO_INFO_ON_NODE + " " +
                           getCheckBoxItemState(ActionsIF.MACRO_INFO_ON_NODE));
                writer.newLine();
                writer.write(ActionsIF.AUTO_SAVE + " " +
                                getCheckBoxItemState(ActionsIF.AUTO_SAVE));
                writer.newLine();
                writer.write(ActionsIF.CONN_ON_TOP + " " +
                                  getCheckBoxItemState(ActionsIF.CONN_ON_TOP));
                writer.newLine();
                writer.write(ActionsIF.RESTORE_WINDOWS + " " +
                              getCheckBoxItemState(ActionsIF.RESTORE_WINDOWS));
                writer.newLine();
                writer.write(ActionsIF.TRIM_CONNECTIONS + " " +
                             getCheckBoxItemState(ActionsIF.TRIM_CONNECTIONS));
                writer.newLine();
                writer.write(ActionsIF.PERSISTENT_CLIPBOARD + " " +
                         getCheckBoxItemState(ActionsIF.PERSISTENT_CLIPBOARD));
                writer.newLine();
                writer.write(ActionsIF.AUTO_VALIDATE_NODES + " " +
                         getCheckBoxItemState(ActionsIF.AUTO_VALIDATE_NODES));
                writer.newLine();

                writer.flush();
                writer.close();
            }
        }
        catch(IOException _e) {
            Alfi.warn("AlfiMainFrame.quit__writePreferenceFiles():\n" + _e);
        }

            // preferences local to the working directory
        try {
            FileWriter fw = new FileWriter(Alfi.LOCAL_PREFERENCES_FILE);
            BufferedWriter writer = new BufferedWriter(fw);
            if (writer != null) {
                writer.write("LOCAL_PREFERENCES_WRITTEN_BY " +
                                                         Alfi.ms_alfi_version);
                writer.newLine();

                    // window information
                HashMap boxes_to_load = new HashMap();
                HashMap edit_windows_to_show = new HashMap();
                if (getCheckBoxItemState(ActionsIF.RESTORE_WINDOWS)) {
                    EditorFrame[] editors =
                                      Alfi.getTheNodeCache().getEditSessions();
                    for (int i = 0; i < editors.length; i++) {
                        if (editors[i].isShowing()) {
                            ALFINode node = editors[i].getNodeInEdit();

                            ALFINode root_container = (node.isRootNode()) ?
                                           node : node.containerNode_getRoot();
                            ALFIFile file = root_container.getSourceFile();
                            boxes_to_load.put(file.getCanonicalPath(), null);

                            String name = node.generateFullNodePathName();
                            Point p = editors[i].getLocationOnScreen();
                            Dimension dim = editors[i].getSize();
                            Rectangle rect = new Rectangle(p, dim);
                            edit_windows_to_show.put(name, rect);
                        }
                    }

                        // write root containers to load
                    for (Iterator i = boxes_to_load.keySet().iterator();
                                                               i.hasNext(); ) {
                        String box_path = (String) i.next();
                        writer.write(box_path);
                        writer.newLine();
                    }

                        // write node edit windows to show
                    for (Iterator i = edit_windows_to_show.keySet().iterator();
                                                               i.hasNext(); ) {
                        String node_name = (String) i.next();
                        Rectangle rect =
                               (Rectangle) edit_windows_to_show.get(node_name);
                        writer.write(node_name + " " + rect.x + "x" + rect.y +
                                         "x" + rect.width + "x" + rect.height);
                        writer.newLine();
                    }

                        // placement of main window
                    {
                        String key = "MAIN_WINDOW_RECT";
                        Point p = getLocationOnScreen();
                        Dimension dim = getSize();
                        writer.write(key + " " + p.x + "x" + p.y +
                                           "x" + dim.width + "x" + dim.height);
                        writer.newLine();
                    }

                        // placement of primitive toolbar
                    {
                        ToolbarFrame toolbar = Alfi.getPrimitivesToolbarFrame();
                        String key = "PRIMITIVE_TOOLBAR_RECT";
                        if ((toolbar != null) && toolbar.isShowing()) {
                            Point p = toolbar.getLocationOnScreen();
                            Dimension dim = toolbar.getSize();
                            writer.write(key + " " + p.x + "x" + p.y +
                                           "x" + dim.width + "x" + dim.height);
                            writer.newLine();

                            writer.write("SHOW_PRIMITIVE_TOOLBAR true");
                            writer.newLine();
                        }
                        else {
                            String previous_setting =
                                            (String) Alfi.PREFERENCES.get(key);
                            if (previous_setting != null) {
                                writer.write(key + " " + previous_setting);
                            }
                        }
                    }
                }
                writer.flush();
                writer.close();
            }
        }
        catch(IOException _e) {
            Alfi.warn("AlfiMainFrame.quit__writePreferenceFiles():\n" + _e);
        }
    }

        /** Resets options to settings that were used in the previous session.*/
    private void setOptionsFromPreviousSession() {
        String[] names = {
                   ActionsIF.TIP, ActionsIF.TIP_INHERITED,
                   ActionsIF.MACRO_INFO_IN_TREE, ActionsIF.MACRO_INFO_ON_NODE,
                   ActionsIF.AUTO_SAVE, ActionsIF.CONN_ON_TOP,
                   ActionsIF.RESTORE_WINDOWS, ActionsIF.TRIM_CONNECTIONS,
                   ActionsIF.PERSISTENT_CLIPBOARD, ActionsIF.AUTO_VALIDATE_NODES
                         };
        for (int i = 0; i < names.length; i++) {
            String setting = (String) Alfi.PREFERENCES.get(names[i]);
            if (setting != null) {
                if (setting.equals("true")) {
                    setCheckBoxItemState(names[i], true);
                }
                else if (setting.equals("false")) {
                    setCheckBoxItemState(names[i], false);
                }
            }
        }
    }

    public void showClipboard () {
        ALFINode clipboard_node = EditorFrame.getClipboard().getClipboardNode();
        if (clipboard_node != null) {
            EditorFrame editor = displayNodeWindow(clipboard_node);
            editor.resetView();
        }
    }

    public void find ( ) { m_find_box_file.setVisible(true); }

    public void showPrimitiveToolbar (boolean _show) {
        final ToolbarFrame toolbar = Alfi.getPrimitivesToolbarFrame();
        toolbar.setVisible(false);

        if (toolbar.getState() == Frame.ICONIFIED) {
            toolbar.setState(Frame.NORMAL);
        }

        String last_position =
                       (String) Alfi.PREFERENCES.get("PRIMITIVE_TOOLBAR_RECT");
        Rectangle window_bounds = null;
        if (last_position != null) {
            String[] parts = last_position.split("x");
            if (parts.length == 4) {
                int[] specs = { -1, -1, -1, -1 };
                for (int i = 0; i < 4; i++) {
                    try { specs[i] = Integer.parseInt(parts[i]); }
                    catch(NumberFormatException _nfe) {
                        specs = null;
                        break;
                    }
                }

                if (specs != null) {
                    int x = specs[0] + Alfi.WINDOW_MANAGER_OFFSET.x;
                    int y = specs[1] + Alfi.WINDOW_MANAGER_OFFSET.y;
                    int w = specs[2];
                    int h = specs[3];
                    window_bounds = new Rectangle(x, y, w, h);
                }
            }
        }

        if (window_bounds != null) {
            toolbar.setBounds(window_bounds);
        }
        else {
            Dimension toolbar_size = toolbar.getSize();
            Point main_loc = getLocation();
            final Point loc =
                       new Point(main_loc.x, main_loc.y - toolbar_size.height);
            toolbar.setLocation(loc);


                // kluge for fvwm
            if (Alfi.WINDOW_MANAGER.equals("fvwm")) {
                loc.translate(Alfi.WINDOW_MANAGER_OFFSET.x,
                                                 Alfi.WINDOW_MANAGER_OFFSET.y);

                java.util.Timer timer = new java.util.Timer();
                timer.scheduleAtFixedRate(
                    new TimerTask() {
                        public void run() {
                            if (toolbar.isShowing()) {
                                toolbar.setLocation(loc);
                                cancel();
                            }
                        }
                    }, 250, 250);
            }
        }

        if (_show) {
            toolbar.setVisible(true);
            toolbar.toFront();
            toolbar.requestFocus();
        } 

        BasicMenu view_menu = m_menubar.getViewMenu();
        Component[] menu_components = view_menu.getMenuComponents();
        for (int i = 0; i < menu_components.length; i++) {
            if (menu_components[i] instanceof JCheckBoxMenuItem) {
                JCheckBoxMenuItem item = (JCheckBoxMenuItem) menu_components[i];
                if (item.getActionCommand().equals(ActionsIF.PRIMITIVE_TB)) {
                    item.setState(_show);
                    break;
                }
            }
        }
    }

    public void showOverviewWindow () {
        OverviewDialog overview_dialog = new OverviewDialog(this);
        overview_dialog.show();

        ALFINode node_to_display = overview_dialog.getNodeToDisplay();
        overview_dialog.dispose();

        if (node_to_display != null) {
            displayNodeWindow(node_to_display);
        }

        updateTipDisplayStatus ();
    }

    private ALFINode getNodeSelectedInTree() {
        TreePath path = m_container_tree_widget.getSelectionPath();
        ALFINode selected_node = null;
        if (path != null) {
            DefaultMutableTreeNode bullet =
                          (DefaultMutableTreeNode) path.getLastPathComponent();
            Object associated_object = bullet.getUserObject();
            selected_node = (associated_object instanceof ALFINode) ?
                                           (ALFINode) associated_object : null;
        }
        return selected_node;
    }

    public boolean getCheckBoxItemState(String _action) {
        BasicMenu options_menu = m_menubar.getOptionsMenu();
        Component[] menu_components = options_menu.getMenuComponents();
        for (int i = 0; i < menu_components.length; i++) {
            if (menu_components[i] instanceof JCheckBoxMenuItem) {
                JCheckBoxMenuItem item = (JCheckBoxMenuItem) menu_components[i];
                if (item.getActionCommand().equals(_action)) {
                    return item.getState();
                }
            }
        }

            // error
        Alfi.warn("AlfiMainFrame.getCheckBoxItemState(" + _action + "):\n" +
            "\tAction not recognized.");
        return false;
    }

    public void setCheckBoxItemState(String _action, boolean _state) {
        BasicMenu options_menu = m_menubar.getOptionsMenu();
        Component[] menu_components = options_menu.getMenuComponents();
        for (int i = 0; i < menu_components.length; i++) {
            if (menu_components[i] instanceof JCheckBoxMenuItem) {
                JCheckBoxMenuItem item = (JCheckBoxMenuItem) menu_components[i];
                if (item.getActionCommand().equals(_action)) {
                    item.setState(_state);
                    return;
                }
            }
        }

            // error
        Alfi.warn("AlfiMainFrame.setCheckBoxItemState(" + _action + "...):\n" +
            "\tAction not recognized.");
    }

    public void updateTipDisplayStatus () {
        ToolTipManager.sharedInstance().
                               setEnabled(getCheckBoxItemState(ActionsIF.TIP));

        int delay = (getCheckBoxItemState(ActionsIF.TIP_INHERITED)) ?
                                                                  10000 : 5000;
        ToolTipManager.sharedInstance().setDismissDelay(delay);
    }

    public void updateNodeMacroInfoDisplayStatus() {
        EditorFrame[] editors = Alfi.getTheNodeCache().getEditSessions();
        for (int i = 0; i < editors.length; i++) {
            MemberNodeWidget[] mnws = editors[i].getMemberNodeWidgets();
            for (int j = 0; j < mnws.length; j++) {
                mnws[j].refresh();
            }
        }
    }

    public void updateTreeMacroInfoDisplayStatus() {
        int rows_showing = m_container_tree_widget.getRowCount();
        for (int i = 0; i <  rows_showing; i++) {
            TreePath path = m_container_tree_widget.getPathForRow(i);
            DefaultMutableTreeNode bullet =
                          (DefaultMutableTreeNode) path.getLastPathComponent();
            DefaultTreeModel model =
                         (DefaultTreeModel) m_container_tree_widget.getModel();
            model.nodeChanged(bullet);
        }
    }

    public boolean addInheritedTips() {
        return getCheckBoxItemState(ActionsIF.TIP_INHERITED);
    }

    public void autoSmooth (boolean isAutoSmooth) { }
    public void transparentBitmaps (boolean isTransparent) { }
    public void transparentIcons (boolean isIcons) { }
    public void saveAlways (boolean flag) { }
    public void autoSave (boolean flag) { }

        /** Tells all open edit sessions to draw connections on top. */
    public void drawConnectionsOnTop (boolean _b_put_connections_on_top) {
        EditorFrame[] sessions = Alfi.getTheNodeCache().getEditSessions();
        for (int i = 0; i < sessions.length; i++) {
            sessions[i].setConnectionsOnTop(_b_put_connections_on_top);
        }
    }

    public void restoreWindows (boolean flag) { }

    public boolean isAutoTrimConnections() {
        return getCheckBoxItemState(ActionsIF.TRIM_CONNECTIONS);
    }

    public void setAutoTrimConnections (boolean flag) {
        setCheckBoxItemState(ActionsIF.TRIM_CONNECTIONS, flag);
    }

    public boolean isPersistentClipboard() {
        return getCheckBoxItemState(ActionsIF.PERSISTENT_CLIPBOARD);
    }

    public void setPersistentClipboard (boolean flag) {
        setCheckBoxItemState(ActionsIF.PERSISTENT_CLIPBOARD, flag);
    }

    public boolean isAutoValidateNodes() {
        return getCheckBoxItemState(ActionsIF.AUTO_VALIDATE_NODES);
    }

    public void setAutoValidateNodes (boolean flag) {
        setCheckBoxItemState(ActionsIF.AUTO_VALIDATE_NODES, flag);
    }

    public void debugALFI (boolean flag) { }
    public void debugToStdErr (boolean flag) { }

        /** Convenience method.  See validateNodes(null, null, false) .*/
    public boolean validateNodes() {
        return validateNodes(null, null, false);
    }

        /** Convenience method.  See
          * validateNodes( { _node }, _flags, _b_vetoable).
          */
    public boolean validateNode(ALFINode _node, boolean[] _flags,
                                                         boolean _b_vetoable) {
        ALFINode[] nodes = { _node };
        return validateNodes(nodes, _flags, _b_vetoable);
    }

        /** This runs a check on all the nodes in the _nodes[], or, if _nodes
          * is null, on all nodes currently loaded in the system (though the
          * clipboard node is ignored.)  _flags is an array of booleans which
          * tell what to run validations on.  So far:
          *
          * [0] = true -> validate node connections
          * [1] = true -> validate misc
          *
          * If _flags == null, do all forms of validation.
          *
          * _b_vetoable should be set to true if you wish the user to veto
          * something like a quit process.  (Check the exit status for a VETO
          * response.)  If _b_vetoable, false will be returned if the user
          * doesn't choose to continue after the validation.  Otherwise, true
          * is always returned.
          */
    public boolean validateNodes(ALFINode[] _nodes, boolean[] _flags,
                                                         boolean _b_vetoable) {
System.out.println("AlfiMainFrame.validateNodes(...): TBD");

/*
        ALFINode[] nodes = (_nodes != null) ? _nodes :
                             Alfi.getTheNodeCache().getRootBoxNodes_unsorted();

        boolean b_validate_connections = ((_flags == null) || _flags[0]);
        Hashtable unresolved_channels_tree = new Hashtable();

        boolean b_validate_misc = ((_flags == null) || _flags[1]);

        
        final ALFINode CLIPBOARD_NODE = (EditorFrame.getClipboard() != null) ?
                          EditorFrame.getClipboard().getClipboardNode() : null;
        for (int i = 0; i < nodes.length; i++) {
            if ((CLIPBOARD_NODE != null) && (nodes[i] != CLIPBOARD_NODE)) {
                Object[] validation_objects =
                    nodes[i].getNodeValidationObjects(b_validate_connections,
                                                      b_validate_misc);
                if (b_validate_connections && (validation_objects[0] != null)) {
                    ALFINode root = nodes[i].isRootNode() ?
                                   nodes[i] : nodes[i].containerNode_getRoot();
                    if (validation_objects[0] != null) {
                        unresolved_channels_tree.put(root,
                                            (Hashtable) validation_objects[0]);
                    }
                }
            }
        }

            // to be used when more than one report will be presented to user
        int report_number = 1;
        int report_count = 0;
        if (unresolved_channels_tree.size() > 0) { report_count++; }

        if (unresolved_channels_tree.size() > 0) {
            UnresolvableChannelsDialog dialog = UnresolvableChannelsDialog.
                     executeDialog(this, unresolved_channels_tree,
                                   report_number++, report_count, _b_vetoable);
            if (dialog.getExitStatus() == dialog.VETO) {
                return false;
            }
        }
*/

        return true;
    }

    public void helpAbout() {
        String title = "About Alfi";
        String message = "Alfi Version " + Alfi.ms_alfi_version +"\n\n" +
            "Developed in Java by:\n" +
            "  Bruce Sears (bsears@ligo.caltech.edu)\n" +
            "  Melody Araya (maraya@ligo.caltech.edu)\n\n" +
            "Based on Alfi Version 4, Developed in C++ by:\n" +
            "  Ed Maros (emaros@ligo.caltech.edu)\n\n" +
            "LIGO Laboratory\n" +
            "California Institute of Technology";
        JOptionPane.showMessageDialog(this, message, title,
                                      JOptionPane.INFORMATION_MESSAGE);
    }

    public void helpUpdates(String _version_last_used) {
        String title = "Latest Alfi Updates";
        String message = "Alfi Updates:\n\n";
        message += Alfi.getUpdateInfo(_version_last_used);
        int rows = message.split("\n").length;

        JTextArea text_area = new JTextArea(message, rows, 80);
        text_area.setLineWrap(false);
        text_area.setEditable(false);

        JScrollPane scroll_pane = new JScrollPane(text_area,
                                   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        final JDialog dialog = new JDialog(this, title, false);

        JButton ok_button = new JButton("Okay");
        ok_button.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    dialog.dispose();
                }
            });

        Box box = Box.createHorizontalBox();
        box.add(Box.createHorizontalGlue());
        box.add(ok_button);
        box.add(Box.createHorizontalGlue());
        dialog.getContentPane().add(scroll_pane, BorderLayout.CENTER);
        dialog.getContentPane().add(box, BorderLayout.SOUTH);
        dialog.setBounds(200, 200, 600, 400);

        dialog.show();
    }

    public void helpDocumentation () {

        if (checkBrowserPreference()) { 
            String docURL = "http://www.ligo.caltech.edu/~e2e/alfi5/docs/index.html";
            BrowserControl.displayURL(docURL);
        }
    }

    public void helpReportBugs () {
        if (checkBrowserPreference()) { 
            String docURL = "https://ldas-sw.ligo.caltech.edu/cgi-bin/gnatsweb.cgi";
            BrowserControl.displayURL(docURL);
        }
    }

    private boolean checkBrowserPreference () {

        while (true) {

            if (Alfi.DEFAULT_BROWSER != null) {
                return true;                
            }

            String message = "Your default web browser has not been set.\n"+
                "Do you want to specify the browser for Alfi to use?";
 
            String title = "Set Web Browser";
            String[] options = {"Yes", "No"};
        
            int response = JOptionPane.showOptionDialog(this,
                message, title, JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, null);

            switch (response) {
                case 0:
                    showPreferences();
                    break;
                case 1:
                    return false;
            }
        }
        
    }

/*
    public void close () {
        m_window_mgr.closeActiveWindow();
    }

    public void cascade () {
        m_window_mgr.cascade ();
    }

    public void tile () {
        m_window_mgr.tile();
    }

    public void layer () {
        m_window_mgr.layer();
    }
*/

    /**
     * returns the default status text for this application.
     * @return the default status text for the status bar.
     */
    public String getDefaultStatusBarText () {
       return (new String (""));
    }

        /** Returns the state of "Options -> Connections on Top" menu option. */
    public boolean getConnectionsOnTopState() {
        return getCheckBoxItemState(ActionsIF.CONN_ON_TOP);
    }

    /**
      * This method creates the JTrees to display the data trees in
      * the node cache.  It also creates listeners for auxilliary
      * mouse events on the JTrees.
      */
    private void addTrees() {
        DefaultTreeModel tree = Alfi.getTheNodeCache().getContainerTree();
        m_container_tree_widget = new JTree(tree);
        Font font = new Font("monospaced", Font.PLAIN,
                m_container_tree_widget.getFont().getSize());
        m_container_tree_widget.setFont(font);
        m_container_tree_widget.setBackground(Color.white);
        m_container_tree_widget.setRootVisible(true);
        m_container_tree_widget.setEditable(true);

        // create and set a custom cell renderer
        class ContainerTreeCellRenderer extends DefaultTreeCellRenderer {
            public ContainerTreeCellRenderer() {
                super();
                backgroundNonSelectionColor = Color.white;
                backgroundSelectionColor = Color.white;
                borderSelectionColor = Color.green;
            }

            public Component getTreeCellRendererComponent(JTree tree,
                        Object value, boolean selected, boolean expanded,
                        boolean leaf, int row, boolean hasFocus) {
                Component component = super.getTreeCellRendererComponent(tree,
                            value, selected, expanded, leaf, row, hasFocus);
                DefaultMutableTreeNode bullet = (DefaultMutableTreeNode) value;
                if (bullet.getUserObject() instanceof ALFINode) {
                    ALFINode node = (ALFINode) bullet.getUserObject();
                    component.setForeground(determineNodeDisplayColor(node));

                    if (component instanceof JLabel) {
                        addCommentsToText((JLabel) component, node);
                    }
                }
                else { component.setForeground(Color.black); }
                return component;
            }
        }
        m_container_tree_widget.setCellRenderer(
                                  new ContainerTreeCellRenderer());

        JScrollPane scroll_pane = new JScrollPane(m_container_tree_widget);
        getWorkingPanel().add(scroll_pane, "Center");

        // add aux listener on the tree
        class NodeInfoRetriever extends MouseAdapter {
            public void mouseClicked(MouseEvent e) {
                if (e.getSource() == null) return;

                TreePath path = ((JTree) e.getSource()).
                            getPathForLocation(e.getX(), e.getY());
                if (path == null) return;
                DefaultMutableTreeNode tree_bullet =
                        (DefaultMutableTreeNode) path.getLastPathComponent();
                Object o = tree_bullet.getUserObject();
                if (o instanceof ALFINode) {
                    if (e.getClickCount() == 2) {
                        if (SwingUtilities.isRightMouseButton(e)) {
                            System.err.println(((ALFINode) o).toString(true));
                        }
                        else if (SwingUtilities.isLeftMouseButton(e)) {
                            ALFINode node = (ALFINode) o;
                            if (node.isBox()) {
                                try { displayNodeWindow(node, null, true); }
                                catch (Exception ex) { ; }
                            }
                            else {
                                NodePropertiesDialog settings =
                                          new NodePropertiesDialog(null, node);
                                settings.setVisible(true);
                            }
                        }
                    }
                        // see TreeWillExpandListener below
                    else { mb_temporarilly_disallow_tree_expansion = true; }
                }
            }
        }
        m_container_tree_widget.addMouseListener(new NodeInfoRetriever());

            // this is a complete kluge which is the only way i've found to
            // prevent a 2xclick on an entry in the tree to _not_ do the tree
            // expand for that entry.  it should only open up an edit session.
            // this method is all based on what order the TreeExpansionEvent
            // and the mouseClicked events occur, and may change!
        m_container_tree_widget.addTreeWillExpandListener(
                new TreeWillExpandListener() {
                    public void treeWillExpand(TreeExpansionEvent e)
                                              throws ExpandVetoException {
                        if (mb_temporarilly_disallow_tree_expansion) {
                            mb_temporarilly_disallow_tree_expansion = false;
                            throw new ExpandVetoException(e);
                        }
                    }
                    public void treeWillCollapse(TreeExpansionEvent e)
                                              throws ExpandVetoException {
                        if (mb_temporarilly_disallow_tree_expansion) {
                            mb_temporarilly_disallow_tree_expansion = false;
                            throw new ExpandVetoException(e);
                        }
                    }
                });
    }

        /** Adds comments to the name of a tree node. */
    private void addCommentsToText(JLabel _label, ALFINode _node) {
        StringBuffer buffer = new StringBuffer();
        boolean b_buffer_changed = false;

            // macro comments
        if (getCheckBoxItemState(ActionsIF.MACRO_INFO_IN_TREE)) {
            String info = _node.macro_createContainmentInfoText();
            if (b_buffer_changed) { buffer.append(", "); }
            buffer.append(info);
            b_buffer_changed = (b_buffer_changed || (info.length() > 0));
        }

            // other comments go here

            // change label text
        if (b_buffer_changed) {
            buffer.insert(0, " [");
            buffer.append("]");
            _label.setText(_label.getText() + buffer.toString());
        }
    }

        /** Try calling if trees do not auto-update themselves properly. */
    public void updateTrees () {
        m_container_tree_widget.treeDidChange();
        // m_inheritance_tree_widget.treeDidChange();
    }

        /** Tree nodes need to be told they've been updated (!!!)  Lame.  */
    public void updateTreeNode(ALFINode _node) {
            // do the same for all contained nodes
        ALFINode[] member_nodes = _node.memberNodes_getDirectlyContained();
        for (int i = 0 ; i < member_nodes.length; i++) {
            updateTreeNode(member_nodes[i]);
        }

            // actual update for this node
        DefaultTreeModel tree_model = Alfi.getTheNodeCache().getContainerTree();
        DefaultMutableTreeNode bullet= _node.getAssociatedContainerTreeBullet();
        tree_model.nodeChanged(bullet);
    }

        /** TreeSelectionListener method.  Enables/disables Unload and Reload in
          * the File menu depending on which (if any) node is selected in the
          * tree.
          */
    public void valueChanged(TreeSelectionEvent _event) {
        ALFINode selected_root_node = getNodeSelectedInTree();

        boolean b_enable_unload =
            ((selected_root_node != null) && selected_root_node.isRootNode());

        BasicActionIF unload_action =
                         (BasicActionIF) m_actions.getAction(ActionsIF.UNLOAD);

        BasicActionIF reload_action =
                         (BasicActionIF) m_actions.getAction(ActionsIF.RELOAD);

        unload_action.setEnabled(b_enable_unload);
        reload_action.setEnabled(b_enable_unload);

/*          doesen't change menu text.  maybe figure this out later
        String unload_desc = (b_enable_unload) ?
                   ("Unload " + selected_root_node) : ("Unload Selected Node");
        String reload_desc = (b_enable_unload) ?
                   ("Reload " + selected_root_node) : ("Reload Selected Node");

        unload_action.setShortDescription(unload_desc);

        reload_action.setShortDescription(reload_desc);
*/
    }
}     

