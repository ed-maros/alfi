package edu.caltech.ligo.alfi.summary;

import java.lang.Runnable;

/**    
  * <pre>
  * The interface for constants and actions for AlfiMainFrame.
  *
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.AlfiFrame
  */
public interface AlfiMainFrameIF {

    /**
      * Constant for accessing resources for the ParentFrame.
      *
      * @see edu.caltech.ligo.alfi.resource.AlfiResources
      */
    public static final String PARENT_FRAME = "alfi.parent.frame";
     
    public void newBox ();
    public void load ();
    public void reload (boolean _b_inform_user);
    public void unload (boolean _b_inform_user);
    public void showPreferences ();
    public void save ();
    public void forceSaveAll ();
    public void quit ();
    public void showClipboard ();
    public void find ();
    public void showPrimitiveToolbar (boolean show);
    public void showOverviewWindow ();
    public void updateTipDisplayStatus ();
    public void updateTreeMacroInfoDisplayStatus ();
    public void updateNodeMacroInfoDisplayStatus ();
    public void autoSmooth (boolean isAutoSmooth);
    public void transparentBitmaps (boolean isTransparent);
    public void transparentIcons (boolean isIcons);
    public void saveAlways (boolean flag);
    public void autoSave (boolean flag);
    public void drawConnectionsOnTop (boolean flag);
    public void restoreWindows (boolean flag);
    public void setPersistentClipboard (boolean flag);
    public void debugALFI (boolean flag);
    public void debugToStdErr (boolean flag);
    public void setAutoValidateNodes (boolean flag);
    public boolean validateNodes ();
    public void helpAbout ();
    public void helpUpdates (String _version_last_used);
    public void helpDocumentation ();
    public void helpReportBugs ();

/*
    public void cascade ();
    public void tile ();
    public void layer ();
*/

}     

