package edu.caltech.ligo.alfi.summary;

import java.awt.event.ActionEvent;
import javax.swing.JCheckBoxMenuItem;

import edu.caltech.ligo.alfi.common.BasicAction;
import edu.caltech.ligo.alfi.common.BasicActionIF;
import edu.caltech.ligo.alfi.common.BasicMenu;
import edu.caltech.ligo.alfi.common.Actions;
import edu.caltech.ligo.alfi.common.UIResourceBundle;

/**    
  * <pre>
  * The actions for the Alfi mainframe
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.Actions
  * @see edu.caltech.ligo.alfi.common.BasicAction
  */
public class AlfiActions extends Actions {

    protected AlfiMainFrameIF m_main_frame;

    public AlfiActions (UIResourceBundle _resources, AlfiMainFrame _frame) {
        super(_resources);
        m_main_frame = _frame;
    }

    protected void loadActions () {
        loadAction(ActionsIF.NEW_BOX, new NewBoxAction());
        loadAction(ActionsIF.LOAD, new LoadAction());
        loadAction(ActionsIF.RELOAD, new ReloadAction());
        loadAction(ActionsIF.UNLOAD, new UnloadAction());
        loadAction(ActionsIF.SHOW_PREFERENCES, new ShowPreferencesAction());
        loadAction(ActionsIF.SAVE, new SaveAction());
        loadAction(ActionsIF.FORCE_SAVE_ALL, new ForceSaveAllAction());
        loadAction(ActionsIF.QUIT, new QuitAction());
        loadAction(ActionsIF.SHOW_CLIPBOARD, new ShowClipboardAction());
        loadAction(ActionsIF.FIND, new FindAction());
        loadAction(ActionsIF.PRIMITIVE_TB, new ShowToolbarAction());
        loadAction(ActionsIF.OVERVIEW_WINDOW, new ShowOverviewWindowAction());
        loadAction(ActionsIF.TIP, new ShowTipsAction());
        loadAction(ActionsIF.TIP_INHERITED, new ShowInheritedTipsAction());
        loadAction(ActionsIF.MACRO_INFO_IN_TREE,
                                              new ShowMacroInfoInTreeAction());
        loadAction(ActionsIF.MACRO_INFO_ON_NODE,
                                              new ShowMacroInfoOnNodeAction());
        loadAction(ActionsIF.AUTO_SAVE, new AutoSaveAction());
        loadAction(ActionsIF.CONN_ON_TOP, new ConnectionsOnTopAction());
        loadAction(ActionsIF.RESTORE_WINDOWS, new RestoreWindowsAction());
        loadAction(ActionsIF.TRIM_CONNECTIONS, new AutoTrimConnectionsAction());
        loadAction(ActionsIF.PERSISTENT_CLIPBOARD,
                                              new PersistentClipboardAction());
        loadAction(ActionsIF.AUTO_VALIDATE_NODES,new AutoValidateNodesAction());
        loadAction(ActionsIF.VALIDATE_NODES, new ValidateNodesAction());
        loadAction(ActionsIF.ABOUT, new AboutAction());
        loadAction(ActionsIF.UPDATES, new UpdatesAction());
        loadAction(ActionsIF.DOCUMENTATION, new DocumentationAction());
        loadAction(ActionsIF.BUG_REPORT, new ReportBugsAction());
/*
        loadAction(WindowIF.TILE, new TileAction());
        loadAction(WindowIF.LAYER, new LayerAction());
        loadAction(WindowIF.CASCADE, new CascadeAction());
*/
    }

    class NewBoxAction extends BasicAction {
        NewBoxAction () {
            super(ActionsIF.NEW_BOX, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.newBox();
        }
    }

    class LoadAction extends BasicAction {
        LoadAction () {
            super(ActionsIF.LOAD, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.load();
        }
    }

    class ReloadAction extends BasicAction {
        ReloadAction () {
            super(ActionsIF.RELOAD, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.reload(true);
        }
    }

    class UnloadAction extends BasicAction {
        UnloadAction () {
            super(ActionsIF.UNLOAD, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.unload(true);
        }
    }

    class ShowPreferencesAction extends BasicAction {
        ShowPreferencesAction () {
            super(ActionsIF.SHOW_PREFERENCES, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.showPreferences();
        }
    }

    class SaveAction extends BasicAction {
        SaveAction () {
            super(ActionsIF.SAVE, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.save();
        }
    }

    class ForceSaveAllAction extends BasicAction {
        ForceSaveAllAction () {
            super(ActionsIF.FORCE_SAVE_ALL, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.forceSaveAll();
        }
    }

    class QuitAction extends BasicAction {
        QuitAction () {
            super(ActionsIF.QUIT, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.quit();
        }
    }

    class ShowClipboardAction extends BasicAction {
        ShowClipboardAction () {
            super(ActionsIF.SHOW_CLIPBOARD, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.showClipboard();
        }
    }

    class FindAction extends BasicAction {
        FindAction () {
            super(ActionsIF.FIND, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.find();
        }
    }

    class ShowToolbarAction extends BasicAction {
        ShowToolbarAction () {
            super(ActionsIF.PRIMITIVE_TB, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            if (_event.getSource() instanceof JCheckBoxMenuItem) {
                boolean display_toolbar = 
                    ((JCheckBoxMenuItem) _event.getSource()).getState();
                m_main_frame.showPrimitiveToolbar(display_toolbar);
            }
        }
    }

    class ShowOverviewWindowAction extends BasicAction {
        ShowOverviewWindowAction () {
            super(ActionsIF.OVERVIEW_WINDOW, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.showOverviewWindow();
        }
    }

    class ShowTipsAction extends BasicAction {
        ShowTipsAction () {
            super(ActionsIF.TIP, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.updateTipDisplayStatus();
        }
    }

    class ShowInheritedTipsAction extends BasicAction {
        ShowInheritedTipsAction () {
            super(ActionsIF.TIP_INHERITED, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.updateTipDisplayStatus();
        }
    }

    class ShowMacroInfoInTreeAction extends BasicAction {
        ShowMacroInfoInTreeAction () {
            super(ActionsIF.MACRO_INFO_IN_TREE, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.updateTreeMacroInfoDisplayStatus();
        }
    }

    class ShowMacroInfoOnNodeAction extends BasicAction {
        ShowMacroInfoOnNodeAction () {
            super(ActionsIF.MACRO_INFO_ON_NODE, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.updateNodeMacroInfoDisplayStatus();
        }
    }

    class AutoSaveAction extends BasicAction {
        AutoSaveAction () {
            super(ActionsIF.AUTO_SAVE, getResources());
        }

        public void actionPerformed(ActionEvent _event) {
            m_main_frame.autoSave(true);
        }
    }

    class ConnectionsOnTopAction extends BasicAction {
        ConnectionsOnTopAction () {
            super(ActionsIF.CONN_ON_TOP, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            if (_event.getSource() instanceof JCheckBoxMenuItem) {
                boolean b_put_connections_on_top =
                        ((JCheckBoxMenuItem) _event.getSource()).getState();
                m_main_frame.drawConnectionsOnTop(b_put_connections_on_top);
            }
        }
    }

    class RestoreWindowsAction extends BasicAction {
        RestoreWindowsAction () {
            super(ActionsIF.RESTORE_WINDOWS, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.restoreWindows(true);
        }
    }

    class AutoTrimConnectionsAction extends BasicAction {
        AutoTrimConnectionsAction () {
            super(ActionsIF.TRIM_CONNECTIONS, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            // automatically changes check box state.  is all we need here.
        }
    }

    class PersistentClipboardAction extends BasicAction {
        PersistentClipboardAction () {
            super(ActionsIF.PERSISTENT_CLIPBOARD, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            // automatically changes check box state.  is all we need here.
        }
    }

    class AutoValidateNodesAction extends BasicAction {
        AutoValidateNodesAction () {
            super(ActionsIF.AUTO_VALIDATE_NODES, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            // automatically changes check box state.  is all we need here.
        }
    }

    class ValidateNodesAction extends BasicAction {
        ValidateNodesAction () {
            super(ActionsIF.VALIDATE_NODES, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.validateNodes();
        }
    }

    class AboutAction extends BasicAction {
        AboutAction () {
            super(ActionsIF.ABOUT, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.helpAbout();
        }
    }

    class UpdatesAction extends BasicAction {
        UpdatesAction () {
            super(ActionsIF.UPDATES, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.helpUpdates(null);
        }
    }

    class DocumentationAction extends BasicAction {
        DocumentationAction () {
            super(ActionsIF.DOCUMENTATION, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.helpDocumentation();
        }
    }

    class ReportBugsAction extends BasicAction {
        ReportBugsAction () {
            super(ActionsIF.BUG_REPORT, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.helpReportBugs();
        }
    }
/*
    class TileAction extends BasicAction {
        TileAction () {
            super(WindowIF.TILE, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.tile();
        }
    }

    class LayerAction extends BasicAction {
        LayerAction () {
            super(WindowIF.LAYER, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.layer();
        }
    }

    class CascadeAction extends BasicAction {
        CascadeAction () {
            super(WindowIF.CASCADE, getResources());
        }

        public void actionPerformed (ActionEvent _event) {
            m_main_frame.cascade();
        }
    }
*/
}

