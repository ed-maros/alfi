package edu.caltech.ligo.alfi.summary;

import java.awt.*;
import java.awt.event.*;

import java.util.*;
import java.util.regex.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.tools.ALFINodeCache;
import edu.caltech.ligo.alfi.editor.EditorFrame;
import edu.caltech.ligo.alfi.dialogs.NodePropertiesDialog;


/**    
  * <p>
  * <code>FindBoxFile</code> finds the nodes that match certain criteria.
  * The user is allowed to search by type of the module or by the name of
  * the module.
  * </pre>
  *
  * @author  Melody Araya
  * @version %I%, %G%
  */

public class FindBoxFile extends JFrame
                         implements ChangeListener, 
                                    ListSelectionListener {

    private static final String TITLE = new String("Find");
    /**
      * Tab title for sorting by name
      */
    private static final String BY_NAME = new String("By Module Name");
    /**
      * Tab title for sorting by module type (primitive/box)
      */
    private static final String BY_TYPE = new String("By Module Type");

    // By Module Name Strings
    private static final String NAME_LABEL = new String("Name");
    /**
      * Combobox choices for the 'search by name'
      */
    private static final String[] NAME_SEARCH_CHOICES = {"is", "contains"};
    /**
      * 'Search by Name' Selection Values
      */
    private static final int EXACT_NAME_SEARCH = 0;
    private static final int SUBSTRING_NAME_SEARCH = 1;
    private static int m_name_selection = EXACT_NAME_SEARCH; 
        
    // By Module Type Strings
    private static final String TYPE_LABEL = new String("Type");
    /**
      * Combobox choices for the 'search by type'
      */
    private static final String[] TYPE_SEARCH_CHOICES = {"box", "primitive"};
    /**
      * 'Search by Type' Selection Values
      */
    private static final int BOX_FILE_SEARCH = 0;
    private static final int PRIMITIVE_SEARCH = 1;
    private static int m_type_selection = BOX_FILE_SEARCH; 

/*
    private static final String SEARCH_PARAMETER = 
        new String("Parameter Search (Optional)");
*/

    /**
      * Sorting Label
      */
    private static final String SORT = new String("Sort");
    private static final String[] SORT_CHOICES = 
            {"Backward Name Sort", "Forward Name Sort", "Depth Sort"};
    /**
      * 'Sort' Selection Values
      */
    private static final int BACKWARD_SORT = 0;
    private static final int FORWARD_SORT = 1;
    private static final int DEPTH_SORT = 2;
    private static int m_sort_selection = BACKWARD_SORT; 

    /**
      * Comparators for the SortedListModel
      */
    private static final Comparator m_reverse_comparator = 
        new ReverseNameComparator();
    private static final Comparator m_depth_comparator = new DepthComparator();

    /**
      * Results Label
      */
    private static final String RESULTS = new String("Results");
    /**
      * Window buttons
      */
    private static final String EDIT = new String("Edit"); 
    private static final String DONE = new String("Done"); 

    private static final int WIDTH = 375;
    private static final int HEIGHT = 500;


    private JTabbedPane  m_tabbed_pane = new JTabbedPane();

    // Search by Name GUI components
    private JComboBox m_name_search_choice;
    private JTextField m_pattern_entry = new JTextField();

    // Search by Type GUI components
    private JComboBox m_type_search_choice;
    private JList m_type_list;
    private SortedListModel m_list_model = new SortedListModel();

    // Parameter value
    private JTextField m_parameter_value_entry = new JTextField();

    // RadioButtons 
    private JComboBox m_sort_choice;

    // Search Results List
    private JList m_results_list;

    // Button Return codes:w

    public static final int RET_DONE = 0;
    public static final int RET_EDIT = 1;

    private int m_return_status = RET_DONE;
    
    private AlfiMainFrame m_main_frame;
    private ALFINodeCache m_node_cache;

    /** 
      * Constructs a <code>FindBoxFile</code> using the given 
      * <code>AlfiMainFrame</code> and the <code>ALFINodeCache</code>.
      */
    public FindBoxFile (AlfiMainFrame _main_frame, ALFINodeCache _node_cache) {
        super(TITLE);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        m_main_frame = _main_frame;
        m_node_cache = _node_cache;

        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                setVisible(false);
            }
        });         

        initComponents();

        pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width - WIDTH)/2, 
                  (screenSize.height - HEIGHT)/2,
                   WIDTH, HEIGHT);
        setCursor(Cursor.getDefaultCursor());
    }

    /** 
      * Clears out all previous selections when the frame is re-displayed.
      */
    public void setVisible (boolean _show) {

        // Clear out all selections/entries and set the defaults
        if (!isVisible() && _show) {
            m_pattern_entry.setText("");
            m_name_search_choice.setSelectedIndex(0);
            m_type_search_choice.setSelectedIndex(0);
            searchNodeFiles();
            m_type_list.clearSelection();
            ((SortedListModel) m_results_list.getModel()).clear();
        }

        super.setVisible(_show);
    }

    /** 
      * Initializes the components.
      */
    private void initComponents () {
        Container content_pane = getContentPane();

        content_pane.setLayout(
            new BoxLayout(content_pane, BoxLayout.Y_AXIS));

        JPanel name_panel = createSearchByNamePanel();
        m_tabbed_pane.addTab(BY_NAME, name_panel);
        m_tabbed_pane.setSelectedIndex(0);

        JPanel type_panel = createSearchByTypePanel();
        m_tabbed_pane.addTab(BY_TYPE, type_panel);
    
        JPanel results_panel = createResultsPanel();

        JPanel sort_panel = createSortPanel();

        JSplitPane split_pane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
            m_tabbed_pane, results_panel);
        split_pane.setOneTouchExpandable(true);
        split_pane.setDividerLocation(200);
        split_pane.setAlignmentX(LEFT_ALIGNMENT);
    

        JPanel button_panel = createButtonPanel();
        button_panel.setAlignmentX(LEFT_ALIGNMENT);
        
        //content_pane.add(parameter_panel);
        content_pane.add(split_pane);
        content_pane.add(sort_panel);
        content_pane.add(button_panel);

        m_tabbed_pane.addChangeListener(this);
    }

    /** 
      * Creates the panel to search by Name
      */
    private JPanel createSearchByNamePanel () {
        JPanel name_panel = new JPanel(); 
        name_panel.setLayout(new BoxLayout(name_panel, BoxLayout.Y_AXIS));
        name_panel.setBorder(GuiConstants.LOWERED_BORDER);

        JPanel name_choice_panel = new JPanel();
        name_choice_panel.setLayout(
            new BoxLayout(name_choice_panel, BoxLayout.X_AXIS));

        JLabel label = new JLabel(NAME_LABEL);
        label.setDisplayedMnemonic(KeyEvent.VK_N);
        label.setAlignmentX(LEFT_ALIGNMENT);

        m_name_search_choice = new JComboBox(NAME_SEARCH_CHOICES);
        label.setLabelFor(m_name_search_choice);
        constrainHeight(m_name_search_choice);
        m_name_search_choice.setAlignmentX(LEFT_ALIGNMENT);
        m_name_search_choice.setSelectedIndex(EXACT_NAME_SEARCH);

        name_choice_panel.setAlignmentX(LEFT_ALIGNMENT);
        name_choice_panel.add(label);
        name_choice_panel.add(m_name_search_choice);

        m_name_search_choice.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                m_name_selection = m_name_search_choice.getSelectedIndex();
                searchNodeFilesByName();
            }
        });

        constrainHeight(m_pattern_entry);
        m_pattern_entry.setAlignmentX(LEFT_ALIGNMENT);
        m_pattern_entry.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                searchNodeFilesByName();
            }
        });

        
        name_panel.add(name_choice_panel);
        name_panel.add(m_pattern_entry); 

        return name_panel;
    }

    /** 
      * Creates the panel to search by Module Type
      */
    private JPanel createSearchByTypePanel () {
        JPanel type_panel = new JPanel(); 
        type_panel.setLayout(new BoxLayout(type_panel, BoxLayout.Y_AXIS));
        type_panel.setBorder(GuiConstants.LOWERED_BORDER);

        JPanel type_choice_panel = new JPanel();
        type_choice_panel.setLayout(
            new BoxLayout(type_choice_panel, BoxLayout.X_AXIS));

        JLabel label = new JLabel(TYPE_LABEL);
        label.setDisplayedMnemonic(KeyEvent.VK_T);
        label.setAlignmentX(LEFT_ALIGNMENT);

        m_type_search_choice = new JComboBox(TYPE_SEARCH_CHOICES);
        label.setLabelFor(m_type_search_choice);
        constrainHeight(m_type_search_choice);
        m_type_search_choice.setAlignmentX(LEFT_ALIGNMENT);
        m_type_search_choice.setSelectedIndex(0);

        type_choice_panel.setAlignmentX(LEFT_ALIGNMENT);
        type_choice_panel.add(label);
        type_choice_panel.add(m_type_search_choice);

        m_type_search_choice.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                m_type_selection = m_type_search_choice.getSelectedIndex();
                listBoxesOrPrimitives(m_type_selection);
                m_type_list.setSelectedIndex(0);
                searchNodeFilesByType();
            }
        });

        m_type_list = new JList(m_list_model);
        m_type_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_type_list.addListSelectionListener(this);
        listBoxesOrPrimitives(BOX_FILE_SEARCH);
        m_type_list.setAlignmentX(LEFT_ALIGNMENT);
        JScrollPane scroll_pane = new JScrollPane(m_type_list);
        scroll_pane.setAlignmentX(LEFT_ALIGNMENT);
        
        type_panel.add(type_choice_panel);
        type_panel.add(scroll_pane);

        return type_panel;
    }


    /** 
      * Creates the panel which displays the results of the search.
      */
    private JPanel createResultsPanel () {
        JPanel results_panel = new JPanel(); 
        results_panel.setLayout(
            new BoxLayout(results_panel, BoxLayout.Y_AXIS));
        results_panel.setBorder(GuiConstants.RAISED_BORDER);

        JLabel label = new JLabel(RESULTS);
        label.setDisplayedMnemonic(KeyEvent.VK_R);

        m_results_list = new JList(new SortedListModel(m_reverse_comparator));

        label.setLabelFor(m_results_list);
        label.setAlignmentX(LEFT_ALIGNMENT);
        JScrollPane scroll_pane = new JScrollPane(m_results_list);
        scroll_pane.setAlignmentX(LEFT_ALIGNMENT);

        results_panel.add(label);
        results_panel.add(scroll_pane);
        
        MouseListener mouse_listener = new MouseAdapter () {
            public void mouseClicked (MouseEvent _event) {
                if (SwingUtilities.isLeftMouseButton(_event)) {
                    if (_event.getClickCount() == 2) {
                    ALFINode node = 
                        (ALFINode) m_results_list.getSelectedValue();
                    displayNodeContainerWindow(node);
                    }
                } else if (SwingUtilities.isRightMouseButton(_event)) {
                    int index =  
                        m_results_list.locationToIndex(_event.getPoint());
                    m_results_list.setSelectedIndex(index);

                    if (_event.getClickCount() == 2) {
                        ALFINode node = 
                            (ALFINode) m_results_list.getSelectedValue();
                        displayNodeInternals(node);
                    }
                }
            }        
        };
        m_results_list.addMouseListener(mouse_listener);
        return results_panel;
    }

    /** 
      * Creates the panel containing the sort buttons. 
      */
    private JPanel createSortPanel () {
        JPanel sort_panel = new JPanel(); 
        sort_panel.setLayout(
            new BoxLayout(sort_panel, BoxLayout.Y_AXIS));
        sort_panel.setBorder(GuiConstants.LOWERED_BORDER);

        JLabel label = new JLabel(SORT);
        label.setDisplayedMnemonic(KeyEvent.VK_S);
        label.setAlignmentX(LEFT_ALIGNMENT);

        m_sort_choice = new JComboBox(SORT_CHOICES);
        label.setLabelFor(m_sort_choice);
        constrainHeight(m_sort_choice);
        m_sort_choice.setAlignmentX(LEFT_ALIGNMENT);
        m_sort_choice.setSelectedIndex(0);

        m_sort_choice.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                if (m_sort_selection != m_sort_choice.getSelectedIndex()) {
                    m_sort_selection = m_sort_choice.getSelectedIndex();
                    sortResults(m_sort_selection);
                }
            }
        });

        sort_panel.add(label);
        sort_panel.add(m_sort_choice);
 
        return sort_panel;
    }

    /** 
      * Creates the panel containing the 'Edit' and 'Done' buttons. 
      */
    private JPanel createButtonPanel () {
        JPanel button_panel = new JPanel();        
        button_panel.setLayout(new FlowLayout (FlowLayout.CENTER, 5, 5));

        JButton edit_button;    
        edit_button = new JButton();        
        edit_button.setText(EDIT);
        edit_button.setMnemonic('E');
        edit_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                editNodeFiles();
            }
        });   

        JButton done_button;    
        done_button = new JButton();        
        done_button.setText(DONE);
        done_button.setMnemonic('D');
        done_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                setVisible(false);
            }
        });   

        button_panel.add(edit_button);
        button_panel.add(done_button);

        getRootPane().setDefaultButton(edit_button);
    
        constrainHeight(button_panel);

        return button_panel;
    }

    /** 
      * Constrains the height but not the width when the UI is resized
      */
    private void constrainHeight (JComponent _component) {
        _component.setAlignmentX(CENTER_ALIGNMENT);
        _component.setAlignmentY(CENTER_ALIGNMENT);
        Dimension dim = _component.getPreferredSize();
        dim.width = Integer.MAX_VALUE;
        _component.setMaximumSize(dim);
    }

    /** 
      * Displays either a list of root base boxes or a list of 
      * primitives. 
      */
    private void listBoxesOrPrimitives (int _search_type) {
        m_list_model.clear();
        m_type_list.clearSelection();
        if (_search_type == BOX_FILE_SEARCH) {
            m_list_model.addAll(m_node_cache.getRootBoxNodes_unsorted());
        } else {
            m_list_model.addAll(m_node_cache.getRootPrimitiveNodes_unsorted());
        }
    }

    /** 
      * Displays the results list in the sort selected. 
      */
    private void sortResults (int _sort_selection) {

        Comparator comparator = null;

        if (_sort_selection == BACKWARD_SORT) {
            comparator = m_reverse_comparator;
        }
        else if (_sort_selection == DEPTH_SORT) {
            comparator = m_depth_comparator;
        }

        SortedListModel old_list_model = 
            (SortedListModel) m_results_list.getModel();

        SortedListModel list_model = new SortedListModel(comparator);
        list_model.addAll(old_list_model.toArray());
        m_results_list.clearSelection();
        m_results_list.setModel(list_model);

    }

    /** Displays the selected node's container. */
    private void displayNodeContainerWindow (ALFINode _node) {
        ALFINode n = (_node.isRootNode()) ? _node : _node.containerNode_get();
        m_main_frame.show(n, null, true);    
    }

    /** 
      * Displays the NodePropertiesDialog if the selected node is a primitive
      * and otherwise displays node contents.
      */
    private void displayNodeInternals (ALFINode _node) {
        if (_node.isPrimitive()) {
            NodePropertiesDialog settings = 
                new NodePropertiesDialog(this, _node);
            settings.setVisible(true);
        } else {
            m_main_frame.show(_node, null, true);    
        }
    }

    /** 
      * Goes through the selected list and displays the containers
      * of each nodes.
      */
    private void editNodeFiles () {
        Object nodes[] = m_results_list.getSelectedValues();
        for (int ii = 0; ii < nodes.length; ii++) {
            displayNodeContainerWindow((ALFINode) nodes[ii]);
        }
    }

    /** 
      * Lists for the ALFINodes which meet the search criteria.
      */
    private void searchNodeFiles () {
        if (m_tabbed_pane.getSelectedIndex() == 0) {
            searchNodeFilesByName();
        } else {
            searchNodeFilesByType();
        }     
    }

    /** 
      * Lists the ALFINodes which match or contains the 
      * string entered by the user.
      */
    private void searchNodeFilesByName () {
        setCursor(new Cursor(Cursor.WAIT_CURSOR));

        ((SortedListModel) m_results_list.getModel()).clear();
        m_results_list.clearSelection();

        String pattern_entry = new String(m_pattern_entry.getText());

        if (m_pattern_entry.getText().trim().length() > 0) { 

            String text_entry = m_pattern_entry.getText().trim();


            DefaultTreeModel ctree = m_node_cache.getContainerTree();
            DefaultMutableTreeNode root = 
                (DefaultMutableTreeNode) ctree.getRoot();

            SortedListModel model = (SortedListModel) m_results_list.getModel();

            for (Enumeration ee = root.postorderEnumeration(); 
                 ee.hasMoreElements(); ) {

                DefaultMutableTreeNode tree_node = 
                    (DefaultMutableTreeNode) ee.nextElement();

                if (tree_node.getUserObject() instanceof ALFINode) {
                    ALFINode node = (ALFINode) tree_node.getUserObject(); 
                    String name = node.determineLocalName();
                    if (m_name_selection == EXACT_NAME_SEARCH) {
                        if (text_entry.equals(name)) {
                            model.add(node);
                        }   
                    } else {
                        Pattern pattern = Pattern.compile(text_entry);
                        Matcher matcher = pattern.matcher(name);
                        if (matcher.find()){
                            model.add(node);
                        }
                    }
                }
            }     
        }
        setCursor(Cursor.getDefaultCursor());
    }

    /** 
      * Lists the ALFINodes which are intances of the box/primitive
      * selected in the list.
      */
    private void searchNodeFilesByType () {
        if (m_type_list.getSelectedIndex() >= 0) {
            ALFINode node = (ALFINode) m_type_list.getSelectedValue();
            searchNodeFilesByType(node);
        }
    }

    /** 
      * Lists the ALFINodes which are intances of the box/primitive
      * selected in the list.
      */
    private void searchNodeFilesByType (ALFINode _node) {

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        ((SortedListModel) m_results_list.getModel()).clear();
        m_results_list.clearSelection();

        if (_node == null) { return; }

        DefaultTreeModel ctree = m_node_cache.getContainerTree();
        DefaultMutableTreeNode root = 
            (DefaultMutableTreeNode) ctree.getRoot();

        SortedListModel model = (SortedListModel) m_results_list.getModel();

        for (Enumeration ee = root.postorderEnumeration(); 
             ee.hasMoreElements(); ) {

            DefaultMutableTreeNode tree_node = 
                (DefaultMutableTreeNode) ee.nextElement();

            if (tree_node.getUserObject() instanceof ALFINode) {
                ALFINode node = (ALFINode) tree_node.getUserObject(); 
                ALFINode root_node = node;

                if (!node.isRootNode()) {
                    root_node = node.baseNode_getRoot();
                } 

                if (root_node == _node) {
                    model.add(node);
                }
            }     
        }
        setCursor(Cursor.getDefaultCursor());
    }

    /**
      * ChangeListener method invoked when the tabs are selected.
      * In this case 
      */
    public void stateChanged (ChangeEvent _event) {
        if (_event.getSource().equals(m_tabbed_pane)) {
            searchNodeFiles();
        }
    }

    /**
      * ListSelectionListener method 
      */
    public void valueChanged (ListSelectionEvent _event) {
        if (_event.getSource().equals(m_type_list)) {
            searchNodeFilesByType();
        } 
    }

}
