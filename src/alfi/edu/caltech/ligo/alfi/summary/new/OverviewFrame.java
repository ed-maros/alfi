package edu.caltech.ligo.alfi.summary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.tools.*;

/**    
  *
  * <pre>import edu.caltech.ligo.alfi.Alfi;


  * The Overview Frame.
  * </pre>
  *
  * @author  Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.AlfiFrame
  */
public class OverviewFrame extends AlfiFrame {

    private Dimension m_screen_size;

    private AlfiDesktopPane m_overview;

    //**************************************************************************
    //********* Constructors ***************************************************
    public OverviewFrame (AlfiMainFrame _parent) {
        super(_parent, "ALFI Overview Window", null); 

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        m_screen_size = Toolkit.getDefaultToolkit().getScreenSize();
        setDefaultWindowSize(m_screen_size);
        initializeFrame();
    }

    /**
      * initializes this frame. This is called after
      * init() on this frame by the window manager.
      */
    public void initializeFrame () {
        setSize(m_screen_size);

        m_overview = new AlfiDesktopPane(this);
        getWorkingPanel().add(m_overview, BorderLayout.CENTER);
        
    }

    protected Actions initializeActions (){
        return null;
    }

    public ALFINode getNodeToDisplay () {
        return m_overview.getNodeToDisplay();
    }


}
