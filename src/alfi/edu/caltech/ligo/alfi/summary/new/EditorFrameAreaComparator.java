package edu.caltech.ligo.alfi.summary;

import java.util.*;
import edu.caltech.ligo.alfi.editor.EditorFrame;

public class EditorFrameAreaComparator implements Comparator {

    public static final int FRAME_UNITS = 200;

    // Compare two elements
    public int compare(Object element1, Object element2) {
        EditorFrame editor1 = (EditorFrame) element1;
        EditorFrame editor2 = (EditorFrame) element2;

        int area_1 = calculateSimplifiedArea(
                        editor1.getWidth(), editor1.getHeight());
        int area_2 = calculateSimplifiedArea(
                        editor2.getWidth(), editor2.getHeight());
        
        if (area_1 < area_2) { return -1; }
        else if (area_2 > area_1 { return 1; }
        else { return 0; }
    }

    private int calculateSimplifiedArea (int _frame_width, int _frame_height ) {

        int width_units, height_units;
        width_units = Math.round(( double )_frame_width/( double )FRAME_UNITS); 
        height_units = Math.round(( double )frame_height/( double )FRAME_UNITS); 
        
        return (width_units * height_units);
    } 



}       

 
