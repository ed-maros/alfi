package edu.caltech.ligo.alfi.file;

import java.io.*;
import java.awt.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.tools.*;

    /** This exception occurs when the parser cannot find an included file. */
public class AlfiIncludeFileNotFoundException extends AlfiException {
    public static final int PATH_ABSOLUTE = 0;
    public static final int PATH_RELATIVE = 1;
    public static final int PATH_E2E      = 2;

    public static final int DO_NOTHING      = 10;
    public static final int MODIFY_E2E_PATH = 11;

    protected int m_path_type;
    protected String m_original_file_path;
    protected String m_include_path;

    public AlfiIncludeFileNotFoundException(Object _event_source,
                        String _original_file_path, String _include_path) {
        super("", ALFI_WARNING);

        m_original_file_path = new String(_original_file_path);
        m_include_path = new String(_include_path);

        if (m_include_path.startsWith(ALFIFile.E2E_PARTIAL_PATH_IDENTIFIER)) {
            m_path_type = PATH_E2E;
        }
        else if (m_include_path.startsWith(File.separator)) {
            m_path_type = PATH_ABSOLUTE;
        }
        else { m_path_type = PATH_RELATIVE; }
    }

    public int getIncludePathType() { return m_path_type; }

        /** Overridden here for more informative message. */
    public String getMessage() {
        int index = m_original_file_path.lastIndexOf(File.separator);
        String original_file_name = m_original_file_path.substring(index+1);
        String original_directory = m_original_file_path.substring(0,index);

        String message = original_file_name + " NOT LOADED.";
        message = TextFormatter.formatMessage(message);

        String s = "While attempting to load the file " +
            m_original_file_path + ", the included file specified in " +
            original_file_name + " using the directive \"#include " +
            m_include_path + "\" could not be found.";
        message += "\n\n" + TextFormatter.formatMessage(s);

        if (m_path_type == PATH_E2E) {
            String include_path = m_include_path.substring(1);
            index = include_path.lastIndexOf(File.separator);
            String include_name = (index >= 0) ?
                              include_path.substring(index + 1) : include_path;

            s = "The include path is an \"E2E partial path\" which " +
                "means that the path " + include_path + " is considered to " +
                "be relative to any of the directories in your E2E_PATH " +
                "search path.  Your E2E_PATH is currently:";
            message += "\n\n" + TextFormatter.formatMessage(s);
            message += "\n\n" + Alfi.getE2EPath();

            s = "In this case, the file path " + include_path + " was not " +
                "found relative to any of the directories specified in the " +
                "current E2E_PATH.";
            message += "\n\n" + TextFormatter.formatMessage(s);

            s = "In order to fix this problem, you may either fix the " +
                "specification in the directive \"#include " + m_include_path +
                "\", move the file " + include_name + " into a location " +
                "where " + include_path + " will be found somewhere in the " +
                "current E2E_PATH search path, or you may change the " +
                "current E2E_PATH value.";
            message += "\n\n" + TextFormatter.formatMessage(s);

            s = "Do you wish to change this Alfi session's E2E_PATH search " +
                "path value?";
            message += "\n\n" + TextFormatter.formatMessage(s);
        }
        else {
            String include_full_path =
                    (m_path_type == PATH_ABSOLUTE) ? m_include_path :
                          original_directory + File.separator + m_include_path;
            index = include_full_path.lastIndexOf(File.separator);
            String include_directory = include_full_path.substring(0, index);
            String include_name = include_full_path.substring(index + 1);

            String description = (m_path_type == PATH_ABSOLUTE) ?
                "The include path is an absolute path in the file system." :
                "The include path is a path relative to the original " +
                        "box file's directory ( " + original_directory + " ).";

            s = description + "  This means that the file " + include_name +
                " in the directory " + include_directory + " does not exist.  ";
            message += "\n\n" + TextFormatter.formatMessage(s);

            s = "To fix this problem, you must either place " +
                "the file " + include_name + " in the directory " +
                include_directory + " or edit the original container box " +
                "file " + original_file_name + " in a text editor to " +
                "point to the proper location of " + include_name + " .";
            message += "\n\n" + TextFormatter.formatMessage(s);
        }

        return message;
    }

        /** This method shows a dialog and returns a response in regards to the
          * user's choice in how to deal with this exception.
          */
    public int showMessage(Component _parent) {
        String title = "Included File Not Found";

        if (m_path_type == PATH_E2E) {
            int response = JOptionPane.showConfirmDialog(_parent, getMessage(),
                title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (response == JOptionPane.YES_OPTION) {
                return MODIFY_E2E_PATH;
            }
            else { return DO_NOTHING; }
        }
        else {
            JOptionPane.showMessageDialog(_parent, getMessage(), title,
                                              JOptionPane.INFORMATION_MESSAGE);
            return DO_NOTHING;
        }
    }
}
