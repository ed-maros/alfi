package edu.caltech.ligo.alfi.file;

import java.io.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;


    /**
     * This class contains all the warnings, error messages, etc needed by the
     * parser in order to inform users and the log of Alfi file format
     * problems.  It also contains utility methods for their access and use.
     *
     * This class is package-only visible.
     */
public class FileFormatProblemManager {

    //**************************************************************************
    //***** constants **********************************************************

        // member node messages
    //final static String 

        // member node warnings
    final static String BAD_MEMBER_DESCRIPTION_LINE =
        "Bad member node description line.";
    final static String BAD_MEMBER_REMOVE_START_LINE =
        "Bad member node remove start line.";
    final static String UNRECOGNIZED_NODE_TYPE_FORMAT =
        "Type of member node to be added not recognized.";
    final static String BAD_PRIMITIVE_INCLUDE_LINE =
        "Bad include line for primitive type member node.";
    final static String BAD_BOX_DESCRIPTION_LINE = "Bad box description line.";
    final static String BAD_BOX_FILE_TO_INCLUDE_LINE =
        "Bad file to include line.";
    final static String BAD_BOX_INCLUDE_SECTION =
        "Bad box include section format.";

        // member node errors
    //final static String 

        // port messages
    final static String BAD_POSITION_LINE =
        "Bad position format for port.  But port and its position are " +
        "possibly okay still.";
    final static String BAD_EDGE__REVERT =
        "Bad edge format for port.  Reverting to default edge.  (This may " +
        "cause subsequent ports to be forced out of position.)";
    final static String BAD_EDGE_POSITION__REVERT =
        "Bad edge position format for port.  Reverting to default edge " +
        "position.  (This may cause subsequent ports to be forced out of " +
        "position.)";
    final static String BAD_PORT_CONFIG = "Bad port-configuration line format.";
    final static String BAD_PORT_CONFIG_DIRECTIVE =
        "Unknown port-configuration directive.";
    final static String PORT_DATA_TYPE_UNSPECIFIED =
        "Port data type left unspecified.  Defaulting to Unknown data type.";

        // port warnings
    final static String BAD_PORT_DESCRIPTION = "Bad port description format.";
    final static String UNRECOGNIZED_PORT = "Port not found.";
    final static String BAD_BUNDLE_PORT_LINES = "Bad bundle port description.";
    final static String INVALID_IO_TYPE = "Invalid I/O type for port.";
    final static String PORT_EXISTS =
        "Port of same name and I/O type already exists.";

        // port errors
    //final static String 

        // connection messages
    //final static String 

        // connection warnings
    final static String CONNECTION_NOT_FOUND = "Connection not found.";
    final static String BAD_CONNECTION_DESCRIPTION =
        "Bad connection description";
    final static String BAD_CONNECTION_PATH_DESCRIPTION =
        "Bad connection path description";
    final static String BAD_ADD_CONNECTIONS_SECTION =
        "A parsing error occured somewhere within this Add Connections " +
        "section, most likely due to missing or extra open/close brackets.  " +
        "One or more connections in this section may have not been loaded.";
    final static String CONNECTION_NOT_ADDED = "Connection not added.";

        // connection errors
    //final static String 

        // junction messages
    final static String BAD_ADD_JUNCTION_START_LINE =
        "Bad start line in add junction section.";
    final static String BAD_JUNCTION_DESCRIPTION_LINE =
        "Bad junction description.";
    final static String BAD_JUNCTION_POSITION = "Bad junction Position.";

        // junction warnings
    //final static String 

        // junction errors
    //final static String 

        // bundler messages handled by new methodology: see BundleProxy.parse()

        // macro messages
    //final static String 

        // macro warnings
    final static String BAD_MACRO = "Bad Macro format.";

        // macro errors
    //final static String 

        // parameter messages
    //final static String 

        // parameter warnings
    final static String BAD_PARAMETER_DECLARATION =
        "Bad Parameter Declaration format.";
    final static String INVALID_NODE_FOR_PARAMETER_MODS =
        "Invalid node type to have Parameter Modification entries.";
    final static String UNRECOGNIZED_PARAMETER =
        "Parameter name not recognized.";
    final static String BAD_PARAMETER_MOD =
        "Invalid Parameter Modification format.";
    final static String BAD_AT_AT_INCLUDE_FORMAT =
        "Invalid @@INCLUDE line format.";

        // parameter errors
    //final static String 

        // misc messages
    final static String BAD_NUMBER_FORMAT = "Bad number format.";
    final static String VERSION_DEPRECATED =
        "This file contains deprecated formats which may not be supported in " +
        "the future.  You should save this file in this session to rewrite " +
        "it in a more current form.";
    final static String BAD_GUI_SETTINGS_SECTION =
        "Badly formatted GUI Settings section.  None of these settings will " +
        "be applied to this node:";
    final static String BAD_GUI_SETTINGS_DIRECTIVE =
        "Invalid GUI Settings directive.";
    final static String BAD_SCREEN_SIZE = "Bad screen size format.  Ignored.";
    final static String BAD_ORIGIN = "Bad origin format.  Ignored.";
    final static String BAD_ROTATION = "Bad rotation format.  Ignored.";
    final static String BAD_SWAP = "Bad swap format.  Ignored.";
    final static String BAD_SYMMETRY = "Bad symmetry format.  Ignored.";
    final static String BAD_NODE_DISPLAY = "Bad node display format.  Ignored.";
    final static String OBSOLETE_METHOD_CALLED =
        "Contents of this section of the the file being read result in a " +
        "call to a method which should be obsolete if the version marker at " +
        "the top of the file is correct.";
    final static String BAD_PARAMETER_LINK_DECLARATION_SECTION =
        "Bad parameter link declaration format for node: ";
    final static String BAD_PARAMETER_LINK_SETTING_SECTION =
        "Bad parameter link setting format for node: ";
    final static String BAD_NODE_DESCRIPTIONS_SECTION =
        "Badly formatted node descriptions section.  " +
        "None of these settings will be applied to this node:";

        // misc warnings
    final static String BAD_SETTINGS = "Bad settings line format.";
    final static String BAD_SECTION_START = "Bad section start.";
    final static String BAD_SECTION_MARKER = "Bad section marker.";

        // misc errors
    final static String BAD_VERSION_FORMAT = "Bad version line format.";
    final static String BAD_SECTION_BEGIN = "Bad section begin format.";
    final static String BAD_SECTION_END = "Bad section end format.";

    //**************************************************************************
    //***** static fields ******************************************************

    private static boolean msb_deprecated_param_bool_type_warning_given = false;
    private static boolean msb_deprecated_port_bool_type_warning_given = false;

    //**************************************************************************
    //***** fields *************************************************************

    private boolean mb_pause_after_each_message;
    private ParseErrorDialog m_issues_dialog;
    private String m_previous_warning_file_path;

    //**************************************************************************
    //***** constructors *******************************************************

    public FileFormatProblemManager() {
        mb_pause_after_each_message = true;
        m_issues_dialog = null;
        m_previous_warning_file_path = null;
    }

    //**************************************************************************
    //***** methods ************************************************************

    public void resolve() {
        if (m_issues_dialog != null) {
            boolean b_write_report = m_issues_dialog.getLoggingFlag();
            resolve(b_write_report);

            m_issues_dialog.dispose();
        }
    }

    private void resolve(boolean _b_write_report) {
        if (_b_write_report) { writeReport(); }

        int user_choice = m_issues_dialog.getChoice();
        switch (user_choice) {
          case ParseErrorDialog.CONTINUE:
                // do nothing.  user has already acknowledged messages.
            break;
          case ParseErrorDialog.COMPLETE_LOAD:
                // if the dialog is showing at this point it is because more
                // messages have occured.  make dialog modal again and get
                // final user response.
            if (m_issues_dialog.isVisible()) {
                m_issues_dialog.finalizeCompleteLoad();
                    // if report not yet written, but user now asks for it
                boolean b_report =
                    (! _b_write_report) && m_issues_dialog.getLoggingFlag();
                resolve(b_report);
            }
            break;
          case ParseErrorDialog.STOP_LOAD:
                // do nothing further.  FileLoadingCanceledException should have
                // prompted everything that needs to be done in this case.
            break;
          case ParseErrorDialog.EXIT_ALFI:
            AlfiMainFrame main_window = Alfi.getMainWindow();
            if (main_window != null) {
                main_window.quit();
            }
            else { System.exit(1); }
        }
    }

    private void appendToFormatIssuesDialog(String _message) {
        if (m_issues_dialog == null) {
            m_issues_dialog = new ParseErrorDialog(Alfi.getMainWindow());
            m_issues_dialog.pack();
            EditorFrame.centerDialog(m_issues_dialog);
        }

        m_issues_dialog.appendMessage(_message);

        if (mb_pause_after_each_message) {
            if (! m_issues_dialog.isModal()) { m_issues_dialog.setModal(true); }
        }
        else { m_issues_dialog.setModal(false); }

        if (! m_issues_dialog.isVisible()) {
            m_issues_dialog.setVisible(true);
        }

        int choice = m_issues_dialog.getChoice();
        if ((choice == ParseErrorDialog.CONTINUE) ||
                                  (choice == ParseErrorDialog.COMPLETE_LOAD)) {
            mb_pause_after_each_message = (choice == ParseErrorDialog.CONTINUE);
        }
    }

        /** Sends file format information to user and log. */
    void doMessage(String _file_path, String _message, String[] _file_lines) {
        doMessage(_file_path, _message, concatLines(_file_lines));
    }

        /** Sends file format information to user and log. */
    void doMessage(String _file_path, String _message, String _file_lines) {
        try {
            throw new AlfiFileFormatException(_message, _file_path,
                                      _file_lines, AlfiException.ALFI_INFO);
        }
        catch(AlfiFileFormatException e) {
            appendToFormatIssuesDialog(e.toString());
        }
    }

        /** Sends file format warning to user and log. */
    void doWarning(String _file_path, String _warning, String[] _file_lines)
                                          throws FileLoadingCanceledException {
        doWarning(_file_path, _warning, concatLines(_file_lines));
    }

        /** Sends file format warning to user and log. */
    void doWarning(String _file_path, String _warning, String _file_lines)
                                          throws FileLoadingCanceledException {
            // check if the loading has been cancelled
        if (m_issues_dialog != null) {
            int user_choice = m_issues_dialog.getChoice();
            if ((user_choice == ParseErrorDialog.STOP_LOAD) ||
                                 (user_choice == ParseErrorDialog.EXIT_ALFI)) {

                    // there is a trick to this STOP LOAD procedure.  when the
                    // user chooses to STOP LOAD, in order to stop the loading
                    // cleanly, we want to throw a FileLoadingCanceledException,
                    // but the STOP LOAD button may be pressed at any time
                    // during a load process, thus being processed in a
                    // different thread.  we want that exception to be thrown
                    // in the thread doing the loading, so instead, we just
                    // have the user button press event set the current
                    // user_choice state.  then we lie in wait in this method
                    // for the load thread to (maybe) find another error, and
                    // then the exception is thrown and the loading is stopped.
                    // the file that was being loaded at the time of the STOP
                    // LOAD button press is kept in m_previous_warning_file_path
                    // which is subsequently cleaned up (unloaded) during the
                    // resolve() method which must always be called after the
                    // file loading associated with a particular
                    // FileFormatProblemManager is finished.
    
                throw new FileLoadingCanceledException(_file_path,
                                                 m_previous_warning_file_path);
            }
        }

        try {
            throw new AlfiFileFormatException(_warning, _file_path,
                                      _file_lines, AlfiException.ALFI_WARNING);
        }
        catch(AlfiFileFormatException e) {
            appendToFormatIssuesDialog(e.toString());
        }

        m_previous_warning_file_path = _file_path;
    }

        /** Sends file format error to user and log. */
    void doError(String _file_path, String _error, String[] _file_lines)
                                               throws AlfiFileFormatException {
        doError(_file_path, _error, concatLines(_file_lines));
    }

        /**
         * Just throws (and does not catch) an appropriate
         * AlfiFileFormatException.
         */
    void doError(String _file_path, String _error, String _file_lines)
                                               throws AlfiFileFormatException {
        throw new AlfiFileFormatException(_error, _file_path, _file_lines,
                                          AlfiException.ALFI_ERROR);
    }

    private String concatLines(String[] _lines) {
        String s = new String();
        for (int i = 0; i < _lines.length; i++) {
            s += _lines[i] + "\n";
        }
        return s;
    }

    private void writeReport() {
        String report = m_issues_dialog.getReportText();
        File report_file = null;
        try {
            report_file = new File("parse_error_report.txt");
            FileWriter writer = new FileWriter(report_file);
            writer.write(report, 0, report.length());
            writer.flush();
            writer.close();

            String title = "Parse Error Report Written";
            String message = "The parse error report has been " +
                             "written to file " + report_file + ".";
            int type = JOptionPane.PLAIN_MESSAGE;
            JOptionPane.showMessageDialog(null, message, title, type);
        }
        catch(IOException _e) {
            System.err.println(_e.toString());
            System.err.println("\nThe following report was NOT written " +
                               "to file:\n");
            System.err.println(report);
        }
    }

    void processDeprecatedParamBoolTypeWarning(String _file_name) {
        if (! msb_deprecated_param_bool_type_warning_given) {
          String message =
            "The file [" + _file_name + "]\n" +
            "    contains parameter declarations containing the deprecated\n" +
            "    data type string \"bool\".  This file and others like it\n" +
            "    should be updated to use the string \"boolean\" instead.\n" +
            "    Alfi currently recognizes this parameter type to be\n" +
            "    \"boolean\", but may not in future.\n\n" +

            "    This message is informational only and will not affect\n" +
            "    this session of Alfi, and it will not be repeated in this\n" +
            "    session even if further instances of this problem occur in\n" +
            "    this or other files.\n";
          System.err.println(message);
          msb_deprecated_param_bool_type_warning_given = true;
        }
    }

    void processDeprecatedPortBoolTypeWarning(String _file_name) {
        if (! msb_deprecated_port_bool_type_warning_given) {
          String message =
            "The file [" + _file_name + "]\n" +
            "    contains ports specifying the deprecated data type string\n" +
            "    \"bool\".  This file and others like it should be updated\n" +
            "    to use the string \"boolean\" instead.  Alfi currently\n" +
            "    recognizes this port data type to be \"boolean\", but may\n" +
            "    not in future.\n\n" +

            "    This message is informational only and will not affect\n" +
            "    this session of Alfi, and it will not be repeated in this\n" +
            "    session even if further instances of this problem occur in\n" +            "    this or other files.\n";
          System.err.println(message);
          msb_deprecated_port_bool_type_warning_given = true;
        }
    }
}
