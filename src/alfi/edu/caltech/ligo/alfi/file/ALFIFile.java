package edu.caltech.ligo.alfi.file;

import java.io.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;


    /**
     * Information about location and access to box and primitive files in the
     * system.
     */
public class ALFIFile {

    //**************************************************************************
    //***** constants **********************************************************

    public static final String E2E_PARTIAL_PATH_IDENTIFIER = "*";
    public static final String BOX_NODE_FILE_SUFFIX = ".box";
    public static final String MODELER_FILE_SUFFIX = ".sim";
    public static final String PRIMITIVE_NODE_FILE_SUFFIX = ".prm";
    public static final String UDP_FILE_SUFFIX = ".udp";

    //**************************************************************************
    //***** private member fields **********************************************

        /** This field contains the fully qualified unix path to the file. */
    private String m_canonical_file_path;

        /**
         * This is always a root container node, and can be either a box or
         * primitive node.
         */
    private ALFINode m_node;

        /** Signals if a box file was explicitly loaded by the user (incuding
          * command line and loads due to previous session set-up.)  Box files
          * which are loaded automatically because of includes in other boxes
          * have a false value.  Primitive's files always have a false value.
          */
    private final boolean mb_user_loaded;

        /** Signals whether this node is a box or a primitive. */
    private final boolean mb_is_primitive;

        /**
         * What parse version was this file written as:
         *
         * int[3] = { major version, minor version, bug-fix version }
         */
    private int[] m_parse_version_numbers = { 0, 0, 0 };

        /**
         * This stamp is used only in determining whether the disk file needs to
         * be updated.  At save and quit time, this stamp is compared with that
         * of the time stamps on m_node and all its contained nodes.  If they
         * have a later time stamp than this file does, then the file needs
         * to be saved.
         */
    private Date m_time_last_modified;

        /** This set keeps track of box files already read.   This is used
         *  for the .sim file used by the modeler
         */
    private static HashSet ms_defined_node = new HashSet();

    private static JLabel ms_status_label = null;

    //**************************************************************************
    //***** constructors *******************************************************

    public ALFIFile(String _canonical_path_to_box_or_primitive_file,
                    FileFormatProblemManager _format_problem_manager) throws
                                             AlfiIncludeFileNotFoundException,
                                             CyclicAlfiFileIncludeException,
                                             FileLoadingCanceledException {
        this(_canonical_path_to_box_or_primitive_file,
                                               false, _format_problem_manager);
    }

    public ALFIFile(String _canonical_path_to_box_or_primitive_file,
                    boolean _b_user_requested_load,
                    FileFormatProblemManager _format_problem_manager) throws
                                             AlfiIncludeFileNotFoundException,
                                             CyclicAlfiFileIncludeException,
                                             FileLoadingCanceledException {
        this(_canonical_path_to_box_or_primitive_file, _b_user_requested_load,
                                                null, _format_problem_manager);
    }

    public ALFIFile(String _canonical_path_to_box_or_primitive_file,
                    boolean _b_user_requested_load, JLabel _status_label,
                    FileFormatProblemManager _format_problem_manager) throws
                                              AlfiIncludeFileNotFoundException,
                                              CyclicAlfiFileIncludeException,
                                              FileLoadingCanceledException {
            // aux string for error messages if any
        String method_call =
                "ALFIFile(" + _canonical_path_to_box_or_primitive_file +
                ", " + _b_user_requested_load + "...):";
        //System.err.println(method_call);

        m_canonical_file_path = _canonical_path_to_box_or_primitive_file;

        File file = new File(m_canonical_file_path);
        if (! file.exists()) {
                // create a new file
            try {
                FileWriter writer = new FileWriter(m_canonical_file_path);
                String file_contents = Parser.getVersionSignature() + Parser.NL;
                writer.write(file_contents, 0, file_contents.length());
                writer.flush();
                writer.close();
            }
            catch(IOException e) {
                System.err.println(
                               method_call + "\tERROR: Could not write file.");
                mb_is_primitive = false;
                mb_user_loaded = false;
                return;
            }
        }

        String directory = file.getParent();
        String file_name = file.getName();

        int suffix_length = -1;
        if (file_name.endsWith(PRIMITIVE_NODE_FILE_SUFFIX)) {
            suffix_length = PRIMITIVE_NODE_FILE_SUFFIX.length();
            mb_is_primitive = true;
        }
        else if (file_name.endsWith(BOX_NODE_FILE_SUFFIX)) {
            suffix_length = BOX_NODE_FILE_SUFFIX.length();
            mb_is_primitive = false;
        }
        else {
                // error condition
            System.err.println(method_call + "\tWARNING: Trying to load file " +
                        "not marked as a box or primitive.");
                // final fields must be initialized.
            mb_is_primitive = false;
            mb_user_loaded = false;
            return;
        }

        mb_user_loaded = (mb_is_primitive) ? false : _b_user_requested_load;

        if (_status_label != null) { ms_status_label = _status_label; }

        setTimeLastModified();

            // this is the serious stuff.  parse the file and create the base
            // node and the nodes it contains.  Make sure no proxy listener
            // registration is attempted on any nodes before they are fully
            // read into the system.
        ProxyListenerRegistrationTool registrar =
                                          Alfi.getTheProxyListenerRegistrar();
        registrar.interrupt(this);
        read(_format_problem_manager);

        registrar.resumeProcessingLoop(this);
    }

    //**************************************************************************
    //***** methods ************************************************************

        /**
         * This method is used when we wish to write an already
         * established node out to a file other than its own ALFIFile.
         */
    public void writeNodeToDifferentFile(File _file) {
        if (_file.exists()) {
            String title = "Overwrite File?";
            String message = "File " + _file + " already exists.  Do you " +
                "wish to overwrite it?";
            int response = JOptionPane.showConfirmDialog(null,
                                  message, title, JOptionPane.YES_NO_OPTION);
            if (response == JOptionPane.YES_OPTION) { _file.delete(); }
            else { return; }
        }

        String canonical_file_path = null;
        try { canonical_file_path = _file.getCanonicalPath(); }
        catch(IOException e) {
            Alfi.warn("Failed to create file " + _file + ".", false);
            e.printStackTrace();
            return;
        }

            // remember the current time stamp on this ALFIFile
        Date original_time_stamp = (Date) m_time_last_modified.clone();

            // create a backup of the current file
        File backup_file = writeCurrentFileBackup();

            // now write m_node to file as usual (but no backup needed)
        write(false);

            // move newly written file to new name
        File new_file = new File(canonical_file_path);
        createAssociatedJavaFile().renameTo(new_file);

            // copy the backup that was made back into original place
        try { FileIO.copyFile(backup_file.getPath(), m_canonical_file_path); }
        catch(Exception e) {
            String warning = "May have failed to reinstate original version " +
                "of " + m_canonical_file_path + ".  A backup exists at " +
                backup_file.getPath() + ".";
            Alfi.warn(warning);
            return;
        }

            // finally, reinstate this ALFIFile's time stamp
        setTimeLastModified(original_time_stamp);
    }

        /** Read the file and create the ALFINodes in its hierarchy. */
    private void read(FileFormatProblemManager _format_problem_manager) throws
                                              AlfiIncludeFileNotFoundException,
                                              CyclicAlfiFileIncludeException,
                                              FileLoadingCanceledException {
        if (ms_status_label != null) {
            String message = "Reading " + m_canonical_file_path + " . . .";
            ms_status_label.setText(message);

                // this is a kluge due to some repaint problems initially
            Graphics g = ms_status_label.getGraphics();
            Component parent = ms_status_label.getParent();
            if ((g != null) && (parent != null)) {
                Graphics parent_g = parent.getGraphics();
                if (parent_g != null) {
                    g.clearRect(0, 0, 600, 60);
                    parent.paintAll(parent_g);
                }
            }
        }

        Parser parser = new Parser(this);
    
        File file = new File(m_canonical_file_path);
        String file_name = file.getName();
        if (file_name.equals(UDPLoader.UDP_LOADER_FILE_NAME)){
            parser.allowToReadUserDefinedPrimitiveSections(); 
        }

        m_node = parser.read(_format_problem_manager);

        if (m_node != null) {
                // set time stamps to the disk file's time stamp
            ALFINode[] member_nodes = m_node.memberNodes_getAllGenerations();
            for (int i = 0; i < member_nodes.length; i++) {
                member_nodes[i].setTimeLastModified(m_time_last_modified);
            }
            m_node.setTimeLastModified(m_time_last_modified);
        }
        else {
                // error condition
            String error = "(" + this + ").read():\n" +
                "\tERROR: Failure to create root ALFINode.";
            System.err.println(error);
            return;
        }
    }

        /** Write the root node and its hierarchy to file. */
    public void write(boolean _b_make_backup) {
        try {
                // make a backup
            if (_b_make_backup) { writeCurrentFileBackup(); }

            cleanUpBackupDirectory();

                // get the writer object
            FileWriter writer = new FileWriter(m_canonical_file_path);

                // write the file
            Parser parser = new Parser(this);
            String file_contents = parser.createNodeContentsString();
            if (_b_make_backup) {
                System.out.println("Writing " + m_canonical_file_path);
            }
            writer.write(file_contents, 0, file_contents.length());
            writer.flush();
            writer.close();
            setTimeLastModified();
        }
        catch(IOException e) {
            System.err.println("ALFIFile.write():\n\tERROR: Could not write " +
                "file: " + m_canonical_file_path);
        }
    }

        /** 
         * Export the contents for this box file and all the
         * included box files into one file that the modeler uses.
         * This writes a .sim file.
         */
    public void writeModelerFile () {
            // Keep a list of nodes that have already been defined.
        ms_defined_node.clear(); 

        try {
            String file_name = new String(m_canonical_file_path);
            
            int start_index = file_name.lastIndexOf(BOX_NODE_FILE_SUFFIX);
            int end_index = start_index + BOX_NODE_FILE_SUFFIX.length();

            StringBuffer modeler_file = new StringBuffer(file_name).
                replace(start_index, end_index, MODELER_FILE_SUFFIX);
            System.err.println("Writing modeler file: " + modeler_file);

                // get the writer object
            FileWriter writer = new FileWriter(modeler_file.toString());

                // write the file
            Parser parser = new Parser(this);
                
                // write out the definitions of the box files. 
                // The definitions of the box files are written out first.
                // Then the contents of the main box file is read.
            String box_definitions = getBoxDefinitions(m_node, parser);
            String file_contents = parser.createModelerFileContents();

            StringBuffer buffer = new StringBuffer(box_definitions);
            buffer.append(file_contents);
            writer.write(buffer.toString());
            writer.flush();
            writer.close();
            setTimeLastModified();
        }
        catch(IOException e) {
            System.err.println("ALFIFile.write():\n\tERROR: Could not write " +
                "file: " + m_canonical_file_path);
        }
    }

        /** 
         * Recurse through the box files and write out their definitions
         * at the beginning of the .sim file
         */
    private String getBoxDefinitions (ALFINode _node, Parser _parser) {

        ALFINode[] member_nodes = _node.memberNodes_getDirectlyContained();    
        StringBuffer buffer = new StringBuffer();

            // Recurse to the innermost (contained) box file.
        for (int i = 0; i < member_nodes.length; i++){
            buffer.append(getBoxDefinitions(member_nodes[i], _parser));
        }
        
            // Make sure that the definition is written only once.
        if ( _node.isBox() &&
            !ms_defined_node.contains(_node.baseNode_getRoot()) &&
            (_node != m_node)) {
            buffer.append(_parser.getBoxFileDefinition(_node));
            ms_defined_node.add(_node.baseNode_getRoot());
        }

        return buffer.toString();
    }

    private File writeCurrentFileBackup() {
        File backup_dir = Alfi.LOCAL_UTILITY_DIRECTORY;
        String backup_name = generateBackupName(backup_dir,
                                        createAssociatedJavaFile().getName());
        File backup_file = new File(backup_dir, backup_name);
        try { FileIO.copyFile(m_canonical_file_path, backup_file.getPath()); }
        catch(Exception e) {
            System.err.println(e);
            return null;
        }
        return backup_file;
    }
    
    private void cleanUpBackupDirectory() {
        File backup_dir = Alfi.LOCAL_UTILITY_DIRECTORY;
        File backup_files[] = backup_dir.listFiles();

        Calendar save_until = Calendar.getInstance();
        save_until.add(Calendar.DATE, -31);

        // Arrays to save the files less than a year old, latest one per month
        Map file_list = new LinkedHashMap();
        
        for (int i = backup_files.length - 1; i >= 0; i--) {
            String backup_name = backup_files[i].getName();
            int index = backup_name.indexOf(".box-");

            // The index value is where the dot is located 
            if (index > 0) {
                // This means that it's one of the backup files.
                // Check the dates.
                Calendar backup_date = Calendar.getInstance();
                backup_date.setTimeInMillis(backup_files[i].lastModified());

                if (backup_date.before(save_until)){
                    // These files are more than 30 days old
                    Calendar year_old = Calendar.getInstance();
                    year_old.add(Calendar.YEAR, -1);
                    if (backup_date.before(year_old)){
                        backup_files[i].delete();
                    }
                    else {
                        Date b_date = new Date(backup_files[i].lastModified());
                        String root_name = backup_name.substring(0, index);
                        // Check if this file has been added to the list
                        if (!file_list.containsKey(root_name)) {
                            file_list.put(root_name, backup_files[i]);
                        }
                        else {
                            File file_in_list = (File)file_list.get(root_name);
                            Calendar file_in_list_date = Calendar.getInstance();
                            file_in_list_date.
                                setTimeInMillis(file_in_list.lastModified());
                            if (file_in_list_date.before(backup_date)){
                                file_list.remove(root_name);
                                file_in_list.delete();
                                file_list.put(root_name, backup_files[i]);
                            }
                            else {
                                backup_files[i].delete();
                            }
                        }
                    }
                }
                
            }
        }
    }


    private String generateBackupName(File _backup_dir, String _file_name) {
        String name_prefix = _file_name + "-";

        Calendar calendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        name_prefix += year + "_";

        int month = calendar.get(Calendar.MONTH) + 1;
        name_prefix += ((month < 10) ? ("0" + month) : ("" + month)) + "_";

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        name_prefix += ((day < 10) ? ("0" + day) : ("" + day)) + "__";

        int count = 0;
        while (true) {
            String name = name_prefix;
            if (count < 10) { name += "0"; }
            name += count;
            File file = new File(_backup_dir, name);
            if (! file.exists()) {
                return name;
            }
            else { count++; }
        }
    }
        

    void setParseVersion(int[] _parse_version_numbers) {
        for (int i = 0; i < m_parse_version_numbers.length; i++) {
            m_parse_version_numbers[i] = _parse_version_numbers[i];
        }
    }

    public int[] getParseVersion() { return m_parse_version_numbers; }

    public boolean fileVersionEqualOrGreaterThan(int[] _test_version_numbers) {
            // test major release numbers.
        if (m_parse_version_numbers[0] > _test_version_numbers[0]) {
            return true;
        }
        else if (m_parse_version_numbers[0] < _test_version_numbers[0]) {
            return false;
        }

            // same major release.  test minor release numbers.
        if (m_parse_version_numbers[1] > _test_version_numbers[1]) {
            return true;
        }
        else if (m_parse_version_numbers[1] < _test_version_numbers[1]) {
            return false;
        }

            // also same minor release.  test bug-fix numbers.
        if (m_parse_version_numbers[2] >= _test_version_numbers[2]) {
            return true;
        }
        else { return false; }
    }

    public File createAssociatedJavaFile() {
        return new File(m_canonical_file_path);
    }

        /**
         * Tests whether the path is an "absolute (vs. relative) alfi path".
         */
    static boolean isE2EPartialPath(String _pre_resolved_path) {
        if (_pre_resolved_path.startsWith(E2E_PARTIAL_PATH_IDENTIFIER)) {
            return true;
        }
        else { return false; }
    }

        /** Last seen in cvs version 1.26. */
    public void setSourceFile(File _file) {
            Alfi.warn("ALFIFile.setSourceFile() no longer in use.", false);
    }

    public static boolean directoryIsInE2E_PATH(File _directory) {
        StringTokenizer st = new StringTokenizer(Alfi.getE2EPath(),
                                                 File.pathSeparator);
        String directory_path = null;
        try { directory_path = _directory.getCanonicalPath(); }
        catch(IOException e) {
            System.err.println("ALFIFile.directoryIsInE2E_PATH(" + _directory +
                "):\n\tERROR: Couldn't get canonical path for this directory.");
            return false;
        }

        while (st.hasMoreTokens()) {
            try {
                String path = (new File(st.nextToken())).getCanonicalPath();
                if (path.equals(directory_path)) { return true; }
            }
            catch(IOException e) { ; }
        }

        return false;
    }

        /** Determines root node name (same as file name, stripped of suffix.)*/
    public String determineRootNodeName() {
        String file_name = createAssociatedJavaFile().getName();
        int suffix_length = mb_is_primitive ?
            PRIMITIVE_NODE_FILE_SUFFIX.length() : BOX_NODE_FILE_SUFFIX.length();
        return file_name.substring(0, file_name.length() - suffix_length);
    }

    /////////////////////////////////
    // member field access //////////

    public String getCanonicalPath() { return m_canonical_file_path; }

        /**
         * Returns the java.io.File corresponding to the canonical path to
         * the directory this file sits in.
         */
    public File getCanonicalDirectory() {
        return (new File(m_canonical_file_path)).getParentFile();
    }

    public ALFINode getRootNode() { return m_node; }

    public Date getTimeLastModified() { return m_time_last_modified; }

    public boolean setTimeLastModified() {
        m_time_last_modified = new Date();
        return true;
    }

    public boolean setTimeLastModified(Date _time_stamp) {
        m_time_last_modified = (Date) _time_stamp.clone();
        return true;
    }

    public boolean isBoxNode() { return (! isPrimitiveNode()); }

    public boolean isPrimitiveNode() { return mb_is_primitive; }

    public String toString() {
        String info = "ALFIFile " + m_canonical_file_path + " [" +
                m_time_last_modified.getTime() + "])";
        return info;
    }

    public String toString(boolean _b_verbose) {
        if (! _b_verbose) { return toString(); }

        String info = "ALFIFile " + m_canonical_file_path + "\n" +
          "\tm_canonical_file_path: " + m_canonical_file_path + "\n" +
          "\tm_node: " + m_node + "\n" +
          "\tmb_is_primitive: " + mb_is_primitive + "\n" +
          "\tm_time_last_modified: " + m_time_last_modified.getTime();
        return info;
    }

        /** Returns mb_user_loaded.  See mb_user_loaded. */
    public boolean isUserLoadedBox() { return mb_user_loaded; }
}
