package edu.caltech.ligo.alfi.file;

import java.io.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;

import edu.caltech.ligo.alfi.widgets.MemberNodeWidget;


    /** The class which reads/writes ALFINodes from/to disk. */
public class Parser {

    //**************************************************************************
    //***** constants **********************************************************

    public static final String COMMENT_MARKER = "%";
    public static final String ALFI_ONLY_MARKER = COMMENT_MARKER + "*";
    public static final String SECTION_BEGIN_MARKER = "{";
    public static final String SECTION_END_MARKER = "}";
    public static final String FILE_INCLUDE_MARKER = "#include";
    public static final String QUOTE_BEGIN = "#quote_begin";
    public static final String QUOTE_END = "#quote_end";
    public static final String DEFINITION_BEGIN = "#define";
    public static final String DEFINITION_END = "#end";
    public static final String USE_MARKER = "#use";

    public static final String NL = "\n";
    public static final String STANDARD_SPACER = "    ";

    public static final String VERSION_MARKER = "Alfi_Version";
    public static final String OLD_VERSION_MARKER = "Alfi5_Parser_version";

    public static final String USER_DEFINED_PRIMITIVES_MARKER =
                                                     "User_Defined_Primitives";
    public static final String MEMBER_NODE_ADD_MARKER = "Add_Submodules";
    public static final String MEMBER_NODE_REMOVE_MARKER = "Remove_Submodules";
    public static final String MEMBER_NODE_SETTINGS_MARKER = "Settings";
    public static final String PORT_ADD_MARKER = "Port";
    public static final String PORT_REMOVE_MARKER = "Remove_Ports";
    public static final String CONNECTION_ADD_MARKER = "Add_Connections";
    public static final String CONNECTION_REMOVE_MARKER = "Remove_Connections";
    public static final String GUI_CONNECTION_ADD_MARKER= "Add_Connections_gui";
    public static final String GUI_CONNECTION_REMOVE_MARKER =
                                                      "Remove_Connections_gui";
    public static final String JUNCTION_ID_STRING = "__JUNCTION__";
    public static final String JUNCTION_ADD_MARKER = "Add_Junctions";
    public static final String JUNCTION_REMOVE_MARKER = "Remove_Junctions";
    public static final String BUNDLER_ID_STRING = "__BUNDLER__";
    public static final String BUNDLER_ADD_MARKER = "Add_Bundlers";
    public static final String BUNDLER_REMOVE_MARKER = "Remove_Bundlers";
    public static final String BUNDLER_IO_SETTING_MARKER= "Bundler_IO_Settings";
    public static final String PARAMETER_SETTING_MARKER = "_no_marker_in_file_";
    public static final String LINK_DECLARATION_MARKER = "Link_Declarations";
    public static final String LINK_SETTING_MARKER = "Link_Settings";
    public static final String MACRO_MARKER = "Add_Macros";
    public static final String GUI_INFO_MARKER = "GUI_Settings";
    public static final String NODE_DESCRIPTION = "Node_Descriptions";
    public static final String TEXT_COMMENT = "TextComment";
    public static final String COLORED_BOX = "ColoredBox";
    public static final String MEMBER_NODE = "MemberNode";
    public static final String JUNCTION = "Junction";
    public static final String BUNDLER = "Bundler";
    public static final String LAYERED_GROUP = "LayeredGroup";

    public static final String[] MARKERS = {
        USER_DEFINED_PRIMITIVES_MARKER,
        MEMBER_NODE_ADD_MARKER, MEMBER_NODE_REMOVE_MARKER,
        MEMBER_NODE_SETTINGS_MARKER,
        PORT_ADD_MARKER, PORT_REMOVE_MARKER,
        CONNECTION_ADD_MARKER, CONNECTION_REMOVE_MARKER,
        GUI_CONNECTION_ADD_MARKER, GUI_CONNECTION_REMOVE_MARKER,
        JUNCTION_ADD_MARKER, JUNCTION_REMOVE_MARKER,
        BUNDLER_ADD_MARKER, BUNDLER_REMOVE_MARKER, BUNDLER_IO_SETTING_MARKER,
        PARAMETER_SETTING_MARKER, LINK_DECLARATION_MARKER, LINK_SETTING_MARKER,
        MACRO_MARKER, GUI_INFO_MARKER, VERSION_MARKER, OLD_VERSION_MARKER,
        NODE_DESCRIPTION, TEXT_COMMENT, COLORED_BOX, MEMBER_NODE, JUNCTION,
        BUNDLER, LAYERED_GROUP
    };

        // member node aux markers
    public static final String MEMBER_ADD__BOX = "box";

        // port aux markers
    public static final String PORT__DATA_TYPE = "dataType";
    public static final String PORT__DEFAULT = "DefaultValue";
    public static final String PORT__BUNDLE_PORT_DEFAULT_CONTENT =
                                                    "BundlePortDefaultContent";
    public static final String PORT__INTERNAL_ORIENT = "internalOrientation";
    public static final String PORT__EXTERNAL_ORIENT = "externalOrientation";

        // bundler aux markers
    public static final String BUNDLER__SECONDARY_OUTPUT_CHANNEL_NAME =
                                               "SECONDARY_OUTPUT_CHANNEL_NAME";
    public static final String BUNDLER__SECONDARY_INPUT_NAME =
                                                        "SECONDARY_INPUT_NAME";
    public static final String BUNDLER__SECONDARY_INPUT_UNWRAPS =
                                                     "SECONDARY_INPUT_UNWRAPS";

        // bundle aux markers
    public static final String BUNDLE__IS_BUNDLE = "IS_BUNDLE";
    public static final String BUNDLE__IS_BUNDLER_PRIMARY_INPUT =
                                                    "IS_BUNDLER_PRIMARY_INPUT";
    public static final String BUNDLE__IS_BUNDLER_PRIMARY_OUTPUT =
                                                   "IS_BUNDLER_PRIMARY_OUTPUT";

        // gui info aux markers
    public static final String GUI__FRAME_SIZE = "ScreenSize";
    public static final String GUI__PRIMITIVE_GROUP = "Group";
    public static final String GUI__NODE_DISPLAY = "Display";
    public static final String GUI__ICON_INFO = "Icon";
    public static final String GUI__ORIGIN = "Origin";
    public static final String GUI__ROTATION = "Rotation";
    public static final String GUI__SWAP = "Swap";
    public static final String GUI__SYMMETRY = "Symmetry";
    public static final String GUI__CONNECTION = "GUI_Connection";
    public static final String GUI__ICON_ROTATES = "IconRotates";
    public static final String GUI__PORTS_CENTER__EXTERNAL =
                                                         "ExternalPortsCenter";
    public static final String GUI__COLOR = "Color";
    public static final String GUI__LAYER = "Layer";
    public static final String GUI__SIZE  = "Size";
    public static final String GUI__FONT_SIZE = "FontSize";

    private static boolean msb_readUserDefinedPrimitiveSections_has_run = false;

    //**************************************************************************
    //***** private member fields **********************************************

        /** The disk file.  */
    private final ALFIFile m_root_node_file;

        /**
         * Auxilliary reference.  Keeps track of the node currently being read
         * into.
         */
    private ALFINode m_current_container_node;

        /** Field to save file's comment in.  It is stripped out otherwise. */
    private String m_root_node_comment;

        /** Object which deals with warnings about poor formatting in file.
          * Only used in file reads.
          */
    private FileFormatProblemManager m_fpm;

        /** Used to detect cyclic #include errors. */
    static final HashMap ms_includes_map = new HashMap();

        /** Used to detect duplicates in #define tags. */
    static HashMap ms_node_vs_defines_map = new HashMap();

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    Parser(ALFIFile _root_node_file) {
        m_root_node_file = _root_node_file;
        m_current_container_node = null;
        m_fpm = null;

        if (_root_node_file.isBoxNode()) {
            ms_includes_map.put(m_root_node_file.getCanonicalPath(),
                                                       new HashMap());
        }
    }

    //**************************************************************************
    //***** main methods *******************************************************

        /**
         * Parses a root node's file on disk and creates necessary hierarchy of
         * ALFINodes and their associated components.  Returns the root node.
         */
    ALFINode read(FileFormatProblemManager _format_problem_manager) throws
                                              AlfiIncludeFileNotFoundException,
                                              CyclicAlfiFileIncludeException,
                                              FileLoadingCanceledException {
        String[] lines = getFileContents();
        if (lines == null) { return null; }

        m_fpm = _format_problem_manager;

        if (m_root_node_file.isBoxNode()) {
                // create base box nodes referred to by #include statements
            processBoxIncludes(lines);
        }

        String node_type = m_root_node_file.determineRootNodeName();
        ALFINode node_to_edit = null;
        if (node_type.startsWith("FUNC_X")) {
            node_to_edit = new FUNCX_Node(m_root_node_file);
        }
        else { 
            node_to_edit = new ALFINode(m_root_node_file); 
        }

        read(node_to_edit, lines);

            // comment was stripped out of lines, so add it now.
        node_to_edit.comment_addLocal(m_root_node_comment);

        return node_to_edit;
    }

        /**
         * This is the main read routine which takes in a set of settings and
         * uses them to edit the passed in ALFINode.
         */
    private void read(ALFINode _node_to_edit, String[] _lines) throws
                                             FileLoadingCanceledException,
                                             AlfiIncludeFileNotFoundException {
            // disable time stamp mods until read is finished (beware that this
            // setting might be temporarily overridden by special cases, but in
            // general, no time stamps are modified herein.)
        _node_to_edit.timeStamp_enableUpdates(false);

            // get the node comment if it exists and strip it out
        Object[] comment_and_remaining_lines = stripNodeComment(_lines);
        String node_comment = (String) comment_and_remaining_lines[0];
        _lines = (String[]) comment_and_remaining_lines[1];

        _node_to_edit.comment_addLocal(node_comment);

        try {
                // in order to create elements in a nice order (such as ports
                // before connections, etc.) sections_vector_map will contain a
                // vector of section lines for each type of element (or null if
                // not used).  After the intial allocation of sections to the
                // respective vectors, then the sections will be parsed.
            HashMap sections_vector_map = slotSectionsIntoVectorHolders(_lines);

            // the following order matters!! //////////

            Vector sections = null;

                // first get the file's parser version
            sections = (Vector) sections_vector_map.get(VERSION_MARKER);
            if (sections == null) {
                sections = (Vector) sections_vector_map.get(OLD_VERSION_MARKER);
            }
            if (sections != null) {
                int[] version_numbers = readVersionSection(sections);
                m_root_node_file.setParseVersion(version_numbers);
            }

                // create base nodes for user defined primitives
            sections =
                (Vector)sections_vector_map.get(USER_DEFINED_PRIMITIVES_MARKER);
            if (sections != null) {
                readUserDefinedPrimitiveSections(sections);
            }

                // insert parameter declarationss (root primitives only)
            for (int i=0; i < ParameterDeclaration.PARAMETER_TYPES.length;i++) {
                sections = (Vector) sections_vector_map.get(
                                       ParameterDeclaration.PARAMETER_TYPES[i]);
                if (sections != null) {
                    readParameterDeclarationSections(_node_to_edit, sections);
                }
            }
            _node_to_edit.parameterDeclaration_addIncludePath();

                // next insert parameter mods (primitive instance nodes only)
            sections=(Vector) sections_vector_map.get(PARAMETER_SETTING_MARKER);
            if (sections != null) {
                readParameterModSections(_node_to_edit, sections);
            }

                // next insert macros (boxes only)
            sections = (Vector) sections_vector_map.get(MACRO_MARKER);
            if (sections != null) {
                readMacroSections(_node_to_edit, sections);
            }

                // next add ports
            sections = (Vector) sections_vector_map.get(PORT_ADD_MARKER);
            if (sections != null) {
                readPortAddSections(_node_to_edit, sections);
            }

                // next add junctions
            sections = (Vector) sections_vector_map.get(JUNCTION_ADD_MARKER);
            if (sections != null) {
                readJunctionAddSections(_node_to_edit, sections);
            }

                // next add bundlers
            sections = (Vector) sections_vector_map.get(BUNDLER_ADD_MARKER);
            if (sections != null) {
                readBundlerAddSections(_node_to_edit, sections);
            }

                // next read bundler io settings for inherited bundlers (only)
            sections = (Vector) sections_vector_map.get(
                                                    BUNDLER_IO_SETTING_MARKER);
            if (sections != null) {
                readBundlerIOSettingSections(_node_to_edit, sections);
            }

                // next add member nodes
            sections = (Vector) sections_vector_map.get(MEMBER_NODE_ADD_MARKER);
            if (sections != null) {
                readMemberAddSections(_node_to_edit, sections);
            }

                // next process the settings sections for member nodes.
                //   must be done before connections are added as local ports
                //   may be added to member nodes herein.
            sections = (Vector) sections_vector_map.get(
                                             MEMBER_NODE_SETTINGS_MARKER);
            if (sections != null) {
                ALFINode container_node = _node_to_edit;
                readSettingsSections(container_node, sections);
            }

                // next remove connections
            sections = (Vector) sections_vector_map.get(
                                               GUI_CONNECTION_REMOVE_MARKER);
            if (sections != null) {
                readGuiConnectionRemoveSections(_node_to_edit, sections);
            }

                // next add connections
            sections = (Vector) sections_vector_map.get(
                                               GUI_CONNECTION_ADD_MARKER);
            if (sections != null) {
                readGuiConnectionAddSections(_node_to_edit, sections);
            }

                // next remove member nodes
                // must be after connection removes at the least
            sections = (Vector) sections_vector_map.get(
                                                 MEMBER_NODE_REMOVE_MARKER);
            if (sections != null) {
                readMemberRemoveSections(_node_to_edit, sections);
            }

                // next add gui info
            sections = (Vector) sections_vector_map.get(GUI_INFO_MARKER);
            if (sections != null) {
                readGUISettingsSections(_node_to_edit, sections);
            }

                // next add GUI node description
            sections = (Vector) sections_vector_map.get(NODE_DESCRIPTION);
            if (sections != null) {
                readNodeDescriptionSections(_node_to_edit, sections);
            }

                // add parameter link declarations
            sections = (Vector) sections_vector_map.get(
                                                      LINK_DECLARATION_MARKER);
            if (sections != null) {
                readLinkDeclarationSections(_node_to_edit, sections);
            }

                // add parameter link settings
            sections = (Vector) sections_vector_map.get(LINK_SETTING_MARKER);
            if (sections != null) {
                readLinkSettingSections(_node_to_edit, sections);
            }

                // next add text comments
            sections = (Vector) sections_vector_map.get(TEXT_COMMENT);
            if (sections != null) {
                readTextCommentSections(_node_to_edit, sections);
            }

                // next add ColoredBox 
            sections = (Vector) sections_vector_map.get(COLORED_BOX);
            if (sections != null) {
                readColoredBoxSections(_node_to_edit, sections);
            }

                // next add LayeredGroup
            sections = (Vector) sections_vector_map.get(LAYERED_GROUP);
            if (sections != null) {
                readLayeredGroupSections(_node_to_edit, sections);
            }

        }
        catch(AlfiFileFormatException e) {
            Alfi.getMessenger().inform(e);
        }

            // re-enable time stamp mods
        _node_to_edit.timeStamp_enableUpdates(true);
    }

        /** This method maps all the modeler connections which should be in
          * each node vs the node.  The returned HashMap is keyed by ALFINodes
          * and the values are HashSet[3] = { full list of modeler connections,
          * diffs from base adds, diffs from base removes }.
          */
    private HashMap createModelerConnectionMap() {
        HashMap modeler_connection_map = new HashMap();

        ALFINode root_node = m_root_node_file.getRootNode();
        addToModelerConnectionMap(root_node, modeler_connection_map);

        return modeler_connection_map;
    }

    private void addToModelerConnectionMap(ALFINode _node,
                                    HashMap _existing_modeler_connection_map) {
        Object o = _existing_modeler_connection_map.get(_node);

            // this node has already been fully processed?
        if (o instanceof HashSet[]) { return; }

            // process member boxes first
        ALFINode[] member_nodes = _node.memberNodes_getDirectlyContained();
        for (int i = 0; i < member_nodes.length; i++) {
            if (member_nodes[i].isBox()) {
                addToModelerConnectionMap(member_nodes[i],
                                             _existing_modeler_connection_map);
            }
        }

            // does this node have any local changes or bundle content diffs?
        HashSet connection_set = null;
        HashSet adds = null;
        HashSet removes = null;
        if (_node.mayHaveModelerConnectionChanges()) {
                // _node was previously partially processed?
            connection_set = (o instanceof HashSet) ? (HashSet) o :
                                   _node.generateAllModelerConnectionsInNode();

                // to generate the add/remove diff sets, we need the base's cons
            HashSet base_connection_set = null;
            ALFINode base_node = _node.baseNode_getDirect();
            if (base_node != null) {
                o = _existing_modeler_connection_map.get(base_node);
                if (o == null) {
                    base_connection_set =
                               base_node.generateAllModelerConnectionsInNode();
                        // save for future use if needed
                    _existing_modeler_connection_map.put(base_node,
                                                          base_connection_set);
                }
                else if (o instanceof HashSet) {
                        // was determined previously and saved
                    base_connection_set = (HashSet) o;
                }
                else if (o instanceof HashSet[]) {
                        // base node was fully processed previously
                    base_connection_set = ((HashSet[]) o)[0];
                }
                else {
                        // no other valid possibilites.  error.
                    System.err.println("Parser.addToModelerConnectionMap(" +
                                                        _node + ", . . .):\n" +
                        "\tError 1.");
                }
            }
            else { base_connection_set = new HashSet(); }

            if (base_connection_set.isEmpty()) {
                adds = connection_set;
            }
            else {
                adds = new HashSet(connection_set);
                adds.removeAll(base_connection_set);
                if (adds.isEmpty()) { adds = null; }

                removes = new HashSet(base_connection_set);
                removes.removeAll(connection_set);
                if (removes.isEmpty()) { removes = null; }
            }
        }

        HashSet[] sets = { connection_set, adds, removes };
        _existing_modeler_connection_map.put(_node, sets);
    }

        /** Creates the string describing the root container node and its
          * associated container hierarchy.  This is the text that will be
          * written to file on a save.
          */
    String createNodeContentsString() {
        StringBuffer buffer = new StringBuffer();

            // write the parse version
        buffer.append(getVersionSignature() + NL);

        buffer.append(createNodeContentsString(m_root_node_file.getRootNode()));

        return buffer.toString();
    }

    private static String createNodeContentsString (ALFINode _node) {
        return createNodeContentsString(_node, true);
    }

        /**
         * Writes the contents of the node either for a box file 
         * or a .sim file
         */
    private static String createNodeContentsString (ALFINode _node,
                                                     boolean _b_for_box_file) {
        return createNodeContentsString(_node, _b_for_box_file, "");
    }

        /** Writes the content of _node.  The order of some of these sections
          * does matter.
          */
    private static String createNodeContentsString (ALFINode _node,
                               boolean _b_for_box_file, String _space_indent) {
        return createNodeContentsString(_node, _b_for_box_file, false, "");
    }

        /** Writes the content of _node.  The order of some of these sections
          * does matter.  _b_format_for_UDP_section is used to flag a string
          * description of a node to be written inside a UserDefinedPrimitive
          * section, in which case some of the usually alfi-only information
          * needs to be "exposed" (not alfi-only commented) to the modeler
          * parser.
          */
    public static String createNodeContentsString (ALFINode _node,
                    boolean _b_for_box_file, boolean _b_format_for_UDP_section,
                    String _space_indent) {
        int container_depth = _node.getContainmentDepth();
        StringBuffer buffer = new StringBuffer();

            // prepare spacers
        String spacer = _space_indent;
        String alfi_only_spacer = ALFI_ONLY_MARKER;

        for (int i = 0; i < container_depth; i++) { spacer += STANDARD_SPACER; }
        if (alfi_only_spacer.length() < spacer.length()) {
            alfi_only_spacer += spacer.substring(alfi_only_spacer.length());
        }

        if (_b_for_box_file || (_node instanceof UserDefinedPrimitive)) {
            // write comment
            String comment = _node.comment_getLocal();
            if ((comment != null) && (comment.length() > 0)) {
                StringTokenizer st = new StringTokenizer(comment, NL, true);
                while (st.hasMoreTokens()) {
                    String token = st.nextToken();
                    if (! token.equals(NL)) {
                        buffer.append(spacer + COMMENT_MARKER + " " + token);
                        if (st.hasMoreTokens()) { 
                            buffer.append(st.nextToken()); 
                        }
                    }
                    else { buffer.append(spacer + COMMENT_MARKER + NL); }
                }
                buffer.append(NL);
            }
        }

            // write UserDefinedPrimitive includes (root container box only)
            // At startup, this section is used to read in all the UDPs in
            // the E2E_PATH via a temporary dynamically generated "box" file,
            // but after this file is read, all subsequent box files read
            // ignore this section, but the modeler still requires it.
        if (_node.isRootNode()) {
            ALFINode[] all_members = _node.memberNodes_getAllGenerations();
            HashMap udp_map = new HashMap();
            for (int i = 0; i < all_members.length; i++) {
                if (all_members[i] instanceof UserDefinedPrimitive) {
                    udp_map.put(all_members[i].baseNode_getRoot(), null);
                }
            }

            if (! udp_map.isEmpty()) {
//
//                  // make sure udp files will be written to a known location
//              File box_directory =
//                               _node.getSourceFile().getCanonicalDirectory();
//              File wd = Alfi.WORKING_DIRECTORY;
//              if (! box_directory.equals(wd)) {
//                  warnUserWhereUdpFileWillBeWritten(_node, box_directory, wd);
//              }
//
                buffer.append(spacer);
                buffer.append(USER_DEFINED_PRIMITIVES_MARKER);
                buffer.append(NL);
                buffer.append(spacer);
                buffer.append(SECTION_BEGIN_MARKER);

                String spacer_inside = spacer + STANDARD_SPACER;
                for (Iterator i = udp_map.keySet().iterator(); i.hasNext(); ) {
                    UserDefinedPrimitive root = (UserDefinedPrimitive) i.next();
                    String udp_name = root.determineLocalName();
                    String udp_file_name = udp_name + ALFIFile.UDP_FILE_SUFFIX;
                    buffer.append(NL);
                    buffer.append(spacer_inside);
                    buffer.append(FILE_INCLUDE_MARKER);
                    buffer.append(" ");
                    buffer.append(ALFIFile.E2E_PARTIAL_PATH_IDENTIFIER);
                    buffer.append(udp_file_name);

//
//                  StringBuffer udp_file_buffer = new StringBuffer(udp_name);
//                  udp_file_buffer.append(NL);
//                  udp_file_buffer.append(SECTION_BEGIN_MARKER);
//                  udp_file_buffer.append(NL);
//
//                  String description = createNodeContentsString(root,
//                                true, true, STANDARD_SPACER);
//                  udp_file_buffer.append(description);
//                  udp_file_buffer.append(SECTION_END_MARKER);
//
//                      // write the .udp file
//                  try {
//                      FileWriter udp_writer = new FileWriter(udp_file_name);
//                      System.out.println("Writing " +
//                               Alfi.WORKING_DIRECTORY + "/" + udp_file_name);
//                      String udp_file_string = udp_file_buffer.toString();
//                      udp_writer.write(udp_file_string, 0,
//                                                   udp_file_string.length());
//                      udp_writer.flush();
//                      udp_writer.close();
//                  }
//                  catch(IOException _e) {
//                      System.err.println(
//                          "Parser.createNodeContentsString(...):\n" +
//                          "\tProblem occured when attempting to write " +
//                          udp_file_name + ".");
//                  }
//
                }

                buffer.append(NL);
                buffer.append(spacer);
                buffer.append(SECTION_END_MARKER);
                buffer.append(NL);
            }
        }

            // write parameter declarations (used only when writing a user
            // defined primitive.  otherwise these only exist in .prm files.)
        if (_node.isRootNode() && (_node instanceof UserDefinedPrimitive)) {
            ParameterDeclaration[] declarations =
                                        _node.parameterDeclarations_getLocal();
            for (int i = 0; i < declarations.length; i++) {
                String description =
                          declarations[i].generateParserRepresentation(spacer);
                buffer.append(description + NL);
            }
        }

            // write parameter settings (primitives only)
            //
            // important note: it is possible that ParameterSettings exist in
            // primitive root nodes for UDPs, but these are only used internally
            // in alfi and are not written out to the box file.  i.e., such
            // ParameterSetting objects are ignored here.
        if (!_node.isRootNode()) {
            ParameterSetting[] settings = _node.parameterSettings_getLocal();
            for (int i = 0; i < settings.length; i++) {
                String setting_string =
                                    settings[i].generateParserRepresentation();
                if (setting_string != null) {
                    buffer.append(spacer + setting_string);
                    buffer.append(NL);
                }
            }
        }

            // write macros (boxes only)
        Macro macro = _node.macro_getLocal();
        if (macro != null) {
            buffer.append(spacer + MACRO_MARKER  + NL);
            buffer.append(spacer + SECTION_BEGIN_MARKER + NL);

            buffer.append(macro.generateParserRepresentation(spacer +
                                                             STANDARD_SPACER));
            buffer.append(NL);

            buffer.append(spacer + SECTION_END_MARKER + NL);
        }

            // get data used for writing modeler connections
        HashSet[] modeler_connection_info =
                  (_node.isBox()) ?  _node.modelerConnectionCache_get() : null;
        HashSet modeler_add_connections_set = null;
        HashSet modeler_remove_connections_set = null;
        if (modeler_connection_info != null) {
            modeler_add_connections_set = modeler_connection_info[1];
            modeler_remove_connections_set = modeler_connection_info[2];
        }

            // write Remove_Connections section
        if (modeler_remove_connections_set != null) {
            buffer.append(spacer + CONNECTION_REMOVE_MARKER);
            if (_b_for_box_file) {
                buffer.append("    % Modeler only section (ignored by Alfi.)");
            }
            buffer.append(NL);
            buffer.append(spacer + SECTION_BEGIN_MARKER + NL);
            for (Iterator i = modeler_remove_connections_set.iterator();
                                                                i.hasNext();) {
                buffer.append(spacer + STANDARD_SPACER);
                buffer.append(i.next());
                buffer.append(NL);
            }

            buffer.append(spacer + SECTION_END_MARKER + NL);
        }

            // write gui version of Remove_Connections
        if (_b_for_box_file) {
            ConnectionProxy[] connections = _node.connections_getLocalRemoves();
            if (connections.length > 0) {
                buffer.append(alfi_only_spacer +
                                            GUI_CONNECTION_REMOVE_MARKER + NL);
                buffer.append(alfi_only_spacer + SECTION_BEGIN_MARKER + NL);
                for (int i = 0; i < connections.length; i++) {
                    buffer.append(connections[i].
                            generateParserRepresentation_guiInfo(
                                    alfi_only_spacer + STANDARD_SPACER, false));
                    buffer.append(NL);
                }
                buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
            }
        }

            // write Remove_Submodules section
        MemberNodeProxy[] mnps = _node.memberNodeProxies_getLocalRemoves();
        ALFINode[] removed_nodes = new ALFINode[mnps.length];
        for (int i = 0; i < removed_nodes.length; i++) {
            removed_nodes[i] = mnps[i].getDirectlyAssociatedMemberNode();
        }
        if (removed_nodes.length > 0) {
            buffer.append(spacer + MEMBER_NODE_REMOVE_MARKER + NL);
            buffer.append(spacer + SECTION_BEGIN_MARKER + NL);
            for (int i = 0; i < removed_nodes.length; i++) {
                buffer.append(spacer + STANDARD_SPACER);
                buffer.append(removed_nodes[i].determineLocalName() + NL);
            }
            buffer.append(spacer + SECTION_END_MARKER + NL);
        }

            // write Add_Submodules section
        mnps = _node.memberNodeProxies_getLocal();
        ALFINode[] added_nodes = new ALFINode[mnps.length];
        for (int i = 0; i < added_nodes.length; i++) {
            added_nodes[i] = mnps[i].getDirectlyAssociatedMemberNode();
        }
        if (added_nodes.length > 0) {
            buffer.append(spacer + MEMBER_NODE_ADD_MARKER + NL);
            
            buffer.append(spacer + SECTION_BEGIN_MARKER + NL);
            for (int i = 0; i < added_nodes.length; i++) {
                buffer.append(spacer + STANDARD_SPACER);
                if (added_nodes[i].isPrimitive()) {
                    String type =
                        added_nodes[i].baseNode_getRoot().determineLocalName();
                    String name = added_nodes[i].determineLocalName();
                    buffer.append(type + " " + name + NL);
                 }
                else {
                    if (_b_for_box_file) {
                        buffer.append(attachIncludePath(added_nodes[i])); 
                    }
                    else {
                        buffer.append(attachBoxFileContents(added_nodes[i]));
                    } 
                }
            }
            buffer.append(spacer + SECTION_END_MARKER + NL);
        }

            // write "Settings X" for locally added nodes
        for (int i = 0; i < added_nodes.length; i++) {
            String settings_string = createNodeContentsString(added_nodes[i],
                                                      _b_for_box_file, spacer);

            if (settings_string.length() > 0) {
                buffer.append(spacer + MEMBER_NODE_SETTINGS_MARKER + " " +
                                     added_nodes[i].determineLocalName() + NL);
                buffer.append(spacer + SECTION_BEGIN_MARKER + NL);

                    // recursive call for the contained node
                buffer.append(settings_string);

                buffer.append(spacer + SECTION_END_MARKER + NL);
            }
        }

            // write "Settings X" for inherited nodes with changes
        mnps = _node.memberNodeProxies_getInherited();
        for (int i = 0; i < mnps.length; i++) {
                // get the associated member node inside _node (the container)
            ALFINode member_node = mnps[i].getAssociatedMemberNode(_node);

                // bundle content may generate modeler connection adds/removes
            HashSet[] member_modeler_connection_info = (member_node.isBox()) ?
                               member_node.modelerConnectionCache_get() : null;
            if (member_node.isModifiedFromDirectBase(true) ||
                         (member_node.isBox() &&
                             ((member_modeler_connection_info[1] != null) ||
                              (member_modeler_connection_info[2] != null)) )) {
                buffer.append(spacer + MEMBER_NODE_SETTINGS_MARKER + " " +
                                        member_node.determineLocalName() + NL);
                buffer.append(spacer + SECTION_BEGIN_MARKER + NL);

                    // recursive call for the contained node
                buffer.append(createNodeContentsString(member_node,
                                                     _b_for_box_file, spacer));

                buffer.append(spacer + SECTION_END_MARKER + NL);
            }
        }

            // write Add_Connections section
        if (modeler_add_connections_set != null) {
            buffer.append(spacer + CONNECTION_ADD_MARKER);
            if (_b_for_box_file) {
                buffer.append("    % Modeler only (ignored by Alfi.)");
            }
            buffer.append(NL);
            buffer.append(spacer + SECTION_BEGIN_MARKER + NL);
            for (Iterator i = modeler_add_connections_set.iterator();
                                                               i.hasNext(); ) {
                buffer.append(spacer + STANDARD_SPACER);
                buffer.append(i.next());
                buffer.append(NL);
            }

            buffer.append(spacer);
            buffer.append(SECTION_END_MARKER);
            buffer.append(NL);
        }

            // write gui version of Add_Connections
        ConnectionProxy[] connections = _node.connections_getLocal();
        if (_b_for_box_file && (connections.length > 0)) {
            buffer.append(alfi_only_spacer + GUI_CONNECTION_ADD_MARKER + NL);
            buffer.append(alfi_only_spacer + SECTION_BEGIN_MARKER + NL);
            for (int i = 0; i < connections.length; i++) {
                buffer.append(
                        connections[i].generateParserRepresentation_guiInfo(
                                     alfi_only_spacer + STANDARD_SPACER, true));
                buffer.append(NL);
            }
            buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
        }


            // currently the port remove list is ignored and not written to file


        if (_b_for_box_file || (_node instanceof UserDefinedPrimitive)) {
                // write Port sections
            PortProxy[] ports = _node.ports_getLocal();
            for (int i = 0; i < ports.length; i++) {
                String port_name = ports[i].getName();
                int io_type = ports[i].getIOType();
                int data_type = ports[i].getDataType();
                HashMap default_bundle_channels = null;
                if ( (data_type == GenericPortProxy.DATA_TYPE_ALFI_BUNDLE) &&
                                        (io_type == PortProxy.IO_TYPE_INPUT) &&
                                        _node.isRootNode() ) {
                    default_bundle_channels =
                             ((BundlePortProxy) ports[i]).getDefaultChannels();
                }

                    // in user defined primitive sections, some things need to
                    // be exposed to the modeler parser, so the usual
                    // alfi_only_spacer is replaced by the non-comment spacer
                String spacer_pick =
                       (_b_format_for_UDP_section ? spacer : alfi_only_spacer);
                buffer.append(spacer_pick + PORT_ADD_MARKER + " ");
                buffer.append(PortProxy.IO_TYPE_BY_STRING_ARRAY[io_type]);
                buffer.append(" " + port_name + NL);
                buffer.append(spacer_pick + SECTION_BEGIN_MARKER + NL);
                buffer.append(spacer_pick + STANDARD_SPACER);
                buffer.append(PORT__DATA_TYPE + " = " +
                               PortProxy.DATA_TYPE_BY_STRING_ARRAY[data_type]);
                buffer.append(NL);

                    // bundle input ports on root nodes only
                if ((default_bundle_channels != null) &&
                                        (! default_bundle_channels.isEmpty())) {
                    buffer.append(alfi_only_spacer + STANDARD_SPACER +
                                       PORT__BUNDLE_PORT_DEFAULT_CONTENT + NL);
                    buffer.append(alfi_only_spacer + STANDARD_SPACER +
                                                    SECTION_BEGIN_MARKER + NL);
                    for (Iterator j=default_bundle_channels.keySet().iterator();
                                                               j.hasNext(); ) {
                        String channel_path = (String) j.next();
                        data_type = ((Integer) default_bundle_channels.
                                                 get(channel_path)).intValue();
                        String data_type_string =
                                PortProxy.DATA_TYPE_BY_STRING_ARRAY[data_type];

                        buffer.append(alfi_only_spacer + STANDARD_SPACER +
                                         STANDARD_SPACER + channel_path +
                                         " " + data_type_string + NL);
                    }
                    buffer.append(alfi_only_spacer + STANDARD_SPACER +
                                                      SECTION_END_MARKER + NL);
                }

                    // port positioning info
                int[] position = ports[i].getPosition();
                buffer.append(alfi_only_spacer + STANDARD_SPACER);
                buffer.append(PORT__INTERNAL_ORIENT + " " +
                   PortProxy.EDGE_BY_STRING_ARRAY[position[0]] +" "+ 
                                                                 position[1]);
                buffer.append(NL);
    
                buffer.append(alfi_only_spacer + STANDARD_SPACER);
                buffer.append(PORT__EXTERNAL_ORIENT + " " +
                   PortProxy.EDGE_BY_STRING_ARRAY[position[2]] +" "+ 
                                                                 position[3]);
                buffer.append(NL);
    
                buffer.append(spacer_pick + SECTION_END_MARKER + NL);
            }

                // write junction adds
            JunctionProxy[] junctions = _node.junctions_getLocal();
            if (junctions.length > 0) {
                buffer.append(alfi_only_spacer + JUNCTION_ADD_MARKER + NL);
                buffer.append(alfi_only_spacer + SECTION_BEGIN_MARKER + NL);
                for (int i = 0; i < junctions.length; i++) {
                    buffer.append(alfi_only_spacer + STANDARD_SPACER);
                    buffer.append(junctions[i].generateParserRepresentation());
                    buffer.append(NL);
                }
                buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
            }

                // write bundler adds
            BundlerProxy[] bundlers = _node.bundlers_getLocal();
            if (bundlers.length > 0) {
                buffer.append(alfi_only_spacer + BUNDLER_ADD_MARKER + NL);
                buffer.append(alfi_only_spacer + SECTION_BEGIN_MARKER + NL);
                String starting_spacer = alfi_only_spacer + STANDARD_SPACER;
                for (int i = 0; i < bundlers.length; i++) {
                    buffer.append(bundlers[i].generateParserRepresentation(
                                                      _node, starting_spacer));
                    buffer.append(NL);
                }
                buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
            }

                // write bundler io settings for inherited bundlers
            BundlerIOSetting[] io_settings = _node.bundlerSettings_getLocal();
            ArrayList settings_for_inherited_bundlers = new ArrayList();
            for (int i = 0; i < io_settings.length; i++) {
                BundlerProxy bundler = io_settings[i].getAssociatedBundler();
                if (_node.bundler_containsInherited(bundler)) {
                    settings_for_inherited_bundlers.add(io_settings[i]);
                }
            }
            if (! settings_for_inherited_bundlers.isEmpty()) {
                buffer.append(alfi_only_spacer + BUNDLER_IO_SETTING_MARKER +NL);
                buffer.append(alfi_only_spacer + SECTION_BEGIN_MARKER + NL);
                String starting_spacer = alfi_only_spacer + STANDARD_SPACER;
                for (Iterator i = settings_for_inherited_bundlers.iterator();
                                                               i.hasNext(); ) {
                    BundlerIOSetting io_setting = (BundlerIOSetting) i.next();
                    buffer.append(io_setting.generateParserRepresentation(
                                                             starting_spacer));
                    buffer.append(NL);
                }
                buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
            }

                // write parameter link declarations
            LinkDeclaration[] link_decs = _node.linkDeclarations_getLocal();
            if (link_decs.length > 0) {
                buffer.append(alfi_only_spacer + LINK_DECLARATION_MARKER + NL);
                buffer.append(alfi_only_spacer+ SECTION_BEGIN_MARKER+ NL);
                String starting_spacer = alfi_only_spacer + STANDARD_SPACER;
                for (int i = 0; i < link_decs.length; i++) {
                    buffer.append(link_decs[i].generateParserRepresentation(
                                                             starting_spacer));
                    buffer.append(NL);
                }
                buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
            }

                // write parameter link settings
            LinkSetting[] link_settings = _node.linkSettings_getLocal();
            if (link_settings.length > 0) {
                buffer.append(alfi_only_spacer + LINK_SETTING_MARKER+ NL);
                buffer.append(alfi_only_spacer+ SECTION_BEGIN_MARKER+ NL);
                String starting_spacer = alfi_only_spacer + STANDARD_SPACER;
                for (int i = 0; i < link_settings.length; i++) {
                    buffer.append(link_settings[i].generateParserRepresentation(
                                                             starting_spacer));
                    buffer.append(NL);
                }
                buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
            }

                // write TextComment section . . .
            TextComment[] comments = _node.textComments_getLocal();

            for (int i = 0; i < comments.length; i++) {
                String name = comments[i].getName();
                
                buffer.append(alfi_only_spacer + TEXT_COMMENT + " " + name+ NL);
                buffer.append(alfi_only_spacer + SECTION_BEGIN_MARKER + NL);
                
                String comment_value = comments[i].getCommentValue();
                if (comment_value != null) {
                    StringTokenizer st =
                                  new StringTokenizer(comment_value, NL, true);
                    while (st.hasMoreTokens()) {
                        String token = st.nextToken();
                        if (! token.equals(NL)) {
                            buffer.append(alfi_only_spacer + STANDARD_SPACER);
                            buffer.append(token + NL); 
                        }
                    }
                }
                
                buffer.append(alfi_only_spacer + STANDARD_SPACER);
                buffer.append(GUI_INFO_MARKER + NL);
                
                String pad = alfi_only_spacer + STANDARD_SPACER;
                buffer.append(pad + SECTION_BEGIN_MARKER + NL );
        
                Point p = comments[i].getLocation();
                buffer.append(pad + STANDARD_SPACER);
                buffer.append(GUI__ORIGIN + " " + p.x + "x"+ p.y + NL );
                
                int font_size = comments[i].getFontSize();
                buffer.append(pad + STANDARD_SPACER);
                buffer.append(GUI__FONT_SIZE + " "+ font_size + NL);

                int color_id = comments[i].getColor();
                String color = AlfiColors.getColorString(color_id);
                buffer.append(pad + STANDARD_SPACER);
                buffer.append(GUI__COLOR + " "+ color + NL);
    
                if (comments[i].isBold()) {
                    buffer.append(pad + STANDARD_SPACER);
                    buffer.append(AlfiFonts.FONT_STYLE_BOLD + NL);
                }
                if (comments[i].isItalic()) {
                    buffer.append(pad + STANDARD_SPACER);
                    buffer.append(AlfiFonts.FONT_STYLE_ITALIC + NL);
                }
                if (comments[i].isUnderlined()) {
                    buffer.append(pad + STANDARD_SPACER);
                    buffer.append(AlfiFonts.FONT_STYLE_UNDERLINED + NL);
                }

                buffer.append(pad + SECTION_END_MARKER + NL);
                buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
            }

                // write ColoredBox section . . .
            ColoredBox[] colored_box = _node.coloredBoxes_getLocal();

            for (int i = 0; i < colored_box.length; i++) {
                String name = colored_box[i].getName();
                
                buffer.append(alfi_only_spacer + COLORED_BOX + " " + name + NL);
                buffer.append(alfi_only_spacer + SECTION_BEGIN_MARKER + NL);
                
                String pad = alfi_only_spacer + STANDARD_SPACER;
                String color = 
                    AlfiColors.getColorString(colored_box[i].getColor());
                buffer.append(pad + GUI__COLOR + " " + color + NL );

                Point p = colored_box[i].getLocation();
                buffer.append(pad + GUI__ORIGIN + " " );
                buffer.append(p.x + "x"+ p.y + NL );
                
                Dimension sz = colored_box[i].getSize();
                buffer.append(pad + GUI__SIZE + " ");
                buffer.append(sz.width + "x"+ sz.height+ NL );

                int layer = colored_box[i].getLayer();
                if (layer > 0) {
                    buffer.append(pad + GUI__LAYER + " ");
                    buffer.append(ColoredBox.getStringFromLayerId(layer) + NL); 
                }
                buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
            }

                // write LayeredGroup section . . .
            LayeredGroup[] layered_group = _node.layeredGroups_getLocal();

            for (int i = 0; i < layered_group.length; i++) {
                String name = layered_group[i].getName();
                int num_elements = layered_group[i].numElements();
                if (num_elements > 0) {
                    buffer.append(alfi_only_spacer + LAYERED_GROUP + " " + 
                                                                    name + NL);
                    buffer.append(alfi_only_spacer + SECTION_BEGIN_MARKER + NL);
                    String pad = alfi_only_spacer + STANDARD_SPACER;
                    for (int j = 0; j < num_elements; j++) {
                        GroupableIF element = layered_group[i].getElement(j);
                        if (element instanceof TextComment) {
                            TextComment comment = ( TextComment )element;
                            buffer.append(pad + TEXT_COMMENT + " ");
                            buffer.append(comment.getName() + NL);
                        }
                        else if (element instanceof ColoredBox) {
                            ColoredBox bg_color = ( ColoredBox )element;
                            buffer.append(pad + COLORED_BOX + " ");
                            buffer.append(bg_color.getName() + NL);
                        }
                        else if (element instanceof JunctionProxy) {
                            JunctionProxy junction = ( JunctionProxy )element;
                            buffer.append(pad + JUNCTION + " ");
                            buffer.append(junction.getName() + NL);
                        }
                        else if (element instanceof BundlerProxy) {
                            BundlerProxy bundler = ( BundlerProxy )element;
                            buffer.append(pad + BUNDLER + " ");
                            buffer.append(bundler.getName() + NL);
                        }
                        else if (element instanceof ALFINode) {
                            ALFINode node = ( ALFINode )element;
                            buffer.append(pad + MEMBER_NODE + " ");
                            buffer.append(node.determineLocalName() + NL);
                        }
                    }
                    
                    buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
                }
            }
                
                // write GUI_Settings section . . .
            buffer.append(alfi_only_spacer + GUI_INFO_MARKER + NL);
            buffer.append(alfi_only_spacer + SECTION_BEGIN_MARKER + NL);

                // information about associated proxy in its container
            MemberNodeProxy proxy = _node.memberNodeProxy_getAssociated();
                // this info is only appropriate for locally added nodes
            if ((proxy != null) && 
                _node.containerNode_get().
                    memberNodeProxy_containsLocal(proxy)) {
                String pad = alfi_only_spacer + STANDARD_SPACER;
    
                    // . . . Origin line
                Point p = proxy.getLocation();
                buffer.append(pad+ GUI__ORIGIN+ " "+ p.x+ "x"+ p.y+ NL);
    
                    // . . . Rotation line
                if (! proxy.hasDefaultRotation()) {
                    String rot = proxy.ROTATION_SETTING_STRINGS[
                                                          proxy.getRotation()];
                    buffer.append(pad + GUI__ROTATION + " = " + rot + NL);
                }
    
                    // . . . Swap line
                if (! proxy.hasDefaultSwap()) {
                    String swap = proxy.SWAP_SETTING_STRINGS[proxy.getSwap()];
                    buffer.append(pad + GUI__SWAP + " = " + swap + NL);
                }
    
                    // . . . Symmetry line
                if (! proxy.hasDefaultSymmetry()) {
                    String sym = proxy.SYMMETRY_SETTING_STRINGS[
                                                          proxy.getSymmetry()];
                    buffer.append(pad + GUI__SYMMETRY + " = " + sym + NL);
                }
            }

                // . . . ScreenLocation line (not yet implemented)
    
                // . . . ScreenSize line
            Dimension dim = _node.getFrameSize();
            if (! dim.equals(ALFINode.DEFAULT_SCREEN_SIZE)) {
                buffer.append(alfi_only_spacer + STANDARD_SPACER);
                buffer.append(GUI__FRAME_SIZE + " ");
                buffer.append(dim.width + "x" + dim.height + NL);
            }

                // . . . Display line
            if (! _node.hasDefaultLabel()) {
                String label = _node.LABEL_TYPE_STRINGS[_node.getLabelType()];
                buffer.append(alfi_only_spacer + STANDARD_SPACER);
                buffer.append(GUI__NODE_DISPLAY + " = " + label + NL);
            }
    
                // . . . IconRotates line
            boolean b_icon_rotates = _node.getIconRotates();
            if (b_icon_rotates) {
                buffer.append(alfi_only_spacer + STANDARD_SPACER);
                buffer.append(GUI__ICON_ROTATES + NL);
            }

                // . . . external port centering on (root nodes only)
            if (_node.isRootNode() && _node.getCentersPortsFlag()) {
                buffer.append(alfi_only_spacer + STANDARD_SPACER);
                buffer.append(GUI__PORTS_CENTER__EXTERNAL + NL);
            }
            
                // . . . Group line
            if (_b_format_for_UDP_section && _node.isRootNode()){
                buffer.append(alfi_only_spacer + STANDARD_SPACER);
                buffer.append(GUI__PRIMITIVE_GROUP + " '"); 
                buffer.append(_node.getPrimitiveGroup() + "'"+ NL);

                // . . .Icon
                if (_node.getIconInfo() != null) {
                buffer.append(alfi_only_spacer + STANDARD_SPACER);
                buffer.append(_node.getIconInfo() +  NL);
                }
            }
            buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);

                // write Node_Description section . . .
            String description = _node.getDescriptionLabel();
            if ((description != null) && (description.length() > 0)) {
                buffer.append(alfi_only_spacer + NODE_DESCRIPTION + NL);
                buffer.append(alfi_only_spacer+ SECTION_BEGIN_MARKER+ NL);
                StringTokenizer st = new StringTokenizer(description, NL, true);
                while (st.hasMoreTokens()) {
                    String token = st.nextToken();
                    if (! token.equals(NL)) {
                        buffer.append(alfi_only_spacer + STANDARD_SPACER);
                        buffer.append(token + NL); 
                    }
                }

                buffer.append(alfi_only_spacer + SECTION_END_MARKER + NL);
            }
        }

        return buffer.toString();
    }

        /** Writes out the include file statement.  Used in box files. */
    private static String attachIncludePath (ALFINode _added_node) {
        StringBuffer buffer = new StringBuffer();

        buffer.append(MEMBER_ADD__BOX + " ");
        buffer.append(_added_node.determineLocalName());
        buffer.append(" " + SECTION_BEGIN_MARKER + " ");
        buffer.append(FILE_INCLUDE_MARKER + " ");
        buffer.append(_added_node.
            memberNodeProxy_getAssociated().getIncludeLinePath());
        buffer.append(" " + SECTION_END_MARKER + NL);

        return buffer.toString();
    }
    
        /**
         * Writes out the #use statement.  Used in .sim files. 
         */
    private static String attachBoxFileContents (ALFINode _added_node){

        StringBuffer buffer = new StringBuffer();

        buffer.append(USE_MARKER + " "); 
        buffer.append(_added_node.determineLocalName() + " ");
        buffer.append(getUniqueModelerTagName(_added_node));
        buffer.append(NL);
        
        return buffer.toString();
    }

        /**
         * When writing out the definitions of the box files in 
         * the sim file, this method creates a unique tag (name)
         * for the #define statement.  It uses the node's local
         * name with ".box" to easily distinguish the tag from 
         * the instance name.  
         */
    private static String getUniqueModelerTagName (ALFINode _node){

        StringBuffer buffer = new StringBuffer();
        
        ALFINode root_node = _node.baseNode_getRoot(); 
        if (ms_node_vs_defines_map.containsKey(root_node)) { 
            buffer.append(ms_node_vs_defines_map.get(root_node));
        }
        else {
            String local_name = root_node.determineLocalName();

            String unique_tag = new String(local_name + 
                                    ALFIFile.BOX_NODE_FILE_SUFFIX);
            StringBuffer base_tag = new StringBuffer(local_name);
            // Make sure that the tag hasn't been used
            while (ms_node_vs_defines_map.containsValue(unique_tag)) {
                base_tag = base_tag.append("_1");
                unique_tag = base_tag + ALFIFile.BOX_NODE_FILE_SUFFIX;
            }
            ms_node_vs_defines_map.put(root_node, unique_tag);
            buffer.append(unique_tag);
        }
        return buffer.toString(); 
    }
    
        /**
         * Writes out the contents of the modeler (.sim) file.
         */
    public String createModelerFileContents () {
        StringBuffer buffer = new StringBuffer();
    
        buffer.append(
            createNodeContentsString(m_root_node_file.getRootNode(), false));

        return buffer.toString();
    }

        /**
         * Writes out the contents of the box file for the definition block. 
         */
    public String getBoxFileDefinition (ALFINode _node) {
        StringBuffer buffer = new StringBuffer();
    
        buffer.append(DEFINITION_BEGIN + " "); 
        buffer.append(getUniqueModelerTagName(_node)); 
        buffer.append(NL + 
                    createNodeContentsString(_node.baseNode_getRoot(), false));
        buffer.append(DEFINITION_END + NL); 

        return buffer.toString();
    }

    //**************************************************************************
    //***** specific section parsing methods ***********************************

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * Alfi_Version 5.2.13
         *
         * or (obsoleted)
         *
         * Alfi5_Parser_version 5.2.11
         * </PRE>
         */
    private int[] readVersionSection(Vector _sections)
                                               throws AlfiFileFormatException {
        String line = ((String[]) _sections.get(0))[0];
        StringTokenizer st = new StringTokenizer(line);
        if (st.countTokens() == 2) {
            st.nextToken();    // get past marker
            String version = st.nextToken();
            StringTokenizer st_2 = new StringTokenizer(version, ".");
            if (st_2.countTokens() == 3) {
                try {
                    int major = Integer.parseInt(st_2.nextToken());
                    int minor = Integer.parseInt(st_2.nextToken());
                    int bug_fix = Integer.parseInt(st_2.nextToken());
                    int[] version_numbers = { major, minor, bug_fix };
                    return version_numbers;
                }
                catch(NumberFormatException e) { ; }
            }
        }

        m_fpm.doError(getFileName(), m_fpm.BAD_VERSION_FORMAT, line);
        return null;
    }

    public void allowToReadUserDefinedPrimitiveSections() {
        msb_readUserDefinedPrimitiveSections_has_run = false;
    }

        /** Expects only .udp include lines in format:
          *
          *   User_Defined_Primitives
          *   {
          *   #include *udp_1.udp
          *   #include *udp_2.udp
          *   }
          *
          * These .udp files are searched for in the E2E_PATH and their content
          * should be something like (after stripping "%*"):
          *
          * Alfi_Version 6.6.2
          * various_data_prop_udp
          * {
          * % UDP test propogation of string, integer, real, and complex types.
          * string Equations = @@INCLUDE various_data_prop_udp.EQUATIONS.txt
          * string MemberDecl = 
          * string MemberImpl = 
          * string Global = 
          * string Header = 
          * string Constructor = 
          * string Destructor = 
          * string DoWhenGlobalChanges = 
          * boolean showInstanceSettings = false
          * Port output out_string
          * {
          * dataType = string
          * internalOrientation east 0
          * externalOrientation east 0
          * }
          * Port output out_integer
          * {
          * dataType = integer
          * internalOrientation east 1
          * externalOrientation east 1
          * }
          * Port input in_integer
          * {
          * dataType = integer
          * internalOrientation west 1
          * externalOrientation west 1
          * }
          * GUI_Settings
          * {
          * Group 'UDP'
          * }
          * }
          *
          * This method is called first at startup when all UDPs found in the
          * E2E_PATH are loaded into Alfi automatically.  All  further calls
          * to this method are superfluous, and are ignored.  These calls are
          * made only because a User_Defined_Primitives section is written into
          * any box using one or more for the modeler only. Alfi effectively
          * ignores these sections by ignoring subsequent calls to this method.
          */
    private void readUserDefinedPrimitiveSections(Vector _sections)
                                      throws FileLoadingCanceledException,
                                             AlfiIncludeFileNotFoundException {
//System.out.println("Parser.readUserDefinedPrimitiveSections(...):");

            // see method notes
        if (msb_readUserDefinedPrimitiveSections_has_run) { return; }
        else { msb_readUserDefinedPrimitiveSections_has_run = true; }

        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            String[] lines = new String[section_lines.length - 3];
            System.arraycopy(section_lines, 2, lines, 0, lines.length);

                // get the lines from all the included udp files
            ArrayList preprocessed_lines_list = new ArrayList();
            for (int j = 0; j < lines.length; j++) {
                String[] udp_lines = getIncludedUdpFileLines(lines[j]);
                if (udp_lines != null) {
                    for (int k = 0; k < udp_lines.length; k++) {
                        preprocessed_lines_list.add(udp_lines[k]);
                    }
                }
            }

            String[] preprocessed_lines =
                                    new String[preprocessed_lines_list.size()];
            preprocessed_lines_list.toArray(preprocessed_lines);

            HashMap type_definition_map =
                    MiscTools.getKeyAndBracketedValuePairs(preprocessed_lines);
            for (Iterator j = type_definition_map.keySet().iterator();
                                                               j.hasNext(); ) {
                String type_name = (String) j.next();
                String[] definition_lines = 
                                 (String[]) type_definition_map.get(type_name);

                    // strip comment out of definition
                Object[] comment_and_remaining_lines =
                                            stripNodeComment(definition_lines);
                String node_comment = (String) comment_and_remaining_lines[0];
                definition_lines = (String[]) comment_and_remaining_lines[1];

                HashMap sections_map =
                               slotSectionsIntoVectorHolders(definition_lines); 
                UserDefinedPrimitive base = new UserDefinedPrimitive(type_name);

                base.comment_addLocal(node_comment);

                Vector sections = null;

                    // insert parameter declarations
                final String[] TYPES = ParameterDeclaration.PARAMETER_TYPES;
                for (int k = 0; k < TYPES.length; k++) {
                    sections = (Vector) sections_map.get(TYPES[k]);
                    if (sections != null) {
                        readParameterDeclarationSections(base, sections);
                    }
                }

                    // add ports
                sections = (Vector) sections_map.get(PORT_ADD_MARKER);
                if (sections != null) { readPortAddSections(base, sections); }

                    // next add gui info
                sections = (Vector) sections_map.get(GUI_INFO_MARKER);
                if (sections != null) {
                    readGUISettingsSections(base, sections);
                }
            }
        }
    }

        /** Searches the E2E_PATH for the file named in the _include_line. */
    private File findUdpFile(String _include_line) throws
                                             AlfiIncludeFileNotFoundException {
        File udp_file = null;

        String[] parts = _include_line.split("\\s+");
        if ((parts.length == 2) && parts[0].equals(FILE_INCLUDE_MARKER)) {
            String file_name = parts[1];
            if (file_name.startsWith(ALFIFile.E2E_PARTIAL_PATH_IDENTIFIER)) {
                file_name = file_name.substring(1);
            }
            else {
                System.err.println(
                    "Parser.readUserDefinedPrimitiveSections():\n" +
                    "\tWARNING: UDP file includes should always use\n" +
                    "\t\"E2E Partial Paths\" which are marked by a\n" +
                    "\t\"*\" in the include line.  The .udp include:\n"+
                    "\n\t\t" + _include_line + "\n\n\tis not well formed.");
            }
            udp_file = MiscTools.findFile(file_name, Alfi.E2E_PATH);
            if (udp_file == null) {
                throw new AlfiIncludeFileNotFoundException(this,
                                m_root_node_file.getCanonicalPath(), parts[1]);
            }
        }
        else {
          System.err.println(
            "Parser." + "readUserDefinedPrimitiveSections():\n" +
            "  Warning: Ignoring malformed include line in " +
            m_root_node_file.getCanonicalPath() + ":\n\t" +
            _include_line + "\n\n");
        }

        return udp_file;
    }

        /** Finds an included UDP file and returns the contents.  If file not
          * found, null is returned.
          */
    private String[] getIncludedUdpFileLines(String _include_line) throws 
                                             FileLoadingCanceledException,
                                             AlfiIncludeFileNotFoundException {
        String[] udp_file_lines = null;

        ALFINodeCache cache = Alfi.getTheNodeCache();
        File udp_file = findUdpFile(_include_line);
        if (udp_file != null) {
            String file_name = udp_file.getName();
            String udp_name = file_name.substring(0, (file_name.length() - 4));
            ALFINode node = cache.findNode(udp_name);

                // Check if the UDP is already in the cache.
                // Skip it if it already exists.
            if (node != null) {return null; }

            String[] raw_udp_file_lines = getFileContents(udp_file);
            if (checkAlfiVersionNumber(raw_udp_file_lines[0])) {
                udp_file_lines = new String[raw_udp_file_lines.length - 1];
                System.arraycopy(raw_udp_file_lines, 1,
                                 udp_file_lines, 0, udp_file_lines.length);
            }
            else {
                System.err.println("Parser." +
                    "readUserDefinedPrimitiveSections():\n" +
                    "WARNING: UDP file [" + udp_file + "] has "+
                    "obsolete Alfi version number.\n\n" +
                    "\t...but still attempting to load...");
            }
        }

        return udp_file_lines;
    }

        /** Returns true if line 0 has the correct format and valid version.
          * Format should be "Alfi_Version n.n.n"
          */
    private boolean checkAlfiVersionNumber(String _udp_file_line_0) {
        String[] parts = _udp_file_line_0.split("\\s+");
        if ((parts.length == 2) && parts[0].equals(VERSION_MARKER)) {
            String[] version_numbers = parts[1].split("\\.");
            if (version_numbers.length == 3) {
                return true;
            }
        }
        return false;
    }

        /** See also Macro.parse().  Reads sections formatted as:
         *
         * <PRE>
         * Add_Macros 
         * {
         * ...
         * ...
         * ...
         * ...
         * }
         * </PRE>
         */
    private void readMacroSections(ALFINode _node_to_edit, Vector _sections)
                                          throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            StringBuffer sb = new StringBuffer();
            boolean b_quoted = false;
            for (int j = 2; j < section_lines.length - 1; j++) {
                String line = section_lines[j];

                if (j > 2) { sb.append(NL); }

                if (line.charAt(0) == '\"') {
                    b_quoted = true;
                    line = line.substring(1);
                }

                sb.append(line);
            }

                // gets { value, comment }
            String[] macro_data = Macro.parse(sb.toString());

            if (macro_data[0] != null) {
                _node_to_edit.macro_set(macro_data[0], macro_data[1], b_quoted);
            }
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * type name = value
         *
         * or
         *
         * type name                 // sets value to ""
         * </PRE>
         */
    private void readParameterDeclarationSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
//System.out.println("readParameterDeclarationSections(" + _node_to_edit + ",...):");
        for (int i = 0; i < _sections.size(); i++) {
                // these sections are just a single line each
            String line = ((String[]) _sections.elementAt(i))[0];
                // discard any is_quoted marker (a starting " character).  not
                // currently used here.
            boolean b_quoted = false;
            if (line.startsWith("\"")) {
                line = line.substring(1);
                b_quoted = true;
            }

            String[] dec_info = ParameterDeclaration.parse(line);
            if (dec_info != null) {
                _node_to_edit.parameterDeclaration_add(dec_info[0],
                                                     dec_info[1], dec_info[2]);

                if (_node_to_edit instanceof UserDefinedPrimitive) {
                    UserDefinedPrimitive udp =
                                          (UserDefinedPrimitive) _node_to_edit;
                    String name = dec_info[1]; 
                    String value = dec_info[2];

                        // check to be sure any @@INCLUDE files exist.  they
                        // should ALWAYS exist in the same directory as the
                        // udp file they are associated with
                    if (value.startsWith(FUNCX_Node.AT_AT_INCLUDE)) {
                        String[] parts = value.split("\\s+");
                        if (parts.length == 2) {
                            String file_name = parts[1];
                            File udp_file = udp.findAssociatedUDPFile();

                                // this is where the @@INCLUDE file SHOULD be
                            File atAtFile =
                                 new File(udp_file.getParentFile(), file_name);
                            if (! atAtFile.exists()) {
                                String warning = "The UDP file " + udp_file +
                                    " contains an include directive which " +
                                    "cannot be executed.  The file [" +
                                    file_name + "] specified in the line [" +
                                    line + "] cannot be found.  Files " +
                                    "specified in an \"@@INCLUDE\" directive " +
                                    "must be located in the same directory as" +
                                    " the associated UDP file.  This UDP will" +
                                    " continue loading, but will ignore this " +
                                    "include directive.";
                                Alfi.warn(warning, false);
                            }
                        }
                        else {
                            m_fpm.doWarning(getFileName(),
                                         m_fpm.BAD_AT_AT_INCLUDE_FORMAT, line);
                        }
                    }
                }
            }
            else {
                m_fpm.doWarning(getFileName(),
                                        m_fpm.BAD_PARAMETER_DECLARATION, line);
            }
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * name = value
         * </PRE>
         */
    private void readParameterModSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
            // primitive instance nodes only
        if (_node_to_edit.isBox() || _node_to_edit.isRootNode()) {
            m_fpm.doWarning(getFileName(),
                            m_fpm.INVALID_NODE_FOR_PARAMETER_MODS,
                            (String[]) _sections.elementAt(0));
        }

        for (int i = 0; i < _sections.size(); i++) {
                // these sections are just a single line each
            String line = ((String[]) _sections.elementAt(i))[0];

            String name = null;
            String value = null;
            boolean b_quoted = false;

            if (line.trim().startsWith(FILE_INCLUDE_MARKER)) {
                StringTokenizer st = new StringTokenizer(line.trim());
                if (st.countTokens() == 2) {
                    name = st.nextToken().trim();
                    value = st.nextToken().trim();
                }
            }
            else {
                int index_of_1st_equal = line.indexOf('=');
                if (index_of_1st_equal >= 0) {
                    name = line.substring(0, index_of_1st_equal).trim();
                    if (name.charAt(0) == '\"') {
                        b_quoted = true;
                        name = name.substring(1);    // remove quote marker
                    }
                    value = line.substring(index_of_1st_equal + 1).trim();
                }
            }

            if ((name != null) && (value != null)) {
                if (name.equals(FILE_INCLUDE_MARKER) ||
                          _node_to_edit.baseNode_getRoot().
                                     parameterDeclaration_containsLocal(name) ||
                          (getPortByNameAndIOType(_node_to_edit,
                                     name, PortProxy.IO_TYPE_INPUT) != null)) {
                    _node_to_edit.parameterSetting_add(name, value, b_quoted);

                        // check that @@INCLUDE files exist
                    if (_node_to_edit instanceof FUNCX_Node) {
                            // null unless value == @@INCLUDE xxxx.yyy
                        String atAtFileName =
                                        FUNCX_Node.parseAtAtIncludeLine(value);
                        if (atAtFileName != null) {
                                // an @@INCLUDE file must sit in the same
                                // directory as the box file which contains the
                                // @@INCLUDE directive (for parameter settings.
                                // different rule for @@INCLUDES in parameter
                                // declaration defaults.)
                            ALFINode container_box =
                                             _node_to_edit.containerNode_get();
                            ALFINode container_box_root =
                                (container_box.isRootNode()) ? container_box :
                                              container_box.baseNode_getRoot();
                            File root_container_file = container_box_root.
                                    getSourceFile().createAssociatedJavaFile();
                            File root_container_dir =
                                           root_container_file.getParentFile();
                            File atAtFile =
                                    new File(root_container_dir, atAtFileName);
                            if (! atAtFile.exists()) {
                                String warning = "While loading the box [" +
                                    container_box_root + "], an unexecutable " +
                                    "@@INCLUDE directive was found.  Any file" +
                                    " included via an @@INCLUDE directive must"+
                                    " be located in the same directory as the" +
                                    " box file which contains the directive. " +
                                    " In this case, the directive [" +
                                    value + "] for the parameter [" + name +
                                    "] specified in the box file [" +
                                    root_container_file + "] expects " +
                                    atAtFileName + " to be located at [" +
                                    atAtFile + "].  Loading will continue " +
                                    "setting the content of " + atAtFileName +
                                    " to be empty.";
                                Alfi.warn(warning, false);
                            }
                        }
                    }
                }
                else {
                    m_fpm.doWarning(getFileName(),
                                    m_fpm.UNRECOGNIZED_PARAMETER, line);
                }
            }
            else {
                m_fpm.doWarning(getFileName(), m_fpm.BAD_PARAMETER_MOD, line);
            }
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * Port my_io_type my_name
         * {
         * dataType = my_data_type
         * [DefaultValue = my_default_value]
         * [BundlePortDefaultContent
         *  {
         *      x.aaa real
         *      b5.b2.r2 real
         *      b5 alfi_bundle
         *  }
         * ]
         * [externalOrientation top|bottom|right|left [edge_position]]
         * [internalOrientation top|bottom|right|left [edge_position]]
         * }
         * </PRE>
         */
    private void readPortAddSections(ALFINode _node_to_edit, Vector _sections)
                                          throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() == 3) {
                st_1.nextToken();
                    // information on start line
                int io_type_id =
                            PortProxy.getIOTypeIdFromString(st_1.nextToken());
                String name = st_1.nextToken();

                    // check that I/O type and name are ok
                if (io_type_id == PortProxy.IO_TYPE_INVALID) {
                    m_fpm.doWarning(getFileName(), m_fpm.INVALID_IO_TYPE,
                                                                section_lines);
                    continue;
                }
                else if (_node_to_edit.port_nameAndSameIOExists(name,
                                                                 io_type_id)) {
                    m_fpm.doWarning(getFileName(), m_fpm.PORT_EXISTS,
                                                                section_lines);
                    continue;
                }

                    // information inside the brackets
                int edge_i = PortProxy.EDGE_INVALID;
                int edge_position_i = PortProxy.EDGE_POSITION_INVALID;
                int edge_e = PortProxy.EDGE_INVALID;
                int edge_position_e = PortProxy.EDGE_POSITION_INVALID;
                int data_type_id = PortProxy.DATA_TYPE_INVALID;
                String default_value = null;
                HashMap default_bundle_channels = null;
                for (int j = 2; j < section_lines.length - 1; j++) {
                    String line = section_lines[j];
                    StringTokenizer st_2 = new StringTokenizer(line);
                    String key = st_2.nextToken();

                    if (key.equals(PORT__INTERNAL_ORIENT) ||
                                           key.equals(PORT__EXTERNAL_ORIENT)) {
                        int edge =
                            PortProxy.getEdgeIdFromString(st_2.nextToken());
                        if (edge == PortProxy.EDGE_INVALID) {
                            m_fpm.doMessage(getFileName(),
                                            m_fpm.BAD_EDGE__REVERT, line);
                        }

                        int relative_edge_position =
                                           PortProxy.EDGE_POSITION_INVALID;
                        if (st_2.hasMoreTokens()) {
                            try {
                                relative_edge_position =
                                            Integer.parseInt(st_2.nextToken());
                            }
                            catch(NumberFormatException e) { ; }

                            if (relative_edge_position < 0) {
                                m_fpm.doMessage(getFileName(),
                                        m_fpm.BAD_EDGE_POSITION__REVERT, line);
                            }
                        }

                        if (st_2.hasMoreTokens()) {
                            m_fpm.doMessage(getFileName(),
                                                m_fpm.BAD_POSITION_LINE, line);
                        }

                        if (key.equals(PORT__INTERNAL_ORIENT)) {
                            edge_i = edge;
                            edge_position_i = relative_edge_position;
                        }
                        else {
                            edge_e = edge;
                            edge_position_e = relative_edge_position;
                        }
                    }
                    else if (key.equals(PORT__BUNDLE_PORT_DEFAULT_CONTENT)) {
                        j++;
                        default_bundle_channels = new HashMap();
                        line = section_lines[j].trim();
                        if (line.equals(SECTION_BEGIN_MARKER)) {
                            j++;
                            for ( ; j < section_lines.length; j++) {
                                line = section_lines[j].trim();
                                if (! line.equals(SECTION_END_MARKER)) {
                                    st_2 = new StringTokenizer(line);
                                    if (st_2.countTokens() == 2) {
                                        key = st_2.nextToken();
                                        String value = st_2.nextToken();
                                        int data_type = GenericPortProxy.
                                                getDataTypeIdFromString(value);
                                        Integer type = new Integer(data_type);
                                        default_bundle_channels.put(key, type);
                                    }
                                    else {
                                        m_fpm.doMessage(getFileName(),
                                            m_fpm.BAD_BUNDLE_PORT_LINES, line);
                                    }
                                }
                                else { break; }
                            }
                        }
                        else {
                            m_fpm.doMessage(getFileName(),
                                            m_fpm.BAD_BUNDLE_PORT_LINES, line);
                        }
                    }
                    else if ((st_2.countTokens() == 2) &&
                                              (st_2.nextToken().equals("="))) {
                        String value = st_2.nextToken();
                        if (key.equals(PORT__DATA_TYPE)) {
                            data_type_id =
                                      PortProxy.getDataTypeIdFromString(value);

                                // handle the deprecated type string "bool"
                                // (should be "boolean").  The warning messages
                                // are set to start 4/1/2007.
                            if (data_type_id == PortProxy.DATA_TYPE_INVALID) {
                                if (value.equals("bool")) {
                                    GregorianCalendar start_warnings_date =
                                             new GregorianCalendar(2007, 4, 1);
                                    if (start_warnings_date.before(
                                                    new GregorianCalendar())) {

                                        m_fpm.
                                           processDeprecatedPortBoolTypeWarning(
                                                                getFileName());
                                    }
                                    data_type_id = PortProxy.DATA_TYPE_BOOLEAN;
                                }
                            }
                                // end of deprecated "bool" code

                        }
                        else if (key.equals(PORT__DEFAULT)) {
                            default_value = value;
                        }
                        else {
                            m_fpm.doMessage(getFileName(),
                                        m_fpm.BAD_PORT_CONFIG_DIRECTIVE, line);
                        }
                    }
                    else {
                        m_fpm.doMessage(getFileName(),
                                                  m_fpm.BAD_PORT_CONFIG, line);
                    }
                }

                if (data_type_id == PortProxy.DATA_TYPE_INVALID) {
                    m_fpm.doMessage(getFileName(),
                              m_fpm.PORT_DATA_TYPE_UNSPECIFIED, section_lines);
                    data_type_id = PortProxy.DATA_TYPE_1_UNKNOWN;
                }

                    // prepare information about port location
                int[] position =
                        { edge_i, edge_position_i, edge_e, edge_position_e };

                PortProxy port = _node_to_edit.port_add(
                               name, io_type_id, data_type_id, position);

                if (port != null) {
                    if (default_value != null) {
                        port.setDefaultValue(default_value);
                    }

                    if (port instanceof BundlePortProxy) {
                        ((BundlePortProxy) port).setDefaultChannelMap(
                                                       default_bundle_channels);
                    }
                }
            }
            else {
                m_fpm.doWarning(getFileName(),
                                    m_fpm.BAD_PORT_DESCRIPTION, section_lines);
            }
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * Add_Junctions
         * {
         * Jctn_0 48x72
         * Jctn_1 120x66
         * }
         * </PRE>
         */
    private void readJunctionAddSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() == 1) {
                for (int j = 2; j < section_lines.length - 1; j++) {
                    StringTokenizer st_2= new StringTokenizer(section_lines[j]);
                    if (st_2.countTokens() == 2) {
                        String name = st_2.nextToken();
                        StringTokenizer st_3 =
                                   new StringTokenizer(st_2.nextToken(), "x");
                        if (st_3.countTokens() == 2) {
                            try {
                                int x = Integer.parseInt(st_3.nextToken());
                                int y = Integer.parseInt(st_3.nextToken());

                                _node_to_edit.junction_add(name,new Point(x,y));
                            }
                            catch(NumberFormatException e) {
                                m_fpm.doWarning(getFileName(),
                                    m_fpm.BAD_NUMBER_FORMAT, section_lines[j]);
                            }
                        }
                        else {
                            m_fpm.doWarning(getFileName(),
                                m_fpm.BAD_JUNCTION_POSITION, section_lines[j]);
                        }
                    }
                    else {
                        m_fpm.doWarning(getFileName(),
                                        m_fpm.BAD_JUNCTION_DESCRIPTION_LINE,
                                        section_lines[j]);
                    }
                }
            }
            else {
                m_fpm.doWarning(getFileName(),
                             m_fpm.BAD_ADD_JUNCTION_START_LINE, section_lines);
            }
        }
    } 

        /** See BundlerProxy.parse() for format expected. */
    private void readBundlerAddSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);

            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            for (int j = 2; j < section_lines.length - 1; j++) {
                ArrayList line_list = new ArrayList();

                while (! section_lines[j].equals(SECTION_END_MARKER)) {
                    line_list.add(section_lines[j++]);
                }
                line_list.add(section_lines[j]);

                String[] lines = new String[line_list.size()];
                line_list.toArray(lines);
                try {
                    HashMap bundler_info_map = BundlerProxy.parse(this, lines);
                    String name =
                           (String) bundler_info_map.get(BundlerProxy.NAME);
                    Point location =
                           (Point) bundler_info_map.get(BundlerProxy.LOCATION);
                    BundlerProxy bundler =
                                     _node_to_edit.bundler_add(name, location);

                    HashMap secondary_output_name_map =
                            (HashMap) bundler_info_map.get(
                                       BUNDLER__SECONDARY_OUTPUT_CHANNEL_NAME);
                    if (secondary_output_name_map != null) {
                        bundler.setSecondaryOutputChannelNames(_node_to_edit,
                                                    secondary_output_name_map);
                    }
                    else {
                        boolean b_unwraps = bundler_info_map.containsKey(
                                             BUNDLER__SECONDARY_INPUT_UNWRAPS);
                        String secondary_input_name = (String)
                            bundler_info_map.get(BUNDLER__SECONDARY_INPUT_NAME);
                        if ((secondary_input_name == null) && b_unwraps) {
                            secondary_input_name = "not_set";
                        }

                        if (secondary_input_name != null) {
                            bundler.setSecondaryInputConfiguration(
                                _node_to_edit, secondary_input_name, b_unwraps);
                        }
                    }
                }
                catch (AlfiFileFormatException _e) {
                    Alfi.getMessenger().inform(_e);
                }
            }
        }
    } 

        /** See BundlerIOSetting.parse() for format expected. */
    private void readBundlerIOSettingSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);

            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            for (int j = 2; j < section_lines.length - 1; j++) {
                ArrayList line_list = new ArrayList();

                while (! section_lines[j].equals(SECTION_END_MARKER)) {
                    line_list.add(section_lines[j++]);
                }
                line_list.add(section_lines[j]);

                String[] lines = new String[line_list.size()];
                line_list.toArray(lines);
                try {
                    Object[] info = BundlerIOSetting.parse(this, lines);
                    String bundler_name = (String) info[0];
                    boolean b_is_input = ((Boolean) info[1]).booleanValue();
                    boolean b_unwraps = ((Boolean) info[2]).booleanValue();
                    HashMap io_name_map = (HashMap) info[3];


                    BundlerProxy bundler =
                                 getBundlerByName(_node_to_edit, bundler_name);
                    if (b_is_input) {
                        String secondary_input_name =
                               (String) io_name_map.keySet().iterator().next();
                        bundler.setSecondaryInputConfiguration(_node_to_edit,
                                              secondary_input_name, b_unwraps);
                    }
                    else {
                        bundler.setSecondaryOutputChannelNames(_node_to_edit,
                                                                  io_name_map);
                    }
                }
                catch (AlfiFileFormatException _e) {
                    Alfi.getMessenger().inform(_e);
                }
            }
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * Remove_Submodules
         * {
         * D_3
         * }
         * </PRE>
         */
    private void readMemberRemoveSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() == 1) {
                for (int j = 2; j < section_lines.length - 1; j++) {
                    StringTokenizer st_2= new StringTokenizer(section_lines[j]);
                    if (st_2.countTokens() == 1) {
                        String local_name_of_node_to_remove = st_2.nextToken();
                        MemberNodeProxy[] proxies_inherited =
                                _node_to_edit.memberNodeProxies_getInherited();
                        for (int k = 0; k < proxies_inherited.length; k++) {
                            if (proxies_inherited[k].getLocalName().equals(
                                            local_name_of_node_to_remove)) {
                                _node_to_edit.memberNodeProxy_remove(
                                                  proxies_inherited[k]);
                                break;
                            }
                        }
                    }
                    else {
                        m_fpm.doWarning(getFileName(),
                                        m_fpm.BAD_MEMBER_DESCRIPTION_LINE,
                                        section_lines[j]);
                    }
                }
            }
            else {
                m_fpm.doWarning(getFileName(),
                            m_fpm.BAD_MEMBER_REMOVE_START_LINE, section_lines);
            }
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * Add_Submodules
         * {
         * xor xor_0
         *     // the include lines have been changed in processBoxIncludes()
         * box B_4
         * {
         * #include *B.box /users/bruce/e2e/boxes/B.box
         * }
         * box B_5
         * {
         * #include /users/bruce/e2e/boxes/B.box /users/bruce/e2e/boxes/B.box
         * }
         * box B_6
         * {
         * #include boxes/B.box /users/bruce/e2e/boxes/B.box
         * }
         * }
         * </PRE>
         */
    private void readMemberAddSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        ALFINodeCache cache = Alfi.getTheNodeCache();
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);

            if (st_1.countTokens() == 1) {
                for (int j = 1; j < section_lines.length; j++) {
                    String line = section_lines[j];
                    if (line.startsWith(SECTION_BEGIN_MARKER)) { continue; }
                    if (line.startsWith(SECTION_END_MARKER)) { break; }

                    String[] subsection = getNextSection(section_lines, j);

                        // is it the format for an included primitive?
                    if (subsection.length == 1) {
                        StringTokenizer st = new StringTokenizer(subsection[0]);
                        if (st.countTokens() == 2) {
                            String primitive_type_name = st.nextToken();
                            ALFINode member_node_root = cache.
                                getRootPrimitiveNodeByName(primitive_type_name);

                                // if not found, then check if it should be a
                                // template generated primitive
                            if (member_node_root == null) {
                                int indexOf__ =
                                             primitive_type_name.indexOf("__");
                                if (indexOf__ > 0) {
                                    String[] parts =
                                               primitive_type_name.split("__");
                                    String base_name = parts[0];
                                    PrimitiveTemplate pt =
                                    cache.getPrimitiveTemplateByName(base_name);
                                    if (pt != null) {
                                        int[] io_set_counts =
                                             pt.parseSetCountsString(parts[1]);
                                        if (io_set_counts != null) {
                                            member_node_root =
                                                new ALFINode(pt, io_set_counts);
                                        }
                                    }
                                }
                            }

                                // primitive not loaded?
                            if (member_node_root == null) {
                                if (findMissingPrimitive(primitive_type_name,
                                        line, _node_to_edit.getSourceFile().
                                                         getCanonicalPath())) {
                                    Alfi.getMainWindow().showPreferences();
                                    j--;            // try again
                                    continue;
                                }
                                else {
                                        // cancel the load process
                                    throw new FileLoadingCanceledException(
                                            _node_to_edit.getSourceFile().
                                                     getCanonicalPath(), null);
                                }
                            }

                            String member_node_local_name = st.nextToken();

                                // places member at a default location.  changed
                                //   later by a GUI_Settings parameter.
                            MemberNodeProxy mnp =
                                _node_to_edit.memberNodeProxy_add(
                                    member_node_root, member_node_local_name,
                                    primitive_type_name, new Point(10,10));
                        }
                        else {
                            m_fpm.doWarning(getFileName(),
                               m_fpm.BAD_PRIMITIVE_INCLUDE_LINE, subsection[0]);
                        }
                    }
                        // is it the format for an included box?
                    else if (subsection.length == 4) {
                        if (! subsection[1].equals(SECTION_BEGIN_MARKER) ||
                                (! subsection[3].equals(SECTION_END_MARKER))) {
                            m_fpm.doWarning(getFileName(),
                                 m_fpm.BAD_BOX_INCLUDE_SECTION, subsection[0]);
                            continue;
                        }

                        String node_type = null;
                        String member_node_local_name = null;
                        String include_line_path = null;
                        String canonical_file_path = null;

                        StringTokenizer st = new StringTokenizer(subsection[0]);
                        if (st.countTokens() == 2) {
                            node_type = st.nextToken();
                            member_node_local_name = st.nextToken();
                        }
                        else {
                            m_fpm.doWarning(getFileName(),
                                m_fpm.BAD_BOX_DESCRIPTION_LINE, subsection[0]);
                            continue;
                        }

                        st = new StringTokenizer(subsection[2]);
                        if ((st.countTokens() == 3) &&
                                st.nextToken().equals(FILE_INCLUDE_MARKER)) {
                            include_line_path = st.nextToken();
                            canonical_file_path = st.nextToken();
                        }
                        else {
                            m_fpm.doWarning(getFileName(),
                                            m_fpm.BAD_BOX_FILE_TO_INCLUDE_LINE,
                                            subsection[2]);
                            continue;
                        }

                        ALFINode member_node_root =
                                        cache.getRootNode(canonical_file_path);
                        MemberNodeProxy mnp = _node_to_edit.memberNodeProxy_add(
                            member_node_root, member_node_local_name,
                            include_line_path, new Point(10,10));
                    }
                    else {
                        m_fpm.doWarning(getFileName(),
                              m_fpm.UNRECOGNIZED_NODE_TYPE_FORMAT, subsection);
                    }

                    j += subsection.length - 1;
                }
            }
            else {
                m_fpm.doWarning(getFileName(),
                          m_fpm.BAD_MEMBER_DESCRIPTION_LINE, section_lines[0]);
            }
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * Remove_Connections_gui
         * {
         * this i1 -> xor_0 a
         * xor_0 0 -> or_1 a
         * __JUNCTION__ Jctn_0 -> or_1 a
         * }
         * </PRE>
         */
    private void readGuiConnectionRemoveSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            for (int j = 2; j < section_lines.length - 1; j++) {
                ConnectionProxy connection = getConnectionByDescription(
                                              _node_to_edit, section_lines[j]);
                if (connection != null) {
                    _node_to_edit.connection_remove(connection);
                }
                else {
                    m_fpm.doWarning(getFileName(), m_fpm.CONNECTION_NOT_FOUND,
                                                  section_lines[j]);
                }
            }
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * Remove_Connections
         * {
         * this i1 -> xor_0 a
         * xor_0 0 -> or_1 a
         * C_2 01 -> D_3 i3
         * }
         * </PRE>
         */
    private void readConnectionRemoveSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            for (int j = 2; j < section_lines.length - 1; j++) {
                ConnectionProxy connection_to_remove =
                    getConnectionByDescription(_node_to_edit, section_lines[j]);
                if (connection_to_remove != null) {
                    if (_node_to_edit.connection_contains(
                                                    connection_to_remove)) {
                        _node_to_edit.connection_remove(connection_to_remove);
                    }
                }
                else {
                    m_fpm.doWarning(getFileName(), m_fpm.CONNECTION_NOT_FOUND,
                                                  section_lines[j]);
                }
            }
        }
    }

        /** Parses the text created in
          * ConnectionProxy.generateParserRepresentation_guiInfo().
          *
          * Expects _connection_description formatted as:
          *
          * <PRE>
          * this b -> __BUNDLER__ Bundler_1
          * {
          *     [IS_BUNDLE
          *     [IS_BUNDLER_PRIMARY_INPUT]
          *     [IS_BUNDLER_PRIMARY_OUTPUT]]
          *     x=30 y=60
          * }
          * this y -> this yy
          * {
          *     x=30 y=60
          * }
          * this y -> this yy
          * {
          *     z=cw|ccw    // dog-leg indicator
          * }
          * this y -> this yy
          * { }
          * </PRE>
          */
    private void readGuiConnectionAddSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            Object[][] connection_descriptions =
                        getIndividualConnectionDescriptions(_node_to_edit,
                                                            section_lines);

            for (int j = 0; j < connection_descriptions.length; j++) {
                Object[] connection_parts = determineConnectionComponents(
                        _node_to_edit, (String) connection_descriptions[j][0]);
                if ((connection_parts == null) || (connection_parts[1] == null)
                                             || (connection_parts[3] == null)) {
                    m_fpm.doWarning(getFileName(),
                                    m_fpm.BAD_CONNECTION_DESCRIPTION,
                                    (String) connection_descriptions[j][0]);
                    continue;
                }

                MemberNodeProxy source_member =
                                  (MemberNodeProxy) connection_parts[0];
                GenericPortProxy source= (GenericPortProxy) connection_parts[1];
                MemberNodeProxy sink_member =
                                  (MemberNodeProxy) connection_parts[2];
                GenericPortProxy sink = (GenericPortProxy) connection_parts[3];

                Integer[] path_definition = null;
                int dog_leg_hint = ConnectionProxy.DOG_LEG_HINT_NONE;
                if ((connection_descriptions[j].length == 1) ||
                                     (connection_descriptions[j][1] == null)) {
                    path_definition = null;
                }
                else {
                    String path_desc = (String) connection_descriptions[j][1];
                    if (path_desc.startsWith("z=")) {
                        path_definition = null;
                        if (path_desc.endsWith("ccw")) {
                            dog_leg_hint = ConnectionProxy.DOG_LEG_HINT_CCW;
                        }
                        else if (path_desc.endsWith("cw")) {
                            dog_leg_hint = ConnectionProxy.DOG_LEG_HINT_CW;
                        }
                    }
                    else {
                        path_definition = ConnectionProxy.
                                          parseStringPathDefinition(path_desc);
                        if (path_definition == null) {
                            m_fpm.doWarning(getFileName(),
                                        m_fpm.BAD_CONNECTION_PATH_DESCRIPTION,
                                        path_desc);
                            continue;
                        }
                    }
                }

                    // this section is being added to remove old (pre 6.5.0)
                    // method of storing info to create a dog-leg connection
                    // path.  such paths now calculated automatically by default
                int[] new_dog_leg_connection_version = { 6, 5, 0 };
                if (! m_root_node_file.fileVersionEqualOrGreaterThan(
                                             new_dog_leg_connection_version)) {
                    if (path_definition != null) {
                        if ((path_definition.length == 1) ||
                                      ((path_definition.length == 2) &&
                                               (path_definition[0] == null))) {
                            if (shouldBeDoglegDefaultPath(_node_to_edit, source,
                                           source_member, sink, sink_member)) {
                                    // we don't try to determine the dog-leg
                                    // hint here, as it has too much to do
                                    // with the geometry in the display.
                                path_definition = null;
                            }
                        }
                    }
                }

                HashMap config_map = (HashMap) connection_descriptions[j][2];
                boolean b_is_bundle = config_map.containsKey(BUNDLE__IS_BUNDLE);
                TerminalBundlersConfiguration bundlers_config =
                    generateBundlerConfig(_node_to_edit,source,sink,config_map);
                ConnectionProxy connection = _node_to_edit.connection_add(
                                source, source_member, sink, sink_member,
                                path_definition, b_is_bundle, bundlers_config);
                if (dog_leg_hint != ConnectionProxy.DOG_LEG_HINT_NONE) {
                    connection.setDogLegHint(dog_leg_hint, false);
                }
            }
        }
    }

        /** This is used only for the loading of pre version 6.4.0 box files
          * which may have old methods of tracking dog-leg connection paths.
          * IMPORTANT: This method assumes straight paths have already been
          * discounted.
          */
    private boolean shouldBeDoglegDefaultPath(ALFINode _container,
                     GenericPortProxy _source, MemberNodeProxy _source_member,
                     GenericPortProxy _sink, MemberNodeProxy _sink_member) {

        int[] start_and_end_directions =
                  ConnectionProxy.getDefaultStartAndEndDirections(_container,
                                 _source, _source_member, _sink, _sink_member);

        int source_start = start_and_end_directions[0];
        int sink_end = start_and_end_directions[1];

        return ((source_start == ConnectionProxy.DIRECTION_ANY) ||
                            (sink_end == ConnectionProxy.DIRECTION_ANY) ||
                                                   (source_start != sink_end));
    }

        /** Creates an appropriate TerminalBundlersConfiguration from
          * information in parsed connection config and that which was already
          * placed in sink and/or source bundlers in its config info.  If
          * neither source nor sink are bundlers, null is returned.
          */
    private TerminalBundlersConfiguration generateBundlerConfig(ALFINode _node,
                              GenericPortProxy _source, GenericPortProxy _sink,
                              HashMap _config_map) {
        boolean b_create_bundler_terminals_config = false;

        boolean b_is_primary_out = false;
        HashMap secondary_output_channel_name_map = null;

        boolean b_is_primary_in = false;
        String secondary_input_name = null;
        boolean b_unwraps_secondary_input = false;

        if (_source instanceof BundlerProxy) {
            b_create_bundler_terminals_config = true;
            BundlerProxy source_bundler = (BundlerProxy) _source;
            if (_config_map.containsKey(BUNDLE__IS_BUNDLER_PRIMARY_OUTPUT)) {
                b_is_primary_out = true;
            }
            else {
                secondary_output_channel_name_map =
                          source_bundler.getSecondaryOutputChannelNames(_node);
            }
        }

        if (_sink instanceof BundlerProxy) {
            b_create_bundler_terminals_config = true;
            BundlerProxy sink_bundler = (BundlerProxy) _sink;
            if (_config_map.containsKey(BUNDLE__IS_BUNDLER_PRIMARY_INPUT)) {
                b_is_primary_in = true;
            }
            else {
                secondary_input_name= sink_bundler.getSecondaryInputName(_node);
                b_unwraps_secondary_input =
                                     sink_bundler.secondaryInputUnwraps(_node);
            }
        }

        if (b_create_bundler_terminals_config) {
            return new TerminalBundlersConfiguration(b_is_primary_out,
                            secondary_output_channel_name_map, b_is_primary_in,
                            secondary_input_name, b_unwraps_secondary_input);
        }
        else { return null; }
    }

        /** Finds the parts needed to build (or find) a connection.  Returns an
          * Object[4] composed of { source_member_proxy, source_proxy,
          *                         sink_member_proxy, sink_proxy }.
          */
    private Object[] determineConnectionComponents(ALFINode _node_to_edit,
                                                  String _connection_string) {
        StringTokenizer st = new StringTokenizer(_connection_string);

        if (st.countTokens() != 5) {
                 // user editted out some white space maybe?
             if ((st.countTokens() == 3) || (st.countTokens() == 4)) {
                 int i_start= _connection_string.indexOf(ConnectionProxy.ARROW);
                 if (i_start > 0) {
                     int i_end = i_start + ConnectionProxy.ARROW.length();
                     StringBuffer sb = (new StringBuffer(_connection_string)).
                         replace(i_start, i_end, ConnectionProxy._ARROW_);
                     st = new StringTokenizer(sb.toString());
                 }
             }
             if (st.countTokens() != 5) { return null; }
        }

        String source_member_name = st.nextToken();
        String source_name = st.nextToken();
        st.nextToken();  // ignore "->"
        String sink_member_name = st.nextToken();
        String sink_name = st.nextToken();

            // determine source_member_proxy and source_proxy
        Object[] components = new Object[4];
        if (source_member_name.equals("this")) {
            components[0] = null;
            components[1] = getPortByNameAndIOType(_node_to_edit, source_name,
                                                      PortProxy.IO_TYPE_INPUT);
        }
        else if (source_member_name.equals(JUNCTION_ID_STRING)) {
            components[0] = null;
            components[1] = getJunctionByName(_node_to_edit, source_name);
        }
        else if (source_member_name.equals(BUNDLER_ID_STRING)) {
            components[0] = null;
            components[1] = getBundlerByName(_node_to_edit, source_name);
        }
        else {
            ALFINode member =
                  getDirectMemberNodeByName(_node_to_edit, source_member_name);
            if (member != null) {
                components[0] = member.memberNodeProxy_getAssociated();
                components[1] = getPortByNameAndIOType(member, source_name,
                                                     PortProxy.IO_TYPE_OUTPUT);
            }
            else { return null; }
        }

            // determine sink_member_proxy and sink_proxy
        if (sink_member_name.equals("this")) {
            components[2] = null;
            components[3] = getPortByNameAndIOType(_node_to_edit, sink_name,
                                                     PortProxy.IO_TYPE_OUTPUT);
        }
        else if (sink_member_name.equals(JUNCTION_ID_STRING)) {
            components[2] = null;
            components[3] = getJunctionByName(_node_to_edit, sink_name);
        }
        else if (sink_member_name.equals(BUNDLER_ID_STRING)) {
            components[2] = null;
            components[3] = getBundlerByName(_node_to_edit, sink_name);
        }
        else {
            ALFINode member =
                    getDirectMemberNodeByName(_node_to_edit, sink_member_name);
            if (member != null) {
                components[2] = member.memberNodeProxy_getAssociated();
                components[3] = getPortByNameAndIOType(member, sink_name,
                                                      PortProxy.IO_TYPE_INPUT);
            }
            else { return null; }
        }

        return components;
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * GUI_Settings
         * {
         * ScreenSize 150x150
         * Group 'Real Function'
         * Icon XBM inverse.xbm
         * Origin 82x54
         * Rotation = East
         * Swap = X
         * Symmetry = XY
         * GUI_Connection this i3 -> zerocross_0 val    // very old style!!
         * {                                            //
         * 8x40                                         //
         * 10x40                                        //
         * 10x177                                       //
         * 42x177                                       //
         * }                                            //
         * IconRotates
         * }
         * </PRE>
         */
    private void readGUISettingsSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() != 1) {
                m_fpm.doMessage(getFileName(),
                                m_fpm.BAD_GUI_SETTINGS_SECTION + _node_to_edit,
                                section_lines);
                continue;
            }

            for (int j = 1; j < section_lines.length; j++) {
                if (section_lines[j].startsWith(SECTION_BEGIN_MARKER)) {
                    continue;
                }
                else if (section_lines[j].startsWith(SECTION_END_MARKER)) {
                    break;
                }

                String[] subsection = getNextSection(section_lines, j);
                j += subsection.length - 1;

                StringTokenizer st_2 = new StringTokenizer(subsection[0]);
                String first_token = st_2.nextToken();
                if (first_token.startsWith(GUI__FRAME_SIZE)) {
                    StringTokenizer st_3 =
                            new StringTokenizer(st_2.nextToken(), "x");
                    try {
                        int w = Integer.parseInt(st_3.nextToken());
                        int h = Integer.parseInt(st_3.nextToken());
                        _node_to_edit.setFrameSize(new Dimension(w, h));
                    }
                    catch(NumberFormatException e) {
                        _node_to_edit.setFrameSize(new Dimension(200, 200));
                        m_fpm.doMessage(getFileName(),
                                       m_fpm.BAD_NUMBER_FORMAT, subsection[0]);
                    }
                    catch(NoSuchElementException e) {
                        _node_to_edit.setFrameSize(new Dimension(200, 200));
                        m_fpm.doMessage(getFileName(),
                                         m_fpm.BAD_SCREEN_SIZE, subsection[0]);
                    }
                }
                else if (first_token.equals(GUI__ORIGIN)) {
                    StringTokenizer st_3 =
                            new StringTokenizer(st_2.nextToken(), "x");
                    MemberNodeProxy proxy =
                                _node_to_edit.memberNodeProxy_getAssociated();
                    try {
                        int x = Integer.parseInt(st_3.nextToken());
                        int y = Integer.parseInt(st_3.nextToken());
                        int[] first_version_using_UL_as_location = { 5, 2, 13 };
                        if (m_root_node_file.fileVersionEqualOrGreaterThan(
                                        first_version_using_UL_as_location)) {
                            proxy.setLocation(new Point(x, y));
                        }
                        else {
                             Point upper_left = MemberNodeWidget.
                                 determineULLocationFromCenterLocation(proxy,
                                                                          x, y);
                             proxy.setLocation(upper_left);
                        }
                    }
                    catch(NumberFormatException e) {
                        proxy.setLocation(new Point(10, 10));
                        m_fpm.doMessage(getFileName(),
                                       m_fpm.BAD_NUMBER_FORMAT, subsection[0]);
                    }
                    catch(NoSuchElementException e) {
                        proxy.setLocation(new Point(10, 10));
                        m_fpm.doMessage(getFileName(),
                                              m_fpm.BAD_ORIGIN, subsection[0]);
                    }
                }
                else if (first_token.equals(GUI__ROTATION)) {
                    if (! st_2.nextToken().equals("=")) {
                        m_fpm.doMessage(getFileName(),
                                            m_fpm.BAD_ROTATION, subsection[0]);
                        continue;
                    }

                    String rotation = st_2.nextToken();
                    int id = MemberNodeProxy.getRotationFromString(rotation);
                    if (id != MemberNodeProxy.ROTATION_INVALID) {
                        _node_to_edit.memberNodeProxy_getAssociated().
                                                               setRotation(id);
                    }
                    else {
                        m_fpm.doMessage(getFileName(),
                                            m_fpm.BAD_ROTATION, subsection[0]);
                        continue;
                    }
                }
                else if (first_token.equals(GUI__SWAP)) {
                    if (! st_2.nextToken().equals("=")) {
                        m_fpm.doMessage(getFileName(),
                                                m_fpm.BAD_SWAP, subsection[0]);
                        continue;
                    }

                    String swap = st_2.nextToken();
                    int id = MemberNodeProxy.getSwapFromString(swap);
                    if (id != MemberNodeProxy.SWAP_INVALID) {
                        _node_to_edit.memberNodeProxy_getAssociated().
                                                                   setSwap(id);
                    }
                    else {
                        m_fpm.doMessage(getFileName(),
                                                m_fpm.BAD_SWAP, subsection[0]);
                        continue;
                    }
                }
                else if (first_token.equals(GUI__SYMMETRY)) {
                    if (! st_2.nextToken().equals("=")) {
                        m_fpm.doMessage(getFileName(),
                                            m_fpm.BAD_SYMMETRY, subsection[0]);
                        continue;
                    }

                    String symmetry = st_2.nextToken();
                    int id = MemberNodeProxy.getSwapFromString(symmetry);
                    if (id != MemberNodeProxy.SYMMETRY_INVALID) {
                        _node_to_edit.memberNodeProxy_getAssociated().
                                                               setSymmetry(id);
                    }
                    else {
                        m_fpm.doMessage(getFileName(),
                                            m_fpm.BAD_SYMMETRY, subsection[0]);
                        continue;
                    }
                }
                else if (first_token.equals(GUI__PRIMITIVE_GROUP)) {
                    String group_name = subsection[0].substring(
                                   GUI__PRIMITIVE_GROUP.length()).trim();
                    group_name = group_name.replace('\'', ' ').trim();
                    _node_to_edit.setPrimitiveGroup(group_name);
                }
                else if (first_token.equals(GUI__ICON_INFO)) {
                    _node_to_edit.setIconInfo(subsection[0]);
                }
                else if (first_token.equals(GUI__ICON_ROTATES)) {
                    _node_to_edit.setIconRotates(true);
                }
                else if (first_token.equals(GUI__PORTS_CENTER__EXTERNAL)) {
                    _node_to_edit.setCentersPortsFlag(true);
                }
                else if (first_token.equals(GUI__NODE_DISPLAY)) {
                    if (! st_2.nextToken().equals("=")) {
                        m_fpm.doMessage(getFileName(),
                                        m_fpm.BAD_NODE_DISPLAY, subsection[0]);
                        continue;
                    }

                    String node_display = st_2.nextToken();
                    int id = ALFINode.getLabelFromString(node_display);
                    if (id != ALFINode.LABEL_TYPE_INVALID) {
                        _node_to_edit.setLabelType(id);
                    }
                    else {
                        m_fpm.doMessage(getFileName(),
                                        m_fpm.BAD_NODE_DISPLAY, subsection[0]);
                        continue;
                    }
                }

                    // only here for reading pre 5.2.24 files
                else if (first_token.equals(GUI__CONNECTION)) {
                    parseAndSetConnectionPath(_node_to_edit, subsection);
                }

                else {
                    m_fpm.doMessage(getFileName(),
                              m_fpm.BAD_GUI_SETTINGS_DIRECTIVE, subsection[0]);
                }
            }
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * Node_Description
         * {
         *      node description 1
         *      node description 2
         * }
         * </PRE>
         */
    private void readNodeDescriptionSections(ALFINode _node_to_edit,
                                                       Vector _sections) {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() != 1) {
                m_fpm.doMessage(getFileName(),
                                m_fpm.BAD_GUI_SETTINGS_SECTION + _node_to_edit,
                                section_lines);
                continue;
            }

            String description = new String();
            for (int j = 1; j < section_lines.length; j++) {
                if (section_lines[j].startsWith(SECTION_BEGIN_MARKER)) {
                    continue;
                }
                else if (section_lines[j].startsWith(SECTION_END_MARKER)) {
                    break;
                }
                String line = section_lines[j];
                description += line.trim() + NL;
            }
            _node_to_edit.setDescriptionLabel(description.trim());
            
        }
    }

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * Settings B_6
         * {
         *    // all options are possible in here
         * }
         */
    private void readSettingsSections(ALFINode _container_node,
                    Vector _sections) throws FileLoadingCanceledException,
                                             AlfiIncludeFileNotFoundException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            StringTokenizer st = new StringTokenizer(section_lines[0]);
            if ((st.countTokens() != 2) ||
                      (! st.nextToken().equals(MEMBER_NODE_SETTINGS_MARKER))) {
                m_fpm.doWarning(getFileName(), m_fpm.BAD_SETTINGS,
                                                                section_lines);
                continue;
            }

            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            String node_to_edit_local_name = st.nextToken();
            ALFINode node_to_edit = getDirectMemberNodeByName(_container_node,
                                                      node_to_edit_local_name);

                // deal with the possibility that the node include was removed
                // from the box file, but settings for it were not removed.
            if (node_to_edit != null) {
                String[] settings_lines = new String[section_lines.length - 3];
                System.arraycopy(section_lines, 2, settings_lines, 0,
                                                    settings_lines.length);

                    // read the settings into the node (woohoo!)
                read(node_to_edit, settings_lines);
            }
        }
    }

        /** Expects sections in this format:
          *
          * Parameter_Link_Declarations
          * {
          *     initial_value
          *     {
          *         5.0
          *         __END_VALUE__
          *         a comment
          *         __END_COMMENT__
          *         q_0.data_in_0.init
          *         q_0.data_in_1.init
          *     }
          * }
          */
    private void readLinkDeclarationSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() != 1) {
                m_fpm.doMessage(getFileName(),
                    m_fpm.BAD_PARAMETER_LINK_DECLARATION_SECTION+_node_to_edit,
                    section_lines);
                continue;
            }

            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            for (int j = 2; j < section_lines.length - 1; ) {
                ArrayList line_list = new ArrayList();
                String line = section_lines[j++].trim();
                while (! line.equals(SECTION_END_MARKER)) {
                    line_list.add(line);
                    line = section_lines[j++].trim();
                }
                line_list.add(line);

                String[] declaration_lines = new String[line_list.size()];
                line_list.toArray(declaration_lines);
                String[] parsed_parts= LinkDeclaration.parse(declaration_lines);

                String name = parsed_parts[0];
                String value = parsed_parts[1];
                String comment = parsed_parts[2];
                HashMap link_map = new HashMap();
                for (int k = 3; k < parsed_parts.length; k += 2) {
                    String primitive_node_name = parsed_parts[k];
                    ALFINode primitive = getAnyContainedNodeByName(
                                           _node_to_edit, primitive_node_name);
                    String parameter_name = parsed_parts[k + 1];

                    String[] new_names = { parameter_name };    // default
                    String[] names = (String[]) link_map.get(primitive);
                    if (names != null) {
                        new_names = new String[names.length + 1];
                        System.arraycopy(names, 0, new_names, 0, names.length);
                        new_names[new_names.length - 1] = parameter_name;
                    }

                    link_map.put(primitive, new_names);
                }

                _node_to_edit.linkDeclaration_add(name, value,
                                                            link_map, comment);
            }
        }
    }

        /** Expects sections in this format:
          *
          * Parameter_Link_Settings
          * {
          *     imaginary_1		[this is the link name]
          *     {
          *         5.0			[the new setting for the link]
          *         -3.0
          *     }
          *     imaginary_2		[this is the link name]
          *     {
          *         5.5			[the new setting for the link]
          *         -3.5
          *     }
          * }
          */
    private void readLinkSettingSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        ALFINode root_node = _node_to_edit.baseNode_getRoot();
        if (root_node == null) {
            Alfi.warn("Parser.readLinkSettingSections(" + _node_to_edit +
                ",...): Invalid call for root nodes.");
            return;
        }

        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);
            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() != 1) {
                m_fpm.doMessage(getFileName(),
                      m_fpm.BAD_PARAMETER_LINK_SETTING_SECTION + _node_to_edit,
                     section_lines);
                continue;
            }

            if (! checkBracketsInMultilineSection(section_lines)) { continue; }

            for (int j = 2; j < section_lines.length - 1; ) {
                ArrayList line_list = new ArrayList();
                String line = new String();
                while (! line.equals(SECTION_END_MARKER)) {
                    line = section_lines[j++].trim();
                    line_list.add(line);
                }
                String[] lines = new String[line_list.size()];
                line_list.toArray(lines);

                String[] name_and_value = LinkSetting.parse(lines);
                _node_to_edit.linkSetting_add(name_and_value[0],
                                              name_and_value[1], "", true);
            }
        }
    }
        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * TextComment my_name 
         * {
         *      Multiline Text  
         *      Comment Here
         *      GUI_Settings
         *      {
         *          Origin 216x48
         *          FontSize 12
         *      }
         * }
         * </PRE>
         */
    private void readTextCommentSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);

            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() == 2)  {
                st_1.nextToken();
                    // information on start line
                String  name = st_1.nextToken();

                    // Now parse the window comment
                StringBuffer sb = new StringBuffer();
                boolean b_quoted = false;
                int j = 2;
                for (; j < section_lines.length - 1; j++) {
                    String line = section_lines[j];
    
                    StringTokenizer st_2 = new StringTokenizer(line);
                    String key = st_2.nextToken();
                
                    if (key.equals(GUI_INFO_MARKER)) {
                        break;
                    }

                    if (j > 2) { sb.append(NL); }

                    if (line.charAt(0) == '\"') {
                        b_quoted = true;
                        line = line.substring(1);
                    }

                    sb.append(line);
                }
                
                String color = 
                    AlfiColors.getColorString(AlfiColors.ALFI_BLACK);
                Point bg_loc = new Point(10, 10);
                Point text_loc = new Point(0, 0);
                int font_size = 
                    AlfiFonts.getFontSize(AlfiFonts.FONT_MEDIUM);
                boolean bold = false;
                boolean italic = false;
                boolean underline = false;

                for (int k = j; k < section_lines.length - 1; k++) {
                    String line = section_lines[k];
                    StringTokenizer st_3 = new StringTokenizer(line);
                    String key = st_3.nextToken();
                    if (key.equals(GUI__COLOR)) {
                        color = st_3.nextToken();
                    }
                    else if (key.equals(GUI__ORIGIN)) {
                        StringTokenizer st_4 =
                            new StringTokenizer(st_3.nextToken(), "x");
                        try {
                            int x = Integer.parseInt(st_4.nextToken());
                            int y = Integer.parseInt(st_4.nextToken());
                            int[] first_version_using_UL_as_location = 
                                                                { 5, 2, 13 };
                            text_loc = new Point(x,y);

                        }
                        catch(NumberFormatException e) {
                            text_loc = new Point(10, 10);
                            m_fpm.doMessage(getFileName(),
                                    m_fpm.BAD_NUMBER_FORMAT,
                                    section_lines[k]);
                        }
                        catch(NoSuchElementException e) {
                            text_loc = new Point(10, 10);
                            m_fpm.doMessage(getFileName(),
                                           m_fpm.BAD_ORIGIN, 
                                           section_lines[k]);
                        }
                    }
                    else if (key.equals(GUI__ORIGIN)) {
                        StringTokenizer st_4 =
                            new StringTokenizer(st_3.nextToken(), "x");
                        try {
                            int x = Integer.parseInt(st_4.nextToken());
                            int y = Integer.parseInt(st_4.nextToken());
                            int[] first_version_using_UL_as_location = 
                                                                { 5, 2, 13 };
                            text_loc = new Point(x,y);

                        }
                        catch(NumberFormatException e) {
                            text_loc = new Point(0, 0);
                            m_fpm.doMessage(getFileName(),
                                    m_fpm.BAD_NUMBER_FORMAT,
                                    section_lines[k]);
                        }
                        catch(NoSuchElementException e) {
                            text_loc = new Point(0, 0);
                            m_fpm.doMessage(getFileName(),
                                           m_fpm.BAD_ORIGIN, 
                                           section_lines[k]);
                        }
                    }
                    else if (key.equals(GUI__FONT_SIZE)) {
                        font_size = Integer.parseInt(st_3.nextToken());
                    }
                    else if (key.equals(AlfiFonts.FONT_STYLE_BOLD)) {
                        bold = true;
                    }
                    else if (key.equals(AlfiFonts.FONT_STYLE_ITALIC)) {
                        italic = true;
                    }
                    else if (key.equals(AlfiFonts.FONT_STYLE_UNDERLINED)) {
                        underline = true;
                    }
                }

                if (name != null) {
                    int color_id = AlfiColors.getColorId(color);
                    _node_to_edit.textComment_add(name, sb.toString(), 
                    text_loc, font_size, color_id, bold, italic, underline); 
                }
            }
        }
    } 

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * ColoredBox my_name 
         * {
         *      Origin 216x48
         *      Size 151x137 
         *      Color yellow
         * }
         * </PRE>
         */
    private void readColoredBoxSections(ALFINode _node_to_edit,
                  Vector _sections) throws FileLoadingCanceledException {
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);

            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() == 2)  {
                st_1.nextToken();
                    // information on start line
                String  name = st_1.nextToken();

                    // Now parse the attributes
                String color = 
                    AlfiColors.getColorString(AlfiColors.ALFI_YELLOW);
                Point bg_loc = new Point(10, 10);
                Dimension size = new Dimension(200, 200);
                int bg_layer = ColoredBox.HILITE_LAYER_BACK;
        
                for (int j = 2; j < section_lines.length - 1; j++) {
                    String line = section_lines[j];
                    StringTokenizer st_2 = new StringTokenizer(line);
                    String key = st_2.nextToken();
                    if (key.equals(GUI__COLOR)) {
                        color = st_2.nextToken();
                    }
                    else if (key.equals(GUI__LAYER)) {
                        String layer = st_2.nextToken();
                        if (layer.equals(ColoredBox.FRONT_LAYER)) {
                            bg_layer = ColoredBox.HILITE_LAYER_FRONT;
                        }
                    }
                    else if (key.equals(GUI__ORIGIN)) {
                        StringTokenizer st_3 =
                            new StringTokenizer(st_2.nextToken(), "x");
                        try {
                            int x = Integer.parseInt(st_3.nextToken());
                            int y = Integer.parseInt(st_3.nextToken());
                            int[] first_version_using_UL_as_location = 
                                                                { 5, 2, 13 };
                            bg_loc = new Point(x,y);
                        }
                        catch(NumberFormatException e) {
                            bg_loc = new Point(10, 10);
                            m_fpm.doMessage(getFileName(),
                                    m_fpm.BAD_NUMBER_FORMAT,
                                    section_lines[j]);
                        }
                        catch(NoSuchElementException e) {
                            bg_loc = new Point(10, 10);
                            m_fpm.doMessage(getFileName(),
                                           m_fpm.BAD_ORIGIN, 
                                           section_lines[j]);
                        }
                    }
                    else if (key.equals(GUI__SIZE)) {
                        StringTokenizer st_3 =
                            new StringTokenizer(st_2.nextToken(), "x");

                        try {
                            int w = Integer.parseInt(st_3.nextToken());
                            int h = Integer.parseInt(st_3.nextToken());
                            size = new Dimension(w, h);
                        }
                        catch(NumberFormatException e) {
                            size = new Dimension(200, 200);
                            m_fpm.doMessage(getFileName(),
                                     m_fpm.BAD_NUMBER_FORMAT, section_lines[j]);
                        }
                        catch(NoSuchElementException e) {
                            size = new Dimension(200, 200);
                            m_fpm.doMessage(getFileName(),
                                       m_fpm.BAD_SCREEN_SIZE, section_lines[j]);
                        }
                    }
                    else if (key.equals(GUI__ORIGIN)) {
                        StringTokenizer st_3 =
                            new StringTokenizer(st_2.nextToken(), "x");
                        try {
                            int x = Integer.parseInt(st_3.nextToken());
                            int y = Integer.parseInt(st_3.nextToken());
                            int[] first_version_using_UL_as_location = 
                                                                { 5, 2, 13 };
                            bg_loc = new Point(x,y);
                        }
                        catch(NumberFormatException e) {
                            bg_loc = new Point(0, 0);
                            m_fpm.doMessage(getFileName(),
                                    m_fpm.BAD_NUMBER_FORMAT,
                                    section_lines[j]);
                        }
                        catch(NoSuchElementException e) {
                            bg_loc = new Point(0, 0);
                            m_fpm.doMessage(getFileName(),
                                           m_fpm.BAD_ORIGIN, 
                                           section_lines[j]);
                        }
                    }
                }

                if (name != null) {
                    int color_id = AlfiColors.getColorId(color);
                    _node_to_edit.coloredBox_add( name, color_id, bg_loc, 
                                size, bg_layer ); 
                }
            }
        }
    } 

        /**
         * Reads sections formatted as:
         *
         * <PRE>
         * LayeredGroup Group_6
         * {
         *     ColoredBox CommentBG_1
         *     CommentText CommentText_0
         * }
         * </PRE>
         */
    private void readLayeredGroupSections(ALFINode _node_to_edit,
                        Vector _sections) throws FileLoadingCanceledException {
        LayeredGroup new_group = null;
        for (int i = 0; i < _sections.size(); i++) {
            String[] section_lines = (String[]) _sections.get(i);

            StringTokenizer st_1 = new StringTokenizer(section_lines[0]);
            if (st_1.countTokens() == 2)  {
                st_1.nextToken();
                    // information on start line
                new_group = _node_to_edit.layeredGroup_add(st_1.nextToken());
            }
 
            for (int j = 2; j < section_lines.length - 1; j++) {
                ArrayList line_list = new ArrayList();

                while (! section_lines[j].equals(SECTION_END_MARKER) &&
                       ! section_lines[j].equals(GUI_INFO_MARKER) ) {
                    line_list.add(section_lines[j++]);
                }


                String[] lines = new String[line_list.size()];
                line_list.toArray(lines);
                ArrayList element_list = new ArrayList();

                    // cleanup first
                for (int k = 0; k < lines.length; k++) {
                    lines[k].trim();
                    if (lines[k].startsWith(ALFI_ONLY_MARKER)) {
                        lines[k] = 
                            lines[k].substring(ALFI_ONLY_MARKER.length());
                    }
                    lines[k].trim();
                }


                for (int k = 0; k < lines.length; k++) {
                    // get the type and name
                    String[] parts = lines[k].split("\\s+");
    
                    if (parts.length == 2) {
                        String type = parts[0];
                        String object_name = parts[1];

                            // Figure out what the element is
                        GroupableIF group_element = 
                            _node_to_edit.getElement(type, object_name);
                        
                        if (group_element != null) {
                            new_group.addGroupElement(group_element);
                        }
                    }
                }
            }
        }
    } 


    //**************************************************************************
    //***** Auxilliary methods *************************************************

        /**
         * This method takes one line from a file representing an ALFI node and
         * removes comment markers, ALFI only markers (which make the file
         * compatible with the modeler file reader), and trims white space.  It
         * also separates any section start and end markers onto their own
         * lines.  Thus the return of a string array, in case this line needs
         * to be represented by multiple lines actually.
         */
    private String[] cleanupFileLine(String _node_file_line) {
        String line = _node_file_line.trim();

            // remove ALFI-only marker if found.
        if (_node_file_line.startsWith(ALFI_ONLY_MARKER)) {
            line = _node_file_line.substring(ALFI_ONLY_MARKER.length());
        }
            // if a comment marker starts the line, leave it in, otherwise
            // remove the comment marker and all that follows it
        else {
            int i = line.indexOf(COMMENT_MARKER);
            if (i > 0) { line = line.substring(0, i - 1); }
        }

            // remove white space at beginning or end of line.
        line = line.trim();

            // separate section start and end markers onto their own lines,
            // but not if they are part of a comment
        Vector lines_vector = new Vector();
        if (line.startsWith(COMMENT_MARKER)) {
            lines_vector.addElement(line);
        }
        else {
            StringTokenizer st = new StringTokenizer(line,
                               SECTION_BEGIN_MARKER + SECTION_END_MARKER, true);
            while (st.hasMoreTokens()) {
                String segment = st.nextToken().trim();
                if (segment.length() > 0) {
                    lines_vector.addElement(new String(segment));
                }
            }
        }

        String[] lines = new String[lines_vector.size()];
        lines_vector.toArray(lines);

        return lines;
    }

    private String[] getFileContents() throws FileLoadingCanceledException {
        File java_file = m_root_node_file.createAssociatedJavaFile();
        return getFileContents(java_file);
    }

    private String[] getFileContents(File java_file)
                                          throws FileLoadingCanceledException {
        StringBuffer sb = new StringBuffer((int) java_file.length());
        char[] char_buffer = new char[4096];	// reads 4k at a time from disk
        int char_count = 0;

        try {
            FileReader reader = new FileReader(java_file);
            while ((char_count = reader.read(char_buffer)) != -1) {
                sb.append(char_buffer, 0, char_count);
            }
            reader.close();
        }
        catch(IOException e) {
            String message = "Parser.getFileContents() :\n" + "\tERROR: " + e;
            m_fpm.doWarning(getFileName(), message, "");
            return null;
        }
        return prepareFileContents(sb);
    }

    private String[] prepareFileContents(StringBuffer sb)
                                          throws FileLoadingCanceledException {
        StringTokenizer st = new StringTokenizer(sb.toString(), NL, true);
        Vector lines_vector = new Vector(st.countTokens());
        boolean b_completed_comment = false;
        m_root_node_comment = new String();
        while (st.hasMoreTokens()) {
            String next_line = st.nextToken();
            if (next_line.equals(NL)) { continue; }

                // get the top level node comment
            if (! b_completed_comment) {
                if (next_line.indexOf(VERSION_MARKER) >= 0) { ; }
                else if (next_line.indexOf(OLD_VERSION_MARKER) >= 0) { ; }
                else {
                    String line = (new String(next_line)).trim();
                    if (line.length() == 0) { ; }
                    else if (line.startsWith(ALFI_ONLY_MARKER)) {
                        b_completed_comment = true;
                    }
                    else if (line.startsWith(COMMENT_MARKER)) {
                        m_root_node_comment += line.substring(1).trim() + NL;
                        continue;
                    }
                    else { b_completed_comment = true; }
                }
            }

                 // deal with #quote_begin ... #quote_end multiline objects
            if (next_line.trim().startsWith(QUOTE_BEGIN)) {
                st.nextToken();    // get past \n
                StringBuffer sb_2 = new StringBuffer();
                String line = st.nextToken();
                while (! line.endsWith(QUOTE_END)) {
                    sb_2.append(line);
                    line = st.nextToken();
                }
                sb_2.setLength(sb_2.length() - 1);    // remove last \n
                    // leading quote character marks value as QUOTEd
                next_line =
                        "\"" + escapeSpecialCharacters(sb_2.toString()).trim();
            }

                 //deal with backslashed newlines
            if (next_line.endsWith("\\")) {
                while (next_line.startsWith(" ")) {    // remove begin spaces
                    next_line = next_line.substring(1);
                }
                next_line = "\"" + next_line;    // mark as quoted

                while (next_line.endsWith("\\")) {
                    next_line = next_line.substring(0, next_line.length() - 1);
                    next_line += st.nextToken() + st.nextToken();
                }
            }

                // turn other escaped characters into numerical char values
            if (next_line.indexOf('\\') >= 0) {
                StringBuffer sb_2 = new StringBuffer(next_line);
                for (int i = 1; i < sb_2.length(); i++) {

                        // create an escape value like "u00B7" (4 digit hex)
                    if (sb_2.charAt(i - 1) == '\\') {
                        String escape_value =
                                Integer.toHexString((int) sb_2.charAt(i));
                        while (escape_value.length() < 4) {
                            escape_value = "0" + escape_value;
                        }
                        escape_value = "u" + escape_value;

                        sb_2.deleteCharAt(i);
                        sb_2.insert(i, escape_value);
                    }
                }
                next_line = sb_2.toString();
            }

                // don't clean quoted lines
            if (next_line.startsWith("\"")) {
                next_line = reinsertEscapedChars(next_line);
                lines_vector.add(next_line);
            }
            else {
                String[] clean_lines = cleanupFileLine(next_line);
                for (int i = 0; i < clean_lines.length; i++) {
                    String line = reinsertEscapedChars(clean_lines[i]);
                    lines_vector.add(line);
                }
            }
        }
        String[] lines = new String[lines_vector.size()];
        lines_vector.toArray(lines);
        return lines;
    }

        /** Used to place \ characters in front of special characters which
          * might be inside a quoted section.
          */
    private String escapeSpecialCharacters(String _raw_line) {
        if (_raw_line.length() == 0) { return ""; }

        String[] special_chars = { "{", "}" };
        String line = new String(_raw_line);
        for (int i = 0; i < special_chars.length; i++) {
            String regexp = "\\" + special_chars[i];
            String escaped = "\\\\" + special_chars[i];
            line = line.replaceAll(regexp, escaped);
        }
        return line;
    }

        /** In the file, connections are described in text, so we must have a
          * way of finding a connections by description (e.g.,
          * "this in_1 -> B_0 o2") in the parser.
          */
    private ConnectionProxy getConnectionByDescription(ALFINode _node_to_edit,
                                              String _connection_description) {
        Object[] components = determineConnectionComponents(_node_to_edit,
                                                      _connection_description);
        ConnectionProxy[] connections = _node_to_edit.connections_get();
        for (int i = 0; i < connections.length; i++) {
            MemberNodeProxy source_member = connections[i].getSourceMember();
            if (components[0] != source_member) { continue; }

            GenericPortProxy source = connections[i].getSource();
            if (components[1] != source) { continue; }

            MemberNodeProxy sink_member = connections[i].getSinkMember();
            if (components[2] != sink_member) { continue; }

            GenericPortProxy sink = connections[i].getSink();
            if (components[3] != sink) { continue; }

            return connections[i];
        }

        return null;
    }

        /** In the file, ports are only identified by name, so we must have a
          * way of finding a port them by name in the parser.
          */
    private PortProxy getPortByNameAndIOType(ALFINode _node, String _port_name,
                                                                 int _io_type) {
        PortProxy[] ports = _node.ports_get();
        for (int i = 0; i < ports.length; i++) {
            if (_port_name.equals(ports[i].getName()) &&
                                          (_io_type == ports[i].getIOType())) {
                return ports[i];
            }
        }

        return null;
    }

        /**
         * In the file, junctions are identified only by name, so we must have a
         * way to find them by name in the parser.  Outside the parser, objects
         * are never identified by their names, which are changeable fields.
         */
    private JunctionProxy getJunctionByName(ALFINode _node, String _name) {
        JunctionProxy[] junctions = _node.junctions_get();
        for (int i = 0; i < junctions.length; i++) {
            if (junctions[i].getName().equals(_name)) { return junctions[i]; }
        }
        return null;
    }

        /** In the file, bundlers are identified only by name, so we must have a
         * way to find them by name in the parser.  Outside the parser, objects
         * are never identified by their names, which are changeable fields.
         */
    private BundlerProxy getBundlerByName(ALFINode _node, String _name) {
        BundlerProxy[] bundlers = _node.bundlers_get();
        for (int i = 0; i < bundlers.length; i++) {
            if (bundlers[i].getName().equals(_name)) { return bundlers[i]; }
        }
        return null;
    }

        /** After the load procedure, local names can be changed and should not
          * be relied upon.  But in the file, nodes can only be identified by
          * name, so this method is only used by the parser.
          */
    private ALFINode getDirectMemberNodeByName(ALFINode _container_node,
                                                          String _local_name) {
        ALFINode[] member_nodes =
                    _container_node.memberNodes_getDirectlyContained();
        for (int i = 0; i < member_nodes.length; i++) {
            if (member_nodes[i].determineLocalName().equals(_local_name)) {
                return member_nodes[i];
            }
        }
        return null;
    }

        /** This method finds a member node down to any depth of containment.
          * The _local_name must be the full relative name.
          */
    private ALFINode getAnyContainedNodeByName(ALFINode _container_node,
                                                          String _local_name) {
        String[] name_parts = _local_name.split("\\.", 2);
        ALFINode member =
                     getDirectMemberNodeByName(_container_node, name_parts[0]);
        if (name_parts.length == 1) {
            return member;
        }
        else { return getAnyContainedNodeByName(member, name_parts[1]); }
    }

        /**
         * General method for getting the next section header line plus any
         * section body which may follow the header line.
         */
    private String[] getNextSection(String[] _lines, int _header_line_no) {
        if (_lines.length == (_header_line_no + 1)) {    // last line
            String[] section_lines = new String[1];
            section_lines[0] = _lines[_header_line_no];
            return section_lines;
        }
        else if (_lines[_header_line_no + 1].startsWith(SECTION_BEGIN_MARKER)) {
            int section_depth = 0;
            for (int i = _header_line_no; i < _lines.length; i++) {
                if (_lines[i].startsWith(SECTION_BEGIN_MARKER)) {
                    section_depth++;
                }
                else if (_lines[i].startsWith(SECTION_END_MARKER)) {
                    section_depth--;
                    if (section_depth == 0) {
                        String[] section_lines =
                                            new String[i - _header_line_no + 1];
                        for (int j = _header_line_no; j <= i; j++) {
                            section_lines[j - _header_line_no] = _lines[j];
                        }
                        return section_lines;
                    }
                }
            }
                // error condition
            String message =
                "Parser.getNextSection(<lines>, " + _header_line_no + ") :\n" +
                "\tERROR: No section end found in:\n";
            for (int i = _header_line_no; i < _lines.length; i++) {
                message += "\t\t" + i + ": " + _lines[i] + "\n";
            }
            System.err.println(message);
            return null;
        }
        else {    // single line (header with no body)
            String[] section_lines = new String[1];
            section_lines[0] = _lines[_header_line_no];
            return section_lines;
        }
    }

        /**
          * Reinserts escaped characters in a string.  Escaped characters have
          * the format: /uXXXX where X is a hex digit.
          */
    private String reinsertEscapedChars(String _line_in) {
        if (_line_in.indexOf('\\') >= 0) {
            StringBuffer sb = new StringBuffer(_line_in);
            char[] escape_chars = new char[6];
            int escape_i = sb.toString().indexOf('\\');
            while (escape_i >= 0) {
                sb.getChars(escape_i, escape_i + 6, escape_chars, 0);
                if (escape_chars[1] == 'u') {
                    String hex_value = new String(escape_chars, 2, 4);

                    try {
                        Integer int_value = Integer.valueOf(hex_value, 16);
                        char escaped_char = (char) int_value.intValue();
                        sb.delete(escape_i, escape_i + 6);
                        sb.insert(escape_i, escaped_char);
                        escape_i = sb.toString().indexOf('\\');
                    }
                    catch(NumberFormatException e) {
                        System.err.println("Parser.insertEscapedChars(\"" +
                            _line_in + "\"):\n\tWARNING: Invalid escape " +
                            "sequence (1).");
                        break;
                    }
                }
                else {
                    System.err.println("Parser.insertEscapedChars(\"" +
                        _line_in + "\"):\n\tWARNING: Invalid escape sequence " +
                        "(2).");
                    break;
                }
            }
            return sb.toString();
        }
        else { return _line_in; }
    }

        /**
         * This method is only here now to read pre 5.2.24 files.
         *
         * Parses subsections formatted as:
         *
         * <PRE>
         * GUI_Connection this i3 -> zerocross_0 val
         * {
         * 8x40
         * 10x40
         * 10x177
         * 42x177
         * }
         *
         *    or, starting with version 5.2.21,
         *
         * GUI_Connection this i3 -> zerocross_0 val
         *   {  x=23  y=45  x=13  }
         *
         * </PRE>
         */
    private void parseAndSetConnectionPath(ALFINode _node_to_edit,
                    String[] _subsection) throws FileLoadingCanceledException {
        Alfi.warn("Parser.parseAndSetConnectionPath(" + _node_to_edit + "):" +
                  "\n\tThe node you are attempting to load was written long\n" +
                  "\tago by a now incompatable Alfi parser.  It can no\n" +
                  "\tlonger be read completely by Alfi.");
/*
            // this method is obsolete for files writen by 5.2.24 and above
        int[] connections_removed_from_gui_settings_version = { 5, 2, 24 };
        int[] file_version = m_root_node_file.getParseVersion();
        if (m_root_node_file.fileVersionEqualOrGreaterThan(
                              connections_removed_from_gui_settings_version)) {
            m_fpm.doMessage(getFileName(), m_fpm.OBSOLETE_METHOD_CALLED + "  " +
                            m_fpm.BAD_CONNECTION_PATH_DESCRIPTION, _subsection);
            return;
        }

        String connection_description =
              _subsection[0].substring(GUI__CONNECTION.length()).trim();
        ConnectionProxy connection =
              getConnectionByDescription(_node_to_edit, connection_description);

        if (connection == null) {
            m_fpm.doWarning(getFileName(),
                           m_fpm.CONNECTION_NOT_FOUND, connection_description);
            return;
        }

        if (! checkBracketsInMultilineSection(_subsection)) { return; }

        Integer[] path_definition = null;

            // format for describing connection paths changed at 5.2.21
        int[] new_path_version = { 5, 2, 21 };
        if (m_root_node_file.fileVersionEqualOrGreaterThan(new_path_version)) {
            if (! _subsection[2].startsWith(SECTION_END_MARKER)) {
                path_definition =
                    ConnectionProxy.parseStringPathDefinition(_subsection[2]);
                if (path_definition == null) {
                    m_fpm.doMessage(getFileName(),
                           m_fpm.BAD_CONNECTION_PATH_DESCRIPTION, _subsection);
                    return;
                }
            }
        }
            // old style path definition via points
        else {
            Vector path_points = new Vector();
            for (int i = 2; i < _subsection.length - 1; i++) {
                StringTokenizer st = new StringTokenizer(_subsection[i], "x");
                if (st.countTokens() != 2) {
                    m_fpm.doMessage(getFileName(),
                           m_fpm.BAD_CONNECTION_PATH_DESCRIPTION, _subsection);
                    return;
                }

                try {
                    int x = Integer.parseInt(st.nextToken());
                    int y = Integer.parseInt(st.nextToken());
                    path_points.add(new Point(x, y));
                }
                catch(NumberFormatException e) {
                    m_fpm.doMessage(getFileName(),
                           m_fpm.BAD_CONNECTION_PATH_DESCRIPTION, _subsection);
                    return;
                }
            }

            path_definition =
                    ConnectionProxy.pathPointsToPathDefinition(path_points);
        }

        connection.setPathDefinition(path_definition);
*/
    }

        /**
         * Does box 1 include box 2?  Returns true even if box 1 includes box 3
         * and box 3 includes box 2.
         */
    boolean box1IsIncludedInBox2(String _box_1, String _box_2) {
        HashMap box_2s_includes_map = (HashMap) ms_includes_map.get(_box_2);
        if (box_2s_includes_map != null) {
            if (box_2s_includes_map.containsKey(_box_1)) { return true; }
            for (Iterator i = box_2s_includes_map.keySet().iterator();
                                                          i.hasNext(); ) {
                String file_included_in_box_2 = (String) i.next();
                if (box1IsIncludedInBox2(_box_1, file_included_in_box_2)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void processBoxIncludes(String[] lines)
                                     throws AlfiIncludeFileNotFoundException,
                                            CyclicAlfiFileIncludeException,
                                            FileLoadingCanceledException {
        String containers_directory =
                    m_root_node_file.getCanonicalDirectory().getPath();

            // paths used as keys to avoid multiple entries of same path
        HashMap file_path_map = new HashMap();

        boolean b_inside_add_submodules = false;
        int bracket_depth = 0;
        for (int i = 0; i < lines.length; i++) {
             if (! b_inside_add_submodules) {
                if (lines[i].equals(MEMBER_NODE_ADD_MARKER)) {
                    b_inside_add_submodules = true;
                }
                continue;
            }

                // we are inside a Add_Submodules section
            if (lines[i].equals(SECTION_BEGIN_MARKER)) {
                bracket_depth++;
                continue;
            }
            else if (lines[i].equals(SECTION_END_MARKER)) {
                bracket_depth--;
                if (bracket_depth == 0) {
                    b_inside_add_submodules = false;
                }
                continue;
            }

            if (lines[i].startsWith(FILE_INCLUDE_MARKER)) {
                StringTokenizer st = new StringTokenizer(lines[i]);
                st.nextToken();
                String include_path = st.nextToken();
                String full_include_path = null;
                if (ALFIFile.isE2EPartialPath(include_path)) {
                    String e2e_partial_path = include_path;
                    full_include_path =
                                 determineCanonicalFilePath(e2e_partial_path);
                }
                    // include_path is full path (but may contain links?)
                else if (include_path.startsWith(File.separator)) {
                    try {
                        full_include_path =
                                    (new File(include_path)).getCanonicalPath();
                    }
                    catch(IOException e) {
                        System.err.println(e);
                        continue;
                    }
                }
                else {
                    full_include_path = (new File(containers_directory,
                                                       include_path)).getPath();
                }

                if (            (full_include_path != null) &&
                                (new File(full_include_path)).exists()) {

                        // check for cyclic includes
                    if (full_include_path.endsWith(
                                            ALFIFile.BOX_NODE_FILE_SUFFIX)) {
                        String roots_path = m_root_node_file.getCanonicalPath();
                        if (        (box1IsIncludedInBox2(roots_path,
                                                      full_include_path)) ||
                                    roots_path.equals(full_include_path) ) {
                            throw new CyclicAlfiFileIncludeException(
                                                     roots_path, lines[i]);
                        }
                        else {
                            ((HashMap) ms_includes_map.get(roots_path)).put(
                                                     full_include_path, null);
                        }
                    }

                    file_path_map.put(full_include_path, new Integer(i));

                        // add full_include_path to line[i]
                    lines[i] += " " + full_include_path;
                }
                else {
                    throw new AlfiIncludeFileNotFoundException(this,
                            m_root_node_file.getCanonicalPath(), include_path);
                }
            }
        }

        Set path_set = file_path_map.keySet();
        Iterator i = path_set.iterator();
        while (i.hasNext()) {
            String path = (String) i.next();
            if (! Alfi.getTheNodeCache().containsRootNode(path)) {
                new ALFIFile(path, m_fpm);    // node registers itself
            }
        }
    }

        /** Determines the canonical file path to the root node's file. */
    private String determineCanonicalFilePath(File _containers_directory,
                                              String _path_from_include_line) {
        if (ALFIFile.isE2EPartialPath(_path_from_include_line)) {
            String e2e_partial_path = _path_from_include_line;
            return determineCanonicalFilePath(e2e_partial_path);
        }
        else {
            File file = null;

                // where to search?  is it a _unix_ absolute path?
            if (_path_from_include_line.startsWith(File.separator)) {
                file = new File(_path_from_include_line);
            }
                // else prepend the containing node's directory
            else {
                file = new File(_containers_directory, _path_from_include_line);
            }

            if (! file.exists()) {    // file doesn't exist yet.  create it.
                try {
                    FileWriter fw = new FileWriter(file);
                    String signature = getVersionSignature() + NL;
                    System.err.println("Writing " + file.getCanonicalPath());
                    fw.write(signature, 0, signature.length());
                    fw.flush();
                    fw.close();
                }
                catch(IOException e) {
                    e.printStackTrace();
                    System.err.println("\tERROR: Problems creating new file: " +
                            "\"" + file.getPath() + "\"");
                    return null;
                }
            }

            try {
                if (file.exists()) { return file.getCanonicalPath(); }
            }
            catch(IOException e) {
                System.err.println(e);
                return null;
            }
        }

        return null;
    }

        /**
         * Determines the canonical file path to the root node's file, taking
         * an E2E partial path (from a line like: #include *X.box) as a
         * parameter.  Finding this file is independent of the container node's
         * file location.
         */
    private String determineCanonicalFilePath(String _e2e_partial_path) {
        if (ALFIFile.isE2EPartialPath(_e2e_partial_path)) {

                // remove the e2e_partial_path identifier
            _e2e_partial_path = _e2e_partial_path.substring(
                                 ALFIFile.E2E_PARTIAL_PATH_IDENTIFIER.length());

            StringTokenizer st =
                    new StringTokenizer(Alfi.getE2EPath(), File.pathSeparator);
            while (st.hasMoreTokens()) {
                String directory = st.nextToken();
                File file = new File(directory, _e2e_partial_path);
                try {
                    if (file.exists()) { return file.getCanonicalPath(); }
                }
                catch(IOException e) {
                    System.err.println(e);
                    return null;
                }
            }

                // error dealt with in caller
            return null;
        }
        else {
            System.err.println("Parser.determineCanonicalFilePath(" +
                _e2e_partial_path + "):\n" +
                "\tWARNING: Invalid E2E Partial Path.");
            return null;
        }
    }

        /** Stores sections of the file in different slots so we can create
          * elements in the order we like instead of the order listed in the
          * file.
          */
    private HashMap slotSectionsIntoVectorHolders(String[] _lines)
                                          throws FileLoadingCanceledException {
        HashMap section_vector_map = new HashMap();
        for (int i = 0; i < _lines.length; ) {    // note lack of i++
            String[] section_lines = getNextSection(_lines, i);
            i += section_lines.length;

                // there is a vector for each type of section (e.g. port adds)
            StringTokenizer st = new StringTokenizer(section_lines[0]);
            int token_count = st.countTokens();
            if (! st.hasMoreTokens()) {
                    // throws an AlfiFileFormatException
                m_fpm.doWarning(getFileName(), m_fpm.BAD_SECTION_START,
                                                             section_lines[0]);
            }
            String section_marker = st.nextToken();

                // for quoted values (marked by an initial " character), strip
                // the " off the start for determining a section marker
            if (section_marker.startsWith("\"")) {
                section_marker = section_marker.substring(1);
            }

                //   special case is parameter modifications which have no
                //   marker, but are merely in the format "name = value".
                //   also, a #include can be used to specify a file with
                //   settings values in it.
            if (section_marker.equals(FILE_INCLUDE_MARKER)) {
                section_marker = PARAMETER_SETTING_MARKER;
            }
            else if (! isValidSectionMarker(section_marker)) {
                if (st.hasMoreTokens() && st.nextToken().equals("=") &&
                                                          st.hasMoreTokens()) {
                    section_marker = PARAMETER_SETTING_MARKER;
                }
                    // deprecated parameter declaration data type "bool".
                    // should be "boolean".  set to start warnings 4/1/2007.
                    // when this code is removed in the future, also look in
                    // ParameterDeclaration.parse() for obsolete code.
                else if (section_marker.equals("bool")) {
                    GregorianCalendar start_warnings_date =
                                             new GregorianCalendar(2007, 4, 1);
                    if (start_warnings_date.before(new GregorianCalendar())) {
                        m_fpm.processDeprecatedParamBoolTypeWarning(
                                                                getFileName());
                    }
                    section_marker = "boolean";
                }
                else {
                        // throws an AlfiFileFormatException
                    m_fpm.doWarning(getFileName(), m_fpm.BAD_SECTION_MARKER,
                                                             section_lines[0]);
                }
            }

            Vector section_vector =
                               (Vector) section_vector_map.get(section_marker);
            if (section_vector == null) {
                section_vector = new Vector();
                section_vector_map.put(section_marker, section_vector);
            }

            section_vector.addElement(section_lines);
        }

        return section_vector_map;
    }

    private boolean isValidSectionMarker(String _marker) {
            // standard markers
        for (int i = 0; i < MARKERS.length; i++) {
            if (MARKERS[i].equals(_marker)) { return true; }
        }

            // parameter declaration types
        for (int i = 0; i < ParameterDeclaration.PARAMETER_TYPES.length; i++) {
            if (ParameterDeclaration.PARAMETER_TYPES[i].equals(_marker)) {
                return true;
            }
        }

        return false;
    }

        /**
         * This method takes the lines configuring a particular node, looks
         * for any comment lines in it, creates a comment for the node
         * if found, and strips said comment lines from the lines array.  The
         * returned object array is the comment string and then the stripped
         * lines string array.
         */
    private Object[] stripNodeComment(String[] _lines) {
        ArrayList non_comment_lines_list = new ArrayList();
        String comment = new String();
        int depth = 0;    // for tracking descent into subsections
        for (int i = 0; i < _lines.length; i++) {
            if (_lines[i].startsWith(SECTION_BEGIN_MARKER)) {
                depth++;
                non_comment_lines_list.add(_lines[i]);
            }
            else if (_lines[i].startsWith(SECTION_END_MARKER)) {
                depth--;
                non_comment_lines_list.add(_lines[i]);
            }
            else if ((depth == 0) && _lines[i].startsWith(COMMENT_MARKER)) {
                String line = _lines[i].substring(COMMENT_MARKER.length());
                comment += line.trim() + NL;
            }
            else { non_comment_lines_list.add(_lines[i]); }
        }
        comment = comment.trim();

        String[] non_comment_lines = new String[non_comment_lines_list.size()];
        non_comment_lines_list.toArray(non_comment_lines);

        Object[] comment_and_stripped_lines = { comment, non_comment_lines };
        return comment_and_stripped_lines;
    }

        /**
         * Checks for the brackets in a generic multiline section:
         *
         * identifier_line
         * {
         * some_line_of_info
         * another_line_of_info
         * }
         *
         * Informs user and log if the brackets are not found in the correct
         * locations, and returns false in such cases.  Otherwise returns true.
         */
    private boolean checkBracketsInMultilineSection(String[] _section_lines)
                                          throws FileLoadingCanceledException {
        if (! _section_lines[1].equals(SECTION_BEGIN_MARKER)) {
            m_fpm.doWarning(getFileName(), m_fpm.BAD_SECTION_BEGIN,
                                                               _section_lines);
            return false;
        }
        else if (! _section_lines[_section_lines.length - 1].equals(
                                                         SECTION_END_MARKER)) {
            m_fpm.doWarning(getFileName(), m_fpm.BAD_SECTION_END,
                                                               _section_lines);
            return false;
        }
        else { return true; }
    }

        /** Utility for parsing sections formatted as (be aware that many parts
          * are optional and/or mutually exclusive):
          *
          * <PRE>
          * this b -> __BUNDLER__ Bundler_1
          * {
          * IS_BUNDLE
          * IS_BUNDLER_PRIMARY_INPUT
          * IS_BUNDLER_PRIMARY_OUTPUT
          * AS_BUNDLER_INPUT__NAME data_1
          * AS_BUNDLER_OUTPUT__NAME_EXTRACTED data_1
          * AS_BUNDLER_OUTPUT__NAME_EXTRACTED data_2
          * AS_BUNDLER_OUTPUT__NAME_EXTRACTED data_3
          * x=30 y=60
          * }
          * this y -> this yy
          * {
          * x=30 y=60
          * }
          * this y -> this yy
          * {
          * z=cw|ccw    // z=cw signals CW dog-leg, z=ccw gives CCW dog-leg
          * }
          * </PRE>
          *
          * Returns Object[n][3] where n will be the number of connection
          * descriptions, and the individual [i][] will be composed of 
          *     { connection_description (String),
          *       path_description (String),
          *       config_map (HashMap) }
          *
          * e.g., { "this i1 -> xor_0 a", "x=null y=84 x=54 . . .", config_map }
          *
          * The config_map may contain these key/value pairs (all are optional,
          * and some are mutually exclusive.  The keys are String constant names
          * defined in Parser.):
          *
          * BUNDLE__IS_BUNDLE / null                  // assumed true if exists
          * BUNDLE__IS_BUNDLER_PRIMARY_INPUT / null   // assumed true if exists
          * BUNDLE__IS_BUNDLER_PRIMARY_OUTPUT / null  // assumed true if exists
          */
    private Object[][] getIndividualConnectionDescriptions(
                                     ALFINode _node_to_edit, String[] _lines)
                                          throws FileLoadingCanceledException {
        ArrayList connection_description_list = new ArrayList();
        int i = 2;

        MAIN_LOOP: while (i < _lines.length - 1) {
            Object[] description = new Object[3];
            HashMap config_map = new HashMap();
            try {
                description[0] = _lines[i];    // "this i1 -> xor_0 a" part
                i++;

                if (! _lines[i].equals(SECTION_BEGIN_MARKER)) {
                    m_fpm.doWarning(getFileName(),
                                    m_fpm.BAD_ADD_CONNECTIONS_SECTION, _lines);
                    break MAIN_LOOP;
                }
                i++;

                while (! _lines[i].equals(SECTION_END_MARKER)) {
                    if (_lines[i].equals(BUNDLE__IS_BUNDLE)) {
                        config_map.put(BUNDLE__IS_BUNDLE, null);
                    }
                    else if (_lines[i].equals(
                                           BUNDLE__IS_BUNDLER_PRIMARY_INPUT)) {
                        config_map.put(BUNDLE__IS_BUNDLER_PRIMARY_INPUT, null);
                    }
                    else if (_lines[i].equals(
                                          BUNDLE__IS_BUNDLER_PRIMARY_OUTPUT)) {
                        config_map.put(BUNDLE__IS_BUNDLER_PRIMARY_OUTPUT, null);
                    }
                    else if (_lines[i].startsWith("x=")) {
                        description[1] = _lines[i];    // x=36 y=102 x=66 ...
                    }
                    else if (_lines[i].startsWith("z=")) {
                        description[1] = _lines[i];    // z=cw|ccw (dog-leg)
                    }
                    else {
                        m_fpm.doWarning(getFileName(),
                                    m_fpm.BAD_ADD_CONNECTIONS_SECTION, _lines);
                        break MAIN_LOOP;
                    }

                    i++;
                }

                description[2] = config_map;
            }
            catch(ArrayIndexOutOfBoundsException e) {
                m_fpm.doWarning(getFileName(),
                                    m_fpm.BAD_ADD_CONNECTIONS_SECTION, _lines);
            }

            connection_description_list.add(description);

            i++;
        }

        Object[][] connection_descriptions =
                              new Object[connection_description_list.size()][];
        connection_description_list.toArray(connection_descriptions);

        return connection_descriptions;
    }

    public static String getVersionSignature() {
        return (ALFI_ONLY_MARKER + " " + VERSION_MARKER + " " +
                                                  Alfi.ms_alfi_version);
    }

    public String getFileName() {
        String file_name = 
            m_root_node_file.createAssociatedJavaFile().getName();
        try {
            file_name =
                m_root_node_file.createAssociatedJavaFile().getCanonicalPath();
        } catch (IOException ioe) {
        }
        return file_name;
    }

        /** Ask the user for other places to search for primitves. */
    private boolean findMissingPrimitive(String _primitive_type_name,
                           String _include_line, String _container_file_path) {
        String title = "Primitive Not Found";
        String message = "A primitive by the name \"" + _primitive_type_name +
            "\", requested in the line ";
        message = TextFormatter.formatMessage(message);

        message += "\n\n" + _include_line;
        String s = " in the file";
        message += "\n\n" + s + "\n\n" + _container_file_path;
        s = "has not been loaded.  This may be due to an error in the " +
            "current value of the E2E_PATH search path, which is used to " +
            "find primitive files.";
        message += "\n\n" + TextFormatter.formatMessage(s);

        s = "It is recommended that you modify the E2E_PATH value at this " +
            "time in order to find the primitives required.  It is likely " +
            "that more problems in loading box files into Alfi will occur " +
            "if this is not done.";
        message += "\n\n" + TextFormatter.formatMessage(s);

        s = "Modify the E2E_PATH search path now?";
        message += "\n\n" + TextFormatter.formatMessage(s);

        String[] options = { "Modify E2E_PATH", "Cancel Load" };
        int response = JOptionPane.showOptionDialog(null, message, title,
                   JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                   null, options, options[0]);
        return (response == 0);
    }
}
