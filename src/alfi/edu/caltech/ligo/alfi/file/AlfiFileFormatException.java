package edu.caltech.ligo.alfi.file;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.tools.*;

    /**
     * This exception occurs when something in an Alfi file does not fit the
     * required format.
     */
public class AlfiFileFormatException extends AlfiException {
        /** Defaults to an ALFI_WARNING. */
    public AlfiFileFormatException(String _custom_message, String _file_path,
                                                      String _offending_text) {
        this(_custom_message, _file_path, _offending_text, ALFI_WARNING);
    }

    public AlfiFileFormatException(String _custom_message, String _file_path,
                                  String _offending_text, int _exception_type) {

            // this ridiculous construction is because super has to be first
        super(

            "<PRE>" +

            TextFormatter.formatMessage("An error exists in the format of " +
                "the file " + _file_path + ".  " +
                TextFormatter.formatMessage(_custom_message)) +

            (((_offending_text != null) && (_offending_text.length() > 0)) ?
                "\n\n  The line(s)" + "\n\n" + _offending_text + "\n\n" +
                TextFormatter.formatMessage("are not valid in the context in " +
                "which they are written (or perhaps at all.)") : "") +

            "</PRE>", _exception_type);
    }

    protected String getMessageFooter() {
        String footer = new String();
        switch (m_exception_type) {
          case ALFI_WARNING:
            footer += "The poorly formatted information has been ignored " +
                "by Alfi, and will not be incorporated in any way into this " +
                "session, nor in any resulting file written.";
            break;
          default:
            footer += super.getMessageFooter();
            break;
        }

        return footer;
    }
}	
