package edu.caltech.ligo.alfi.file;

import java.io.*;
import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;


    /** This object is used to load all UDPs in the E2E_PATH. */
public class UDPLoader {

    public static String UDP_LOADER_NAME = "udp_loader";
    public static String UDP_LOADER_FILE_NAME = UDP_LOADER_NAME + ".box";
 
    public UDPLoader(String _search_path, FileFormatProblemManager _ffpm){
        this(_search_path, null, _ffpm);
    }

        /** Constructor.  Searches the search path for primitive files and
          * creates an ALFIFile for each.  _names_to_ignore is a list of names
          * of primitives to ignore the loading of (most likely because they've
          * already been loaded.)
          */
    public UDPLoader(String _search_path, String[] _names_to_ignore,
                                              FileFormatProblemManager _ffpm) {
        if (_names_to_ignore == null) { _names_to_ignore = new String[0]; }

        StringTokenizer st =
                          new StringTokenizer(_search_path, File.pathSeparator);

        HashMap file_vs_udpName_map = new HashMap();
        while (st.hasMoreTokens()) {
            File directory = new File(st.nextToken());
            File[] udp_files = directory.listFiles(
                new FilenameFilter() {
                    public boolean accept(File f, String s) {
                        return s.endsWith(ALFIFile.UDP_FILE_SUFFIX);
                    }
                } );
            if (udp_files == null) { continue; }

            for (int i = 0; i < udp_files.length; i++) {
                String name = udp_files[i].getName();
                boolean b_ignore_name = false;
                for (int j = 0; j < _names_to_ignore.length; j++) {
                    if (name.equals(_names_to_ignore[j])) {
                        b_ignore_name = true;
                        break;
                    }
                }
                if ((! b_ignore_name) &&
                             (! file_vs_udpName_map.containsKey(name))) {
                    file_vs_udpName_map.put(name, udp_files[i]);
                }
            }
        }

            // create a dummy box file which includes all these UDPs and
            // can then be read in
        StringBuffer buffer = new StringBuffer();
        buffer.append(Parser.getVersionSignature());
        buffer.append(Parser.NL);
        buffer.append(Parser.USER_DEFINED_PRIMITIVES_MARKER);
        buffer.append(Parser.NL);
        buffer.append(Parser.SECTION_BEGIN_MARKER);
        buffer.append(Parser.NL);
        for (Iterator i = file_vs_udpName_map.keySet().iterator();i.hasNext();){
             buffer.append(Parser.FILE_INCLUDE_MARKER);
            buffer.append(" ");
            buffer.append(ALFIFile.E2E_PARTIAL_PATH_IDENTIFIER);
            buffer.append((String) i.next());
            buffer.append(Parser.NL);
        }
        buffer.append(Parser.SECTION_END_MARKER);

            // write dummy box
        String file_path = Alfi.LOCAL_UTILITY_DIRECTORY + 
                                                    "/" +  UDP_LOADER_FILE_NAME;
        File udp_loader_file =
                      new File(Alfi.LOCAL_UTILITY_DIRECTORY, 
                                                          UDP_LOADER_FILE_NAME);
        if (MiscTools.writeSmallTextFile(udp_loader_file, buffer.toString())) {
            try {
                file_path = udp_loader_file.getCanonicalPath();
                ALFIFile alfi_file = new ALFIFile(file_path, _ffpm);
            }
            catch(AlfiIncludeFileNotFoundException e) {
                String warning = "AlfiIncludeFileNotFoundException thrown " +
                    "while loading  " + file_path + " .";
                warning = TextFormatter.formatMessage(warning);
                warning += "\n\nFile not loaded.";
                Alfi.warn(warning);
            }
            catch(CyclicAlfiFileIncludeException e) {
                String warning = e.getMessage() + "\n\n" +
                  "This error should never occur while loading the udp_loader.";
                Alfi.warn(warning);
            }
            catch(FileLoadingCanceledException _e) {
                ALFINodeCache node_cache = Alfi.getTheNodeCache();
                String file_loading = _e.getFileLoading();
                ALFINode possibly_corrupt_node =
                                          node_cache.getRootNode(file_loading);
                if (possibly_corrupt_node != null) {
                    node_cache.removeNode(possibly_corrupt_node);
                }

                    // deal with file that set cancel flag (possibly different)
                String flagged_file_path = _e.getFlaggingFile();
                if ((flagged_file_path != null) &&
                                  (! flagged_file_path.equals(file_loading))) {
                    ALFINode another_possibly_corrupt_node =
                                     node_cache.getRootNode(flagged_file_path);
                    if (another_possibly_corrupt_node != null) {
                        node_cache.removeNode(another_possibly_corrupt_node);
                    }
                }
            }
            catch(IOException e) { System.err.println(e); }
        }
    }
}
