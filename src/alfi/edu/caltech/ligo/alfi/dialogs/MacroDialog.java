package edu.caltech.ligo.alfi.dialogs;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import edu.caltech.ligo.alfi.bookkeeper.*;


public class MacroDialog extends JDialog {
    public static final int CANCEL = 0;
    public static final int OK = 1;

    private final static int WIDTH = 600;
    private final static int HEIGHT = 600;

    private JTextPane m_notes_text_widget;    
    private JTextPane m_comment_text_widget;    
    private JTextPane m_value_text_widget;    
    
    private ALFINode m_node;
    private Macro m_macro;

    private String m_value;
    private String m_comment;

    private int m_return_status;

    public MacroDialog(Frame frame, ALFINode _node) {
        super(frame, new String(_node + " - Macro"), true);

        m_node = _node;
        m_macro = m_node.macro_getLocal();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(WIDTH, HEIGHT));
        setLocation((screenSize.width - WIDTH)/2,
                                               (screenSize.height - HEIGHT)/2);

        initComponents();
        pack();

        setResizable(false);

        m_value = null;
        m_comment = null;
        m_return_status = CANCEL;
    }

    public int getReturnStatus() {
        return m_return_status;
    } 

        /** Returns { value, comment } (either of which may be null.) */
    public String[] getNewMacroData() {
        String[] return_data = { m_value, m_comment };
        return return_data;
    }

    private void initComponents() {
        Container content_pane = getContentPane();
        content_pane.setLayout(new BoxLayout(content_pane, BoxLayout.Y_AXIS));

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent _event) {
                closeDialog(_event);
            }
        });

        SettingsPanel comments_panel =
                    new SettingsPanel(this, "Comment", WIDTH, HEIGHT * 20/100);
        comments_panel.add(createCommentsComponent());
        content_pane.add(comments_panel);

        SettingsPanel value_panel =
                new SettingsPanel(this, "Macro Value", WIDTH, HEIGHT * 50/100);
        value_panel.add(createValueComponent());
        content_pane.add(value_panel);

        SettingsPanel notes_panel =
                 new SettingsPanel(this, "Help Notes", WIDTH, HEIGHT * 20/100);
        notes_panel.add(createNotesComponent());
        content_pane.add(notes_panel);

        JPanel button_panel;    
        button_panel = new JPanel();        
        button_panel.setLayout(new FlowLayout (FlowLayout.CENTER, 5, 5));

        JButton ok_button;    
        ok_button = new JButton();        
        ok_button.setText("OK");
        ok_button.setMnemonic('O');
        ok_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent _event) {
                okButtonActionPerformed(_event);
            }
        });   
        button_panel.add(ok_button);
        getRootPane().setDefaultButton(ok_button);

        JButton cancel_button;    
        cancel_button = new JButton();        
        cancel_button.setText("Cancel");
        cancel_button.setMnemonic('C');
        cancel_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent _event) {
                cancelButtonActionPerformed(_event);
            }
        });   

        button_panel.add(cancel_button);

        content_pane.add(button_panel);
    }

    private void okButtonActionPerformed(ActionEvent _event) {
        m_value = m_value_text_widget.getText();
        m_comment = m_comment_text_widget.getText();

        doClose(OK);
    }

    private void cancelButtonActionPerformed(ActionEvent _event) {
        doClose(CANCEL);
    }

    private Component createValueComponent() {
        m_value_text_widget = new JTextPane();
        m_value_text_widget.setBorder(new EtchedBorder());        
        m_value_text_widget.setFont(Font.getFont("monospaced"));
        if (m_macro != null) {
            String value = m_macro.getValue();
            if (value != null) {
                m_value_text_widget.setText(value);
            }
        }

        JScrollPane scroll_pane = new JScrollPane(m_value_text_widget, 
                                   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll_pane.setBorder(BorderFactory.createLoweredBevelBorder());
        return scroll_pane;
    }

    private Component createCommentsComponent() {
        m_comment_text_widget = new JTextPane();
        m_comment_text_widget.setBorder(new EtchedBorder());

        if (m_macro != null) {
            String comment = m_macro.getComment();
            if (comment != null) {
                m_comment_text_widget.setText(comment);
            }
        }

        JScrollPane scroll_pane = new JScrollPane(m_comment_text_widget,
                                   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll_pane.setBorder(BorderFactory.createLoweredBevelBorder());
        return scroll_pane;
    }

    private Component createNotesComponent() {
        m_notes_text_widget = new JTextPane();
        String notes =
            "Backslashes may be used to continue multi-line macro values.";

        m_notes_text_widget.setText(notes.trim());
        m_notes_text_widget.setEditable(false);
        m_notes_text_widget.setBackground(Color.lightGray);

        JScrollPane scroll_pane = new JScrollPane(m_notes_text_widget,
                                   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll_pane.setBorder(BorderFactory.createLoweredBevelBorder());
        return scroll_pane;
    }

    private void closeDialog (WindowEvent _event) {
        doClose(CANCEL);
    }

    private void doClose (int _return_status) {
        m_return_status = _return_status;
        setVisible(false);
    }   
}
