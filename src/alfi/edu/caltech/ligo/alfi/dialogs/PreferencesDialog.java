package edu.caltech.ligo.alfi.dialogs;

import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;

public class PreferencesDialog extends JDialog {

    private ArrayList m_e2e_path_list;
    private JPanel m_e2e_panel;
    private JButton m_e2e_save_changes_button;
    private JButton m_e2e_reset_changes_button;
    private boolean mb_e2e_path_changed_and_reload_occurred;
    private final String m_original_e2e_path;

    private JPanel m_cwd_panel;
    private final String m_original_cwd_path;

    private JPanel m_default_browser_panel;
    private String m_default_browser;

    private JPanel m_backup_options_panel;

    public PreferencesDialog(JFrame _parent) {
        super(_parent, "Alfi Preferences", true);

        JTabbedPane tab_pane = new JTabbedPane();
        tab_pane.setPreferredSize(new Dimension(600, 450));

        {
            m_e2e_panel = new JPanel();
            JScrollPane scroll_pane = new JScrollPane(m_e2e_panel,
                           ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                           ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            tab_pane.add(scroll_pane, "E2E_PATH");

            m_original_e2e_path = new String(Alfi.E2E_PATH);
            setE2EPathList(Alfi.E2E_PATH);

            mb_e2e_path_changed_and_reload_occurred = false;
        }

        {
            m_cwd_panel = new JPanel();
            tab_pane.add(m_cwd_panel, "Working Directory");

            String cwd = null;
            try { cwd = Alfi.WORKING_DIRECTORY.getCanonicalPath(); }
            catch(IOException _e) { _e.printStackTrace(); }

            m_original_cwd_path = new String(cwd);
            updateWorkingDirectoryPanel(cwd);
        }

        {
            m_default_browser_panel = new JPanel();
            tab_pane.add(m_default_browser_panel, "Default Web Browser");

            String default_browser = null;
            default_browser = Alfi.getDefaultBrowser();
            
            if (default_browser == null) {
                default_browser = "";
            }

            m_default_browser = new String(default_browser);
            updateDefaultBrowserPanel(default_browser);
        }

        {
            m_backup_options_panel = new JPanel();
            tab_pane.add(m_backup_options_panel, "Backup Files");
            updateBackupOptions();
        }

        getContentPane().add(tab_pane, BorderLayout.CENTER);

        JButton close_button = new JButton("Close");
        close_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    dispose();
                }
            });
        Box box = Box.createHorizontalBox();
        box.add(Box.createHorizontalGlue());
        box.add(close_button);
        box.add(Box.createHorizontalGlue());
        getContentPane().add(box, BorderLayout.SOUTH);

        pack();
    }

    private void searchForDotAlfiDirectory(File parent_directory) {

        File[] dot_alfi_dirs = parent_directory.listFiles(
            new FilenameFilter() {                // anon class def
                public boolean accept(File f, String s) {
                    return s.equals(".alfi");
                }
            });
  
        for (int i = 0; i < dot_alfi_dirs.length; i++) {
            // This means that it's one of the backup files.
            File[] backup_files = dot_alfi_dirs[i].listFiles();
            for (int j = 0; j < backup_files.length; j++) {
                System.err.println("Removing "+ backup_files[j]);
                backup_files[j].delete();
            }
            System.err.println("Removing the directory " + dot_alfi_dirs[i]);
            if (!dot_alfi_dirs[i].delete()) {
                System.err.println("Failed to remove the directory " +
                                                       dot_alfi_dirs[i] + ".");
            }
        }

        File[] other_files = parent_directory.listFiles();

        for (int i = 0; i < other_files.length; i++) {
            if (other_files[i].isDirectory()) {
                searchForDotAlfiDirectory(other_files[i]);
            }
        }
    }


    private void updateBackupOptions() {
        m_backup_options_panel.removeAll();
        m_backup_options_panel.repaint();

        JLabel title = new JLabel("Backup file options");
        String notice = "Do you wish to delete all files in ALFI's backup " +
                                                          "directory (.alfi)?";
        notice = TextFormatter.formatMessage(notice, 80);

        JTextArea notice_area = new JTextArea(notice);
        notice_area.setFont(title.getFont());
        notice_area.setEditable(false);

        JButton delete_button = new JButton("Delete");
        delete_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    searchForDotAlfiDirectory(Alfi.WORKING_DIRECTORY);
                }
            });
        
        m_backup_options_panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 3, 0, 0);

        int i = 0;

        c.gridy = i++;
        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.insets = new Insets(5, 3, 5, 3);
        m_backup_options_panel.add(title, c);

        c.gridy = i++;
        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 3;
        m_backup_options_panel.add(notice_area, c);

        c.gridy = i++;
        c.gridx = 0;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.CENTER;
        m_backup_options_panel.add(notice_area, c);
        c.gridx = 1;
        m_backup_options_panel.add(delete_button, c);

        pack();
    }

    private void updateWorkingDirectoryPanel(String _path) {
        m_cwd_panel.removeAll();
        m_cwd_panel.repaint();

        JLabel title = new JLabel("Set Working Directory");
        String notice =
            "[Currently, because of startup complexities, changes in the " +
            "working directory requires that Alfi be restarted.  Apologies " +
            "for the inconvenience.]";
        notice = TextFormatter.formatMessage(notice, 80);

        JTextArea notice_area = new JTextArea(notice);
        notice_area.setFont(title.getFont());
        notice_area.setEditable(false);

        final JTextField cwd_field = new JTextField(30);
        cwd_field.setText(_path);

        JButton browse_path_button = new JButton("Browse");
        browse_path_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    JFileChooser chooser =
                                  new JFileChooser(Alfi.WORKING_DIRECTORY);
                    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    int response = chooser.showDialog(null, "Select Directory");
                    if (response == JFileChooser.APPROVE_OPTION) {
                        File file = chooser.getSelectedFile();
                        if (file != null) {
                            while ((! file.exists()) || file.isFile()) {
                                file = file.getParentFile();
                                if (file == null) { return; }
                            }
                            if (file.exists() && file.isDirectory()) {
                                cwd_field.setText(file.toString());
                            }
                        }
                    }
                }
            });

        JButton reset_button = new JButton("Reset");
        reset_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    cwd_field.setText(m_original_cwd_path);
                }
            });

        JButton save_button = new JButton("Change Working Directory and Quit");
        save_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    String path = cwd_field.getText();
                    String cwd_path = Alfi.WORKING_DIRECTORY.getPath();
                    if (! path.equals(cwd_path)) {
                        File file = new File(path);
                        if (file.exists() && file.isDirectory()) {
                            Alfi.WORKING_DIRECTORY = file;
                            Alfi.getMainWindow().quit();

                                // if quit() is cancelled, reset
                            Alfi.WORKING_DIRECTORY =
                                                 new File(m_original_cwd_path);
                            cwd_field.setText(m_original_cwd_path);
                        }
                        else { cwd_field.setText(m_original_cwd_path); }
                    }
                }
            });

        m_cwd_panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 3, 0, 0);

        int i = 0;

        c.gridy = i++;
        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.insets = new Insets(5, 3, 5, 3);
        m_cwd_panel.add(title, c);

        c.gridy = i++;
        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 3;
        m_cwd_panel.add(notice_area, c);

        c.gridy = i++;
        c.gridx = 0;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.CENTER;
        m_cwd_panel.add(cwd_field, c);
        c.gridx = 1;
        m_cwd_panel.add(browse_path_button, c);
        c.gridx = 2;
        m_cwd_panel.add(reset_button, c);

        c.gridy = i++;
        c.gridx = 0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        m_cwd_panel.add(save_button, c);

        pack();
    }

    private void setE2EPathList(String _path_string) {
        ArrayList e2e_path_list = new ArrayList();
        String[] e2e_path_parts = _path_string.split(File.pathSeparator);
        for (int i = 0; i < e2e_path_parts.length; i++) {
            e2e_path_list.add(e2e_path_parts[i]);
        }
        setE2EPathList(e2e_path_list);
    }

    private void setE2EPathList(ArrayList _path_list) {
        boolean b_initial_call = (m_e2e_path_list == null);

        if (_path_list != null) {
            m_e2e_path_list = _path_list;
        }
        else { m_e2e_path_list = new ArrayList(); }

        updateE2EPane();

        m_e2e_save_changes_button.setEnabled(! b_initial_call);
        m_e2e_reset_changes_button.setEnabled(! b_initial_call);
    }

    private void updateE2EPane() {
        m_e2e_panel.removeAll();
        m_e2e_panel.repaint();

        m_e2e_panel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 3, 0, 0);

        int i = 0;
        c.gridy = i++;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        m_e2e_panel.add(new JLabel("E2E_PATH Directories", JLabel.CENTER), c);
        c.gridx = 1;
        m_e2e_panel.add(new JLabel("Re-order", JLabel.CENTER), c);

        for (int j = 0; j < m_e2e_path_list.size(); j++) {
            c.gridy = i++;
            String directory_name = (String) m_e2e_path_list.get(j);

            c.anchor = GridBagConstraints.WEST;
            c.gridx = 0;
            m_e2e_panel.add(new JLabel(directory_name, JLabel.CENTER), c);

            c.anchor = GridBagConstraints.CENTER;
            c.gridx = 1;
            final int index = j;
            final int max_index = m_e2e_path_list.size() - 1;
            JSpinner spinner = new JSpinner();
            spinner.addChangeListener(
                new ChangeListener() {
                    public void stateChanged(ChangeEvent _e) {
                        JSpinner spinner = (JSpinner) _e.getSource();
                        int value = ((Integer) spinner.getValue()).intValue();

                        if (value != 0) {
                            int new_index = index - value;
                            if ((new_index >= 0) && (new_index <= max_index)) {
                                Object moving = m_e2e_path_list.remove(index);
                                m_e2e_path_list.add(new_index, moving);
                                updateE2EPane();
                            }
                            spinner.setValue(new Integer(0));
                        }
                    }
                });
            spinner.setEditor(new JLabel(" "));
            m_e2e_panel.add(spinner, c);

            c.anchor = GridBagConstraints.CENTER;
            c.gridx = 2;
            JButton button = new JButton("Delete");
            button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent _e) {
                        m_e2e_path_list.remove(index);
                        updateE2EPane();
                    }
                });
            m_e2e_panel.add(button, c);
        }

        c.gridy = i++;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 0;
        final JTextField add_to_path_text_field = new JTextField(30);
        m_e2e_panel.add(add_to_path_text_field, c);

        c.gridx = 1;
        JButton browse_path_button = new JButton("Browse");
        browse_path_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    JFileChooser chooser =
                                  new JFileChooser(Alfi.WORKING_DIRECTORY);
                    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    int response = chooser.showDialog(null, "Select Directory");
                    if (response == JFileChooser.APPROVE_OPTION) {
                        File file = chooser.getSelectedFile();
                        if (file != null) {
                            while ((! file.exists()) || file.isFile()) {
                                file = file.getParentFile();
                                if (file == null) { return; }
                            }
                            if (file.exists() && file.isDirectory()) {
                                add_to_path_text_field.setText(file.toString());
                            }
                        }
                    }
                }
            });
        m_e2e_panel.add(browse_path_button, c);

        c.gridx = 2;
        JButton add_to_path_button = new JButton("Add To Path");
        add_to_path_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    String directory = add_to_path_text_field.getText();
                    if (directory.length() > 0) {
                        m_e2e_path_list.add(directory);
                        updateE2EPane();
                    }
                }
            });
        m_e2e_panel.add(add_to_path_button, c);

        c.gridy = i++;
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        JPanel panel = new JPanel();
        m_e2e_save_changes_button = new JButton("Save Changes");
        m_e2e_save_changes_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    updateE2EPathAndReload(true);
                    m_e2e_save_changes_button.setEnabled(false);
                }
            });
        panel.add(m_e2e_save_changes_button);

        m_e2e_reset_changes_button = new JButton("Reset");
        panel.add(m_e2e_reset_changes_button);
        m_e2e_reset_changes_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    setE2EPathList(m_original_e2e_path);
                    if (mb_e2e_path_changed_and_reload_occurred) {
                        updateE2EPathAndReload(false);
                    }
                    m_e2e_save_changes_button.setEnabled(false);
                    m_e2e_reset_changes_button.setEnabled(false);
                    mb_e2e_path_changed_and_reload_occurred = false;
                }
            });
        m_e2e_panel.add(panel, c);

        pack();
    }

    private void updateE2EPathAndReload(boolean _b_ask_user_to_ok) {
        if (_b_ask_user_to_ok) {
            String title = "Confirm E2E_PATH Update";
            String message =
                "Changing the E2E_PATH will trigger the reloading of all\n" +
                "primitives and boxes to assure consistency in the source\n" +
                "data of these objects.\n\n" +
                "Do you wish to proceed?";
            int response = JOptionPane.showConfirmDialog(this, message, title,
                       JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

            if (response != JOptionPane.YES_OPTION) { return; }
        }

        String new_E2E_path = "";
        for (Iterator i = m_e2e_path_list.iterator(); i.hasNext(); ) {
            new_E2E_path += (String) i.next();
            if (i.hasNext()) { new_E2E_path += File.pathSeparator; }
        }

        AlfiMainFrame main_window = Alfi.getMainWindow();
        Object[] info = main_window.unloadAll();
        String[] paths_to_reload = (String[]) info[0];
        HashMap editor_map = (HashMap) info[1];
        Point toolbar_location = (Point) info[2];

            // set E2E_PATH
        Alfi.setE2EPath(new_E2E_path);

            // reload the primitives
        FileFormatProblemManager ffpm = new FileFormatProblemManager();
        new PrimitiveLoader(Alfi.getE2EPath(), ffpm);
        ffpm.resolve();

        ToolbarFrame toolbar = Alfi.getPrimitivesToolbarFrame();
        toolbar_location.x += Alfi.WINDOW_MANAGER_OFFSET.x;
        toolbar_location.y += Alfi.WINDOW_MANAGER_OFFSET.y;
        toolbar.setLocation(toolbar_location);
        toolbar.setVisible(true);

            // reload all the boxes, order does not matter
        File[] files = new File[paths_to_reload.length];
        for (int i = 0; i < paths_to_reload.length; i++) {
            files[i] = new File(paths_to_reload[i]);
        }
        main_window.load_and_show__engine(files, editor_map);

        mb_e2e_path_changed_and_reload_occurred = true;

        toFront();

            // one more warning
        String title = "Possible Future Session E2E_PATH Conflict";
        String message =
            "If you use a script such as alfi_ws to start Alfi, it uses\n" +
            "(or may use) an environment variable E2E_PATH in your\n" +
            "system's shell to initialize the value of E2E_PATH\n" +
            "in Alfi, and this will override the change you have just\n" +
            "made the next time you start Alfi.  If you wish to\n" +
            "maintain this change in future Alfi sessions, you should\n" +
            "update your E2E_PATH environment variable before using\n" +
            "Alfi again.\n\n" +

            "If you use Java Web Start directly to start Alfi,\n" +
            "disregard this warning.  In such cases, E2E_PATH is\n" +
            "initialized solely through your preference file, which\n" +
            "will be changed to the value you have just set at the end\n" +
            "of this session.";
        JOptionPane.showMessageDialog(this, message, title,
                                                  JOptionPane.WARNING_MESSAGE);
    }


    private void updateDefaultBrowserPanel(String _path) {
        m_default_browser_panel.removeAll();
        m_default_browser_panel.repaint();

        JLabel title = new JLabel("Set Default Web Browser");

        final JTextField browser_field = new JTextField(30);
        browser_field.setText(_path);

        JButton browse_path_button = new JButton("Browse");
        browse_path_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    JFileChooser chooser =
                                  new JFileChooser(Alfi.WORKING_DIRECTORY);
                    int response = chooser.showDialog(null, "Select File");
                    if (response == JFileChooser.APPROVE_OPTION) {
                        File file = chooser.getSelectedFile();
                        if (file != null) {
                            if (file.exists() && !file.isDirectory()) {
                                browser_field.setText(file.toString());
                            }
                        }
                    }
                }
            });

        JButton reset_button = new JButton("Reset");
        reset_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    browser_field.setText(m_default_browser);
                }
            });

        JButton save_button = new JButton("Change Browser");
        save_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    Alfi.DEFAULT_BROWSER = browser_field.getText();
                }
            });

        m_default_browser_panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 3, 0, 0);

        int i = 0;

        c.gridy = i++;
        c.gridx = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.insets = new Insets(5, 3, 5, 3);
        m_default_browser_panel.add(title, c);

        c.gridy = i++;
        c.gridx = 0;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.CENTER;
        m_default_browser_panel.add(browser_field, c);
        c.gridx = 1;
        m_default_browser_panel.add(browse_path_button, c);
        c.gridx = 2;
        m_default_browser_panel.add(reset_button, c);

        c.gridy = i++;
        c.gridx = 0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        m_default_browser_panel.add(save_button, c);

        pack();
    }

}
