package edu.caltech.ligo.alfi.dialogs;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.tree.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.tools.*;

public class UnresolvableChannelsDialog extends JDialog {
        // exit status values
    public static final int CONTINUE = 0;
    public static final int VETO     = 1;

    private int m_exit_status;
    private Hashtable m_unresolvable_channels_data;
    private JButton m_continue_button;
    private JButton m_cancel_button;

    UnresolvableChannelsDialog(Frame _parent,
                               Hashtable _unresolvable_channels_data,
                                               boolean _b_enable_veto_signal) {
        super(_parent, "Unresolvable Channels Dialog");

        m_unresolvable_channels_data = _unresolvable_channels_data;

            // "special" objects are connections deemed real issues and their
            // containers...
        final HashSet SPECIAL_objects_set =
                        determineSPECIALObjects(m_unresolvable_channels_data);

        setVisible(false);
        setModal(_b_enable_veto_signal);
        if (_b_enable_veto_signal) {
            m_cancel_button = new JButton("Cancel");
            m_cancel_button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        triggerVeto(false);
                    }
                });
        }
        else { m_cancel_button = null; }

        Box tree_box = Box.createHorizontalBox();
        tree_box.setBorder(BorderFactory.createTitledBorder(
                       BorderFactory.createLoweredBevelBorder(),
                       "Unresolvable Channels (Double Click to Locate)"));

        final JTree tree = new JTree(m_unresolvable_channels_data);
        tree.setCellRenderer(new DefaultTreeCellRenderer() {
                public Component getTreeCellRendererComponent(JTree _tree,
                            Object _value, boolean _b_sel, boolean _b_expanded,
                            boolean _b_leaf, int _row, boolean _b_has_focus) {

                    Component component =
                        super.getTreeCellRendererComponent(_tree, _value,
                            _b_sel, _b_expanded, _b_leaf, _row,  _b_has_focus);
                    Object user_object =
                              ((DefaultMutableTreeNode) _value).getUserObject();
                    if (SPECIAL_objects_set.contains(user_object)) {
                        if (user_object instanceof ALFINode) {
                            component.setForeground(Color.black);
                        }
                        else { component.setForeground(Color.red); }
                    }
                    else { component.setForeground(Color.gray); }

                    return component;
                }
            });
        final UnresolvableChannelsDialog dialog = this;
        tree.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent _event) {
                    boolean b_left_click =
                        ((_event.getModifiers() & InputEvent.BUTTON1_MASK)!= 0);
                    boolean b_double_click = (_event.getClickCount() == 2);
                    if (b_left_click && b_double_click) {
                        Point p = _event.getPoint();
                        TreePath tree_path = tree.getPathForLocation(p.x, p.y);
                        if (tree_path != null) {
                            DefaultMutableTreeNode tree_node =
                                       (DefaultMutableTreeNode)
                                              tree_path.getLastPathComponent();
                            Object o = tree_node.getUserObject();
                            ConnectionProxy connection = null;
                            if (o instanceof ConnectionProxy) {
                                connection = (ConnectionProxy) o;
                                tree_node = (DefaultMutableTreeNode)
                                                         tree_node.getParent();
                                o = tree_node.getUserObject();
                            }

                            if (o instanceof ALFINode) {
                                ALFINode node = (ALFINode) o;

                                triggerVeto(true);

                                try {
                                    Alfi.getMainWindow().show(node, null, true);
                                    if (connection != null) {
                                        EditorFrame editor =
                                            Alfi.getTheNodeCache().
                                                          getEditSession(node);
                                        editor.determineConnectionWidget(
                                                   connection).blink(true,
                                                           Color.white, 20000);
                                    }
                                }
                                catch (Exception ex) { ; }
                            }
                        }
                    }
                }
            });

        revealSPECIALObjects(tree, SPECIAL_objects_set);

        JScrollPane tree_scroller = new JScrollPane(tree,
                              ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                              ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        tree_box.add(tree_scroller);

        JPanel button_panel = new JPanel();
        button_panel.setLayout(new BoxLayout(button_panel, BoxLayout.X_AXIS));
        m_continue_button = new JButton("Continue");
        m_continue_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    m_exit_status = CONTINUE;
                    setVisible(false);
                }
            });
        button_panel.add(m_continue_button);
        if (m_cancel_button != null) {
            button_panel.add(Box.createHorizontalStrut(10));
            button_panel.add(m_cancel_button);
        }
        button_panel.add(Box.createHorizontalStrut(30));
        final JButton report_button = new JButton("Write Report");
        report_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    writeReport();
                    report_button.setEnabled(false);
                }
            });
        button_panel.add(report_button);
        getRootPane().setDefaultButton(m_continue_button);

        Box main_box =  Box.createVerticalBox();
        main_box.add(tree_box);
        main_box.add(button_panel);

        Container content_pane = getContentPane();
        content_pane.add(main_box);

        m_exit_status = CONTINUE;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///// static methods ///////////////////////////////////////////////////////

    public static UnresolvableChannelsDialog executeDialog(Frame _owner_frame,
            Hashtable _unresolved_channels_tree, boolean _b_enable_veto_signal){
        UnresolvableChannelsDialog dialog = new UnresolvableChannelsDialog(
               _owner_frame, _unresolved_channels_tree, _b_enable_veto_signal);

        dialog.pack();
        EditorFrame.centerDialog(dialog);

        dialog.setVisible(true);

        return dialog;
    }


    ////////////////////////////////////////////////////////////////////////////
    ///// methods //////////////////////////////////////////////////////////////


    public int getExitStatus() { return m_exit_status; }

    private File writeReport() {
        String report = createReport();
        File report_file = null;
        try {
            report_file = new File("data_channel_validation_report.txt");
            FileWriter writer = new FileWriter(report_file);
            writer.write(report, 0, report.length());
            writer.flush();
            writer.close();

            String title = "Validation Report Written";
            String message = "The data channel validation report has been " +
                             "written to file " + report_file + ".";
            int type = JOptionPane.PLAIN_MESSAGE;
            JOptionPane.showMessageDialog(this, message, title, type);
        }
        catch(IOException _e) {
            System.err.println(_e.toString());
            System.err.println("\nThe following report was NOT written " +
                               "to file:\n");
            System.err.println(report);
        }

        return report_file;
    }

    private String createReport() {
        return "Unresolvable Data Channel Report\n" +
               "--------------------------------\n\n" +
               MiscTools.treeToString(m_unresolvable_channels_data);
    }

    private void expandAllChannelEntries(JTree _tree) {
        DefaultTreeModel tree_model = (DefaultTreeModel) _tree.getModel();
        DefaultMutableTreeNode root_node =
                                 (DefaultMutableTreeNode) tree_model.getRoot();
        showLeaves(_tree, root_node);
    }

    private void showLeaves(JTree _tree, DefaultMutableTreeNode _node) {
        if (_node.isLeaf()) {
            DefaultMutableTreeNode parent =
                                    (DefaultMutableTreeNode) _node.getParent();
            TreePath parent_path = new TreePath(parent.getPath());
            _tree.expandPath(parent_path);
        }
        else {
            Enumeration children = _node.children();
            while (children.hasMoreElements()) {
                DefaultMutableTreeNode child =
                               (DefaultMutableTreeNode) children.nextElement();
                showLeaves(_tree, child);
            }
        }
    }

    private void triggerVeto(boolean _b_keep_dialog__nonmodal) {
        m_exit_status = VETO;
        setVisible(false);
        setModal(false);
        if (_b_keep_dialog__nonmodal) {
            if (m_cancel_button != null) {
                m_continue_button.setEnabled(false);
            }
            setVisible(true);
        }
    }

        /** Determines connections and their containers which especially need
          * to be brought to the user's attention when it appears in the tree.
          * The current definition of such special objects are connections
          * which are unresolved, and are contained in a non-root node which is
          * at the end of the inheritance hierarchy for the nodes currently
          * loaded into Alfi.
          */
    private HashSet determineSPECIALObjects(Hashtable
                                                 _unresolvable_channels_data) {
        HashSet SPECIAL_set = new HashSet();
        addObjectsIfSpecial(SPECIAL_set, null, _unresolvable_channels_data);
        return SPECIAL_set;
    }

    private void addObjectsIfSpecial(HashSet _SPECIAL_set, ALFINode _container,
                                       Hashtable _unresolvable_channels_data) {
            // b_container_is_base flags whether container is at the end of
            // the inheritance hierarchy for the nodes currently loaded in Alfi.
        boolean b_container_is_base = (_container == null) ? true :
                      (_container.isRootNode() ||
                             (_container.derivedNodes_getDirect().length > 0));

        for (Iterator i = _unresolvable_channels_data.keySet().iterator();
                                                               i.hasNext(); ) {
            Object key = i.next();
            if (key instanceof ConnectionProxy) {
                if (! b_container_is_base) {
                    _SPECIAL_set.add(key);
                    if (! _SPECIAL_set.contains(_container)) {
                        ALFINode node = _container;
                        while (node != null) {
                            _SPECIAL_set.add(node);
                            node = node.containerNode_get();
                        }
                    }
                }
            }
            else if (key instanceof ALFINode) {
                ALFINode container = (ALFINode) key;
                Hashtable contents =
                        (Hashtable) _unresolvable_channels_data.get(container);
                addObjectsIfSpecial(_SPECIAL_set, container, contents);
            }
        }
    }

    private void revealSPECIALObjects(JTree _tree,
                                                HashSet _SPECIAL_objects_set) {
        DefaultTreeModel model = (DefaultTreeModel) _tree.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        Enumeration children = root.depthFirstEnumeration();
        while (children.hasMoreElements()) {
            DefaultMutableTreeNode node =
                               (DefaultMutableTreeNode) children.nextElement();
            Object user_object = node.getUserObject();
            if (_SPECIAL_objects_set.contains(user_object) &&
                                           (user_object instanceof ALFINode)) {
                TreePath path = new TreePath(node.getPath());
                _tree.expandPath(path);
            }
        }

            // now make sure the first connection expanded to is showing...
        int rows = _tree.getRowCount();
        for (int i = 0; i < rows; i++) {
            TreePath path = _tree.getPathForRow(i);
            DefaultMutableTreeNode node =
                          (DefaultMutableTreeNode) path.getLastPathComponent();
            Object user_object = node.getUserObject();
            if (user_object instanceof ConnectionProxy) {
                _tree.makeVisible(path);
                break;
            }
        }
    }
}
