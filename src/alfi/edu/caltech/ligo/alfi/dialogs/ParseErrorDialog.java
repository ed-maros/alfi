package edu.caltech.ligo.alfi.dialogs;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.tools.*;

public class ParseErrorDialog extends JDialog {
        // choice values
    public static final int CONTINUE      = 0;
    public static final int COMPLETE_LOAD = 1;
    public static final int STOP_LOAD     = 2;
    public static final int EXIT_ALFI     = 3;

    private int m_choice;
    private JTextArea m_text_area;
    private Color m_new_message_color;
    private JCheckBox m_log_check_box;

    private JButton m_continue_button;
    private JButton m_complete_button;
    private JButton m_cancel_button;
    private JButton m_exit_button;

    public ParseErrorDialog(Frame _parent) {
        super(_parent, "Parse Error Dialog");

        setVisible(false);

        m_text_area = new JTextArea(30, 60);
        JScrollPane text_area_scroller = new JScrollPane(m_text_area,
                              ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                              ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        m_new_message_color = Color.magenta;
        m_text_area.setSelectionColor(Color.white);
        m_text_area.setBackground(Color.white);
        m_text_area.setForeground(Color.lightGray);

        JPanel button_panel = new JPanel();
        button_panel.setLayout(new BoxLayout(button_panel, BoxLayout.X_AXIS));
        m_continue_button = new JButton("OK");
        m_continue_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    m_choice = CONTINUE;
                    if (m_log_check_box.isSelected()) {
                        m_log_check_box.setEnabled(false);
                    }
                    setVisible(false);
                }
            });
        button_panel.add(m_continue_button);

        button_panel.add(Box.createHorizontalStrut(15));

        m_complete_button = new JButton("Complete Loading");
        m_complete_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    m_complete_button.setEnabled(false);
                    m_continue_button.setEnabled(false);
                    if (m_log_check_box.isSelected()) {
                        m_log_check_box.setEnabled(false);
                    }

                    m_choice = COMPLETE_LOAD;
                    setVisible(false);
                }
            });
        button_panel.add(m_complete_button);

        m_cancel_button = new JButton("Stop Loading");
        m_cancel_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    m_choice = STOP_LOAD;
                    setVisible(false);
                }
            });
        button_panel.add(m_cancel_button);

        button_panel.add(Box.createHorizontalStrut(10));

        m_log_check_box = new JCheckBox("Log Errors", false);
        button_panel.add(m_log_check_box);

        button_panel.add(Box.createHorizontalStrut(20));

        m_exit_button = new JButton("Exit Alfi");
        m_exit_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    m_choice = EXIT_ALFI;
                    setVisible(false);
                }
            });
        button_panel.add(m_exit_button);

        getRootPane().setDefaultButton(m_continue_button);

        Box main_box =  Box.createVerticalBox();
        main_box.add(text_area_scroller);
        main_box.add(Box.createVerticalStrut(5));
        main_box.add(button_panel);

        Container content_pane = getContentPane();
        content_pane.add(main_box);

        m_choice = CONTINUE;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///// static methods ///////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    ///// methods //////////////////////////////////////////////////////////////

    private Color getNextMessageColor() {
        if (m_new_message_color == Color.red) {
            m_new_message_color = Color.magenta;
        }
        else { m_new_message_color = Color.red; }

        return m_new_message_color;
    }

    public void appendMessage(String _message) {
        m_text_area.setSelectedTextColor(getNextMessageColor());

        int old_text_end = m_text_area.getText().length();
        m_text_area.append(_message);
        m_text_area.append("\n\n");
        m_text_area.append("- - - - - - - - - - - - - - - - - - - - - - - - -");
        m_text_area.append("\n\n");
        m_text_area.setSelectionStart(old_text_end);
        m_text_area.setSelectionEnd(m_text_area.getText().length());

        repaint();

            // give user a chance to see how many errors are being written
        try { Thread.currentThread().sleep(500); }
        catch(InterruptedException _e) { }
    }

    public int getChoice() { return m_choice; }

    public boolean getLoggingFlag() { return m_log_check_box.isSelected(); }

    public String getReportText() { return m_text_area.getText(); }

    public void finalizeCompleteLoad() {
        setVisible(false);
        m_continue_button.setEnabled(true);
        m_cancel_button.setEnabled(false);

        setModal(true);
        setVisible(true);
    }
}
