/*
 */

package edu.caltech.ligo.alfi.dialogs;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class HorizontalPanel extends JPanel {
    final static boolean COLORS = false;

    private int m_height;
    private int m_width;

    HorizontalPanel (JDialog _dialog, String _title, int _width, int _height) {

        m_height = _height;
        m_width = _width;

        if (COLORS) {
            setBackground(Color.lightGray);
        }

        setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createTitledBorder(_title),
            BorderFactory.createEmptyBorder(5,5,5,5)));

        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    }
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }
    public Dimension getPreferredSize() {
        return new Dimension(m_width, m_height);
        //super.getPreferredSize().height);
    }
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
}
