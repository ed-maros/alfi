package edu.caltech.ligo.alfi.dialogs;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;


public class NodePropertiesDialog extends AlfiDialog {

    public static final String THIS_NODE_COMMENTS_TITLE = 
        new String("This Instance Node's Comments [EDITABLE]");
    public static final String BASE_NODE_COMMENTS_TITLE = 
        new String("Base Node's Comments [READ ONLY]");
    public static final String USAGE_NOTES_TITLE = 
        new String("Usage Notes");
    private static final String BASE_NODE_COMMENTS_1 = 
        new String("Base node '");
    private static final String BASE_NODE_COMMENTS_2 = 
        new String("' comments: ");
    private static final String NO_COMMENTS = new String("[no comments]");
    private static final String COMMENT_SEPARATOR = 
             new String("- - - - - - - - - - - - - - - - - - - - - - - - - -");
    private String[] m_column_names = {"Current Value", 
                                       "Type", 
                                       "Data Type", 
                                       "notes re current value",
                                       "DEFAULT Value",
                                       "notes re DEFAULT value"};
    private static final int WIDTH =  800;
    private static final int HEIGHT = 600;

    private String m_original_comment = new String("");

    // Swing components
    private JPanel m_comment_panel;    
    private ParameterTable m_parameter_table;
    private JTextPane m_instance_node_comments;    
    private JTextArea m_node_comments;    
    private JTextPane m_usage_notes;    
    
    private ALFINode m_node;

    public NodePropertiesDialog(Frame frame, ALFINode _node) {
        super( frame,
               new String(_node.generateFullNodePathName()+" - Properties"), 
               true, // modal
               WIDTH, HEIGHT);
        m_node = _node;

        initComponents();
    }

    public NodePropertiesDialog() {
        super((Frame)null, "ALFI Node Properties", true, WIDTH, HEIGHT);
    }


    protected void initComponents () {
        getContentPane().setLayout(
            new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        //Dimension size = getContentPane().getSize();
        SettingsPanel this_node_comments_panel = 
            new SettingsPanel(this, THIS_NODE_COMMENTS_TITLE, 
                              WIDTH, HEIGHT * 20/100);
        this_node_comments_panel.add(createNodeCommentsComponents());

        SettingsPanel base_node_comments_panel = 
            new SettingsPanel(this, BASE_NODE_COMMENTS_TITLE, 
                              WIDTH, HEIGHT * 20/100);
        base_node_comments_panel.add(createBaseNodeCommentsComponents());

        SettingsPanel table_panel = 
            new SettingsPanel(this, "", WIDTH, HEIGHT * 50/100);
        table_panel.add(createTableComponents());

        getContentPane().add(this_node_comments_panel);
        getContentPane().add(base_node_comments_panel);
        getContentPane().add(table_panel);

        JPanel button_panel;    
        button_panel = new JPanel();        
        button_panel.setLayout(new FlowLayout (FlowLayout.CENTER, 5, 5));

        JButton ok_button;    
        ok_button = new JButton();        
        ok_button.setText(OK);
        ok_button.setMnemonic('O');
        ok_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                okButtonActionPerformed(_event);
            }
        });   
        ok_button.addFocusListener(new FocusAdapter() {
            public void focusGained (FocusEvent _event) {
                Component lost_focus = _event.getOppositeComponent(); 
                if (lost_focus instanceof JTextField) {
                    m_parameter_table.setTableValue((JTextField) lost_focus);
                }
            }
        });
        button_panel.add(ok_button);
        getRootPane().setDefaultButton(ok_button);

        JButton cancel_button;    
        cancel_button = new JButton();        
        cancel_button.setText(CANCEL);
        cancel_button.setMnemonic('C');
        cancel_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                cancelButtonActionPerformed(_event);
            }
        });   

        button_panel.add(cancel_button);

        JButton helpButton;    
        helpButton = new JButton();        
        helpButton.setText(HELP);
        helpButton.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                helpButtonActionPerformed(_event);
            }
        });   

        // Disable for now.
        //button_panel.add(helpButton);

        getContentPane().add(button_panel);

        super.initComponents();
    }

    private Component createNodeCommentsComponents() {
        m_instance_node_comments = new JTextPane();
        m_instance_node_comments.setBorder(new EtchedBorder());        
        String node_comment = m_node.comment_getLocal();
        if (node_comment != null) {
            m_instance_node_comments.setText(node_comment);        
            m_original_comment = new String(node_comment.trim());
            m_instance_node_comments.moveCaretPosition(0);
        }

        m_instance_node_comments.addFocusListener(new FocusAdapter() {
            public void focusGained (FocusEvent _event) {
                Component lost_focus = _event.getOppositeComponent(); 
                if (lost_focus instanceof JTextField) {
                    m_parameter_table.setTableValue((JTextField) lost_focus);
                }
            }
        });

        JScrollPane scroll_pane = new JScrollPane(m_instance_node_comments, 
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll_pane.setBorder(BorderFactory.createLoweredBevelBorder());
        return scroll_pane;
    }

    private String addBaseNodeComments (ALFINode _node, String _orig_comment) {
        StringBuffer buffer = new StringBuffer(_orig_comment);
        if (_orig_comment.length() > 0) {
            buffer.append(Parser.NL + COMMENT_SEPARATOR + Parser.NL);
        }
        String header = new String(BASE_NODE_COMMENTS_1 + 
                                   _node.generateFullNodePathName() +
                                   BASE_NODE_COMMENTS_2 + Parser.NL);
        buffer.append(header);
        String node_comment = _node.comment_getLocal();
        if (node_comment == null || node_comment.length() == 0) {
            buffer.append(NO_COMMENTS + Parser.NL);
        } else {
            buffer.append(node_comment);
        }

        return buffer.toString();
    }

    private Component createBaseNodeCommentsComponents() {
        m_node_comments = new JTextArea(); 
        m_node_comments.setEditable(false); 
        m_node_comments.setLineWrap(true); 
        m_node_comments.setBorder(new EtchedBorder());        

        ALFINode base_node = m_node.baseNode_getDirect();
        
        String comments = new String();
        while (base_node != null) {
            comments =  addBaseNodeComments(base_node, comments);
            base_node = base_node.baseNode_getDirect();
        }
        m_node_comments.setText(comments);
        m_node_comments.moveCaretPosition(0);

        JScrollPane scroll_pane = new JScrollPane(m_node_comments, 
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll_pane.setBorder(BorderFactory.createLoweredBevelBorder());
        return scroll_pane;
    }
    
    private Component createTableComponents() {
        
        m_parameter_table = new ParameterTable(this, m_node, 
            m_column_names, WIDTH);
        ToolTipManager.sharedInstance().registerComponent(m_parameter_table);

        JScrollPane scrollPane = new JScrollPane(m_parameter_table, 
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setRowHeaderView(m_parameter_table.getRowHeader());
        scrollPane.setBorder(BorderFactory.createLoweredBevelBorder());

        return scrollPane;
    }
    
    private Component createUsageNotesComponents() {
        m_usage_notes = new JTextPane();        
        m_usage_notes.setBorder(new EtchedBorder());        
        return m_usage_notes; 
    }

    protected void updateData() {

        // First update the local comments
        String comment = m_instance_node_comments.getText();
        comment.trim();
        if (!comment.equals(m_original_comment)) {
            m_node.comment_clearLocal();
            m_node.comment_addLocal(comment);
        }
  
        // Get the values from the parameter table.
        ValueModification[] value_changes = 
                                  m_parameter_table.getParameterValueChanges();

        // Go through the values in the table and replace the 
        // node's settings
        for (int ii = 0; ii < value_changes.length; ii++) {
            String name = value_changes[ii].getHeaderName();
            String value = value_changes[ii].getValue();
            value = value.trim();
            System.out.println("Value_changes " +name + " = " + value);
            String setting_comment = value_changes[ii].getComment();
            boolean is_quoted = ParameterTableModel.isMultilineText(value);
            NodeSettingIF setting = m_node.setting_get(name);
            if (setting != null) {
                if (! is_quoted) { is_quoted = setting.isQuoted(); }
                if (m_node.setting_containsLocal(name)) {
                    m_node.setting_removeLocal(name);
                }
            }

            if (value.length() > 0) {
                if (!value.equals(ParameterTableModel.DEFAULT_VALUE)) {
                    m_node.setting_add(name, value, setting_comment, is_quoted);
                }
            }
        }

        if (m_node instanceof FUNCX_Node) {   
            FUNCX_Node funcx_node = (FUNCX_Node) m_node;
            // No need to do any sort of setting finalizations if 
            // there are no changes in the table.
            if (value_changes.length > 0) {
                funcx_node.setting_finalize();
            }
        }
    }
    /**
      * Checks if the user has made any modifications
      */
    protected boolean hasChangedData () {
        return m_parameter_table.hasChangedData();
    }

    protected boolean hasUnsavedData() {
        return m_parameter_table.hasChangedData();
    }

    protected void okButtonActionPerformed (ActionEvent _event) {
        try {
            if (m_parameter_table.saveAllValues()) {
                updateData();
                doClose(RET_OK);
            }
        }
        catch (Exception e) { e.printStackTrace(); }
    }
 
    protected void doClose (int _return_status) {
        m_parameter_table.cancelAllDialogs();
        super.doClose(_return_status);
    }   
}
