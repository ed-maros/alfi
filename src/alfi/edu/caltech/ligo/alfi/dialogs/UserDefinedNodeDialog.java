package edu.caltech.ligo.alfi.dialogs; 
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.widgets.*;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.tools.*;

    /**
     *  The dialog used to create UserDefinedPrimitives.   The dialog allows
     *  the user to create primitives and set the following items.
     *  o.  Group 
     *  o.  Icon location
     *  o.  Comment
     *  o.  Default value for the ParameterDeclaration
     *  o.  Input and Output ports
     *
     *  The event listeners are for the following:
     *    NodeContentChangeEventListener: listens to m_working_udp for port adds
     *        and deletes and updates port lists in the dialog
     *    ProxyChangeEventListener: listens to all the ports for changes of
     *        name, I/O type, and data type and updates the lists in the dialog
     *        
     */

public class UserDefinedNodeDialog extends AlfiDialog
                                   implements NodeContentChangeEventListener,
                                              ProxyChangeEventListener,
                                              ContainedElementModEventListener, 
                                              NodeInterfaceChangeEventListener {
    private static final int WIDTH =  800;
    private static final int HEIGHT = 600;

    public static final String EMPTY_COMMENT = new String("");
    public static final String GROUP_NOT_SET = new String("GROUP_NOT_SET");

    public static final String SHOW_NODE = new String("Show UDP Node");
    public static final String HIDE_NODE = new String("Hide UDP Node");

    private UserDefinedPrimitive m_original_udp;
    private UserDefinedPrimitive m_working_udp;
    private EditorFrame m_working_udp_editor;
    
    private JTextField m_name_pane;
    private JComboBox m_group_list;
    private JTextField m_icon_location_entry;
    private JTextPane m_comment_pane;
    private JTextField m_new_port_name;
    private ParameterList m_param_decl_list;
    /** DefaultListModel containing ParameterDeclarations */ 
    private DefaultListModel m_param_decl_list_model = new DefaultListModel();

    private JButton m_add_input_button;
    private JButton m_remove_input_button;
    private JButton m_replicate_input;
    private JList m_input_data_list;
    /** DefaultListModel containing PortProxies for the input ports*/ 
    private DefaultListModel m_input_list_model = new DefaultListModel();

    private JButton m_add_output_button;
    private JButton m_remove_output_button;
    private JButton m_replicate_output;
    private JList m_output_data_list;
    /** DefaultListModel containing PortProxies for the output ports*/ 
    private DefaultListModel m_output_list_model = new DefaultListModel();

    private JList m_data_type_list;
    /** Used to specify the number of replications for the input ports */
    private JSpinner m_input_spinner;
    /** Used to specify the number of replications for the output ports */
    private JSpinner m_output_spinner;

    public UserDefinedNodeDialog(Frame _parent, String _udp_type_name_to_edit) {
        this(_parent, _udp_type_name_to_edit, null, false);
    }
    
    public UserDefinedNodeDialog(Frame _parent, String _udp_type_name_to_edit,
                                 boolean _modal) {
        this(_parent, _udp_type_name_to_edit, null, _modal);
    }

    public UserDefinedNodeDialog(Frame _parent, String _udp_type_name_to_edit,
                               String _udp_type_name_to_copy) {
        this(_parent, _udp_type_name_to_edit, _udp_type_name_to_copy, false);
    }


    public UserDefinedNodeDialog(Frame _parent, String _udp_type_name_to_edit,
                               String _udp_type_name_to_copy, boolean _modal) {
        super( _parent,
               new String("User Defined Primitive Definition - " +
                                                        _udp_type_name_to_edit),
               _modal,
               WIDTH, HEIGHT);

        ALFINode node_to_edit =
                       Alfi.getTheNodeCache().findNode(_udp_type_name_to_edit);
        ALFINode node_to_copy = (_udp_type_name_to_copy != null) ?
                Alfi.getTheNodeCache().findNode(_udp_type_name_to_copy) : null;
        if (node_to_edit != null) {
                // at end, changes in working udp will transfer to original
            m_original_udp = (UserDefinedPrimitive) node_to_edit;
            String name = "_" + _udp_type_name_to_edit + "_";
            if (Alfi.getTheNodeCache().findNode(name) == null) {
                    // working udp will be destroyed after transfer to original
                m_working_udp = new UserDefinedPrimitive(name,
                          m_original_udp.comment_getLocal(),
                          m_original_udp.getPrimitiveGroup(),
                          m_original_udp.ports_getByPosition(false),
                          m_original_udp.parameterDeclarations_getLocal());
                m_working_udp.setIconInfo(m_original_udp.getIconInfo());
                m_working_udp.setTempIconPath(m_original_udp.getIconFilePath());
            }
            else {
                System.err.println( "UserDefinedNodeDialog():\n" +
                          "\tTemp UDP \"" + name + "\" already exists.");
                return;
            }
        }
        else if ((node_to_copy != null) &&
                              (node_to_copy instanceof UserDefinedPrimitive)) {
                // there is no node to change at end.  working udp will persist.
            m_original_udp = null;
                // node_to_copy is used only to create the working udp.
            UserDefinedPrimitive udp_to_copy =
                                           (UserDefinedPrimitive) node_to_copy;
            String name = _udp_type_name_to_edit;
            if (Alfi.getTheNodeCache().findNode(name) == null) {
                m_working_udp = new UserDefinedPrimitive(name,
                            udp_to_copy.comment_getLocal(),
                            udp_to_copy.getPrimitiveGroup(),
                            udp_to_copy.ports_getByPosition(false),
                            udp_to_copy.parameterDeclarations_getLocal());
                m_working_udp.setIconInfo(udp_to_copy.getIconInfo());
                m_working_udp.setTempIconPath(udp_to_copy.getIconFilePath());
            }
            else {
                System.err.println("UserDefinedNodeDialog():\n" +
                          "\tUDP \"" + name + "\" already exists.");
                return;
            }
        }
        else {
                // there is no node to change at end.  working udp will persist.
            m_original_udp = null;
                // create an empty udp
            m_working_udp = new UserDefinedPrimitive(_udp_type_name_to_edit,
                             EMPTY_COMMENT, GROUP_NOT_SET,
                             new PortProxy[4][0], new ParameterDeclaration[0]);
        }

        if (m_working_udp == null) { return; }

        m_working_udp.addNodeContentChangeListener(this);
        m_working_udp.addNodeInterfaceChangeListener(this);
        m_working_udp.addContainedElementModListener(this);

        m_working_udp_editor = null;

        initComponents();
        updateDialogPortListsFromWorkNodeData();
        setResizable(true);
    }

    /** Create the dialog components */
    protected void initComponents () {

        Container container_pane = getContentPane();
        
        SettingsPanel top_panel = 
            new SettingsPanel(this, "",
                              WIDTH, HEIGHT * 40/100, BoxLayout.X_AXIS);
        createTopComponents(top_panel);
        container_pane.add(top_panel, BorderLayout.PAGE_START);

        SettingsPanel left_panel = 
            new SettingsPanel(this, "Input Port", 
                              WIDTH * 30/100, 
                              HEIGHT * 50/100,
                              BoxLayout.Y_AXIS);
        createInputPortSettings(left_panel);
        container_pane.add(left_panel, BorderLayout.LINE_START);

        SettingsPanel center_panel = 
            new SettingsPanel(this, "Added Ports", 
                              WIDTH * 40/100 , 
                              HEIGHT * 50/100, 
                              BoxLayout.X_AXIS);
        createNewPortSettings(center_panel);
        container_pane.add(center_panel, BorderLayout.CENTER);

        SettingsPanel right_panel = 
            new SettingsPanel(this, "Output Port", 
                              WIDTH * 30/100 , 
                              HEIGHT * 50/100,
                              BoxLayout.PAGE_AXIS);
        createOutputPortSettings(right_panel);
        container_pane.add(right_panel, BorderLayout.LINE_END);

        SettingsPanel bottom_panel = 
            new SettingsPanel(this, "", 
                              WIDTH, HEIGHT * 10/100);
        createSettingsButtons(bottom_panel);
        container_pane.add(bottom_panel, BorderLayout.PAGE_END);


        super.initComponents();
    }


    /** Create the components on top panel.   Mostly node name, icon path,
        etc and also the default values for the ParameterDeclaration */

    private void createTopComponents(Container _container) {

        JPanel leftbox = new JPanel();
        leftbox.setLayout(new BoxLayout(leftbox, BoxLayout.Y_AXIS));

        JLabel group_label = new JLabel("Primitive Group:");
        String[] group_names = 
            (( PrimitivesPopupMenu )(Alfi.getPrimitivesMenu())).getGroupNames();
        Arrays.sort(group_names);
        
        m_group_list = new JComboBox(group_names);
        m_group_list.setEditable(true);
        m_group_list.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent _e) {
                JComboBox cb = (JComboBox) _e.getSource();
                m_working_udp.setPrimitiveGroup((String) cb.getSelectedItem());
            }
        });

        String udp_group = m_working_udp.getPrimitiveGroup();
        for (int i = 0 ; i < group_names.length; i++) {
            if (group_names[i].equals(udp_group)) {
                m_group_list.setSelectedIndex(i);
            }
        }

        JPanel box2 = new JPanel();
        box2.setLayout(new BoxLayout(box2, BoxLayout.X_AXIS)); 
        box2.add(group_label);
        box2.add(m_group_list);
        //box2.setPreferredSize(new Dimension(150, 15));

        JPanel box3 = new JPanel();
        box3.setLayout(new BoxLayout(box3, BoxLayout.X_AXIS)); 
        JLabel icon_location_label = new JLabel("Icon Location:");
        box3.add(icon_location_label);
        m_icon_location_entry = new JTextField();
       
        m_icon_location_entry.setText(m_working_udp.getIconFilePath());
        box3.add(m_icon_location_entry);
        JScrollPane icon_location_pane = new JScrollPane(m_icon_location_entry,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        icon_location_pane.setBorder(BorderFactory.createLoweredBevelBorder());
        box3.add(icon_location_pane); 

        // The m_icon_location_entry textfield also allows for the user to 
        // just type in.   So, you'll need to make sure that when the
        // textfield to set the temp udp's icon information
        m_icon_location_entry.addFocusListener(new FocusAdapter() {

            public void focusLost(FocusEvent _event) {
                String icon_location = m_icon_location_entry.getText().trim();
                if (icon_location.length() > 0) {
                    if (validateIconPath(icon_location)) {
                        m_working_udp.setTempIconPath(icon_location);
                        String icon_info = composeIconInfo(icon_location);
                        m_working_udp.setIconInfo(icon_info);
                        m_working_udp.setLabelType(ALFINode.LABEL_TYPE_ICON);
                    }
                    else { invalidImageSelectedMessage(); }
                }
                else {
                    m_working_udp.setLabelType(ALFINode.LABEL_TYPE_NAME);
                    m_working_udp.setTempIconPath(null);
                    m_working_udp.setIconInfo(null);
                }
            }
        });
        //box3.setPreferredSize(new Dimension(150, 15));

        JButton browse_path_button = new JButton("Browse");
        browse_path_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    JFileChooser chooser =
                                  new JFileChooser(Alfi.WORKING_DIRECTORY);
                    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                    int response = chooser.showDialog(null, "Select Icon File");
                    if (response == JFileChooser.APPROVE_OPTION) {
                        File file = chooser.getSelectedFile();
                        if (file != null) {
                            while ((! file.exists()) || file.isDirectory()) {
                                file = file.getParentFile();
                                if (file == null) { return; }
                            }
                            if (file.exists() && file.isFile()) {
                                if (!validateIconPath(file.toString())) {
                                    invalidImageSelectedMessage();
                                }
                                else {
                                    m_icon_location_entry.setText(
                                                               file.toString());
                                    String icon_info = 
                                                composeIconInfo(file.getName());
                                    m_working_udp.setTempIconPath(
                                                               file.toString());
                                    m_working_udp.setIconInfo(icon_info);
                                    m_working_udp.setLabelType(
                                                     ALFINode.LABEL_TYPE_ICON);
                                }
                            }
                        }
                    }
                }
            });
        box3.add(browse_path_button);

        
        JPanel box4 = new JPanel();
        box4.setLayout( new BoxLayout(box4, BoxLayout.X_AXIS)); 
        JLabel comment_label = new JLabel("Comment:");
        box4.add(comment_label);       
        m_comment_pane = new JTextPane();
        m_comment_pane.setBorder(new EtchedBorder());
        JScrollPane comment_scroll_pane = new JScrollPane(m_comment_pane,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        comment_scroll_pane.setBorder(BorderFactory.createLoweredBevelBorder());
        m_comment_pane.setText(m_working_udp.comment_getLocal()); 

        m_comment_pane.addKeyListener(new KeyAdapter() {
                // triggered AFTER the comment update in the node takes place
            public void keyReleased(KeyEvent _e) {
                m_working_udp.comment_addLocal(m_comment_pane.getText());
            }
        });

        box4.add(comment_scroll_pane);
        box4.setPreferredSize(new Dimension(250, 150));

        leftbox.add(box2);
        leftbox.add(box3);
        leftbox.add(box4);


        JPanel rightbox = new JPanel();
        rightbox.setLayout(new BoxLayout(rightbox, BoxLayout.Y_AXIS));
        rightbox.setBackground(Color.lightGray);
        
        JLabel label = new JLabel("Setting Default Values:"); 
        
        ParameterDeclaration params[] = 
                            m_working_udp.parameterDeclarations_getLocal();
        if (params.length == 0) {
            params = UserDefinedPrimitive.parameterDeclarations_getCoreList(); 
            for (int i = 0; i < params.length; i++) {
                ParameterDeclaration param = params[i];
                    // add them to the new UDP
                m_working_udp.parameterDeclaration_add(param.getType(),
                        param.getName(), param.getValue(), param.getComment());
            }
        }

        for (int i = 0; i < params.length; i++) {
            ParameterDeclaration param = params[i];
            m_param_decl_list_model.addElement(param);
        }

        m_param_decl_list = new ParameterList(this, m_param_decl_list_model);
        JScrollPane decl_list_scroller = new JScrollPane(m_param_decl_list);
        decl_list_scroller.setPreferredSize(new Dimension(400, 100));

        label.setLabelFor(decl_list_scroller);
        rightbox.add(label);
        rightbox.add(decl_list_scroller);

        _container.add(leftbox);
        _container.add(Box.createRigidArea(new Dimension(10, 0)));
        _container.add(rightbox);
    }


    /** Create the components for defining input ports.  The list's 
        DefaultListModel is composed of PortProxy objects */
    private void createInputPortSettings(Container _container) {
        
        JPanel box1 = new JPanel();
        box1.setLayout(new BoxLayout(box1, BoxLayout.X_AXIS)); 
        box1.setBackground(Color.lightGray);

        m_input_data_list = new JList(m_input_list_model);
        JScrollPane list_scroller = new JScrollPane(m_input_data_list);
        list_scroller.setPreferredSize(new Dimension(250, 80));
        box1.add(list_scroller);

        m_remove_input_button = new JButton(">");
        m_remove_input_button.addActionListener(new ActionListener() {
            public void actionPerformed( ActionEvent _event ) {
                removeInputPorts();
            }
        });

        box1.add(m_remove_input_button);
        
        JPanel box2 = new JPanel();
        box2.setLayout(new BoxLayout(box2, BoxLayout.X_AXIS)); 
        box2.setBackground(Color.lightGray);

        m_replicate_input = new JButton("Replicate");
        m_replicate_input.addActionListener(new ActionListener() {
            public void actionPerformed( ActionEvent _event ) {
                replicateInputPorts();
            }
        });
        box2.add(m_replicate_input);
        
        Integer value = new Integer(1); 
        Integer min = new Integer(1);
        Integer max = new Integer(10); 
        Integer step = new Integer(1); 
        SpinnerNumberModel model = new SpinnerNumberModel(value, min, max, step);     
        m_input_spinner = new JSpinner(model);
        m_input_spinner.setMaximumSize(new Dimension(40,20));
        box2.add(m_input_spinner);
        box2.setPreferredSize(new Dimension(100,30));
        
        _container.add(box1);
        _container.add(box2);
    }
    

    /** Create the components in the middle of the dialog.  It consists
        of the different datatypes and an entry to put the port name. */

    private void createNewPortSettings(Container _container) {
        JPanel box1 = new JPanel();
        box1.setLayout(new BoxLayout(box1, BoxLayout.Y_AXIS));
        box1.setBackground(Color.lightGray);

        m_add_input_button = new JButton("<");
        m_add_input_button.addActionListener(new ActionListener() {
            public void actionPerformed( ActionEvent _event ) {
                addInputPorts();
            }
        });
        box1.add(m_add_input_button);
        box1.add(Box.createRigidArea(new Dimension(0, 30)));
        
        JPanel box2 = new JPanel();
        box2.setLayout(new BoxLayout(box2, BoxLayout.Y_AXIS));
        box2.setBackground(Color.lightGray);
        JLabel name_label = new JLabel("Name: ");

        m_new_port_name = new JTextField();
        m_new_port_name.setBorder(new EtchedBorder());
        JScrollPane name_scroll_pane = new JScrollPane(m_new_port_name,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        name_scroll_pane.setBorder(BorderFactory.createLoweredBevelBorder());
        name_label.setLabelFor(name_scroll_pane);
        name_label.setHorizontalAlignment(SwingConstants.LEFT);


        box2.add(name_label);
        box2.add(name_scroll_pane); 
  
        Object[] data_types = 
            new Object[GenericPortProxy.DATA_TYPE_BY_STRING_ARRAY.length];
        int j=0;
        for (int i = 0; i < GenericPortProxy.DATA_TYPE_BY_STRING_ARRAY.length; 
             i++) {
            if (i != GenericPortProxy.DATA_TYPE_SINGLE_MODE_FIELD &&
                i != GenericPortProxy.DATA_TYPE_BUNDLE &&
                i != GenericPortProxy.DATA_TYPE_USER_DEFINED &&
                i != GenericPortProxy.DATA_TYPE_1_UNRESOLVED){
                data_types[j++] = GenericPortProxy.DATA_TYPE_BY_STRING_ARRAY[i];
            }
        }
    
        m_data_type_list = new JList(data_types);
        JScrollPane list_scroller = new JScrollPane(m_data_type_list);
        list_scroller.setPreferredSize(new Dimension(50, 250));
        box2.add(list_scroller);

        JPanel box3 = new JPanel();
        box3.setBackground(Color.lightGray);
        box3.setLayout(new BoxLayout(box3, BoxLayout.Y_AXIS));
        m_add_output_button = new JButton(">");
        m_add_output_button.addActionListener(new ActionListener() {
            public void actionPerformed( ActionEvent _event ) {
                addOutputPorts();
            }
        });
        box3.add(m_add_output_button);
        box3.add(Box.createRigidArea(new Dimension(0, 30)));

        _container.add(box1);
        _container.add(box2);
        _container.add(box3);
    }
    
    /** Create the components for defining output ports.  The list's 
        DefaultListModel is composed of PortProxy objects */

    private void createOutputPortSettings(Container _container) {
        JPanel box1 = new JPanel();
        box1.setLayout(new BoxLayout(box1, BoxLayout.X_AXIS)); 
        box1.setBackground(Color.lightGray);

        m_remove_output_button = new JButton("<");
        m_remove_output_button.addActionListener(new ActionListener() {
            public void actionPerformed( ActionEvent _event ) {
                removeOutputPorts();
            }
        });
        box1.add(m_remove_output_button);
    
        m_output_data_list = new JList(m_output_list_model);
        JScrollPane list_scroller = new JScrollPane(m_output_data_list);
        list_scroller.setPreferredSize(new Dimension(250, 80));
        box1.add(list_scroller);

        JPanel box2 = new JPanel();
        box2.setLayout(new BoxLayout(box2, BoxLayout.X_AXIS)); 
        box2.setBackground(Color.lightGray);

        m_replicate_output = new JButton("Replicate");
        m_replicate_output.addActionListener(new ActionListener() {
            public void actionPerformed( ActionEvent _event ) {
                replicateOutputPorts();
            }
        });
        box2.add(m_replicate_output);
        
        Integer value = new Integer(1); 
        Integer min = new Integer(1);
        Integer max = new Integer(10); 
        Integer step = new Integer(1); 
        SpinnerNumberModel model = new SpinnerNumberModel(value, min, max, step);     
        m_output_spinner = new JSpinner(model);
        m_output_spinner.setMaximumSize(new Dimension(40,20));
        box2.add(m_output_spinner);
        box2.setPreferredSize(new Dimension(100,30));
        
        _container.add(box1);
        _container.add(box2);
    }

    /** Create OK and Cancel buttons.*/

    private void createSettingsButtons(Container _container) {
        JPanel button_panel;    
        button_panel = new JPanel();        
        button_panel.setLayout(new FlowLayout (FlowLayout.CENTER, 5, 5));
        button_panel.setBackground(Color.lightGray);

        JButton ok_button;    
        ok_button = new JButton();        
        ok_button.setText(OK);
        ok_button.setMnemonic('O');
        ok_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                okButtonActionPerformed(_event);
            }
        });   
        button_panel.add(ok_button);

        JButton cancel_button;    
        cancel_button = new JButton();        
        cancel_button.setText(CANCEL);
        cancel_button.setMnemonic('C');
        cancel_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                cancelButtonActionPerformed(_event);
            }
        });   

        button_panel.add(cancel_button);

        final JButton port_placement_button = new JButton();        
        port_placement_button.setText(SHOW_NODE);
        port_placement_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {

                if (port_placement_button.getText().equals(SHOW_NODE)) {
                    port_placement_button.setText(HIDE_NODE);
                    if (m_working_udp_editor == null) {
                        Alfi.getMainWindow().show(m_working_udp, null, false);
                        m_working_udp_editor = Alfi.getTheNodeCache().
                                                 getEditSession(m_working_udp);
                        m_working_udp_editor.setAlwaysOnTop(true);
                        m_working_udp_editor.
                                         disableParameterSettingWindowAccess();

                            // when focus lost, checks if window closed
                        m_working_udp_editor.addWindowFocusListener(
                            new WindowAdapter() {
                                public void windowLostFocus(WindowEvent _e) {
                                    Window node_window = _e.getWindow();
                                    if ((! node_window.isShowing()) &&
                                             port_placement_button.
                                                 getText().equals(HIDE_NODE)) {
                                       port_placement_button.setText(SHOW_NODE);
                                    }
                                }
                            });

                            // listens for iconization/deiconization
                        m_working_udp_editor.addWindowStateListener(
                            new WindowAdapter() {
                                public void windowStateChanged(WindowEvent _e) {
                                    int new_state = _e.getNewState();
                                    String new_button_text =
                                            (new_state == Frame.ICONIFIED) ?
                                                         SHOW_NODE : HIDE_NODE;
                                    if (! port_placement_button.
                                            getText().equals(new_button_text)) {
                                        port_placement_button.setText(
                                                              new_button_text);
                                    }
                                }
                            });

                    }
                    else {
                        m_working_udp_editor.setExtendedState(Frame.NORMAL);
                    }
                    m_working_udp_editor.setVisible(true);
                    port_placement_button.setText(HIDE_NODE);
                }
                else {
                    port_placement_button.setText(SHOW_NODE);
                    m_working_udp_editor.setVisible(false);
                }
            }
        });   
        button_panel.add(port_placement_button);

        _container.add(button_panel);
    }

    public boolean validateIconPath(String _test_icon_path) {
        String icon_path = new String(_test_icon_path);
        icon_path = icon_path.trim();
        String full_path = icon_path;

        if (icon_path.length() == 0) {
            return false;
        }
        else {
            // check if the file really exists
            if (_test_icon_path.startsWith(File.separator)) {
                // An absolute path has been entered.  Check if it does exist.
                File test_file = new File(_test_icon_path);
                if (test_file == null) { return false; }   
            }     
            else { 
                // If the file is a relative path filename,
                // check if the Icon file name is in the E2E_PATH.
                File icon_file =
                             MiscTools.findFile(_test_icon_path, Alfi.E2E_PATH);

                if (icon_file == null) {return false;}

                full_path = icon_file.getPath();
            }
        }

        // Now check if the file is an icon 
        if ( full_path.toLowerCase().endsWith("jpg") ||
             full_path.toLowerCase().endsWith("gif") ||
             full_path.toLowerCase().endsWith("xbm") ) {
            return true;
        }
        else {
            return false;
        } 
     }


        /** If the dialog has problems creating the m_working_udp,
            the dialog is not workable */

     public boolean isValid() { return (m_working_udp != null); }

     private void invalidImageSelectedMessage() {
        String message = 
            "The selected file does not contain a valid image.\n" +
            "Please make another selection.";
        String title = "Invalid Icon File Name";

        JOptionPane.showMessageDialog(this, message, title,
                                          JOptionPane.INFORMATION_MESSAGE);
    }

        /** If this is a new UDP, create the new UDP file and read it in.  If
          * this is an edited verson of an already existing node, find the
          * changes and try to apply them- and if all goes well, write out
          * the new version of te UDP.
          */
    protected void okButtonActionPerformed (ActionEvent _event) {
        UserDefinedPrimitive udp_to_save = null;
        String udp_name = null;
        File udp_file = null;
        if (m_original_udp == null) {
                // this is a new UDP
            udp_to_save = m_working_udp;
            udp_name = udp_to_save.determineLocalName();
            String file_name = udp_name + ALFIFile.UDP_FILE_SUFFIX;
            udp_file = new File(Alfi.WORKING_DIRECTORY, file_name);
        }
        else {
            // this is after an edit of an existing UDP
            udp_to_save = m_original_udp;
            udp_name = udp_to_save.determineLocalName();
            String file_name = udp_name + ALFIFile.UDP_FILE_SUFFIX;
            udp_file = MiscTools.findFile(file_name, Alfi.E2E_PATH);
        }

            // try to apply the changes to the node being edited.  if there
            // are problems, return to the dialog after warning user.
        boolean b_changes_made = false;
        try { b_changes_made = applyChanges(udp_to_save); }
        catch(UDPChangesCanceledException _e) {
            String title = "Existing Connections Problem";
            String message =
                "An existing connection to the port [" + _e.getPort() + "]\n" +
                "in node [" + _e.getContainer() + "] must be removed before\n" +
                "port [" + _e.getPort() + "] can be modified or removed\n" +
                "in UDP [" + udp_to_save + "].\n\n" +
                "Would you like to open a an edit session for node [" +
                                                      _e.getContainer() + "]?";
            int response = JOptionPane.showConfirmDialog(this, message, title,
                   JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
            if (response == JOptionPane.YES_OPTION) {
                Alfi.getMainWindow().showNodeWindow(_e.getContainer());
            }

                // don't save anything, but keep dialog up...
            b_changes_made = false;
            udp_to_save = null;
            udp_name = null;
            udp_file = null;
        }

            // write the UDP to file
        if (b_changes_made && (udp_to_save != null) && (udp_file != null)) {

                // Save the icon to the working directory
            if (udp_to_save.getIconInfo() != null) {
                String new_icon_file_path = m_working_udp.getTempIconPath();
                if (new_icon_file_path != null) {
                    saveIconInWorkingDirectory(new_icon_file_path);
                }
            }

            StringBuffer udp_file_buffer =
                                new StringBuffer(Parser.getVersionSignature());
            udp_file_buffer.append(Parser.NL);
            udp_file_buffer.append(udp_name);
            udp_file_buffer.append(Parser.NL);
            udp_file_buffer.append(Parser.SECTION_BEGIN_MARKER);
            udp_file_buffer.append(Parser.NL);

            String description = Parser.createNodeContentsString(udp_to_save,
                                           true, true, Parser.STANDARD_SPACER);
            udp_file_buffer.append(description);
            udp_file_buffer.append(Parser.SECTION_END_MARKER);
            udp_file_buffer.append(Parser.NL);

                // write the .udp file
            try {
                FileWriter udp_writer = new FileWriter(udp_file);
                System.out.println("Writing " + udp_file);
                String udp_file_string = udp_file_buffer.toString();
                udp_writer.write(udp_file_string, 0,
                                             udp_file_string.length());
                udp_writer.flush();
                udp_writer.close();
            }
            catch(IOException _e) {
                System.err.println(
                    "Parser.createNodeContentsString(...):\n" +
                    "\tProblem occured when attempting to write " +
                    udp_file + ".");
            }
        }

            // if udp_to_save is null, do not close the dialog
        if (udp_to_save != null) {
            if (udp_to_save != m_working_udp) {
                // get rid of the working UDP if it was used only for editting
                // an existing UDP
                Alfi.getMainWindow().unload(m_working_udp, false);
            }
    
            // Check if the udp_to_save is in any containers
            boolean in_container = false;
            ALFINode[] derived_nodes = udp_to_save.derivedNodes_getDirect();
            if (derived_nodes.length > 0) {
                in_container = true;
            }
            
            if (in_container == true) {
                Alfi.getMainWindow().reload(udp_to_save, true);
            }
            else {
                Alfi.getMainWindow().unload(udp_to_save, false);
            }
           
            doClose(RET_OK);
        }
        else {
                // reset the dialog to the working UDP state
            updateDialogPortListsFromWorkNodeData();
        }
    }

        /** If this is a new UDP, then the node to be saved will just be
          * m_working_udp which already has most of the field data in it, so
          * there is no need to update that field data.  only parameter
          * declarations data in the dialog needs to put into m_working_udp
          * before saving it.
          *
          * if, on the other hand, this is an edit of an already existing
          * UDP, then _target_udp will be the original UDP, and all the
          * changes made to the m_working_udp copy will need to be made to
          * _target_udp here (as well as the parameter declaration changes
          * mentioned above.)
          * 
          * applyPortChanges() must be done first because it is here that
          * interface changes would occur which must be checked against
          * boxes which may contain the original and may have existing
          * connections, and thus would cause a UDPChangesCanceledException
          * to be thrown.  users must delete such connections before changing
          * the name, I/O type, and/or data type of a port (the ports may
          * be repositioned without causing such a problem though.)
          */
    private boolean applyChanges(UserDefinedPrimitive _target_udp) throws
                                                  UDPChangesCanceledException {

            // call only if _target_udp is not m_working_udp (in other words,
            // only if _target_udp needs to incorporate changes present
            // in m_working_udp.  this only happens when the dialog is making
            // changes to an already existing udp node.)
        boolean b_port_changes_made =
             (_target_udp == m_working_udp) || applyPortChanges(_target_udp);

            // continue with changes
        boolean b_param_dec_changes_made =
                                applyParameterDeclarationChanges(_target_udp);

            // call only if _target_udp is not m_working_udp (see note above.)
        boolean b_other_changes_made =
             (_target_udp == m_working_udp) || applyMiscChanges(_target_udp);

        boolean b_changes_made = (b_port_changes_made ||
                             b_param_dec_changes_made || b_other_changes_made);

        return b_changes_made;
    }

        /** This method called only when _target_udp is not m_working_udp. */
    private boolean applyPortChanges(ALFINode _target_udp) throws
                                                  UDPChangesCanceledException {
        if (_target_udp == m_working_udp) {
            System.err.println("UserDefinedNodeDialog.applyPortChanges():\n" +
                "\tWARNING: Target node should never be m_working_udp.");
            return false;
        }

            // first we need to check that port modifications and removals
            // do not break connections previously made to such ports in any
            // boxes which are loaded and contain instances of _target_udp.

            // determine which ports need to be removed or modified
        HashMap temp_node_port_key_map = new HashMap();
        PortProxy[] temp_ports = m_working_udp.ports_get();
        for (int i = 0; i < temp_ports.length; i++) {
            temp_node_port_key_map.put(generateUniqueKey(temp_ports[i]),
                                                                temp_ports[i]);
        }

        HashMap target_node_port_key_map = new HashMap();
        PortProxy[] target_ports = _target_udp.ports_get();
        for (int i = 0; i < target_ports.length; i++) {
            String key = generateUniqueKey(target_ports[i]);
            if (temp_node_port_key_map.containsKey(key)) {
                    // same port exists in both.  nothing to do.
                temp_node_port_key_map.remove(key);
            }
            else { target_node_port_key_map.put(key, target_ports[i]); }
        }

            // any ports left in target_node_port_key_map need to be removed (if
            // only modified, we will still remove them and remake them later.)
        ALFINode[] target_instances = _target_udp.derivedNodes_getAll();
        for (Iterator i = target_node_port_key_map.values().iterator();
                                                               i.hasNext(); ) {
            PortProxy port_to_remove = (PortProxy) i.next();

                // check for external connections before removing port
            for (int j = 0; j < target_instances.length; j++) {
                    // the member instance the port sits on
                MemberNodeProxy port_owner =
                           target_instances[j].memberNodeProxy_getAssociated();
                    // the container node containg the port_owner
                ALFINode container = target_instances[j].containerNode_get();
                if (port_to_remove.hasConnections(container, port_owner)) {
                        // this port does have an external connection to it
                    throw new UDPChangesCanceledException(container,
                                                               port_to_remove);
                }
            }
        }

        boolean b_changes_made = false;
            // it is now safe to remove any removed/modified ports from target
        for (Iterator i = target_node_port_key_map.values().iterator();
                                                               i.hasNext(); ) {
            _target_udp.port_remove((PortProxy) i.next());
            b_changes_made = true;
        }

            // now replace modified ports and add new ones
        for (Iterator i = temp_node_port_key_map.values().iterator();
                                                               i.hasNext(); ) {
            PortProxy temp_port = (PortProxy) i.next();
            _target_udp.port_add(temp_port.getName(), temp_port.getIOType(),
                                                temp_port.getDataType(), null);
            b_changes_made = true;
        }

            // finally, just update the port order.  this is safe, and will not
            // cause any exceptions (external connections should auto-adjust.)
        b_changes_made =
                repositionPorts(m_working_udp, _target_udp) || b_changes_made;

        return b_changes_made;
    }

        /** Repositions the ports on _target_node to where they currently are
          * in _model_node.  Returns true if any ports were repositioned.  As
          * the port positions in _target_node are checked against the position
          * in _model_node, the first pot which is out of position is moved,
          * then the second, etc.  But if the move-to location is already
          * taken by another port, then the port positions are exchanged, and
          * the check loop is abandoned, and a new call is then made to
          * reposition port.  This continues until the ports have all been
          * jiggled into place.
          */
    private boolean repositionPorts(ALFINode _model_node,ALFINode _target_node){
        HashMap model_positions_map = new HashMap();
        PortProxy[][] model_positions = _model_node.ports_getByPosition(false);
        for (int edge = 0; edge < PortProxy.EDGE_INVALID; edge++) {
            for (int i = 0; i < model_positions[edge].length; i++) {
                if (model_positions[edge][i] != null) {
                    String port_key=generateUniqueKey(model_positions[edge][i]);
                    Point position = new Point(edge, i); // convenient int pair
                    model_positions_map.put(port_key, position);
                }
            }
        }

        return repositionPorts(model_positions_map, _target_node);
    }

        /** Engine for repositionPorts(ALFINode, ALFINode). */
    private boolean repositionPorts(HashMap _model_positions_map,
                                                       ALFINode _target_node) {
        PortProxy[][] target_positions= _target_node.ports_getByPosition(false);
        for (int edge = 0; edge < PortProxy.EDGE_INVALID; edge++) {
            for (int i = 0; i < target_positions[edge].length; i++) {
                PortProxy target_port = target_positions[edge][i];
                if (target_port != null) {
                    Point new_pos = (Point)
                       _model_positions_map.get(generateUniqueKey(target_port));
                    if ((edge != new_pos.x) || (i != new_pos.y)) {
                            // check new location occupancy (out of place too)
                        PortProxy port_at_new_pos =
                            (target_positions[new_pos.x].length > new_pos.y) ?
                                 target_positions[new_pos.x][new_pos.y] : null;
                        if (port_at_new_pos != null) {
                                // exchange port positions
                            _target_node.port_exchangePositions(target_port,
                                                       port_at_new_pos, false);
                        }
                        else {
                            int[] position =
                                { new_pos.x, new_pos.y, new_pos.x, new_pos.y };
                            _target_node.port_setPosition(target_port,
                                                               position, true);
                        }

                            // target_positions is out of date now.  do again...
                        repositionPorts(_model_positions_map, _target_node);
                        return true;
                    }
                }
            }
        }

        return false;
    }

        /** This method called only when _target_udp is not m_working_udp. */
    private boolean applyMiscChanges(UserDefinedPrimitive _target_node) {
        boolean b_changes_made = false;
            // set primitive group
        String new_group = m_working_udp.getPrimitiveGroup();
        String old_group = _target_node.getPrimitiveGroup();
        if (old_group == null) {
            if (new_group != null) {
                _target_node.setPrimitiveGroup(new_group);
                b_changes_made = true;
            }
        }
        else if (new_group == null) {
            _target_node.setPrimitiveGroup(null);
            b_changes_made = true;
        }
        else if (! old_group.equals(new_group)) {
            _target_node.setPrimitiveGroup(new_group);
            b_changes_made = true;
        }

            // set icon info
        String new_icon_path = m_working_udp.getTempIconPath();
        String old_icon_path = _target_node.getIconFilePath();

        String new_info = m_working_udp.getIconInfo();
        String old_info = _target_node.getIconInfo();

        if ((new_icon_path != null) && (new_icon_path.trim().length() > 0)) {
            if (! new_icon_path.equals(old_icon_path)) {
                _target_node.setIconInfo(m_working_udp.getIconInfo());
                m_working_udp.setLabelType(ALFINode.LABEL_TYPE_ICON);
                _target_node.setLabelType(ALFINode.LABEL_TYPE_ICON);
                b_changes_made = true;
            }
        }
        else if (old_icon_path != null) {
            m_working_udp.setLabelType(ALFINode.LABEL_TYPE_NAME);
            _target_node.setLabelType(ALFINode.LABEL_TYPE_NAME);
            m_working_udp.setIconInfo(null);
            _target_node.setIconInfo(null);
            b_changes_made = true;
        }

            // set description label
        String new_desc = m_working_udp.getDescriptionLabel();
        String old_desc = _target_node.getDescriptionLabel();
        if (old_desc == null) {
            if (new_desc != null) {
                _target_node.setDescriptionLabel(new_desc);
                b_changes_made = true;
            }
        }
        else if (new_desc == null) {
            _target_node.setDescriptionLabel(null);
            b_changes_made = true;
        }
        else if (! old_desc.equals(new_desc)) {
            _target_node.setDescriptionLabel(new_desc);
            b_changes_made = true;
        }

            // set visual state (name, icon, description?)
        if (_target_node.getLabelType() != m_working_udp.getLabelType()) { 
            _target_node.setLabelType(m_working_udp.getLabelType());
            b_changes_made = true;
        }

            // set comment
        String new_comment = m_working_udp.comment_getLocal();
        String old_comment = _target_node.comment_getLocal();
        if (old_comment == null) {
            if (new_comment != null) {
                _target_node.comment_addLocal(new_comment);
                b_changes_made = true;
            }
        }
        else if (new_comment == null) {
            _target_node.comment_addLocal(null);
            b_changes_made = true;
        }
        else if (! old_comment.equals(new_comment)) {
            _target_node.comment_addLocal(new_comment);
            b_changes_made = true;
        }

            // set center ports flag
        if (_target_node.getCentersPortsFlag() !=
                                         m_working_udp.getCentersPortsFlag()) { 
            _target_node.setCentersPortsFlag(
                                          m_working_udp.getCentersPortsFlag());
            b_changes_made = true;
        }

        return b_changes_made;
    }
 
        /** Generates a key to ID a port in more than just name. */
    private String generateUniqueKey(PortProxy _port) {
        String unique_port_key = _port.getName() + "__" +
                                _port.getIOType() + "__" + _port.getDataType();
        return unique_port_key;
    }

        /** Update _target_udp with the data in the dialog's parameter
          * declaration list.  Return true if any updates were needed.
          */
    private boolean applyParameterDeclarationChanges(
                                            UserDefinedPrimitive _target_udp) {
        boolean b_changes_made = false;

            // the parameter list contains the only data regarding the state
            // of the new/edited UDP which doesn't reside in m_working_udp.
        HashMap dialog_dec_key_map = new HashMap();
        ParameterDeclaration[] dialog_param_decs = getParameterDeclarations();
        for (int i = 0; i < dialog_param_decs.length; i++) {
            String key = generateUniqueKey(dialog_param_decs[i]);
            dialog_dec_key_map.put(key, dialog_param_decs[i]);
        }

        HashMap target_node_dec_map = new HashMap();
        ParameterDeclaration[] target_node_decs =
                             _target_udp.parameterDeclarations_getLocal();
        for (int i = 0; i < target_node_decs.length; i++) {
            String key = generateUniqueKey(target_node_decs[i]);
                // remove matches- these param decs are the same in both
            if (dialog_dec_key_map.containsKey(key)) {
                dialog_dec_key_map.remove(key);
            }
            else { target_node_dec_map.put(key, target_node_decs[i]); }
        }

            // any decs left in dialog_dec_key_map refer to adds or changes
            // that need to be made
        for (Iterator i = dialog_dec_key_map.values().iterator(); i.hasNext();){
            b_changes_made = true;

            ParameterDeclaration new_dec_temp = (ParameterDeclaration) i.next();
            String name = new_dec_temp.getName();

                // deal with old declaration with same name
            ParameterDeclaration old_dec =
                              _target_udp.parameterDeclaration_getLocal(name);
            if (old_dec != null) {
                _target_udp.parameterDeclaration_removeLocal(name);
                String old_dec_key = generateUniqueKey(old_dec);
                target_node_dec_map.remove(old_dec_key);
            }
            _target_udp.parameterDeclaration_add(new_dec_temp.getType(),
                     name, new_dec_temp.getValue(), new_dec_temp.getComment());
        }

            // since the dialog list of param decs should always have all the
            // named declarations in the _target_udp, this should now be empty
        if (! target_node_dec_map.isEmpty()) {
            System.err.println("UserDefinedNodeDialog.applyChanges():\n" +
                               "\tWarning: target_node_dec_map not empty.");
            return false;
        }
        return true;
    }

        /** Generates a key to ID a ParameterDecw in more than just name. */
    private String generateUniqueKey(ParameterDeclaration _param_dec) {
        String unique_declaration_key =
                    _param_dec.getType() + "__" + _param_dec.getName() + "__" +
                    _param_dec.getValue() + "__" + _param_dec.getComment();
        return unique_declaration_key;
    }
 
    protected void cancelButtonActionPerformed(ActionEvent _event) {
        Alfi.getMainWindow().unload(m_working_udp, false);

        super.cancelButtonActionPerformed(_event);
    }


    private String composeIconInfo(String _icon_file_name) {
        String icon_line = "Icon ";

        if (_icon_file_name.toLowerCase().endsWith("jpg")) {
            icon_line += "JPG ";
        }
        else if (_icon_file_name.toLowerCase().endsWith("gif")) {
            icon_line += "GIF ";
        }
        else if (_icon_file_name.toLowerCase().endsWith("xbm")) {
            icon_line += "XBM ";
        }

        icon_line += _icon_file_name;
        return icon_line;
    }

    /** Save the icon in the WORKING directory */

    private boolean saveIconInWorkingDirectory(String _source_icon_file_path){
        if (_source_icon_file_path == null) { return false; }

        File source_icon_file = new File(_source_icon_file_path);

        if (!source_icon_file.exists()) { return false; }

        String icon_name = source_icon_file.getName();

            // Test for the existence of the file in the WORKING DIRECTORY
        File dest_icon_file = new File(Alfi.WORKING_DIRECTORY, icon_name);
        if (dest_icon_file != null && dest_icon_file.exists()) { return true; }
        
            // Icon does NOT exist so copy the source_icon_file onto
            // the WORKING_DIRECTORY
        try { 
            String dest_path = new String(Alfi.WORKING_DIRECTORY + "/" + icon_name);
            System.out.println("Writing new icon file " + dest_path);
            FileIO.copyFile(source_icon_file.getCanonicalPath(), dest_path);
        }
        catch(Exception e) {
                System.err.println(e);
                return false;
        }
        return true;   
    }

  
    /** Put the ports in the input and output lists into a 2D array of 
        PortProxy */

    private PortProxy[][] getPorts() {

        ArrayList set_list = new ArrayList();

        PortProxy[] north_ports = new PortProxy[0];
        set_list.add(north_ports);

        PortProxy[] south_ports = new PortProxy[0];
        set_list.add(south_ports);

        ArrayList east_ports = getPorts(m_output_list_model);
        PortProxy[] output_ports = new PortProxy[east_ports.size()];
        east_ports.toArray(output_ports); 
        set_list.add(output_ports);

        ArrayList west_ports = getPorts(m_input_list_model);
        PortProxy[] input_ports = new PortProxy[west_ports.size()];
        west_ports.toArray(input_ports); 
        set_list.add(input_ports);


        PortProxy[][] ports = new PortProxy[set_list.size()][]; 
        set_list.toArray(ports);
    
        return ports;

    }

        /* This method synchronizes the port information in the port lists
          * with the information contained in m_working_udp.  See also
          * updatePortDisplayNode().
          */
    private void updateDialogPortListsFromWorkNodeData() {

            // get all of the ports on m_working_udp
        PortProxy[] ports = m_working_udp.ports_get();
        HashMap in_map = new HashMap();
        HashMap out_map = new HashMap();
        for (int i = 0; i < ports.length; i++) {
            PortProxy port = ports[i];
            if (port.getIOType() == PortProxy.IO_TYPE_INPUT) {
                in_map.put(port.getName(), port);
            }
            else if (port.getIOType() == PortProxy.IO_TYPE_OUTPUT) {
                out_map.put(port.getName(), port);
            }
        }

            // ignore listed ports which are also in m_working_udp, and remove
            // listed ports which do not appear in m_working_udp
        ArrayList dialog_input_list = getPorts(m_input_list_model);
        for (Iterator i = dialog_input_list.iterator(); i.hasNext(); ) {
            PortProxy listed_port = (PortProxy) i.next();
            String listed_port_name = listed_port.getName();
            if (in_map.containsKey(listed_port_name)) {
                PortProxy port = (PortProxy) in_map.get(listed_port_name);
                    // possible that only data type changed...
                if (port.getDataType() == listed_port.getDataType()) {
                        // this is the result if there are no changes
                    in_map.remove(listed_port_name);
                }
                else { m_input_list_model.removeElement(listed_port); }
            }
            else { m_input_list_model.removeElement(listed_port); }
        }
        ArrayList dialog_output_list = getPorts(m_output_list_model);
        for (Iterator i = dialog_output_list.iterator(); i.hasNext(); ) {
            PortProxy listed_port = (PortProxy) i.next();
            String listed_port_name = listed_port.getName();
            if (out_map.containsKey(listed_port_name)) {
                PortProxy port = (PortProxy) out_map.get(listed_port_name);
                    // possible that only data type changed...
                if (port.getDataType() == listed_port.getDataType()) {
                        // this is the result if there are no changes
                    out_map.remove(listed_port_name);
                }
                else { m_output_list_model.removeElement(listed_port); }
            }
            else { m_output_list_model.removeElement(listed_port); }
        }

            // anything left in the maps needs to be added to the lists
        for (Iterator i = in_map.values().iterator(); i.hasNext(); ) {
            PortProxy port = (PortProxy) i.next();
            port.addProxyChangeListener(this);
            

            PortProxy in_list_port =
                   new PortProxy(port.getName(), new Integer(port.getIOType()),
                                 new Integer(port.getDataType()), null);

            m_input_list_model.addElement(in_list_port);
        }
        for (Iterator i = out_map.values().iterator(); i.hasNext(); ) {
            PortProxy port = (PortProxy) i.next();
            port.addProxyChangeListener(this);


            PortProxy out_list_port = 
                   new PortProxy(port.getName(), new Integer(port.getIOType()),                                  new Integer(port.getDataType()), null);

            m_output_list_model.addElement(out_list_port);
        }
    }

        /** This method synchronizes the port information in the display node
          * with the info in the port lists.  See als updateDialogPortLists().
          */
    private void updateTempNodeFromDialogPortLists() {

            // organize m_working_udp's existing ports, port vs name in/out maps
        PortProxy[] exsisting_node_ports = m_working_udp.ports_getLocal();
        HashMap exsisting_node_inputs_map = new HashMap();
        HashMap exsisting_node_outputs_map = new HashMap();
        for (int i = 0; i < exsisting_node_ports.length; i++) {
            PortProxy port = exsisting_node_ports[i];
            if (port.getIOType() == PortProxy.IO_TYPE_INPUT) {
                exsisting_node_inputs_map.put(port.getName(), port);
            }
            else if (port.getIOType() == PortProxy.IO_TYPE_OUTPUT) {
                exsisting_node_outputs_map.put(port.getName(), port);
            }
        }

            // get the dialog's lists' ports
        ArrayList dialog_input_list = getPorts(m_input_list_model);
        ArrayList dialog_output_list = getPorts(m_output_list_model);

        ArrayList ports_to_add_list = new ArrayList();

        for (Iterator i = dialog_input_list.iterator(); i.hasNext(); ) {
            PortProxy port = (PortProxy) i.next();
            String port_name = port.getName();
            if (exsisting_node_inputs_map.containsKey(port_name)) {
                exsisting_node_inputs_map.remove(port_name);
            }
            else { ports_to_add_list.add(port); }
        }

        for (Iterator i = dialog_output_list.iterator(); i.hasNext(); ) {
            PortProxy port = (PortProxy) i.next();
            String port_name = port.getName();
            if (exsisting_node_outputs_map.containsKey(port_name)) {
                exsisting_node_outputs_map.remove(port_name);
            }
            else { ports_to_add_list.add(port); }
        }

            // any ports left in the maps need to be removed from the node
        for (Iterator i = exsisting_node_inputs_map.values().iterator();
                                                               i.hasNext(); ) {
            PortProxy port = (PortProxy) i.next();
            port.removeProxyChangeListener(this);
            m_working_udp.port_remove(port);
        }
        for (Iterator i = exsisting_node_outputs_map.values().iterator();
                                                               i.hasNext(); ) {
            PortProxy port = (PortProxy) i.next();
            port.removeProxyChangeListener(this);
            m_working_udp.port_remove(port);
        }

            // now add any ports in the ports_to_add_list to the node
        for (Iterator i = ports_to_add_list.iterator(); i.hasNext(); ) {
            PortProxy temp_port = (PortProxy) i.next();
            PortProxy port = m_working_udp.port_add(temp_port.getName(),
                         temp_port.getIOType(), temp_port.getDataType(), null);
            port.addProxyChangeListener(this);
        }
    }

    /** Returns an ArrayList of ports in the specified list model */

    private ArrayList getPorts( DefaultListModel _list_model) {
        ArrayList ports = new ArrayList();
        for (int i = 0; i < _list_model.getSize(); i++) {
            PortProxy port = ( PortProxy )_list_model.getElementAt(i);
            ports.add(port);
        }
        
        return ports;

    }

    /** Returns an array of ParameterDeclarations, including the default
        values */
    private ParameterDeclaration[] getParameterDeclarations() {
        
        ArrayList param_decls = new ArrayList();
        for (int i = 0; i < m_param_decl_list_model.getSize(); i++) {
            ParameterDeclaration param = ( ParameterDeclaration )
                                m_param_decl_list_model.getElementAt(i);
            param_decls.add(param);
        }
        
        ParameterDeclaration[] param_array = 
            new ParameterDeclaration[param_decls.size()];
        param_decls.toArray(param_array);

        return param_array;

    }

    /** Action performed when creating a new input port button is
        selected (the left arrow button in the center of the dialog)*/
    private void addInputPorts () {

        if ( (m_new_port_name.getText().length() > 0) &&
             (m_data_type_list.getSelectedValue() != null) ) {
            String data_type =  ( String )m_data_type_list.getSelectedValue();
            int data_type_id = 
                           GenericPortProxy.getDataTypeIdFromString(data_type);
            String entered_name = m_new_port_name.getText();

            String port_name =
                        changeIdenticalNames(entered_name, m_input_list_model);
            
            PortProxy new_port = new PortProxy(port_name,
                new Integer(PortProxy.IO_TYPE_INPUT),
                new Integer(data_type_id), null);
            
            m_input_list_model.addElement(new_port);
        }

            // update the node
        updateTempNodeFromDialogPortLists();
    }

    /** Action performed when removing an input port setting in the list
        (the right arrow button in the left side of the dialog)*/
    private void removeInputPorts () {
        
        int[] selected_indices = m_input_data_list.getSelectedIndices();

        for (int i = (selected_indices.length - 1); i >= 0 ; i--) {
            m_input_list_model.removeElementAt(selected_indices[i]);
        }

            // update the node
        updateTempNodeFromDialogPortLists();
    }

    /** Action performed when creating a new output port button is
        selected (the right arrow button in the center of the dialog)*/
    private void addOutputPorts() {
        if ( (m_new_port_name.getText().length() > 0) &&
             (m_data_type_list.getSelectedValue() != null) ) {
            String data_type =  ( String )m_data_type_list.getSelectedValue();
            int data_type_id = 
                           GenericPortProxy.getDataTypeIdFromString(data_type);
            String entered_name = m_new_port_name.getText();

            String port_name =
                       changeIdenticalNames(entered_name, m_output_list_model);
 
            PortProxy new_port = new PortProxy(port_name,
                new Integer(PortProxy.IO_TYPE_OUTPUT),
                new Integer(data_type_id), null);
            
            m_output_list_model.addElement(new_port);
        }

            // update the node
        updateTempNodeFromDialogPortLists();
    }

    /** Action performed when removing an output port setting in the list
        (the left arrow button in the right side of the dialog)*/
    private void removeOutputPorts () {
        int[] selected_indices = m_output_data_list.getSelectedIndices();

        for (int i = (selected_indices.length - 1); i >= 0 ; i--) {
            m_output_list_model.removeElementAt(selected_indices[i]);
        }

            // update the node
        updateTempNodeFromDialogPortLists();
    }

    /** Using the spinner and replicate buttons on the input setting section, 
        the selection can be replicated N number of times */
    private void replicateInputPorts() {

        Integer num_to_replicate = ( Integer )m_input_spinner.getValue();

        // Make a list of all ports and member declarations already defined
        String[] strings_taken = new String[m_input_list_model.getSize()];
        for (int i = 0; i <  m_input_list_model.getSize(); i++) {
            strings_taken[i] =
                (( PortProxy )m_input_list_model.getElementAt(i)).getName();
        }

        replicatePorts(num_to_replicate.intValue(),
                       m_input_data_list, strings_taken);

        updateTempNodeFromDialogPortLists();
    }

    /** Using the spinner and replicate buttons on the input setting section, 
        the selection can be replicated N number of times */
    private void replicateOutputPorts() {
        Integer num_to_replicate = ( Integer )m_output_spinner.getValue();

        // Make a list of all ports and member declarations already defined
        String[] strings_taken = new String[m_output_list_model.getSize()];
        for (int i = 0; i <  m_output_list_model.getSize(); i++) {
             strings_taken[i] = 
                (( PortProxy )m_output_list_model.getElementAt(i)).getName();
        }

        replicatePorts(num_to_replicate.intValue(),
                       m_output_data_list, strings_taken);

        updateTempNodeFromDialogPortLists();
    }

    /** Method to replicate a list of port proxies */
    private void replicatePorts(int _num_repeats, JList _list, 
                                                        String[] _taken_names) {

        Object[] input_selection = _list.getSelectedValues();
        DefaultListModel list_model = ( DefaultListModel )_list.getModel();

        for (int i = 0; i < _num_repeats; i++) {
            for (int j = 0; j < input_selection.length; j++) {
                PortProxy port = ( PortProxy ) input_selection[j];
                String port_name = port.getName();
                
                String new_name = changeIdenticalNames(port_name, list_model);
                
                PortProxy new_port = new PortProxy(new_name,
                                           new Integer(port.getIOType()),
                                           new Integer(port.getDataType()), null);
            
                list_model.addElement(new_port);
            }
        }
    } 

    /** Check the proposed port name with the list of names in the 
        DefaultListModel.  If the name is already taken, then the 
        numerical suffix is incremented */
    private String changeIdenticalNames(String _proposed_name, 
                                        DefaultListModel _list_model) {

        String[] names_list = new String[_list_model.getSize()];

        for (int i = 0; i < _list_model.getSize(); i++) {
            PortProxy port_proxy = ( PortProxy )_list_model.getElementAt(i);
            names_list[i] = port_proxy.getName();
        }

        return new String(changeIdenticalNames(_proposed_name, names_list));
    }

    /** 
      * Check the proposed port name with the list of names.  If the name
      * is already taken, then the numerical suffix is incremented 
      */
    private String changeIdenticalNames(String _proposed_name, 
                                        String[] _taken_names) {

        String new_name = _proposed_name;
        while (nameIsTaken(new_name, _taken_names)) {
            new_name = iterateName(new_name);
        }
        
        return new_name;
    }

        /**
         * Checks if PortProxy _ports[_index_of_testee] has an identical
         * name to any name of any other ports or to any name in
         * _other_disallowed_names.
         */
    private boolean nameIsTaken(String _test_name,
                                String[] _other_disallowed_names) {

        for (int i = 0; i < _other_disallowed_names.length; i++) {
            if (_other_disallowed_names[i].equals(_test_name)) {
                return true;
            }
        }

        return false;
    }


        /**
         * Advances the integer on the end of the name by 1, or adds "_1" if
         * there is no such construct yet.  E.g., ABMatrix_2 -> ABMatrix_3, and
         * ABMatrix -> ABMatrix_1
         */
    private String iterateName(String _name) {
        String iterated_name = null;
        if (_name.matches("^.*_\\d+$")) {
            try {
                String[] parts = _name.split("_");
                int counter = Integer.parseInt(parts[parts.length - 1]);
                counter++;
                parts[parts.length - 1] = Integer.toString(counter);
                iterated_name = new String(parts[0]);
                for (int i = 1; i < parts.length; i++) {
                    iterated_name += "_" + parts[i];
                }
            }
            catch(NumberFormatException e) { ; }
        }

        if (iterated_name == null) { iterated_name = _name + "_1"; }

        return iterated_name;
    }

    //////////////////////////////////////////////////////////
    ////////// NodeContentChangeEventListener methods ////////

        /** NodeContentChangeEventListener method.  This event occurs before the
          * change is made inside m_working_udp, so we cannot just call
          * updateDialogPortListsFromWorkNodeData().
          */
    public void nodeContentChanged(NodeContentChangeEvent _event) {
        if (_event.getAction() == NodeContentChangeEvent.PORT_DELETE) {
                // delete port that is about to be deleted from m_working_udp
            PortProxy port_to_delete= (PortProxy) _event.getAssociatedElement();
            DefaultListModel io_list_model_to_delete_from =
                      (port_to_delete.getIOType() == PortProxy.IO_TYPE_INPUT) ?
                                      m_input_list_model : m_output_list_model;
            ArrayList dialog_port_list = getPorts(io_list_model_to_delete_from);
            for (Iterator i = dialog_port_list.iterator(); i.hasNext(); ) {
                PortProxy listed_port = (PortProxy) i.next();
                if (listed_port.getName().equals(port_to_delete.getName())) {
                    io_list_model_to_delete_from.removeElement(listed_port);
                    break;
                }
            }
        }
    }

    //////////////////////////////////////////////////////////
    ////////// NodeInterfaceChangeEventListener methods //////

        /** NodeInterfaceChangeEventListener method. */
    public void nodeInterfaceChange(NodeInterfaceChangeEvent _event) {
        updateDialogPortListsFromWorkNodeData();
    }

    //////////////////////////////////////////////////////////
    ////////// ProxyChangeEventListener methods ////////

        /** ProxyChangeEventListener method.  This event occurs after the change
          * has been made to m_working_udp, so we can just call
          * updateDialogPortListsFromWorkNodeData().
          */
    public void proxyChangePerformed(ProxyChangeEvent _event) {
        updateDialogPortListsFromWorkNodeData();
    }

    //////////////////////////////////////////////////////////
    ////////// ContainedElementModEventListener methods //////

        /** ContainedElementModEventListener method.  This dialog listens to
          * these events from m_working_udp to let it know if the node's
          * comment has changed.
          */
    public void containedElementModified(ContainedElementModEvent _e) {
        m_comment_pane.setText(m_working_udp.comment_getLocal());
    }

    private static class UDPChangesCanceledException extends Exception {
        private ALFINode m_container;
        private PortProxy m_problem_port;

        public UDPChangesCanceledException(ALFINode _container,
                                                     PortProxy _problem_port) {
            super();
            m_container = _container;
            m_problem_port = _problem_port;
        }

        public ALFINode getContainer() { return m_container; }

        public PortProxy getPort() { return m_problem_port; }
    }
}
