/*
 * A Settings Panel is a convenient way to define a JPanel with a BoxLayout.
 *
 */

package edu.caltech.ligo.alfi.dialogs;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class SettingsPanel extends JPanel {
    final static boolean COLORS = true;

    private int m_height;
    private int m_width;

    SettingsPanel (JDialog _dialog, String _title, int _width, int _height) {
        this(_dialog, _title, _width, _height, BoxLayout.X_AXIS);
    }
    SettingsPanel (JDialog _dialog, String _title, 
                   int _width, int _height, int _box_layout_axis) {

        m_height = _height;
        m_width = _width;

        if (COLORS) {
            setBackground(Color.lightGray);
        }

        setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createTitledBorder(_title),
            BorderFactory.createEmptyBorder(5,5,5,5)));

        setLayout(new BoxLayout(this, _box_layout_axis));
    }

    public Dimension getMinimumSize() {
        return getPreferredSize();
    }
    public Dimension getPreferredSize() {
        return new Dimension(m_width, m_height);
        //super.getPreferredSize().height);
    }
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
}
