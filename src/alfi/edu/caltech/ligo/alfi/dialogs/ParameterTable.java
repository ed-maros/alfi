package edu.caltech.ligo.alfi.dialogs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Dialog;
import java.awt.event.*;
import java.awt.Window;


import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ChangeEvent;


import edu.caltech.ligo.alfi.common.GuiConstants;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;

/**
  * <pre>
  * The table that displays the parameters and values
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */


public class ParameterTable extends JTable 
                            implements ParameterTableIF, MouseListener,
                                       ListSelectionListener{

        // kluge to fix problem when running linux JVM 1.5.x
    private boolean mb_value_change_events_enabled__kluge = false;

    protected final int NUM_COLUMNS = 7;
    private final int[] m_column_widths = {10, 15, 10, 10, 20, 15, 20};
    private int m_table_width;
    private int m_num_rows;
    private JList m_row_header;
    private ArrayList m_edit_dialogs;
    private MultilineEditorDialog m_readonly_dialog;
    private ALFINode m_node;
    private boolean m_editing;
    private boolean m_has_changed_data;
    private int m_has_invalid_data_index;
    private String m_last_cell_value;
    private int m_last_cell_row;

    public ParameterTable (Dialog _parent, 
                           ALFINode _node, 
                           String[] _column_names, 
                           int _table_width) {

        super (new ParameterTableModel(_node, 
                                       _column_names));

        m_num_rows = getRowCount();
        m_table_width = _table_width;
        m_node = _node;
        m_editing = false;
        m_has_changed_data = false;
        m_has_invalid_data_index = -1;
        m_last_cell_value = "";
        m_edit_dialogs = new ArrayList(m_num_rows);

        createValueEditors(_parent);

        showHorizontalLines = true;
        showVerticalLines = true;
        rowHeight = 20;

        initColumnSizes();
        initDataCells();

            // Setup the selection model 
        getSelectionModel().setSelectionMode(
                                          ListSelectionModel.SINGLE_SELECTION);

            // Add the different listeners
        addMouseListener(this);
        
        addFocusListener(new FocusListener() {
            public void focusLost (FocusEvent _event) {
                TableCellEditor editor = getCellEditor();
                if (editor != null) {
                    JTextField text_field = (JTextField)
                         ((DefaultCellEditor) editor).getComponent();
                    Component component = _event.getOppositeComponent();
                    // Need to check this or else can't double click onto
                    // the text field
                    if (!text_field.equals(component)) {
                        editor.stopCellEditing();
                        validateData(getSelectedRow());
                        m_has_changed_data = true;
                    }
                }
            }
            public void focusGained (FocusEvent _event) {
                int row_index = getSelectedRow();
                int column_index = getSelectedColumn();
                showDialogIfVisible(row_index, column_index);
            }
        });

        mb_value_change_events_enabled__kluge = true;
    }
    

    public boolean hasChangedData () {
        return m_has_changed_data;
    }

    private String validateData (int _row_index) {
        String cell_value = "";

        TableCellEditor editor = getCellEditor();
        if (editor == null) {
            cell_value = 
                getValueAt(_row_index, ParameterTableModel.LOCAL_VALUE_INDEX).toString();
        }
        else {
            editor.stopCellEditing();
            int row_index = getSelectedRow();
            int column_index = getSelectedColumn();
            if (row_index == _row_index) {
                String value = ( String )editor.getCellEditorValue();
                value = value.trim();

                if (value.length() == 0) {
                    JOptionPane.showMessageDialog(null, 
                        "Blank or empty values are not allowed.", "Alfi: Warning",
                        JOptionPane.WARNING_MESSAGE);
                    m_has_invalid_data_index = row_index;
                    setTableValue(m_last_cell_value, m_last_cell_row);
                }
                cell_value = value;
            }
        }
        return cell_value;

    }

    private void initColumnSizes () {
        ParameterTableModel model = (ParameterTableModel) getModel();

        TableColumn column = null;
        Component component = null;
        int headerWidth = 0;
        int cellWidth = 0;

        //
        JTableHeader table_header = getTableHeader();
        table_header.setMinimumSize(new Dimension(30, 30));
        TableCellRenderer header_renderer = table_header.getDefaultRenderer();
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        table_header.setDefaultRenderer(renderer);
        //
        // The first column (name) is rendered through using JTableHeader.
        //
        for (int ii = 0; ii < (NUM_COLUMNS - 1); ii++) {   
            column = getColumnModel().getColumn(ii);
            component = header_renderer.getTableCellRendererComponent(
                                 this, column.getHeaderValue(), 
                                 false, false, 0, ii);
            headerWidth = component.getPreferredSize().width;
            int width = m_table_width * m_column_widths[ii+1]/100;
            column.setPreferredWidth(width);
            Dimension size = new Dimension(width, 30);
            table_header.setSize(width, 30); 
            table_header.setPreferredSize(size);
            table_header.setMinimumSize(size);
            table_header.setBorder(UIManager.getBorder
                ("TableHeader.cellBorder"));
         }

    } 

    private void showValue (int _row_index) {

        String value = getValueAt(_row_index, 
                ParameterTableModel.DEFAULT_VALUE_INDEX).toString();
        m_readonly_dialog.setValue(value);

        m_readonly_dialog.setTitle(getRowHeaderName(_row_index)
            + " - DEFAULT VALUE (readonly)");

        m_readonly_dialog.setReadOnly(true);
        m_readonly_dialog.setVisible(true);

    }


    private void editValue (int _row_index, int _column_index) {
        displayValueEditor(_row_index, _column_index);
    }

    private void displayValueEditor (int _row_index, int _column_index) {
        String value = getValueAt(_row_index, _column_index).toString();
        displayValueEditor(_row_index, _column_index, value);
    }

    private void displayValueEditor (int _row_index, int _column_index,
                                                     String _value) {

        if (_column_index != ParameterTableModel.LOCAL_VALUE_INDEX) {
            return;
        }

        if (m_editing) {
            if (getCellEditor() != null) {
                getCellEditor().cancelCellEditing();
            }
            m_editing = false;
        }    

        MultilineEditorDialog edit_dialog = 
            (MultilineEditorDialog) m_edit_dialogs.get(_row_index);
        edit_dialog.setTitle(getRowHeaderName(_row_index)
            + MultilineEditorDialog.DIALOG_TITLE);
        
        if (!edit_dialog.isShowing()) {
            edit_dialog.setValue(_value);
            edit_dialog.setReadOnly(false);
            edit_dialog.setVisible(true);
        }
        else {
            //Kludge to get the focus onto the right window
            edit_dialog.setVisible(false);

            // Yet another kludge... sometimes the dialog is redisplayed
            // really really small.
            edit_dialog.setSize(500, 300);
            edit_dialog.setVisible(true);
        }
    }

    public void createValueEditors (Dialog _parent) {
        for (int ii = 0; ii < m_num_rows; ii++) {
            MultilineEditorDialog edit_dialog = 
                new MultilineEditorDialog(_parent, false, ii, this);
            m_edit_dialogs.add(ii, edit_dialog);
        }

        m_readonly_dialog = new MultilineEditorDialog(_parent, false, 0, null);
        m_readonly_dialog.setReadOnly(true);
    }


    public boolean saveAllValues( ) {
        boolean saved_all = true;
        int num_dialogs = m_edit_dialogs.size(); 
        for (int ii = 0; ii < num_dialogs; ii++) {
            if (!storeModifiedValues(ii)) {
                saved_all = false;    
            } 
        }
        return saved_all;
    }

    public void cancelAllDialogs ( ) {
        int num_dialogs = m_edit_dialogs.size(); 
        for (int ii = 0; ii < num_dialogs; ii++) {
            MultilineEditorDialog edit_dialog = 
                (MultilineEditorDialog) m_edit_dialogs.get(ii);
            edit_dialog.performCancel();
            edit_dialog.dispose();
        }
    }

    private boolean storeModifiedValues (int _row_index) {

        boolean saved_value = true;
        String value = "";
        MultilineEditorDialog edit_dialog = 
            (MultilineEditorDialog) m_edit_dialogs.get(_row_index);

        if (edit_dialog.isShowing()) {
            edit_dialog.performOK();
            value = edit_dialog.getValue();
        }
        else {
            value = validateData(_row_index);
            if (m_has_invalid_data_index != _row_index) {
                updateCell(value, _row_index);
            }
        }

        return (value.length() > 0);
    }

    public void setDefaultValue ( ) {
        int row = getSelectedRow();
        setValueAt(ParameterTableModel.DEFAULT_VALUE, row,
                ParameterTableModel.LOCAL_VALUE_INDEX);
    }

    public void setTableValue (JTextField _text_field) {

        String text_value = _text_field.getText();
        int row = getSelectedRow();

        setTableValue(text_value, row);
    }

    public void setTableValue (String _text_value, int _row) {

        setValueAt(_text_value, _row, ParameterTableModel.LOCAL_VALUE_INDEX);
        m_has_changed_data = true;
        
        TableCellEditor editor = getCellEditor();
        if (editor != null) {
            editor.stopCellEditing();
            m_editing = false;
        }
    }

    public void updateCell (String _text_value, int _row) {
        String trimmed_value = _text_value.trim();
        if (trimmed_value.length() == 0) {
            JOptionPane.showMessageDialog(null, 
            "Blank or empty values are not allowed.", "Alfi: Warning",
            JOptionPane.WARNING_MESSAGE);
            setTableValue(m_last_cell_value, m_last_cell_row);
        }
        else {
            setTableValue(_text_value, _row);
            m_has_changed_data = true;
        }

    }


    public ValueModification[] getParameterValueChanges () {
        ParameterTableModel table_model = (ParameterTableModel) getModel();
        ValueModification[] value_changes = table_model.getDataChanges(); 
        return value_changes;
    }

    public void initDataCells () {
        SimpleCellRenderer renderer =  new SimpleCellRenderer();
        setDefaultRenderer(Object.class, renderer);
        addKeyListener(renderer);

        ParameterTableModel table_model = (ParameterTableModel) getModel();
        ListModel list_model = table_model.getHeaderNameListModel();
        m_row_header = new JList(list_model);

        if (table_model.getRowCount() == 0) {
            m_row_header.setFixedCellWidth
                (m_table_width * m_column_widths[0]/100);
        }

        m_row_header.setFixedCellHeight(this.getRowHeight()); 
        m_row_header.setCellRenderer(new RowHeaderRenderer(this));
    }

    public JList getRowHeader () {
        return m_row_header;
    }

    public boolean isDefaultValue (int _row, int _column) {
        ParameterTableModel model = (ParameterTableModel) getModel();
        return model.isDefaultValue(_row, _column);
    }

    public boolean hasDefaultValue (int _row) {
        ParameterTableModel model = (ParameterTableModel) getModel();
        return model.hasDefaultValue(_row);
    }

    public String  getRowHeaderName (int _row) {
        ParameterTableModel model = (ParameterTableModel) getModel();
        ListModel headers = model.getHeaderNameListModel();
        return (String) headers.getElementAt(_row); 
    }


    private void selectCellComponent () {
        TableCellEditor cell_editor = getCellEditor();
        if (cell_editor != null) {
            Component component = ((DefaultCellEditor)cell_editor).
                getComponent();
            JTextField text = (JTextField) component;
            text.requestFocus();
            text.selectAll();
            m_editing = true;
        }
    }

    private void showDialogIfVisible (int _row, int _column) {
        if (_column ==  ParameterTableModel.LOCAL_VALUE_INDEX) {
            // Check if the edit dialog is already displayed
            MultilineEditorDialog edit_dialog = 
                (MultilineEditorDialog) m_edit_dialogs.get(_row);
            
            if (edit_dialog.isVisible()) {
                TableCellEditor cell_editor = getCellEditor();
            
                if (cell_editor != null) {
                    cell_editor.cancelCellEditing();
                }
                m_editing = false;
                editValue(_row, _column);
            }
            else {
                selectCellComponent();
            }
        }
    }


    // MouseListener methods
    public void mousePressed (MouseEvent _event) {

        if (SwingUtilities.isLeftMouseButton(_event)) {
            int row_index = getSelectedRow();
            int column_index = getSelectedColumn();
            String value = getValueAt(row_index, column_index).toString();

            if (column_index == ParameterTableModel.LOCAL_VALUE_INDEX) {
                value = value.trim();
                if (value.length() > 0) {
                    m_last_cell_value = value;
                    m_last_cell_row = row_index;
                    m_has_changed_data = true;
                }
            }

            int click_count = _event.getClickCount();

            // Double clicks
            if (click_count == 1 ) {
                showDialogIfVisible(row_index, column_index);
            }
            if (click_count == 2 ) {
                if (ParameterTableModel.isMultilineText(value)) {
                    TableCellEditor cell_editor = getCellEditor();
                    if (cell_editor != null) {
                        cell_editor.cancelCellEditing();
                    }
                    m_editing = false;
                    displayValueEditor(row_index, column_index, value);
                } else {
                    showDialogIfVisible(row_index, column_index);
                }
            } else if (click_count == 3 && 
                !ParameterTableModel.isMultilineText(value)) {
                
                showDialogIfVisible(row_index, column_index);
            }
        }
        else if (SwingUtilities.isRightMouseButton(_event)) {
            int row_index = rowAtPoint(_event.getPoint());
            int column_index = columnAtPoint(_event.getPoint());
            String value = getValueAt(row_index, column_index).toString();
            changeSelection(row_index, column_index, false, false);
            if (column_index == ParameterTableModel.DEFAULT_VALUE_INDEX) {
                showValue(row_index);
            } 
            else if (column_index != ParameterTableModel.LOCAL_VALUE_INDEX) {
                if (getCellEditor() != null) {
                    getCellEditor().cancelCellEditing();
                }
                m_editing = false;
                m_readonly_dialog.setVisible(false);
            } 
            else {
                value = value.trim();
                if (value.length() > 0) {
                    m_last_cell_value = value;
                    m_last_cell_row = row_index;
                    m_has_changed_data = true;
                }
                editValue(row_index, column_index);
            }
        }
    }

    public void mouseReleased(MouseEvent e) { ; }
    public void mouseEntered(MouseEvent e) { ; }
    public void mouseExited(MouseEvent e) { ; }
    public void mouseClicked(MouseEvent e) { ; }
    public void valueChanged(ListSelectionEvent e) {
        if (mb_value_change_events_enabled__kluge) { 
            boolean empty_selection = false;
            int selected_index = e.getFirstIndex();
            if (e.getSource() instanceof DefaultListSelectionModel) {
                DefaultListSelectionModel dlsm =
                                     (DefaultListSelectionModel) e.getSource();
                empty_selection = dlsm.isSelectionEmpty();
                selected_index = dlsm.getLeadSelectionIndex();
                if (selected_index >= 0) {
                    String current_value = getValueAt(selected_index, 
                            ParameterTableModel.LOCAL_VALUE_INDEX).toString();
                    String value = getValueAt(m_last_cell_row, 
                            ParameterTableModel.LOCAL_VALUE_INDEX).toString();
                    value = value.trim();
                    if (value.length() > 0) {
                        value = getValueAt(selected_index, 
                            ParameterTableModel.LOCAL_VALUE_INDEX).toString();
                        m_last_cell_value = value.trim();
                        m_last_cell_row = selected_index;
                        m_has_changed_data = true;
                    }
                    else {
                        JOptionPane.showMessageDialog(null, 
                                 "Blank or empty values are not allowed.",
                                 "Alfi: Warning", JOptionPane.WARNING_MESSAGE);
                        setTableValue(m_last_cell_value, m_last_cell_row);
                        changeSelection(m_last_cell_row, ParameterTableModel.
                                              LOCAL_VALUE_INDEX, false, false);
                    }
                }
            }
        }
        super.valueChanged(e);
    }


}
