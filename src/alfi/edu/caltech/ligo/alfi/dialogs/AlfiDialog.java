package edu.caltech.ligo.alfi.dialogs;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.bookkeeper.*;


public class AlfiDialog extends JDialog {

    public static final String PROMPT_SAVE = 
        new String("Do you wish to save your latest changes \nbefore closing the window?");

    public static final int RET_CANCEL = 0;
    public static final int RET_OK = 1;
    public static final int RET_HELP = 2;

    protected static final String CANCEL = new String("Cancel");
    protected static final String OK = new String("OK");
    protected static final String CONTINUE = new String("Continue");
    protected static final String HELP = new String("Help");
    protected static final String SAVE = new String("Save");

    private int m_return_status = RET_CANCEL;
    protected boolean m_read_only = false;
    protected boolean m_closing;

    protected int m_width;
    protected int m_height;
    Frame m_frame;

    public AlfiDialog ( Frame _frame, String _title, boolean _modal, 
                        int _width, int _height ) {
        super(_frame,_title, _modal);

        m_frame = _frame;
        m_width = _width;
        m_height = _height;

        initialize();
    }

    public AlfiDialog ( Dialog _dialog, String _title, boolean _modal, 
                        int _width, int _height ) {
        super(_dialog, _title, _modal);

        m_width = _width;
        m_height = _height;

        initialize();
    }

    protected void initialize() {
        setSize(new Dimension (m_width, m_height));
        pack();

        // Center dialog
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - m_width)/2, 
                    (screenSize.height - m_height)/2);

        setResizable(false);
    }

    public int getReturnStatus() {
        return m_return_status;
    } 

    protected void initComponents () {
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
 
        addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent _event) {
                closeActionPerformed(null);
            }
        });

        setSize(new Dimension (m_width, m_height));
        pack();

        // Center dialog
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - m_width)/2, 
                    (screenSize.height - m_height)/2);

        setResizable(false);
    }

    /**
      * Disallows saving the value
      */
    public void setReadOnly (boolean  _read_only) {
        m_read_only = _read_only;
    }

 
    protected void updateData() {
    }

    protected boolean hasUnsavedData() {
        return false;
    }

    protected boolean hasChangedData() {
        return false;
    }

    /**
      * Closes the dialog.  If any modifications were made,
      * the user is prompted to save.
      */
    protected void closeActionPerformed (ActionEvent _event) {
        try {
            if ( (hasUnsavedData() || hasChangedData()) && !m_read_only) {
                int response =  promptSave();
                if (response == JOptionPane.YES_OPTION) {
                    okButtonActionPerformed(null);
                } else if (response == JOptionPane.NO_OPTION) {
                    doClose(RET_OK);
                } else {
                    return;
                }
            } else {
                // No changes detected, so just cancel out
                doClose(RET_CANCEL);
            }
        } catch (Exception e) {
        }
    }
 
    /**
      * Prompts the user to save the data.
      */
    private int promptSave () {
        int option_type = (m_closing ? 
                           JOptionPane.YES_NO_OPTION :
                           JOptionPane.YES_NO_CANCEL_OPTION); 

        return JOptionPane.showConfirmDialog(this, 
           PROMPT_SAVE, SAVE, option_type, JOptionPane.QUESTION_MESSAGE);
    }

 
    protected void okButtonActionPerformed (ActionEvent _event) {
        try {
            updateData();
            doClose(RET_OK);
        }
        catch (Exception e) { e.printStackTrace(); }
    }
    
    protected void cancelButtonActionPerformed (ActionEvent _event) {
        try {
            doClose(RET_CANCEL);
        }
        catch (Exception e) { e.printStackTrace(); }
    }

    protected void helpButtonActionPerformed (ActionEvent _event) {
        try {
            doClose(RET_CANCEL);
        }
        catch (Exception e) { e.printStackTrace(); }
    }
    
  
    protected void closeDialog (WindowEvent _event) {
        doClose(RET_CANCEL);
    }
    
    
    protected void doClose (int _return_status) {
        m_return_status = _return_status;
        setVisible(false);
    }   
    
}
