package edu.caltech.ligo.alfi.dialogs;

import javax.swing.JOptionPane;
import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.JFrame;
import java.beans.*; //Property change stuff
import java.awt.*;
import java.awt.event.*;

import edu.caltech.ligo.alfi.common.AlfiConstants;
import edu.caltech.ligo.alfi.editor.EditorFrame;

public class EntryDialog extends JDialog {
    public static final String DIALOG_TITLE = "ALFI";
    private static final String SPACE = new String(" ");
    private static final String UNDERSCORE = new String("_");
    public static final int TEXT_WIDTH = 30;
    private String m_typed_text;
    private String m_original_text;

    public String getValidatedText() {
        String validated_text = null;
        // Replace all spaces with underscores
        if (m_typed_text != null) {
            validated_text = m_typed_text.replaceAll(SPACE, UNDERSCORE);
        }
        return validated_text;
    }

    public EntryDialog (EditorFrame _parent_frame, String _prompt_label) {
        this(_parent_frame, _prompt_label, AlfiConstants.EMPTY);
    }

    public EntryDialog (EditorFrame _parent_frame, String _prompt_label, 
                        String _initial_text) {
        super(_parent_frame, _parent_frame.getName(), true);
        m_original_text = _initial_text;

        initComponents(_prompt_label);
        pack();
        Dimension screen_size = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dialog_size = getSize();
        setBounds((screen_size.width - dialog_size.width)/2,
                  (screen_size.height - dialog_size.height)/2,
                   dialog_size.width, dialog_size.height);
    }

    private void initComponents (String _prompt_label) {

        final JTextField text_field = new JTextField(m_original_text,
            TEXT_WIDTH);
        Object[] components = {_prompt_label, text_field};

        final String button_string_1 = "OK";
        final String button_string_2 = "Cancel";
        Object[] options = {button_string_1, button_string_2};

        final JOptionPane option_pane = new JOptionPane(components, 
                                    JOptionPane.QUESTION_MESSAGE,
                                    JOptionPane.OK_CANCEL_OPTION,
                                    null,
                                    options,
                                    options[0]);
        setContentPane(option_pane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
            /*
             * Instead of directly closing the window,
             * we're going to change the JOptionPane's
             * value property.
             */
            option_pane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
            }
        });

        text_field.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                option_pane.setValue(button_string_1);
            }
        });

        option_pane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == option_pane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = option_pane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    option_pane.setValue(
                            JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(button_string_1)){
                        m_typed_text = text_field.getText();
                        setVisible(false);
                    } else { 
                        // user closed dialog or clicked cancel
                        m_typed_text = null;
                        setVisible(false);
                    } 
                }
            }
        });

        text_field.requestFocus();
        text_field.setCaretPosition(0);
        text_field.selectAll();
    }
    

}
