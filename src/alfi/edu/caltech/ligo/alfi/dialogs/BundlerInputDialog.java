package edu.caltech.ligo.alfi.dialogs;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.tools.*;

public class BundlerInputDialog extends JDialog {
        // exit status values
    public static final int CANCEL = 0;
    public static final int OK     = 1;

    private int m_exit_status;
    private JTextField m_name_field;
    private JCheckBox m_unwraps_check_box;
    private boolean mb_propagate_config_to_root_bundler;
    final private ALFINode m_bundler_root_container;
    final private BundlerProxy m_bundler;
    final private EditorFrame m_editor;

    public BundlerInputDialog(Frame _owner_frame, BundlerProxy _sink_bundler,
                                     ALFINode _container, boolean _b_is_bundle,
                                     String _name, boolean _b_unwraps) {
        super(_owner_frame, "Input Name Specification Dialog");

        m_bundler_root_container = _sink_bundler.getDirectContainerNode();
        m_bundler = _sink_bundler;
        m_editor = (EditorFrame) _owner_frame;

        setVisible(false);
        setModal(true);

        Container content_pane = getContentPane();
        content_pane.setLayout(new BoxLayout(content_pane, BoxLayout.Y_AXIS));

            // name field
        {
            JPanel field_panel = new JPanel();
            field_panel.setLayout(new BoxLayout(field_panel, BoxLayout.Y_AXIS));
            field_panel.setBorder(BorderFactory.createTitledBorder(
                                  BorderFactory.createLoweredBevelBorder(),
                                                        "Bundler Input Name"));
            m_name_field = new JTextField();
            if ((_name != null) && (_name.length() > 0)) {
                m_name_field.setText(_name);
                m_name_field.setSelectionStart(0);
                m_name_field.setSelectionEnd(_name.length());
            }
            field_panel.add(m_name_field);
            content_pane.add(field_panel);
        }

            // checkbox area
        {
            JPanel inner_panel = new JPanel();
            inner_panel.setLayout(new BoxLayout(inner_panel, BoxLayout.Y_AXIS));
            inner_panel.setBorder(BorderFactory.createTitledBorder(
                         BorderFactory.createLoweredBevelBorder(), "Options"));
            m_unwraps_check_box =
                     new JCheckBox("Unwrap input bundle content", false);
            m_unwraps_check_box.setEnabled(_b_is_bundle);
            m_unwraps_check_box.setSelected(_b_unwraps);
            inner_panel.add(m_unwraps_check_box);

            if (_container != m_bundler_root_container) {
                String s = "Propogate input name to root bundler container [" +
                                                m_bundler_root_container + "]";
                final JCheckBox propagate_checkbox = new JCheckBox(s, false);
                mb_propagate_config_to_root_bundler =
                                               propagate_checkbox.isSelected();
                propagate_checkbox.addActionListener(
                    new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            mb_propagate_config_to_root_bundler =
                                               propagate_checkbox.isSelected();
                            if (mb_propagate_config_to_root_bundler) {
                                if (! warnRePropogation()) {
                                    m_exit_status = CANCEL;
                                    setVisible(false);
                                }
                            }
                        }
                    });
                inner_panel.add(propagate_checkbox);
            }
            JPanel checkbox_panel = new JPanel();
            checkbox_panel.setLayout(new BoxLayout(checkbox_panel,
                                                            BoxLayout.X_AXIS));
            checkbox_panel.add(inner_panel);
            content_pane.add(checkbox_panel);
        }

            // OK and cancel buttons
        {
            JPanel button_panel = new JPanel();
            button_panel.setLayout(new BoxLayout(button_panel,
                                   BoxLayout.X_AXIS));
            button_panel.setBorder(BorderFactory.createEtchedBorder());
            button_panel.add(Box.createHorizontalGlue());
            JButton ok_button = new JButton("OK");
            ok_button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        String name = m_name_field.getText();
                        if (BundlerProxy.inputChannelNameIsValid(name)) {
                            m_exit_status = OK;
                            setVisible(false);
                        }
                        else {
                            Alfi.warn("Invalid bundler input name.", false);
                        }
                    }
                });
            button_panel.add(ok_button);
            getRootPane().setDefaultButton(ok_button);

            JButton cancel_button = new JButton("Cancel");
            cancel_button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        m_exit_status = CANCEL;
                        setVisible(false);
                    }
                });
            button_panel.add(Box.createHorizontalStrut(10));
            button_panel.add(cancel_button);

            button_panel.add(Box.createHorizontalGlue());
            content_pane.add(button_panel);
        }

        m_exit_status = CANCEL;
    }

    public int getExitStatus() { return m_exit_status; }

    public String getInputName() { return m_name_field.getText(); }

    public boolean getUnwrapsInput() {
        return m_unwraps_check_box.getModel().isSelected();
    }

    public boolean propagateConfigToRootBaseBundler() {
        return mb_propagate_config_to_root_bundler;
    }

        /** Returns false if user selects Cancel. */
    private boolean warnRePropogation() {
        String s = "This bundler input name will be propogated to this " +
            "bundler [" + m_bundler + "] in the bundler's local container " +
            "node [" + m_bundler_root_container + "].";
        s = TextFormatter.formatMessage(s, "  ", 75);
        String message = "Note:\n" + s;

        ALFINode[] affected_boxes = getAffectedBoxes();
        if (affected_boxes.length > 0) {
            s = "The following box files will be flagged to be resaved in " +
                "order to incorporate this change:";
            s = TextFormatter.formatMessage(s, "  ", 75);
            message += "\n" + s;

            for (int i = 0; i < affected_boxes.length; i++) {
                message += "\n    " +
                          affected_boxes[i].getSourceFile().getCanonicalPath();
            }
        }

        message += "\n";

        String intermediate_settings_warning =
                     m_editor.checkForIntermediateBundlerIOSettings(m_bundler);
        if (intermediate_settings_warning != null) {
            String[] parts = intermediate_settings_warning.split("\t");
            s = parts[0].trim();
            s = s.replaceAll("\n", " ");
            s = TextFormatter.formatMessage(s, "  ", 75);

            message += "\nFurther Note:\n" + s + "\n";

            for (int i = 1; i < parts.length; i++) {
                message += "    " + parts[i];
            }
            message += "\n";
        }

        String[] options = { "Continue", "Cancel" };
        String title = "Settings Propogation Warning";
        int choice = JOptionPane.showOptionDialog(this, message, title,
               JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null,
               options, options[0]);
        return (choice == 0);
    }

    public ALFINode[] getAffectedBoxes() {
        ALFINode[] derived_nodes=m_bundler_root_container.derivedNodes_getAll();
        HashMap box_map = new HashMap();
        box_map.put(m_bundler_root_container, null);
        for (int i = 0; i < derived_nodes.length; i++) {
            ALFINode box = (derived_nodes[i].isRootNode()) ?
                   derived_nodes[i] : derived_nodes[i].containerNode_getRoot();
            box_map.put(box, null);
        }

        ArrayList box_list = new ArrayList(box_map.keySet());
        Collections.sort(box_list);
        ALFINode[] boxes = new ALFINode[box_list.size()];
        box_list.toArray(boxes);

        return boxes;
    }
}
