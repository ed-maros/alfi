package edu.caltech.ligo.alfi.dialogs;

/**
  * <pre>
  * The class which contains the parameter or input port
  * modifications, its corresponding value and comment.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class ValueModification extends Object {

    private String m_header_name;
    private String m_value;
    private String m_comment;        

    public ValueModification (String _header_name, String _value, 
                              String _comment) {
        m_header_name = _header_name;
        m_value = _value;
        m_comment = _comment;
    }
        
    public String getHeaderName () {return m_header_name;}
    public String getValue () {return m_value;}
    public String getComment () {return m_comment;}
        
}
