package edu.caltech.ligo.alfi.dialogs;

import java.util.StringTokenizer;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.*;
import javax.swing.plaf.basic.BasicGraphicsUtils;

import edu.caltech.ligo.alfi.common.GuiConstants;

/*
 * This interface defines methods for the different types of renderers
 * in the ParameterTable.
 */

/**    
  * <pre>
  * This class is the renderer for the table data
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class SimpleCellRenderer extends JLabel
                                implements TableCellRenderer, KeyListener  {

    protected boolean m_has_focus = false;
    protected boolean m_is_selected = false;
    private boolean m_is_default = true;
    private Border m_unselected_border = null;
    private Border m_selected_border = null;
    private int m_current_column = -1;
    private int m_current_row = -1;

    public SimpleCellRenderer (){

        m_selected_border = BorderFactory.createMatteBorder(2,5,2,5,
                            GuiConstants.SELECTED_BACKGROUND);
        setOpaque(true);
        setVerticalAlignment(JLabel.TOP);
        setHorizontalAlignment(JLabel.CENTER);
        setCursor(new Cursor(Cursor.TEXT_CURSOR));
    }
    
    public Color getBackdrop() {
        if (m_is_default) {
            return GuiConstants.DEFAULT_VALUE; 
        } else {
            return GuiConstants.MODIFIED_VALUE;
        }
    }
    
    public Component getTableCellRendererComponent (JTable _parent, 
        Object _value, boolean _is_selected, boolean _has_focus, 
        int _row, int _column) {
    
        m_current_column = _column;
        m_current_row = _row;

        Color foreground, background;
        ParameterTable table = (ParameterTable) _parent;

        m_has_focus = _has_focus;
        m_is_selected = _is_selected;
        m_is_default = table.isDefaultValue(_row, _column);

        String cell_value;
        if (_value != null) {
            cell_value = _value.toString();
        }
        else {
            cell_value = "";    
        }

        setText(cell_value);

        boolean is_multiline = false;


        if ((_column == ParameterTableModel.LOCAL_VALUE_INDEX ||
             _column == ParameterTableModel.DEFAULT_VALUE_INDEX) &&
             (cell_value.length() > 0)){
            // For now, we only care about the LOCAL_VALUE for multilines
            is_multiline = ParameterTableModel.isMultilineText(cell_value);
                             
            if (is_multiline) {
                setToolTipText(createHTMLTip(cell_value));
            } else {
                setToolTipText(cell_value);
            }
        }
        else {
            setToolTipText(null);
        }

        if (m_is_selected)
        {
            if (is_multiline) {
                background = Color.green;
            } else {
                background = GuiConstants.SELECTED_BACKGROUND;
            }

            if (m_is_default) {
                foreground = GuiConstants.SELECTED_DEFAULT_FOREGROUND;
            } else {
                foreground = GuiConstants.SELECTED_MODIFIED_FOREGROUND;
            }
            setBorder(m_selected_border);
        
        } else {
            if (_column == ParameterTableModel.DEFAULT_VALUE_INDEX ||
                _column == ParameterTableModel.DEFAULT_COMMENT_INDEX) {

                if (is_multiline) {
                    background = Color.green;
                } else if (table.hasDefaultValue(_row)) {
                    background = GuiConstants.DEFAULT_COMMENT_VALUE;
                } else {
                    background = GuiConstants.DEFAULT_VALUE;
                }

            } else {

                background = getBackdrop();
                if (is_multiline && 
                    _column == ParameterTableModel.LOCAL_VALUE_INDEX) {
                    background = Color.green;
                }
            }

            foreground = Color.black;
            setBorder(null);
        }

        setBackground(background);
        setForeground(foreground);

        return this;
    }

    private String createHTMLTip (String _text) {
        final String HTML_HEADER = "<html><font color=green>";
        final String HTML_TRAILER = "</font></html>";
        final String HTML_LINE_BREAK = "<br>";
        StringBuffer html_line = new StringBuffer(HTML_HEADER);      

        StringTokenizer tokenizer = new StringTokenizer(_text, 
            ParameterTableModel.LINE_DELIMETERS);
        while (tokenizer.hasMoreTokens()) {
            html_line.append(tokenizer.nextToken());
            html_line.append(HTML_LINE_BREAK);
        }
        html_line.append(HTML_TRAILER);
        return html_line.toString();
    }
    
    public void setCellValue (String _value) {  
        setText(_value);
    }

    /** Handle the key typed event from the label. */
    public void keyTyped (KeyEvent _event) { }

    /** Handle the key pressed event from the label. */
    public void keyPressed (KeyEvent _event) {

        if (_event.isControlDown() && _event.getKeyCode() == KeyEvent.VK_D) {
            ParameterTable table = (ParameterTable) _event.getSource();
            table.setDefaultValue();
        } 
    }

    /** Handle the key released event from the label. */
    public void keyReleased (KeyEvent _event) { }

}
  
