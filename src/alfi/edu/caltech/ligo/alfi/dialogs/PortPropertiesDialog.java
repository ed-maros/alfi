package edu.caltech.ligo.alfi.dialogs;

import edu.caltech.ligo.alfi.bookkeeper.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import com.nwoods.jgo.*;

public class PortPropertiesDialog extends JDialog {
        // exit status values
    public static final int CANCEL = 0;
    public static final int OK     = 1;

    private int m_exit_status;

        // swing components used in the dialog
    private JTextField m_name_field;
    private JRadioButton[] m_port_io_type_buttons;
    private JComboBox m_port_data_type_combo_box;
    private JRadioButton[] m_internal_orientation_buttons;
    private JRadioButton[] m_external_orientation_buttons;

    public PortPropertiesDialog(Frame _owner_frame, PortProxy _port_proxy) {
        super(_owner_frame, ((_port_proxy != null)? "Modify Port": "Add Port"));
        setVisible(false);
        setModal(true);

            // port name field
        Box name_box = Box.createHorizontalBox();
        name_box.add(new JLabel("Name: "));
        m_name_field = new JTextField();
        name_box.add(m_name_field);

            // i/o type selection
        JPanel io_type_panel = new JPanel();
        io_type_panel.setLayout(new BoxLayout(io_type_panel, BoxLayout.Y_AXIS));
        io_type_panel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLoweredBevelBorder(), "Port I/O Type"));
        final int[] io_types = { PortProxy.IO_TYPE_INPUT,
                                 PortProxy.IO_TYPE_OUTPUT };
        final int[] default_edge_ids = { PortProxy.EDGE_WEST,
                                         PortProxy.EDGE_EAST };
        m_port_io_type_buttons = new JRadioButton[io_types.length];
        ButtonGroup io_button_group = new ButtonGroup();
        for (int i = 0; i < m_port_io_type_buttons.length; i++) {
            int io_type = io_types[i];
            String label = PortProxy.IO_TYPE_BY_STRING_ARRAY[io_type];
            m_port_io_type_buttons[i] = new JRadioButton(label);
            io_button_group.add(m_port_io_type_buttons[i]);
            io_type_panel.add(m_port_io_type_buttons[i]);

                // selection of these i/o buttons should set edge defaults
            m_port_io_type_buttons[i].addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JRadioButton source = (JRadioButton) e.getSource();
                        if (source.isSelected()) {
                            for (int j = 0; j < io_types.length; j++) {
                                if (source == m_port_io_type_buttons[j]) {
                                    int edge = default_edge_ids[j];
                                    m_internal_orientation_buttons[edge].
                                                           setSelected(true);
                                    m_external_orientation_buttons[edge].
                                                           setSelected(true);
                                }
                            }
                        }
                    }
                });
        }

            // data type selection
        JPanel data_type_selection_panel = new JPanel();
        data_type_selection_panel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLoweredBevelBorder(), "Port Data Type"));
        m_port_data_type_combo_box =
                new JComboBox(GenericPortProxy.DATA_TYPE_BY_STRING_ARRAY);
        data_type_selection_panel.add(m_port_data_type_combo_box);

            // internal orientation selection
        JPanel internal_orientation_selection_panel = new JPanel();
        internal_orientation_selection_panel.setLayout(new BoxLayout(
                internal_orientation_selection_panel, BoxLayout.X_AXIS));
        internal_orientation_selection_panel.setBorder(BorderFactory.
                createTitledBorder(BorderFactory.createLoweredBevelBorder(),
                        "Internal View Orientation"));
        m_internal_orientation_buttons =
                new JRadioButton[PortProxy.EDGE_BY_STRING_ARRAY.length];
        ButtonGroup internal_orientation_button_group = new ButtonGroup();
        for (int i = 0; i < m_internal_orientation_buttons.length; i++) {
            String label = PortProxy.EDGE_BY_STRING_ARRAY[i];
            m_internal_orientation_buttons[i] = new JRadioButton(label);
            internal_orientation_button_group.add(
                    m_internal_orientation_buttons[i]);
            internal_orientation_selection_panel.add(
                    m_internal_orientation_buttons[i]);
        }

            // external orientation selection
        JPanel external_orientation_selection_panel = new JPanel();
        external_orientation_selection_panel.setLayout(new BoxLayout(
                external_orientation_selection_panel, BoxLayout.X_AXIS));
        external_orientation_selection_panel.setBorder(BorderFactory.
                createTitledBorder(BorderFactory.createLoweredBevelBorder(),
                        "External View Orientation"));
        m_external_orientation_buttons =
                new JRadioButton[PortProxy.EDGE_BY_STRING_ARRAY.length];
        ButtonGroup external_orientation_button_group = new ButtonGroup();
        for (int i = 0; i < m_external_orientation_buttons.length; i++) {
            String label = PortProxy.EDGE_BY_STRING_ARRAY[i];
            m_external_orientation_buttons[i] = new JRadioButton(label);
            external_orientation_button_group.add(
                    m_external_orientation_buttons[i]);
            external_orientation_selection_panel.add(
                    m_external_orientation_buttons[i]);
        }

            // if there is a port to be modified, initialize values
        if (_port_proxy != null) {
            m_name_field.setText(_port_proxy.getName());

            for (int i = 0; i < m_port_io_type_buttons.length; i++) {
                if (io_types[i] == _port_proxy.getIOType()) {
                    m_port_io_type_buttons[i].setSelected(true);
                }
            }

            int data_type = _port_proxy.getDataType();
            m_port_data_type_combo_box.setSelectedIndex(data_type);

            int[] position = _port_proxy.getPosition();
            m_internal_orientation_buttons[position[0]].setSelected(true);
            m_external_orientation_buttons[position[2]].setSelected(true);
        }
        else {
                // set defaults
            m_port_io_type_buttons[0].setSelected(true);
            m_port_data_type_combo_box.setSelectedIndex(
                                         GenericPortProxy.DATA_TYPE_REAL);
            int index = PortProxy.EDGE_WEST;
            m_internal_orientation_buttons[index].setSelected(true);
            m_external_orientation_buttons[index].setSelected(true);
        }

            // OK and cancel buttons
        JPanel button_panel = new JPanel();
        button_panel.setLayout(new BoxLayout(button_panel, BoxLayout.X_AXIS));
        button_panel.setBorder(BorderFactory.createEtchedBorder());
        JButton ok_button = new JButton("OK");
        ok_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    m_exit_status = OK;
                }
            });
        button_panel.add(ok_button);
        getRootPane().setDefaultButton(ok_button);

        JButton cancel_button = new JButton("Cancel");
        cancel_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    m_exit_status = CANCEL;
                }
            });
        button_panel.add(cancel_button);

        Box main_box =  Box.createVerticalBox();
        main_box.add(Box.createVerticalStrut(5));
        main_box.add(name_box);
        main_box.add(Box.createVerticalStrut(10));
        main_box.add(io_type_panel);
        main_box.add(Box.createVerticalStrut(10));
        main_box.add(data_type_selection_panel);
        main_box.add(Box.createVerticalStrut(10));
        main_box.add(internal_orientation_selection_panel);
        main_box.add(Box.createVerticalStrut(10));
        main_box.add(external_orientation_selection_panel);
        main_box.add(Box.createVerticalStrut(10));
        main_box.add(button_panel);
        main_box.add(Box.createVerticalStrut(5));

        Container content_pane = getContentPane();
        content_pane.add(main_box);

        m_exit_status = CANCEL;
    }

    public int getExitStatus() { return m_exit_status; }

    public String getPortName() { return m_name_field.getText(); }

    public int getPortIOType() {
        String input_str =
                PortProxy.IO_TYPE_BY_STRING_ARRAY[PortProxy.IO_TYPE_INPUT];
        String output_str =
                PortProxy.IO_TYPE_BY_STRING_ARRAY[PortProxy.IO_TYPE_OUTPUT];

        for (int i = 0; i < m_port_io_type_buttons.length; i++) {
            if (m_port_io_type_buttons[i].isSelected()) {
                String button_label = m_port_io_type_buttons[i].getText();
                if (button_label.equals(input_str)) {
                    return PortProxy.IO_TYPE_INPUT;
                }
                else if (button_label.equals(output_str)) {
                    return PortProxy.IO_TYPE_OUTPUT;
                }
                break;
            }
        }
        return PortProxy.IO_TYPE_INVALID;
    }

    public int getPortDataType() {
        return m_port_data_type_combo_box.getSelectedIndex();
    }

    public int getPortInternalOrientation() {
        for (int i = 0; i < m_internal_orientation_buttons.length; i++) {
            if (m_internal_orientation_buttons[i].isSelected()) { return i; }
        }
        return PortProxy.EDGE_INVALID;
    }

    public int getPortExternalOrientation() {
        for (int i = 0; i < m_external_orientation_buttons.length; i++) {
            if (m_external_orientation_buttons[i].isSelected()) { return i; }
        }
        return PortProxy.EDGE_INVALID;
    }
}
