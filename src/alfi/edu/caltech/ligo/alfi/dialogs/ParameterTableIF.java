package edu.caltech.ligo.alfi.dialogs;

/**    
  * <pre>
  * The interface for constants and actions for ParameterTable
  *
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.dialogs.ParameterTableIF
  */
public interface ParameterTableIF {

    /**
      * Method to update a cell
      * @see edu.caltech.ligo.alfi.resource.AlfiResources
      */
     
    public void updateCell (String new_text, int row);
}     

