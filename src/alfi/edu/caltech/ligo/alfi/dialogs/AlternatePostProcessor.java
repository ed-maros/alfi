package edu.caltech.ligo.alfi.dialogs;

import java.awt.KeyEventPostProcessor;
import java.awt.KeyboardFocusManager;
import javax.swing.JComponent;
import java.awt.event.KeyEvent;


/**
  * <pre>
  * The class sends the KeyEvents to the appropriate JDialog
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class AlternatePostProcessor implements KeyEventPostProcessor{

    public boolean postProcessKeyEvent(KeyEvent e){
        
        System.out.println("AlternatePostProcessor");
        return true;
    }

}
