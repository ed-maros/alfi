package edu.caltech.ligo.alfi;

import java.util.*;
import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.filechooser.*;

import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.resource.AlfiResources;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.editor.EditorFrame;
import edu.caltech.ligo.alfi.editor.AlfiClipboard;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;

import com.nwoods.jgo.*;

/** 
  * Starting class for the ALFI application.
  * 
  * @author Melody Araya
  * @version %I%, %G%
  *
  * @see edu.caltech.ligo.alfi.summary.AlfiMainFrame
  */

public class Alfi { 
    public final static String ms_alfi_version = "6.6.2";

    UIResourceBundle m_resources;

    final static ALFINodeCache ms_the_node_cache = new ALFINodeCache();
    final static ProxyListenerRegistrationTool ms_proxy_listener_registrar =
                                           new ProxyListenerRegistrationTool();
    static PrimitivesPopupMenu ms_primitives_menu = null; 
    static ToolbarFrame ms_primitives_toolbar = null; 
    static AlfiMainFrame ms_main_window;
    static AlfiPopupMenuListener ms_popup_menu_listener = 
                                                   new AlfiPopupMenuListener();
    public static Messenger ms_messenger = null;

    public static File UNIVERSAL_PREFERENCES_FILE = null;
    public static File WORKING_DIRECTORY = null;
    public static File LOCAL_UTILITY_DIRECTORY = null;
    public static File CLIPBOARD_FILE = null;
    public static File LOCAL_PREFERENCES_FILE = null;

    public static HashMap PREFERENCES = null;
    public static String E2E_PATH = null;
        // only used when writing preference file
    public static boolean SAVE_WORKING_DIRECTORY_IN_E2E_PATH = false;

    public static String WINDOW_MANAGER = null;
    public final static Point WINDOW_MANAGER_OFFSET = new Point(0, 0);

    public static String DEFAULT_BROWSER = null;

        // this is also important for widget size inquiries before edit windows
        // are constructed
    final static JFrame ms_splash_frame = new JFrame("Alfi");
    final static JGoView ms_splash_view = new JGoView();


    StartupManager m_startup_manager = null;

     /**
     * Construct an instance of the application
     */
    public Alfi () {
            // error messages need to appear in dialogs warnings as well
        System.setErr(new PrintStreamDialogWriter(System.err));

        System.err.println("Alfi version " + ms_alfi_version + " starting...");

            // initializes many things, like WORKING_DIRECTORY, etc.
        m_startup_manager = new StartupManager(ms_splash_frame);
        if (! m_startup_manager.initializeAlfi()) {
            System.exit(1);
        }

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception ex){
            System.err.println ("Could not find the system Look and Feel");
            System.exit(1);
        }

        m_resources = (UIResourceBundle) ResourceBundle.getBundle(
                            AlfiResources.class.getName(), Locale.getDefault());

            // load in all primitive base nodes
        ms_proxy_listener_registrar.interrupt(this);

        FileFormatProblemManager ffpm = new FileFormatProblemManager();
        PrimitiveLoader pl = new PrimitiveLoader(E2E_PATH, ffpm);
        UDPLoader udp_loader = new UDPLoader(E2E_PATH, ffpm);
        ffpm.resolve();

            // load primitive templates
        new PrimitiveTemplateLoader(E2E_PATH);

        ms_proxy_listener_registrar.resumeProcessingLoop(this);


            // Invokes the parent frame used for primary interaction with user.
        try {
            ms_main_window = new AlfiMainFrame(m_resources);

            ms_main_window.pack();
            ms_main_window.run();

            String position = (String) PREFERENCES.get("MAIN_WINDOW_RECT");
            if (! ms_main_window.restorePosition(position,
                                                      WINDOW_MANAGER_OFFSET)) {
                ms_main_window.resetSize();
            }
            ms_main_window.setVisible(true);

            String show_toolbar_string =
                            (String) PREFERENCES.get("SHOW_PRIMITIVE_TOOLBAR");
            if ((show_toolbar_string != null) &&
                                          show_toolbar_string.equals("true")) {
                ms_main_window.showPrimitiveToolbar(true);
            }

            initializeClipboard();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    //**************************************************************************
    //********** Instance Methods **********************************************

    private void initializeClipboard() {
        try {
            boolean b_persistent_clipboard = CLIPBOARD_FILE.exists();
            ms_main_window.setPersistentClipboard(b_persistent_clipboard);

            if (b_persistent_clipboard) {
                removeClipboardFileIfIncludesAreNotValid();
            }

            ALFIFile clipboard_file =
                         new ALFIFile(CLIPBOARD_FILE.getCanonicalPath(), null);
            ALFINode clipboard_node = clipboard_file.getRootNode();
            EditorFrame.setClipboard(new AlfiClipboard(clipboard_node));
        }
        catch(Exception e) { 
            System.err.println(e); 
            
        }
    }

        /** If there are include lines in the clipboard to files which do not
          * exist, just clear the clipboard.
          */
    private void removeClipboardFileIfIncludesAreNotValid() {
        String[] lines = MiscTools.readSmallTextFile(CLIPBOARD_FILE);
        if ((lines == null) || (lines.length == 0) || (lines[0] == null)) {
            if (CLIPBOARD_FILE.exists()) {
                CLIPBOARD_FILE.delete();
            }
            return;
        }

            // check for bad include lines
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].matches(".*#include.*")) {
                String[] parts = lines[i].split("\\s+");
                for (int j = 0; j < parts.length; j++) {
                    if (parts[j].equals("#include")) {
                        String path = parts[j + 1];
                        if (path.startsWith(File.separator)) {
                            File file = new File(path);
                            if (! file.exists()) {
                                CLIPBOARD_FILE.delete();
                                return;
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    //**************************************************************************
    //********** Static Methods ************************************************

    /**
     * Main entry point for the ALFI 
     *
     * @param _args command line arguments.
     */
    public static void main (String _args[]) {
        doSplash();

        final Alfi app = new Alfi();

            // args passed in via command_line file, _args via command line
        String args = (String) PREFERENCES.get("args");
        String[] arg_array = (args != null) ? args.split("\\s+") : _args;
        if (arg_array.length > 0) { parseArguments(app, arg_array); }
        else if (ms_main_window.getCheckBoxItemState(
                                                  ActionsIF.RESTORE_WINDOWS)) {
                // restore windows from last session
            app.m_startup_manager.restoreWindows();
        }

        ms_splash_frame.setVisible(false);

        String version_last_used =
              (String) app.PREFERENCES.get("UNIVERSAL_PREFERENCES_WRITTEN_BY");
        if (version_last_used == null) { version_last_used = "unknown"; }
        if (! version_last_used.equals(app.ms_alfi_version)) {
            ms_main_window.helpUpdates(version_last_used);
        }
    }

    private static void doSplash() {
        Color background_color = Color.red;
        Color text_color = Color.white;
        
        ms_splash_view.setHorizontalScrollBar(null);
        ms_splash_view.setVerticalScrollBar(null);
        JScrollPane scroll_pane = new JScrollPane(ms_splash_view,
                    ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        Container content_pane = ms_splash_frame.getContentPane();
        content_pane.add(scroll_pane);

        Dimension screen_size = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frame_size = new Dimension(200, 200);
        ms_splash_frame.setBounds(
            (screen_size.width - frame_size.width) / 2,
            (screen_size.height - frame_size.height) / 2,
            frame_size.width, frame_size.height);

        ms_splash_view.getDocument().setPaperColor(background_color);
        ms_splash_frame.setVisible(true);

        JGoText title = new JGoText("Alfi");
        title.setTextColor(text_color);
        title.setBkColor(background_color);
        title.setLocation((content_pane.getSize().width / 2) -
                                         (title.getSize().width / 2),
                          (content_pane.getSize().height / 3) -
                                         (title.getSize().height / 2));
        title.setFontSize(24);
        ms_splash_view.getDocument().addObjectAtHead(title);

        JGoText version = new JGoText("Version " + ms_alfi_version);
        version.setTextColor(text_color);
        version.setBkColor(background_color);
        version.setLocation((content_pane.getSize().width / 2) -
                                              (version.getSize().width / 2),
                          (2 * content_pane.getSize().height / 3) -
                                              (version.getSize().height / 2));
        ms_splash_view.getDocument().addObjectAtHead(version);
    }

    static void parseArguments(final Alfi _app, String _args[]) {
        ArrayList files_to_open_list = new ArrayList();
        for (int i = 0; i < _args.length; i++) {
            if (_args[i].endsWith(ALFIFile.BOX_NODE_FILE_SUFFIX)) {
                File file = new File(_args[i]);
                if (file.exists()) {
                    files_to_open_list.add(file);
                }
                else {
                    String warning = "File " + _args[i] + " not found. " +
                        "Creating new box file " + _args[i] + " .";
                    warn(warning, false);
                }
            }
        }

        File[] files_to_open = new File[files_to_open_list.size()];
        files_to_open_list.toArray(files_to_open);
        ms_main_window.show(files_to_open, null, null);
    }

    public static ALFINodeCache getTheNodeCache() { return ms_the_node_cache; }

    public static Messenger getMessenger() { return ms_messenger; }

    public static ProxyListenerRegistrationTool getTheProxyListenerRegistrar() {
        return ms_proxy_listener_registrar;
    }

    public static JPopupMenu getPrimitivesMenu()  {
        if (ms_primitives_menu != null) {
            ms_primitives_menu.cleanupMenu();
        }
        ms_primitives_menu = new PrimitivesPopupMenu(
                    ms_the_node_cache.getRootPrimitiveNodes_sorted(),
                    ms_the_node_cache.getPrimitiveTemplates(), ms_main_window);

        return (JPopupMenu) ms_primitives_menu;
    }
    public static ToolbarFrame getPrimitivesToolbarFrame()  {
        if (ms_primitives_toolbar == null) {
            ms_primitives_toolbar = new ToolbarFrame(
                              ms_the_node_cache.getRootPrimitiveNodes_sorted(),
                              ms_main_window);
        }

        return ms_primitives_toolbar;
    }

    public static PrimitivesToolbar getPrimitivesToolbar()  {
        return ms_primitives_toolbar.getToolbar();
    }

    public static Point clearPrimitivesToolbar() {
        Point location = null;
        if (ms_primitives_toolbar != null) {
            location = new Point(ms_primitives_toolbar.getLocation());
            ms_primitives_toolbar.setVisible(false);
            ms_primitives_toolbar.removeAll();
            ms_primitives_toolbar = null;
        }
        return location;
    }

    public static JGoView getSplashView() { return ms_splash_view; }

    public static AlfiPopupMenuListener getPopupListener()  {
        return ms_popup_menu_listener;
    }

    public static boolean showInheritedTips() {
        return ms_main_window.addInheritedTips();
    }

        /** See warn(null, false). */
    public static void warn() { Alfi.warn(null, true); }

        /** See warn(null, _b_send_to_stderr). */
    public static void warn(boolean _b_send_to_stderr) {
        warn(null, _b_send_to_stderr);
    }

        /** See warn(_message, false). */
    public static void warn(String _message) { Alfi.warn(_message, true); }

        /**
         * This is a method that throws a very generic exception, for use to
         * keep code less cluttered.  All it does is throw and catch an
         * Exception object and then prints the stack trace to stderr.  All it
         * is is a warning that something that shouldn't have happened
         * did happen, and where.  If _b_send_to_stderr is true, the message
         * along with a stack trace is printed to stderr.
         */
    public static void warn(String _message, boolean _b_send_to_stderr) {
        if (_message == null) {
            _message = "Generic Alfi consistency check failed at";
        }

        String message = TextFormatter.formatMessage("WARNING: " + _message);

        if (_b_send_to_stderr) {
            try { throw new Exception(); }
            catch(Exception e) {
                System.out.println("\n" + message + ":\n");
                e.printStackTrace(System.out);
                String post_message =
                    "The java.lang.Exception above was generated as part of " +
                    "the warning message and can otherwise be ignored.  " +
                    "Please mail this warning message to " +
                    "hiro@ligo.caltech.edu if you believe the error is in " +
                    "the Alfi program (as opposed to in a box file.)  This " +
                    "is Alfi version " + ms_alfi_version +".";
                System.out.println("\n" +
                    TextFormatter.formatMessage(post_message) + "\n");
            }

            message+= "\n\n(Message repeated at stderr for ease in reporting.)";
        }

        String title = "ALFI: Warning";
        JOptionPane.showMessageDialog(null, message, title,
                                      JOptionPane.WARNING_MESSAGE);
    }

    public static String getE2EPath() { return new String(E2E_PATH); }

    public static void setE2EPath(String _path) {
        E2E_PATH = new String(_path);
    }

    public static String getDefaultBrowser() { 
        if ((DEFAULT_BROWSER == null) || (DEFAULT_BROWSER.length() <= 0)) {
            return null;
        }
        else {
            return new String(DEFAULT_BROWSER); 
        }
    }

    public static void setDefaultBrowser(String _browser_path) {
        DEFAULT_BROWSER = new String(_browser_path);
    }

    public static AlfiMainFrame getMainWindow() { return ms_main_window; }

    public static String getUpdateInfo(String _version_last_used) {
        LinkedHashMap news_map = createNewsMap();
        StringBuffer sb = new StringBuffer();

        if (_version_last_used != null) {
            sb.append("Current version: " + ms_alfi_version + "\n" +
                   "Version last used by you: " + _version_last_used + "\n\n");
        }

        for (Iterator i = news_map.keySet().iterator(); i.hasNext(); ) {
            String version = (String) i.next();
            String value = (String) news_map.get(version);
            String[] lines = value.split("\n");

            sb.append("Version " + version + ":\n");
            for (int j = 0; j < lines.length; j++) {
                sb.append("    " + lines[j] + "\n");
            }
            sb.append("\n\n");
        }
        return sb.toString();
    }

    private static LinkedHashMap createNewsMap() {
        LinkedHashMap version_news_map = new LinkedHashMap();

version_news_map.put("6.6.2",
"New Feature:  User Defined Primitives can be created through the File menu\n" +
"    in the main window.  After the UDP is created, the user will be able\n" +
"    create instances of it through the primitive popup menu.\n\n" +

"New Feature:  FUNC_X and UDP primitives are able to display instance\n" +
"    settings (simple view) in the Primitive Setting Dialog  when the \n" +
"    settings are retrieved from an external file (through @@INCLUDE)."
);

version_news_map.put("6.6.1",
"Admin changes to support files only."
);

version_news_map.put("6.6.0",
"Java Upgrade: Removed deprecated java methods.  Updated to version 1.5."
);

version_news_map.put("6.5.1",
"New Feature: Text Comment and Highlight widgets are now available.\n" +
"    This widgets are used for labeling and coloring the editor windows.\n\n" +

"New Feature: Member nodes, junctions, bundlers, text comments, and\n" +
"    highlight widgets are Groupable.  This means that once a set of widgets\n"+
"    are grouped, they are selected as one.  Moving individual widgets in\n" +
"    a group is still possible by dragging the selected widget using the\n" +
"    middle mouse button, or by pressing the Shift key while dragging the\n" +
"    widget with the left mouse button." 
);

version_news_map.put("6.5.0",
"New Feature: Template generated primitives are now available.  Such\n" +
"    primitives allow for the dynamic setting of the number of input\n" +
"    and output sets of ports.  For example, see the template \"<FUNC>\"\n" +
"    in the \"FUNC\" group of primitives.\n\n" +

"Reimplemented Feature: Dog-leg (L shaped) connections now react correctly\n" +
"    in all cases of connection shape adjustments, and the minimum set\n" +
"    of information to store and restore them is written to box file."
);

version_news_map.put("6.4.1",
"Bug Fix: Command line preferences now work correctly when Alfi is started\n" +
"    directly via a java command line (as opposed to the alfi_ws script.)\n" +
"    (This bug is unlikely to have affected anyone other than the\n" +
"    developers.)\n\n" +

"Feature Upgrade: When a dialog comes up warning the user that a box\n" +
"    includes a primitive which could not be found in the current E2E_PATH,\n" +
"    they now have the option to stop the loading of the box instead of\n" +
"    only being able to update the E2E_PATH to find said primitive."
);

version_news_map.put("6.4.0",
"Reimplemented Feature: Dog-leg (L shaped) connections now behave in a\n" +
"    manner when adjusted by the user.  I.e., no more bad-dog-legs..."
);

version_news_map.put("6.3.14",
"Bug Fix: Deletion of junctions with only a single connection to it no\n" +
"    longer causes an exception."
);

version_news_map.put("6.3.13",
"Bug Fix: Fixed bundler input rename bug."
);

version_news_map.put("6.3.12",
"Bug Fix: Erasing macros in the Macros Dialog now clears the node's macro\n" +
"    correctly."
);

version_news_map.put("6.3.11",
"Bug Fix: Parameter declaration setting type \"vector_integer\" now accepted."
);

version_news_map.put("6.3.10",
"Temporary safe fix for a bug occuring in manipulation of dog-leg\n" +
"    connections.  The movement of such connections is no longer pretty,\n" +
"    but is safe.  More work is being done on this problem to restore the\n" +
"    dog-leg connection movement mechanism in a safe manner."
);

version_news_map.put("6.3.9",
"Bug Fix: When saving a box file, Display settings (through Rename) are\n" +
"    saved only when it is not the default."
);

version_news_map.put("6.3.8",
"Bug Fix: The node's label (i.e. done through Rename) now follows the \n" +
"    base box's settings.\n\n" +

"Bug Fix: Symmetry operations on member nodes now move the affected ports\n" +
"    to the correct position."
);

version_news_map.put("6.3.7",
"Bug Fix: Junctions created on non-bundle input connections to bundlers\n" +
"    throwing exceptions.  This has been fixed.\n\n" +

"Bug Fix: Modeler connections were being improperly updated when member\n" +
"    node renames were occuring.  The modeler connections being written\n" +
"    to file were incorrect.  This has been fixed."
);

version_news_map.put("6.3.6",
"Performance Enhancement: File saves are now faster, due to the caching of\n" +
"    modeler-read-only connections."
);

version_news_map.put("6.3.5",
"Cross-Platform Bug Fix: Windows line terminators are causing some problems\n" +
"    on loading box files.  A first step has been done to see if this takes\n" +
"    care of the problem."
);

version_news_map.put("6.3.4",
"User Interface Enhancement: Wait cursors are displayed more consistently\n" +
"    when the user is expected to wait a short time for an operation.\n\n" +

"Bug Fix: The user is allowed to enter an alternate utility directory when\n" +
"    the application does not have write access to the working directory.\n" +
"    The clipboard, backup, and preferences files are written into the\n" +
"    directory.\n\n" +

"User Interface Enhancement: Data in the Unresolved Connection Dialog has\n" +
"    been reformatted in order for the user to be able to see actual\n" +
"    problem connections more readily."
);

version_news_map.put("6.3.3",
"Bug Fix: Reloading nodes with windows open containing bundlers sometimes\n" +
"    caused incorrect user warnings on reload.  This has been fixed."
);

version_news_map.put("6.3.2",
"Bug Fix: Connections to sink ports that could not ultimately trace back to\n" +
"    a source port were causing exceptions at save time.  This has been fixed."
);

version_news_map.put("6.3.1",
"Performance Enhancement: The new safer file saves have been speeded up\n" +
"    considerably, though further work on this may still be required.  The\n" +
"    \"easy\" fixes have now been done though."
);

version_news_map.put("6.3.0",
"Major Bug Fix for Modeler Input: A more thorough methodology for the\n" +
"    determination of Modeler-Read-Only connection data has been\n" +
"    implemented.  (This refers to the connection data written into the\n" +
"    Add_Connections and Remove_Connections sections of .box and .sim\n" +
"    files- information Alfi generates only for modeler use.)  The new\n" +
"    method slows down saves significantly, but is much more reliable in\n" +
"    producing the correct modeler info.  The coming versions of Alfi\n" +
"    should cut this save time considerably."
);

version_news_map.put("6.2.6",
"Bug Fix: When the side of a port is changed, the port is not placed outside\n"+
"    the box's edge.\n\n" +

"Bug Fix: A new box is created with a bigger window frame and displayed.\n" +
"    in the middle of the screen.\n\n" +

"Bug Fix: Fixed the problem where the external view frame is smaller\n" +
"    than the node if it's displayed with a large icon.\n\n" +

"Bug Fix Re Modeller: Local bundler I/O settings in inherited bundlers was\n" +
"    creating a problem where modeller-only connection data was not being\n" +
"    generated properly.  This has been fixed, but needs more testing..."
);

version_news_map.put("6.2.5",
"Bug Fix: Deletion of a junction on a bundler's primary output sometimes\n" +
"    confused the bundler's primary vs. secondary output state.  Fixed.\n\n" +

"Bug Fix: A secondary bug was introduced in the fixing of the recent bundler\n"+
"    channel filtering bug fixes.  This has now been remedied."
);

version_news_map.put("6.2.4",
"Bug Fix: Deletion of an inherited bundler secondary i/o connection\n" +
"    mistakenly triggered the attempted removal of a local bundler\n" +
"    setting in the inherited bundler, something which generally does not\n" +
"    exist.  This has been fixed.\n\n" +

"Bug Fix: Swap Nodes was sometimes giving exceptions due to a race condition\n"+
"    associated with background bundle content update threads.  This update\n" +
"    has been moved into the foreground.  If performance seems an issue in\n" +
"    the future, this issue will need to be revisited with a better\n" +
"    implementation of the background thread mechanism.\n\n" +

"Bug Fix: Double clicking on a box file's Parameter Setting in the \n" +
"    Info Panel displays the Link Declaration Dialog instead of the\n" +
"    box file window.\n\n" +

"Bug Fix: The Window Menu updates properly.\n\n" +

"Bug Fix: The Vista Window will not display if there are no opened \n" +
"    box file windows."
);

version_news_map.put("6.2.3",
"Bug Fix: Bundler output became confused when it asked for a channel whose\n" +
"    matched the beginning of multiple names in the bundle content.  This\n" +
"    has been fixed."
);

version_news_map.put("6.2.2",
"Functionality Improvement: Bundlers which have local IO settings now\n" +
"    appear as having a modified state.\n\n" +

"Functionality Improvement: It is now easier to see the path of the primary\n" +
"    bundle through a bundler.  And the colors of bundlers have been changed\n"+
"    to signify whether they are secondary input or output bundlers (i.e.,\n" +
"    bundlers which add channels to the primary bundle throughput or\n" +
"    bundlers which extract content from the primary bundle throughput.)"
);

version_news_map.put("6.2.1",
"Bug Fix: Fixed a problem in the implementation of default channels in\n" +
"    input bundle ports."
);

version_news_map.put("6.2.0",
"New Feature: Input bundle ports in root nodes may now have default\n" +
"    channels assigned to them by the user.  Right click on the input\n" +
"    bundle port in the root node and select \"Import Channels\".  These\n" +
"    channel names are preserved when the node is written to file."
);

version_news_map.put("6.1.1",
"Bug Fix: A middle-mouse-button-click now properly cancels a new connection\n" +
"    build."
);

version_news_map.put("6.1.0",
"New Feature: Users may now set bundler I/O channels for inherited bundlers\n" +
"    independent of the settings in the base node in which the bundler was\n" +
"    originally added.\n\n" +

"Functionality Addition: Most entries in the Settings Panel allow \n" +
"     access to their corresponding dialogs for editing (through double \n" +
"     clicking).  Input value defaults and primitive base nodes cannot be \n" +
"     accessed through the list.\n\n" +

"Functionality Improvement: The Edit dialogs from the Setting dialog are\n"+
"     now resizable."
);

version_news_map.put("6.0.14",
"Functionality Improvement: Some operations such as multiple member node\n" +
"    deletes, port renames, etc, were causing multiple warning dialogs\n" +
"    to appear, one after the other.  These are valid messages, but were\n" +
"    annoying because they all gave the user essentially the same\n" +
"    information multiple times.  These message batches have been condensed\n" +
"    into single dialogs now."
);

version_news_map.put("6.0.13",
"Bug Fix: A background bundle cache update thread was expiring while waiting\n"+
"    for user response to warning/info dialogs.  This has been remedied."
);

version_news_map.put("6.0.12",
"Functionality Addition: The Settings Panel (the RHS of the mainwindow) \n" +
"     automatically updates when a node change occurs.   Also, macros\n" +
"     are no longer parsed and displayed in a list.  This preserves all \n"+
"     macro comments."
);

version_news_map.put("6.0.11",
"New Feature: The right hand side of the main window displays information\n" +
"    such as the selected node's comments, input port defaults, macros, and\n"+
"    local parameter settings.\n\n"+

"Bug Fix: Better implementation of multiple object removes in edit window."
);

version_news_map.put("6.0.10",
"Test of New Code: New objects have been added in order to enable the\n" +
"    setting of inherited bundler I/O channels (as opposed to only allowing\n" +
"    such settings in locally added bundlers.)  But in this version, the\n" +
"    inherited bundler I/O setting has not yet been allowed.  This version\n" +
"    is a test run to be sure that the current use of bundlers still works\n" +
"    under this new implementation."
);

version_news_map.put("6.0.9",
"Bug Fix: Full-path include-directives warning feature introduced in 6.0.6\n" +
"    introduced a bug for the copy/pasting of primitives.  This has been\n" +
"    fixed."
);

version_news_map.put("6.0.8",
"Bug Fix: Confusion between local and inherited parameter settings was\n" +
"    occuring in Alfi when a user attempted to locally reset a parameter\n" +
"    which had a previous inherited setting.  This problem has been fixed."
);

version_news_map.put("6.0.7",
"Minor Change: A persistant clipboard is automatically cleared at startup if\n"+
"    full-path include-directives are found in it which do not correspond\n" +
"    to existing files.\n\n" +

"Functionality Addition: Alfi now completes exits after an exception occurs\n" +
"    in the quit method."
);

version_news_map.put("6.0.6",
"General Change: Macro editting been disallowed for all member nodes.\n" +
"    I.e., Macros may only be added to / editted in root base nodes.\n\n" +

"Library Upgrade:  ALFI release with JGo version 5.14.\n\n"+

"New Feature: Because boxes with full-path include-directives are not very\n" +
"    portable, and because such lines can be inadvertently added to a box\n" +
"    via the Copy/Paste of instance nodes from one project area to another,\n" +"    a warning now appears to the user so that they are aware that they\n" +
"    are about to create a non-portable system of box files by doing this."
);

version_news_map.put("6.0.5",
"Bug Fix: Add member node bug introduced in 6.0.4 fixed."
);

version_news_map.put("6.0.4",
"USER INTERFACE MODIFICATION: The Vista shortcut key is now CTRL-I.\n\n" +

"New Feature:  Parser errors are now dealt with in a much nicer manner for\n" +
"    the end users.  Instead of a multitude of error dialogs appearing\n" +
"    when several errors related to one problem occur, Alfi now shows all\n" +
"    of these messages in a single window, and gives the user the option to\n" +
"    stop the load, ignore further problems, and to write an error log.\n\n" +

"New Feature:  Swap Nodes.  Users may now swap positions and connections\n" +
"    between two different nodes with the same external port interfaces.\n" +
"    Select both member nodes in the edit window, and then choose Swap Nodes\n"+
"    from the member node popup menu.  The nodes will switch positions, and\n" +
"    the connections to each will be reconnected to the other node.\n" +
"    WARNING: Currently issues may arise when attempting this Swap Node\n" +
"             action when there are several bundle connections involved in\n" +
"             the swap.  This is a known issue and is being addressed."
);

version_news_map.put("6.0.3",
"New Feature:  Users may now select \"Validate Bundle Connections\" in a\n" +
"    background edit window popup menu.  This operation will post a dialog\n" +
"    listing all unresolved data channels in the node or any node\n" +
"    contained therein (to any depth.)  The dialog also allows the user to\n" +
"    write this report to file.  Furthermore, any box which is about to be\n" +
"    saved will automatically execute such a validation before being\n" +
"    written to file."
);

version_news_map.put("6.0.2",
"USER INTERFACE MODIFICATION: The Uncloak feature has been renamed to...\n" +
"     Vista.  Shortcut keys have changed: CTRL-S.\n\n"+

"Feature Upgrade: Vista has a new way of displaying the windows,\n"+
"     making better use of the space.\n\n"+

"Change: #include in the node Parameter Settings Dialog is place at the\n" +
"    bottom of the list.\n\n" +

"Bug Fix: New Bundle Ports are generated correctly via user clicks in the\n" +
"    port border of a box edit window."
);

version_news_map.put("6.0.1",
"No changes to Alfi.  Only a test of the updated release generation code."
);

version_news_map.put("6.0.0",
"This major upgrade is the culmination of many major feature additions\n" +
"    which have occurred from 5.9.0 on, including bundles/bundlers, FuncX\n" +
"    nodes, \"opaque\" boxes, box settings, etc.  These features have now\n" +
"    in use and tested and debugged enough to be considered stable.  One\n" +
"    major internal upgrade in version 6 is the upgrade of the JGo library\n" +
"    from version 4.x to 5.0.  Further features and fixes specific to this\n" +
"    release follow:\n\n" +

"New Feature:  A view containing miniature versions of all opened windows.\n"+
"    Check out: View->Uncloak or CTRL-O.\n\n" +

"Bug Fix:  The Parameter Settings Dialog no longer accepts blank entries.\n\n" +

"Bug Fix:  The closing the Parameter Settings Dialog through the window \n" +
"    button, allows the user to save the new entries, acting more like an \n"+
"    OK button. \n\n"+

"Bug Fix:  Fixed the focus problem when multiple Editor dialogs (displayed\n"+
"    from the Parameter Settings Dialog) are displayed."
);

version_news_map.put("5.12.21",
"Bug Fix: FUNCX nodes now show modified status (turn gray) correctly when\n" +
"    new settings are made in them."
);

version_news_map.put("5.12.20",
"Bug Fix: Ports added by clicking in the black border were not appearing in\n" +
"    the correct location when the node already contained inherited ports\n" +
"    on that edge.  This has been fixed.\n\n" +

"Bug Fix: External view windows now appear with the correct size.  Also,\n" +
"    switching from external to internal views (and vice-versa) now\n" +
"    preserves the location of the edit window."
);

version_news_map.put("5.12.19",
"Bug Fix: Exception sometimes occuring when decrementing a port position\n" +
"    in the external view of a node has been fixed."
);

version_news_map.put("5.12.18",
"Bug Fix: After double clicking on the top-left window manager button.\n"+
"    to quit alfi, the CANCEL button always closes the application.\n"+
"    Now the user can Cancel quitting.\n\n" +

"Bug Fix: Exception occuring when moving ports in an external node view\n" +
"    while port centering was enabled has been fixed."
);

version_news_map.put("5.12.17",
"New Feature: Node alignment option has been implemented.\n\n" +

"Feature Fix: Comments in Macros are now saved to and reloaded from file."
);

version_news_map.put("5.12.16",
"Bug Fix: Junction deletion was deleting connections attached to it instead\n" +
"    of joining the connections together.  Also the sink port of the deleted\n"+
"    downstream connection would no longer accept new connections.  These\n" +
"    symptoms were all due to the bookkeeping data being correct, but the\n" +
"    visual display not being updated correctly.  At any rate, this bug has\n" +
"    now been fixed."
);

version_news_map.put("5.12.15",
"New Feature: Alfi documentation, available from the Help Menu, is displayed\n"+
"    in a web browser.   The Preferences Dialog allows the user to \n" +
"    select a web browser other than netscape (in UNIX), mozilla (in Linux),\n"+
"    or IE (in Windows). \n\n" +

"New Feature: A Port Centering option is now available in the member node\n" +
"    popup menu for the EXTERNAL view of ROOT nodes.  When this is turned\n" +
"    on in a root node, all member instances' external ports will be\n" +
"    centered along their respective edges to the extent possible.  This\n" +
"    includes when local ports are added to such an instance (i.e., the\n" +
"    centering takes into account all ports, inherited and local.  But do\n" +
"    not confuse this with the fact that the centering option itself can be\n" +
"    turned on and off only at the root node level.\n\n" +

"Bug Fix: Deleting a junction with no source connection no longer \n" +
"    causes exception.\n\n" +

"New Feature: Alfi allows user to exit using the window manager close menu\n" +
"    or double-clicking the top-left button on the main frame.\n\n" +

"Bug Fix: Disable the Rotate, Symmetry, and Swap menus for the external \n" +
"    view of a node."
);

version_news_map.put("5.12.14",
"Bug Fix: Fixed bug introduced in 5.12.13."
);

version_news_map.put("5.12.13",
"Bug Fix: The Alfi main window and primitive toolbar size and position\n" +
"    are now restored on restarts."
);

version_news_map.put("5.12.12",
"New Feature: Deletion of multiple ports has been implemented.  In the\n" +
"    external view of a node, the user may select multiple ports by\n" +
"    clicking on them individually or \"rubberbanding\" them (via middle\n" +
"    mouse drag).  Then pressing the Delete key will delete any local ports\n" +
"    so selected.  Warning messages may occur if there are outside\n" +
"    connections to these ports on nodes derived from this node.\n\n" +

"Bug Fix: Port movement in both the internal and external view has been\n" +
"    fixed, but slightly altered  as per the mechanics listed in the Alfi\n" +
"    documentation (see Repositioning Ports in http://www.ligo.caltech.edu/\n" +
"    ~e2e/alfi5/docs/Alfi_Documentation.html#Ports).\n\n" +

"New Feature: Networks of junctions with no data source (i.e., all the\n" +
"    junctions are connected to each other and to sink ports only) may\n" +
"    now be reconnected to a data source by connecting a source to ANY\n" +
"    of the junctions.  The data flow directions between junctions will\n" +
"    auto-correct.\n\n" +

"New Feature: Bundler secondary input names may now be changed via the\n" +
"    Bundler Popup Menu option: \"Rename Bundler Secondary I/O\".\n\n" +

"Performance upgrade: Some old low-level methods which were kept in use in\n" +
"    order to double-check the results of a newer, more efficient set of\n" +
"    replacement methods have been removed.\n\n" +

"Bug Fix: Blank input settings are no longer allowed.\n\n" +

"Bug Fix: An exception was occuring when a member node which had been\n" +
"    removed from a derived container was then also attempted to be\n" +
"    deleted from said container's base node.  This has been fixed.\n\n" +

"Bug Fix: Empty AddMacro blocks are no longer written to the box file.\n\n" +

"Bug Fix: A problem with an unwritable log file which was causing an\n" +
"    exception on exiting ALFI (when started via webstart) has been fixed."
);

version_news_map.put("5.12.11",
"Bug Fix: ALFI_LOG is now written in the correct location (in the user's\n" +
"    defined working directory.)"
);

version_news_map.put("5.12.10",
"Currently Debugging: Fixing the location of where the ALFI_LOG file is\n" +
"    written.  Debugging happening in release due to need for startup\n" +
"    in a standard manner- via webstart."
);

version_news_map.put("5.12.9",
"USER INTERFACE MODIFICATION: Blank input values are now ignored in the\n" +
"    setting of a primitive's input ports.\n\n" +

"Internals Bug Fix: The new, faster method which checks for nodes'\n" +
"    modification status was not working correctly for removed local\n" +
"    Link and Parameter settings.  This has been fixed.\n\n" +

"Currently Debugging: This version will print information regarding the\n" +
"    ALFI_LOG file.  This is for debugging purposes only.  Please ignore."
);

version_news_map.put("5.12.8",
"Bug Fix: NullPointerException when ALFI is first executed has been fixed.\n\n"+

"Bug Fix: The Copy command was not copying certain node information: \n"+
"    Link Settings, Display type, and Node Description.\n" +
""
);

version_news_map.put("5.12.7",
"Bug Fix: Links (Box Settings) having target parameters inside nodes\n" +
"    which are being removed are now updated to remove such obsolete targets."
);

version_news_map.put("5.12.6",
"Bug Fix: The duplicate command used to sometimes put graphical objects out\n" +
"    of the view of the window.   Objects are now copied close to the \n" +
"    origin, if the selection is about to be placed out of view.\n\n" +

"New Feature: Link (Box Setting) Declarations can now be renamed in the\n" +
"    Box Setting dialog of a root node."
);

version_news_map.put("5.12.5",
"USER INTERFACE MODIFICATION: Changes to presentation in Node Rename Dialog."
);

version_news_map.put("5.12.4",
"USER INTERFACE MODIFICATION: Each node instance can have 3 label options.\n" +
"    It can display the node name, a node description, or an icon if it \n" +
"    has one associated to it."
);

version_news_map.put("5.12.1",
"Alfi Internals: Better implementation of a much used internal method to\n" +
"    improve overall performance."
        );

version_news_map.put("5.12.0",
"Major Upgrade: New implementation for bundle internals.  Considerable\n" +
"    speed increase in the writing of boxes containing heavy use of bundles.\n"
        );

version_news_map.put("5.11.14",
"Bug Fix: Deletion of a bundler sometimes confused the state of an upstream\n" +
"    bundler.  This has been remedied.\n"
        );

version_news_map.put("5.11.13",
"Bug Fix: External view window size changes no longer affect the internal\n" +
"    view window size."
        );

version_news_map.put("5.11.12",
"Bug Fix: Housekeeping cleanup of a few misc null pointer exceptions.\n"
        );

        version_news_map.put("5.11.11",
"Bug Fix: Bundler primary I/O indicators updating correctly after changing\n" +
"    a primary to a secondary I/O.\n"
        );

        version_news_map.put("5.11.10",
"USER INTERFACE MODIFICATION: Added an option in the dialog when renaming.\n" +
"    or deleting ports and member nodes to suppress it from being displayed\n"+
"    in the duration of the session.\n\n"+

"Feature: Implemented a cleanup utility for backup files.  A button to\n" + 
"    delete the .alfi directory (location of the backup files and the \n" +
"    preferences files) can be found in Alfi Preferences dialog.\n\n"+

"Feature: Backup files older than a year are deleted when saving a box file.\n"+
"    All backup files less than 30 days are retained.\n" +
"    All but the latest copy of files older than 30 are deleted. \n\n" + 

"Bug Fix: Enabled/Disabled popup menus when options are appropriate.\n\n" +

"Bug Fix: Trace back of bundle channels to correct genesis port now working.\n"
        );

        version_news_map.put("5.11.9",
"Bug Fix: Connections in multiple object drags now translate properly.\n"
        );

        version_news_map.put("5.11.8",
"USER INTERFACE MODIFICATION: Added \"Alfi Only Save\" to main menu." 
        );

        version_news_map.put("5.11.7",
"Alfi Internals: BundlerWidget paint() made faster.  Adjusting connections\n" +
"    attached to bundlers is nicer now.\n\n" +

"USER INTERFACE MODIFICATION: \"Alfi Only Save\" now available in editor\n" +
"    popup menu.  This is a (hopefully temporary) workaround to speed up\n" +
"    saves which do not need modeler information (specifically bundle\n" +
"    related information which is taking a very long time to collect." 
        );

        version_news_map.put("5.11.6",
"Alfi Internals: Fixed small Primitive Toolbar related bug.\n"
        );

        version_news_map.put("5.11.5",
"USER INTERFACE MODIFICATION: Primitives Toolbar now off by default.\n\n" +

"Alfi Internals: Bundle channel validation not performed automatically\n" +
"    on save.  Too slow.  To be turned back on when performance is upgraded.\n"
        );

        version_news_map.put("5.11.4",
"Alfi Internals: Fixed small connection removal bug.\n"
        );

        version_news_map.put("5.11.3",
"Alfi Parser: Fixed parser bug which was writing spurious bundle-related\n" +
"    connections in the Modeler's read-only connection section in box files.\n"
        );

        version_news_map.put("5.11.2",
"Alfi Internals: Unresolvable output data channels from bundlers have now\n" +
"    been fully implemented.\n\n" +

"Alfi Internals: Downstream connection state changes are now better\n" +
"    reflected when modifications are made upstream.\n"
        );

        version_news_map.put("5.10.14",
"Feature: Implemented \"Locate Box File\" in node popup menu.  Allows users\n" +
"    to easily locate the box file(s) associated with a node.\n\n" +

"Bug Fix: Dog-leg connections now adjust properly.\n\n" +

"Bug Fix: Primitive nodes now retain and display the #include values.\n"
        );

version_news_map.put("5.10.13",
"Alfi Internals: Unresolved connections implemented.  Better data type\n" +
"    query methods.\n"
        );

version_news_map.put("5.10.12",
"Bug Fix: BEGIN_QUOTE/END_QUOTE sequence removed from use with macros.\n"
        );

version_news_map.put("5.10.11",
"Feature: Bundle/bundler copy/paste implementation completed.\n"
        );

        version_news_map.put("5.10.10",
"New Feature: Dialogs for the creation of Link Declarations for base opaque\n" +
"    boxes have been implemented.  Access via Settings menu choice in the\n" +
"    base node window.\n"
        );

        version_news_map.put("5.10.9",
"Bug Fix: New bundle trace out of a base box's external port no longer\n" +
"    causes a Null Pointer Exception.\n"
        );

        version_news_map.put("5.10.8",
"Bug Fix: Bundle generated connections and ports (which only appear in\n" +
"    modeler sections of the box files) are no longer spuriously duplicated\n" +
"    from fully inherited bundle paths.\n"
        );

        version_news_map.put("5.10.7",
"Change: Bundlers cannot be inserted on inherited bundles.\n\n" +

"Bug Fix: Bundlers can now be properly inserted on a local bundle running\n" +
"    between just a bundle output port and a bundle input port.\n"
        );

        version_news_map.put("5.10.6",
"New Feature: The user can now change a bundler's secondary output name(s)\n" +
"    via a right-click menu on a bundler.  If this is done in an instance\n" +
"    node, the changes will be propogated down to the root node\n" +
"    automatically.\n"
        );

        version_news_map.put("5.10.5",
"Bug Fix: Bundles/bundlers should be auto-generating connections only the\n" +
"    modeler sees in the box files (i.e., information Alfi writes to the\n" +
"    box files for the modeler's sake, but does not read back in itself.)\n" +
"    These connections were not being generated correctly and now are.\n"
        );

        version_news_map.put("5.10.4",
"New Feature: Parameter Link (Box Settings) Declaration Editor has been\n" +
"    implemented.  Allows root box designer to define the settings for an\n" +
"    opaque box.\n"
        );

        version_news_map.put("5.10.3",
"USER INTERFACE MODIFICATION: Double clicking on an \"opaque\" box node\n" +
"    results in the box settings dialog to appear instead of the box node's\n" +
"    internal view.  The internal view of the node is still accessible via\n" +
"    the member node's right-click popup menu.\n"
        );

        version_news_map.put("5.10.2",
"Bug Fix: NodePropertiesDialog bug.\n"
        );

        version_news_map.put("5.10.1",
"Bug Fix: ParameterValueChange.generateParserRepresentation() small fix in\n" +
"    new code.\n"
        );

        version_news_map.put("5.10.0",
"New Feature: Box Settings implemented.\n\n" +

"New Feature: FuncX nodes implemented.\n\n" +

"Alfi Internals: Parser modified to read/write box settings\n"
        );

        version_news_map.put("5.9.5",
"USER INTERFACE MODIFICATION: Port adds can now be cancelled, and the\n" +
"    default data type is now Real.\n"
        );

        version_news_map.put("5.9.4",
"Alfi Internals: Junction auto-creation bug fixed.\n"
        );

        version_news_map.put("5.9.3",
"Alfi Internals: Bundler primary to bundler secondary connections have been\n" +
"    disallowed (and vice-versa.)\n\n" +

"Alfi Internals: Other minor bundle/bundler fixes.\n"
        );

        version_news_map.put("5.9.2",
"USER INTERFACE MODIFICATION: Bundlers may now be created on inherited\n" +
"    bundles\n\n" +

"Misc: Unresolvable bundle output channels are flagged for the user at save\n" +
"    time.  Such unresolvable connections will not appear in the modeler\n" +
"    specific sections of the box files.\n"
        );

        version_news_map.put("5.9.1",
"USER INTERFACE MODIFICATION: New Bundlers and Bundles implemented.  See\n" +
"    main documentation for usage details.\n\n" +

"Misc: Base name changes and node deletions for modified instances now\n" +
"    trigger a save option on Quit.\n" 
        );

        version_news_map.put("5.9.0",
"Alfi Internals: Upgraded external JGo widgets library to version 5."
        );

        version_news_map.put("5.8.2",
"USER INTERFACE MODIFICATION: The edit menu has 2 additional menu items:\n" +
"    \"Select All\" and Duplicate.  Duplicate is an operation which combines\n" +
"    Copy and Paste.  This operations can also be invoked through the \n" +
"    following keystrokes: \n" +
"        CTRL-D   Duplicate\n" +
"        CTRL-A   Select All\n\n" +

"Misc: Instance node windows default to the same size as their root base\n" +
"    nodes' windows.\n\n" +

"Bug Fix: 5.8.1. introduced a small bug in New Box creation.  Now fixed."
        );

        version_news_map.put("5.8.1",
"USER INTERFACE MODIFICATION: Users may now change the working directory\n" +
"    via the Preferences dialog accessed through the File menu in the main\n" +
"    window.  Currently this requires that Alfi be restarted after the\n" +
"    change.  This inconvenience will be addressed at a later date.\n\n" +

"USER INTERFACE MODIFICATION: The New Box item in the main window's file\n" +
"    menu now allows users to open pre-existing files if they find there\n" +
"    is already a box file with the name they have selected.\n\n" +

"USER INTERFACE MODIFICATION: A small dialog appears while box loading\n" +
"    is taking place to show the user what is happening since this can\n" +
"    be time consuming."
        );

        version_news_map.put("5.8.0",
"USER INTERFACE MODIFICATION: Modification of the E2E_PATH is now done\n" +
"    in the Preferences Dialog, via the the main window File menu.\n\n" +

"USER INTERFACE MODIFICATION: Inherited ports can no longer be deleted.\n\n" +

"Alfi/Modeler Interface Bug Fix: Alfi allows implicit removals of\n" +
"    connections which were linked to inherited member nodes which have\n" +
"    been (explicitly) removed by the user.  The modeler expects explicit\n" +
"    references to these connection removals in box files it reads.  Alfi\n" +
"    now writes these removals explicitly in the Remove_Connections section\n" +
"    of a box file which the modeler reads.  Alfi ignores this section when\n" +
"    reading boxes itself.\n\n" +

"Misc: Old box files now trigger a resave option on Quit in order to update\n" +
"    them to prevent possible future parsing problems.\n\n" +

"Misc: The Alfi splash window now disappears after startup.\n\n" +

"Preference file change: The local alfi_prefs.txt file has been reformatted\n" +
"    to make restarts consistent regarding possible E2E_PATH changes from\n" +
"    session to session.  Pre 5.8.0 versions of this file will no longer\n" +
"    work.  Apologies for the inconvenience (your last session's windows\n" +
"    will not automatically appear the first time you run version\n" +
"    5.8.0+.)\n\n" +

"Alfi Internals: Significant changes have been made in the implementation\n" +
"    of the loading of box files, showing of edit windows, and modification\n" +
"    of the E2E_PATH.  These are not parser changes, but are general code\n" +
"    condensing and cleanup.  In particular, various calls to load boxes\n" +
"    now all use the same method and methodology."
        );

        version_news_map.put("5.7.7",
"USER INTERFACE MODIFICATION: The popup menus used to add boxes and\n" +
"    primitives are now sorted alphabetically.\n\n" +

"PARSER Bug Fix: Version 5.7.6 introduced a bug which could corrupt macros\n" +
"    as they are written out to box files.  This is now fixed."
        );

        version_news_map.put("5.7.6",
"USER INTERFACE MODIFICATION: Port creation is no longer done via the\n" +
"    right-click popup menu.  To create a port, merely left-click anywhere\n" +
"    in the black port border, and a new port will appear at that location,\n" +
"    along with a modification dialog to change the port from its default\n" +
"    state.\n\n" +

"New Feature: Added Alfi Updates information dialog.  It is available\n" +
"    through the Help menu and also automatically at startup whenever a\n" +
"    user starts a newer version than they last used.\n\n" +

"Alfi Internals: Macro sections are no longer parsed as name = value pairs.\n" +
"    The entire section is now read as a single block of text, editted as\n" +
"    such from within Alfi, and saved to box files as such.  The only\n" +
"    parsing of macros is done in the modeller."
        );

        return version_news_map;
    }
}
