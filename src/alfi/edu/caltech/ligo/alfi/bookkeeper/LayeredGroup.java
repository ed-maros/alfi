package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.*;
import java.util.*;

import edu.caltech.ligo.alfi.*;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.file.*;


    /**
     * A LayeredGroup is used to create a compound object consisting of one
     * or more CommentTexts and one or more CommentBackground. 
     * 
     */
public class LayeredGroup implements  ProxyChangeEventSource,
                                      ContainedElementModEventSource {


    ////////////////////////////////////////////////////////////////////////////
    /////// member fields //////////////////////////////////////////////////////

        /** This is the node in which this LayeredGroup was locally added. */
    protected final ALFINode m_direct_container_node;

        /** The name of the LayeredGroup object. */
    protected String m_name;

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected HashMap m_proxy_change_listener_map;

        /** Used in conjunction with ContainedElementModEventSource. */
    protected HashMap m_contained_element_mod_listeners_map;

        /** Used in conjunction with GroupElementModEventSource interface. */
    protected HashMap m_group_change_listener_map;

        /** This content is used to track the layers and groupings of the 
          * widgets */
    private Vector m_group_elements;


    //**************************************************************************
    //***** constructors *******************************************************

        /** Main constructor.  Should only be called from ALFINode. */
    LayeredGroup(ALFINode _container_node, String _name) {

        m_direct_container_node = _container_node;

        if ((_name == null) || (_name.length() < 1)) {
            m_name = generateDefaultName(_container_node);
        }
        else { m_name = new String(_name); }
        m_proxy_change_listener_map = new HashMap();
        m_contained_element_mod_listeners_map = new HashMap();
        m_group_change_listener_map = new HashMap();
  
    }

        /**
         * Auxilliary constructor 
         */
    public LayeredGroup (String _name) {
        m_name = _name;
        m_direct_container_node = null;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// general methods ////////////////////////////////////////////////////

    public String getName () { return m_name; }
    public void setName (String _name) {
        m_name = _name;
    }

        /** 
         * Add an element in the group
         */
    public void addGroupElement (GroupableIF _element) {
        if (m_group_elements == null) {
            m_group_elements = new Vector();
        }
        m_group_elements.add(_element);
        _element.group_set(this);
    }

        /** 
         * Remove the element in the list
         */
    public void removeElementFromGroup (GroupableIF _element) {
        int index = m_group_elements.indexOf(_element);
        
        if (index >= 0) {
            m_group_elements.remove(index);
        }

        _element.group_set(null);
    }

        /** 
         * Remove the element in the list
         */
    public void moveElement (int _old_index, int _new_index) {

        GroupableIF element = (GroupableIF)m_group_elements.remove(_old_index);
        m_group_elements.insertElementAt(element, _new_index);
    }


        /**
         * Sends the element to the beginning of the list
         */
    public void sendToFirst (GroupableIF _element) {
        
        int cur_index = m_group_elements.indexOf(_element);

        if (cur_index >= 0) {
           moveElement(cur_index, 0);
        }
    }

        /**
         * Sends the element to the beginning of the list
         */
    public void sendToLast (GroupableIF _element) {
        
        int cur_index = m_group_elements.indexOf(_element);
        int last_index = numElements() - 1;

        if (cur_index >= 0) {
           moveElement(cur_index, last_index);
        }
    }

        /** 
         * Get the element in the list
         */
    public GroupableIF  getElement (int index) {
        return ( GroupableIF )m_group_elements.get(index); 
    }

        /**
         * Get the index of the element in the group
         */
    public int getIndex (GroupableIF _element) {
        return m_group_elements.indexOf(_element);
    }

        /**
         * Get the index of an element  using its string name
         */
    public int getIndex (String _name) {

        for (int i = 0; i < numElements(); i++) {
            GroupableIF element = getElement(i);
            if (element.getElementName().equals(_name)) {
                return i;
            }
        }

        return -1;
    }

        /**
         * Get the number of items in the group
         */
    public int numElements () {
        if (m_group_elements == null) { return 0; }

        return m_group_elements.size();
    }

        /**
         * Prints the elements in order 
         */
    public void printElements () {
        for (int i = 0; i < numElements(); i++) {
            System.out.println(i + " " + getElement(i));
        }
    }

        /**
         * Arranges the group elements, following the order
         * of another layeredGroup.   This is used when groups
         * are copied and order needs to be maintained.   
         * Note: this LayeredGroup does not have to contain
         * the same number of elements as the source
         */
    public void copyGroupOrder (LayeredGroup _source) {

        // Create temporary vector using the elements of _source,
        // but checking if the element's name exists in m_group_elements;
        Vector temp_vector = new Vector();
        
        for (int i = 0; i < _source.numElements(); i++){
            GroupableIF src_elem = _source.getElement(i);
    
            if (getIndex(src_elem.getElementName()) >= 0) {
                // This means we found this entry in m_group_elements
                // so add it in our temp vector
                temp_vector.add(src_elem);
            }    
        }

        // Now let's match the ordering
        for (int i = 0; i < temp_vector.size(); i++) {
            GroupableIF tmp_elem = (GroupableIF) temp_vector.elementAt(i);
            int this_index = getIndex(tmp_elem.getElementName());
            if (this_index != i) {
                moveElement(this_index, i);
            }
        }
        
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// class methods //////////////////////////////////////////////////////

    protected String generateDefaultName (ALFINode _container_node) {
        LayeredGroup[] groups = _container_node.layeredGroups_get();
        String[] taken_names = new String[groups.length];
        for (int i = 0; i < groups.length; i++) {
            taken_names[i] = groups[i].getName();
        }

        String root = "LayeredGroup_";
        int n = 0;
        String name = "";
        boolean b_name_taken = true;
        while (b_name_taken) {
            name = root + n;
            b_name_taken = false;
            for (int i = 0; i < taken_names.length; i++) {
                if (name.equals(taken_names[i])) {
                    b_name_taken = true;
                    n++;
                    break;
                }
            }
        }
        return name;
    }


    public String toString() { return getName(); }


    //////////////////////////////////////////////////////
    ////////// ProxyChangeEventSource Methods //////////

        /** ProxyChangeEventSource method. */
    public void addProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.put(_listener, null);
    }

        /** ProxyChangeEventSource method. */
    public void removeProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.remove(_listener);
    }

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected void fireProxyChangeEvent(ProxyChangeEvent e) {
        for (Iterator i = m_proxy_change_listener_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ProxyChangeEventListener) i.next()).proxyChangePerformed(e);
        }
    }


    /////////////////////////////////////////////////////////////////
    ////////// ContainedElementModEventSource methods ///////////////

        /** ContainedElementModEventSource method. */
    public void addContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.put(_listener, null);
    }

        /** ContainedElementModEventSource method. */
    public void removeContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with ContainedElementModEventSource. */
    protected void fireContainedElementModEvent(ContainedElementModEvent e) {
        for (Iterator i =
                m_contained_element_mod_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ContainedElementModEventListener) i.next()).
                                                   containedElementModified(e);
        }
    }

}
