package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /**
     * This event is created by any widget element whose associatedProxy is a
     * is a part of a LayeredGroup and can be modified (moved, deleted, 
     * added to, deleted from, etc.)  It is fired whenever one of the 
     * group element is changed.  
     *
     */
public class GroupElementModEvent extends EventObject {

    //**************************************************************************
    //***** constants **********************************************************

        // action codes
     public static final int GROUP_SELECT    = 0;
     public static final int GROUP_UNSELECT  = 1;
     public static final int GROUP_DELETE    = 2;
     public static final int GROUP_DUPLICATE = 3;

    //**************************************************************************
    //***** fields *************************************************************

         /** Action which has taken place.  See action codes. */
     private final int m_action_code;

         /** Element associated with the action. 
           * In most cases, it's the group_name.
           */
     private final Object m_associated_element;


    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public GroupElementModEvent(Object _source, int _action, 
                Object _associated_element) {
        super(_source);
        m_action_code = _action;
        m_associated_element = _associated_element;
    }

    //**************************************************************************
    //***** methods ************************************************************

    /////////////////////////////////
    // member field access //////////

    public int getAction() { return m_action_code; }

    public Object getAssociatedElement() { return m_associated_element; }

    public String toString() {
        return "GroupElementModEvent[" + getSource() + ", " + m_action_code +
               ", " + m_associated_element + "]";
    }
}
