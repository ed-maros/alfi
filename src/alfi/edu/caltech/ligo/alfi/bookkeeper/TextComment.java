package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.Point;
import java.awt.Dimension;
import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.tools.*;

    /**
     * TextComment objects are graphical texts displayed on a window.  
     * It is used to display any information about the node.
     */
public class TextComment implements  GroupableIF,
                                     ProxyChangeEventSource,
                                     GroupElementModEventSource,
                                     ContainedElementModEventSource {

    ////////////////////////////////////////////////////////////////////////////
    /////// constants //////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////
    /////// member fields //////////////////////////////////////////////////////

        /** This is the node in which this PortProxy was locally added. */
    protected final ALFINode m_direct_container_node;

        /** The name of the comment object. */
    protected String m_name;

        /** The comment itself. */
    private String m_comment_value;

        /** The location of the text label with respect to the widget's coord */
    private Point m_location;

        /** The font size */
    private int m_font_size;

        /** The text color */
    private int m_color;

        /** Is the text bolded? */
    private boolean mb_bold;

        /** Is the text italicized? */
    private boolean mb_italic;

        /** Is the text underlined? */
    private boolean mb_underline;

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected HashMap m_proxy_change_listener_map;

        /** Used in conjunction with ContainedElementModEventSource. */
    protected HashMap m_contained_element_mod_listeners_map;

        /** Used in conjunction with GroupableIF interface. */
    protected LayeredGroup m_group_container;

        /** Used in conjuction with GroupElementModEventSource */
    private HashMap m_group_element_mod_listeners_map;
 
    ////////////////////////////////////////////////////////////////////////////
    /////// constructors ///////////////////////////////////////////////////////

    public TextComment (ALFINode _container_node, String _name, String _value, 
            Point _location, int _font_size) {

        this(_container_node, _name, _value, _location, _font_size, 
            AlfiColors.ALFI_BLACK, false, false, false);
    }


        /** Main constructor.  Should only be called from ALFINode. */
    public TextComment (ALFINode _container_node, String _name, String _value, 
            Point _location, int _font_size, int _color, 
            boolean _bolded, boolean _italicized, boolean _underlined) {
        assert(_container_node != null);

        m_comment_value = _value;

            // final member initializations
        m_direct_container_node = _container_node;

            // others
        m_group_container = null;
        m_name = (_name != null) ? new String(_name) :
                                   generateDefaultName(_container_node);

        m_location = _location;
        m_font_size = _font_size;
        m_color = _color;
        mb_bold = _bolded;
        mb_italic = _italicized;
        mb_underline = _underlined;

        m_proxy_change_listener_map = new HashMap();
        m_contained_element_mod_listeners_map = new HashMap();
        m_group_element_mod_listeners_map = new HashMap();
     }

        /** Auxilliary constructor which creates a proxy for use by
          * ProxyChangeEvent sources.  These objects are useless elsewhere
          * because the (final) container node field is null.  These objects
          * are used only to transport change information regarding the
          * original.  Unchanged fields are set to null.
          */
    public TextComment (String _name, String _value, Point _text_loc, 
            Integer _font_size, Integer _color, 
            Boolean _bolded, Boolean _italicized, Boolean _underlined) {

        m_direct_container_node = null;
        m_name = _name;

        m_comment_value = _value;
        m_location = _text_loc;

        if (_font_size != null) { m_font_size = _font_size.intValue(); }
        if (_color != null) { m_color = _color.intValue(); }
        if (_bolded != null) { mb_bold = _bolded.booleanValue(); }
        if (_italicized != null) { mb_italic = _italicized.booleanValue(); }
        if (_underlined != null) { mb_underline = _underlined.booleanValue(); }

        m_proxy_change_listener_map = null;
        m_contained_element_mod_listeners_map = null;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// class methods //////////////////////////////////////////////////////

    protected String generateDefaultName (ALFINode _container_node) {
        TextComment[] comments = _container_node.textComments_get();
        String[] taken_names = new String[comments.length];
        for (int i = 0; i < comments.length; i++) {
            taken_names[i] = comments[i].getName();
        }

        String root = "TextComment_";
        int n = 0;
        String name = "";
        boolean b_name_taken = true;
        while (b_name_taken) {
            name = root + n;
            b_name_taken = false;
            for (int i = 0; i < taken_names.length; i++) {
                if (name.equals(taken_names[i])) {
                    b_name_taken = true;
                    n++;
                    break;
                }
            }
        }
        
        return name;
    }


    ////////////////////////////////////////////////////////////////////////////
    /////// general methods ////////////////////////////////////////////////////

    public String getName () { return m_name; }

        /** Sets the name */
    public boolean setName (String _name) {

        if ((_name != null) && (_name.length() > 0)
                            && (! _name.equals(m_name))) {
            m_name = _name;
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new TextComment(_name, null, null, null,
                                null, null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }
        /** Returns a copy of the String value if set, otherwise null. */
    public String getCommentValue () {
        return (m_comment_value != null) ? new String(m_comment_value) : null;
    }

        /** Sets the Comment string */
    public boolean setCommentValue (String _value) {

        if (! _value.equals(m_comment_value)) {
            m_comment_value = _value;
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new TextComment(null, _value, null, null,
                                null, null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

        /** Returns the location of the text. */
    public Point getLocation () {return new Point(m_location);}

        /** Sets location of the text. */
    public boolean setLocation (Point _new_location) {

        if (!_new_location.equals(m_location)) {
        
            m_location = _new_location; 

            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new TextComment(null, null, _new_location, null, 
                                null, null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }


        /** Returns the font size of the text. */
    public int getFontSize () {return m_font_size;}

        /** Sets the size of the font. */
    public boolean setFontSize (int _new_font_size) {

        if (m_font_size == 0 && _new_font_size != 0) {
            m_font_size = _new_font_size;
            return true;
        }
        else if (_new_font_size != m_font_size) {
        
            m_direct_container_node.setTimeLastModified();
            m_font_size = _new_font_size;

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new TextComment(null, null, null, new Integer(_new_font_size),
                                null, null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public int getColor() {return m_color;}

    public boolean setColor(int _new_color) {

        if (_new_color != m_color) {
        
            m_direct_container_node.setTimeLastModified();
            m_color = _new_color;

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new TextComment(null, null, null, null,
                    new Integer(_new_color), null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
   }


    public boolean isBold() {return mb_bold;}

    public boolean setBold (boolean _b_bold) {

        if (_b_bold != mb_bold) {
        
            m_direct_container_node.setTimeLastModified();
            mb_bold = _b_bold;

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new TextComment(null, null, null, null,
                                null, new Boolean(mb_bold), null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
        
   }

    public boolean isItalic() {return mb_italic;}

    public boolean setItalic(boolean _b_italic) {

        if (_b_italic != mb_italic) {
        
            m_direct_container_node.setTimeLastModified();
            mb_italic = _b_italic;

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new TextComment(null, null, null, null,
                                null, null, new Boolean(mb_italic), null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public boolean isUnderlined() {return mb_underline;}

    public boolean setUnderline(boolean _b_underline) {

        if (_b_underline != mb_underline) {
        
            m_direct_container_node.setTimeLastModified();
            mb_underline = _b_underline;

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new TextComment(null, null, null, null,
                                null, null, null, new Boolean(mb_underline)));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public String toString() { return toString(false); }

    public String toString(boolean _b_verbose) {
        String info = getName();
        if (_b_verbose) {
            info += ", <" + getCommentValue() + ">";
        }
        return info;
    }
    //////////////////////////////////////////////////////
    ////////// GroupableIF Methods ///////////////////////

        /** GroupableIF method */
    public LayeredGroup group_get() {return m_group_container; };

        /** GroupableIF method */
    public void group_set(LayeredGroup _group) {
        m_group_container = _group;

        if (m_group_container == null) {
            m_group_element_mod_listeners_map.clear();
        }
    }
        /** GroupableIF method */
    public String getElementName() { return getName(); }


         /** GroupableIF method */
    public void group_deleteElement( ) {
    }

        /** GroupableIF method */
    public void group_moveElement( ) {
    }
        /** GroupableIF method */
    public void group_registerListener(
                                      GroupElementModEventListener _listener) {
        addGroupElementModListener(_listener);
    }

    //////////////////////////////////////////////////////
    ////////// ProxyChangeEventSource Methods ////////////

        /** ProxyChangeEventSource method. */
    public void addProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.put(_listener, null);
    }

        /** ProxyChangeEventSource method. */
    public void removeProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.remove(_listener);
    }

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected void fireProxyChangeEvent(ProxyChangeEvent e) {
        for (Iterator i = m_proxy_change_listener_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ProxyChangeEventListener) i.next()).proxyChangePerformed(e);
        }
    }


    /////////////////////////////////////////////////////////////////
    ////////// ContainedElementModEventSource methods ///////////////

        /** ContainedElementModEventSource method. */
    public void addContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.put(_listener, null);
    }

        /** ContainedElementModEventSource method. */
    public void removeContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with ContainedElementModEventSource. */
    protected void fireContainedElementModEvent(ContainedElementModEvent e) {
        for (Iterator i =
                m_contained_element_mod_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ContainedElementModEventListener) i.next()).
                                                   containedElementModified(e);
        }
    }


    /////////////////////////////////////////////////////////////////
    ////////// GroupElementModEventSource methods ///////////////

        /** GroupElementModEventSource method. */
    public void addGroupElementModListener(
                        GroupElementModEventListener _listener) {
        m_group_element_mod_listeners_map.put(_listener, null);
    }

        /** GroupElementModEventSource method. */
    public void removeGroupElementModListener(
                        GroupElementModEventListener _listener) {
        m_group_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with GroupElementModEventSource. */
    public void fireGroupElementModEvent(GroupElementModEvent e) {
        for (Iterator i =
                m_group_element_mod_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((GroupElementModEventListener) i.next()).groupElementModified(e);
        }
    }

    
}
