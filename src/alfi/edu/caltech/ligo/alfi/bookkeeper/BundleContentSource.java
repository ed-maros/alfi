package edu.caltech.ligo.alfi.bookkeeper;

    /** This interface is just a marker, used to identify objects which may be
      * sources for bundle content.  Usually this is just input bundlers where
      * content is fed into a bundle, or an output bundler where the user can
      * create not-yet-existent channel names to be fed into a bundle, but also
      * input bundle ports with no external bundle connection may have a default
      * set of channel names to feed into a bundle as well.
      */
public interface BundleContentSource { }
