package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.Point;
import java.awt.Dimension;
import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.tools.*;

    /**
     * ColoredBox objects are graphical notes posted along a window.  
     * It is used to display any information about the node.
     */
public class ColoredBox implements  GroupableIF,
                                    ProxyChangeEventSource,
                                    GroupElementModEventSource,
                                    ContainedElementModEventSource {

    ////////////////////////////////////////////////////////////////////////////
    /////// constants //////////////////////////////////////////////////////////


    public final static int HILITE_LAYER_BACK  = 0;
    public final static int HILITE_LAYER_FRONT = 1;

    //**************************************************************************
    //***** static fields ******************************************************

    public static final String BACK_LAYER  = "Back";
    public static final String FRONT_LAYER = "Front";


    ////////////////////////////////////////////////////////////////////////////
    /////// member fields //////////////////////////////////////////////////////

        /** This is the node in which this PortProxy was locally added. */
    protected final ALFINode m_direct_container_node;

        /** The name of the comment object. */
    protected String m_name;

        /** The color of comment */
    private int m_background_color;

        /** The location of the widget with respect to the ALFIView */
    private Point m_background_location;

        /** The size of the whole area*/
    private Dimension m_size;

        /** Box is in the front/back layer */
    private int m_layer;

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected HashMap m_proxy_change_listener_map;

        /** Used in conjunction with ContainedElementModEventSource. */
    protected HashMap m_contained_element_mod_listeners_map;

        /** Used in conjuction with GroupElementModEventSource */
    private HashMap m_group_element_mod_listeners_map;
 
        /** Used in conjunction with GroupableIF interface. */
    protected LayeredGroup m_group_container;


    ////////////////////////////////////////////////////////////////////////////
    /////// constructors ///////////////////////////////////////////////////////

        /** Main constructor.  Should only be called from ALFINode. */
    public ColoredBox (ALFINode _container_node, String _name, int _color,
                       Point _bg_location, Dimension _size, int _layer)  {
        assert(_container_node != null);

        m_background_color = _color;

        m_group_container = null;

            // final member initializations
        m_direct_container_node = _container_node;

            // others
        m_name = (_name != null) ? new String(_name) :
                                   generateDefaultName(_container_node);

        m_background_location = _bg_location;
        m_size = _size;
        m_layer = _layer;

        m_proxy_change_listener_map = new HashMap();
        m_contained_element_mod_listeners_map = new HashMap();
        m_group_element_mod_listeners_map = new HashMap();
     }

        /** Auxilliary constructor which creates a proxy for use by
          * ProxyChangeEvent sources.  These objects are useless elsewhere
          * because the (final) container node field is null.  These objects
          * are used only to transport change information regarding the
          * original.  Unchanged fields are set to null.
          */
    public ColoredBox (String _name, Integer _color, Point _location, 
                                              Dimension _size, Integer _layer) {

        m_direct_container_node = null;
        m_name = _name;

        if (_color != null) { m_background_color = _color.intValue(); }
        else  { m_background_color = AlfiColors.ALFI_YELLOW; } 

        m_background_location = _location;
        m_size = _size;
        
        if (_layer != null) { m_layer = _layer.intValue(); }
        else  { m_layer = HILITE_LAYER_BACK; } 

        m_proxy_change_listener_map = null;
        m_contained_element_mod_listeners_map = null;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// class methods //////////////////////////////////////////////////////


        /** Returns the string used by the parser */
    public static String getStringFromLayerId (int _layer_id) {
        if (_layer_id == HILITE_LAYER_FRONT) {
            return FRONT_LAYER;
        }
        else
            return BACK_LAYER;
    }

        /**  Finds the next availabe name */
    protected String generateDefaultName (ALFINode _container_node) {
        ColoredBox[] comments = _container_node.coloredBoxes_get();
        String[] taken_names = new String[comments.length];
        for (int i = 0; i < comments.length; i++) {
            taken_names[i] = comments[i].getName();
        }

        String root = "CommentBG_";
        int n = 0;
        String name = "";
        boolean b_name_taken = true;
        while (b_name_taken) {
            name = root + n;
            b_name_taken = false;
            for (int i = 0; i < taken_names.length; i++) {
                if (name.equals(taken_names[i])) {
                    b_name_taken = true;
                    n++;
                    break;
                }
            }
        }
        return name;
    }


    ////////////////////////////////////////////////////////////////////////////
    /////// general methods ////////////////////////////////////////////////////

        /** Each ColoredBox has a unique name */
    public String getName () { return m_name; }

        /** Sets the name */
    public boolean setName (String _name) {
        if ((_name != null) && (_name.length() > 0)
                            && (! _name.equals(m_name))) {
            m_name = _name;

            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new ColoredBox(_name, null, null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

        /** Returns the color of the box */
    public int getColor () { return m_background_color; }

        /** Sets color of the box */
    public boolean setColor (int _color) {

        if (_color != m_background_color) {
            m_background_color = _color;

            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new ColoredBox(null, new Integer(_color), null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e1 = 
                                     new ContainedElementModEvent(this);
            fireContainedElementModEvent(e1);

            return true;
        }
        else { return false; }
    }

        /** Returns the location of the ColoredBox */
    public Point getLocation () {return new Point(m_background_location);}

        /** Sets the location of the ColoredBox */
    public boolean setLocation (Point _new_location) {

        if (!_new_location.equals(m_background_location)) {
        
            //m_background_location =  GridTool.getNearestGridPoint(_new_location); 
            m_background_location = _new_location; 

            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new ColoredBox(null, null, _new_location, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }


        /** Returns the size */
    public Dimension getSize () {return new Dimension(m_size);}

        /** Sets the size */
    public boolean setSize (Dimension _new_size) {

        if (m_size == null && _new_size != null) {
            m_size = _new_size;
            return true;
        }
        else if (!_new_size.equals(m_size)) {
        
            m_direct_container_node.setTimeLastModified();
            m_size = _new_size;

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new ColoredBox(null, null, null, _new_size, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
        
    }

        /** Returns the layer, HILITE_LAYER_BACK or HILITE_LAYER_FRONT */
    public int getLayer() {return m_layer;}

        /** Sets the layer */
    public boolean setLayer (int _layer) {
        if (m_layer != _layer) {
            m_layer = _layer;
        
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                new ColoredBox(null, null, null, null, new Integer(_layer)));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
        
    }

    public String toString() { return toString(false); }

    public String toString(boolean _b_verbose) {
        String info =  getName();
        if (_b_verbose) {
            info += ", " + AlfiColors.getColorString(m_background_color) + 
                    ", " + getSize() + 
                    ", " + getLocation() +
                    ", " + getStringFromLayerId(m_layer);
        }
        return info;
    }
    //////////////////////////////////////////////////////
    ////////// GroupableIF Methods ///////////////////////

        /** GroupableIF method */
    public LayeredGroup group_get() {return m_group_container; };

        /** GroupableIF method */
    public void group_set(LayeredGroup _group) {
        m_group_container = _group;

        if (m_group_container == null) {
            m_group_element_mod_listeners_map.clear();
        }
    }
        /** GroupableIF method */
    public String getElementName() { return getName(); }

        /** GroupableIF method */
    public void group_deleteElement( ) {
    }

        /** GroupableIF method */
    public void group_moveElement( ) {
    }
        /** GroupableIF method */
    public void group_registerListener(
                                      GroupElementModEventListener _listener) {
        addGroupElementModListener(_listener);
    }


    //////////////////////////////////////////////////////
    ////////// ProxyChangeEventSource Methods //////////

        /** ProxyChangeEventSource method. */
    public void addProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.put(_listener, null);
    }

        /** ProxyChangeEventSource method. */
    public void removeProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.remove(_listener);
    }

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected void fireProxyChangeEvent(ProxyChangeEvent e) {
        for (Iterator i = m_proxy_change_listener_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ProxyChangeEventListener) i.next()).proxyChangePerformed(e);
        }
    }


    /////////////////////////////////////////////////////////////////
    ////////// ContainedElementModEventSource methods ///////////////

        /** ContainedElementModEventSource method. */
    public void addContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.put(_listener, null);
    }

        /** ContainedElementModEventSource method. */
    public void removeContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with ContainedElementModEventSource. */
    protected void fireContainedElementModEvent(ContainedElementModEvent e) {
        for (Iterator i =
                m_contained_element_mod_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ContainedElementModEventListener) i.next()).
                                                   containedElementModified(e);
        }
    }

    /////////////////////////////////////////////////////////////////
    ////////// GroupElementModEventSource methods ///////////////

        /** GroupElementModEventSource method. */
    public void addGroupElementModListener(
                        GroupElementModEventListener _listener) {
        m_group_element_mod_listeners_map.put(_listener, null);
    }

        /** GroupElementModEventSource method. */
    public void removeGroupElementModListener(
                        GroupElementModEventListener _listener) {
        m_group_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with GroupElementModEventSource. */
    public void fireGroupElementModEvent(GroupElementModEvent e) {
        for (Iterator i =
                m_group_element_mod_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((GroupElementModEventListener) i.next()).groupElementModified(e);
        }
    }

  
}
