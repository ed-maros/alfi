package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /** This event is fired by the node about to be removed from the system to
      * any interested listeners, such as associated EditorFrames which need to
      * be closed.
      */
public class NodeToBeDestroyedEvent extends EventObject {
    public NodeToBeDestroyedEvent(ALFINode _node_to_be_destroyed) {
        super(_node_to_be_destroyed);
    }
}
