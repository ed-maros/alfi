package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.Color;
import java.beans.BeanInfo;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Arrays;
import java.util.ListResourceBundle;

import com.l2fprod.common.beans.BaseBeanInfo;
import com.l2fprod.common.beans.editor.ComboBoxPropertyEditor;
import com.l2fprod.common.model.DefaultBeanInfoResolver;
import com.l2fprod.common.propertysheet.Property;
import com.l2fprod.common.propertysheet.PropertySheet;
import com.l2fprod.common.propertysheet.PropertySheetPanel;
import com.l2fprod.common.swing.LookAndFeelTweaks;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;

    /**
     *
     */
public class ALFINodeBean {

    private ALFINode m_node;

    public void setNode (ALFINode _node) {
        m_node = _node;
    }

    public String getName () {
        return m_node.generateFullNodePathName();
    }
    
    public String getComment () {
        return m_node.comment_getLocal();
    }

    public String toString () {
        return m_node.toString(true);
    }

    public boolean isBox () {
        return m_node.isBox();
    }

    public String getNodeInfo () {
        return m_node.toString(true);
    }

    
}

