package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.*;
import java.util.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.*;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.file.*;


    /** Instances of primitives may modify the default settings of the
      * primitives parameters and input ports.  These are very lightweight
      * objects.  These objects are only added to instances of primitives,
      * never boxes.
      */
public class ParameterSetting implements NodeSettingIF {

    //**************************************************************************
    //***** member fields ******************************************************

        /** This is the node in which this ParameterSetting was locally added.*/
    protected final ALFINode m_direct_container_node;

        /** Must be one of the names of parameters or input ports in the
          * primitive's root base.
          */
    protected final String m_name;

        /** The new value for the parameter referenced by m_name. */
    protected final String m_value;

        /** An optional comment associated with this modification. */
    protected StringBuffer m_comment;

        /** Saves the state on whether the value was quoted in the file. */
    protected final boolean mb_quoted;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor.  Only ALFINode should construct ParameterSettings. */
    ParameterSetting(ALFINode _container_node, String _name, String _value,
                                                           boolean _b_quoted) {
            // final member initializations
        m_direct_container_node = _container_node;
        m_name = _name;
        m_value = _value;
        mb_quoted = _b_quoted;

            // other members
        m_comment = null;
    }

    //**************************************************************************
    //***** methods ************************************************************

        /** Write the object in its parser representation. */
    public String generateParserRepresentation() {
        boolean b_is_include_marker = m_name.equals(Parser.FILE_INCLUDE_MARKER);

        String s = m_name;
        s += (b_is_include_marker) ? " " : " = ";
        s += m_value;

        if (m_comment != null) {
            s += Parser.STANDARD_SPACER + Parser.COMMENT_MARKER +
                                                        " " + m_comment;
        }

            // parameters with string types should always be quoted
            //  [type is null for ports, etc.]
        String type = determineType();
        boolean b_auto_quote = (type == null) ? false :
                                 type.equals(ParameterDeclaration.TYPE_STRING);
            
        if ( !b_is_include_marker && (mb_quoted || b_auto_quote) ){
            s = Parser.QUOTE_BEGIN + Parser.NL + s + Parser.NL +
                                     Parser.STANDARD_SPACER + Parser.QUOTE_END;
        }
        return s;
    }

        /** Generate a description of the connection. */
    public String toString() {
        String info = "ParameterSetting(" +
            m_direct_container_node.generateFullNodePathName() + "): (" +
            m_name + ", " + m_value + (mb_quoted ? " (QUOTED)" : "") + ")";
        return info;
    }

    private String determineType() {
        ALFINode node = m_direct_container_node.
                            parameterDeclaration_find(m_name);
        if (node != null) {
            ParameterDeclaration param = 
                node.parameterDeclaration_getLocal(m_name);
            return param.getType();
        }

/*
        ALFINode root_primitive = m_direct_container_node.baseNode_getRoot();
        ParameterDeclaration[] parameter_declarations =
                               root_primitive.parameterDeclarations_getLocal();
        for (int i = 0; i < parameter_declarations.length; i++) {
            if (parameter_declarations[i].getName().equals(m_name)) {
                return parameter_declarations[i].getType();
            }
        }
*/

        return null;
    }

    /////////////////////////////////
    // member field access //////////

        /**
         * @returns m_direct_container_node.
         */
    ALFINode getDirectContainerNode() {
        return m_direct_container_node;
    }

    public boolean isQuoted() { return mb_quoted; }

    public boolean appendLocalComment(String _comment) {
        String NL = Parser.NL;

        _comment.trim();
        if (m_comment == null) { m_comment = new StringBuffer(_comment); }
        else {
            m_comment.append(NL + NL + "    ----    ----" + NL + NL);
            m_comment.append(_comment);
        }
        m_direct_container_node.setTimeLastModified();
        return true;
    }
            
    public boolean clearLocalComment() {
        if (m_comment != null) {
            m_comment = null;
            m_direct_container_node.setTimeLastModified();
            return true;
        }
        else { return false; }
    }

    /////////////////////////////////
    // NodeSettingIF methods ////////

    public String getName() { return m_name; }

    public String getValue() { return m_value; }

    public String getType() { return determineType(); }

    public String getComment() {
        if (m_comment != null) { return m_comment.toString(); }
        else { return new String(""); }
    }
}
