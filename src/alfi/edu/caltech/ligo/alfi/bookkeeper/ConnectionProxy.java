package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.editor.*;


    /**
     * When a connection is added to a container node, a ConnectionProxy object
     * is created and added to the container node's add_connection list.  This
     * object represents all the connections which appear in the container node
     * and all nodes based on the container node.  The whole line of inherited
     * nodes share the source port, sink port, and path of this ConnectionProxy.
     * This object is similar to the MemberNodeProxy.
     */
public class ConnectionProxy implements ProxyChangeEventSource,
                                        ContainedElementModEventSource,
                                        UpstreamDataChannelChangeEventSource {

    //**************************************************************************
    //***** local exception classes ********************************************

    //**************************************************************************
    //***** static member fields ***********************************************

        // just optimization
    private static final String SOURCE_TEXT = "Source ";
    private static final String SINK_TEXT = "Sink ";

    public static final String ARROW = "->";
    public static final String _ARROW_ = " " + ARROW + " ";

    public static final int DIRECTION_NORTH      = 0;
    public static final int DIRECTION_SOUTH      = 1;
    public static final int DIRECTION_EAST       = 2;
    public static final int DIRECTION_WEST       = 3;
    public static final int DIRECTION_ANY        = 4;
    public static final int DIRECTION_INVALID    = 5;

    public static final int DOG_LEG_HINT_NONE = 0;
    public static final int DOG_LEG_HINT_CW   = -1;
    public static final int DOG_LEG_HINT_CCW  = 1;

    //**************************************************************************
    //***** member fields ******************************************************

        /** This is the node in which this ConnectionProxy was locally added. */
    protected final ALFINode m_direct_container_node;

        /**
         * This list of Integer objects is the minimal description needed to
         * describe an orthogonal path from its source port's point to its sink
         * port's point.  The numbers in this list are only the x and y
         * coordinates of alternating vertical and horizontal lines.  Even
         * members are for the vertical lines and odd for the horizontal.  If
         * the first element is null, it means that the first line described is
         * a horizontal line.  The first *segment* of the path is implicitly
         * inferred by the information of the source point and of the first
         * *line* described in this list.  If the first line described in this
         * list is a horizontal line at y = 50, and the source point is
         * (10, 20), then this _inferred_ first segment would be vertical
         * segment from (10, 20) to (10, 50).  If the next line defined in the
         * list is x = 95, then the next segment would be horizontal (lying on
         * y = 50), from (10, 50) to (95, 50), and so on.  The last segment of
         * the path is inferred in the same manner as the first, from the sink
         * port point and the last line defined in this list.  All of this may
         * seem overly complex, but it is the simplest way of defining a path of
         * orthogonal segments from start (source) point to end (sink) point
         * without any redundant data, which means that the points on the path
         * don't need to always be checked for validity.  It greatly simplifies
         * the manipulation of these paths when a user wants to adjust a path
         * and/or a source or sink port position.
         *
         * E.g., source at (0, 10), sink at (100, 40), and this list being
         * { null, 20, 50, 30 } would describe a path from (0, 10) to (0, 20),
         * across y=20 to (50, 20), down x=50 to (50, 30), across y=30 to
         * (100, 30), and then straight down to the sink port at (100, 40).
         * Note that the first and last segments are merely inferred by the
         * simplest path from the lines described in this list to the source
         * and sink points.  The source and sink port points are obtained from
         * their respective port objects and are not included in this list.
         */
    protected Integer[] m_path_definition;

        /** Used to define the direction of dog-leg connections. */
    protected int m_dog_leg_hint;

        /** A port, junction, or bundler. */
    protected GenericPortProxy m_source;

        /**
         * The member node proxy associated with the source port.  This is
         * necessary because a single PortProxy can be used to represent
         * more than one port in a container if multiple instances of the
         * same member node reside in the container node.  This is set to
         * null if the source port is not on a member node in this container.
         */
    protected MemberNodeProxy m_source_member;

        /** A port, junction, or bundler. */
    protected GenericPortProxy m_sink;

        /**
         * The member node proxy associated with the sink.  See notes on
         * m_source_member.
         */
    protected MemberNodeProxy m_sink_member;

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected HashMap m_proxy_change_listener_map;

        /** Used in conjunction with ContainedElementModEventSource. */
    protected HashMap m_contained_element_mod_listeners_map;

        /** Used in conjunction with UpstreamDataChannelChangeEventSource. */
    protected HashMap m_upstream_channel_change_listeners_map;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Main constructor.  Should only be called from ALFINode. */
    ConnectionProxy(ALFINode _container_node,
                    GenericPortProxy _source, MemberNodeProxy _source_member,
                    GenericPortProxy _sink, MemberNodeProxy _sink_member,
                    Integer[] _path_definition) {
        assert(_container_node != null);
        assert(_source != null);
        assert(_sink != null);

            // final member initializations
        m_direct_container_node = _container_node;

            // other members
        m_source = _source;
        m_source_member = _source_member;
        m_sink = _sink;
        m_sink_member = _sink_member;
        m_path_definition =
                (_path_definition != null) ? _path_definition : new Integer[0];
        m_dog_leg_hint = DOG_LEG_HINT_NONE;

        m_proxy_change_listener_map = new HashMap();
        m_contained_element_mod_listeners_map = new HashMap();
        m_upstream_channel_change_listeners_map = new HashMap();

            // if this new connection is attached to a port on a member node,
            // then register it as an external connection to this port.
        if (_source instanceof PortProxy) {
            PortProxy source_port = (PortProxy) _source;
            if (! _container_node.port_contains(source_port) && 
                  source_port.getIOType() != PortProxy.IO_TYPE_INPUT) {
                source_port.registerExternalConnection(this);
            }
        }
        if (_sink instanceof PortProxy) {
            PortProxy sink_port = (PortProxy) _sink;
            if (! _container_node.port_contains(sink_port) &&
                  sink_port.getIOType() != PortProxy.IO_TYPE_OUTPUT) {
                sink_port.registerExternalConnection(this);
            }
        }
    }

        /**
         * This constructor should only be called by a JunctionProxy in the
         * creation of its connection-from-source.
         */
    ConnectionProxy(JunctionProxy _sink_junction, GenericPortProxy _source,
                  MemberNodeProxy _source_member, Integer[] _path_definition) {
            // final member initializations
        m_direct_container_node = _sink_junction.getDirectContainerNode();

            // other members
        m_source = _source;
        m_source_member = _source_member;
        m_sink = _sink_junction;
        m_sink_member = null;
        m_path_definition =
                (_path_definition != null) ? _path_definition : new Integer[0];
        m_dog_leg_hint = DOG_LEG_HINT_NONE;

        m_proxy_change_listener_map = new HashMap();
        m_contained_element_mod_listeners_map = new HashMap();
        m_upstream_channel_change_listeners_map = new HashMap();

            // if this new connection is attached to a port on a member node,
            // then register it as an external connection to this port.
        if (_source instanceof PortProxy) {
            PortProxy source_port = (PortProxy) _source;
            if (! m_direct_container_node.port_contains(source_port)) {
                source_port.registerExternalConnection(this);
            }
        }
    }

        /** Auxilliary constructor which creates a proxy for temporary storage
          * of ConnectionProxy information only.  These objects are useless
          * elsewhere because the (final) container node field is null.
          */
    public ConnectionProxy(GenericPortProxy _source,
                MemberNodeProxy _source_member, GenericPortProxy _sink,
                MemberNodeProxy _sink_member, Integer[] _path_definition,
                Integer _dog_leg_hint) {

            // set all final fields to null
        m_direct_container_node = null;

            // others
        m_source = _source;
        m_source_member = _source_member;
        m_sink = _sink;
        m_sink_member = _sink_member;
        m_path_definition = _path_definition;
        m_dog_leg_hint = (_dog_leg_hint != null) ?
                                  _dog_leg_hint.intValue() : DOG_LEG_HINT_NONE;

        m_proxy_change_listener_map = null;
        m_contained_element_mod_listeners_map = null;
    }

        /** Another auxilliary constructor, used to easily create temporary
          * bundle generated connections which are used only to write themselves
          * in a box file.
          */
    public ConnectionProxy(PortProxy _source, MemberNodeProxy _source_member,
                  String _channel_path_at_source, PortProxy _sink,
                  MemberNodeProxy _sink_member, String _channel_path_at_sink) {

        this(

                 // source port (possibly "virtual")
             ((_channel_path_at_source == null) ? _source : new PortProxy(
                         _source.getName() + "__" + _channel_path_at_source,
                                                            null, null, null)),

             _source_member,

                 // sink port (possibly "virtual")
             ((_channel_path_at_sink == null) ? _sink : new PortProxy(
                             _sink.getName() + "__" + _channel_path_at_sink,
                                                            null, null, null)),

             _sink_member,

             null,

             null);
    }

    //**************************************************************************
    //***** static methods *****************************************************

        /** Given a set of path points for a connection, creates the path
          * definition.  The information about the source and sink points is
          * thrown away, as that information is stored with the port objects.
          * The port objects, combined with this path definition holds the
          * minimum set of information needed to determine the path.  The set of
          * path points which results actually holds redundant information, so
          * the points themselves are not used to hold the path information to
          * prevent the possibility of corrupt/inconsistant data.
          *
          * In cases were 3 or fewer VALID path points (see validatePathPoints)
          * are passed, null will be returned in the path definition object
          * because the straight line or dog-leg path represented is dependent
          * only on the start and end points (not info stored in a path defn),
          * and possibly a dog-leg hint to say whether it would bend left/right.
          *
          * The Integer[2][] returned is { Integer[n] path_definition,
          *                                Integer[1] dog_leg_hint }.
          */
    public static Integer[][] pathPointsToPathDefinition(Vector _path_points) {
        if (_path_points == null) {
            Alfi.warn("ConnectionProxy.pathPointsToPathDefinition(" +
                      _path_points + "): " + "Invalid call (1).");
            return null;
        }

            // the second "Integer[]" always has only a single element
        Integer[][] path_def_and_hint = { null, new Integer[1] };
        path_def_and_hint[1][0] = new Integer(DOG_LEG_HINT_NONE);

        Vector valid_points = validatePathPoints(_path_points);
        if ((valid_points == null) || (valid_points.size() < 2)) {
            Alfi.warn("ConnectionProxy.pathPointsToPathDefinition(" +
                      _path_points + "): " + "Invalid call (2).");
            return path_def_and_hint;
        }
        else if (valid_points.size() > 3) {
                // general path, not straight, not dog-leg

                // this is line count in the definition, not a count of the
                // segments in the path (of which there will be 1 or 2 more)
            int line_count = valid_points.size() - 3;

            valid_points.remove(0);    // source point, not used in defintn
                // due to how definition is stored, 1st line is assumed to
                // be vertical, but if not, then 1st element set to null.
            boolean b_first_line_is_vertical =
                                (    ((Point) valid_points.get(0)).x ==
                                     ((Point) valid_points.get(1)).x    );
            int i = 0;
            Integer[] path_definition = null;
            if (! b_first_line_is_vertical) {
                valid_points.set(0, new Object());    // dummy, not accessed
                path_definition = new Integer[line_count + 1];
                path_definition[i] = null;
                i++;
            }
            else { path_definition = new Integer[line_count]; }

                // after all this, i corresponds to both the next element
                // to be filled in in the path definition, and to the end
                // point of the related segment in the path points vector
            for ( ; i < path_definition.length; i++) {
                if ((i % 2) == 0) {    // vertical line in definition
                    path_definition[i] =
                                new Integer(((Point) valid_points.get(i)).x);
                }
                else {                 // horizontal line in definition
                    path_definition[i] =
                                new Integer(((Point) valid_points.get(i)).y);
                }
            }

            path_def_and_hint[0] = path_definition;
        }
        else if (valid_points.size() == 3) {
                // dog-leg shape
            Point[] path_points = new Point[3];
            valid_points.toArray(path_points);
            int hint = dog_leg__vertex_to_hint(path_points[0],
                                               path_points[2], path_points[1]);
            path_def_and_hint[1][0] = new Integer(hint);
        }
        // else if valid_points.size() == 2 gives straight line and the
        // required return value for path_def_and_hint is already set...

        return path_def_and_hint;
    }

        /**
         * This method takes a string specification of a path definition
         * (e.g., "x=24 y=68 x=36") and returns the path definition.  null is
         * returned if _string_spec does not have the correct format.
         */
    public static Integer[] parseStringPathDefinition(String _string_spec) {
        StringTokenizer st = new StringTokenizer(_string_spec);
        Integer[] path_definition = new Integer[st.countTokens()];
        for (int i = 0; i < path_definition.length; i++) {
            String line_def = st.nextToken();
            StringTokenizer st_2 = new StringTokenizer(line_def, "=");
            if (st_2.countTokens() == 2) {
                String coord_name = st_2.nextToken();
                String value = st_2.nextToken();
                if (coord_name.equals("x") || coord_name.equals("y")) {
                    if (value.equals("null")) {
                        path_definition[i] = null;
                    }
                    else {
                        try {
                            path_definition[i] = new Integer(value);
                        }
                        catch(NumberFormatException e) { return null; }
                    }
                }
                else { return null; }
            }
            else { return null; }
        }
        return path_definition;
    }

        /**
         * Makes sure that a set of path points checks out as plotting an
         * orthogonal route, and that only corner points are represented.
         * Null is returned if the points don't plot an orthogonal set of
         * segments, and a modified vector of points is returned if redundant
         * (non-corner) points are removed.  If the path passed is fine, it
         * is merely returned itself.
         *
         * Also, if the path is found to cross or overlap itself, this is
         * cleaned up as well.
         */
    protected static Vector validatePathPoints(Vector _path_points) {
        assert(_path_points != null);

            // clean up simple redundancies first
        _path_points = validatePath_removeRedundancies(_path_points);
        if (_path_points.size() < 2 ) { return null; }
        if (_path_points.size() == 2) {
            Point p_0 = (Point) _path_points.get(0);
            Point p_1 = (Point) _path_points.get(1);
            if ((p_0.x == p_1.x) || (p_0.y == p_1.y)) {    // horiz or vert?
                return _path_points;
            }
            else { return null; }
        }

        Point previous = null;
        Point p = null;
        Point next = null;
        for (int i = 1; i < (_path_points.size() - 1); i++) {
            previous = (Point) _path_points.get(i - 1);
            p = (Point) _path_points.get(i);
            next = (Point) _path_points.get(i + 1);
                // is p in line with previous and next?  if so, remove it.
            if (        ((p.x == previous.x) && (p.x == next.x)) ||
                        ((p.y == previous.y) && (p.y == next.y))  ) {
                _path_points.remove(i);
                i--;    // do it over at same position in vector
            }
                // for orthogonality, p and previous must share one coordinate
            else if (! ((p.x == previous.x) || (p.y == previous.y)) ) {
                return null;    // orthogonality requirement broken
            }
        }

            // check last orthogonality
        if (! ((next.x == p.x) || (next.y == p.y)) ) {
            return null;    // orthogonality requirement broken
        }

            // now check for and dispose of any extraneous loops where the path
            // crosses over itself.
        Line2D[] segments = new Line2D[_path_points.size() - 1];
        for (int i = 0; i < segments.length; i++) {
            segments[i] = new Line2D.Double((Point) _path_points.elementAt(i),
                                         (Point) _path_points.elementAt(i + 1));
        }
            // counts on adjacent segments being non-overlying (why j = i + 2)
        boolean b_intersection_occurred = false;
        for (int i = 0; i < segments.length - 1; i++) {
            if (segments[i] == null) { continue; }
            for (int j = i + 2; j < segments.length; j++) {
                if (segments[i].intersectsLine(segments[j])) {
                    b_intersection_occurred = true;
                    Rectangle2D intersection = segments[i].getBounds2D().
                                 createIntersection(segments[j].getBounds2D());
                    Point crossing = intersection.getBounds().getLocation();

                        // delete loop
                    Point2D p1 = segments[i].getP1();
                    Point2D p2 = crossing;
                    if (! p1.equals(p2)) { segments[i].setLine(p1, p2); }
                    else { segments[i] = null; }

                    p1 = crossing;
                    p2 = segments[j].getP2();
                    if (! p1.equals(p2)) { segments[j].setLine(p1, p2); }
                    else { segments[j] = null; }

                    for (int k = i + 1; k < j; k++) { segments[k] = null; }
                }
            }
        }
        if (b_intersection_occurred) {
            Vector trimmed_path = new Vector();
            for (int i = 0; i < segments.length; i++) {
                if (segments[i] != null) {
                    if (trimmed_path.isEmpty()) {
                            // start point
                        trimmed_path.add(new Point((int) segments[i].getX1(),
                                                   (int) segments[i].getY1()));
                    }
                        // following points
                    trimmed_path.add(new Point((int) segments[i].getX2(),
                                               (int) segments[i].getY2()));
                }
            }
            return trimmed_path;
        }
        else { return _path_points; }
    }

        /** This method looks at a vector of path points and removes any
          * sections which lead back to a point already existing earlier in the
          * path.  E.g., (0, 0), (0, 10), (10, 10), (0, 10), (0, 20) would
          * result in just (0, 0), (0, 10), (0, 20) (which would itself be
          * reduced, but not in this method.)
          */
    private static Vector validatePath_removeRedundancies(Vector _path_points) {
        int initial_size;
        do {
            initial_size = _path_points.size();

            OUT:
            for (int i = 0; i < _path_points.size() - 1; i++) {
                Point point_i = (Point) _path_points.get(i);
                for (int j = i + 1; j < _path_points.size(); j++) {
                    if (((Point) _path_points.get(j)).equals(point_i)) {
                        for (int k = j; k > i; k--) {
                            _path_points.remove(k);
                        }
                        break OUT;
                    }
                }
            }
        } while (_path_points.size() < initial_size);

        return _path_points;
    }

        /** Checks that all the prerequisites for the viability of a particular
          * connection are in place.  This may be used either as a check before
          * building the connection in question or as a check that something
          * needed by an already built connection has or hasn't been removed.
          * In the former case, usually _b_complain would be set to true, and
          * in the latter, false, as it is valid for a port to be removed, thus
          * implicitly removing a connection linked to it.
          */
    public static boolean prerequisitesExist(ALFINode _container,
                 GenericPortProxy _source, MemberNodeProxy _source_port_member,
                 GenericPortProxy _sink, MemberNodeProxy _sink_port_member,
                 boolean _b_complain) {

            // _source and _sink must be non-null
        if ((_source == null) || (_sink == null)) {
            if (_b_complain) { Alfi.warn("Source and/or sink are null"); }
            return false;
        }

            // check source
        String problem = prerequisitesExist(_container, _source,
                                            _source_port_member, SOURCE_TEXT);

        if (problem == null) {
                // check sink
            problem = prerequisitesExist(_container, _sink,
                                         _sink_port_member, SINK_TEXT);
        }

        if (problem != null) {
            if (_b_complain) {
                Alfi.warn("ConnectionProxy.prerequisitesExist:\n\t" + problem);
            }
            return false;
        }

        return true;
    }

        /** If a prerequisite is missing, returns a string message, otherwise
          * null is returned.
          */
    protected static String prerequisitesExist(
                ALFINode _container, GenericPortProxy _terminal,
                MemberNodeProxy _terminal_member, String _terminal_desc) {
        String complaint = null;
        if (_terminal_member == null) {
            if (! _container.genericPort_contains(_terminal)) {
                complaint = "generic port [" + _terminal +
                                            "] not contained in " + _container;
            }
        }
        else {
            if (! _container.memberNodeProxy_contains(_terminal_member)) {
                complaint = "port member [" + _terminal_member +
                                            "] not contained in " + _container;
            }
            else if (_terminal instanceof PortProxy) {
                ALFINode member_node =
                      _terminal_member.getAssociatedMemberNode(_container);
                if (! member_node.port_contains((PortProxy) _terminal)) {
                    complaint = "port [" + _terminal + "] not contained in " +
                                                                   member_node;
                }
            }
            else { complaint = "object type not recognized."; }
        }

        if (complaint != null) { complaint = _terminal_desc + complaint; }

        return complaint;
    }

        /** Tries to determine which default (i.e., with no knowledge of user
          * choices to set the dog-leg hint otherwise) direction the connection
          * should point at both the start and end of the connection where it is
          * connected to * the source and sink terminals.  Beware that the sink
          * direction is the pointing of the connection INTO the sink terminal.
          */
    public static int[] getDefaultStartAndEndDirections(ALFINode _container,
                     GenericPortProxy _source, MemberNodeProxy _source_member,
                     GenericPortProxy _sink, MemberNodeProxy _sink_member) {
        int source_start = DIRECTION_ANY;
        if (_source instanceof PortProxy) {
            boolean b_on_member = (_source_member != null);
            int edge = ((PortProxy) _source).determineEdgeAfterAllOps(
                                                   _container, _source_member);
            switch (edge) {
              case PortProxy.EDGE_NORTH:
                source_start = b_on_member ? DIRECTION_NORTH : DIRECTION_SOUTH;
                break;
              case PortProxy.EDGE_SOUTH:
                source_start = b_on_member ? DIRECTION_SOUTH : DIRECTION_NORTH;
                break;
              case PortProxy.EDGE_EAST:
                source_start = b_on_member ? DIRECTION_EAST : DIRECTION_WEST;
                break;
              case PortProxy.EDGE_WEST:
                source_start = b_on_member ? DIRECTION_WEST : DIRECTION_EAST;
                break;
            }
        }

        int sink_end = DIRECTION_ANY;
        if (_sink instanceof PortProxy) {
            boolean b_on_member = (_sink_member != null);
            int edge = ((PortProxy) _sink).determineEdgeAfterAllOps(
                                                     _container, _sink_member);
            switch (edge) {
              case PortProxy.EDGE_NORTH:
                sink_end = b_on_member ? DIRECTION_SOUTH : DIRECTION_NORTH;
                break;
              case PortProxy.EDGE_SOUTH:
                sink_end = b_on_member ? DIRECTION_NORTH : DIRECTION_SOUTH;
                break;
              case PortProxy.EDGE_EAST:
                sink_end = b_on_member ? DIRECTION_WEST : DIRECTION_EAST;
                break;
              case PortProxy.EDGE_WEST:
                sink_end = b_on_member ? DIRECTION_EAST : DIRECTION_WEST;
                break;
            }
        }

        int[] start_and_end_directions = { source_start, sink_end };
        return start_and_end_directions;
    }

    public static Point dog_leg__hint_to_vertex(Point _source, Point _sink,
                                                                   int _hint) {
        Point vertex = null;
        if (_sink.x > _source.x) {
            if (_sink.y < _source.y) {
                vertex = (_hint == DOG_LEG_HINT_CCW) ?
                                                new Point(_sink.x, _source.y) :
                                                new Point(_source.x, _sink.y);
            }
            else {
                vertex = (_hint == DOG_LEG_HINT_CCW) ?
                                                new Point(_source.x, _sink.y) :
                                                new Point(_sink.x, _source.y);
            }
        }
        else {
            if (_sink.y < _source.y) {
                vertex = (_hint == DOG_LEG_HINT_CCW) ?
                                                new Point(_source.x, _sink.y) :
                                                new Point(_sink.x, _source.y);
            }
            else {
                vertex = (_hint == DOG_LEG_HINT_CCW) ?
                                                new Point(_sink.x, _source.y) :
                                                new Point(_source.x, _sink.y);
            }
        }
        return vertex;
    }

    public static int dog_leg__vertex_to_hint(Point _source, Point _sink,
                                                               Point _vertex) {
        int hint = DOG_LEG_HINT_NONE;
        if (_vertex.y < _source.y) {
            if (_sink.x < _vertex.x) {
                hint = DOG_LEG_HINT_CCW;
            }
            else if (_sink.x > _vertex.x) { hint = DOG_LEG_HINT_CW; }
        }
        else if (_vertex.y > _source.y) {
            if (_sink.x < _vertex.x) {
                hint = DOG_LEG_HINT_CW;
            }
            else if (_sink.x > _vertex.x) { hint = DOG_LEG_HINT_CCW; }
        }
        else if (_vertex.x < _source.x) {
            if (_sink.y < _vertex.y) {
                hint = DOG_LEG_HINT_CW;
            }
            else if (_sink.y > _vertex.y) { hint = DOG_LEG_HINT_CCW; }
        }
        else if (_vertex.x > _source.x) {
            if (_sink.y < _vertex.y) {
                hint = DOG_LEG_HINT_CCW;
            }
            else if (_sink.y > _vertex.y) { hint = DOG_LEG_HINT_CW; }
        }
        return hint;
    }

    //**************************************************************************
    //***** methods ************************************************************

        /** Checks that all the prerequisites for the viability of this
          * connection are in place.
          */
    public boolean prerequisitesExist(ALFINode _container, boolean _b_complain){
        return prerequisitesExist(_container, getSource(), getSourceMember(),
                                      getSink(), getSinkMember(), _b_complain);
    }

        /** The source is not final and may be reset to facilitate the placement
          * (or removal) of junctions/bundlers somewhere along the connection.
          * The parameter _new_source_owner_member is passed as null if the
          * source is either an external port or a junction (i.e., whenever
          * there is no member node associated with the source.
          */
    public void setSource(ALFINode _container_node,
                          GenericPortProxy _new_source,
                          MemberNodeProxy _new_source_owner_member,
                          Integer[] _new_path_definition) {
        assert(_container_node != null);
        assert(_new_source != null);
        String warning = "(" + this + ").setSource(" + _container_node + ", " +
                         _new_source + ", " + _new_source_owner_member + ", " +
                         _new_path_definition + "):\n";

        boolean b_change_made = false;
        if (_new_source_owner_member == null) {
            if (_container_node.genericPort_contains(_new_source)) {
                if (m_source != _new_source) {
                    m_source = _new_source;
                    m_source_member = null;
                    b_change_made = true;
                }
            }
            else {
                Alfi.warn(warning + "Generic port [" + _new_source + "] not " +
                         "found in container node [" + _container_node + "].");
            }
        }
        else {
            if (_container_node.memberNodeProxy_contains(
                                                   _new_source_owner_member)) {
                assert(_new_source instanceof PortProxy);
                PortProxy source_port = (PortProxy) _new_source;
                ALFINode member_node = _container_node.memberNode_get(
                                                     _new_source_owner_member);
                if (member_node.port_contains(source_port)) {
                    if ((m_source != source_port) ||
                               (m_source_member != _new_source_owner_member)) {
                        m_source = source_port;
                        m_source_member = _new_source_owner_member;
                        b_change_made = true;
                    }   
                }
                else {
                    Alfi.warn(warning + "Port [" + source_port + "] not " +
                       "found in member [" + _new_source_owner_member + "].");
                }
            }
            else {
                Alfi.warn(warning + "Member [" + _new_source_owner_member +
                          "] " + "not found in container node [" +
                          _container_node + "].");
            }
        }

        if (b_change_made) {
            setPathDefinition(_new_path_definition, false);

                // fire event signalling that this proxy has changed
            ConnectionProxy cp_temp = new ConnectionProxy(
                m_source, m_source_member, null, null, m_path_definition, null);
            ProxyChangeEvent e = new ProxyChangeEvent(this, cp_temp);
            fireProxyChangeEvent(e);

            m_direct_container_node.setTimeLastModified();
        }
    }

        /** Returns null if neither source nor sink are bundlers. */
    public TerminalBundlersConfiguration
                    generateCurrentTerminalBundlersConfigurationState(
                                                         ALFINode _container) {
        BundlerProxy source_bundler = (getSource() instanceof BundlerProxy) ?
                                           ((BundlerProxy) getSource()) : null;
        BundlerProxy sink_bundler = (getSink() instanceof BundlerProxy) ?
                                           ((BundlerProxy) getSink()) : null;

        if ((source_bundler != null) || (sink_bundler != null)) {
            boolean b_is_primary_out = isSourceBundlerPrimaryOut();
            boolean b_is_primary_in = isSinkBundlerPrimaryIn();

            HashMap output_name_map = (source_bundler == null) ? null :
                     source_bundler.getSecondaryOutputChannelNames(_container);

            String input_name = (sink_bundler == null) ? null :
                                sink_bundler.getSecondaryInputName(_container);
            boolean b_input_unwraps = (sink_bundler == null) ? false :
                                sink_bundler.secondaryInputUnwraps(_container);

            return new TerminalBundlersConfiguration(b_is_primary_out,
                output_name_map, b_is_primary_in, input_name, b_input_unwraps);
        }
        else { return null; }
    }

        /** Always false unless overridden in subclass. */
    public boolean isSourceBundlerPrimaryOut() { return false; }

        /** Always false unless overridden in subclass. */
    public boolean isSinkBundlerPrimaryIn() { return false; }

        /** This method test to see if, in the node _container, this connection
          * is one of the connections which _connection uses to trace a route
          * back to its origin port.  I.e., if there are a series of connections
          * joined by junctions, does _connection need THIS connection to be in
          * place in order to get back to _connection's origin port?
          */
    public boolean isAnUpstreamSourceConnectionFor(ALFINode _container,
                                                 ConnectionProxy _connection) {
        GenericPortProxy source = _connection.getSource();
        while (source instanceof JunctionProxy) {
            JunctionProxy source_junction = (JunctionProxy) source;
            ConnectionProxy source_junctions_source_connection =
                               source_junction.getSourceConnection(_container);
            if (source_junctions_source_connection == null) {
                return false;
            }
            else if (source_junctions_source_connection == this) {
                return true;
            }
            else {
                source = source_junctions_source_connection.getSource();
            }
        }
        return false;
    }

        /** Returns the node which locally contains this connection. */
    public ALFINode getDirectContainerNode() { return m_direct_container_node; }

        /** Returns the source port or junction. */
    public GenericPortProxy getSource() { return m_source; }

        /** Returns the sink port or junction. */
    public GenericPortProxy getSink() { return m_sink; }

        /** Returns the member which owns the source port (or null, if none.) */
    public MemberNodeProxy getSourceMember() { return m_source_member; }

        /** Returns the member which owns the sink port (or null, if none.) */
    public MemberNodeProxy getSinkMember() { return m_sink_member; }

        /** Returns the PortProxy or BundlerProxy which is the origin of a
          * connection or series of connections via junctions in the node
          * _container.  Returns null if no such origin exists.
          */
    public GenericPortProxy getOrigin(ALFINode _container) {
        GenericPortProxy source = getSource();
        while (source instanceof JunctionProxy) {
            ConnectionProxy connection =
                 ((JunctionProxy) source).getSourceConnection(_container);
            if (connection != null) { source = connection.getSource(); }
            else { return null; }
        }

        if ((source instanceof PortProxy) || (source instanceof BundlerProxy)) {
            return source;
        }
        else { return null; }
    }

        /** Returns the member which contains the origin port of a connection or
          * series of connections via junctions (in the node _container.)  Null
          * if there is no origin port currently, or if origin port is an
          * external port not associated with a member node in the node which
          * contains this connection.
          */
    public MemberNodeProxy getOriginMember(ALFINode _container) {
        GenericPortProxy source = getSource();
        ConnectionProxy connection = this;
        while (source instanceof JunctionProxy) {
            connection =
                      ((JunctionProxy) source).getSourceConnection(_container);
            if (connection != null) { source = connection.getSource(); }
            else { return null; }
        }
        return connection.getSourceMember();
    }

        /** This method compares a test path definition with the current path
          * definition in this ConnectionProxy.
          */
    public boolean hasEquivalentPathDefinition(Integer[] _test_path_definition){
        if ((m_path_definition == null) && (_test_path_definition == null)) {
            return true;
        }
        else if ((m_path_definition == null)||(_test_path_definition == null)) {
            return false;
        }
        else if (m_path_definition.length == _test_path_definition.length) {
            for (int i = 0; i < m_path_definition.length; i++) {
                if (! m_path_definition[i].equals(_test_path_definition[i])) {
                    return false;
                }
            }
            return true;
        }
        else { return false; }
    }

        /** Returns a copy of the path definition.  As defined in the comments
          * on the field m_path_definition.
          */
    public Integer[] getPathDefinition() {
        if (m_path_definition == null) { return null; }
        else {
            Integer[] copy = new Integer[m_path_definition.length];
            for (int i = 0; i < copy.length; i++) {
                copy[i] = (m_path_definition[i] == null) ? null :
                                  new Integer(m_path_definition[i].intValue());
            }
            return copy;
        }
    }

        /** Sets the path definition, and triggers a ProxyChangeEvent.  The
          * path definition must be in the form as described in the comments on
          * the field m_path_definition.
          */
    public void setPathDefinition(Integer[] _path_definition) {
        setPathDefinition(_path_definition, true);
    }

        /** Private form of this method may be executed without triggering a
          * ProxyChangeEvent.  This is done by methods which will eventually
          * fire such an event themselves.
          */
    protected void setPathDefinition(Integer[] _path_definition,
                                         boolean _b_fires_proxy_change_event) {
        if (_path_definition == null) { _path_definition = new Integer[0]; }

        boolean b_path_changes_detected = false;
        if (_path_definition.length != m_path_definition.length) {
            b_path_changes_detected = true;
        }
        else if (_path_definition.length > 0) {
                // the first element is allowed to be null
            Integer I1 = _path_definition[0];
            Integer I2 = m_path_definition[0];
            if (I1 == null) {
                if (I2 != null) { b_path_changes_detected = true; }
            }
            else if (I2 == null) { b_path_changes_detected = true; }
            else if (! I1.equals(I2)) { b_path_changes_detected = true; }

            if (! b_path_changes_detected) {
                for (int i = 1; i < _path_definition.length; i++) {
                    if (! _path_definition[i].equals(m_path_definition[i])) {
                        b_path_changes_detected = true;
                        break;
                    }
                }
            }
        }

        if (b_path_changes_detected) {
            m_path_definition = new Integer[_path_definition.length];

                // first element may be null.  deal with that possibility.
            int i = 0;
            if ((_path_definition.length > 0) && (_path_definition[0] == null)){
                m_path_definition[0] = null;
                i++;
            }
                
            for ( ; i < m_path_definition.length; i++) {
                m_path_definition[i] =
                        new Integer(_path_definition[i].intValue());
            }

                // fire event signalling that this proxy has changed
            if (_b_fires_proxy_change_event) {
                ConnectionProxy cp_temp = new ConnectionProxy(null, null,
                                           null, null, _path_definition, null);
                ProxyChangeEvent e = new ProxyChangeEvent(this, cp_temp);
                fireProxyChangeEvent(e);
            }

            m_direct_container_node.setTimeLastModified();
        }
    }

    public void setDogLegHint(int _hint, boolean _b_fires_proxy_change_event) {
        if ((_hint == DOG_LEG_HINT_NONE) || (_hint == DOG_LEG_HINT_CW) ||
                                                 (_hint == DOG_LEG_HINT_CCW)) {
            if (_hint != m_dog_leg_hint) {
                m_dog_leg_hint = _hint;
                if (_b_fires_proxy_change_event) {
                    ConnectionProxy cp_temp = new ConnectionProxy(null, null,
                                null, null, null, new Integer(m_dog_leg_hint));
                    ProxyChangeEvent e = new ProxyChangeEvent(this, cp_temp);
                    fireProxyChangeEvent(e);
                }

                m_direct_container_node.setTimeLastModified();
            }
        }
    }

    public int getDogLegHint() { return m_dog_leg_hint; }

        /** Given the source and sink points for this connection, uses the path
          * definition to calculate the points that the connection follows from
          * corner point to corner point along the orthogonal path.  The source
          * and sink points, along with the path definition hold the minimum
          * set of information needed to determine the path.  The set of path
          * points which results actually holds redundant information, so the
          * points themselves are not used to hold the path information to
          * prevent the possibility of corrupt/inconsistant data.
          */
    public Vector calculatePathPoints(ALFINode _container, Point _source_point,
                                                           Point _sink_point) {
        Vector path_points = new Vector();
        if (m_path_definition.length > 0) {
                // the 1st value (an x value) may be null
            int i = (m_path_definition[0] != null) ? 0 : 1;

            path_points.add(new Point(_source_point));
            Point previous = _source_point;

            Point p = null;
            for ( ; i < m_path_definition.length; i++) {
                if ((i % 2) == 0) {    // lines defined as  x = val
                    p = new Point(m_path_definition[i].intValue(), previous.y);
                }
                else {                 // lines defined as  y = val
                    p = new Point(previous.x, m_path_definition[i].intValue());
                }
                if (! p.equals(previous)) {
                    path_points.add(p);
                    previous = p;
                }
            }

            if ((i % 2) == 0) { p = new Point(_sink_point.x, previous.y); }
            else { p = new Point(previous.x, _sink_point.y); }
            if (! p.equals(previous)) {
                path_points.add(p);
            }

            if (! _sink_point.equals((Point) path_points.lastElement())) {
                path_points.add(new Point(_sink_point));
            }
        }
        else if (m_dog_leg_hint == DOG_LEG_HINT_NONE) {
                // create a default path
            Point[] mid_points = getDefaultMidPoints(_container,
                                                   _source_point, _sink_point);
            path_points.add(new Point(_source_point));
            for (int i = 0; i < mid_points.length; i++) {
                path_points.add(mid_points[i]);
            }
            path_points.add(new Point(_sink_point));
        }
        else {
                // explicit dog-leg path
            Point vertex = dog_leg__hint_to_vertex(_source_point, _sink_point,
                                                               m_dog_leg_hint);
            path_points.add(new Point(_source_point));
            path_points.add(vertex);
            path_points.add(new Point(_sink_point));
        }

        return path_points;
    }

    private Point[] getDefaultMidPoints(ALFINode _container,
                                      Point _source_point, Point _sink_point) {
        int[] startAndEndDirections =
                                   getDefaultStartAndEndDirections(_container);
        int start = startAndEndDirections[0];
        int end = startAndEndDirections[1];
        ArrayList point_list = new ArrayList();
        if (isStraightLine(_source_point, _sink_point)) {
            ;  // add no mid points
        }
        else if (isDogLeg(startAndEndDirections)) {
            Point vertex = null;
            if ((start == DIRECTION_NORTH) || (start == DIRECTION_SOUTH) ||
                          (end == DIRECTION_EAST) || (end == DIRECTION_WEST)) {
                vertex = new Point(_source_point.x, _sink_point.y);
            }
            else { vertex = new Point(_sink_point.x, _source_point.y); }
            point_list.add(vertex);
        }
        else {
                // straight line and dog-leg have been discounted
            Point p1 = null;
            Point p2 = null;
            if ((start == DIRECTION_NORTH) || (start == DIRECTION_SOUTH)) {
                int mid = (_source_point.y + _sink_point.y) / 2;
                p1 = new Point(_source_point.x, mid);
                p2 = new Point(_sink_point.x, mid);
            }
            else {
                int mid = (_source_point.x + _sink_point.x) / 2;
                p1 = new Point(mid, _source_point.y);
                p2 = new Point(mid, _sink_point.y);
            }
            point_list.add(p1);
            point_list.add(p2);
        }

        Point[] points = new Point[point_list.size()];
        point_list.toArray(points);
        return points;
    }

    private int[] getDefaultStartAndEndDirections(ALFINode _container) {
        return getDefaultStartAndEndDirections(_container,
                             m_source, m_source_member, m_sink, m_sink_member);
    }

    private boolean isStraightLine(Point _source_point, Point _sink_point) {
        return ((m_path_definition.length == 0) &&
                                         ((_source_point.x == _sink_point.x) ||
                                          (_source_point.y == _sink_point.y)));
    }

        /** Assumes straight line has already been discounted. */
    private boolean isDogLeg(int[] _startAndEndDirections) {
        int start = _startAndEndDirections[0];
        int end = _startAndEndDirections[1];
        if ((start == DIRECTION_ANY) || (end == DIRECTION_ANY)) {
            return true;
        }
        else if ((start == DIRECTION_NORTH) || (start == DIRECTION_SOUTH)) {
            return ((end != DIRECTION_NORTH) && (end != DIRECTION_SOUTH));
        }
        else {
            return ((end != DIRECTION_EAST) && (end != DIRECTION_WEST));
        }
    }

        /** Translates whole path definition by (_dx, _dy). */
    public void translate(int _dx, int _dy) {
        Integer[] path_definition = getPathDefinition();    // gets copy
        if ((path_definition != null) && (path_definition.length > 0)) {
            if (_dx != 0) {
                for (int i = ((path_definition[0] != null) ? 0 : 2);
                                        i < path_definition.length; i += 2) {
                    path_definition[i] =
                            new Integer(path_definition[i].intValue() + _dx);
                }
            }

            if (_dy != 0) {
                for (int i = 1; i < path_definition.length; i += 2) {
                    path_definition[i] =
                            new Integer(path_definition[i].intValue() + _dy);
                }
            }
        }

        setPathDefinition(path_definition);
    }

        /** Generate non-GUI parser representation (if any) of this connection.
          * If the sink of this connection is a JunctionProxy, it should only
          * have a GUI parser representation.  A null is returned from this
          * method in such cases.
          */
    public String generateParserRepresentation(ALFINode _container) {
        GenericPortProxy origin = getOrigin(_container);
        if (origin != null) {
            if ((m_sink instanceof PortProxy) &&
                                          ! (origin instanceof BundlerProxy)) {
                String[] origin_names = determineOriginNames(_container);
                String[] sink_names = determineSinkNames();

                return (origin_names[1] + " " + origin_names[0] +
                                 _ARROW_ + sink_names[1] + " " + sink_names[0]);
            }
        }

        return null;
    }

        /** This method returns null except in cases where the sink is a
          * PortProxy, this connection is not a bundle (see overriding method
          * in BundleProxy- it just returns null immediately), and the origin
          * (not only source) is a bundler.  The returned value is a temporary
          * connection appropriate for placing in the modeler-only sections of
          * a box file, specifically in the Add_Connections and
          * Remove_Connections sections.  If the channel associated with this
          * secondary output from the origin bundle is traced back to anything
          * other than a PortProxy (including BundlePortProxys), then null will
          * be returned (and is not an indication of any error.)
          */
    public ConnectionProxy generateModelerSuitableConnection(ALFINode
                                                                  _container) {
       if (m_sink instanceof PortProxy) {
           PortProxy sink = (PortProxy) m_sink;
           GenericPortProxy origin_gpp = getOrigin(_container);
           if (origin_gpp instanceof BundlerProxy) {
               BundlerProxy origin_bundler = (BundlerProxy) origin_gpp;
               Object[] source_info = traceChannelBackToPort(_container, null);
               if (source_info != null) {
                   PortProxy source_port = (PortProxy) source_info[0];
                   MemberNodeProxy source_owner =
                                              (MemberNodeProxy) source_info[1];
                   String channel_path_at_source = (String) source_info[2];
                   return new ConnectionProxy(source_port, source_owner,
                           channel_path_at_source, sink, getSinkMember(), null);
               }
           }
       }

       return null;
    }

        /** Returns { sink-name, sink-node-name } */
    protected String[] determineSinkNames() {
        String[] sink_names = { m_sink.getName(), null };
        if (m_sink instanceof PortProxy) {
            sink_names[1] = ((m_sink_member != null) ?
                        m_sink_member.getLocalName() : "this");
        }
        else if (m_sink instanceof JunctionProxy) {
            sink_names[1] = Parser.JUNCTION_ID_STRING;
        }
        else if (m_sink instanceof BundlerProxy) {
            sink_names[1] = Parser.BUNDLER_ID_STRING;
        }
        return sink_names;
    }

        /** Returns { source-name, source-node-name } */
    protected String[] determineSourceNames() {
        String[] source_names = { m_source.getName(), null };
        if (m_source instanceof PortProxy) {
            source_names[1] = ((m_source_member != null) ?
                        m_source_member.getLocalName() : "this");
        }
        else if (m_source instanceof JunctionProxy) {
            source_names[1] = Parser.JUNCTION_ID_STRING;
        }
        else if (m_source instanceof BundlerProxy) {
            source_names[1] = Parser.BUNDLER_ID_STRING;
        }
        return source_names;
    }

        /** Returns { origin-name, origin-node-name } */
    protected String[] determineOriginNames(ALFINode _container) {
        String[] origin_names = { null, null };
        GenericPortProxy origin = getOrigin(_container);
        MemberNodeProxy origin_member = getOriginMember(_container);
        if (origin != null) {
            origin_names[0] = origin.getName();
            if (origin instanceof PortProxy) {
                origin_names[1] = (origin_member != null) ?
                                       origin_member.getLocalName() : "this";
            }
            else if (origin instanceof BundlerProxy) {
                origin_names[1] = "__BUNDLER__";
            }
        }
        else {
            String warning =
                "Connection [" + this + "] unable to determine orign.  This " +
                "may be due to a junction between this connection's sink and " +
                "its ultimate origin having been disassociated with its " +
                "source (i.e. the connection from the junction's source to " +
                "the junction was removed and not replaced by another " +
                "connection back to some source.  No file should be written " +
                "including the current connection until this problem is fixed.";
            Alfi.warn(warning);
            origin_names = null;
        }
        return origin_names;
    }

        /** Generate parser representation of GUI info for this connection.
          * Every ConnectionProxy writes its GUI information, as opposed to
          * how the method generateParserRepresentation() works.  The argument
          * _line_prepend is prepended to every line, and the argument
          * _b_with_definition_info flags whether to add the path definition.
          */
    public String generateParserRepresentation_guiInfo(String _line_prepend,
                                             boolean _b_with_definition_info) {
        String line_start = (_line_prepend == null) ? "" : _line_prepend;
        String[] source_names = determineSourceNames();
        String[] sink_names = determineSinkNames();

        String rep = line_start + source_names[1] + " " + source_names[0] +
                     _ARROW_ + sink_names[1] + " " + sink_names[0];
        line_start = Parser.NL + line_start;
        String indent_start = line_start + Parser.STANDARD_SPACER;
        if (_b_with_definition_info) {
            String config_lines = "";

            config_lines += generateAuxConfigInfoLines(indent_start);
            String path_def = generatePathDefinitionString();
            if (path_def.length() > 0) {
                config_lines += indent_start + path_def;
            }
            else if (m_dog_leg_hint != DOG_LEG_HINT_NONE) {
                String state = (m_dog_leg_hint == DOG_LEG_HINT_CW) ? "cw":"ccw";
                config_lines += indent_start + "z=" + state;
            }

            if (config_lines.length() > 0) {
                rep += line_start + Parser.SECTION_BEGIN_MARKER +
                       config_lines +
                       line_start + Parser.SECTION_END_MARKER;
            }
            else {
                rep += line_start + Parser.SECTION_BEGIN_MARKER +
                       " " + Parser.SECTION_END_MARKER;
            }
        }
        return rep;
    }

        /** May be overridden by sub-classes to give more info. */
    protected String generateAuxConfigInfoLines(String _indent) {
        return "";
    }

        /** Returns the path definition as a string. */
    protected String generatePathDefinitionString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < m_path_definition.length; i++) {
            sb.append(  (((i % 2) == 0) ? "x=" : "y=")  );
            sb.append(  ((m_path_definition[i] != null) ?
                                 m_path_definition[i].toString() : "null")  );
            sb.append(" ");
        }

        return sb.toString().trim();
    }

        /** This method returns a descriptive string which is used by ALFINode
          * for creating copies of this connection, so it should not be tampered
          * with lightly.
          */
    String generateDescriptionForCopy() {
        String[] source_names = determineSourceNames();
        String[] sink_names = determineSinkNames();

        if (m_source instanceof JunctionProxy) {
            source_names[1] = "(Junction)";
        }
        else if (m_source instanceof BundlerProxy) {
            source_names[1] = "(Bundler)";
        }

        if (m_sink instanceof JunctionProxy) {
            sink_names[1] = "(Junction)";
        }
        else if (m_sink instanceof BundlerProxy) {
            sink_names[1] = "(Bundler)";
        }

        return (source_names[1] + " " + source_names[0] + _ARROW_ +
                                          sink_names[1] + " " + sink_names[0]);
    }

        /** Standard all purpose toString().  The return value of this method
          * should never be assumed to be parsable or useful in any way other
          * than human friendly information.
          */
    public String toString() {
        return generateDescriptionForCopy();
    }

        /** This method looks upstream for a source data type. */
    public int determineUpstreamDataType(ALFINode _container) {
        return m_source.determineSourceDataTypeForConnection(_container, this);
    }

        /** This method looks downstream for a sink data type. */
    public int determineDownstreamDataType(ALFINode _container) {
        return m_sink.determineSinkDataTypeForConnection(_container, this);
    }

    //////////////////////////////////////////////////////////
    //////////  ProxyChangeEventSource methods ///////////////

        /** ProxyChangeEventSource method. */
    public void addProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.put(_listener, null);
    }

        /** ProxyChangeEventSource method. */
    public void removeProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.remove(_listener);
    }

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected void fireProxyChangeEvent(ProxyChangeEvent e) {
        for (Iterator i = m_proxy_change_listener_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ProxyChangeEventListener) i.next()).proxyChangePerformed(e);
        }
    }

        /** _channel_path superfluous here.  Needed in overriding methods. */
    Object[] traceChannelBackToPort(ALFINode _container, String _channel_path) {
        GenericPortProxy source = getSource();
        if (source instanceof PortProxy) {
            MemberNodeProxy owner = getSourceMember();
            Object[] info = { source, owner, null };
            return info;
        }
        else if (source instanceof JunctionProxy) {
            ConnectionProxy upstream_con =
                      ((JunctionProxy) source).getSourceConnection(_container);
            if (upstream_con != null) {
                return upstream_con.traceChannelBackToPort(_container, null);
            }
            else { return null; }
        }
        else if (source instanceof BundlerProxy) {
            BundlerProxy bundler = (BundlerProxy) source;
            BundleProxy primary_in = bundler.getPrimaryInput(_container);
            if (primary_in != null) {
                Hashtable flat_content = BundleProxy.contentMapToFlatChannelMap(
                          bundler.getSecondaryOutContent(_container, true), "");
                for (Iterator i = flat_content.keySet().iterator();
                                                               i.hasNext(); ) {
                    String channel_name = (String) i.next();
                    BundleContentMapKey key =
                          (BundleContentMapKey) flat_content.get(channel_name);
                    if (! key.isContentHolderOnly()) {
                        return primary_in.traceChannelBackToPort(_container,
                                                                 channel_name);
                    }
                }
            }

            return null;
        }
        else {
            Alfi.warn("(" + this + ").traceChannelBackToPort(" + _container +
                      ", " + _channel_path + "): Source port type error.");
            return null;
        }
    }

        /** This method updates the content status of anything associated with
          * this connection, and propogates the update signal to the sink.
          */
    void updateContentAndPropogateDownstream(ALFINode _container) {
            // update any registered connection widget
        fireUpstreamDataChannelChangeEvent(new
                                         UpstreamDataChannelChangeEvent(this));

            // propogate signal
        GenericPortProxy sink_gpp = getSink();
        MemberNodeProxy _owner_member = getSinkMember();
        sink_gpp.updateContentAndPropogateDownstream(_container, _owner_member);
    }

        /** This was originally implemented as a background thread, but there
          * are some race conditions occasionally, so the thread created here
          * is no longer placed in the background.  If speed becomes an issue,
          * this will need to be reisited.  See also
          * GenericPortProxy.doContentUpdate().
          */
    public void doContentUpdate(final ALFINode _container) {
        final ConnectionProxy connection = this;
        Thread background_task =
            new Thread() {
                public void run() {
/*
                        // may need to wait for multiple ops to get finished
                    int sleep_count = 0;
                    while (BundleProxy.getBundleContentUpdatesSuspended()) {
                        int sleepy_time = (sleep_count < 20) ? 250 : 1000;
                        try { Thread.currentThread().sleep(sleepy_time); }
                        catch(InterruptedException _e) { ; }

                            // in case state has changed while sleeping
                        if (! _container.connection_contains(connection)) {
                            return;
                        }
                        else if (sleep_count++ > 3600) {    // about an hour...
                            Alfi.warn("startBackgroundContentUpdate(): " +
                                      "Too many sleeps.  Stopping thread...");
                            return;
                        }
                    }
*/

                    updateContentAndPropogateDownstream(_container);
                    ALFINode[] derived_nodes = _container.derivedNodes_getAll();

                    for (int i = 0; i < derived_nodes.length; i++) {
                        updateContentAndPropogateDownstream(derived_nodes[i]);
                    }
                }
            };

        //background_task.setPriority(Thread.MIN_PRIORITY);
        background_task.start();

            // this basically moves "background_task" into the foreground
        try { background_task.join(); }
        catch(InterruptedException _e) {
            Alfi.warn("background_task interrupted!");
        }
    }

    /////////////////////////////////////////////////////////////////
    ////////// ContainedElementModEventSource methods ///////////////

        /** ContainedElementModEventSource method. */
    public void addContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.put(_listener, null);
    }

        /** ContainedElementModEventSource method. */
    public void removeContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with ContainedElementModEventSource. */
    protected void fireContainedElementModEvent(ContainedElementModEvent e) {
        for (Iterator i =
                m_contained_element_mod_listeners_map.keySet().iterator();
                                                               i.hasNext(); ) {
            ((ContainedElementModEventListener) i.next()).
                                                   containedElementModified(e);
        }
    }

    /////////////////////////////////////////////////////////////////
    ////////// UpstreamDataChannelChangeEventSource methods /////////

        /** UpstreamDataChannelChangeEventSource method. */
    public void addUpstreamDataChannelChangeEventListener(
                            UpstreamDataChannelChangeEventListener _listener) {
        m_upstream_channel_change_listeners_map.put(_listener, null);
    }

        /** UpstreamDataChannelChangeEventSource method. */
    public void removeUpstreamDataChannelChangeEventListener(
                            UpstreamDataChannelChangeEventListener _listener) {
        m_upstream_channel_change_listeners_map.remove(_listener);
    }

         /** Used in conjunction with UpstreamDataChannelChangeEventSource.
           * This event firing is dealt with in the background.
           */
    protected void fireUpstreamDataChannelChangeEvent(
                                 final UpstreamDataChannelChangeEvent _event) {
        for (Iterator i = m_upstream_channel_change_listeners_map.keySet().
                                                   iterator(); i.hasNext(); ) {
            ((UpstreamDataChannelChangeEventListener) i.next()).
                                                upstreamChangeOccurred(_event);
        }
    }
}
