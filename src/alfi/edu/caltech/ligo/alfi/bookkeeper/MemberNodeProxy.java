package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.tools.*;


    /**
     * When a member node is added to a container node, what actually occurs is
     * that the member ALFINode object is created as well as an associated
     * MemberNodeProxy object.  This MemberNodeProxy object is added to the
     * container node's add_node list.  A MemberNodeProxy is added to the
     * container because it may represent an entire lineage of inherited nodes
     * instead of just one particular node.  Local name and position (among
     * other things) in the container are common properties to all nodes in this
     * lineage, and are stored here instead of in any ALFINode itself.
     */
public class MemberNodeProxy implements ProxyChangeEventSource,
                                        ContainedElementModEventSource {

    //**************************************************************************
    //***** constants **********************************************************

    public static final int ROTATION_SOUTH   = 0;
    public static final int ROTATION_EAST    = 1;
    public static final int ROTATION_WEST    = 2;
    public static final int ROTATION_NONE    = 3;
    public static final int ROTATION_INVALID = 4;
    public static final String[] ROTATION_SETTING_STRINGS =
                                { "South", "East", "West", "None" };

    public static final int SYMMETRY_X        = 0;
    public static final int SYMMETRY_Y        = 1;
    public static final int SYMMETRY_PLUS_XY  = 2;
    public static final int SYMMETRY_MINUS_XY = 3;
    public static final int SYMMETRY_NONE     = 4;
    public static final int SYMMETRY_INVALID  = 5;
    public static final String[] SYMMETRY_SETTING_STRINGS =
                                { "X", "Y", "XY", "-XY", "None" };

    public static final int SWAP_X       = 0;
    public static final int SWAP_Y       = 1;
    public static final int SWAP_XY      = 2;
    public static final int SWAP_NONE    = 3;
    public static final int SWAP_INVALID = 4;
    public static final String[] SWAP_SETTING_STRINGS =
                                              { "X", "Y", "XY", "None" };

    public static final String FUNC_X_GROUP = "FUNC_X";

    //**************************************************************************
    //***** member fields ******************************************************

        /**
         * This is the node in which this MemberNodeProxy was locally added.
         */
    private final ALFINode m_direct_container_node;

        /**
         * This is the member node directly associated with the creation of this
         * MemberNodeProxy.
         */
    private final ALFINode m_directly_associated_member_node;

        /**
         * The path used by the container node to find this node's root node
         * file.  It is the path which appears in the disk file which contains
         * the directives to create this member node originally, E.g.,
         * #include "boxes/X.box".
         */
    private String m_include_line_path;

        /**
         * All nodes in this inheritance lineage share an identical local name.
         * This is the only place in which the name is stored.
         */
    private String m_local_name;

        /**
         * All nodes in this inheritance lineage share an identical position in
         * their respective container nodes.  This is the only place in which
         * this location is stored.
         */
    private Point m_position_in_container;

        /**
         * The member may be visually rotated.  This member stores the rotation
         * information, if any: ROTATION_EAST, ROTATION_SOUTH,
         * ROTATION_WEST, ROTATION_NONE.
         */
    private int m_rotation;

        /**
         * The member may have its visual elements mirrored across some axis of
         * symmetry.  This member stores this information via: SYMMETRY_X,
         * SYMMETRY_Y, SYMMETRY_PLUS_XY, SYMMETRY_MINUS_XY, SYMMETRY_NONE.
         */
    private int m_symmetry;

        /**
         * The member may have the visual elements swapped across an axis.  This
         * is similar to the symmetry property except that it only affects the
         * elements on sides parallel to the axis.  Sides crossing this axis are
         * not rearranged.  Possible values are: SWAP_X, SWAP_Y, and SWAP_NONE.
         */
    private int m_swap;

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected HashMap m_proxy_change_listener_map;

        /** Used in conjunction with ContainedElementModEventSource. */
    private HashMap m_contained_element_mod_listeners_map;

    //**************************************************************************
    //***** constructors *******************************************************

        /**
         * Constructor.  It is important to note that this constructor also
         * creates the container hierarchy of the actual ALFINodes involved
         * as well as just the lightweight MemberNodeProxy object itself.  This
         * is done because both this object and the base ALFINode which it
         * represents need to include references to each other to be complete.
         *
         * This constructor should only be called from ALFINode.
         */
    MemberNodeProxy(ALFINode _base_node, ALFINode _container_node,
                    String _local_name, String _include_line_path,
                    Point _location_in_container) {

            // final member initializations
        m_direct_container_node = _container_node;

            // other members
        m_local_name = _local_name;
        m_include_line_path = _include_line_path;
        m_position_in_container = _location_in_container;
        m_rotation = ROTATION_NONE;
        m_symmetry = SYMMETRY_NONE;
        m_swap = SWAP_NONE;

            // final member
            // deriveInstance will automatically create the right type of node
        m_directly_associated_member_node =
                              _base_node.deriveInstance(_container_node, this);

        m_proxy_change_listener_map = new HashMap();
        m_contained_element_mod_listeners_map = new HashMap();
    }

        /**
         * Auxilliary constructor which creates a proxy for temporary storage
         * of MemberNodeProxy information only.  Thes objects are useless
         * elsewhere because the (final) container node field is null.
         */
    public MemberNodeProxy(String _local_name, Point _location,
                           String _include_line_path,  Integer _rotation,
                           Integer _symmetry, Integer _swap) {

        m_direct_container_node = null;
        m_directly_associated_member_node = null;

        m_local_name = _local_name;
        m_position_in_container = _location;
        m_include_line_path = _include_line_path;
        
        if (_rotation != null) { m_rotation = _rotation.intValue(); }
        else { m_rotation = ROTATION_INVALID; }

        if (_symmetry != null) { m_symmetry = _symmetry.intValue(); }
        else { m_symmetry = SYMMETRY_INVALID; }

        if (_swap != null) { m_swap = _swap.intValue(); }
        else { m_swap = SWAP_INVALID; }

        m_proxy_change_listener_map = null;
        m_contained_element_mod_listeners_map = null;
    }

    //**************************************************************************
    //***** methods ************************************************************

        /**
         * This method returns a PortProxy[][] ordered by edge and position on
         * edge after rotation, swap, and symmetry operations have been applied.
         * It takes the raw positions (i.e. before these ops) and rearranges
         * them.
         *
         * The order of these operations is rotate, swap, symmetry.
         */
    public PortProxy[][] determinePortPositionsAfterRotateEtcOps(
                                                     ALFINode _container_node) {
        ALFINode node = _container_node.memberNode_get(this);
        PortProxy[][] port_order = node.ports_getByPosition(false);

            // order of operations matters
        port_order = performRotationOperation(port_order);
        port_order = performSwapOperation(port_order);
        port_order = performSymmetryOperation(port_order);

        return port_order;
    }

    private PortProxy[][] performRotationOperation(PortProxy[][] _port_order) {
        if (m_rotation == ROTATION_NONE) { return _port_order; }

        PortProxy[][] new_order = new PortProxy[_port_order.length][];
        switch (m_rotation) {
          case ROTATION_EAST:
            new_order[PortProxy.EDGE_EAST] = _port_order[PortProxy.EDGE_NORTH];

            new_order[PortProxy.EDGE_SOUTH] = _port_order[PortProxy.EDGE_EAST];
            reverseOrder(new_order[PortProxy.EDGE_SOUTH]);

            new_order[PortProxy.EDGE_WEST] = _port_order[PortProxy.EDGE_SOUTH];

            new_order[PortProxy.EDGE_NORTH] = _port_order[PortProxy.EDGE_WEST];
            reverseOrder(new_order[PortProxy.EDGE_NORTH]);
            break;

          case ROTATION_SOUTH:
            new_order[PortProxy.EDGE_SOUTH] = _port_order[PortProxy.EDGE_NORTH];
            reverseOrder(new_order[PortProxy.EDGE_SOUTH]);

            new_order[PortProxy.EDGE_WEST] = _port_order[PortProxy.EDGE_EAST];
            reverseOrder(new_order[PortProxy.EDGE_WEST]);

            new_order[PortProxy.EDGE_NORTH] = _port_order[PortProxy.EDGE_SOUTH];
            reverseOrder(new_order[PortProxy.EDGE_NORTH]);

            new_order[PortProxy.EDGE_EAST] = _port_order[PortProxy.EDGE_WEST];
            reverseOrder(new_order[PortProxy.EDGE_EAST]);
            break;

          case ROTATION_WEST:
            new_order[PortProxy.EDGE_WEST] = _port_order[PortProxy.EDGE_NORTH];
            reverseOrder(new_order[PortProxy.EDGE_WEST]);

            new_order[PortProxy.EDGE_NORTH] = _port_order[PortProxy.EDGE_EAST];

            new_order[PortProxy.EDGE_EAST] = _port_order[PortProxy.EDGE_SOUTH];
            reverseOrder(new_order[PortProxy.EDGE_EAST]);

            new_order[PortProxy.EDGE_SOUTH] = _port_order[PortProxy.EDGE_WEST];
            break;

          default:
            new_order = null;
            break;
        }
        return new_order;
    }

    private PortProxy[][] performSwapOperation(PortProxy[][] _port_order) {
        if (m_swap == SWAP_NONE) { return _port_order; }

        PortProxy[][] new_order = new PortProxy[_port_order.length][];
        switch (m_swap) {
          case SWAP_X:
            new_order[PortProxy.EDGE_NORTH] = _port_order[PortProxy.EDGE_SOUTH];
            new_order[PortProxy.EDGE_SOUTH] = _port_order[PortProxy.EDGE_NORTH];
            new_order[PortProxy.EDGE_EAST] = _port_order[PortProxy.EDGE_EAST];
            new_order[PortProxy.EDGE_WEST] = _port_order[PortProxy.EDGE_WEST];
            break;

          case SWAP_Y:
            new_order[PortProxy.EDGE_NORTH] = _port_order[PortProxy.EDGE_NORTH];
            new_order[PortProxy.EDGE_SOUTH] = _port_order[PortProxy.EDGE_SOUTH];
            new_order[PortProxy.EDGE_EAST] = _port_order[PortProxy.EDGE_WEST];
            new_order[PortProxy.EDGE_WEST] = _port_order[PortProxy.EDGE_EAST];
            break;

          default:
            new_order = null;
        }
        return new_order;
    }

    private PortProxy[][] performSymmetryOperation(PortProxy[][] _port_order) {
        if (m_symmetry == SYMMETRY_NONE) { return _port_order; }

        PortProxy[][] new_order = new PortProxy[_port_order.length][];
        switch (m_symmetry) {
          case SYMMETRY_X:
            new_order[PortProxy.EDGE_SOUTH] = _port_order[PortProxy.EDGE_NORTH];

            new_order[PortProxy.EDGE_EAST] = _port_order[PortProxy.EDGE_EAST];
            reverseOrder(new_order[PortProxy.EDGE_EAST]);

            new_order[PortProxy.EDGE_NORTH] = _port_order[PortProxy.EDGE_SOUTH];

            new_order[PortProxy.EDGE_WEST] = _port_order[PortProxy.EDGE_WEST];
            reverseOrder(new_order[PortProxy.EDGE_WEST]);
            break;

          case SYMMETRY_Y:
            new_order[PortProxy.EDGE_NORTH] = _port_order[PortProxy.EDGE_NORTH];
            reverseOrder(new_order[PortProxy.EDGE_NORTH]);

            new_order[PortProxy.EDGE_WEST] = _port_order[PortProxy.EDGE_EAST];

            new_order[PortProxy.EDGE_SOUTH] = _port_order[PortProxy.EDGE_SOUTH];
            reverseOrder(new_order[PortProxy.EDGE_SOUTH]);

            new_order[PortProxy.EDGE_EAST] = _port_order[PortProxy.EDGE_WEST];
            break;

          case SYMMETRY_PLUS_XY:
            new_order[PortProxy.EDGE_EAST] = _port_order[PortProxy.EDGE_NORTH];
            reverseOrder(new_order[PortProxy.EDGE_EAST]);

            new_order[PortProxy.EDGE_NORTH] = _port_order[PortProxy.EDGE_EAST];
            reverseOrder(new_order[PortProxy.EDGE_NORTH]);

            new_order[PortProxy.EDGE_WEST] = _port_order[PortProxy.EDGE_SOUTH];
            reverseOrder(new_order[PortProxy.EDGE_WEST]);

            new_order[PortProxy.EDGE_SOUTH] = _port_order[PortProxy.EDGE_WEST];
            reverseOrder(new_order[PortProxy.EDGE_SOUTH]);
            break;

          case SYMMETRY_MINUS_XY:
            new_order[PortProxy.EDGE_WEST] = _port_order[PortProxy.EDGE_NORTH];

            new_order[PortProxy.EDGE_SOUTH] = _port_order[PortProxy.EDGE_EAST];

            new_order[PortProxy.EDGE_EAST] = _port_order[PortProxy.EDGE_SOUTH];

            new_order[PortProxy.EDGE_NORTH] = _port_order[PortProxy.EDGE_WEST];
            break;

          default:
            new_order = null;
            break;
        }
        return new_order;
    }

        /** Convenience method.  Just reverses the order of an array of ports.*/
    private void reverseOrder(PortProxy[] _ports) {
        ArrayList list = new ArrayList(Arrays.asList(_ports));
        Collections.reverse(list);
        list.toArray(_ports);
    }

        /**
         * This method determines which edge the passed port resides on the
         * member node after rotation, swap, and symmetry operations have
         * been done on the node.
         */
    public int determinePortEdgeAfterOps(PortProxy _port) {
        int edge = _port.getPosition()[2];

            // rotation operation
        switch (m_rotation) {
          case ROTATION_EAST:
            switch (edge) {
              case PortProxy.EDGE_NORTH: edge = PortProxy.EDGE_EAST;   break;
              case PortProxy.EDGE_EAST:  edge = PortProxy.EDGE_SOUTH;  break;
              case PortProxy.EDGE_SOUTH: edge = PortProxy.EDGE_WEST;   break;
              case PortProxy.EDGE_WEST:  edge = PortProxy.EDGE_NORTH;  break;
            }
            break;
          case ROTATION_SOUTH:
            switch (edge) {
              case PortProxy.EDGE_NORTH: edge = PortProxy.EDGE_SOUTH;  break;
              case PortProxy.EDGE_EAST:  edge = PortProxy.EDGE_WEST;   break;
              case PortProxy.EDGE_SOUTH: edge = PortProxy.EDGE_NORTH;  break;
              case PortProxy.EDGE_WEST:  edge = PortProxy.EDGE_EAST;   break;
            } 
            break;
          case ROTATION_WEST:
            switch (edge) {
              case PortProxy.EDGE_NORTH: edge = PortProxy.EDGE_WEST;   break;
              case PortProxy.EDGE_EAST:  edge = PortProxy.EDGE_NORTH;  break;
              case PortProxy.EDGE_SOUTH: edge = PortProxy.EDGE_EAST;   break;
              case PortProxy.EDGE_WEST:  edge = PortProxy.EDGE_SOUTH;  break;
            } 
            break;
        }

            // swap operation
        switch (m_swap) {
          case SWAP_X:
            switch (edge) {
              case PortProxy.EDGE_NORTH: edge = PortProxy.EDGE_SOUTH;  break;
              case PortProxy.EDGE_EAST:  edge = PortProxy.EDGE_EAST;   break;
              case PortProxy.EDGE_SOUTH: edge = PortProxy.EDGE_NORTH;  break;
              case PortProxy.EDGE_WEST:  edge = PortProxy.EDGE_WEST;   break;
            }
            break;
          case SWAP_Y:
            switch (edge) {
              case PortProxy.EDGE_NORTH: edge = PortProxy.EDGE_NORTH;  break;
              case PortProxy.EDGE_EAST:  edge = PortProxy.EDGE_WEST;   break;
              case PortProxy.EDGE_SOUTH: edge = PortProxy.EDGE_SOUTH;  break;
              case PortProxy.EDGE_WEST:  edge = PortProxy.EDGE_EAST;   break;
            }
            break;
        }

            // symmetry operation
        switch (m_symmetry) {
          case SYMMETRY_X:
            switch (edge) {
              case PortProxy.EDGE_NORTH: edge = PortProxy.EDGE_SOUTH;  break;
              case PortProxy.EDGE_EAST:  edge = PortProxy.EDGE_EAST;   break;
              case PortProxy.EDGE_SOUTH: edge = PortProxy.EDGE_NORTH;  break;
              case PortProxy.EDGE_WEST:  edge = PortProxy.EDGE_WEST;   break;
            }
            break;
          case SYMMETRY_Y:
            switch (edge) {
              case PortProxy.EDGE_NORTH: edge = PortProxy.EDGE_NORTH;  break;
              case PortProxy.EDGE_EAST:  edge = PortProxy.EDGE_WEST;   break;
              case PortProxy.EDGE_SOUTH: edge = PortProxy.EDGE_SOUTH;  break;
              case PortProxy.EDGE_WEST:  edge = PortProxy.EDGE_EAST;   break;
            }
            break;
          case SYMMETRY_PLUS_XY:
            switch (edge) {
              case PortProxy.EDGE_NORTH: edge = PortProxy.EDGE_EAST;   break;
              case PortProxy.EDGE_EAST:  edge = PortProxy.EDGE_NORTH;  break;
              case PortProxy.EDGE_SOUTH: edge = PortProxy.EDGE_WEST;   break;
              case PortProxy.EDGE_WEST:  edge = PortProxy.EDGE_SOUTH;  break;
            }
            break;
          case SYMMETRY_MINUS_XY:
            switch (edge) {
              case PortProxy.EDGE_NORTH: edge = PortProxy.EDGE_WEST;   break;
              case PortProxy.EDGE_EAST:  edge = PortProxy.EDGE_SOUTH;  break;
              case PortProxy.EDGE_SOUTH: edge = PortProxy.EDGE_EAST;   break;
              case PortProxy.EDGE_WEST:  edge = PortProxy.EDGE_NORTH;  break;
            }
            break;
        }

        return edge;
    }

    ///////////////////////////////////////////////////////////////////////////
    // information about other objects related to the MemberNodeProxy //////////

        /**
         * This MemberNodeProxy is associated with a particular ALFINode
         * which is contained in the node _container_node.  Returns this
         * member node.  [Note that this is the same as the node contained in
         * the field m_directly_associated_member_node only when _container_node
         * is m_direct_container_node!]
         */
    public ALFINode getAssociatedMemberNode(ALFINode _container_node) {
        return _container_node.memberNode_get(this);
    }

        /** Returns an array of ConnectionProxy objects connected to this member
          * node in _container.
          */
    public ConnectionProxy[] getConnectionsTo(ALFINode _container) {
        ConnectionProxy[] connections = _container.connections_get();
        ArrayList connection_list = new ArrayList();
        for (int i = 0; i < connections.length; i++) {
            ConnectionProxy connection = connections[i];
            if ((connection.getSourceMember() == this) ||
                                        (connection.getSinkMember() == this)) {
                connection_list.add(connection);
            }
        }

        connections = new ConnectionProxy[connection_list.size()];
        connection_list.toArray(connections);

        return connections;
    }

        /**
         * Gets an array of PortProxy objects on this member node at
         * this point in the container hierarchy.  The booleans specify if
         * input ports, output borts, or both are desired.
         */
    PortProxy[] getPorts(ALFINode _container_node, boolean _b_get_inputs,
                                                       boolean _b_get_outputs) {
        ALFINode member = getAssociatedMemberNode(_container_node);
        PortProxy[] ports = member.ports_get();
        ArrayList port_list = new ArrayList();
        for (int i = 0; i < ports.length; i++) {
            if (    (_b_get_inputs &&
                        (ports[i].getIOType() == PortProxy.IO_TYPE_INPUT)) ||
                    (_b_get_outputs &&
                        (ports[i].getIOType() == PortProxy.IO_TYPE_OUTPUT)) ) {
                port_list.add(ports[i]);
            }
        }
        ports = new PortProxy[port_list.size()];
        port_list.toArray(ports);
        return ports;
    }

    /////////////////////////////////
    // member field access //////////

        /**
         * @returns m_direct_container_node.
         */
    ALFINode getDirectContainerNode() { return m_direct_container_node; }

        /**
         * @returns m_directly_associated_member_node.
         */
    public ALFINode getDirectlyAssociatedMemberNode() {
	return m_directly_associated_member_node;
    }

    public String getLocalName() { 
        return ((m_local_name != null) ? new String(m_local_name) : null);
    }

    public boolean setLocalName(String _name) {
        if ((_name != null) && (_name.length() > 0)
                            && (! _name.equals(m_local_name))) {
            m_local_name = _name;
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                     new MemberNodeProxy(_name, null, null, null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public String getIncludeLinePath() {
        return ((m_include_line_path != null) ?
                                 new String(m_include_line_path) : null);
    }

    public void setIncludeLinePath(String _include_line_path) {
        m_include_line_path = _include_line_path;
    }

    public Point getLocation() { return new Point(m_position_in_container); }

       /** Sets the location of the member node in it's container at the
         * closest grid point to _new_location (i.e., auto snaps to grid.)
         */
    public boolean setLocation(Point _new_location) {
        return setLocation(_new_location, true);
    }

       /** Sets the location of the member node in it's container at the
         * closest grid point to _new_location if _b_snap_to_grid is true
         * (else exactly to the point _new_location (rarely if ever used.))
         *
         * Set private unless and until non-snap-to-grid functionality needed.
         */
    private boolean setLocation(Point _new_location, boolean _b_snap_to_grid) {

        if (! _new_location.equals(m_position_in_container)) {
            m_position_in_container = (_b_snap_to_grid) ?
                                GridTool.getNearestGridPoint(_new_location) :
                                new Point(_new_location);

            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                       new MemberNodeProxy(null, _new_location,
                                           null, null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public int getRotation() { return m_rotation; }

    public boolean setRotation(int _new_rotation) {
        if (_new_rotation != m_rotation) {
            m_rotation = _new_rotation;
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                   new MemberNodeProxy(null, null, null,
                                       new Integer(_new_rotation), null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public boolean hasDefaultRotation() {
        if (m_rotation == ROTATION_NONE) {
            return true;
        }
        else { return false; }
    }

    public static int getRotationFromString(String _rotation_string) {
        for (int i = 0; i < ROTATION_INVALID; i++) {
            if (_rotation_string.equals(ROTATION_SETTING_STRINGS[i])) {
                return i;
            }
        }
        return ROTATION_INVALID;
    }

    public static int getSwapFromString(String _swap_string) {
        for (int i = 0; i < SWAP_INVALID; i++) {
            if (_swap_string.equals(SWAP_SETTING_STRINGS[i])) {
                return i;
            }
        }
        return SWAP_INVALID;
    }

    public static int getSymmetryFromString(String _symmetry_string) {
        for (int i = 0; i < SYMMETRY_INVALID; i++) {
            if (_symmetry_string.equals(SYMMETRY_SETTING_STRINGS[i])) {
                return i;
            }
        }
        return SYMMETRY_INVALID;
    }

    public int getSymmetry() { return m_symmetry; }

    public boolean setSymmetry(int _new_symmetry) {
        if (_new_symmetry != m_symmetry) {
            m_symmetry = _new_symmetry;
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                   new MemberNodeProxy(null, null, null,
                                       null, new Integer(_new_symmetry), null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public boolean hasDefaultSymmetry() {
        if (m_symmetry == SYMMETRY_NONE) { return true; }
        else { return false; }
    }

    public int getSwap() { return m_swap; }

    public boolean setSwap(int _new_swap) {
        if (_new_swap != m_swap) {
            m_swap = _new_swap;
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                   new MemberNodeProxy(null, null, null,
                                       null, null, new Integer(_new_swap)));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public boolean hasDefaultSwap() {
        if (m_swap == SWAP_NONE) { return true; }
        else { return false; }
    }

    public String toString() {
        String info = "MNProxy " + m_local_name + " (" +
            m_direct_container_node.generateFullNodePathName() + ")";
        return info;
    }

    //////////////////////////////////////////////////////
    ////////// ProxyChangeEventSource Methods //////////

        /** ProxyChangeEventSource method. */
    public void addProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.put(_listener, null);
    }

        /** ProxyChangeEventSource method. */
    public void removeProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.remove(_listener);
    }

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected void fireProxyChangeEvent(ProxyChangeEvent e) {
        for (Iterator i = m_proxy_change_listener_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ProxyChangeEventListener) i.next()).proxyChangePerformed(e);
        }
    }

    /////////////////////////////////////////////////////////////////
    ////////// ContainedElementModEventSource methods ///////////////

        /** ContainedElementModEventSource method. */
    public void addContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.put(_listener, null);
    }

        /** ContainedElementModEventSource method. */
    public void removeContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with ContainedElementModEventSource. */
    protected void fireContainedElementModEvent(ContainedElementModEvent e) {
        for (Iterator i =
                m_contained_element_mod_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ContainedElementModEventListener) i.next()).
                                                   containedElementModified(e);
        }
    }
}
