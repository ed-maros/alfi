package edu.caltech.ligo.alfi.bookkeeper;

    /** Defines the queries needed by the NodePropertiesDialog. */
public interface NodeSettingDeclarationIF {
    public String getName();
    public String getValue();
    public boolean isQuoted();
    public String getType();
    public String getComment();
}
