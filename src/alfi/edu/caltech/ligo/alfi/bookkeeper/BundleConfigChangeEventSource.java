package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.bookkeeper.*;

    /** This interface defines fields and methods used by sources of 
      * BundleConfigChangeEvents.
      */
public interface BundleConfigChangeEventSource {
    public void addBundleConfigChangeEventListener(
                                    BundleConfigChangeEventListener _listener);
    public void removeBundleConfigChangeEventListener(
                                    BundleConfigChangeEventListener _listener);
}
