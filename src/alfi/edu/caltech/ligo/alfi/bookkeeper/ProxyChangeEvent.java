package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;


    /**
     * This event is sent to listening proxies from widgets representing them
     * when the user makes an edit on such a widget in an edit window.  They
     * are used to update the associated proxy.  Such events may also be sent
     * to other edit sessions interested in this change.
     */
public class ProxyChangeEvent extends EventObject {

    //**************************************************************************
    //***** fields *************************************************************

         /** New proxy object created ONLY to pass changed info to listener. */
     private Object m_temp_proxy_containing_changes;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public ProxyChangeEvent(Object _source,
                            Object _temp_proxy_containing_changes) {
        super(_source);
        m_temp_proxy_containing_changes = _temp_proxy_containing_changes;
    }

    //**************************************************************************
    //***** methods ************************************************************


    /////////////////////////////////
    // member field access //////////

    public Object getTempProxyObjectContainingChanges() {
        return m_temp_proxy_containing_changes;
    }
}
