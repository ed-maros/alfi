package edu.caltech.ligo.alfi.bookkeeper;

import java.io.*;
import java.util.*;
import java.lang.Math;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.tools.*;

    /**
      * A FUNCX_Node is a subclass of ALFINode which is the entity used for
      * a FUNC_X Primitive.   This primitive is different from the rest
      * because the user can have two different kinds of displays in
      * the Primitive Setting Dialog. 
      * <p>
      * If the <i>showInstanceSettings</i> 
      * is set to <code>true</code>, the dialog displays a simple view
      * containing the type, variable name, and value of the variables defined
      * in the <code>MemberDecl</code> setting.  
      * <p> Otherwise, if the 
      * <i>showInstanceSettings</i> is set to <code>false</code> then the
      * C++ code snippet is displayed along with all the other primitive
      * settings (like <code> Equation, Constsructor, Destructor</code>, etc.)
      */
public class FUNCX_Node extends ALFINode {

        /** Directive to look at an auxilliary include file for value. */
    public static final String AT_AT_INCLUDE = "@@INCLUDE";

        /** Primitive setting to be parsed */
    public static final String FUNC_X_MEMBER_DECL = "MemberDecl";

        /** Start of the substring in MemberDecl to be used in the 
          * <i>simple view</i>
          */
    public static final String FUNC_X_MEMBER_VAR_START =
                                                   "//FUNC_X_MEMBER_VAR_START";

        /** End of the substring in MemberDecl to be used in the 
          * <i>simple view</i>
          */
    public static final String FUNC_X_MEMBER_VAR_END= "//FUNC_X_MEMBER_VAR_END";

        /** If the setting has a value defined in this object instance, this
          * delimiter is prepended before the initialized value (ending with a
          * semicolon
          */
    public static final String FUNC_X_MEMBER_INIT = "//**FUNC_X_INIT_VALUE";

        /** Primitive setting which contains the initialized values */
    public static final String FUNC_X_CONSTRUCTOR = "Constructor";

        /** Start of the substring in the Constructor setting for the
          * initialized values.
          */
    public static final String FUNC_X_CTOR_INIT_START =
                                                  "//FUNC_X_MEMBER_INIT_START";

        /** End of the substring in the Constructor setting for the initialized
          * values.
          */
    public static final String FUNC_X_CTOR_INIT_END= "//FUNC_X_MEMBER_INIT_END";

        /** Comment prepended in the inserted substrings */
    public static final String FUNC_X_WARNING_NO_EDIT =
        "//The following code segment is generated automatically.  DO NOT EDIT";
 
    public static final String FUNC_X_EQUATIONS = "Equations";
    public static final String FUNC_X_MEMBER_IMPL = "MemberImpl";
    public static final String FUNC_X_GLOBAL = "Global";
    public static final String FUNC_X_HEADER = "Header";
    public static final String FUNC_X_DESTRUCTOR = "Destructor";
    public static final String FUNC_X_DOWHENGLOBALCHANGES =
                                                         "DoWhenGlobalChanges";

    public static final String[] FUNC_X_SETTING_ORDER =
        { FUNC_X_EQUATIONS,
          FUNC_X_MEMBER_DECL,
          FUNC_X_MEMBER_IMPL,
          FUNC_X_GLOBAL,
          FUNC_X_HEADER,
          FUNC_X_CONSTRUCTOR,
          FUNC_X_DESTRUCTOR,
          FUNC_X_DOWHENGLOBALCHANGES
        };  

    public static final String[] FUNC_X_AT_AT_INCLUDE_MARKERS =
        { "EQUATION",
          "MEMBER_DECL",
          "MEMBER_IMPL",
          "GLOBAL",
          "HEADERS",
          "CONSTRUCTOR",
          "DESTRUCTOR",
          "DO_WHEN_GLOBAL_CHANGES"
        };  

    public static final String FUNC_X_SETTING_BEGIN = "/*@@BEGIN_FX_";
    public static final String FUNC_X_SETTING_END = "/*@@END_FX_";


        /** Series of Member
        /** ParameterDeclaration objects list.  Unlike normal ALFINode where 
          * only the primitive nodes can add parameter declarations,
          * func_x instance nodes can have parameter declarations locally 
          * defined.
          */
    private ArrayList m_funcx_param_decl_list;

        /** FUNCX_ParameterSetting objects list. */
    private ArrayList m_funcx_setting_list;

        /** FUNCX_ParameterSetting default values from base_nodes. */
    private ArrayList m_funcx_default_setting_list;

        /** Flag to indicate that setting showInstanceSettings is set to true */
    private boolean m_instance_parameters_only;

        /** Flag to indicate that setting showInstanceSettings is set to DEFAULT*/
    private boolean m_default_instance_setting;

        /** Flag to indicate that the ParameterDeclaration list has been created
          */
    private boolean mb_param_decl_list_constructed;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor for creating a root node only. */
// this constructor is associated with creating a root box node from a box
// file.  this method should probably not exist for FUNCX_Node.
    public FUNCX_Node(ALFIFile _root_node_file) {
        super(_root_node_file);
        m_funcx_default_setting_list = null;
        mb_param_decl_list_constructed = false;
        m_default_instance_setting = true;;
    } 

        /** Constructor for creating a member FUNCX_Node.  See deriveInstance()
          * below for "public" access to this method.  This constructor is never
          * called except by FUNCX_Node.deriveInstance() and by super() in any
          * derived class's constructors.
          */
    protected FUNCX_Node(ALFINode _base_node, ALFINode _container_node,
                                  MemberNodeProxy _associated_MemberNodeProxy) {
        super(_base_node, _container_node, _associated_MemberNodeProxy);
        m_funcx_default_setting_list = null;
        mb_param_decl_list_constructed = false;
        m_default_instance_setting = true;;
    }

        /* This constructor only exists for UDP to call super in its
         * constructor.  Do not use this constructor to create FUNCX_Nodes.
         */
    protected FUNCX_Node(String _node_type, String _primitive_group) {
        super(_node_type, _primitive_group);
        m_default_instance_setting = true;;
    }

    //**************************************************************************
    //***** public methods *****************************************************

    ////////////////////////////////////////////
    // base and derived nodes methods //////////
    ////////////////////////////////////////////

        /** Overrides the ALFINode call in order to create the correct type
          * of node.  This call should always be made by the node which is to
          * be the DIRECT base node of the node to be created.  And of course,
          * this new node will be being placed inside another container node
          * (_container_node), and will always have a MemberNodeProxy
          * (_associated_MemberNodeProxy) associated with the new node's
          * placement in _container_node.
          */
    ALFINode deriveInstance(ALFINode _container_node,
                                 MemberNodeProxy _associated_MemberNodeProxy) {
        return new FUNCX_Node(this,_container_node,_associated_MemberNodeProxy);
    }

        /**  
          * Traverses through the ALFINode hierarchy and initializes the
          * lists of instance variables.
          */
    public void initialize () {
        if (baseNode_getDirect() instanceof FUNCX_Node) {
            FUNCX_Node base_node = (FUNCX_Node) baseNode_getDirect();
            base_node.initialize();
        }

        boolean instance_params_only = false; 
        instance_params_only = determineInstanceParameterSetting(this);
        if (m_default_instance_setting ) { 
            if (baseNode_getDirect() instanceof FUNCX_Node) {
                FUNCX_Node base_node = (FUNCX_Node) baseNode_getDirect();
                instance_params_only = base_node.isInstanceParametersOnly();
            }
        }
        setInstanceParametersOnly(instance_params_only);

        initializeLocalFuncXVariable(); 
    }


    public int getSettingIdFromString(String _setting_string) {
        for (int i = 0; i < FUNC_X_SETTING_ORDER.length; i++) {
            if (_setting_string.equals(FUNC_X_SETTING_ORDER[i])) {
                return i;
            }
        }
        return (-1);
    }


        /** This method searches for the latest setting object associated with
          * a value for _setting_name, and then checks if the value has the
          * format:
          *
          * @@INCLUDE file_name
          *
          * If so, then the object is returned.  If not, null is returned.
          *
          * The object returned will usually be a ParameterSetting, but it
          * may also be a ParameterDeclaration which has a default value
          * for _setting_name which is an @@INCLUDE.  (This will never be the
          * case for FUNCX_Nodes, but ParameterDeclaration objects may be
          * returned by subclasses, and the check here is for completeness.)
          *
          * _setting_name will be something like FUNC_X_CONSTRUCTOR or
          * FUNC_X_MEMBER_DECL, etc.
          */
    public Object getAtAtIncludeSettingValueObject(String _setting_name) {
        Object object_with_include_value = null;

        ParameterSetting setting = parameterSetting_get(_setting_name);
        if (setting != null) {
            if (parseAtAtIncludeLine(setting.getValue()) != null) {
                object_with_include_value = setting;
            }
        }
        else {
            ALFINode base = isRootNode() ? this : baseNode_getRoot();
            ParameterDeclaration declaration =
                             base.parameterDeclaration_getLocal(_setting_name);
            if (declaration != null) {
                if (parseAtAtIncludeLine(declaration.getValue()) != null) {
                    object_with_include_value = declaration;
                }
            }
            else {
                System.err.println(
                    "(FUNCX_Node).getAtAtIncludeSettingValueObject(" +
                                                       _setting_name + "):\n" +
                    "\tWarning: " + _setting_name + " has not been declared " +
                    "as a parameter in this node (" + this + ").");
            }
        }

        return object_with_include_value;
    }
    
        /** Checks if there is an @@INCLUDE value associated with a setting
          * name.  _setting_name will be something like FUNC_X_CONSTRUCTOR or
          * FUNC_X_MEMBER_DECL, etc.  This includes a check for a default value
          * for _setting_name from the root base node's associated
          * ParameterDeclaration for completeness.
          */
    public String determineAtAtSettingValue(String _setting_name) {
    
        String[] file_contents = getAtAtIncludeContent(_setting_name);
        StringBuffer buffer = new StringBuffer();
         
        if (file_contents != null && file_contents.length > 0) {
            int setting_id = getSettingIdFromString(_setting_name);
            if (setting_id  >= 0) {
                String start_delimeter = 
                    new String(FUNC_X_SETTING_BEGIN + 
                               FUNC_X_AT_AT_INCLUDE_MARKERS[setting_id] +
                               "*/");
                String end_delimeter = 
                    new String(FUNC_X_SETTING_END+ 
                               FUNC_X_AT_AT_INCLUDE_MARKERS[setting_id] +
                               "*/");
                
                // Extract the value string
                for (int i = 0; i < file_contents.length; i++) {

                    boolean found_end = false;
                    if (file_contents[i].indexOf(start_delimeter) >= 0) {

                        //Found the start delimeter
                        for (int j = i+1; j < file_contents.length; j++) {
                             
                            // Compose the string buffer as long as the 
                            // end delimeter isn't found
                            if (file_contents[j].indexOf(end_delimeter) < 0) { 
                                buffer.append(file_contents[j]);
                                buffer.append(Parser.NL);
                            }
                            else {
                                found_end = true;
                                break;
                            }
                        }
                    }
                    if (found_end) { break; }
                }
                if (buffer.length() > 0) {
                    return buffer.toString();
                }
            }
        }
        return null;
    }

        /** Checks if there is an @@INCLUDE value associated with a setting
          * name.  _setting_name will be something like FUNC_X_CONSTRUCTOR or
          * FUNC_X_MEMBER_DECL, etc.  This includes a check for a default value
          * for _setting_name from the root base node's associated
          * ParameterDeclaration for completeness.
          */
    public boolean hasAtAtIncludeSettingValueObject(String _setting_name) {
        return (getAtAtIncludeSettingValueObject(_setting_name) != null);
    }

        /** Get the content of an auxilliary @@INCLUDE file.  Returns null if
          * there is no setting associated with _setting_name, if the
          * associated setting is not an @@INCLUDE, or if the @@INCLUDE
          * refers to a non-existant or unreadable file.
          *
          * _setting_name should be something like FUNC_X_CONSTRUCTOR or
          * FUNC_X_MEMBER_DECL, etc.
          */
    public String[] getAtAtIncludeContent(String _setting_name) {
        String[] content = null;

        File include_file = determineAtAtIncludeFile(_setting_name);

        if ((include_file != null) && include_file.exists()) {
            content = MiscTools.readSmallTextFile(include_file);
        }
        else {
/*
                // error conditions
            String error_message = "Problem opening include file in " + 
                generateFullNodePathName() + " for setting: " + 
                _setting_name +"."; 
            System.err.println(error_message);
*/                              
        }

        return content;
    }

        /** This method determines where an @@INCLUDE file should exist.
          * _setting_name should be something like FUNC_X_CONSTRUCTOR,
          * FUNC_X_MEMBER_DECL, etc.
          *
          * WARNING!  A non-null return value does not mean that the file
          *           exists already!  Use File.exists() to determine actual
          *           file existence if needed.
          */
    File determineAtAtIncludeFile(String _setting_name) {

        File include_file = null;

        Object object_with_include_value =
                               getAtAtIncludeSettingValueObject(_setting_name);
        if (object_with_include_value != null) {
            if (object_with_include_value instanceof ParameterSetting) {
                ParameterSetting setting =
                            (ParameterSetting) object_with_include_value;

                String file_name = parseAtAtIncludeLine(setting.getValue());
                ALFINode direct_container_node=setting.getDirectContainerNode();
                    // get the box which contains said FUNC_X node
                ALFINode parent_box = direct_container_node.containerNode_get();

                    // find the root container of this box.  this box will be
                    // associated with the box file which would actually contain
                    // the line "@@INCLUDE..."  such an included file should
                    // exist in the same directory
                ALFIFile box_file =
                             parent_box.containerNode_getRoot().getSourceFile();

                File base_search_directory = box_file.getCanonicalDirectory();

                include_file = new File(base_search_directory, file_name);
            }
        }

        return include_file;
    }

        /** Parses an @@INCLUDE line and returns the file name.  Expects a
          * single line with the format:
          *
          *   @@INCLUDE file_name
          *
          * Returns null if the line is not a properly formatted @@INCLUDE line.
          */
    String parseAtAtIncludeLine(String _line) {
        String[] parts = _line.split("\\s+");
        if ((parts.length == 2) && parts[0].equals(AT_AT_INCLUDE)) {
            return parts[1];
        }
        else { return null; }
    }


        /**
         * Determines the showInstanceSettings parameter value.   
         * If it's not set in this node, the base nodes are searched
         * until a value is found
         */
    private boolean determineInstanceParameterSetting (ALFINode _node) {
        boolean instance_params_only = false;

        ALFINode node = this;
            
        while (node != null) {
            if (node instanceof FUNCX_Node){
                FUNCX_Node funcx_node = (FUNCX_Node) node;
                ParameterSetting[] local_settings = 
                                       funcx_node.parameterSettings_getLocal();

                for (int ii = 0; ii < local_settings.length; ii++) {
                    if (local_settings[ii].getName().equals (
                        ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG)) {

                        if (local_settings[ii].getValue().equals("true")) {
                            m_default_instance_setting = false;;
                            return true;
                        }
                        else if (local_settings[ii].getValue().equals("false")) {
                            m_default_instance_setting = false;;
                            return false; 
                        }
                    }
                }


                if (parameterDeclaration_containsLocal(
                        ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG)) {
                    ParameterDeclaration instance_flag = 
                        parameterDeclaration_getLocal(
                        ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG); 
                    if (instance_flag.getValue().equals("true")) {
                        m_default_instance_setting = true;;
                        return true;
                    }
                    else if (instance_flag.getValue().equals("false")) {
                        m_default_instance_setting = true;;
                        return false; 
                    }
                } 
            }
            node = node.baseNode_getDirect();
        }

        return instance_params_only;
    }


        /**
         * Parse the parameter declaration based on the
         * MemberDecl value
         */
    public boolean processParameterSetting_MemberDecl (String _value) {

        String value = new String(_value);
/*
        // Check if the ParameterSetting value is an @@INCLUDE
        if (_value.startsWith(AT_AT_INCLUDE)) {
            value = determineAtAtSettingValue(FUNC_X_MEMBER_DECL);
            if (value == null || value.length() == 0) {
                value = _value;
            }
        }
*/    
        String name;
        String type;
        String initial_value;
        boolean b_processed = false;
        int start_location = value.indexOf(FUNC_X_MEMBER_VAR_START, 0);
        String variable_section;  

        if (start_location >= 0) {
            start_location += FUNC_X_MEMBER_VAR_START.length();

            int end_location = value.indexOf(FUNC_X_MEMBER_VAR_END, 0);

            variable_section = value.substring(start_location, end_location);
                
            StringTokenizer line_token =
                              new StringTokenizer(variable_section, Parser.NL);
            String line;

                // Parse the value line by line
            while (line_token.hasMoreTokens()) {

                name = "";
                type = "";
                initial_value = "";
                line = line_token.nextToken();
            
                StringTokenizer tokens = new StringTokenizer(line);
                    //Parse the type
                if (tokens.hasMoreTokens()) {
                    type = tokens.nextToken();
                }

                    // Parse the variable name
                if (tokens.hasMoreTokens()) {
                    // Remove semicolon at the end
                    StringBuffer name_buffer = 
                        new StringBuffer(tokens.nextToken());
                    int semicolon_index = name_buffer.indexOf(";"); 
                    if (semicolon_index >= 0) {
                        name_buffer.delete(semicolon_index, 
                            name_buffer.length());
                    }
                    name = name_buffer.toString();
                    name.trim();
                }
                    
                    // Parse the initial value
                StringBuffer init_val_search = new StringBuffer(line); 
                int init_val_comment_index = init_val_search.indexOf
                    (FUNC_X_MEMBER_INIT);
                boolean has_default_value = true;

                if (init_val_comment_index >= 0) {
                    int start = init_val_comment_index 
                              + FUNC_X_MEMBER_INIT.length();
                    int end = init_val_search.indexOf(Parser.NL);
                        // If can't find the end-of-line character, 
                        // just get the end of the line
                    if (end < 0) {
                        end = line.length() - 1;
                    }
                    initial_value = init_val_search.substring(start, end);
                    initial_value = initial_value.trim();
                    has_default_value = false;
                }

                // Now search for a default value
                String base_default_value = initial_value;
                if (initial_value.length() == 0) {
                    base_default_value = 
                        parameterSetting_findDefaultValue(name, type);
                    has_default_value = true;
                }
                else {
                    String dummy =
                        parameterSetting_findDefaultValue(name, type);
                    if (dummy.length() > 0) {
                        if (dummy.equals(initial_value)) {
                            has_default_value = true;
                        }    
                        else {
                            base_default_value = dummy;
                        }
                    }
                }

//System.out.println("In processParameterSetting_memberDecl " + generateFullNodePathName());
//System.out.println("Initial value for " + name +" = "+ initial_value);
//System.out.println("Base Default Value " + name +" = "+ base_default_value);
                if ( (initial_value.length() > 0) &&
                     (!initial_value.equals(base_default_value))|| 
                      isRootNode()) {
                    // Add this entry to the list of ParameterDeclarations
                    parameterDeclaration_addSpecialVariables(type, name,
                                                         initial_value); 
                    // ...and to the ParameterSettings
                    parameterSetting_addSpecialVariables(name, type, initial_value,
                                                            has_default_value);
                }

                b_processed = true;
            }
        }

        return b_processed;
    }

        /**
         * Parse the parameter settings from the Constructor value
         */
    public boolean processParameterSetting_Constructor (String _value) {

        String value = new String(_value);
        // Check if the ParameterSetting value is an @@INCLUDE
        if (_value.startsWith(AT_AT_INCLUDE)) {
            value = determineAtAtSettingValue(FUNC_X_CONSTRUCTOR);
            if (value == null || value.length() == 0) {
                System.err.println("Constructor: Error processing " + _value);
                value = _value;
            }
        }
        boolean b_processed = false;
        int start_location = value.indexOf(FUNC_X_CTOR_INIT_START, 0);

        if (start_location >= 0) {
            start_location += FUNC_X_CTOR_INIT_START.length();

            int end_location = value.indexOf(FUNC_X_CTOR_INIT_END, 0);

            String variable_section = 
                value.substring(start_location, end_location);
                
            StringTokenizer line_token =
                              new StringTokenizer(variable_section, Parser.NL);
            String line;

                // Parse the value line by line
            while (line_token.hasMoreTokens()) {

                String name = "";
                String parm_value = "";
                line = line_token.nextToken();
            
                StringTokenizer tokens = new StringTokenizer(line);
                    //Parse the variable name
                if (tokens.hasMoreTokens()) {
                    name = tokens.nextToken();
                }

                    // Parse the value 
                if (tokens.hasMoreTokens()) {
                    // Remove the equals sign in the beginning
                    if (tokens.nextToken().equals("=")) {

                        // Remove semicolon at the end
                        StringBuffer value_buffer = 
                            new StringBuffer(tokens.nextToken());
                        int semicolon_index = value_buffer.indexOf(";"); 
                        if (semicolon_index >= 0) {
                            value_buffer.delete(semicolon_index, value_buffer.length());
                        }
                        parm_value = value_buffer.toString();
                        parm_value.trim();
                    }
                }
                    
                // Check if the parameterSetting is already in the list.  This would
                // have been previously added in the MemberDecl section if it is
                // declared in this object.   Otherwise if the parameterSetting
                // is inherited from a base object, the value can be changed in
                // this Constructor section.
                if (!parameterSetting_containsLocal(name)) { 
                
                    // Find the parameterDeclaration 
                    ALFINode node = parameterDeclaration_find(name);
                    if (node != null) {
                        ParameterDeclaration decl = node.parameterDeclaration_getLocal(name);
                        String decl_type = decl.getType(); 
                        parameterSetting_addSpecialVariables(name, decl_type, parm_value, false);
                    }
                }

                b_processed = true;
            }
        }

        return b_processed;
    }

        /**
         * Finds a local MemberDecl declaration and initializes the 
         * lists of ParameterDeclarations and ParameterSettings
         */
    public void initializeLocalFuncXVariable () {
        clearLists();
   
        // Find the direct base node which contains FUNCX variables
        if (parameterSetting_containsLocal(FUNC_X_MEMBER_DECL)) {

            ParameterSetting[] local_settings= parameterSettings_getLocal();

            for (int ii = 0; ii < local_settings.length; ii++) {
               if (local_settings[ii].getName().equals(FUNC_X_MEMBER_DECL)){
                    String value = local_settings[ii].getValue();
                    if (value.length() > 0) {
                        processParameterSetting_MemberDecl(value);
                        break;
                    }
               }
            }
        }
        else {
            ALFINode node = parameterDeclaration_find(FUNC_X_MEMBER_DECL);

            if (node != null && !node.isRootNode()) {
                ParameterDeclaration member_decl = 
                   ((ALFINode)node).parameterDeclaration_getLocal(FUNC_X_MEMBER_DECL); 
                String member_decl_value = member_decl.getValue();
        
                if (member_decl_value.length() > 0) {
                    processParameterSetting_MemberDecl(member_decl_value);
                }
            }
        } 

    }

    ///////////////////////////////////////////
    // parameter declaration methods //////////
    ///////////////////////////////////////////

        /**
         * Finds out if the parameter is in the list of the 
         * ParameterDeclarations in the node's hierarchy.
         */
    public boolean parameterDeclaration_contains (String _parameter_name) {
        ALFINode node = parameterDeclaration_find(_parameter_name);
        if ( (node != null) && (node.parameterDeclaration_containsLocal(
                                                              _parameter_name)))  
        { return true; }
        else if (parameterDeclaration_containsLocalFuncXVariable(
                                                               _parameter_name)) 
        { return true; }
        else if (parameterDeclaration_containsFuncXVariable(_parameter_name)) 
        { return true; }
        else 
        { return false; }

    }

        /**
         * Returns an array of all ParameterDeclaration objects which have been
         * added to the node.  Returns null if called by anything
         * else.
         */
    public ParameterDeclaration[] parameterDeclarations_getLocal() {

        ArrayList parameter_declaration = new ArrayList();
        
        if (m_funcx_param_decl_list != null) {
            parameter_declaration.addAll(m_funcx_param_decl_list);
        }
        if (m_parameter_declaration_list != null) {
            parameter_declaration.addAll(m_parameter_declaration_list);
        }

        if (parameter_declaration.size() == 0 ) {
            return new ParameterDeclaration[0];
        }
        else {
            ParameterDeclaration[] pd_array =
                  new ParameterDeclaration[parameter_declaration.size()];
            parameter_declaration.toArray(pd_array);
            return pd_array;
        }
    }

        /**
         * Returns an array of all ParameterDeclaration objects which have been
         * added to the node.  Returns null if called by anything
         * else.
         */
    public ParameterDeclaration[] 
            parameterDeclarations_getLocal(boolean _no_instance_settings) {

        if (_no_instance_settings) {
            return super.parameterDeclarations_getLocal();
        }
        else {
            return parameterDeclarations_getLocal();
        }
    }

        /**
         * @return an array of ParameterDeclaration objects which have been
         * added to the FUNCX_Node. 
         */
    public ParameterDeclaration[] parameterDeclarations_getLocalFuncXVariable() {
        if (m_funcx_param_decl_list == null) {
            return new ParameterDeclaration[0];
        }
        else {  
            ParameterDeclaration[] pd_array =
              new ParameterDeclaration[m_funcx_param_decl_list.size()];
            m_funcx_param_decl_list.toArray(pd_array);
            return pd_array;
         }
    }
 
        
        /**
         * Checks if a local parameter declaration exists in this primitive.  
         */
    public boolean parameterDeclaration_containsLocal (String _parameter_name) {
        ParameterDeclaration[] pd_array = parameterDeclarations_getLocal();
        for (int i = 0; i < pd_array.length; i++) {
            if ( pd_array[i].getName().equals(_parameter_name) )
            { return true; }
        }
        return false;
    }

        /**
         * Checks if a local instance parameter declaration exists in 
         * this primitive.  
         */
    public boolean parameterDeclaration_containsLocalFuncXVariable
                                                      (String _parameter_name) {
        ParameterDeclaration[] pd_array = parameterDeclarations_getLocal();
        for (int i = 0; i < pd_array.length; i++) {
            if ( ( pd_array[i].getName().equals(_parameter_name) &&
                 (pd_array[i].isInstanceParameter()))) 
            { return true; }
        }
        return false;
    }

        /**
         * Retrieves the index of parameter in the ParameterDeclaration 
         * list. 
         * @return -1 if it's not in the ParameterDeclaration list.  Otherwise,
         * it returns the index. 
         */
    public int parameterDeclaration_getLocalFuncXVariableIndex
                                        (String _parameter_name, String _type) {
        // If the m_funcx_param_decl_list is empty, check if the MemberDecl 
        // exists, then parse the contents.

        ParameterDeclaration[] pd_array = 
            parameterDeclarations_getLocalFuncXVariable();
        for (int i = 0; i < pd_array.length; i++) {
            if (  pd_array[i].getName().equals(_parameter_name) &&
                  pd_array[i].getType().equals(_type) )
            { return i; }
        }
        
        return -1;
    }

        /**
         * @return the ParameterDeclaration object for the parameter requested.
         */
    public ParameterDeclaration parameterDeclaration_getFuncXVariable
                                                      (String _parameter_name) {

        ALFINode node = this;
        while (node != null) {
            if (node instanceof FUNCX_Node) {
                FUNCX_Node funcx_node = (FUNCX_Node) node;
                if (funcx_node.m_funcx_param_decl_list != null)  {
                    if (funcx_node.m_funcx_param_decl_list.size() > 0) {
                        ParameterDeclaration[] pd_array =
                            new ParameterDeclaration[funcx_node.m_funcx_param_decl_list.size()];
                        funcx_node.m_funcx_param_decl_list.toArray(pd_array);
                        for (int ii = 0; ii < pd_array.length; ii++) {
                            if ( pd_array[ii].getName().equals(_parameter_name)) {
                                return pd_array[ii];
                            }
                        }
                    }
                }
            }
            node = node.baseNode_getDirect();
        }
        return null;
    }

        /**
         * Checks if the parameter is a FUNC_X instance variable in
         * this node's hierarchy
         */
    public boolean parameterDeclaration_containsFuncXVariable   
                                                      (String _parameter_name) {
        ALFINode node = this;
        while (node != null) {
            if (node instanceof FUNCX_Node) {
                FUNCX_Node funcx_node = (FUNCX_Node) node;
                if (funcx_node.parameterDeclaration_containsLocalFuncXVariable
                                                    (_parameter_name)) 
                { return true; }
            }
            node = node.baseNode_getDirect();
        }
        return false;
        
    }

        /**
         * Creates an instance ParameterDeclaration and adds the object to
         * the list.
         */
    public void parameterDeclaration_addSpecialVariables
                         (String _type, String _parameter_name, String _value) {

        if (m_funcx_param_decl_list == null) {
             m_funcx_param_decl_list = new ArrayList();
        }

        int index = -1;
        if (parameterDeclaration_containsLocalFuncXVariable(_parameter_name)){
            index = parameterDeclaration_getLocalFuncXVariableIndex(
                _parameter_name, _type);

        }

        ParameterDeclaration pd = new ParameterDeclaration(
                _type, _parameter_name, _value, "", true);

        if (index >= 0) {
            m_funcx_param_decl_list.remove(index);
            m_funcx_param_decl_list.add(index, pd);
        }
        else {
            m_funcx_param_decl_list.add(pd);
        }
        //setTimeLastModified();
        
    }

        /**
         * Creates a list of FUNC_X ParameterDeclaration objects based on
         * the base nodes.
         * @return true if a FUNC_X ParameterDeclaration list is created.
         */
    public boolean parameterDeclaration_constructList ( ) {

        // First check this node for a MemberDecl parameterSetting 
        if (parameterSetting_containsLocal( FUNC_X_MEMBER_DECL)) {
            ParameterSetting[] funcx_settings = 
                                       parameterSettings_getLocalFuncXVariable();
            ParameterDeclaration[] funcx_params = 
                                       parameterDeclarations_getLocalFuncXVariable();

        }
        
        
        ALFINode base_node =  baseNode_getDirect();

        while (base_node != null) {
            if (base_node instanceof FUNCX_Node) {
                FUNCX_Node funcx_base_node = (FUNCX_Node) base_node; 

                if (funcx_base_node.m_funcx_param_decl_list != null ) {
                    //parameterDeclaration_copyList(funcx_base_node);
                    mb_param_decl_list_constructed = true;
                    return true;
                }
                else {            
                    if (funcx_base_node.parameterSetting_containsLocal(
                                                     FUNC_X_MEMBER_DECL)) {
                        //parameterDeclaration_copyList(funcx_base_node);
                        //parameterSetting_copyList(funcx_base_node);
                        funcx_base_node.setInstanceParametersOnly(false);
                        mb_param_decl_list_constructed = true;
                        return true;
                    }
                }
            }
            base_node = base_node.baseNode_getDirect();
        }

        return false;
    }

        /**
         * Copies the list of FUNC_X ParameterDeclaration objects 
         * from the base node specified.
         */
    public void parameterDeclaration_copyList (FUNCX_Node _base_node) {
        if (m_funcx_param_decl_list == null) {
            m_funcx_param_decl_list = new ArrayList();
        }
        else { 
            m_funcx_param_decl_list.clear();
        }
                    
        if (_base_node.m_funcx_param_decl_list != null) {
            for (int i = 0; i < _base_node.m_funcx_param_decl_list.size(); i++){
                ParameterDeclaration pd = (ParameterDeclaration) 
                                    _base_node.m_funcx_param_decl_list.get(i);
            
                ParameterDeclaration new_pd = new ParameterDeclaration(
                                    pd.getType(), pd.getName(), pd.getValue(), 
                                    pd.getComment(), pd.isInstanceParameter());
            
                m_funcx_param_decl_list.add(new_pd);  
            }
        }
    }

    //////////////////////////////////////////////////
    // parameter value modification methods //////////

        /** Checks if the ParameterSetting object is defined locally */
    public boolean parameterSetting_containsLocal (String _parameter_name) {
        ParameterSetting[] settings = parameterSettings_getLocal();
        for (int i = 0; i < settings.length; i++) {
            if (_parameter_name.equals(settings[i].getName())) { return true; }
        }
        return false;
    }

        /** Add a parameter value modification to this instance of a primitive.
          * Include a comment.
          *
          * [Comments not yet supported.]
          */
    public ParameterSetting parameterSetting_add (String _name, String _value,
                                          String _comment, boolean _b_quoted) {
        return parameterSetting_add(_name, _value, _comment, _b_quoted, false);
    }

        /* Add parameter value modification to this instance of a primitive. */
    public ParameterSetting parameterSetting_add (String _name, String _value,
                String _comment, boolean _b_quoted, boolean _b_default_value) {
//System.out.println("(" + this.generateFullNodePathName() + ").parameterSetting_add(" + _name + " = " + _value + ")");
    
 
        ParameterSetting ps_to_return;

        String param_value = new String(_value);
        if (param_value.length() > 0) { param_value = param_value.trim(); }

        // Find out if the parameter setting is an instance parameter
        // (i.e. parsed from the MemberDecl or Constructor settings) 
        boolean instance_parameter = false;
        ParameterDeclaration pd = null;

        FUNCX_Node node = ( FUNCX_Node )parameterDeclaration_find(_name);
        if (node != null) {
            // If it setting's parameter declaration is a local funcx variable
            // then it is an instance parameter
            instance_parameter = 
                    node.parameterDeclaration_containsLocalFuncXVariable(_name);
          
            if (instance_parameter) {
               pd = node.parameterDeclaration_getFuncXVariable(_name);
               if (pd == null) {instance_parameter = false;}
            }
        }      
        if (!instance_parameter) {
            ps_to_return =  
                 parameterSetting_addNormal(_name, param_value, _comment, _b_quoted);
            
            // Now check if this setting has an @@INCLUDE value
            if (param_value.startsWith(AT_AT_INCLUDE)) {
                String new_value = determineAtAtSettingValue(_name);    
                if (new_value == null) {
System.out.println("COULD NOT determineAtAtSettingValue");
                }
                if (new_value != null && new_value.length() > 0){
                    parameterSetting_add(_name, new_value, _comment, _b_quoted);
                }
            }
            return parameterSetting_addNormal(_name, param_value, _comment, _b_quoted);
        }
        else {
                // add Instance Parameter 
            if (pd != null) {
                String type = pd.getType();
                if (param_value.length() == 0) {
                    param_value = pd.getValue();
                }
                return parameterSetting_addSpecialVariables(_name, type,
                                                  param_value, _b_default_value);
            }
            else {
System.out.println("DID NOT find a parameter declaration for the instance");
                return null;
            }
        }

    }

        /** Adds a parameter value modification which is not a FUNC_X instance 
          * variable to the node
          */
    private ParameterSetting parameterSetting_addNormal (String _name,
                           String _value, String _comment, boolean _b_quoted) {

        return parameterSetting_addNormal(_name, _value,
                                          _comment, _b_quoted, true);
    }

        /** Adds a parameter value modification which is not a FUNC_X instance 
          * variable to the node.   
          * @param _b_process_special_variables  
          *                   if a MemberDecl is added, parse the value and
          *                   process any FUNC_X ParameterSettings
          */
    private ParameterSetting parameterSetting_addNormal (String _name,
                     String _value, String _comment,
                     boolean _b_quoted, boolean _b_process_special_variables) {

        if ((_name != null) && (_name.length() != 0)) {
                // remove any already existing local mod with this name
            if (parameterSetting_containsLocal(_name)) {
                parameterSetting_removeLocal(_name);
            }

            if (_value == null) { _value = ""; }
            if (_comment == null) { _comment = ""; }
        
            if (_name.equals(FUNC_X_MEMBER_DECL) &&
                                                _b_process_special_variables) {
                processParameterSetting_MemberDecl(_value);
            }
    
            ParameterSetting setting =
                          new ParameterSetting(this, _name, _value, _b_quoted);
            if (setting != null) {
                if (m_parameter_setting_list == null) {
                    m_parameter_setting_list = new ArrayList();
                    m_container_list_map.put(PARAMETER_SETTINGS,
                                                     m_parameter_setting_list);
                }
                m_parameter_setting_list.add(setting);
                setTimeLastModified();

                ContainedElementModEvent e = new ContainedElementModEvent(this);
                fireContainedElementModEvent(e);
            }
            return setting;
        }
    else {
                // error conditions
            String error_message = generateFullNodePathName() +
                ".parameterSetting_add(" + _name + ", " + _value +
                ", " + _comment + ", " + _b_quoted + ") :\n";
            if (_name == null) {
                error_message += "\tERROR: null parameter name passed.";
            }
            else if (_name.length() == 0) {
                error_message += "\tERROR: Zero length parameter name passed.";
            }
            System.err.println(error_message);
            return null;
        }
    }

        /**
         * When a primitive instance exists in this node, this method
         * composes the MemberDecl and Constructor primitive settings 
         * and adds them to the node.
         */
    public void parameterSetting_addAll ( ) {

        // No need to do anything if there are no instance parameters
        if (!isInstanceParametersOnly ()) { return; }
        
        ParameterSetting[] settings = parameterSettings_getLocal();

        boolean modified_member_decl = false;
        boolean has_definition = parameterSetting_hasDefinition();

        
        for (int ii = 0; ii < settings.length; ii++) {
            if (settings[ii].getName().equals(FUNC_X_MEMBER_DECL)) {
                String value = settings[ii].getValue();
                String comment = settings[ii].getComment();
                boolean is_quoted = settings[ii].isQuoted();
                String new_value = settingReplace_memberDecl(value);
                if (new_value.length() > 0) {
                    parameterSetting_removeLocal(FUNC_X_MEMBER_DECL);
                    parameterSetting_addNormal(FUNC_X_MEMBER_DECL, new_value,
                                        comment, is_quoted, false);
                    modified_member_decl = true;
                }
            }
        }

        
        if (!modified_member_decl && has_definition) {
            // Find the base node's MemberDecl value baseNode_getRoot() 
            ParameterDeclaration base_member_decl  =  baseNode_getRoot().
                              parameterDeclaration_getLocal(FUNC_X_MEMBER_DECL);
            String type = base_member_decl.getType();
        
            String base_value = "   ";
            boolean found_base_value;
            ParameterSetting member_decl = 
                                  parameterSetting_get(FUNC_X_MEMBER_DECL);
            if (member_decl != null) {
                base_value = member_decl.getValue();  
            }

            String new_value = settingReplace_memberDecl(base_value);
            new_value = new_value.trim();
            if (new_value.length() > 0) {
                parameterSetting_addNormal(FUNC_X_MEMBER_DECL, new_value, "", 
                                                                   true, false);
            }
        } 

        clearLists();
    }

        /** Adds a FUNCX_ParameterSetting to the node. */
    public ParameterSetting parameterSetting_addSpecialVariables (String _name,
                       String _type, String _value, boolean _b_default_value) {

        if ((_name != null) && (_name.length() != 0)) {

            if (m_funcx_setting_list == null) {
                m_funcx_setting_list = new ArrayList();
            }

            int index = 
                    parameterDeclaration_getLocalFuncXVariableIndex(_name, _type); 
            boolean old_value_is_default = false;

            if (index >= 0 && (m_funcx_setting_list.size() > index)) { 
                FUNCX_ParameterSetting old_setting =
                      (FUNCX_ParameterSetting) m_funcx_setting_list.get(index);
                old_value_is_default = old_setting.isDefaultValue();
            }

            // remove any already existing local mod with this name
            // Don't remove a defined value if adding a default value
            if (parameterSetting_containsLocalFuncXVariable(_name)) {
                // Remove if we're defining a PVM
                parameterSetting_removeSpecialVariable(_name);
            }

            FUNCX_ParameterSetting setting = new FUNCX_ParameterSetting(this,
                                       _name, _type, _value, _b_default_value);

            if (setting != null) {
                m_funcx_setting_list.add(setting); 
            }
            return setting;
        }
        else {
                // error conditions
            String error_message = generateFullNodePathName() +
                ".parameterSetting_addSpecialVariables(" + _name + ", "
                + _value + ") :\n";
            if (_name == null) {
                error_message += "\tERROR: null parameter name passed.";
            }
            else if (_name.length() == 0) {
                error_message += "\tERROR: Zero length parameter name passed.";
            }
            System.err.println(error_message);
            return null;
        }
    }


        /** Retrieves a list of all ParameterSettings in this
          * node's hierarchy tree.   
          */
    public ParameterSetting[] parameterValueMods_get () {
        ArrayList setting_list = new ArrayList();
        ALFINode node = this;
        while (node != null) {
            ArrayList local_setting_list = node.m_parameter_setting_list;
            if (local_setting_list != null) {
                setting_list.addAll(local_setting_list);
            }

            if (node instanceof FUNCX_Node) {
                FUNCX_Node funcx_node = (FUNCX_Node) node;
                ArrayList local_funcx_setting_list = 
                                               funcx_node.m_funcx_setting_list;

                if (local_funcx_setting_list != null) {
                    setting_list.addAll(local_funcx_setting_list);
                }
            }
            node = node.baseNode_getDirect();
        }

        ParameterSetting[] settings = new ParameterSetting[setting_list.size()];
        setting_list.toArray(settings);
        return settings;
    }

        /** Calls the proper overloaded method */
    private ParameterSetting[] parameterValueMods_getInherited () {
        ALFINode base = baseNode_getDirect();
        if (base != null) {
            if (base instanceof FUNCX_Node) {
                return ((FUNCX_Node) base).parameterValueMods_get();
            }
            else {
                return  base.parameterSettings_get();
            }
        }
        else { return new ParameterSetting[0]; }
    }

        /** Gets the list of all added ParameterSettings.
           * For this object, it includes the instance parameters 
           * (FUNCX_ParameterSetting objects)
           */
    public ParameterSetting[] parameterSettings_getLocal () {

        ArrayList setting_list = new ArrayList();

        if (m_funcx_setting_list != null) {
            setting_list.addAll(m_funcx_setting_list);
        }
            

        if (m_parameter_setting_list != null) {
            setting_list.addAll(m_parameter_setting_list);
        }

        ParameterSetting[] settings = new ParameterSetting[setting_list.size()];

        setting_list.toArray(settings);
        return settings;
        
    }

        /** @return an array of FUNCX_ParameterSetting objects 
          * locally added to this instance.
          */
    public ParameterSetting[] parameterSettings_getLocalFuncXVariable () {

        ArrayList setting_list = new ArrayList();

        if (m_funcx_setting_list != null) {
            setting_list.addAll(m_funcx_setting_list);
        }
        if (parameterSetting_containsLocal(
                        ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG)) {
            setting_list.add(parameterSetting_get(
                        ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG));
        }
        if (parameterSetting_containsLocal(
                        ParameterDeclaration.FILE_INCLUDE)){
            setting_list.add(parameterSetting_get(
                        ParameterDeclaration.FILE_INCLUDE));
        }
         
        ParameterSetting[] settings = new ParameterSetting[setting_list.size()];

        setting_list.toArray(settings);
        return settings;
        
    }


        /** @return an array of NON-FUNCX_ParameterSetting objects 
          */
    public ParameterSetting[] parameterSettings_getLocalNormalSettings() {

        ArrayList setting_list = new ArrayList();

        if (m_parameter_setting_list != null) {
            setting_list.addAll(m_parameter_setting_list);
        }
        
        ParameterSetting[] settings = new ParameterSetting[setting_list.size()];

        setting_list.toArray(settings);
        return settings;
        
    }

        /** 
          * Checks if the parameterSetting exists locally in this setting.
          */
    public boolean parameterSetting_containsLocalFuncXVariable 
                                                       (String _parameter_name){

        ParameterSetting[] settings = parameterSettings_getLocalFuncXVariable();
        for (int i = 0; i < settings.length; i++) {
            if ( settings[i].getName().equals(_parameter_name) )
            { return true; }
        }
        return false;
    }

        /** Removes a locally added ParameterSetting from this
          * primitive instance.  Inherited mods cannot be removed, only
          * overridden by a local add.
          */
    public boolean parameterSetting_removeLocal (String _parameter_name) {

        boolean instance_parameter = false;

        ParameterSetting setting = parameterSetting_get(_parameter_name);
        if (setting instanceof FUNCX_ParameterSetting) {
            instance_parameter = true;
        }
        if (!instance_parameter) {
            super.parameterSetting_removeLocal(_parameter_name);
        }
        else {
            parameterSetting_setToDefault (_parameter_name);
            return true;
        }
        return false;
    }

        /**
         * Removes the instance variable from the list of this node's
         * FUNCX_ParameterSettings
         */
    public int parameterSetting_removeSpecialVariable 
                                                      (String _parameter_name) {
        ParameterSetting[] local_settings =
                                     parameterSettings_getLocalFuncXVariable();
        for (int i = 0; i < local_settings.length; i++) {
            if (local_settings[i].getName().equals(_parameter_name)) {
                ParameterSetting setting_to_remove = local_settings[i];
                boolean b_modified = 
                    m_funcx_setting_list.remove(setting_to_remove);
                if (b_modified) {
                    setTimeLastModified();

                    ContainedElementModEvent e =
                                new ContainedElementModEvent(this);
                    fireContainedElementModEvent(e);

                    return i;
                }
            }
        }

        // error condition
        System.err.println(generateFullNodePathName() +
                ".parameterSetting_removeSpecialVariable(" + _parameter_name +
                                                                      ") :\n" +
                "\tERROR: Attempting to remove parameter value modification " +
                "not found in the local add list.");
         return -1;
    }
        /**
         * This method creates a list of FUNCX_ParameterSettings
         * containing the default values.
         */
    public ParameterSetting[] parameterSetting_getDefaultList () {

        if (!isInstanceParametersOnly ()) { 
            ArrayList setting_list = new ArrayList();
            if (m_parameter_setting_list != null) {
                setting_list.addAll(m_parameter_setting_list);
            }

            ParameterSetting[] settings = new ParameterSetting[setting_list.size()];

            setting_list.toArray(settings);
            return settings;
        }

        if (m_funcx_default_setting_list == null) {
            m_funcx_default_setting_list = new ArrayList();
        }
    
        if (m_funcx_default_setting_list.size() == 0) {
            ALFINode[] base_nodes = baseNodes_getHierarchyPath();
    
            if (base_nodes.length <= 1) {return new ParameterSetting[0]; }
    
            for (int ii = (base_nodes.length - 1); ii > 0; ii--) {
                if ( isBasedOn(base_nodes[ii]) && 
                    (! base_nodes[ii].isRootNode())&&
                    (base_nodes[ii] != this)) {
                        
                    if (base_nodes[ii] instanceof FUNCX_Node) { 
                        FUNCX_Node funcx_node = (FUNCX_Node) base_nodes[ii];
                 
                        if (funcx_node.m_funcx_setting_list != null) {
                            for (int jj = 0;
                                    jj < funcx_node.m_funcx_setting_list.size();
                                                                         jj++) {
                                FUNCX_ParameterSetting setting = 
                                   (FUNCX_ParameterSetting) 
                                       funcx_node.m_funcx_setting_list.get(jj);

                                String name = setting.getName();
                                String type = setting.getType();
                                String value = setting.getValue();
                                boolean b_default = true;

                                boolean exists = false;

                                // Check if this entry exists in the ArrayList
                                for (int kk = 0;
                                     kk < m_funcx_default_setting_list.size();
                                                                        kk++) {
                                ParameterSetting default_setting = 
                                    (ParameterSetting) 
                                          m_funcx_default_setting_list.get(kk);

                                if (setting instanceof FUNCX_ParameterSetting) {
                                    FUNCX_ParameterSetting funcx_setting =
                                       (FUNCX_ParameterSetting) default_setting;

                                    if (funcx_setting.getName().equals(name) &&
                                        funcx_setting.getType().equals(type)) { 
                                        exists = true;
                                    }
                                }
                            }

                            if (!exists) {
                                FUNCX_ParameterSetting new_setting =
                                    new FUNCX_ParameterSetting(this, name,
                                                       type, value, b_default);
            
                                m_funcx_default_setting_list.add(new_setting);
                            }
                        }
                        }
                    }
                }
            }
        }

        ParameterSetting[] settings =
                     new ParameterSetting[m_funcx_default_setting_list.size()];
        m_funcx_default_setting_list.toArray(settings);
        return settings;
    }

        /** The FUNCX_ParameterSettings can't really be removed
          * just reset to its default value
          */
    public ParameterSetting parameterSetting_setToDefault (
                                                      String _parameter_name) {
        ParameterDeclaration pd = 
                    parameterDeclaration_getFuncXVariable(_parameter_name);

        if (pd != null) {
            String type = pd.getType();
            String value = pd.getValue();
            return parameterSetting_addSpecialVariables(_parameter_name, type, 
                    value, true);
        }
        return null;
        
    }

        /** Copies the list of added ParameterSettings from a base 
          * node to this node's list of default ParameterSettings .
          */
    private void parameterSetting_copyList (FUNCX_Node _base_node) {
        
        if (m_funcx_default_setting_list == null) {
            m_funcx_default_setting_list = new ArrayList();
        }

        if (_base_node.m_funcx_setting_list != null) {
            for (int i = 0; i < _base_node.m_funcx_setting_list.size(); i++) {
                FUNCX_ParameterSetting setting = (FUNCX_ParameterSetting)
                                        _base_node.m_funcx_setting_list.get(i);
            
                if (setting instanceof FUNCX_ParameterSetting) {
                    parameterSetting_addSpecialVariables(setting.getName(),
                                  setting.getType(), setting.getValue(), true); 
                }
            }
        }
    }

    private void parameterSetting_copyList (ParameterSetting[] _settings) {
        for (int i = 0; i < _settings.length; i++) {
            ParameterSetting setting = _settings[i];
            
            boolean default_value = false;
            if (setting instanceof FUNCX_ParameterSetting) {
                FUNCX_ParameterSetting funcx_setting =
                    ( FUNCX_ParameterSetting ) setting;
                default_value = funcx_setting.isDefaultValue();
                parameterSetting_addSpecialVariables(setting.getName(),
                    setting.getType(),setting.getValue(), default_value);
            }
            
        }
    }

        /** Checks if there is at least one FUNC_X ParameterValueMod
          * defined in this object.
          */
    private boolean parameterSetting_hasDefinition () {

        boolean has_definition = false;

        if (m_funcx_setting_list != null) {
            for (int i = 0; i < m_funcx_setting_list.size(); i++) {
                ParameterSetting setting =
                                (ParameterSetting) m_funcx_setting_list.get(i);
                if (setting instanceof FUNCX_ParameterSetting) {
                    FUNCX_ParameterSetting funcx_setting =
                                              (FUNCX_ParameterSetting) setting;
                    if (!funcx_setting.isDefaultValue()) {
                        has_definition = true;
                    }
                }
            }
        }
        
        return has_definition;
    }

        /** Looks for a ParameterSetting with the given name
          * and type and searches the node inheritance to find the
          * the default value.
          */
    private String parameterSetting_findDefaultValue (String _name,
                                                      String _type) {

        String default_value = "";
        ALFINode base_node = baseNode_getDirect();
        boolean found = false;

        while (base_node != null && !found) {
            if (base_node instanceof FUNCX_Node) {
                FUNCX_Node funcx_base_node = (FUNCX_Node) base_node;
                int index = funcx_base_node.
                    parameterDeclaration_getLocalFuncXVariableIndex
                        (_name, _type);
    
                if (index >= 0) {
                    ParameterDeclaration pd = funcx_base_node.
                        parameterDeclaration_getFuncXVariable(_name);
                    String pd_value = pd.getValue();
                    if (pd_value.length() > 0) {
                        default_value = pd_value;
                        if (default_value.length() == 0) {
                            default_value = pd_value;
                        }
                    found = true;
                    }
                }
            }
            base_node = base_node.baseNode_getDirect();
        }
    
        if (!found) {
            if (!mb_param_decl_list_constructed) {
                parameterDeclaration_constructList();
            }

            ParameterSetting[] default_list = parameterSetting_getDefaultList();
            for (int ii = 0; ii < default_list.length; ii++) {
                String name = default_list[ii].getName();
                String type = default_list[ii].getType();
                String value = default_list[ii].getValue();
   
                if (name.equals(_name) && type.equals(_type)) {
                    default_value = value;
                }
            }
        }

        
        return default_value;
    }

        /** 
          * Sets the flag that displays only the instance parameters
          */
    public void setInstanceParametersOnly (boolean _flag) {
        m_instance_parameters_only = _flag; 
    }

        /** 
          * Access method to the flag to the instance parameters
          */
    public boolean isInstanceParametersOnly () {
        return m_instance_parameters_only;
    }

        /** 
          * @return the new MemberDecl FUNC_X primitive setting
          */
    private String settingReplace_memberDecl (String _old_value) {

        
//System.out.println("In settingReplace_memberDecl " + generateFullNodePathName());
//System.out.println("old value "+ _old_value);
        String old_value = new String(_old_value);
        if (_old_value.startsWith(AT_AT_INCLUDE)) {
            old_value = determineAtAtSettingValue(FUNC_X_MEMBER_DECL);
            if (old_value == null || old_value.length() == 0) {
                old_value = _old_value;
            }
        }
        int start_location = 0;
        int end_location = 0;
        String variable_section = new String();

    
        if (m_funcx_setting_list != null) {

            int marker_start = old_value.indexOf(FUNC_X_MEMBER_VAR_START, 0);
            int marker_end = old_value.indexOf(FUNC_X_MEMBER_VAR_END, 0) +
                            FUNC_X_MEMBER_VAR_END.length() + 1;
        
            end_location = marker_end;
    
            if (marker_start < 0) {
                // There were no previous autogenerated comments
                start_location = 0;
                end_location = 0;
            } else {
                start_location = marker_start;
            }
            variable_section = old_value.substring(start_location, 
                                                                  end_location);

//System.out.println("variable_section "+ variable_section);

            FUNCX_ParameterSetting[] funcx_settings =
                       new FUNCX_ParameterSetting[m_funcx_setting_list.size()];
            m_funcx_setting_list.toArray(funcx_settings);
 
            for (int i = 0; i < funcx_settings.length; i++) {
                String value = funcx_settings[i].getValue();
                boolean default_value = false;
                if (value.equals("DEFAULT")) {
                    System.out.println("Found DEFAULT value for "+ 
                        funcx_settings[i].getName());
                    value = "";
                    default_value = true;
                }
                value = value.trim();
                
                if (value.length() > 0) {
                    int from_index = 0;
                    int start_of_name = 0;
                    int end_of_name = -1;
                    boolean found = false;
                    
                    while (!found && 
                            (start_of_name != variable_section.length() -1) ){
                        String setting_name = funcx_settings[i].getName();
                        // Find the Setting Variable in the section
                        start_of_name = variable_section.indexOf(setting_name, 
                                                                  start_of_name);
                        if (start_of_name >= 0) {
                            end_of_name = variable_section.indexOf(';', 
                                                                 start_of_name);
                            String test_name = "";
                            if (end_of_name >= 0) {
                                test_name = variable_section.substring(
                                                    start_of_name-1, end_of_name);
                                test_name = test_name.trim();
                                if (test_name.equals(setting_name)) {
                                    found = true;
                                }
                            }
                            start_of_name = end_of_name;
                        }
                    }
                    if (found) {
                        int start_of_member_init = variable_section.indexOf(
                            FUNC_X_MEMBER_INIT, end_of_name);
                        StringBuffer string_buffer = new StringBuffer(
                                                              variable_section);
                        if (start_of_member_init >= 0) {
                            int start_init_value = start_of_member_init +
                                FUNC_X_MEMBER_INIT.length() + 1;
                            int end_of_init_value = variable_section.indexOf(
                                ';', start_init_value);
                            if (default_value) {
                                string_buffer = 
                                    string_buffer.replace(start_init_value,
                                                         end_of_init_value, " ");
                            }
                            else {
                                string_buffer = 
                                          string_buffer.replace(start_init_value,
                                                       end_of_init_value, value);
                            }
                        } 
                        else {
                            int new_line = variable_section.indexOf(Parser.NL);
                            if (!default_value) {
                                String new_init_value = "      " + 
                                        FUNC_X_MEMBER_INIT + "  " + value + ";";
                                string_buffer = 
                                    string_buffer.replace(end_of_name, new_line,
                                                                new_init_value);
                            }
                        }
                        variable_section = string_buffer.toString();
                    }
                }
            }

        }

        if (_old_value.length() > 0) {
            StringBuffer string_buffer = new StringBuffer(old_value);
            string_buffer = string_buffer.replace(start_location,
                        end_location, variable_section);

            return string_buffer.toString();
        }
        else {
            return "";
        }
    }

        /** 
          * @return the new Constructor FUNC_X primitive setting
          * It should look something like:
          *
             //The following code segment is generated automatically.  DO NOT EDIT
             //FUNC_X_MEMBER_INIT_START
             a = 100;
             b = 1/sqrt( 1 + pow(a,9) );
             c = b+a;
             abc = "abc";
             //FUNC_X_MEMBER_INIT_END
          *
          */
    private String settingReplace_constructor (String _old_value) {

        int start_location = 0;
        int end_location = 0;
        String new_value = FUNC_X_WARNING_NO_EDIT + Parser.NL +
                                            FUNC_X_CTOR_INIT_START + Parser.NL;

        if (m_funcx_setting_list != null) { 
            int comment_start= _old_value.indexOf(FUNC_X_WARNING_NO_EDIT, 0);
            int marker_start = _old_value.indexOf(FUNC_X_CTOR_INIT_START, 0);
            int marker_end = _old_value.indexOf(FUNC_X_CTOR_INIT_END, 0) +
                            FUNC_X_CTOR_INIT_END.length() + 1;
        
            end_location = marker_end;
    
            if (comment_start < 0 && marker_start < 0) {
                // There were no previous autogenerated comments
                start_location = 0;
                end_location = 0;
            } else if (comment_start >= 0 &&  marker_start >= 0) {
                start_location = Math.min(comment_start, marker_start);    
            } else if (comment_start < 0) {
                start_location = marker_start;
            } else {
                start_location = comment_start;
            }

            // Get all the parameter settings
            FUNCX_ParameterSetting[] funcx_settings =
                       new FUNCX_ParameterSetting[m_funcx_setting_list.size()];
            m_funcx_setting_list.toArray(funcx_settings);

            for (int i = 0; i < funcx_settings.length; i++) {
                String value = funcx_settings[i].getValue();

                if ((value != null) && (value.length() > 0) && !(value.equals("DEFAULT"))) {
                    String name = funcx_settings[i].getName();
                    new_value += name;
                    new_value += " = " + value + ";" + Parser.NL;
                }
            }
        }
        new_value += FUNC_X_CTOR_INIT_END + Parser.NL;

        StringBuffer string_buffer = new StringBuffer(_old_value);
        string_buffer = string_buffer.replace(start_location,
                    end_location, new_value);
        
        return string_buffer.toString();
    }
    
        /** Deletes all the values in the list of ParameterDeclaration
          * and ParameterSettings.
          */
    private void clearLists ( ) {

        if (isRootNode()) { return; }

        if (m_funcx_param_decl_list != null) {
            m_funcx_param_decl_list.clear();
            m_funcx_param_decl_list = null;
        }
    
        if (m_funcx_setting_list != null) {
            m_funcx_setting_list.clear(); 
            m_funcx_setting_list = null;
        }

        if (m_funcx_default_setting_list != null) {
            m_funcx_default_setting_list.clear(); 
            m_funcx_default_setting_list = null;
        }

        mb_param_decl_list_constructed = false;
    }



    //////////////////////
    // generic settings //
    //////////////////////
        /** Convenience method for dealing with ParameterSetting (for
          * primitives).
          */
    public NodeSettingIF setting_add (String _name, String _value,
                                     String _comment, boolean _b_quoted) {

        return parameterSetting_add(_name, _value, _comment, _b_quoted);
    }


        /** Convenience method for dealing with getting a list
          * of FUNCX_ParameterSetting 
          */
    public NodeSettingIF[] settings_getLocal () {
        ArrayList setting_list = new ArrayList();

        if (m_funcx_setting_list != null) {
            FUNCX_ParameterSetting[] funcx_settings =
                       new FUNCX_ParameterSetting[m_funcx_setting_list.size()];
            m_funcx_setting_list.toArray(funcx_settings);
 
            for (int i = 0; i < funcx_settings.length; i++) {
                if (!funcx_settings[i].isDefaultValue()) 
                    setting_list.add(funcx_settings[i]);
            }
        }

        if (m_parameter_setting_list != null) {
            setting_list.addAll(m_parameter_setting_list);
        }

        ParameterSetting[] settings = new ParameterSetting[setting_list.size()];

        setting_list.toArray(settings);
        return settings;
            
        
    }

        /** Performs the final steps when modifying a FUNCX_Node setting.
           * In the FUNCX_Node case, after all modifications are made
           * to the instance parameters, the primitive setting <code>
           * MemberDecl </code> and <code> Constructor </code> are updated
           * and added to the node's ParameterSetting list
           */
    public void setting_finalize () {
        if (m_instance_parameters_only) {
            parameterSetting_addAll();
        }
    }

        /** Overrides the ALFINode method to add possible FUNCX changes too. */
    public boolean hasLocalChanges(boolean _b_include_trivial_changes) {
        return ( super.hasLocalChanges(_b_include_trivial_changes)// ||
//                 ((m_funcx_param_decl_list != null) &&
//                                       (m_funcx_param_decl_list.size() > 0)) ||
//                 ((m_funcx_setting_list != null) &&
//                                          (m_funcx_setting_list.size() > 0)) ||
//                 ((m_funcx_default_setting_list != null) &&
//                                  (m_funcx_default_setting_list.size() > 0))
                 );
    }


    public String toString(boolean _b_is_long_version) {
        String node_info = super.toString(_b_is_long_version);

        if (_b_is_long_version) {
            node_info += 
                "\n\t- - - - - - - - - - FUNCX Specific - - - - - - - - - -\n" +
                "\tm_funcx_param_decl_list =" +
                                   listContentString(m_funcx_param_decl_list) +
                "\tm_funcx_setting_list =" +
                                      listContentString(m_funcx_setting_list) +
                "\tm_funcx_default_setting_list =" +
                               listContentString(m_funcx_default_setting_list);
        }

        return node_info;
    }

    private String listContentString(ArrayList _list) {
        StringBuffer sb = new StringBuffer();
        if (_list == null) {
            sb.append(" null\n");
        }
        else {
            sb.append("\n");
            for (Iterator i = _list.iterator(); i.hasNext(); ) {
                sb.append("\t\t" + i.next() + "\n");
            }
        }

        return sb.toString();
    }
}


