package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

import edu.caltech.ligo.alfi.Alfi;


    /**
     * PortProxy objects track name and positioning information when a port is
     * added to a node.  If this node is derived from to create another, the
     * same PortProxy object will inform the new node where the inherited port
     * sits and what it is named.
     */
public class PortProxy extends GenericPortProxy {

    ////////////////////////////////////////////////////////////////////////////
    /////// constants //////////////////////////////////////////////////////////

    public static final int EDGE_NORTH   = 0;
    public static final int EDGE_SOUTH   = 1;
    public static final int EDGE_EAST    = 2;
    public static final int EDGE_WEST    = 3;
    public static final int EDGE_INVALID = 4;
    public static final String[] EDGE_BY_STRING_ARRAY =
                                          { "north", "south", "east", "west" };
    public static final String[] EDGE_BY_STRING_ARRAY_OLD =
                                          { "top", "bottom", "right", "left" };

    public static final int EDGE_POSITION_INVALID = -1;

    public static final int MOVE_INCREMENT = 0;
    public static final int MOVE_DECREMENT = 1;
    public static final int PUSH_INCREMENT = 2;
    public static final int PUSH_DECREMENT = 3;
    public static final int MOVE_NORTH     = 4;
    public static final int MOVE_SOUTH     = 5;
    public static final int MOVE_EAST      = 6;
    public static final int MOVE_WEST      = 7;

    public static final int IO_TYPE_NOT_SET = 0;
    public static final int IO_TYPE_INPUT   = 1;
    public static final int IO_TYPE_OUTPUT  = 2;
    public static final int IO_TYPE_CLAMP   = 3;
    public static final int IO_TYPE_INVALID = 4;
    public static final String[] IO_TYPE_BY_STRING_ARRAY =
                                     { "not_set", "input", "output", "clamp" };

    ////////////////////////////////////////////////////////////////////////////
    /////// member fields //////////////////////////////////////////////////////

        /** The type of I/O this port is associated with (from the perspective
          * of the node it is attached to.)  Can be any of the IO_TYPE_XXX
          * static final members of this object.
          */
    private int m_io_type;

        /** The data type this port is associated with.  Can be any of the
          * DATA_TYPE_XXX static final members of superclass GenericPortProxy.
          */
    private int m_data_type;

        /** Primitive root node input ports may have a default value. */
    private String m_default_value;

        /** Tracks external connections made to this port.  (I.e. connections
          * made to this port when it appears on a member node.)
          */
    protected HashMap m_external_connections_map;

    ////////////////////////////////////////////////////////////////////////////
    /////// constructors ///////////////////////////////////////////////////////

        /** Main constructor.  Should only be called from ALFINode. */
    PortProxy(ALFINode _container_node, String _name, int _io_type,
                                                               int _data_type) {
        super(_container_node, _name);
        //assert(ioTypeIdIsValid(_io_type) && dataTypeIdIsValid(_data_type));

        m_io_type = _io_type;
        m_data_type = _data_type;
        m_default_value = null;

        m_external_connections_map = new HashMap();
    }

        /** Auxilliary constructor which creates a proxy for use by
          * ProxyChangeEvent sources.  These objects are useless elsewhere
          * because the (final) container node field is null.  These objects
          * are used only to transport change information regarding the
          * original.  Unchanged fields are set to null.
          */
    public PortProxy(String _changed_name, Integer _changed_io_type,
                   Integer _changed_data_type, String _changed_default_value) {
        super();

        int io_type = (_changed_io_type != null) ?
                             _changed_io_type.intValue() : IO_TYPE_INVALID;
        int data_type = (_changed_data_type != null) ?
                             _changed_data_type.intValue() : DATA_TYPE_INVALID;

        m_name = _changed_name;
        m_io_type = io_type;
        m_data_type = data_type;
        m_default_value = _changed_default_value;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// class methods //////////////////////////////////////////////////////

    public static int getEdgeIdFromString(String _edge) {
        for (int i = 0; i < EDGE_BY_STRING_ARRAY.length; i++) {
            if (_edge.equals(EDGE_BY_STRING_ARRAY[i]) ||
                                    _edge.equals(EDGE_BY_STRING_ARRAY_OLD[i])) {
                return i;
            }
        }
        return EDGE_INVALID;
    }
        
    public static int getIOTypeIdFromString(String _io_type_string) {
        for (int i = 0; i < IO_TYPE_BY_STRING_ARRAY.length; i++) {
            if (_io_type_string.equals(IO_TYPE_BY_STRING_ARRAY[i])) {
                return i;
            }
        }
        return IO_TYPE_INVALID;
    }

    static boolean ioTypeIdIsValid(int _io_type_id) {
        if ((_io_type_id >= 0) && (_io_type_id < IO_TYPE_INVALID)) {
            return true;
        }
        else { return false; }
    }

    static boolean dataTypeIdIsValid(int _data_type_id) { 
        return (GenericPortProxy.dataTypeIdIsValid(_data_type_id) &&
                                     (_data_type_id != DATA_TYPE_ALFI_BUNDLE));
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// implementations of abstract GenericPortProxy methods ///////////////

        /** Determines the data type which is output from this port as a source
          * for connection _connection.
          */
    public int determineSourceDataTypeForConnection(ALFINode _container,
                                                 ConnectionProxy _connection) {
        assert(_container != null);
        assert(_connection.getSource() == this);

        return m_data_type;
    }

        /** Determines the data type which is input to this port as a sink
          * for connection _connection.
          */
    public int determineSinkDataTypeForConnection(ALFINode _container,
                                                 ConnectionProxy _connection) {
        assert(_container != null);
        assert(_connection.getSink() == this);

        return m_data_type;
    }

        /** Implementation of GenericPortProxy method.  Returns an empty map
          * unless the port is a bundle port, in which case this method will
          * try to trace back to any possibly existing bundle connected to this
          * port in an upstream node (if such exists.)  The returned map will
          * have one key/value pair at most: { upstream_bundle,
          * upstream_container }.
          */
    protected HashMap getUpstreamConnections(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        HashMap container_map = new HashMap();

        ALFINode upstream_container = null;
        MemberNodeProxy upstream_owner_member = null;
        if (_owner_member == null) {
            if (! _container.isRootNode()) {
                upstream_container = _container.containerNode_get();
                upstream_owner_member =
                                    _container.memberNodeProxy_getAssociated();
            }
        }
        else {
            upstream_container = _container.memberNode_get(_owner_member);
            upstream_owner_member = null;
        }

        if (upstream_container != null) {
            ConnectionProxy[] connections =
                     getConnections(upstream_container, upstream_owner_member);
            for (int i = 0; i < connections.length; i++) {
                container_map.put(connections[i], upstream_container);
            }
        }

        return container_map;
    }

        /** Implementation of GenericPortProxy method.  Returns an empty map.
          * Is overridden in the BundlePortProxy class.
          */
    protected HashMap getDownstreamConnections(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        return new HashMap();
    }

        /** Any connections to this port on _owner_member in _container_node?
          * If _owner_member is passed as null, then the port is taken to be a
          * port on _container_node.
          */
    public boolean hasConnections(ALFINode _container_node,
                                  MemberNodeProxy _owner_member) {
        if (_owner_member == null) {
            return hasConnections(_container_node);
        }
        else {
            ConnectionProxy[] connections = _container_node.connections_get();
            for (int i = 0; i < connections.length; i++) {
                ConnectionProxy c = connections[i];
                if (    ((c.getSource() == this) &&
                               (c.getSourceMember() == _owner_member)) ||
                        ((c.getSink() == this) &&
                               (c.getSinkMember() == _owner_member)) ) {
                    return true;
                }
            }
            return false;
        }
    }

        /** Returns an array of the connections linked to this port, when
          * _owner_member is the member node in _container_node on which the
          * port sits (this same port object can appear on multiple members
          * if they are derived from the same base.)  If _owner_member is null,
          * this port object is assumed to be an external port on
          * _container_node.
          */
    public ConnectionProxy[] getConnections(ALFINode _container_node,
                                            MemberNodeProxy _owner_member) {
        if (_owner_member != null) {
            ConnectionProxy[] connections = _container_node.connections_get();
            ArrayList connections_list = new ArrayList();
            for (int i = 0; i < connections.length; i++) {
                ConnectionProxy c = connections[i];
                if (       ((c.getSource() == this) &&
                            (c.getSourceMember() == _owner_member)) ||
                           ((c.getSink() == this) &&
                            (c.getSinkMember() == _owner_member))) {
                    connections_list.add(connections[i]);
                }
            }

            connections = new ConnectionProxy[connections_list.size()];
            connections_list.toArray(connections);
            return connections;
        }
        else { return getConnections(_container_node); }
    }

        /** This method should never be called by PortProxy. */
    protected String generateDefaultName(ALFINode _container_node) {
        Alfi.warn("generateDefaultName should never be called by a PortProxy.");
        return null;
    }

        /** Creates a short and long (possibly multi-line) text description of
          * itself for use in status text and elsewhere.  This is a pretty
          * version and is not intended as something that may be parsed.
          * So it may be changed about without fear of breaking box files.
          */
    public String generateSnippetForConnectionDescription(ALFINode _container,
                        ConnectionProxy _connection, boolean _b_long_version) {
        assert(_container != null);
        assert(_connection != null);
        assert(_container.connection_contains(_connection));

        GenericPortProxy source = _connection.getSource();
        GenericPortProxy sink = _connection.getSink();
        assert((this == source) || (this == sink));
        
        MemberNodeProxy owner_member = (this == source) ?
                   _connection.getSourceMember() : _connection.getSinkMember();
        
        String owner_member_string = 
                 (owner_member != null) ? owner_member.getLocalName() : "this";
       return (owner_member_string + ":" + getName());
    }

        /** Does nothing.  (Stops downstream propogation of update signal.) */
    public void updateContentAndPropogateDownstream(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        // do nothing
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// general methods ////////////////////////////////////////////////////

        /** Ports have a static data type.  No parameters needed. */
    public int getDataType() { return m_data_type; }

        /** Registers external connections to this port. */
    void registerExternalConnection(ConnectionProxy _connection) {
        m_external_connections_map.put(_connection, null);
    }

        /** Unregisters external connections to this port. */
    void unregisterExternalConnection(ConnectionProxy _connection) {
        m_external_connections_map.remove(_connection);
    }

    public ConnectionProxy[] getExternalConnections() {
        ConnectionProxy[] connections =
                    new ConnectionProxy[m_external_connections_map.size()];
        m_external_connections_map.keySet().toArray(connections);
        return connections;
    }

    public int getDefaultEdge() {
        if (m_io_type == IO_TYPE_INPUT) { return EDGE_WEST; }
        else if (m_io_type == IO_TYPE_OUTPUT) { return EDGE_EAST; }
        else { return EDGE_INVALID; }
    }

        /** This method takes into account all ops that may or may not have been
          * performed on its owner member node (such as symmetry, etc), if it
          * actually sits on a member node.  If it doesn't, the internal view
          * edge on _container for this port is returned.
          */
    int determineEdgeAfterAllOps(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        int edge = PortProxy.EDGE_INVALID;
        if (_owner_member != null) {
            PortProxy[][] port_positions = _owner_member.
                           determinePortPositionsAfterRotateEtcOps(_container);
            for (int i = 0; i < port_positions.length; i++) {
                for (int j = 0; j < port_positions[i].length; j++) {
                    if (port_positions[i][j] == this) {
                        edge = i;
                    }
                }
            }
        }
        else { edge = getPosition()[0]; }

        if (edge == PortProxy.EDGE_INVALID) {
            Alfi.warn("(" + this + ").determineEdgeAfterAllOps(" + _container +
                           ", " + _owner_member + "): Error: Port not found.");
        }

        return edge;
    }


        /**
         * Gives the position of this port in/on its m_direct_container_node in
         * this format: int[4] = { internal edge, position on internal edge,
         *                         external edge, position on external edge }.
         *
         * (Value returned is a copy of current state.)
         */
    public int[] getPosition() {
        return m_direct_container_node.port_getPosition(this);
    }

        /**
         * Sets the position of this port in/on its m_direct_container_node in
         * this format: int[4] = { internal edge, position on internal edge,
         *                         external edge, position on external edge }.
         */
    public int[] setPosition(int[] _new_position) {
        return m_direct_container_node.port_setPosition(this, _new_position);
    }

    /////////////////////////////////
    // member field access //////////

        /** Returns a copy of the String value if set, otherwise null. */
    public String getDefaultValue() {
        return (m_default_value != null) ? new String(m_default_value) : null;
    }

        /** Default values can only be specified in primitive nodes. */
    public boolean setDefaultValue(String _value) {
        return setDefaultValue(_value, true);
    }


    public boolean setDefaultValue(String _value, boolean _notify_container) {
        if ( (m_default_value == null) || (! m_default_value.equals(_value))) {
            m_default_value = _value;

            if (_notify_container) {
                assert(m_direct_container_node.isPrimitive());
                m_direct_container_node.setTimeLastModified();

                ContainedElementModEvent e = new ContainedElementModEvent(this);
                fireContainedElementModEvent(e);
            }

            return true;
        }
        else { return false; }
    }

    public int getIOType() { return m_io_type; }

    public boolean setIOType(int _io_type) {
        assert(ioTypeIdIsValid(_io_type));

        if (_io_type != m_io_type) {
            m_io_type = _io_type;

            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                       new PortProxy(null, new Integer(_io_type), null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e2 = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e2);

            return true;
        }
        else { return false; }
    }

    public boolean isSourcePort(boolean _b_is_located_on_a_member_node) {
        if (_b_is_located_on_a_member_node) {
            return (m_io_type == IO_TYPE_OUTPUT);
        }
        else { return (m_io_type == IO_TYPE_INPUT); }
    }

    public boolean isSinkPort(boolean _b_is_located_on_a_member_node) {
        if (_b_is_located_on_a_member_node) {
            return (m_io_type == IO_TYPE_INPUT);
        }
        else { return (m_io_type == IO_TYPE_OUTPUT); }
    }

    public boolean setDataType(int _data_type) {
        assert(dataTypeIdIsValid(_data_type));

        if (_data_type != m_data_type) {
            m_data_type = _data_type;

            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                     new PortProxy(null, null, new Integer(_data_type), null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public String toString() { return toString(false); }

    public String toString(boolean _b_verbose) {
        String info =  DATA_TYPE_BY_STRING_ARRAY[m_data_type];
        info += " " + super.toString(_b_verbose);
        if (_b_verbose) {
            info += ", " + IO_TYPE_BY_STRING_ARRAY[getIOType()] +
                    ", <" + getDefaultValue() + ">";
        }
        return info;
    }
}
