package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

import edu.caltech.ligo.alfi.Alfi;


    /** BundlePortProxy objects are specialized PortProxy objects which have
      * added capabilities for determining and storing information regarding
      * bundle content.
      */
public class BundlePortProxy extends PortProxy implements BundleContentSource {

    ////////////////////////////////////////////////////////////////////////////
    /////// constants //////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////
    /////// member fields //////////////////////////////////////////////////////

        /** This content is used only when this port is an external input port
          * for a root node.  These channels are considered "resolved" in such
          * a root node, but once instantiated in another node with an external
          * input bundle attached to it, such resolution will not occur unless
          * there is an actual bundle input into this port which supplies the
          * needed channel content.
          *
          * The format of this map is { full_channel_path, data_type (Integer) }
          */
    private HashMap m_default_channel_map;

    ////////////////////////////////////////////////////////////////////////////
    /////// constructors ///////////////////////////////////////////////////////

        /** Main constructor.  Should only be called from ALFINode.  A further
          * call to setDefaultContent() is required if this bundle port is to
          * have a default content.  See also updateContent().
          */
    BundlePortProxy(ALFINode _container_node, String _name, int _io_type) {
        super(_container_node, _name, _io_type, DATA_TYPE_ALFI_BUNDLE);

        m_default_channel_map = new HashMap();
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// class methods //////////////////////////////////////////////////////

    static boolean dataTypeIdIsValid(int _data_type_id) {
        return (_data_type_id == DATA_TYPE_ALFI_BUNDLE);
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// general methods ////////////////////////////////////////////////////

        /** This method copies information from this port when it is sitting (as
          * an external port) on the instance _instance_to_copy_from and puts
          * the required information in m_default_channel_map.  See
          * m_default_channel_map for the content/format of this information.
          */
    public void setDefaultChannelMap(ALFINode _instance_to_copy_from) {
        HashMap channel_map = new HashMap();
        MemberNodeProxy instance_member =
                        _instance_to_copy_from.memberNodeProxy_getAssociated();
        ALFINode member_container = _instance_to_copy_from.containerNode_get();
        Hashtable content =
                      this.getContent(member_container, instance_member, true);
        Hashtable flat = BundleProxy.contentMapToFlatChannelMap(content, "");
        for (Iterator i = flat.keySet().iterator(); i.hasNext(); ) {
            String path = (String) i.next();
            BundleContentMapKey bcm_key = (BundleContentMapKey) flat.get(path);
            if (! bcm_key.isContentHolderOnly()) {
                Integer data_type = new Integer(bcm_key.getDataType());
                channel_map.put(path, data_type);
            }
        }

        setDefaultChannelMap(channel_map);
    }

        /** See m_default_channel_map for format details. */
    public void setDefaultChannelMap(HashMap _default_channel_map) {
        if (_default_channel_map != null) {
            m_default_channel_map = _default_channel_map;
        }
        else { m_default_channel_map = new HashMap(); }

        getDirectContainerNode().setTimeLastModified();
    }

        /** Determines the bundle content upstream from this bundle port.
          * _container can be either the node directly containing this bundle
          * port (in which case _owner_member == null), or it can be the
          * container of _owner_member which has this bundle port on it.
          * If _b_use_cached_content is true, then this will return the cached
          * content if it exists.  If the cached content does not exist, it
          * will automatically be set to the value returned by this call after
          * the content has been traced.
          */
    public Hashtable getContent(ALFINode _container,
                MemberNodeProxy _owner_member, boolean _b_use_cached_content) {
        ALFINode port_container = (_owner_member == null) ? _container :
                                      _container.memberNode_get(_owner_member);
        assert(port_container.port_contains(this));
        Hashtable content = null;

            // possible short circuit via the cached content
        if (_b_use_cached_content) {
            content = port_container.bundlePort_getCachedContent(this);
            if (content != null) {
                return content;
            }
        }

        ALFINode upstream_bundle_container = null;
        MemberNodeProxy upstream_sink_owner_member = null;
        if (_owner_member == null) {
            if (getIOType() == IO_TYPE_INPUT) {
                    // external input port.  look for upstream bundle in
                    // parent container.
                upstream_bundle_container = _container.containerNode_get();
                if (upstream_bundle_container != null) {
                    upstream_sink_owner_member =
                                    _container.memberNodeProxy_getAssociated();
                }
            }
            else {
                    // external output port.  look for upstream bundle in
                    // original container.
                upstream_bundle_container = _container;
                upstream_sink_owner_member = null;
            }           
        }
        else {
            if (getIOType() == IO_TYPE_INPUT) {
                    // input bundle port on a member node.  look for upstream
                    // bundle in original container.
                upstream_bundle_container = _container;
                upstream_sink_owner_member = _owner_member;
            }           
            else {
                    // output bundle port on a member node.  look for upstream
                    // bundle inside _owner_member container.
                upstream_bundle_container =
                                      _container.memberNode_get(_owner_member);
                upstream_sink_owner_member = null;
            }           
        }

        boolean b_look_for_input_bundle_port_default_channels = false;
        if (upstream_bundle_container != null) {
            ConnectionProxy[] upstream_bundles =
                                    getConnections(upstream_bundle_container,
                                                   upstream_sink_owner_member);
            if (upstream_bundles.length > 0) {
                content = ((BundleProxy) upstream_bundles[0]).getContent(
                             upstream_bundle_container, _b_use_cached_content);
            }
            else {
                content = new Hashtable();
                b_look_for_input_bundle_port_default_channels = true;
            }
        }
        else {
            content = new Hashtable();
            b_look_for_input_bundle_port_default_channels = true;
        }

        if (b_look_for_input_bundle_port_default_channels) {
            if ((_owner_member == null) && (getIOType() == IO_TYPE_INPUT)) {
                content = createContentFromDefaultChannels(port_container);
            }
        }

        port_container.bundlePort_setCachedContent(this, content);
        return content;
    }

        /** See m_default_channel_map for the format of this map. */
    public HashMap getDefaultChannels() { return m_default_channel_map; }

        /** Determines the data type of one of the default channels, if such
          * exists.  Returns DATA_TYPE_INVALID if no such default channel.
          */
    public int getDefaultChannelDataType(String _channel_path) {
        HashMap default_channels = getDefaultChannels();
        Integer data_type = (Integer) default_channels.get(_channel_path);
        if (data_type != null) {
            return data_type.intValue();
        }
        else { return GenericPortProxy.DATA_TYPE_INVALID; }
    }

        /** This method takes the map of default channels and creates a standard
          * bundle content Hashtable tree from it.
          */
    private Hashtable createContentFromDefaultChannels(ALFINode _container) {
        Hashtable flat_default_content = new Hashtable();
        if (m_default_channel_map != null) {
            for (Iterator i = m_default_channel_map.keySet().iterator();
                                                               i.hasNext(); ) {
                String channel_path = (String) i.next();
                String[] path_parts = channel_path.split("\\.");
                String channel_name = path_parts[path_parts.length - 1];

                    // make sure placeholder bundles are formed
                String bundle_path = "";
                for (int j = 0; j < path_parts.length - 1; j++) {
                    String bundle_name = path_parts[j];
                    bundle_path += bundle_name;
                    if (! m_default_channel_map.containsKey(bundle_path)) {
                        int data_type = GenericPortProxy.DATA_TYPE_ALFI_BUNDLE;
                        BundleContentMapKey bmc_key = new BundleContentMapKey(
                                    _container, this, bundle_name, data_type);
                        bmc_key.setIsContentHolderOnly(true);
                        flat_default_content.put(bundle_path, bmc_key);
                    }
                    bundle_path += ".";
                }
                    
                int data_type = ((Integer) m_default_channel_map.get(
                                                     channel_path)).intValue();
                BundleContentMapKey bmc_key = new BundleContentMapKey(
                                    _container, this, channel_name, data_type);
                flat_default_content.put(channel_path, bmc_key);
            }
        }
        Hashtable content =
                  BundleProxy.flatChannelMapToContentMap(flat_default_content);
        return content;
    }

        /** Implementation of GenericPortProxy method.  It will attempt
          * to trace forward to a possibly existing bundle or bundles
          * connected to this port in a downstream node (if such exists.)  The
          * returned map may have multiple key/value pairs:
          * { downstream_bundle_i, downstream_container }.
          */
    protected HashMap getDownstreamConnections(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        HashMap container_map = new HashMap();

        ALFINode downstream_container = null;
        MemberNodeProxy downstream_owner_member = null;
        if (_owner_member == null) {
            if (! _container.isRootNode()) {
                downstream_container = _container.containerNode_get();
                downstream_owner_member =
                                    _container.memberNodeProxy_getAssociated();
            }
        }
        else {
            downstream_container = _container.memberNode_get(_owner_member);
            downstream_owner_member = null;
        }

        if (downstream_container != null) {
            ConnectionProxy[] connections =
                 getConnections(downstream_container, downstream_owner_member);
            for (int i = 0; i < connections.length; i++) {
                container_map.put(connections[i], downstream_container);
            }
        }

        return container_map;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// overrides of GenericPortProxy methods //////////////////////////////

        /** Overrides PortProxy implementation of GenericPortProxy method.
          * Updates this port's cache and propogates update signal downstream.
          */
    public void updateContentAndPropogateDownstream(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        ALFINode port_container = (_owner_member == null) ? _container :
                                      _container.memberNode_get(_owner_member);
        assert(port_container.port_contains(this));
        port_container.bundlePort_clearCachedContent(this);    // clears cache
        getContent(port_container, null, true);                // updates cache

        HashMap downstream_connection_map =
                           getDownstreamConnections(_container, _owner_member);
        for (Iterator i = downstream_connection_map.keySet().iterator();
                                                               i.hasNext(); ) {
            ConnectionProxy connection = (ConnectionProxy) i.next();
            ALFINode container =
                          (ALFINode) downstream_connection_map.get(connection);
            connection.updateContentAndPropogateDownstream(container);
        }
    }
}
