package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /**
     * A simple object used to store and recall information about the
     * configuration of a source and/or sink bundler during a connection add.
     */
public class TerminalBundlersConfiguration {
        // regarding the connection-to-be's source (if it is a bundler)
    boolean mb_connection_is_source_bundlers_primary_output;
    HashMap m_source_bundler_output_channel_name_map;

        // regarding the connection-to-be's sink (if it is a bundler)
    boolean mb_connection_is_sink_bundlers_primary_input;
    String m_sink_bundler_input_channel_name;
    boolean mb_sink_bundler_unwraps_input_bundle_contents;

        /** TerminalBundlersConfiguration(false, null, false, null, false). */
    public TerminalBundlersConfiguration() {
        this(false, null, false, null, false);
    }

    public TerminalBundlersConfiguration(
                       boolean _b_connection_is_source_bundlers_primary_output,
                       HashMap _source_bundler_output_channel_name_map,
                       boolean _b_connection_is_sink_bundlers_primary_input,
                       String _sink_bundler_input_channel_name,
                       boolean _b_sink_bundler_unwraps_input_bundle_contents) {

        mb_connection_is_source_bundlers_primary_output =
                               _b_connection_is_source_bundlers_primary_output;
        m_source_bundler_output_channel_name_map =
                                       _source_bundler_output_channel_name_map;
        mb_connection_is_sink_bundlers_primary_input =
                                  _b_connection_is_sink_bundlers_primary_input;
        m_sink_bundler_input_channel_name = _sink_bundler_input_channel_name;
        mb_sink_bundler_unwraps_input_bundle_contents =
                                 _b_sink_bundler_unwraps_input_bundle_contents;
    }

    public TerminalBundlersConfiguration copy() {
        HashMap output_name_map =
                  (m_source_bundler_output_channel_name_map == null) ?
                                                          null : new HashMap();
        if (output_name_map != null) {
            for (Iterator i = m_source_bundler_output_channel_name_map.
                                          keySet().iterator(); i.hasNext(); ) {
                Object name = i.next();
                Object B_is_bundle =
                            m_source_bundler_output_channel_name_map.get(name);
                output_name_map.put(name, B_is_bundle);
            }
        }

        String input_name = (m_sink_bundler_input_channel_name == null) ?
                          null : new String(m_sink_bundler_input_channel_name);

        return new TerminalBundlersConfiguration(
                 mb_connection_is_source_bundlers_primary_output,
                 output_name_map,
                 mb_connection_is_sink_bundlers_primary_input, input_name,
                 mb_sink_bundler_unwraps_input_bundle_contents);
    }


    boolean connectionIsSourceBundlersPrimaryOutput() {
        return mb_connection_is_source_bundlers_primary_output;
    }

    public void setIsSourceBundlersPrimaryOutput(boolean _flag) {
        mb_connection_is_source_bundlers_primary_output = _flag;
    }

    HashMap getSourceBundlerOutputChannelNames() {
        return m_source_bundler_output_channel_name_map;
    }

        /** _names_map must be { String, Boolean } pairs, name and b_is_bundle.
          */
    public void setSourceBundlerOutputChannelNames(HashMap _names_map) {
        m_source_bundler_output_channel_name_map = _names_map;
    }

    boolean connectionIsSinkBundlersPrimaryInput() {
        return mb_connection_is_sink_bundlers_primary_input;
    }

    public void setIsSinkBundlersPrimaryInput(boolean _flag) {
        mb_connection_is_sink_bundlers_primary_input = _flag;
    }

    String getSinkBundlerInputChannelName() {
        return m_sink_bundler_input_channel_name;
    }

    public void setSinkBundlerInputChannelName(String _name) {
        m_sink_bundler_input_channel_name =
                                    (_name != null) ? new String(_name) : null;
    }

    boolean sinkBundlerUnwrapsInputBundleContents() {
        return mb_sink_bundler_unwraps_input_bundle_contents;
    }

    public void setSinkBundlerUnwrapsInputBundleContents(boolean _flag) {
        mb_sink_bundler_unwraps_input_bundle_contents = _flag;
    }
}
