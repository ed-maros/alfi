package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.tools.*;


    /** Inherited bundlers may change the name(s) of their secondary input or
      * output through these objects.  This override object can only change
      * the secondary I/O names, and cannot change a single channel secondary
      * i/o to a bundle, or vice-versa.
      */
public class BundlerIOSetting {

    //**************************************************************************
    //***** member fields ******************************************************

    private final static String INPUT     = "INPUT";
    private final static String OUTPUT    = "OUTPUT";
    private final static String UNWRAPS   = "UNWRAPS";
    private final static String IS_BUNDLE = "IS_BUNDLE";
    private final static String NOT_SET   = "NOT_SET";

    private final static int INVALID_START_LINE     = 0;
    private final static int INVALID_CHANNEL_NAME   = 1;
    private final static int INVALID_SECTION_FORMAT = 2;

    private final static String VALID_IO_NAME_PATTERN      = "\\w+";
    private final static String VALID_IO_FULL_PATH_PATTERN = "\\w+(\\.\\w+)*";

        /** The container node in which this BundlerIOSetting object has been
          * locally added.
          */
    protected final ALFINode m_direct_container_node;

        /** This is the bundler object the setting is associated with. */
    protected final BundlerProxy m_associated_bundler;

        /** Flags this object as either an input or an output setting. */
    protected boolean mb_is_input_info;

        /** The input name associated with the secondary input of the
          * associated bundler.  Null if this object is for an output bundler.
          * Also may be null if mb_unwraps_secondary_input_bundle is true.
          */
    protected final String m_secondary_input_name;

        /** True if the seconadry input is a bundle which "unwraps" to expose
          * its top level bundle content.
          */
    protected final boolean mb_unwraps_secondary_input_bundle;

        /** Keys are the secondary output names of the associated bundler.  The
          * values are Boolean objects denoting whether the channel is a
          * bundle (true) or no.  Null if this object is for an input bundler.
          */
    protected final HashMap m_secondary_output_map;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor.  Only bundlers should construct BundlerIOSettings.
          * EITHER _secondary_input_name OR _secondary_output_map must be passed
          * as null (XOR).  BundlerIOSettings are always defined for either an
          * input or an output bundler.  Bundlers with no secondary I/O yet
          * should not have any BundlerIOSetting associated with them yet.
          */
    BundlerIOSetting(ALFINode _container_node,
                     BundlerProxy _associated_bundler,
                     String _secondary_input_name,
                     boolean _b_unwraps_secondary_input_bundle,
                     HashMap _secondary_output_map) {

        assert(_container_node.bundler_contains(_associated_bundler));

            // must be either an input bundler XOR an output bundler
        boolean b_is_input_setting = (_secondary_input_name != null);
        boolean b_is_output_setting = (_secondary_output_map != null);
        assert(b_is_input_setting ^ b_is_output_setting);

        if (b_is_input_setting) {
            assert(checkForInvalidIOName(_secondary_input_name, false) == null);
        }
        else {
            assert(checkForInvalidIONames(_secondary_output_map,false) == null);
        }
        
            // final member initializations
        m_direct_container_node = _container_node;
        m_associated_bundler = _associated_bundler;
        mb_is_input_info = (_secondary_input_name != null);
        m_secondary_input_name = _secondary_input_name;
        mb_unwraps_secondary_input_bundle = _b_unwraps_secondary_input_bundle;
        m_secondary_output_map = _secondary_output_map;
    }

    //**************************************************************************
    //***** static methods *****************************************************

        /** Expects a string argument formatted as:
          *
          *     associated_bundler_name INPUT|OUTPUT [UNWRAPS]
          *     {
          *         i/o name 1
          *         [i/o name 2]
          *         [i/o name n]
          *     }
          *
          * Returns an Object[4] = { bundler_name, B_is_input, B_unwraps,
          * io_name_map } unless an error occurs.
          *
          * On error, throws an AlfiFileFormatException.
          */
    public static Object[] parse(Parser _parser, String[] _lines)
                                               throws AlfiFileFormatException {
        String[] parts = _lines[0].trim().split("\\s+");
        String bundler_name = null;
        Boolean B_is_input = null;
        Boolean B_unwraps = null;
        if ((parts.length == 2) || (parts.length == 3)) {
            if (parts[0].matches("^[\\w\\.]+$")) { bundler_name = parts[0]; }

            if (parts[1].equals(INPUT)) {
                B_is_input = new Boolean(true);
            }
            else if (parts[1].equals(OUTPUT)) {
                B_is_input = new Boolean(false);
            }

            if (parts.length == 2) {
                B_unwraps = new Boolean(false);
            }
            else if (parts[2].equals(UNWRAPS)) {
                B_unwraps = new Boolean(true);
            }
        }

        if ((bundler_name == null) ||
                                 (B_is_input == null) || (B_unwraps == null)) {
            throwFormatException(_parser, _lines, INVALID_START_LINE);
        }

        HashMap io_name_map = new HashMap();
        if (_lines[1].trim().equals(Parser.SECTION_BEGIN_MARKER) &&
                              _lines[_lines.length - 1].trim().equals(
                                                  Parser.SECTION_END_MARKER)) {
            for (int i = 2; i < _lines.length - 1; i++) {
                String[] channel_parts = _lines[i].trim().split("\\s+");
                String channel_name = null;
                boolean b_is_bundle = false;
                if ((channel_parts.length == 1) || (channel_parts.length == 2)){
                    if (channel_parts[0].matches("^[\\w\\S]+$")) {
                        channel_name = channel_parts[0];
                        if (channel_parts.length == 2) {
                            if (channel_parts[1].equals(IS_BUNDLE)) {
                                b_is_bundle = true;
                            }
                        }
                    }
                }

                if (channel_name != null) {
                    io_name_map.put(channel_name, new Boolean(b_is_bundle));
                }
                else if (B_is_input.booleanValue() && B_unwraps.booleanValue()){
                    io_name_map.put(NOT_SET, null);
                }
                else {
                    throwFormatException(_parser, _lines, INVALID_CHANNEL_NAME);
                }
            }
        }
        else { throwFormatException(_parser, _lines, INVALID_SECTION_FORMAT); }

        Object[] bundler_io_info =
                          { bundler_name, B_is_input, B_unwraps, io_name_map };
        return bundler_io_info;
    }

    private static void throwFormatException(Parser _parser, String[] _lines,
                              int _error_code) throws AlfiFileFormatException {
        String file_path = _parser.getFileName();
        String message = null;
        switch (_error_code) {
          case INVALID_START_LINE:
            message = "Invalid start line format for bundler I/O setting.";
            break;
          case INVALID_CHANNEL_NAME:
            message = "Invalid I/O channel name for bundler I/O setting.";
            break;
          case INVALID_SECTION_FORMAT:
            message = "Invalid section format for bundler I/O setting.";
            break;
        }

        StringBuffer sb = new StringBuffer(_lines[0]);
        for (int i = 1; i < _lines.length; i++) {
            sb.append(Parser.NL);
            sb.append(_lines[i]);
        }

        throw new AlfiFileFormatException("", file_path, sb.toString());
    }

        /** Returns null on success, or _io_name if bad.  The flag
          * _b_allow_full_names signals whether full channel names (i.e.,
          * possibly having the character "." in it) are okay.
          */
    static String checkForInvalidIOName(String _io_name,
                                                 boolean _b_allow_full_names) {
        HashMap name_map = new HashMap();
        name_map.put(_io_name, null);
        return checkForInvalidIONames(name_map, _b_allow_full_names);
    }

        /** Returns null on success, or first encountered bad name.  The flag
          * _b_allow_full_names signals whether full channel names (i.e.,
          * possibly having the character "." in it) are okay.
          */
    static String checkForInvalidIONames(HashMap _io_name_map,
                                                 boolean _b_allow_full_names) {
        //String match_pattern = _b_allow_full_names ?
        String match_pattern = true ?
                            VALID_IO_FULL_PATH_PATTERN : VALID_IO_NAME_PATTERN;
        if ((_io_name_map != null) && (! _io_name_map.isEmpty())) {
            for (Iterator i = _io_name_map.keySet().iterator(); i.hasNext(); ) {
                String name = (String) i.next();
                if (! name.matches(match_pattern)) { return name; }
            }
        }
        else { return "BAD_IO_NAME_MAP"; }

        return null;
    }

    //**************************************************************************
    //***** methods ************************************************************

        /** Write the object in its parser representation. */
    public String generateParserRepresentation(String _starting_spacer) {
        String full_spacer = _starting_spacer + Parser.STANDARD_SPACER;

        String rep = _starting_spacer +
                     m_associated_bundler.getName() +
                     " " + (mb_is_input_info ? INPUT : OUTPUT) +
                     (mb_unwraps_secondary_input_bundle ? " " + UNWRAPS : "") +
                     Parser.NL;

        rep += _starting_spacer + Parser.SECTION_BEGIN_MARKER + Parser.NL;

        if (isSecondaryInInfo()) {
            rep += full_spacer + m_secondary_input_name + Parser.NL;
        }
        else {
            for (Iterator i = m_secondary_output_map.keySet().iterator();
                                                               i.hasNext(); ) {
                String channel_name = (String) i.next();
                rep += full_spacer + channel_name;

                Boolean B_is_bundle =
                            (Boolean) m_secondary_output_map.get(channel_name);
                if (B_is_bundle.booleanValue()) {
                    rep += " " + IS_BUNDLE;
                }

                rep += Parser.NL;
            }
        }

        rep += _starting_spacer + Parser.SECTION_END_MARKER;

        return rep;
    }

        /** Tests if all the fields (except m_direct_container_node and
          * optionally m_associated_bundler) of _other contain the same
          * information as this BundlerIOSetting.
          */
    public boolean equals(BundlerIOSetting _other,
                                  boolean _b_include_associated_bundler_test) {
        if ((! _b_include_associated_bundler_test) ||
                     (_other.getAssociatedBundler() == m_associated_bundler)) {
            if (mb_is_input_info) {
                if (_other.isSecondaryInInfo()) {
                    if (_other.unwrapsInputBundle() ==
                                           mb_unwraps_secondary_input_bundle) {
                        if (m_secondary_input_name == null) {
                            if (_other.getSecondaryInputName() == null) {
                                return true;
                            }
                        }
                        else if (_other.getSecondaryInputName().equals(
                                                     m_secondary_input_name)) {
                            return true;
                        }
                    }
                }
            }
            else if (! _other.isSecondaryInInfo()) {
                HashMap other_out_map = _other.getSecondaryOutputMap();
                if (other_out_map.size() == m_secondary_output_map.size()) {
                    HashMap name_map = new HashMap();
                    for (Iterator i = other_out_map.keySet().iterator();
                                                               i.hasNext(); ) {
                        Object other_key = i.next();
                        Object other_value = other_out_map.get(other_key);
                        if (m_secondary_output_map.containsKey(other_key)) {
                            if (! m_secondary_output_map.get(other_key).
                                                         equals(other_value)) {
                                return false;
                            }
                        }
                        else { return false; }
                    }
                    return true;
                }
            }
        }

        return false;
    }

    public String toString() {
        String s = "BundlerIOSetting: " + m_associated_bundler.getName();
        if (mb_is_input_info) {
            s += " [" + INPUT + ": " + m_secondary_input_name + "]";
            if (mb_unwraps_secondary_input_bundle) {
                s += " [" + UNWRAPS + "]";
            }
        }
        else {
            s += " [" + OUTPUT + ": { ";
            for (Iterator i = m_secondary_output_map.keySet().iterator();
                                                               i.hasNext(); ) {
                s += i.next() + " ";
            }
            s += "} ]";
        }

        return s;
    }

    /////////////////////////////////
    // member field access //////////

        /** Returns node this setting was locally added to. */
    public ALFINode getDirectContainerNode() { return m_direct_container_node; }

    public BundlerProxy getAssociatedBundler() { return m_associated_bundler; }

    public boolean isSecondaryInInfo() { return mb_is_input_info; }

        /** Valid call only for input bundlers. */
    public boolean unwrapsInputBundle() {
//assert(mb_is_input_info);

        return mb_unwraps_secondary_input_bundle;
    }

        /** Valid call only for input bundlers. */
    public String getSecondaryInputName() {
//assert(mb_is_input_info);

        return ((m_secondary_input_name != null) ?
                                    new String(m_secondary_input_name) : null);
    }

        /** Valid call only for output bundlers. */
    public HashMap getSecondaryOutputMap() {
//assert(! mb_is_input_info);

        return ((m_secondary_output_map != null) ?
                                   new HashMap(m_secondary_output_map) : null);
    }
}
