package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /** ParameterSetting objects for FUNC_X primitives.
      * This derived class includes the type and a flag to indicate
      * that the value is a default value.
      */
public class FUNCX_ParameterSetting extends ParameterSetting {

    //**************************************************************************
    //***** member fields ******************************************************

        /** The parameter type */
    private final String m_type;
 
        /** Flag to indicate that the default value is from the base_node */
    private final boolean mb_default_value;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor.  Only FUNCX_Node should ever construct
          * FUNCX_ParameterSettings.
          */
    FUNCX_ParameterSetting(ALFINode _container_node, String _name,
                       String _type, String _value, boolean _b_default_value) {
        super(_container_node, _name, _value, true);
        m_type = _type;
        mb_default_value = _b_default_value;
        
    }

    //**************************************************************************
    //***** methods ************************************************************

        /** Write the object in its parser representation. */
    public String generateParserRepresentation() {
        return null;
    }

        /** Generate a description of the connection. */
    public String toString() {
        String info = "FUNCX_ParameterSetting(" +
            m_direct_container_node.generateFullNodePathName() + "): (" +
            m_name + ", " + m_type +
            (mb_default_value ? " (DEFAULT) " :
                " (DEFINED) ") + m_value + (mb_quoted ? " (QUOTED)" : "") + ")";
        return info;
    }

        /** Access method to retrieve the type */
       public String getType() { return m_type; }

        /** Access method to find out if the value is a default value */
       public boolean isDefaultValue() { return mb_default_value; }
}
