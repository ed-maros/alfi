package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /**
     * This event is created by any bookkeeping element which can be contained
     * in a node and can be modified (moved, resized, added to, deleted from,
     * etc.)  It is fired whenever the element is changed.  Adds/deletes are
     * considered to modify the container of that element, not the element
     * itself, just for clarification.
     *
     * Currently the only listener for these events is the MemberNodeWidget
     * object.  When it is instantiated, it will register as a listener with all
     * modifiable elements anywhere inside its associated ALFINode (as well as
     * that ALFINode itself.)  It is listening for these events in order to
     * know when it may need to alter its color to show modified or no.
     *
     * IMPORTANT!  Do not confuse ContainedElementModEvents with
     * NodeContentChangeEvents.  ContainedElementModEvents are associated with
     * anp propogated along the container hierarchy, while
     * NodeContentChangeEvents are associated with and propogated along the
     * inheritance hierarchy.  Both may be created by the same user action,
     * but they will travel different paths to inform their associated
     * listeners.
     */
public class ContainedElementModEvent extends EventObject {

    //**************************************************************************
    //***** constants **********************************************************

        // action codes
    // public static final int NOT_CURRENTLY USED = 0;

    //**************************************************************************
    //***** fields *************************************************************

    // none

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public ContainedElementModEvent(Object _source) {
        super(_source);
    }

    //**************************************************************************
    //***** methods ************************************************************

    public String toString() {
        return "ContainedElementModEvent[" + getSource() + "]";
    }
}
