package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /** This event is created elements associated with the content of a bundle.
      * If changes to these elements occur, their
      * BundleConfigChangeEventListeners need to update themselves.
      */
public class BundleConfigChangeEvent extends EventObject {

    //**************************************************************************
    //***** constants **********************************************************

        // action codes
    // public static final int NOT_CURRENTLY USED = 0;

    //**************************************************************************
    //***** fields *************************************************************

    // none

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public BundleConfigChangeEvent(Object _source) {
        super(_source);
    }

    //**************************************************************************
    //***** methods ************************************************************

    public String toString() {
        return "BundleConfigChangeEvent[" + getSource() + "]";
    }
}
