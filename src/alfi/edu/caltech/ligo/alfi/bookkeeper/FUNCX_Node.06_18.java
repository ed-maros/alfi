package edu.caltech.ligo.alfi.bookkeeper;

import java.io.*;
import java.util.*;
import java.lang.Math;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.tools.*;

    /**
      * A FUNCX_Node is a subclass of ALFINode which is the entity used for
      * a FUNC_X Primitive.   This primitive is different from the rest
      * because the user can have two different kinds of displays in
      * the Primitive Setting Dialog. 
      * <p>
      * If the <i>showInstanceSettings</i> 
      * is set to <code>true</code>, the dialog displays a simple view
      * containing the type, variable name, and value of the variables defined
      * in the <code>MemberDecl</code> setting.  
      * <p> Otherwise, if the 
      * <i>showInstanceSettings</i> is set to <code>false</code> then the
      * C++ code snippet is displayed along with all the other primitive
      * settings (like <code> Equation, Constsructor, Destructor</code>, etc.)
      */
public class FUNCX_Node extends ALFINode {

        /** Directive to look at an auxilliary include file for value. */
    public static final String AT_AT_INCLUDE = "@@INCLUDE";

        /** Primitive setting to be parsed */
    public static final String FUNC_X_MEMBER_DECL = "MemberDecl";

        /** Start of the substring in MemberDecl to be used in the 
          * <i>simple view</i>
          */
    public static final String FUNC_X_MEMBER_VAR_START =
                                                   "//FUNC_X_MEMBER_VAR_START";

        /** End of the substring in MemberDecl to be used in the 
          * <i>simple view</i>
          */
    public static final String FUNC_X_MEMBER_VAR_END= "//FUNC_X_MEMBER_VAR_END";

        /** If the setting has a value defined in this object instance, this
          * delimiter is prepended before the initialized value (ending with a
          * semicolon
          */
    public static final String FUNC_X_MEMBER_INIT = "//**FUNC_X_INIT_VALUE";

        /** Primitive setting which contains the initialized values */
    public static final String FUNC_X_CONSTRUCTOR = "Constructor";

        /** Start of the substring in the Constructor setting for the
          * initialized values.
          */
    public static final String FUNC_X_CTOR_INIT_START =
                                                  "//FUNC_X_MEMBER_INIT_START";

        /** End of the substring in the Constructor setting for the initialized
          * values.
          */
    public static final String FUNC_X_CTOR_INIT_END= "//FUNC_X_MEMBER_INIT_END";

        /** Comment prepended in the inserted substrings */
    public static final String FUNC_X_WARNING_NO_EDIT =
        "//The following code segment is generated automatically.  DO NOT EDIT";
 
    public static final String FUNC_X_EQUATIONS = "Equations";
    public static final String FUNC_X_MEMBER_IMPL = "MemberImpl";
    public static final String FUNC_X_GLOBAL = "Global";
    public static final String FUNC_X_HEADER = "Header";
    public static final String FUNC_X_DESTRUCTOR = "Destructor";
    public static final String FUNC_X_DOWHENGLOBALCHANGES =
                                                         "DoWhenGlobalChanges";

    public static final String[] FUNC_X_SETTING_ORDER =
        { FUNC_X_EQUATIONS,
          FUNC_X_MEMBER_DECL,
          FUNC_X_MEMBER_IMPL,
          FUNC_X_GLOBAL,
          FUNC_X_HEADER,
          FUNC_X_CONSTRUCTOR,
          FUNC_X_DESTRUCTOR,
          FUNC_X_DOWHENGLOBALCHANGES
        };  

    public static final String[] FUNC_X_AT_AT_INCLUDE_MARKERS =
        { "EQUATION",
          "MEMBER_DECL",
          "MEMBER_IMPL",
          "GLOBAL",
          "HEADERS",
          "CONSTRUCTOR",
          "DESTRUCTOR",
          "DO_WHEN_GLOBAL_CHANGES"
        };  

    public static final String FUNC_X_SETTING_BEGIN = "/*@@BEGIN_FX_";
    public static final String FUNC_X_SETTING_END = "/*@@END_FX_";

    public static final int LIST_NAME            = 0;
    public static final int LIST_TYPE            = 1;
    public static final int LIST_LOCAL_VALUE     = 2;
    public static final int LIST_LOCAL_COMMENT   = 3;
    public static final int LIST_DEFAULT_VALUE   = 4;
    public static final int LIST_DEFAULT_LOCATION= 5;

        /** When a parameter value which is locally set is an @@INCLUDE, this
          * map stores the contents of the file (String[]) against the file name
          * key.
          */
    protected HashMap m_at_at_file_name_to_content_map;

        /** Temporary list of funcx settings to be added. */
    private ArrayList m_funcx_setting_list;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor for creating a root node only. */
// this constructor is associated with creating a root box node from a box
// file.  this method should probably not exist for FUNCX_Node.
    public FUNCX_Node(ALFIFile _root_node_file) {
        super(_root_node_file);

        m_at_at_file_name_to_content_map = new HashMap();
    } 

        /** Constructor for creating a member FUNCX_Node.  See deriveInstance()
          * below for "public" access to this method.  This constructor is never
          * called except by FUNCX_Node.deriveInstance() and by super() in any
          * derived class's constructors.
          */
    protected FUNCX_Node(ALFINode _base_node, ALFINode _container_node,
                                  MemberNodeProxy _associated_MemberNodeProxy) {
        super(_base_node, _container_node, _associated_MemberNodeProxy);

        m_at_at_file_name_to_content_map = new HashMap();
    }

        /* This constructor only exists for UDP to call super in its
         * constructor.  Do not use this constructor to create FUNCX_Nodes.
         */
    protected FUNCX_Node(String _node_type, String _primitive_group) {
        super(_node_type, _primitive_group);

        m_at_at_file_name_to_content_map = new HashMap();
    }

    //**************************************************************************
    //***** public methods *****************************************************

    ////////////////////////////////////////////
    // base and derived nodes methods //////////
    ////////////////////////////////////////////

        /** Overrides the ALFINode call in order to create the correct type
          * of node.  This call should always be made by the node which is to
          * be the DIRECT base node of the node to be created.  And of course,
          * this new node will be being placed inside another container node
          * (_container_node), and will always have a MemberNodeProxy
          * (_associated_MemberNodeProxy) associated with the new node's
          * placement in _container_node.
          */
    ALFINode deriveInstance(ALFINode _container_node,
                                 MemberNodeProxy _associated_MemberNodeProxy) {
        return new FUNCX_Node(this,_container_node,_associated_MemberNodeProxy);
    }


    public ArrayList getRowDeclarations () {
        clearLists();

        ArrayList end_user_row_list = new ArrayList();
        boolean instance_setting = determineInstanceParameterSetting();
        
        if (instance_setting) {
            String[] param_names = endUserParameters_getNames();
            for (int i = 0; i < param_names.length; i++) {
                end_user_row_list.add(parameterSetting_getInfo(param_names[i]));
            } 

            end_user_row_list.add(parameterSetting_getInfo(
                               ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG));

        }
        else {
            ALFINode base = isRootNode() ? this : baseNode_getRoot();
            ParameterDeclaration[] pds  = base.parameterDeclarations_getLocal();
            for (int i = 0; i < pds.length; i++) {
                end_user_row_list.add(parameterSetting_getInfo(pds[i].getName()));
            }
        }
              
        return end_user_row_list;        
        
    }


    public int getSettingIdFromString(String _setting_string) {
        for (int i = 0; i < FUNC_X_SETTING_ORDER.length; i++) {
            if (_setting_string.equals(FUNC_X_SETTING_ORDER[i])) {
                return i;
            }
        }
        return (-1);
    }


        /** This method searches for the latest setting object associated with
          * a value for _setting_name, and then checks if the value has the
          * format:
          *
          * @@INCLUDE file_name
          *
          * If so, then the object is returned.  If not, null is returned.
          *
          * The object returned will usually be a ParameterSetting, but it
          * may also be a ParameterDeclaration which has a default value
          * for _setting_name which is an @@INCLUDE.  (This will never be the
          * case for FUNCX_Nodes, but ParameterDeclaration objects may be
          * returned by subclasses, and the check here is for completeness.)
          *
          * _setting_name will be something like FUNC_X_CONSTRUCTOR or
          * FUNC_X_MEMBER_DECL, etc.
          */
    public Object getAtAtIncludeSettingValueObject(String _setting_name) {
        Object object_with_include_value = null;

        ParameterSetting setting = parameterSetting_get(_setting_name);
        if (setting != null) {
            if (parseAtAtIncludeLine(setting.getValue()) != null) {
                object_with_include_value = setting;
            }
        }
        else {
            ALFINode base = isRootNode() ? this : baseNode_getRoot();
            ParameterDeclaration declaration =
                             base.parameterDeclaration_getLocal(_setting_name);
            if (declaration != null) {
                if (parseAtAtIncludeLine(declaration.getValue()) != null) {
                    object_with_include_value = declaration;
                }
            }
            else {
                System.err.println(
                    "(FUNCX_Node).getAtAtIncludeSettingValueObject(" +
                                                       _setting_name + "):\n" +
                    "\tWarning: " + _setting_name + " has not been declared " +
                    "as a parameter in this node (" + this + ").");
            }
        }

        return object_with_include_value;
    }

        /** If _end_user_parameter_name exists, returns a String[4]:
          *   [0] = the type of this parameter
          *   [1] = the local value of this parameter or null if unchanged from
          *         setting in last MEMBER_DECL definition.
          *   [2] = the default value of this parameter or null if none found.
          *   [3] = the name of the node where [2] was found.
          *
          * Basically this is the same information which is gotten from
          * endUserParameters_getInfo() excepth that this is for a single
          * parameter of known name.
          *
          * If _end_user_parameter_name is not recognized, null is returned.
          */
    protected String[] endUserParameters_getDetails(String 
                                                    _end_user_parameter_name) {

        ArrayList end_user_parameter_info = endUserParameters_getInfo();
        for (Iterator i = end_user_parameter_info.iterator(); i.hasNext(); ) {
            String[] info = (String[]) i.next();
            if (info[1].equals(_end_user_parameter_name)) {
                String[] details = { info[0], info[2], info[3], info[4] };
                return details;
            }
        }

        return null;
    }

        /** Returns a list of current end-user parameter names. */
    protected String[] endUserParameters_getNames() {
        ArrayList end_user_parameter_info = endUserParameters_getInfo();
        String[] names = new String[end_user_parameter_info.size()];
        for (int i = 0; i < end_user_parameter_info.size(); i++) {
            names[i] = ((String[]) end_user_parameter_info.get(i))[1];
        }
        return names;
    }


        /** Returns a list of String[5] elements defined as:
          *   [0] = the type of this parameter
          *   [1] = the name of this parameter
          *   [2] = the local value of this parameter or null if unchanged from
          *         setting in last MEMBER_DECL.
          *   [3] = the default value of this parameter or null if none found.
          *   [4] = the name of the node where [3] was found.
          */
    protected ArrayList endUserParameters_getInfo() {
System.out.println("(" + generateFullNodePathName() + "(FuncX)).endUserParameters_getInfo():");
        HashMap info_vs_name_map = new HashMap();

            // first we will determine the names and types which do not change
        String[] MEMBER_DECL_info = normalParameter_getInfo(FUNC_X_MEMBER_DECL);
        String most_recent_MEMBER_DECL_string = (MEMBER_DECL_info[0] != null) ?
                                    MEMBER_DECL_info[0] : MEMBER_DECL_info[2];
System.out.println("    most_recent_MEMBER_DECL_string = " + most_recent_MEMBER_DECL_string);

        ArrayList end_user_param_info =
                     parseMEMBER_DECLParameterForEndUserValues( 
                                              most_recent_MEMBER_DECL_string);
            // we now have THE list of names and types (ignoring values for now)
        for (Iterator i = end_user_param_info.iterator(); i.hasNext(); ) {
            String[] type_name_value = (String[]) i.next();
            String type = type_name_value[0];
            String name = type_name_value[1];

                // init String[5]s that will be returned in the return list
            String[] param_info = { type, name, null, null, null };
                // (more to be filled in later in method)

                // place the String[5]s in the map for later access via name
            info_vs_name_map.put(name, param_info);
        }

            // now, starting from the root base node, look for values for these
            // settings and find where the last change to this value was made
            // with that info, we can determine the default setting
        ALFINode[] nodes = baseNodes_getHierarchyPath();
        for (int i = 0; i < nodes.length - 1; i++) {  // stop before THIS node
            FUNCX_Node fx_node = (FUNCX_Node) nodes[i];
            MEMBER_DECL_info = 
                          fx_node.normalParameter_getInfo(FUNC_X_MEMBER_DECL);
            if (MEMBER_DECL_info[0] != null) {
                    // a local MEMBER_DECL string exists for this node
                end_user_param_info =
                                   parseMEMBER_DECLParameterForEndUserValues(
                                                         MEMBER_DECL_info[0]);

                    // NOW, we care about values...
                for (Iterator j = end_user_param_info.iterator(); j.hasNext();){
                    String[] type_name_value = (String[]) j.next();
                    String name = type_name_value[1];
                    String value = type_name_value[2];
                        // retrieve the String[5] for this name
                    String[] param_info = (String[]) info_vs_name_map.get(name);

                        // ignore names that were not found in step 1 above
                    if (param_info != null) {
                        if (param_info[3] == null) {
                                // init default value info
                            param_info[3] = value;  // default value
                            param_info[4] = fx_node.generateFullNodePathName();
                        }
                            // if the value is different, make update
                        else if (! value.equals(param_info[3])) {
                            param_info[3] = value;
                            param_info[4] = fx_node.generateFullNodePathName();
                        }
                    }
                }
            }
        }

            // finally we will look in this node for a MEMBER_DECL setting, and
            // if one exists, then we can compare the values there with the
            // default values we have found.  if there is a change, then this
            // will appear as a local setting
        MEMBER_DECL_info = normalParameter_getInfo(FUNC_X_MEMBER_DECL);
        if (MEMBER_DECL_info[0] != null) {
            end_user_param_info =
                     parseMEMBER_DECLParameterForEndUserValues( 
                                                         MEMBER_DECL_info[0]);
            for (Iterator i = end_user_param_info.iterator(); i.hasNext(); ) {
                String[] type_name_value = (String[]) i.next();
                String name = type_name_value[1];
                String value = type_name_value[2];

                    // get the String[5] for this name
                String[] param_info = (String[]) info_vs_name_map.get(name);
                if (! value.equals(param_info[3])) {
                    param_info[2] = value; // this is considered a local setting
                }
            }
        }

            // now all of the parts of the String[5]s have been filled in
        ArrayList info = new ArrayList(info_vs_name_map.values());

       return info;
    }


    public String[] parameterSetting_getInfo(String _parameter_name) {
        
         ALFINode base = isRootNode() ? this : baseNode_getRoot();
         ParameterDeclaration declaration =
                            base.parameterDeclaration_getLocal(_parameter_name);
         if (declaration != null) {
            String[] info = normalParameter_getInfo(_parameter_name);

            // To complete all the information we need, add the type
            String[] complete_info = { _parameter_name, 
                     declaration.getType(), info[0], info[1], info[2], info[3]};

            return complete_info;                       
        }
        
        // Not a normal parameter
        String[] end_user_info =  endUserParameters_getDetails(_parameter_name);
        if (end_user_info != null) {
            // To complete all the information to match the one above,
            // just place an empty string in the 3rd parameter
            String[] complete_info = { _parameter_name, end_user_info[0], 
                      end_user_info[1], "", end_user_info[2], end_user_info[3]};
            return complete_info;
        }
        
        // _parameter_name is not recognized
        return null;

    }
    

        /** This method returns a String[4]:
          *   [0] = the local value* of this parameter or null.  (* see note)
          *   [1] = if [0] is parsed from an @@INCLUDE, the filename. else null.
          *   [2] = the default value* of this parameter or null. (* see note)
          *   [3] = name of the node where default was found or null.
          *
          *     * important note: if the true value of a parameter is an
          *       @@INCLUDE, then the content of the @@INCLUDE file is parsed to
          *       get the value from inside the @@INCLUDE file.  i.e., [0] and
          *       [2]  will never return the strings like "@@INCLUDE x.cc".
          */       
    protected String[] normalParameter_getInfo(String _parameter_name) {

        String[] info = new String[4];
        ParameterSetting local_setting = parameterSetting_get(_parameter_name);
        if (local_setting != null) {
            String raw_value = local_setting.getValue();
            if (raw_value.startsWith(AT_AT_INCLUDE)) {
                info[1] = raw_value;
                String file_name = parseAtAtIncludeLine(raw_value);
                if (! m_at_at_file_name_to_content_map.containsKey(file_name)) {
                    File file = determineAtAtIncludeFile(_parameter_name);
                    String[] content = MiscTools.readSmallTextFile(file);
                    m_at_at_file_name_to_content_map.put(file_name, content);
                }

                info[0] = parseContentForValue(_parameter_name, (String[])
                              m_at_at_file_name_to_content_map.get(file_name));
            }
            else {
                info[0] = raw_value;
                info[1] = null;
            }
        }
        else {
            info[0] = null;
            info[1] = null;
        }

            // now looking for default value if it exists
        FUNCX_Node node = this;
        boolean b_info_complete = false;
        while (node.baseNode_getDirect() != null) {
            node = (FUNCX_Node) node.baseNode_getDirect();
            String[] base_info = node.normalParameter_getInfo(_parameter_name);
            if (base_info[0] != null) {
                info[2] = base_info[0];
                info[3] = node.generateFullNodePathName();

                b_info_complete = true;
                break;
            }
        }

        if (! b_info_complete) {
                // node is now the ROOT base node (it may have been so to start
                // with) so just look in parameter declaration for default value
                // (i.e., there should never be parameter settings in root nodes
                // only parameter declarations with optional default values.)
            ParameterDeclaration dec =
                           node.parameterDeclaration_getLocal(_parameter_name);
            if (dec != null) {
                String raw_value = dec.getValue();
                if (raw_value.startsWith(AT_AT_INCLUDE)) {
                    String file_name = parseAtAtIncludeLine(raw_value);
                    if (! m_at_at_file_name_to_content_map.containsKey(
                                                                  file_name)) {
                        File file =
                                node.determineAtAtIncludeFile(_parameter_name);
                        String[] content = MiscTools.readSmallTextFile(file);
                        m_at_at_file_name_to_content_map.put(file_name,content);
                    }

                    info[2] = parseContentForValue(_parameter_name, (String[])
                              m_at_at_file_name_to_content_map.get(file_name));
                }
                else { info[2] = raw_value; }

                info[3] = node.determineLocalName();
            }
            else {
                // Ports with default setting will end up here
                // They will not have any ParameterDeclaration
/*
                System.err.println("FUNCX_Node.normalParameter_getInfo(" +
                                                     _parameter_name + "):\n" +
                    "\tERROR: Missing ParameterDeclaration for [" +
                    _parameter_name + "] in root node [" + this + "]!\n" +
                    "\tThis should not be possible.");

                info[2] = null;
                info[3] = null;
*/
            }
        }

        return info;
    }

    private String parseContentForValue(String _parameter_name,
                                                      String[] _file_content) {
        if (_file_content != null && _file_content.length > 0) {
            int parameter_id = getSettingIdFromString(_parameter_name);
            if (parameter_id >= 0) {
                String start_delimeter =
                    new String(FUNC_X_SETTING_BEGIN +
                            FUNC_X_AT_AT_INCLUDE_MARKERS[parameter_id] + "*/");
                String end_delimeter =
                    new String(FUNC_X_SETTING_END +
                            FUNC_X_AT_AT_INCLUDE_MARKERS[parameter_id] + "*/");

                    // Extract the value string
                StringBuffer buffer = new StringBuffer();
                for (int i = 0; i < _file_content.length; i++) {
                    boolean found_end = false;
                    if (_file_content[i].indexOf(start_delimeter) >= 0) {
                            // past the start delimiter now...
                        for (int j = (i + 1); j < _file_content.length; j++) {
                                // as long as the end delimiter isn't found...
                            if (_file_content[j].indexOf(end_delimeter) < 0) {
                                buffer.append(_file_content[j]);
                                buffer.append(Parser.NL);
                            }
                            else {
                                found_end = true;
                                break;
                            }
                        }
                    }

                    if (found_end) { break; }
                }
                if (buffer.length() > 0) { return buffer.toString(); }
            }
        }

        return null;
    }

        /** Returns a list whose elements are all String[3] = (type, name,
          * value) for each of the end-user parameters found in _MD_value.
          */
    private ArrayList parseMEMBER_DECLParameterForEndUserValues(String
                                                                   _MD_value) {
        ArrayList end_user_parameter_list = new ArrayList();
        if (_MD_value != null) { 
        String[] lines = _MD_value.split(Parser.NL);
        boolean b_inside_member_var_section = false;
        for (int i = 0; i < lines.length; i++) {

            if (b_inside_member_var_section) {
                if (lines[i].equals(FUNC_X_MEMBER_VAR_END)) { break; }
                else {
                    try {
                        String[] parts = lines[i].split("FUNC_X_INIT_VALUE");
                        String name = "";
                        String type = "";
                        if (parts.length > 0) {
                            parts[0].trim();
                            String[] type_and_name = parts[0].split("\\s+");
                            type = type_and_name[0];
                            String[] name_parts = type_and_name[1].split(";");
                            name = name_parts[0];
                            if (name.endsWith(";")) {
                                name = name.substring(0, name.length() - 1);
                            }
                        }

                        String init_value = "";
                        if (parts.length == 2) {
                            init_value = parts[1].trim();
                            if (init_value.endsWith(";")) {
                                init_value = init_value.substring(0,
                                                      init_value.length() - 1);
                            }
                        }

                        if ((name.length() > 0) && (type.length() > 0)) {
                            String[] info = { type, name, init_value };
                            end_user_parameter_list.add(info);
                        }
                        else { throw new EndUserParamFormatException(); }
                    }
                    catch(EndUserParamFormatException _e) {
                        System.err.println("(" + this +
                            ").parseMEMBER_DECLParameterForEndUser" +
                                                                "Values():\n" +
                            "\tWARNING: Invalid line in MEMBER_DECLARATION:" +
                            "\t\t" + lines[i] + "\n\tLine ignored.");
                    }
                }
            }
            else if (lines[i].equals(FUNC_X_MEMBER_VAR_START)) {
                    b_inside_member_var_section = true;
            }
        }
        }

        return end_user_parameter_list;
    }

        /** Checks if there is an @@INCLUDE value associated with a setting
          * name.  _setting_name will be something like FUNC_X_CONSTRUCTOR or
          * FUNC_X_MEMBER_DECL, etc.  This includes a check for a default value
          * for _setting_name from the root base node's associated
          * ParameterDeclaration for completeness.
          */
    public boolean hasAtAtIncludeSettingValueObject(String _setting_name) {
        return (getAtAtIncludeSettingValueObject(_setting_name) != null);
    }

        /** Get the content of an auxilliary @@INCLUDE file.  Returns null if
          * there is no setting associated with _setting_name, if the
          * associated setting is not an @@INCLUDE, or if the @@INCLUDE
          * refers to a non-existant or unreadable file.
          *
          * _setting_name should be something like FUNC_X_CONSTRUCTOR or
          * FUNC_X_MEMBER_DECL, etc.
          */
    public String[] getAtAtIncludeContent(String _setting_name) {
        String[] content = null;

        File include_file = determineAtAtIncludeFile(_setting_name);

        if ((include_file != null) && include_file.exists()) {
            content = MiscTools.readSmallTextFile(include_file);
        }
        else {
/*
                // error conditions
            String error_message = "Problem opening include file in " + 
                generateFullNodePathName() + " for setting: " + 
                _setting_name +"."; 
            System.err.println(error_message);
*/                              
        }

        return content;
    }

        /** This method determines where an @@INCLUDE file should exist.
          * _setting_name should be something like FUNC_X_CONSTRUCTOR,
          * FUNC_X_MEMBER_DECL, etc.
          *
          * WARNING!  A non-null return value does not mean that the file
          *           exists already!  Use File.exists() to determine actual
          *           file existence if needed.
          */
    File determineAtAtIncludeFile(String _setting_name) {
        File include_file = null;

        Object object_with_include_value =
                               getAtAtIncludeSettingValueObject(_setting_name);
        if (object_with_include_value != null) {
            if (object_with_include_value instanceof ParameterSetting) {
                ParameterSetting setting =
                            (ParameterSetting) object_with_include_value;

                String file_name = parseAtAtIncludeLine(setting.getValue());
                ALFINode direct_container_node=setting.getDirectContainerNode();
                    // get the box which contains said FUNC_X node
                ALFINode parent_box = direct_container_node.containerNode_get();

                    // find the root container of this box.  this box will be
                    // associated with the box file which would actually contain
                    // the line "@@INCLUDE..."  such an included file should
                    // exist in the same directory
                ALFIFile box_file =
                             parent_box.containerNode_getRoot().getSourceFile();

                File base_search_directory = box_file.getCanonicalDirectory();

                include_file = new File(base_search_directory, file_name);
            }
        }

        return include_file;
    }

        /** Parses an @@INCLUDE line and returns the file name.  Expects a
          * single line with the format:
          *
          *   @@INCLUDE file_name
          *
          * Returns null if the line is not a properly formatted @@INCLUDE line.
          */
    String parseAtAtIncludeLine(String _line) {
        String[] parts = _line.split("\\s+");
        if ((parts.length == 2) && parts[0].equals(AT_AT_INCLUDE)) {
            return parts[1];
        }
        else { return null; }
    }


        /**
         * Determines the showInstanceSettings parameter value.   
         * If it's not set in this node, the base nodes are searched
         * until a value is found
         */
    private boolean determineInstanceParameterSetting() {

        String[] SHOW_INSTANCE_SETTING_info = normalParameter_getInfo(
                                ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG);

        String setting_value = SHOW_INSTANCE_SETTING_info[0];
        if (setting_value == null) {
            setting_value = SHOW_INSTANCE_SETTING_info[2];
        }
                    
        if (setting_value == null) {
            return false;
        }


        if (setting_value.toLowerCase().equals("true")) {
            return true;
        }
        else {
            return false;
        }
    }

    ///////////////////////////////////////////
    // parameter declaration methods //////////
    ///////////////////////////////////////////

    //////////////////////////////////////////////////
    // parameter value modification methods //////////

        /** Checks if the ParameterSetting object is defined locally */
    public boolean parameterSetting_containsLocal (String _parameter_name) {

        // Check the normalParameters first
        String[] param_info = normalParameter_getInfo(_parameter_name);
        if (param_info != null) {
            return (param_info[0] != null);
        }
        else{
            String[] user_param_info = endUserParameters_getDetails(
                                                             _parameter_name);
            if (user_param_info == null) {
                return false;
            }

            return  (user_param_info[1] != null);
        }

    }
        /**
         * When a primitive instance exists in this node, this method
         * composes the MemberDecl primitive settings then adds them to 
         * the node.
         */
    private void composeMemberDeclIfNeeded ( ) {

        // No need to do anything if showInstanceSettings = false;
        if (!determineInstanceParameterSetting()) { return; }
        
        // This is the temporary list of ParameterSetting modified during
        // the session.  If it is null that means that there were 
        // no special variables modified during the session
        String[] member_decl = normalParameter_getInfo(FUNC_X_MEMBER_DECL);
        String member_decl_val = member_decl[0];

        if (member_decl_val != null) {
            parameterSetting_removeLocal(FUNC_X_MEMBER_DECL);
        }
        else {
            member_decl_val = "  ";
        }

        String new_value = settingReplace_memberDecl(member_decl_val);
        if (new_value != null) {
            parameterSetting_add(FUNC_X_MEMBER_DECL, new_value, "", true);
        }
    }

        /** Adds a FUNCX_ParameterSetting to the node. */
    public ParameterSetting endUserVariable_add (String _name,
                                                  String _type, String _value) {

        if ((_name != null) && (_name.length() != 0)) {

            if (m_funcx_setting_list == null) {
                m_funcx_setting_list = new ArrayList();
            }

            String[] user_param_info = endUserParameters_getDetails(_name);
            boolean b_default_value = true;

            if (user_param_info != null) {
                if (user_param_info[2] != null) { b_default_value = false; } 
            }
            
System.out.println("addSpecialVariables name = " + _name + " type " + _type+
            " value " + _value);
            FUNCX_ParameterSetting setting = new FUNCX_ParameterSetting(this,
                                       _name, _type, _value, b_default_value);

            m_funcx_setting_list.add(setting);

            return setting;
        }

        return null;
    }
            

        /** 
          * @return the new MemberDecl FUNC_X primitive setting
          */
    private String settingReplace_memberDecl (String _old_value) {
        
System.out.println("In settingReplace_memberDecl " + generateFullNodePathName());
        String old_value = new String(_old_value);
        int start_location = 0;
        int end_location = 0;
        String variable_section = new String();

    
        if (m_funcx_setting_list != null) {

            int marker_start = old_value.indexOf(FUNC_X_MEMBER_VAR_START, 0);
            int marker_end = old_value.indexOf(FUNC_X_MEMBER_VAR_END, 0) +
                            FUNC_X_MEMBER_VAR_END.length();
        
            end_location = marker_end;
    
            if (marker_start < 0) {
                // There were no previous autogenerated comments
                start_location = 0;
                end_location = 0;
            } else {
                start_location = marker_start;
            }
            variable_section = old_value.substring(start_location, 
                                                                end_location);

            FUNCX_ParameterSetting[] funcx_settings =
                       new FUNCX_ParameterSetting[m_funcx_setting_list.size()];
            m_funcx_setting_list.toArray(funcx_settings);
 
            for (int i = 0; i < funcx_settings.length; i++) {
                String value = funcx_settings[i].getValue();
                if (value.equals("DEFAULT")) {
                    System.out.println("Found DEFAULT value for "+ 
                        funcx_settings[i].getName());
                    String[] end_user_info =  
                            endUserParameters_getDetails(
                                                   funcx_settings[i].getName());
                    value = end_user_info[2];
                    System.out.println("changing value to "+ value); 
                }
                value = value.trim();
                
                if (value.length() > 0) {
                    int from_index = 0;
                    int start_of_name = 0;
                    int end_of_name = -1;
                    boolean found = false;
                    
                    while (!found && 
                            (start_of_name != variable_section.length() -1) ){
                        String setting_name = funcx_settings[i].getName();
                        // Find the Setting Variable in the section
                        start_of_name = variable_section.indexOf(setting_name, 
                                                                  start_of_name);
                        if (start_of_name >= 0) {
                            end_of_name = variable_section.indexOf(';', 
                                                                 start_of_name);
                            String test_name = "";
                            if (end_of_name >= 0) {
                                test_name = variable_section.substring(
                                                    start_of_name-1, end_of_name);
                                test_name = test_name.trim();
                                if (test_name.equals(setting_name)) {
                                    found = true;
                                }
                            }
                            start_of_name = end_of_name;
                        }
                    }
                    if (found) {
                        int start_of_member_init = variable_section.indexOf(
                            FUNC_X_MEMBER_INIT, end_of_name);
                        StringBuffer string_buffer = new StringBuffer(
                                                              variable_section);
                        if (start_of_member_init >= 0) {
                            int start_init_value = start_of_member_init +
                                FUNC_X_MEMBER_INIT.length() + 1;
                            int end_of_init_value = variable_section.indexOf(
                                ';', start_init_value);
                            string_buffer = string_buffer.replace(
                                    start_init_value, end_of_init_value, value);
                        } 
                        else {
                            int new_line = variable_section.indexOf(Parser.NL);
                            String new_init_value = "      " + 
                                      FUNC_X_MEMBER_INIT + "  " + value + ";";
                            string_buffer = 
                                    string_buffer.replace(end_of_name, new_line,
                                                                new_init_value);
                        }
                        variable_section = string_buffer.toString();
                    }
                }
            }

        }

        if (_old_value.length() > 0) {
            StringBuffer string_buffer = new StringBuffer(old_value);
            string_buffer = string_buffer.replace(start_location,
                        end_location, variable_section);
            return string_buffer.toString();
        }
        else {
            return "";
        }
    }

    
        /** Deletes all the values in the list of ParameterDeclaration
          * and ParameterSettings.
          */
    private void clearLists ( ) {

        if (m_funcx_setting_list != null) {
            m_funcx_setting_list.clear(); 
            m_funcx_setting_list = null;
        }
    }



    //////////////////////
    // generic settings //
    //////////////////////
        /** Convenience method for dealing with ParameterSetting (for
          * primitives).
          */
    public NodeSettingIF setting_add (String _name, String _value,
                                     String _comment, boolean _b_quoted) {

        boolean instance_setting = determineInstanceParameterSetting();
        
        if (!instance_setting || 
             (_name.equals(ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG)) ){
            return parameterSetting_add(_name, _value, _comment, _b_quoted);
        }
        else {
            String[] user_param_info = endUserParameters_getDetails(_name);
            if (user_param_info == null) {return null; }
            String type = user_param_info[0];
            return endUserVariable_add(_name, type, _value); 
        }
    }

    public NodeSettingIF setting_get(String _name) {

        boolean instance_setting = determineInstanceParameterSetting();
        
        if (instance_setting) {
            if (_name.equals( ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG))
                return parameterSetting_get(_name);
            else 
                return parameterSetting_get(FUNC_X_MEMBER_DECL);
        }
        else {
            return parameterSetting_get(_name);
        }
    }


        /** Performs the final steps when modifying a FUNCX_Node setting.
           * In the FUNCX_Node case, after all modifications are made
           * to the instance parameters, the primitive setting <code>
           * MemberDecl </code> is composed based on the endUserVariables
           */
    public void setting_finalize () {
        composeMemberDeclIfNeeded();
        clearLists();
    }

        /** Overrides the ALFINode method to add possible FUNCX changes too. */
    public boolean hasLocalChanges(boolean _b_include_trivial_changes) {
        return ( super.hasLocalChanges(_b_include_trivial_changes));
    }


    public String toString(boolean _b_is_long_version) {
        String node_info = super.toString(_b_is_long_version);

            node_info += 
                "\n\t- - - - - - - - - - FUNCX Specific - - - - - - - - - -\n"; 
            node_info += "\n\tnormal param info:";
            for (int i = 0; i < FUNC_X_SETTING_ORDER.length; i++) {
                String[] normal_info =
                              normalParameter_getInfo(FUNC_X_SETTING_ORDER[i]);

                node_info += "    " + FUNC_X_SETTING_ORDER[i] + ": " +
                        normal_info[0] + ", " + normal_info[1] + ", " +
                        normal_info[2] + ", " + normal_info[3] + "\n";
            }

            ArrayList info = endUserParameters_getInfo();
            node_info += "\n\tendUserParameters_getInfo =\n";
            for (Iterator i = info.iterator() ; i.hasNext(); ) {
                String[] parts = (String[]) i.next();
                node_info += "    " + parts[0] + ", " +  parts[1] + ", " +
                        parts[2] + ", " +  parts[3] + ", " +  parts[4] + 
")\n";
            }
        return node_info;
    }

    private String listContentString(ArrayList _list) {
        StringBuffer sb = new StringBuffer();
        if (_list == null) {
            sb.append(" null\n");
        }
        else {
            sb.append("\n");
            for (Iterator i = _list.iterator(); i.hasNext(); ) {
                sb.append("\t\t" + i.next() + "\n");
            }
        }

        return sb.toString();
    }

    private class EndUserParamFormatException extends Exception {
        public EndUserParamFormatException() {
            super();
        }
    }
}
