package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.bookkeeper.*;

    /** This interface defines fields and methods used by sources of 
      * UpstreamDataChannelChangeEvent sources.
      */
public interface UpstreamDataChannelChangeEventSource {
    public void addUpstreamDataChannelChangeEventListener(
                             UpstreamDataChannelChangeEventListener _listener);
    public void removeUpstreamDataChannelChangeEventListener(
                             UpstreamDataChannelChangeEventListener _listener);
}
