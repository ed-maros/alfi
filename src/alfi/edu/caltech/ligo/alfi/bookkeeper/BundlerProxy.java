package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.*;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.file.*;


    /**
     * A Bundler is used to expose the contents of and manage the insertion and
     * extraction of data streams into and out of a bundle.  Currently, only
     * locally added bundlers can accept new connections.
     * 
     * A Bundler can be used to perform these various operations:
     * 
     * 1. Input a primary bundle connection.
     * 2. Output a primary bundle connection, which is either a newly created
     *    bundle or a continuation of the primary bundle connection if such
     *    exists.
     * 3. Input or output a single secondary connection:
     *    a. If an input, then this data stream will be added into the contents
     *       of the primary output bundle.  A further option in the case where
     *       the secondary input is itself a bundle, is that the top layer of
     *       the secondary input bundle my be exposed to place the top level
     *       elements of the secondary input bundle into the primary output
     *       bundle as top level, individual chanels.  The input connection
     *       keeps track of the name of the input into the bundler.
     *    b. If an output, then the output connection keeps track of which
     *       element(s) is (are) exposed for the secondary output connection.
     *       If more than one channel is exposed, then the secondary output is a
     *       new bundle itself (albeit with a subset of the channels available
     *       in the primary.)
     */
public class BundlerProxy extends GenericPortProxy
                          implements BundleContentSource,
                                     GroupableIF,
                                     GroupElementModEventSource  {

    //**************************************************************************
    //***** static fields ******************************************************

    public static final String NAME                = "NAME";
    public static final String LOCATION            = "LOCATION";

    private static final int LOCATION_FORMAT_PROBLEM             = 0;
    private static final int INIT_LINE_FORMAT_PROBLEM            = 1;
    private static final int INVALID_CONFIGURATION_DIRECTIVE     = 2;

    //**************************************************************************
    //***** local exception classes ********************************************


    //**************************************************************************
    //***** member fields ******************************************************

        /** The location of the bundler. */
    private Point m_location;

        /** Used in conjunction with GroupableIF interface. */
    protected LayeredGroup m_group_container;

        /** Used in conjuction with GroupElementModEventSource */
    private HashMap m_group_element_mod_listeners_map;
 
     //**************************************************************************
    //***** constructors *******************************************************

        /** Main constructor.  Should only be called from ALFINode. */
    BundlerProxy(ALFINode _container_node, String _name, Point _location) {
        super(_container_node, _name);

        if ((_name == null) || (_name.length() < 1)) {
            m_name = generateDefaultName(_container_node);
        }
        else { m_name = new String(_name); }

        m_location = _location;
        m_group_element_mod_listeners_map = new HashMap();
     }

        /**
         * Auxilliary constructor which creates a proxy for temporary storage
         * of Bundler information only.  Thes objects are useless
         * elsewhere because the (final) container node field is null.
         */
    public BundlerProxy(Point _location, String _name) {
        super();

        m_location = _location;
        m_name = _name;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// class methods //////////////////////////////////////////////////////

        /** Parses the text created in generateParserRepresentation().  Returns
          * an HashMap with the following BundlerProxy keys and object values:
          *
          * NAME, String
          * LOCATION, Point
          *
          * Expects _bundler_description formatted as follows.  Secondary input
          * configuration keywords are exclusive of the secondary output
          * keywords (and vice-versa.)
          *
          * <PRE>
          * Bundler_6
          * {
          *     72x72
          *     SECONDARY_OUTPUT_CHANNEL_NAME xxx (bundle)
          *     SECONDARY_OUTPUT_CHANNEL_NAME yyy
          *     SECONDARY_OUTPUT_CHANNEL_NAME zzz
          *     SECONDARY_INPUT_NAME aaa
          *     SECONDARY_INPUT_UNWRAPS
          * </PRE>
          */
    public static HashMap parse(Parser _parser, String[] _bundler_description)
                                               throws AlfiFileFormatException {
            // utility constants
        final String CHANNEL_OUT= Parser.BUNDLER__SECONDARY_OUTPUT_CHANNEL_NAME;
        final String INPUT_NAME = Parser.BUNDLER__SECONDARY_INPUT_NAME;
        final String INPUT_UNWRAPS = Parser.BUNDLER__SECONDARY_INPUT_UNWRAPS;

        HashMap element_map = new HashMap();
        String[] lines = _bundler_description;

            // cleanup first
        for (int i = 0; i < lines.length; i++) {
            lines[i].trim();
            if (lines[i].startsWith(Parser.ALFI_ONLY_MARKER)) {
                lines[i] = lines[i].substring(Parser.ALFI_ONLY_MARKER.length());
            }
            lines[i].trim();
        }

            // get name
        String[] parts = lines[0].split("\\s+");
        if (parts.length == 1) {
            element_map.put(NAME, parts[0]);
        }
        else {
            throwFormatException(_parser, _bundler_description,
                                                     INIT_LINE_FORMAT_PROBLEM);
        }

        if (lines[1].equals(Parser.SECTION_BEGIN_MARKER) &&
                   lines[lines.length - 1].equals(Parser.SECTION_END_MARKER)) {
            HashMap channel_out_map = null;
            for (int i = 2; i < lines.length - 1; i++) {
                if (lines[i].matches("\\d+x\\d+")) {
                    String[] coordinates = lines[i].split("x");
                    if (coordinates.length == 2) {
                        try {
                            int x = (new Integer(coordinates[0])).intValue();
                            int y = (new Integer(coordinates[1])).intValue();
                            element_map.put(LOCATION, new Point(x, y));
                        }
                        catch (NumberFormatException _e) {
                            throwFormatException(_parser, _bundler_description,
                                                      LOCATION_FORMAT_PROBLEM);
                        }
                    }
                    else {
                        throwFormatException(_parser, _bundler_description,
                                              INVALID_CONFIGURATION_DIRECTIVE);
                    }
                }
                else {
                    parts = lines[i].split("\\s+");
                    if (parts.length == 1) {
                        if (parts[0].equals(INPUT_UNWRAPS)) {
                            element_map.put(INPUT_UNWRAPS, null);
                        }
                        else {
                            throwFormatException(_parser, _bundler_description,
                                              INVALID_CONFIGURATION_DIRECTIVE);
                        }
                    }
                    else if ((parts.length == 2) || (parts.length == 3)) {
                        if (parts[0].equals(CHANNEL_OUT)) {
                            if (channel_out_map == null) {
                                channel_out_map = new HashMap();
                            }

                            boolean b_is_bundle = ((parts.length == 3) &&
                                                (parts[2].equals("(bundle)")));
                            channel_out_map.put(parts[1],
                                                new Boolean(b_is_bundle));
                        }
                        else if (parts[0].equals(INPUT_NAME)) {
                            element_map.put(INPUT_NAME, parts[1]);
                        }
                        else {
                            throwFormatException(_parser, _bundler_description,
                                              INVALID_CONFIGURATION_DIRECTIVE);
                        }
                    }
                    else {
                        throwFormatException(_parser, _bundler_description,
                                              INVALID_CONFIGURATION_DIRECTIVE);
                    }
                }
            }
            element_map.put(CHANNEL_OUT, channel_out_map);
        }

        return element_map;
    }

        /** Utility method for throwing format exceptions in bundler parsing. */
    private static void throwFormatException(Parser _parser,
                              String[] _bundler_description, int _error_code)
                                               throws AlfiFileFormatException {
        String file_path = _parser.getFileName();
        String message = null;
        switch (_error_code) {
          case LOCATION_FORMAT_PROBLEM:
            message = "Format problem with bundler location.";
            break;
          case INIT_LINE_FORMAT_PROBLEM:
            message= "Format problem with initial line in bundler description.";
            break;
          case INVALID_CONFIGURATION_DIRECTIVE:
            message= "Invalid bundler configuration directive.";
            break;
        }

        StringBuffer sb = new StringBuffer(_bundler_description[0]);
        for (int i = 1; i < _bundler_description.length; i++) {
            sb.append(Parser.NL);
            sb.append(_bundler_description[i]);
        }

        throw new AlfiFileFormatException(message, file_path, sb.toString());
    }

        /** Utility for initial parsing of lines involving connections.  */
    private static String retrieveConnectionString(String[] _tokens,
                                                   int _token_index) {
        StringBuffer sb = new StringBuffer(_tokens[_token_index]);
        for (int i = _token_index + 1; i < _token_index + 5; i++) {
            sb.append(" ");
            sb.append(_tokens[i]);
        }
        return sb.toString();
    }

        /** Convenience call to BundlerIOSetting.checkForInvalidIOName(). */
    public static boolean inputChannelNameIsValid(String _name) {
        return (BundlerIOSetting.checkForInvalidIOName(_name, false) == null);
    }

        /** Convenience call to BundlerIOSetting.checkForInvalidIOName(). */
    public static boolean outputChannelPathNameIsValid(String _name) {
        return (BundlerIOSetting.checkForInvalidIOName(_name, true) == null);
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// implementations of abstract GenericPortProxy methods ///////////////

        /** Determines the data type which is output from this port as a source
          * for connection _connection.
          */
    public int determineSourceDataTypeForConnection(ALFINode _container,
                                                 ConnectionProxy _connection) {
        assert(_container != null);
        assert(_connection.getSource() == this);

        if (_connection instanceof BundleProxy) {
            return DATA_TYPE_ALFI_BUNDLE;
        }
        else {
            Hashtable content = getSecondaryOutContent(_container, true);
            while (content.size() == 1) {
                BundleContentMapKey key =
                      (BundleContentMapKey) content.keySet().iterator().next();
                if (key.isContentHolderOnly()) {
                    content = (Hashtable) content.get(key);
                }
                else { return key.getDataType(); }
            }

            Alfi.warn("("+ this+ ").determineSourceDataTypeForConnection(" +
                                     _container + ", " + _connection + "):\n" +
                    "\tError: Multiple channel out names for non-bundle.");
            return DATA_TYPE_INVALID;
        }
    }

        /** Determines the data type which is input to this port as a sink
          * for connection _connection.
          */
    public int determineSinkDataTypeForConnection(ALFINode _container,
                                                 ConnectionProxy _connection) {
        assert(_container != null);
        assert(_connection.getSink() == this);

        if (_connection instanceof BundleProxy) {
            return DATA_TYPE_ALFI_BUNDLE;
        }
            // secondary input into a bundler
        else { return DATA_TYPE_1_UNRESOLVED; }
    }

        /** Implementation of GenericPortProxy method.  The returned map will
          * contain { primary_input, _container } and { secondary_input,
          * _container } if both or either exist.  [_owner_member is ignored for
          * bundlers.]
          */
    protected HashMap getUpstreamConnections(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        HashMap container_map = new HashMap();

        if (hasPrimaryInput(_container)) {
            container_map.put(getPrimaryInput(_container), _container);
        }

        if (hasSecondaryInput(_container)) {
            container_map.put(getSecondaryInput(_container), _container);
        }

        return container_map;
    }

        /** Implementation of GenericPortProxy method.  The returned map will
          * contain { primary_output, _container } and { secondary_output,
          * _container } if both or either exist.  [_owner_member is ignored for
          * bundlers.]
          */
    protected HashMap getDownstreamConnections(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        HashMap container_map = new HashMap();

        if (hasPrimaryOutput(_container)) {
            container_map.put(getPrimaryOutput(_container), _container);
        }

        if (hasSecondaryOutput(_container)) {
            container_map.put(getSecondaryOutput(_container), _container);
        }

        return container_map;
    }

        /** Any connections to this port on _owner_member in _container_node?
          * If _owner_member is passed as null, then the port is taken to be a
          * port on _container_node.
          */
    public boolean hasConnections(ALFINode _container_node,
                                  MemberNodeProxy _owner_member) {
        return hasConnections(_container_node);
    }

        /** Returns an array of the connections linked to this port, when
          * _owner_member is the member node in _container_node on which the
          * port sits (this same port object can appear on multiple members
          * if they are derived from the same base.)  If _owner_member is null,
          * this port object is assumed to be an external port on
          * _container_node.
          */
    public ConnectionProxy[] getConnections(ALFINode _container_node,
                                            MemberNodeProxy _owner_member) {
        return getConnections(_container_node);
    }

        /** Create a default name for the bundler. */
    protected String generateDefaultName(ALFINode _container_node) {
        BundlerProxy[] bundlers = _container_node.bundlers_get();
        String[] taken_names = new String[bundlers.length];
        for (int i = 0; i < bundlers.length; i++) {
            taken_names[i] = bundlers[i].getName();
        }

        String root = "Bundler_";
        int n = 0;
        String name = "";
        boolean b_name_taken = true;
        while (b_name_taken) {
            name = root + n;
            b_name_taken = false;
            for (int i = 0; i < taken_names.length; i++) {
                if (name.equals(taken_names[i])) {
                    b_name_taken = true;
                    n++;
                    break;
                }
            }
        }
        return name;
    }

        /** Creates a short and long (possibly multi-line) text description of
          * itself for use in status text and elsewhere.  This is a pretty
          * version and is not intended as something that may be parsed.
          * So it may be changed about without fear of breaking box files.
          */
    public String generateSnippetForConnectionDescription(ALFINode _container,
                        ConnectionProxy _connection, boolean _b_long_version) {
        assert(_container != null);
        assert(_connection != null);
        assert(_container.connection_contains(_connection));

        GenericPortProxy source = _connection.getSource();
        GenericPortProxy sink = _connection.getSink();

        StringBuffer sb = new StringBuffer();

        if (this == source) {
            if (_connection == getPrimaryOutput(_container)) {
                sb.append(getName() + " (1st Out)");
            }
            else { sb.append(getName() + " (2nd Out)"); }
        }
        else if (this == sink) {
            if (_connection == getPrimaryInput(_container)) {
                sb.append(getName() + " (1st In)");
            }
            else { sb.append(getName() + " (2nd In)"); }
        }
        else {
            Alfi.warn("(" + this + ").generateSnippetForConnectionDescription" +
                      "(" + _container + ", [" + _connection + "], " +
                      _b_long_version + "): Invalid call.");
            return null;
        }

        return sb.toString();
    }

        /** Just propogates the update signal to any downstream connections
          * (no matter whether primary or secondary.)
          */
    public void updateContentAndPropogateDownstream(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        BundleProxy primary_out = getPrimaryOutput(_container);
        if (primary_out != null) {
            primary_out.updateContentAndPropogateDownstream(_container);
        }

        ConnectionProxy secondary_out = getSecondaryOutput(_container);
        if (secondary_out != null) {
            secondary_out.updateContentAndPropogateDownstream(_container);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// general methods ////////////////////////////////////////////////////

    public Point getLocation() { return new Point(m_location); }

    public void setLocation(Point _location) {
        if (! _location.equals(m_location)) {
            m_location = GridTool.getNearestGridPoint(_location);
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce =
                new ProxyChangeEvent(this, new BundlerProxy(m_location, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent ceme = new ContainedElementModEvent(this);
            fireContainedElementModEvent(ceme);
        }
    }

    private BundlerIOSetting getIOSetting(ALFINode _container) {
        assert(_container.bundler_contains(this));

        return _container.bundlerSetting_get(this);
    }

        /** Currently just fires off generic ProxyChangeEvents and
          * ContainedElementModEvents to listeners.
          */
    void signalLocalIOChange() {
        ProxyChangeEvent pce =
                      new ProxyChangeEvent(this, new BundlerProxy(null, null));
        fireProxyChangeEvent(pce);

        ContainedElementModEvent ceme = new ContainedElementModEvent(this);
        fireContainedElementModEvent(ceme);
    }

    public boolean hasPrimaryInput(ALFINode _container) {
        return (getPrimaryInput(_container) != null);
    }

    public BundleProxy getPrimaryInput(ALFINode _container) {
        ConnectionProxy[] connections = _container.connections_get();
        for (int i = 0; i < connections.length; i++) {
            if ((connections[i] instanceof BundleProxy) &&
                     (connections[i].getSink() == this) &&
                     ((BundleProxy) connections[i]).isSinkBundlerPrimaryIn()) {
                return ((BundleProxy) connections[i]);
            }
        }
        return null;
    }

    public void setPrimaryInput(ALFINode _container, BundleProxy _bundle) {
        assert(_container.bundler_contains(this));
        assert((_bundle == null) || _container.connection_contains(_bundle));

        while (hasPrimaryInput(_container)) {
            BundleProxy old_primary_in = getPrimaryInput(_container);
            old_primary_in.setSinkBundlerPrimaryInFlag(false);
        }
        if (_bundle != null) { _bundle.setSinkBundlerPrimaryInFlag(true); }

        ProxyChangeEvent pce =
                      new ProxyChangeEvent(this, new BundlerProxy(null, null));
        fireProxyChangeEvent(pce);

        _container.setTimeLastModified();
    }

    public boolean hasPrimaryOutput(ALFINode _container) {
        return (getPrimaryOutput(_container) != null);
    }

    public BundleProxy getPrimaryOutput(ALFINode _container) {
        assert(_container.bundler_contains(this));

        ConnectionProxy[] connections = _container.connections_get();
        for (int i = 0; i < connections.length; i++) {
            if ((connections[i] instanceof BundleProxy) &&
                    (connections[i].getSource() == this) &&
                    ((BundleProxy) connections[i]).isSourceBundlerPrimaryOut()){
                return ((BundleProxy) connections[i]);
            }
        }
        return null;
    }

    public void setPrimaryOutput(ALFINode _container, BundleProxy _bundle) {
        assert(_container.bundler_contains(this));
        assert((_bundle == null) || _container.connection_contains(_bundle));

        while (hasPrimaryOutput(_container)) {
            BundleProxy old_primary_out = getPrimaryOutput(_container);
            old_primary_out.setSourceBundlerPrimaryOutFlag(false);
        }
        if (_bundle != null) { _bundle.setSourceBundlerPrimaryOutFlag(true); }

        ProxyChangeEvent pce =
                      new ProxyChangeEvent(this, new BundlerProxy(null, null));
        fireProxyChangeEvent(pce);

        _container.setTimeLastModified();
    }

    public boolean hasSecondaryOutput(ALFINode _container) {
        return (getSecondaryOutput(_container) != null);
    }

        /** Returns the secondary channel out if such exists. */
    public ConnectionProxy getSecondaryOutput(ALFINode _container) {
        assert(_container.bundler_contains(this));

        BundleProxy primary_out = getPrimaryOutput(_container);
        ConnectionProxy[] connections = getConnections(_container);
        for (int i = 0; i < connections.length; i++) {
            if ((connections[i].getSource() == this) &&
                                             (connections[i] != primary_out)) {
                return connections[i];
            }
        }

        return null;
    }

        /** Returns a map of { String, Boolean } pairs.  Name and whether the
          * channel is a bundle.
          */
    public HashMap getSecondaryOutputChannelNames(ALFINode _container) {
        BundlerIOSetting io_setting = getIOSetting(_container);
        if (io_setting != null) {
            HashMap output_name_map = io_setting.getSecondaryOutputMap();
            if (output_name_map != null) {
                return output_name_map;
            }
        }

        return new HashMap();
    }

    public boolean secondaryOutChannelIsBundle(ALFINode _container,
                                                        String _channel_name) {
        HashMap output_map = getSecondaryOutputChannelNames(_container);
        if (output_map.containsKey(_channel_name)) {
            Boolean B_is_bundle = (Boolean) output_map.get(_channel_name);
            return B_is_bundle.booleanValue();
        }

        Alfi.warn("(" + this + ").secondaryOutChannelIsBundle(" +
                _container + ", " + _channel_name + "): WARNING: Channel " +
                "name not found.");
        return false;
    }

    public void setSecondaryOutputChannelNames(ALFINode _container,
                                                         HashMap _names_info) {
        assert(_container.bundler_contains(this));

        if (BundlerIOSetting.checkForInvalidIONames(_names_info,false) == null){
            _container.bundlerSetting_add(this, _names_info);
        }
        else {
            String bad_name =
                   BundlerIOSetting.checkForInvalidIONames(_names_info, false);
            Alfi.warn("(" + this + ").setSecondaryOutputChannelNames(...):\n" +
                "Warning: Invalid output channel name [" + bad_name + "].\n" +
                "Secondary output not changed.");
        }
    }

        /** Does this bundler have a secondary data channel input? */
    public boolean hasSecondaryInput(ALFINode _container) {
        assert(_container.bundler_contains(this));

        return (getSecondaryInput(_container) != null);
    }

        /** Returns the secondary channel in if such exists. */
    public ConnectionProxy getSecondaryInput(ALFINode _container) {
        assert(_container.bundler_contains(this));

        BundleProxy primary_in = getPrimaryInput(_container);
        ConnectionProxy[] connections = getConnections(_container);
        for (int i = 0; i < connections.length; i++) {
            if ((connections[i].getSink() == this) &&
                                              (connections[i] != primary_in)) {
                return connections[i];
            }
        }

        return null;
    }

        /** Determines the content of a bundler's secondary input.  This call
          * is invalid for bundlers without a secondary input.
          */
    Hashtable getSecondaryInContent(ALFINode _container,
                                               boolean _b_use_cached_content) {
        assert(hasSecondaryInput(_container));

        Hashtable content = null;

        ConnectionProxy secondary_in = getSecondaryInput(_container);
        String secondary_in_name =
                              getIOSetting(_container).getSecondaryInputName();
            // (key not used in unwrapped case)
        BundleContentMapKey key =
            new BundleContentMapKey(_container, this, secondary_in_name,
                           secondary_in.determineUpstreamDataType(_container));
        if (secondary_in instanceof BundleProxy) {
            content = ((BundleProxy) secondary_in).getContent(_container,
                                                        _b_use_cached_content);

            if (! secondaryInputUnwraps(_container)) {
                Hashtable wrapped_content = new Hashtable();
                wrapped_content.put(key, content);
                content = wrapped_content;
            }
        }
        else {
            content = new Hashtable();
            content.put(key, "");
        }

        return ((content == null) ? new Hashtable() : content);
    }

    public void setSecondaryInputName(ALFINode _container, String _name) {
        assert(_container.bundler_contains(this));

        BundlerIOSetting old_setting = getIOSetting(_container);
        boolean b_old_setting_is_local =
                                 _container.bundlerSetting_containsLocal(this);
        if ((old_setting == null) || old_setting.isSecondaryInInfo()) {
            if (_name == null) {
                if (b_old_setting_is_local) {
                    _container.bundlerSetting_removeLocal(this);
                }
            }
            else if (inputChannelNameIsValid(_name)) {
                boolean b_unwraps =
                    ((old_setting != null) && old_setting.unwrapsInputBundle());
                _container.bundlerSetting_add(this, _name, b_unwraps);
            }
            else {
                Alfi.warn("(" + this + ").setSecondaryInputName(" + _name +
                    "): Warning: Invalid channel name.  Not set.");
                return;
            }
        }
        else {
            Alfi.warn("(" + this + ").setSecondaryInputName(" + _name +
                "):\n\nWarning: Attempting to set input name for an output\n" +
                "bunler.  Call is invalid and has been ignored.");
            return;
        }
    }

    public String getSecondaryInputName(ALFINode _container) {
        assert(_container.bundler_contains(this));

        BundlerIOSetting io_setting = getIOSetting(_container);
        if (io_setting != null) {
//assert(io_setting.isSecondaryInInfo());
            return io_setting.getSecondaryInputName();
        }
        else { return null; }
    }

    public boolean secondaryInputUnwraps(ALFINode _container) {
        BundlerIOSetting io_setting = getIOSetting(_container);
        if (io_setting != null) {
//assert(io_setting.isSecondaryInInfo());
            return io_setting.unwrapsInputBundle();
        }
        else { return false; }
    }

    public void setSecondaryInputUnwraps(ALFINode _container,
                                                          boolean _b_unwraps) {
        assert(_container.bundler_contains(this));

        BundlerIOSetting old_io_setting = getIOSetting(_container);
        if (old_io_setting != null) {
//assert(old_io_setting.isSecondaryInInfo());

            String old_input_name = old_io_setting.getSecondaryInputName();
            _container.bundlerSetting_add(this, old_input_name, _b_unwraps);
        }
    }

        /** Sets name and unwrap flag. */
    public void setSecondaryInputConfiguration(ALFINode _container,
                                            String _name, boolean _b_unwraps) {
        assert(_container.bundler_contains(this));

        _container.bundlerSetting_add(this, _name, _b_unwraps);
    }

    public boolean hasSecondaryIO(ALFINode _container) {
        return (hasSecondaryInput(_container)||hasSecondaryOutput(_container));
    }

        /** Determines the content of a bundler's secondary output.  This call
          * is invalid for bundlers without a secondary output.
          */
    Hashtable getSecondaryOutContent(ALFINode _container,
                                               boolean _b_use_cached_content) {
        assert(hasSecondaryOutput(_container));
        final char BUNDLE_DELIM = '.';

        Hashtable primary_content = hasPrimaryInput(_container) ?
                  getPrimaryInput(_container).getContent(_container,
                                      _b_use_cached_content) : new Hashtable();
        Hashtable flat_content =
                   BundleProxy.contentMapToFlatChannelMap(primary_content, "");
        Hashtable filtered_flat_content = new Hashtable();
        HashMap secondary_out_map = getSecondaryOutputChannelNames(_container);
        String[] secondary_out_names = new String[secondary_out_map.size()];
        secondary_out_map.keySet().toArray(secondary_out_names);

        for (int j = 0; j < secondary_out_names.length; j++) {
                // we want to match channels names that are either equal to
                // a secondary out name, as well as channel names which
                // are channels inside a secondary out when the secondary out
                // is actually a bundle.
            String pattern = "^" + secondary_out_names[j] + "\\b.*";
            for (Iterator i = flat_content.keySet().iterator(); i.hasNext(); ) {
                String channel_name = (String) i.next();
                if (channel_name.matches(pattern)) {
                    filtered_flat_content.put(channel_name,
                                               flat_content.get(channel_name));
                }
            }
        }

            // add missing unresolved channels
        for (int i = 0; i < secondary_out_names.length; i++) {
            int delim_index =
                          secondary_out_names[i].lastIndexOf(BUNDLE_DELIM) + 1;
            String channel_name = secondary_out_names[i].substring(delim_index);
            if (! filtered_flat_content.containsKey(secondary_out_names[i])) {
                int data_type =
                    (getSecondaryOutput(_container) instanceof BundleProxy) ?
                                DATA_TYPE_ALFI_BUNDLE : DATA_TYPE_1_UNRESOLVED;
                BundleContentMapKey content_key = new BundleContentMapKey(
                                    _container, this, channel_name, data_type);
                filtered_flat_content.put(secondary_out_names[i], content_key);
            }
        }

            // add any needed holder bundle elements
        Hashtable holder_bundle_additions = new Hashtable();
        for (Iterator i = filtered_flat_content.keySet().iterator();
                                                               i.hasNext(); ) {
            String channel_name = (String) i.next();
            BundleContentMapKey content_key =
                 (BundleContentMapKey) filtered_flat_content.get(channel_name);
            String[] path_parts = channel_name.split("\\.");
            if (path_parts.length > 1) {
                String bundle_path = new String();
                for (int j = 0; j < path_parts.length - 1; j++) {
                    String bundle_name = path_parts[j];
                    bundle_path += bundle_name;
                    if (! filtered_flat_content.containsKey(bundle_path)) {
                        BundleContentMapKey holder_key =
                                    new BundleContentMapKey(_container, this,
                                           bundle_name, DATA_TYPE_ALFI_BUNDLE);

                            // holder_key is not really part of the content,
                            // but is needed as a holder bundle for other
                            // elements of the content which are inside it
                        holder_key.setIsContentHolderOnly(true);

                        holder_bundle_additions.put(bundle_path, holder_key);
                    }
                    bundle_path += ".";
                }
            }
        }
        filtered_flat_content.putAll(holder_bundle_additions);

            // if the only secondary output name associated with this bundler
            // is a sub-bundle, then it is implicitly unwrapped.  in other words
            // querying the content of the secondary output bundle will not
            // give the sub-bundle itself, but a list of the top-level channels
            // from inside the sub-bundle.
            //
            // to clarify, consider if the output called for 2 sub-bundles
            // instead of just one: in this case, the top level content would
            // list sub_bundle_1 and sub_bundle_2, with neither of them
            // "unwrapped".
        if (secondary_out_names.length == 1) {
            String output_name = secondary_out_names[0];
            BundleContentMapKey key_for_output_name =
                  (BundleContentMapKey) filtered_flat_content.get(output_name);
            if (key_for_output_name.channelIsBundle()) {
                    // unwrap by getting rid of this bundle's key, as well as
                    // any holder bundle containing it
                for (Iterator i = filtered_flat_content.keySet().iterator();
                                                               i.hasNext(); ) {
                    String channel_name = (String) i.next();
                    if (output_name.startsWith(channel_name)) { i.remove(); }
                }
            }
        }

        Hashtable content =
                 BundleProxy.flatChannelMapToContentMap(filtered_flat_content);

        return content;
    }

        /** Creates a string suitable for describing this bundler in file. */
    public String generateParserRepresentation(ALFINode _container,
                                                     String _starting_spacer) {
        final String NL = Parser.NL;

        StringBuffer sb = new StringBuffer(_starting_spacer);
        sb.append(m_name);

        sb.append(NL);
        sb.append(_starting_spacer);
        sb.append(Parser.SECTION_BEGIN_MARKER);

        sb.append(NL);
        sb.append(_starting_spacer);
        sb.append(Parser.STANDARD_SPACER);
        sb.append(m_location.x + "x" + m_location.y);

        if (hasSecondaryOutput(_container)) {
            HashMap output_name_map= getSecondaryOutputChannelNames(_container);
            for (Iterator i = output_name_map.keySet().iterator();i.hasNext();){
                String output_name = (String) i.next();
                Boolean hint = (Boolean) output_name_map.get(output_name);
                boolean b_is_bundle = (hint != null) && hint.booleanValue();

                sb.append(NL);
                sb.append(_starting_spacer);
                sb.append(Parser.STANDARD_SPACER);
                sb.append(Parser.BUNDLER__SECONDARY_OUTPUT_CHANNEL_NAME);
                sb.append(" ");
                sb.append(output_name);
                if (b_is_bundle) { sb.append(" (bundle)"); }
            }
        }

        if (hasSecondaryInput(_container)) {
            sb.append(NL);
            sb.append(_starting_spacer);
            sb.append(Parser.STANDARD_SPACER);
            sb.append(Parser.BUNDLER__SECONDARY_INPUT_NAME);
            sb.append(" ");
            sb.append(getSecondaryInputName(_container));

            if (secondaryInputUnwraps(_container)) {
                sb.append(NL);
                sb.append(_starting_spacer);
                sb.append(Parser.STANDARD_SPACER);
                sb.append(Parser.BUNDLER__SECONDARY_INPUT_UNWRAPS);
            }
        }

        sb.append(NL);
        sb.append(_starting_spacer);
        sb.append(Parser.SECTION_END_MARKER);

        return sb.toString();
    }

    public String toString() { return getName(); }

    //////////////////////////////////////////////////////
    ////////// GroupableIF Methods ///////////////////////

        /** GroupableIF method */
    public LayeredGroup group_get() {return m_group_container; };

        /** GroupableIF method */
    public void group_set(LayeredGroup _group) {
        m_group_container = _group;

        if (m_group_container == null) {
            m_group_element_mod_listeners_map.clear();
        }
    }
        /** GroupableIF method */
    public String getElementName() { return getName(); }


         /** GroupableIF method */
    public void group_deleteElement( ) {
    }

        /** GroupableIF method */
    public void group_moveElement( ) {
    }
        /** GroupableIF method */
    public void group_registerListener(
                                      GroupElementModEventListener _listener) {
        addGroupElementModListener(_listener);
    }

    /////////////////////////////////////////////////////////////////
    ////////// GroupElementModEventSource methods ///////////////

        /** GroupElementModEventSource method. */
    public void addGroupElementModListener(
                        GroupElementModEventListener _listener) {
        m_group_element_mod_listeners_map.put(_listener, null);
    }

        /** GroupElementModEventSource method. */
    public void removeGroupElementModListener(
                        GroupElementModEventListener _listener) {
        m_group_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with GroupElementModEventSource. */
    public void fireGroupElementModEvent(GroupElementModEvent e) {
        for (Iterator i =
                m_group_element_mod_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((GroupElementModEventListener) i.next()).groupElementModified(e);
        }
    }
}
