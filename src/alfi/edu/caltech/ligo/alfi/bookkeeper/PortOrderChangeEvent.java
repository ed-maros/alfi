package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;


    /**
     * This event is sent to a listening node and from there is also forwarded
     * on to any derived nodes' active edit sessions so they can update
     * themselves as well.
     */
public class PortOrderChangeEvent extends EventObject {

    //**************************************************************************
    //***** fields *************************************************************

    private final PortProxy m_port;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public PortOrderChangeEvent(Object _source, PortProxy _port) {
        super(_source);
        m_port = _port;
    }

    //**************************************************************************
    //***** methods ************************************************************


    /////////////////////////////////
    // member field access //////////

    public PortProxy getPort() { return m_port; }
}
