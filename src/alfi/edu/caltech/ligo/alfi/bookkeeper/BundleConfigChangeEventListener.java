package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /** The interface for objects who wish to register as listeners for
      * BundleConfigChangeEvents.
      */
public interface BundleConfigChangeEventListener extends EventListener {
    public void bundleConfigChange(BundleConfigChangeEvent _event);
}
