package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.*;
import java.util.*;

/**
  * This interface defines methods needed for grouping alfi objects
  *
  */
public interface GroupableIF {

        /** Get the LayeredGroup this object is in. */
    public abstract LayeredGroup group_get(); 

        /** Set the LayeredGroup this object is in. */
    public abstract void group_set(LayeredGroup _group);

        /** Gets the name of the element */
    public abstract String getElementName();

        /** Delete the element.  Must be implemented. */
    public abstract void group_deleteElement();

        /** Move the element.  Must be implemented. */
    public abstract void group_moveElement();

        /** Register the listener to element.  Must be implemented. */
    public abstract void group_registerListener(
                    GroupElementModEventListener _listener);
}

