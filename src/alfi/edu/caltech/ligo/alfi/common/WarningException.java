package edu.caltech.ligo.alfi.common;

import edu.caltech.ligo.alfi.tools.AlfiException;

/**    
  * <pre>
  * An exception that is used to caution users.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class WarningException extends AlfiException {

	public WarningException(String msg){
		super(msg, ALFI_WARNING);
	}
}
