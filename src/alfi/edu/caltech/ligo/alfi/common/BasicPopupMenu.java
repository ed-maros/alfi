package edu.caltech.ligo.alfi.common;

import java.util.HashMap;

import java.awt.event.*;
import java.awt.*;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import edu.caltech.ligo.alfi.editor.ALFIView;
import edu.caltech.ligo.alfi.summary.AlfiMainFrame;
import edu.caltech.ligo.alfi.common.AlfiFrame;
import edu.caltech.ligo.alfi.Alfi;


/**    
  * <pre>
  * The basic popup menu bar.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see javax.swing.JPopupMenu
  */
public class BasicPopupMenu extends JPopupMenu { 

    private Actions m_actions;
    private HashMap m_menu_item_map = new HashMap();

    /**
      * Constructor only requires Actions.
      *
      * @see edu.caltech.ligo.alfi.common.Actions
      */
    public BasicPopupMenu (Actions _actions){
        super();
        m_actions = _actions;
        addPopupMenuListener(Alfi.getPopupListener());
    }

    /**
      * returns the Actions associated with this BasicPopupMenu
      *
      * @see edu.caltech.ligo.alfi.common.Actions
      */
    public Actions getActions(){
        return m_actions;
    }

    protected JMenu createCascadeMenu (String _key){
        String name = null;
        try {
            name = (String) m_actions.getResources().
                    get(_key,BasicResourceBundle.NAME);
        } 
        catch (Exception ex) {
            name = AlfiConstants.UNDEFINED;
        }

        JMenu menu = new JMenu(name);
        add(menu);
        m_menu_item_map.put(_key, menu);
        return menu;
    }
    /**
      *
      * Protected accessor method for creating a JMenuItem
      *
      * @param _key - The key that maps to the resource discriptions.
      * @return javax.swing.JMenuItem
      */
    protected JMenuItem createMenu (String _key){
        BasicActionIF action = m_actions.getAction(_key);
        String name = null;
        JMenuItem menu_item = add(action);
        m_menu_item_map.put(_key, menu_item);
        try {
            name = (String) m_actions.getResources().
                    get(_key,BasicResourceBundle.NAME);
        }
        catch (Exception ex) {
            //missing resource exception
            name = AlfiConstants.UNDEFINED;
        }
        
        try {
            Character mnemonic =  null;
            mnemonic =  (Character) m_actions.getResources().
                get(_key,UIResourceBundle.MNEMONIC);
            menu_item.setMnemonic(mnemonic.charValue());
        }
        catch (Exception ex_) {
            //missing resource means no mnemonic
        }

        try {
            KeyStroke keyStroke =  (KeyStroke) m_actions.getResources().
                get(_key,UIResourceBundle.KEY_STROKE);
            menu_item.setAccelerator(keyStroke);
        }
        catch (Exception ex_) {
            //missing resource means no keystroke
        }
        menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
        return menu_item;
    }

    /**
      *
      * Protected accessor method for creating a JMenuItem
      *
      * @param _menu The menu interface to add a Menu item to.
      * @return The created and bound menu item.
      * @see javax.swing.JMenuItem
      */
    protected JMenuItem createMenuItem (JMenu _menu, String _key){
        BasicActionIF action = m_actions.getAction(_key);

        JMenuItem menu_item = _menu.add(action);
        m_menu_item_map.put(_key, menu_item);
        try {
            Character mnemonic =  null;
            mnemonic =  (Character) m_actions.getResources().
                         get(_key,UIResourceBundle.MNEMONIC);
            menu_item.setMnemonic(mnemonic.charValue());
        }
        catch (Exception ex) {
            //missing resource means no mnemonic
        }

        try {
            KeyStroke keyStroke = (KeyStroke) m_actions.getResources().
                                  get(_key,UIResourceBundle.KEY_STROKE);
            menu_item.setAccelerator(keyStroke);
        }
        catch (Exception ex) {
            //missing resource means no mnemonic
        }
        
        menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
        return menu_item;
    }


    /**
      *
      * Protected accessor method for creating a JCheckBoxMenuItem
      *
      * @param _menu The menu interface to add a Menu item to.
      * @return The created and bound menu item.
      * @see javax.swing.JCheckBoxMenuItem
      */
    protected JMenuItem createCheckBoxMenuItem ( String _key){
        BasicActionIF action = m_actions.getAction(_key);

        JMenuItem menu_item = new JCheckBoxMenuItem(action);
        m_menu_item_map.put(_key, menu_item);
        String name = null;
        try {
            name = (String) m_actions.getResources().
                    get(_key,BasicResourceBundle.NAME);
        } 
        catch (Exception ex) {
            name = AlfiConstants.UNDEFINED;
        }
        add(menu_item);

        try {
            Character mnemonic =  null;
            mnemonic =  (Character) m_actions.getResources().
                         get(_key,UIResourceBundle.MNEMONIC);
            menu_item.setMnemonic(mnemonic.charValue());
        }
        catch (Exception ex) {
            //missing resource means no mnemonic
        }

        try {
            KeyStroke keyStroke = (KeyStroke) m_actions.getResources().
                                  get(_key,UIResourceBundle.KEY_STROKE);
            menu_item.setAccelerator(keyStroke);
        }
        catch (Exception ex) {
            //missing resource means no mnemonic
        }
        
        menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
        add(menu_item);
        return menu_item;
        
    }


    /**
      *
      * Disable/enable the menu item
      *
      * @param _key The menu item label
      * @see javax.swing.JMenuItem
      */
    public void setEnabled (String _key, boolean _enabled){
        JMenuItem menu_item = (JMenuItem) m_menu_item_map.get(_key);
        if (menu_item != null) {
            menu_item.setEnabled(_enabled);
        }
    }

    /**
      *
      * Workaround to a problem where the popup menu disappears off
      * the screen
      *
      * @param _component - component 
      */
    private void adjustPopupPositionToFit (Component _component, Point _point) {

        int scrWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int scrHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        int oldX = _point.x; 
        int oldY = _point.y;
        Dimension d = getPreferredSize();
        Point cp = new Point(_point.x, _point.y);

        //Translate point to Screen Coordinates 
        if( _component != null) {
            SwingUtilities.convertPointToScreen(cp, _component);
        }
        _point.x = cp.x; 
        _point.y = cp.y;

        if ( (_point.x + d.width) > scrWidth ) {
            _point.x = scrWidth - d.width;
        }

        if ( (_point.y + d.height) > scrHeight) {
            _point.y = scrHeight - d.height;
        }

        if ( _component != null) {
            SwingUtilities.convertPointFromScreen(_point, _component );
        }
    }

    /**
      *
      * Overloaded to correct the problem with a menu that is off the screen
      *
      */
    public void setLocation (int x, int y){ 
        Point p = new Point(x,y);
        adjustPopupPositionToFit(null, p); 
        super.setLocation(p.x, p.y); 
    }

        /** Overloaded to correct the problem with a menu that is off screen. */
    public void show (Component _invoker, int _x, int _y) {
        Point p = new Point(_x , _y ); 
        adjustPopupPositionToFit(_invoker, p);
 
        super.show(_invoker, _x, _y);
    }

}
