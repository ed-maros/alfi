package edu.caltech.ligo.alfi.common;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Color;

import javax.swing.border.Border;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

/**    
  *
  * <pre>
  * Default values for Swing componenets.
  *
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class GuiConstants{
	
    /**
      * The Color for Selected text.
      *
      * @see java.awt.Color#black
      */
	public static final Color SELECTED_TEXT_COLOR = Color.black;
    /**
      * The Color for Root Nodes (blue).
      *
      * @see java.awt.Color
      */
        public static final Color ROOT_NODE_COLOR = Color.blue;
    /**
      * The Color for Local Unmodified Node (red).
      *
      * @see java.awt.Color
      */
	public static final Color LOCAL_UNMODIFIED_COLOR = Color.red;
    /**
      * The Color for Local Modified Node (gray).
      *
      * @see java.awt.Color
      */
	public static final Color LOCAL_MODIFIED_COLOR = Color.gray;
    /**
      * The Color for Inherited Unmodified Node (red).
      *
      * @see java.awt.Color
      */
	public static final Color INHERITED_UNMODIFIED_COLOR = Color.pink;
    /**
      * The Color for Inherited Modified Node (gray).
      *
      * @see java.awt.Color
      */
	public static final Color INHERITED_MODIFIED_COLOR = Color.lightGray;
    /**
      * The Color for Selection (yellow).
      *
      * @see java.awt.Color
      */
	public static final Color SELECTION_COLOR = new Color(252, 250, 204);
    /**
      * The Color for a Progress Bar (blue).
      *
      * @see java.awt.Color
      */
	public static final Color PROGRESS_BAR_COLOR = new Color(26, 22, 104);
    /**
      * The Color for highlighting
      *
      * @see java.awt.Color
      */
	public static final Color HIGHLIGHT_COLOR = new Color(240,240,240);
    /**
      * The Color for edtiable are backgrounds.
      *
      * @see java.awt.Color#white
      */
	public static final Color EDITABLE_BACKGROUND = Color.white;
    /**
      * The Color for edtiable text.
      *
      * @see java.awt.Color#black
	  */
	public static final Color EDITABLE_TEXT = Color.black;
    /**
      * The Color for text.
      *
      * @see java.awt.Color#black
	  */
	public static final Color TEXT = Color.black;
    /**
      * The Color for a backgroud for text.
      *
      * @see java.awt.Color#white
	  */
	public static final Color TEXT_BACKGROUND= Color.white;

    /**
      * The Color for disabled text.
      *
      * @see java.awt.Color#gray
	  */
	public static final Color DISABLED_TEXT = Color.gray;

    /**
      * The background Color for modified cells.
      *
      * @see java.awt.Color#white
	  */
	public static final Color MODIFIED_VALUE = Color.white;

    /**
      * The background Color for default value cells.
      *
      * @see java.awt.Color#lightGray
	  */
	public static final Color DEFAULT_VALUE = Color.lightGray;

    /**
      * The background Color for default value cells.
      *
      * @see java.awt.Color#white
	  */
	public static final Color DEFAULT_COMMENT_VALUE = Color.blue;

    /**
      * The background Color for selected cells
      *
      * @see java.awt.Color#blue
	  */
	public static final Color SELECTED_BACKGROUND = Color.gray;
    /**
      * The foreground Color for focused cells
      *
      * @see java.awt.Color#white
	  */
	public static final Color SELECTED_DEFAULT_FOREGROUND = Color.white;
	public static final Color SELECTED_MODIFIED_FOREGROUND = Color.yellow;

    /**
      * The Font for nodes.  JGo uses FaceNames
      *
	  */
	public static final String NODE_TEXT_FACE_NAME = new String("Courier");
    /**
      * The Font for text. Dialog 12 point.
      *
      * @see java.awt.Font 
	  */
	public static final Font TEXT_FONT = new Font("Dialog", 0, 12);
    /**
      * The Font for label text. Dialog 12 point bold.
      *
      * @see java.awt.Font 
	  */
	public static final Font LABEL_FONT = new Font("Dialog", Font.BOLD, 12);
    /**
      * The Font for button text. Dialog 12 point bold.
      *
      * @see java.awt.Font 
	  */
	public static final Font BUTTON_FONT = new Font("Dialog", Font.PLAIN, 12);
    /**
      * The deminsion of a button. 80x30.
      *
      * @see java.awt.Dimension
	  */
	public static final Dimension BUTTON_SIZE = new Dimension(80, 35);
    /**
      * The max deminsion of a cell. 300x20.
      *
      * @see java.awt.Dimension
	  */
	public static final Dimension MAX_CELL_SIZE = new Dimension(300, 20);

    /**
      * The deafult window size. 740 x 680.
      *
      * @see java.awt.Dimension
	  */
	public static final Dimension DEFAULT_WINDOW_SIZE = new Dimension(740,680);
    /**
      * The deafult toolbar size. 740 x 35.
      *
      * @see java.awt.Dimension
	  */
	public static final Dimension DEFAULT_TOOLBAR_SIZE = new Dimension(740,35);
    /**
      * The deafult toolbar icon size. 32 x 32.
      *
      * @see java.awt.Dimension
	  */
	public static final Dimension DEFAULT_TOOLBAR_ICON_SIZE =  
        new Dimension(32,32);

    /**
      * The type of lowerd border.
      *
      * @see javax.swing.border.SoftBevelBorder
	  */
	public static final Border LOWERED_BORDER = 
        new SoftBevelBorder(BevelBorder.LOWERED);

    /**
      * The type of raised border.
      *
      * @see javax.swing.border.SoftBevelBorder
	  */
	public static final Border RAISED_BORDER =
        new SoftBevelBorder(BevelBorder.RAISED);

    /**
      * The max size of a status message.
      */
	public static final int MAXIMUM_STATUS_WIDTH = 500;

}
