package edu.caltech.ligo.alfi.common;

import java.awt.Graphics;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.BoxLayout;

/**    
  * <pre>
  * A utility for rendering status at the bottom of the window.
  * For Alfi, this means that when the mouse goes over a node
  * the name and the type of the node is displayed her.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class StatusBar extends JPanel {

	private JLabel m_status_label;
	private String m_default_status;


	public StatusBar (String _default_status){
		super(true);
		init(_default_status, GuiConstants.MAXIMUM_STATUS_WIDTH );
	}

	public StatusBar (String _default_status, int _status_text_width){
		super(true);
		init(_default_status, _status_text_width);
	}

	public void init (String _default_status, int _status_text_width){
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		Dimension dim = new Dimension(_status_text_width, 18);

		m_status_label = new JLabel();
		m_status_label.setBackground(getBackground());
		m_status_label.setOpaque(true);
		m_status_label.setPreferredSize(dim);
		m_status_label.setForeground(GuiConstants.TEXT);
		setStatusText (_default_status);
		setDefaultStatus (_default_status);

		add(m_status_label);

		setBorder(GuiConstants.LOWERED_BORDER);
	}
 
	public void setStatusText (String _status){

		m_status_label.setText(_status);
	}

	/**
      * Return the text which appears in the status bar. 
      *
      * @return String - the text  
      */
	public String getStatusText (){
		return m_status_label.getText();
	}

	/**
	  * Display the default message in the status bar.
      *
      * @return void   
      */
	public void setDefaultStatusText (){

		m_status_label.setText(m_default_status);
		m_status_label.validate();
	}

	/**
      * Return the text that is displayed in the status bar as a default
  	  * whenever setDefaultStatusText() is called. 
      * 
      * @return String    
      */
	public String getDefaultStatus (){
		return m_default_status;
	}

	/**
      * Set a default text message that will be displayed in the status 
      * bar whenever setDefaultStatusText() is called. 
      *
      * @param String _status    
      */
	public void setDefaultStatus (String _status){
		m_default_status = _status;
	}

}
