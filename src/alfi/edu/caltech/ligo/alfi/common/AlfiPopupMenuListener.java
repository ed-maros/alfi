package edu.caltech.ligo.alfi.common;

import java.awt.Container;
import java.awt.event.WindowEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.MenuSelectionManager;
import javax.swing.JPopupMenu;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.common.AlfiFrame;
import edu.caltech.ligo.alfi.editor.ALFIView;
import edu.caltech.ligo.alfi.editor.EditorFrame;
import edu.caltech.ligo.alfi.summary.AlfiMainFrame;

/**    
  * <pre>
  * The Popup Menu Listener for the application.  Makes sure that there
  * is only one instance of any type of popup menu around.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class AlfiPopupMenuListener 
                            implements PopupMenuListener,   
                                       MenuDragMouseListener { 

    private JPopupMenu m_active_popup;
    private boolean m_is_menu_active;

    /////////////////////////////////////////////////
    // PopupMenuListener methods
    /////////////////////////////////////////////////
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) { 
        m_active_popup = (JPopupMenu) e.getSource();
        m_is_menu_active = true;
   } 

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) { 
        m_active_popup = null;
        m_is_menu_active = false;
    } 

    public void popupMenuCanceled(PopupMenuEvent e) { 
        m_active_popup = null;
        m_is_menu_active = false;
    } 

    /////////////////////////////////////////////////
    // MenuDragMouseListener methods
    /////////////////////////////////////////////////

    public void menuDragMouseEntered (MenuDragMouseEvent e) { 
        m_is_menu_active = true;
    } 

    public void menuDragMouseExited (MenuDragMouseEvent e) { 
        m_is_menu_active = true;
    } 

    public void menuDragMouseDragged (MenuDragMouseEvent e) { 
        m_is_menu_active = true;
    } 

    public void menuDragMouseReleased (MenuDragMouseEvent e) { 
        m_is_menu_active = false;
    } 

    public void clearActivePopup (AlfiFrame _deactivated_frame) {
        if (m_active_popup == null) { return; }

        if (!m_is_menu_active) {
            MenuSelectionManager.defaultManager().clearSelectedPath();
        } else {
            if (getPopupFrame() == _deactivated_frame) {
                MenuSelectionManager.defaultManager().clearSelectedPath();
            }
        }

        
        
    }
    
    public JPopupMenu getActivePopup () {
        return m_active_popup;
    }
    
    public EditorFrame getPopupFrame () {
        if (m_active_popup == null) { return null; }
        ALFIView view = (ALFIView) m_active_popup.getInvoker();

        return view.getOwnerFrame();
        
    }
}

