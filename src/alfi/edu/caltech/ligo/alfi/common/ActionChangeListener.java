package edu.caltech.ligo.alfi.common;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**    
  * <pre>
  * Utility class for property change events with a button.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class ActionChangeListener implements PropertyChangeListener{
    AbstractButton item;   

    /**
      *
      * Constructor takes any AbstractButton.
      *
      * @see javax.swing.AbstractButton
      */
    public ActionChangeListener (AbstractButton _item){
        item = _item;
    }

    /**
      * The property change event fired by a BasicAction.
      *
      *
      * @see java.beans.PropertyChangeEvent
      * @see edu.caltech.ligo.alfi.common.BasicAction
      */
    public void propertyChange (PropertyChangeEvent e){
        String propertyName = e.getPropertyName();

        if(propertyName.equals(Action.NAME)){
            item.setText((String) e.getNewValue());
        }
        else if(propertyName.equals(BasicMenuIF.ENABLED)){
            item.setEnabled(((Boolean)e.getNewValue()).booleanValue());
        }
        else if(propertyName.equals(Action.SMALL_ICON)){
            item.setIcon((Icon) e.getNewValue());
            item.invalidate();
            item.repaint();
        }
        else if(propertyName.equals(BasicActionIF.SELECTED)){
            item.setSelected(((Boolean) e.getNewValue()).booleanValue());
        }
    }
}
