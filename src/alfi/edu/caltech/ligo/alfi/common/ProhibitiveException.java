package edu.caltech.ligo.alfi.common;

import edu.caltech.ligo.alfi.tools.AlfiException;

/**    
  * <pre>
  * A utility exception for rejecting continuation of processing.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class ProhibitiveException extends AlfiException {

	public ProhibitiveException(String msg){
		super(msg, ALFI_ERROR);
	}
}
