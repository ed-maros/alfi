package edu.caltech.ligo.alfi.common;

import java.io.*;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.file.*;


/**    
  * <pre>
  * The Editor Frames' popup menu to create box files
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class BoxFilesPopupMenu extends JPopupMenu { 

    private final String HEADING = new String("Box Files");
    private final String CWD_STRING = new String("Working Directory");
    private final String INCLUDE_LINE_PATH_START = 
        ALFIFile.E2E_PARTIAL_PATH_IDENTIFIER;

    private AlfiMainFrame m_parent_frame;
    private EditorFrame m_active_editor;
    private BoxFilenameFilter m_box_file_filter = new BoxFilenameFilter();
 
    /**
      * Constructor for the box files popup menu
      * @param AlfiMainFrame _parent_frame - The main frame of the application
      * @param String _current_directory - the canonical file name path of
      * this popup's editor frame
      */
    public BoxFilesPopupMenu (AlfiMainFrame _parent_frame, 
                              String _current_directory) {

        ArrayList directory_list = new ArrayList();
        m_parent_frame = _parent_frame;                    
        m_active_editor = null;
        JMenuItem title = new JMenuItem(HEADING);
        title.addMenuDragMouseListener(Alfi.getPopupListener());
        add(title);
        addSeparator();
        //
        // Get the E2E_PATH
        //
        StringTokenizer tokenizer =
            new StringTokenizer(Alfi.getE2EPath(), File.pathSeparator);
        //
        // Save the directory list (except for the current directory) 
        // in an ArrayList.  Also ignore the ALFI_BACKUP dirstory.
        //
        while (tokenizer.hasMoreTokens()) {
            String primitive_directory = tokenizer.nextToken();
            File directory = new File(primitive_directory);
            try {
                String full_path = directory.getCanonicalPath();
                if (! full_path.equals(_current_directory)) {
                    directory_list.add(primitive_directory);
                }
            }
            catch (IOException ioe) {
                System.err.println("Failed to get the canonical path for "+
                    primitive_directory);
            }
        }
        //
        // Compose the current directory's menu
        // 
        createMenu(new File(_current_directory), null, null);
        addSeparator();
        //
        // Compose the rest of the menus for the other directories
        // in the E2E_PATH
        // 
        createLibraryMenus(directory_list);
        //
        // Remove any menus which do not have any menu items
        // in them. 
        //
        // For now just display all the directories.  Uncomment
        // code if someone asks for the feature.
        //
        //removeEmptyMenus(null);
        addPopupMenuListener(Alfi.getPopupListener());
    }

    public void cleanUp () {
         removePopupMenuListener(Alfi.getPopupListener());
         MenuSelectionManager.defaultManager().clearSelectedPath();
    }

    /**
      * Always create a menu containing the box files and subdirectories
      * from the "Working Directory" (the directory where the application
      * was started.)  Also exclude any relative paths.
      */
    private void createLibraryMenus (ArrayList _directory_list) {
        // 
        // First create a menu containing the working directory files
        //
        try {
            String full_path = Alfi.WORKING_DIRECTORY.getCanonicalPath();

            createSubMenu(full_path, null, 
                INCLUDE_LINE_PATH_START, CWD_STRING);
        } catch (IOException ioe) {
            System.err.println("Failed to get the canonical path for "+
                Alfi.WORKING_DIRECTORY.getPath());
        }

        ListIterator i = _directory_list.listIterator();
        while (i.hasNext()) {
            String list_entry = (String) i.next(); 
            String full_path = list_entry;

            File directory = new File(list_entry);
            try {
                if (directory.isAbsolute()) {
                    full_path = directory.getCanonicalPath();
                    createSubMenu(full_path, null, INCLUDE_LINE_PATH_START,
                        list_entry);
                }

            } catch (IOException ioe) {
                System.err.println("Failed to get the canonical path for "+
                    directory);
            }
        }
    }
    
    private void removeEmptyMenus (MenuElement _submenu) {
        
        MenuElement[] menu_elements = getSubElements();

        if (_submenu != null) {
            menu_elements = _submenu.getSubElements();
        }

        if (menu_elements == null) { 
            return; 
        }

        for (int ii = 0; ii < menu_elements.length; ii++) {
            if (menu_elements[ii] instanceof JPopupMenu) {
                JPopupMenu menu = (JPopupMenu) menu_elements[ii];
                removeEmptyMenus(menu);
                if (menu.getSubElements().length == 0) {
                    remove(menu);
                }
            } 
            else  if (menu_elements[ii] instanceof JMenu) {
                JMenu menu = (JMenu) menu_elements[ii];
                removeEmptyMenus(menu);

                if (menu.getSubElements().length == 0) {
                    if (_submenu == null) {
                        remove(menu);
                    } else {
                        menu.getParent().remove(menu);
                    }
                } else {
                    if (menu.getItemCount() == 0) {
                        if (_submenu == null) {
                            remove(menu);
                        } else {
                            menu.getParent().remove(menu);
                        }
                    }
                }
            }
        }
    }
    //
    // The actions created below need to provide the "include line
    // path" which the container node would use in the #include
    // statement when it is written/read to/from file.  this will be
    // the path from the directory the container box file sits in,
    // i.e., _current_directory.  in the case of box files found in
    // _current_directory, this include line path will merely be the file
    // name, e.g., fiddle.box  in the case where the popup menu has
    // cascading menus down into subdirectories of _current_directory which
    // contain box files, the include line path will be something
    // like Mech/fiddle.box (assuming Mech is a subdirectory of
    // _current_directory.  in the case where a box is being included from
    // outside _current_directory or its subdirectories, the include line
    // path must start with a "*" to signify it as a box
    // found somewhere in the user's E2E_PATH (e.g, an E2E_PATH
    // entry of /e2e/boxes would allow the user to include the box
    // /e2e/boxes/misc/useful.box, and its include line path would
    // be *misc/useful.box).  this include line path, whatever the
    // form, needs to be passed to the eventual call to
    // EditorFrame.addMemberNode().
    //

    private void createMenu (File _directory, JMenu _submenu, 
        String _include_line_path) {
        //
        // Get the list of filenames
        //
        String[] file_names = _directory.list(m_box_file_filter);
        SortedMap sorted_boxfiles = new TreeMap();
        // 
        // just return if the directory doesn't exist
        //
        if (file_names == null) { return; }

        for (int ii = 0; ii < file_names.length; ii++) {    
            String file_name = new String(file_names[ii]);
            //
            // in the case of files in _directory, the include line path is
            // just the file name
            //
            String full_path = file_name;
            try {
                full_path = _directory.getCanonicalPath();
            } catch (IOException ioe) {
                System.err.println("Failed to get the canonical path for "+
                full_path);    
            }

            String full_path_name =
                         new String(full_path + File.separator + file_name);
            String key = full_path_name.toLowerCase();
            sorted_boxfiles.put(key, full_path_name);
        }
    

        Iterator i = sorted_boxfiles.keySet().iterator();
    
        while (i.hasNext())
        {
            String key = (String) i.next();
            String full_path_name = (String) sorted_boxfiles.get(key);
            File file = new File(full_path_name);
            String file_name = file.getName();
            String include_line_path = 
                composeIncludeLinePath(_include_line_path, file_name);


            if (file.isDirectory()) {
                if (! file.equals(Alfi.LOCAL_UTILITY_DIRECTORY)) {
                    createSubMenu(full_path_name, _submenu, include_line_path);
                }
            }
            else {
                String node_name = file_name.substring(0,
                    file_name.length() - 
                    ALFIFile.BOX_NODE_FILE_SUFFIX.length());

                AddBoxFileAction action = new AddBoxFileAction(node_name,
                                         full_path_name, include_line_path);
                if (_submenu == null) {
                    JMenuItem menu_item = add(action);
                    menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
                }
                else {
                    JMenuItem menu_item = _submenu.add(action);
                    menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
                }
            }
        }
    }

    private void createSubMenu (String _directory_name, JMenu _submenu, 
        String _include_line_path) {

        createSubMenu(_directory_name, _submenu, _include_line_path, null);
    }

    private void createSubMenu (String _directory_name, JMenu _submenu, 
        String _include_line_path, String _display_string) {

        String label;
        if (_display_string != null) {
            label = new String(_display_string);
        } else {
            File directory = new File(_directory_name);
            label = new String(directory.getName());
        }

        CreateSubMenuAction menu_listener = 
            new CreateSubMenuAction(label, _directory_name, _include_line_path);
         
        WrappingMenu submenu = new WrappingMenu(label);
        submenu.addMenuListener(menu_listener);

        if (_submenu == null) {
            JMenuItem menu_item = add(submenu);
            menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
        } else {
            JMenuItem menu_item = _submenu.add(submenu);
            menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
        }

        //createMenu(_directory, submenu, _include_line_path); 
    }

    private String composeIncludeLinePath (String _old_include_line_path,
        String _to_be_added) {

        String include_line_path;

        if (_old_include_line_path == null) {
            include_line_path = new String(_to_be_added);
        } else if (_old_include_line_path.equals(INCLUDE_LINE_PATH_START)){
            include_line_path = new String(_old_include_line_path.concat(
                _to_be_added));
        } else {
            include_line_path = new String(_old_include_line_path.concat(
                File.separator + _to_be_added));
        }
        return include_line_path;
    }

    private void addBoxFile (String _full_path, String _box_name,
                                                String _include_line_path) {
        File file = new File(_full_path);
        ALFINode box_node = null;

            // find or load the box associated with this path
        File[] files = { file };
        ALFINode[] loaded_nodes =
                       Alfi.getMainWindow().load_and_show__engine(files, null);

            // did we get what we wanted?
        if ((loaded_nodes != null) && (loaded_nodes.length == 1) &&
                                                   (loaded_nodes[0] != null)) {
            box_node = loaded_nodes[0];
        }
        else { return; }    // problem.  was reported by load_and_show__engine

        String node_name; 
        ALFINode node_in_edit = m_active_editor.getNodeInEdit();
        if (!node_in_edit.isRootNode()) {
            node_name = node_in_edit.baseNode_getRoot().determineLocalName();
        }
        else { node_name = node_in_edit.determineLocalName(); }

        if (node_name.equals(_box_name)) {
            String warning = "Boxes cannot add instances of themselves.  An " +
                "endless inclusion loop would result.";
            Alfi.warn(warning, false);
            return;
        }

        String new_name = node_in_edit.
            generateUniqueLocalMemberNodeName(_box_name);

        if (m_active_editor != null) {
            m_active_editor.addMemberNode(new_name, box_node,
                                                           _include_line_path);
        }
    }

    private void cleanUpMenuElements (JMenu _submenu) {

        MenuElement[] element = _submenu.getSubElements();
        
        for (int ii = 0; ii < element.length; ii++) {
            if (element[ii] instanceof JMenuItem) {
                JMenuItem menu_item = (JMenuItem) element[ii];
                menu_item.removeMenuDragMouseListener(Alfi.getPopupListener());
            } else if (element[ii] instanceof JMenu) {
                JMenu menu = (JMenu) element[ii];
            }
        }
        _submenu.removeAll();
    } 

    private void adjustPopupPositionToFit(Component _c, Point _p) { 
        int scrWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int scrHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        int oldX = _p.x; 
        int oldY = _p.y;
        Dimension d = getPreferredSize();
        Point cp = new Point(_p.x, _p.y);

        // Divide the popup menu into multiple columns if it gets too long
        // for the screen
        if (d.height > scrHeight) {
            int num_of_columns = d.height/scrHeight + 1;
            setLayout(new GridLayout(0, num_of_columns));
            // get the size after the layout change.
            d = getSize();
        }

        //Translate point to Screen Coordinates 
        if( _c != null) {
            SwingUtilities.convertPointToScreen(cp, _c);
        }
        _p.x = cp.x; 
        _p.y = cp.y;

        if ( (_p.x + d.width) > scrWidth ) {
            _p.x = scrWidth - d.width;
        }

        if ( (_p.y + d.height) > scrHeight) {
            _p.y = scrHeight - d.height;
        }

        if ( _c != null) {
            SwingUtilities.convertPointFromScreen(_p, _c);
        }
    }

    public void setLocation (int _x, int _y) { 
        Point p = new Point(_x,_y);
        adjustPopupPositionToFit(null, p); 
        super.setLocation(p.x, p.y); 
    }

    public void show (ALFIView _invoker, int _x, int _y) {
        m_active_editor = _invoker.getOwnerFrame();
        Point p = new Point(_x , _y); 
        adjustPopupPositionToFit(_invoker, p);
        super.show(_invoker, p.x, p.y); 
    } 

    class BoxFilenameFilter implements FilenameFilter {
        public boolean accept (File _file, String _name) {
            if (_name.endsWith(ALFIFile.BOX_NODE_FILE_SUFFIX)) {
                return true;
            } else {
                try {
                    File file = new File(new String(_file.getCanonicalPath() +
                        File.separator + _name));
                    if (file.isDirectory() && !file.isHidden()) {
                        return true;
                    }
                } catch (IOException e) { System.err.println(e); }
            }
            return false;
        }
    }

    class AddBoxFileAction extends AbstractAction {
        
        private String m_box_name;
        private String m_full_path;
        private String m_include_line_path;

        AddBoxFileAction (String _name, String _full_path,
                                        String _include_line_path) {
            super(_name);
            m_box_name = _name;
            m_full_path = _full_path; 
            m_include_line_path = _include_line_path;
        }

        public void actionPerformed (ActionEvent _event) {
            addBoxFile(m_full_path, m_box_name, m_include_line_path);
        }
    }

    class CreateSubMenuAction implements MenuListener {
        
        private String m_menu_label;
        private String m_full_path;
        private String m_include_line_path;

        CreateSubMenuAction (String _label, String _full_path,
                             String _include_line_path) {
            m_menu_label = _label;
            m_full_path = _full_path; 
            m_include_line_path = _include_line_path;
        }

        public void menuCanceled (MenuEvent e) { 
            JMenu submenu = (JMenu) e.getSource();
            cleanUpMenuElements(submenu);
        } 
    
        public void menuDeselected (MenuEvent e) { 
            JMenu submenu = (JMenu) e.getSource();
            cleanUpMenuElements(submenu);
        } 
    
        public void menuSelected (MenuEvent e) { 
            JMenu submenu = (JMenu) e.getSource();
            createMenu(new File(m_full_path), submenu, m_include_line_path);

        } 

    }

    class WrappingMenu extends JMenu
    {
        /** Ctor that does nothing **/
        public WrappingMenu () {
            super();
        }

        /** Ctor that sets the text for this menu item **/
        public WrappingMenu (String text) {
            super(text);
        }

        public void setPopupMenuVisible(boolean visible)
        {
            JPopupMenu c = getPopupMenu();

            if (visible)
            {
                // check if too tall for the screen
                int screenHeight = 
                (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();

                Dimension dim= c.getPreferredSize();

                if (dim.height > screenHeight)
                {
                    int noOfColumns= dim.height/screenHeight + 1;
                    c.setLayout(new GridLayout(0, noOfColumns));
                }
            }

            super.setPopupMenuVisible(visible);
        }
    }

}
