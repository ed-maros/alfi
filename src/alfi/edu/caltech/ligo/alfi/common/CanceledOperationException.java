package edu.caltech.ligo.alfi.common;

import edu.caltech.ligo.alfi.tools.AlfiException;

/**    
  * <pre>
  * An event that is fired when an operation is canceled.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class CanceledOperationException extends AlfiException {

	public CanceledOperationException(String msg){
		super(msg, ALFI_INFO);
	}
}	
