package edu.caltech.ligo.alfi.common;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.editor.*;

/**    
  * <pre>
  * The Editor Frames' popup menu to create primitives
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class PrimitivesPopupMenu extends JPopupMenu  {

    private final String HEADING = new String ("Primitives");
    protected HashMap m_menu_map = new HashMap();
    protected SortedMap m_sorted_primitives = new TreeMap();
    private AlfiMainFrame m_parent_frame;
    private EditorFrame m_active_editor;
 
    public PrimitivesPopupMenu (ALFINode[] _primitives, 
                 PrimitiveTemplate[] _templates, AlfiMainFrame _parent_frame) {

        m_parent_frame = _parent_frame;                    
        m_active_editor = null;
        setLightWeightPopupEnabled(false); 
        requestFocus(true);

        prepareSortedList(_primitives);

        JMenuItem title = new JMenuItem(HEADING);
        title.addMenuDragMouseListener(Alfi.getPopupListener());
        add(title);
        addSeparator();

        Iterator i = m_sorted_primitives.keySet().iterator();
        while (i.hasNext())
        {
            String key_name = (String) i.next();
            ALFINode node = (ALFINode) m_sorted_primitives.get(key_name);

            if (node != null) {
                String group = node.getPrimitiveGroup();

                AddPrimitiveAction action =
                       new AddPrimitiveAction(node.determineLocalName(), node);

                if (group != null) {
                    if (!m_menu_map.containsKey(group)) {
                        JMenu menu = new JMenu(group);
                        m_menu_map.put(group, menu);
                        JMenuItem menu_item = add(menu);
                        menu_item.addMenuDragMouseListener(
                                                      Alfi.getPopupListener());
                    }

                    JMenu menu = (JMenu) m_menu_map.get(group);
                    JMenuItem menu_item = menu.add(action);
                    menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
                }
                else {
                    JMenuItem menu_item = add(action);
                    menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
                }
            }
        }

        for (int j = 0; j < _templates.length; j++) {
            String base_name = _templates[j].getBaseName();
            String group = _templates[j].getGroup();

            AddTemplateAction action = new AddTemplateAction(_templates[j]);

            if (group != null) {
                if (! m_menu_map.containsKey(group)) {
                    JMenu menu = new JMenu(group);
                    m_menu_map.put(group, menu);
                    JMenuItem menu_item = add(menu);
                    menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
                }

                JMenu menu = (JMenu) m_menu_map.get(group);
                JMenuItem menu_item = menu.add(action);
                menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
            }
            else {
                JMenuItem menu_item = add(action);
                menu_item.addMenuDragMouseListener(Alfi.getPopupListener());
            }
        }

        addPopupMenuListener(Alfi.getPopupListener());
    }

    private void prepareSortedList( ALFINode[] _primitives ) {
        for (int ii = 0; ii < _primitives.length; ii++) {    
            ALFINode node = _primitives[ii];

            if (node != null) {
                String key;
                String group =  node.getPrimitiveGroup();
                String name = node.determineLocalName();
                
                if (group != null)
                    key = group.toLowerCase() + "." + name.toLowerCase();
                else
                    key = name.toLowerCase();

                m_sorted_primitives.put(key, node);
            }
        }
    }

    public void cleanupMenu () {
        removePopupMenuListener(Alfi.getPopupListener());
        MenuSelectionManager.defaultManager().clearSelectedPath();
    }

    public String[] getGroupNames () {
        ArrayList group_list = new ArrayList();
        Set key_set = m_menu_map.keySet();
        Iterator iter = key_set.iterator();

        while (iter.hasNext())
        {
            Object o = iter.next();
            group_list.add(o.toString());
        }
        String[] groups = new String[group_list.size()];
        group_list.toArray(groups);
        return groups;
    }

    class AddPrimitiveAction extends AbstractAction {
        
        private String m_primitive_name;
        private ALFINode m_primitive_node;

        AddPrimitiveAction (String _name, ALFINode _node) {
            super(_name);
            m_primitive_name = _name;
            m_primitive_node = _node;
        }

        public void actionPerformed(ActionEvent _event) {
            String new_name = m_active_editor.getNodeInEdit().
                           generateUniqueLocalMemberNodeName(m_primitive_name);

            if (m_active_editor != null) {
                m_active_editor.addMemberNode(new_name, m_primitive_node, null);
            }
        }
    }

    class AddTemplateAction extends AbstractAction {
        PrimitiveTemplate m_template;

        AddTemplateAction(PrimitiveTemplate _template) {
            super("<" + _template.getBaseName() + ">");
            m_template = _template;
        }

        public void actionPerformed(ActionEvent _event) {
            if (m_active_editor != null) {
                m_active_editor.generatePrimitiveFromTemplate(m_template);
            }
        }
    }

    private void adjustPopupPositionToFit (Component c, Point p) { 
        int scrWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int scrHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        int oldX = p.x; 
        int oldY = p.y;
        Dimension d = getPreferredSize();
        Point cp = new Point(p.x, p.y);

        //Translate point to Screen Coordinates 
        if( c != null) {
            SwingUtilities.convertPointToScreen(cp, c);
        }
        p.x = cp.x; 
        p.y = cp.y;

        if ( (p.x + d.width) > scrWidth ) {
            p.x = scrWidth - d.width;
        }

        if ( (p.y + d.height) > scrHeight) {
            p.y = scrHeight - d.height;
        }

        if ( c != null) {
            SwingUtilities.convertPointFromScreen(p, c );
        }
        //
        // No change is made to the desired (X,Y) values, 
        // when the PopupMenu is too
        // tall OR too wide for the screen 
        //
        //if( p.x < 0 ) p.x = oldX; 
        //if( p.y < 0 ) p.y = oldY; 
        //setLocation(p.x, p.y); 
    }

    public void setLocation (int _x, int _y){ 
        Point p = new Point(_x,_y);
        adjustPopupPositionToFit(null, p); 
        super.setLocation(p.x, p.y); 
    }

    public void show(Component _invoker, int _x, int _y) {
        if (_invoker instanceof ALFIView) {
            m_active_editor = ((ALFIView) _invoker).getOwnerFrame();
            Point p = new Point(_x , _y); 
            adjustPopupPositionToFit(_invoker, p);
            super.show(_invoker, p.x, p.y); 
        }
    } 

    /**
     * It's possible to turn off double-buffering for just the repaint 
     * calls invoked directly on the non double buffered component.  
     * This can be done by overriding paintImmediately() (which is called 
     * as a result of repaint) and getting the current RepaintManager and 
     * turning off double buffering in the RepaintManager before calling 
     * super.paintImmediately(g).
     */
    public void paintImmediately(int x,int y,int w, int h) {
        //System.err.println("paintImmediately");
        RepaintManager repaintManager = null;
        boolean save = true;
        if (!isDoubleBuffered()) {
             repaintManager = RepaintManager.currentManager(this);
             save = repaintManager.isDoubleBufferingEnabled();
             repaintManager.setDoubleBufferingEnabled(false);
        }
        super.paintImmediately(x, y, w, h);

        if (repaintManager != null) {
            repaintManager.setDoubleBufferingEnabled(save);
        }
    }


}

