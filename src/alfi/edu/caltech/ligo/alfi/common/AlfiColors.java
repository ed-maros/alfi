package edu.caltech.ligo.alfi.common;

import java.lang.String;
import java.awt.Color;

/**    
  *
  * <pre>
  * Color constants used in ALFI
  *
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class AlfiColors {

    public static final int ALFI_BLACK            = 0;
    public static final int ALFI_BLUE             = 1;
    public static final int ALFI_CYAN             = 2;
    public static final int ALFI_GREEN            = 3;
    public static final int ALFI_LIGHT_GRAY       = 4;
    public static final int ALFI_MAGENTA          = 5;
    public static final int ALFI_ORANGE           = 6;
    public static final int ALFI_PINK             = 7;
    public static final int ALFI_RED              = 8;
    public static final int ALFI_YELLOW           = 9;
    public static final int ALFI_WHITE            = 10;
    public static final int ALFI_DARK_BLUE        = 11;
    public static final int ALFI_DARK_CYAN        = 12;
    public static final int ALFI_DARK_GRAY        = 13;
    public static final int ALFI_DARK_GREEN       = 14;
    public static final int ALFI_DARK_MAGENTA     = 15;
    public static final int ALFI_DARK_OLIVE_GREEN = 16;
    public static final int ALFI_DARK_ORANGE      = 17;
    public static final int ALFI_DARK_RED         = 18;
    public static final int ALFI_DARK_YELLOW      = 19;

    public static final int ALFI_LAST_COLOR       = 20;

    public static final String[] ALFI_COLOR_BY_STRING_ARRAY =
    { "black", "blue", "cyan", "green", "lightgray", "magenta", 
      "orange", "pink", "red", "yellow", "white",
      "darkBlue", "darkCyan", "darkGray", "darkGreen",
      "darkMagenta", "darkOliveGreen", "darkOrange", "darkRed", "darkYellow"
    };

    static final Color[] COLOR_VS_ID = new Color[ALFI_LAST_COLOR];
    {
        COLOR_VS_ID[ALFI_BLACK] = Color.black;
        COLOR_VS_ID[ALFI_BLUE] = new Color(0x87, 0xCE, 0xEB);
        COLOR_VS_ID[ALFI_CYAN] = Color.cyan;
        COLOR_VS_ID[ALFI_GREEN] = Color.green;
        COLOR_VS_ID[ALFI_LIGHT_GRAY] = Color.lightGray;
        COLOR_VS_ID[ALFI_MAGENTA] = Color.magenta;
        COLOR_VS_ID[ALFI_ORANGE] = Color.orange;
        COLOR_VS_ID[ALFI_PINK] = new Color(0xFF, 0xCC, 0xFF);
        COLOR_VS_ID[ALFI_RED] = new Color(0xFF, 0x40, 0x40);
        COLOR_VS_ID[ALFI_YELLOW] = new Color(0xFF, 0xFF, 0xCC);
        COLOR_VS_ID[ALFI_WHITE] = Color.white;
        COLOR_VS_ID[ALFI_DARK_BLUE] = new Color(0x00, 0x00, 0x8B);
        COLOR_VS_ID[ALFI_DARK_CYAN] = new Color(0x00, 0x8B, 0x8B);
        COLOR_VS_ID[ALFI_DARK_GRAY] = Color.darkGray;
        COLOR_VS_ID[ALFI_DARK_GREEN] = new Color(0x00, 0x64, 0x00);
        COLOR_VS_ID[ALFI_DARK_MAGENTA] = new Color(0x8B, 0x00, 0x8B);
        COLOR_VS_ID[ALFI_DARK_OLIVE_GREEN] = new Color(0x55, 0x6B, 0x2F);
        COLOR_VS_ID[ALFI_DARK_ORANGE] = new Color(0xFF, 0x8C, 0x00);
        COLOR_VS_ID[ALFI_DARK_RED] = new Color(0x8B, 0x00, 0x00);
        COLOR_VS_ID[ALFI_DARK_YELLOW] = new Color(0xCD, 0x95, 0x00);
    }

    public static Color getColor (int _id) {
        if  ((_id >= 0) && (_id < ALFI_LAST_COLOR)) {
            return COLOR_VS_ID[_id];
        }
        else {
            return COLOR_VS_ID[ALFI_BLACK];
        }
    }

    public static int getColorId (String _color) {
        for (int i = 0; i < ALFI_COLOR_BY_STRING_ARRAY.length; i++) {
            if (_color.equals(ALFI_COLOR_BY_STRING_ARRAY[i])) {
                return i;
            }
        }
        return ALFI_YELLOW;
    }

    public static String getColorString (int _color_id) {
        if (_color_id >= 0 && _color_id < ALFI_LAST_COLOR) {
            return ALFI_COLOR_BY_STRING_ARRAY[_color_id];
        }
        else {
            return ALFI_COLOR_BY_STRING_ARRAY[ALFI_YELLOW];
        }
        
     }

}
