package edu.caltech.ligo.alfi.common;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JButton;

/**    
  * <pre>
  * Creates a basic javax.swing.Menu with BasicAction and BasicResource support.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.BasicActionIF
  * @see edu.caltech.ligo.alfi.common.BasicResourceBundle
  */
public class BasicMenu extends JMenu implements BasicMenuIF {

    /**
      * default constructor.
      *
      * @see javax.swing.JMenu
      */
	public BasicMenu (){
		super();
	}

    /**
      * set the label associated with this menu.
      *
      * @see javax.swing.JMenu
      */
	public BasicMenu (String _label){
		super(_label);
	}

    /**
      * set the label and tear-off associated with this menu.
      *
	public BasicMenu (String s, boolean b){
		super(s,b);
	}

    /**
      * Adds the passed action to this menu.
      * The action contains all needed inforamtion and binds all necessary 
      * listeners.
      *
      * @param BasicActionIF _action - the action to assign to the menu.
      * @return JCheckBoxMenuItem - the completed CheckBoxMenuItem
      * @see  edu.caltech.ligo.alfi.common.BasicActionIF
      */
	public JCheckBoxMenuItem addCheckBoxMenuItem (BasicActionIF _action) {
		JCheckBoxMenuItem rval = new JCheckBoxMenuItem(_action.getName(), 
            _action.getSelected());

		rval.setActionCommand((String)_action. 
            getValue(BasicActionIF.ACTION_TYPE));

		rval.setHorizontalTextPosition(JButton.RIGHT);
		rval.setVerticalTextPosition(JButton.CENTER);
		rval.setEnabled(_action.isEnabled());
		rval.addActionListener(_action);

		add(rval);
		_action.addPropertyChangeListener(new ActionChangeListener(rval));

		return rval;
	}

    /**
      * Adds the passed action and associated attributes to
      * a standard JMenuItem. All necessary listeners are bound.
      *
      * @param BasicActionIF _action - the action used to enable the JMenuItem.
      * @return JMenuItem - the completed JMenuItem.
      *
      * @see javax.swing.JMenu
      */
	public JMenuItem add (BasicActionIF _action){
		JMenuItem rval;

		rval = new JMenuItem(_action.getName());

		rval.setActionCommand((String) _action.
            getValue(BasicActionIF.ACTION_TYPE));

		rval.setHorizontalTextPosition(JButton.RIGHT);
		rval.setVerticalTextPosition(JButton.CENTER);
		rval.setEnabled(_action.isEnabled());
		rval.addActionListener(_action);
		rval.setAccelerator(_action.getKeyStroke());
		add(rval);
		
		_action.addPropertyChangeListener(new ActionChangeListener (rval));
		return rval;
	}

    /**
      * add the MenuItem specified by the action in the specified position.
      *
      * @see javax.swing.JMenu
      */
	public JMenuItem insert (BasicActionIF _action, int _position){
		JMenuItem rval;

		rval = new JMenuItem(_action.getName());

		rval.setActionCommand((String) _action.
            getValue(BasicActionIF.ACTION_TYPE));

		rval.setHorizontalTextPosition(JButton.RIGHT);
		rval.setVerticalTextPosition(JButton.CENTER);
		rval.setEnabled(_action.isEnabled());
		rval.addActionListener(_action);
		rval.setAccelerator(_action.getKeyStroke());
		insert(rval, _position);
		
		_action.addPropertyChangeListener(new ActionChangeListener(rval));
		return rval;
	}

    /**
      * Sets associated menu icon to the value (true or false) of 
      * passed constraint. 
      *
      */
	public void setMenuItemIconsVisible (Actions _actions, boolean _visible){
		JMenuItem item;
		int numItems, ii;

		if (_visible) {
			for (ii=0, numItems = getItemCount();ii < numItems;ii++){
				item = getItem(ii);
				BasicActionIF a = _actions.getAction(item.getActionCommand());
				if(a!=null){
					item.setIcon(a.getDefaultIcon());
				}
			}
		}
		else {
			for(ii=0, numItems = getItemCount();ii < numItems;ii++){
				item = getItem(ii);
				item.setIcon(null);
			}
		}	
	}
}
