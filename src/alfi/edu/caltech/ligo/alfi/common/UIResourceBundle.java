package edu.caltech.ligo.alfi.common;

import java.util.List;
import java.util.MissingResourceException;

import java.io.File;

import java.net.URL;

import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.KeyStroke;

/**    
  * An abstract implementation of the UIResourceIF interface.
  *
  * This implementation requires the subclass to provide two methods.
  * A constructor and a getContents method that returns the contents 
  * as an Object[][].
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see java.util.ResourceBundle
  */

public abstract class UIResourceBundle extends BasicResourceBundle {

    /**
     * System property to contain the URL base for images
     */

    public static final String IMAGE_BASE = 
        "edu/caltech/ligo/alfi/images";
    public static final String URL_BASE = 
        "alfi.ui.resource.uiresourcebundle.url.base";
    public static final String URL_BASE_DEFAULT = 
        "http://ligo.caltech.edu/~e2e/images";
    public static final String URLBase = 
        System.getProperty(URL_BASE, URL_BASE_DEFAULT); 
    /**
      * This position is reserved for the name of the
      * nmemonic action associated with this resources reference.
      * This should be a Character.
      */ 
    public static final int MNEMONIC = 3;

    /**
      * This position is reserved for the name of the
      * key stroke action associated with this resources reference.
      * This should be a java.swing.KeyStroke
      * @see javax.swing.KeyStroke
      */ 
    public static final int KEY_STROKE = 4;
    
    /**
      * This position is reserved for the name of the
      * primary image resources reference. This should be a String.
      * It can be a url, relative or absolute file path.
      * A "disabled" image can be simply created from this image using
      * GreyFilter.createDisabledImage (resources.loadImage(_key,
      * UIResourceIF.PRIMARY_IMAGE));
      */ 

    public static final int PRIMARY_IMAGE = 5;

    /**
      * This position is reserved for the name of the
      * rollover image resources reference. This should be a String.
      * It can be a url, relative or absolute file path.
      */ 

    public static final int ROLLOVER_IMAGE = 6;

    /**
      * This position is reserved for the name of the
      * pressed or mouse-click image resources reference. 
      * This should be a String.  It can be a url, relative or 
      * absolute file path.
      */ 

    public static final int PRESSED_IMAGE = 7;

    /**
      * default constructor calls super().
      *
      * @see java.util.ResourceBundle
      */
    public UIResourceBundle () {
        super();
    }

    /**
      * attempts to load an image in the resource bundle.
      * If the first attempt to load fails it is assumed to be relative to 
      * JAVA_HOME. If that load fails an exception is thrown.
      *
      * @param String _key -  the resources key.
      * @param int _value - the key to the value 
      *                 (which image in the ResourceBundle).
      * @return the loaded image.
      * @exception MissingResourceException - thrown if unable to resolve the 
      *                 Image.
      */
    public final ImageIcon loadImage (String _key, int _value) 
    throws MissingResourceException {
        try {
            //try the passed one
            String uri = (String)get (_key,_value);
            try {
                    URL imageURL = ClassLoader.
                        getSystemResource(IMAGE_BASE + uri);    
                    //
                    // ClassLoader does not throw exception 
                    // but returns null if resource not found
                    //
                    if (imageURL != null) {
                        return new ImageIcon (imageURL);
                    } else {
                        //remote reference URL form.
                        return new ImageIcon (new URL (URLBase + uri)); 
                    }
            }
            catch (Exception ex_in) {
                //try java home ref first
                uri = URLBase+File.separator+(String)get (_key,_value);
                //remote reference URL form.
                return new ImageIcon (new URL (URLBase + uri)); 
            }
        }
        catch (Exception ex) {
            if (ex instanceof MissingResourceException) {
                throw (MissingResourceException)ex;
            }
            throw new MissingResourceException (ex.getMessage(),
                getClass().getName(),_key+":"+_value);
        }
    }

    /**
      * Accessor for returning the PRIMARY_IMAGE value.
      *  
      * @param _key is the key to the contents of this resource bundle.
      * @return is presumed to be a path to an image.
      * @exception MissingResourcesException is the resource is not available.
      * @exception ClassCastException is the resource is not an Integer.
      */ 
    public ImageIcon getPrimaryImage (String _key)
    throws MissingResourceException, ClassCastException {
        return loadImage(_key, PRIMARY_IMAGE);
    }
 
    /**
      * Accessor for returning the ROLLOVER_IMAGE value.
      *  
      *  
      * @param _key is the key to the contents of this resource bundle.
      * @return is presumed to be a path to an image.
      * @exception MissingResourcesException is the resource is not available.
      * @exception ClassCastException is the resource is not an Integer.
      */ 
    public ImageIcon getRolloverImage (String _key)
    throws MissingResourceException, ClassCastException {
        return loadImage(_key, ROLLOVER_IMAGE);
    }
 
    /**
      * Accessor for returning the PRESSED_IMAGE value.
      *  
      *  
      * @param _key is the key to the contents of this resource bundle.
      * @return is presumed to be a path to an image.
      * @exception MissingResourcesException is the resource is not available.
      * @exception ClassCastException is the resource is not an Integer.
      */ 
    public ImageIcon getPressedImage (String _key)
    throws MissingResourceException, ClassCastException {
        return loadImage(_key, PRESSED_IMAGE);
    }
 
    /**
      * Accessor for returning the KEY_STROKE value.
      *  
      *  
      * @param _key is the key to the contents of this resource bundle.
      * @return is presumed to be a Keystroke for an Accelerator.
      * @exception MissingResourcesException is the resource is not available.
      * @exception ClassCastException is the resource is not an Integer.
      */ 
    public KeyStroke getKeyStroke (String _key)
    throws MissingResourceException, ClassCastException {
        return (KeyStroke)get(_key, KEY_STROKE);
    }
 
    /**
      * Accessor for returning the MNEMONIC value.
      *  
      *  
      * @param _key is the key to the contents of this resource bundle.
      * @return is presumed to be a Character for a Mnemonic.
      * @exception MissingResourcesException is the resource is not available.
      * @exception ClassCastException is the resource is not an Integer.
      */ 
    public char getMnemonic (String _key)
    throws MissingResourceException, ClassCastException {
        Character rval = (Character)get(_key, MNEMONIC);
        return rval.charValue();
    }
}

