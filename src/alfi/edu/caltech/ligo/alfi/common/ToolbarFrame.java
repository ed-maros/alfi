package edu.caltech.ligo.alfi.common;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import java.lang.Boolean;

import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.summary.AlfiMainFrame;
import edu.caltech.ligo.alfi.editor.EditorFrame;


/**    
  *
  * <pre>
  * The frame for the primitives toolbar.
  * </pre>
  *
  * @author  Melody Araya
  * @version %I%, %G%
  */

public class ToolbarFrame extends JFrame {
    public final String TOOLBAR_TITLE = new String("Primitives");

    private int COLUMN_NUMBER = 3;
    private PrimitivesToolbar m_toolbar;
     
    public ToolbarFrame (ALFINode[] _primitives, 
                         AlfiMainFrame _parent_frame) {
        super("Primitives");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                setVisible(false);
            }
        });         
    
        // Create the Primitives Toolbar    
        m_toolbar = new PrimitivesToolbar(_primitives, 
            _parent_frame);
        Dimension button_size = m_toolbar.getButtonSize();

        //Lay out the content pane.
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(m_toolbar, BorderLayout.CENTER);
        setContentPane(contentPane);
        
        int button_count = m_toolbar.getButtonCount();
        int buttons_per_row = button_count;
        if (button_count > COLUMN_NUMBER) {
            buttons_per_row = button_count/COLUMN_NUMBER;
            if ((button_count % COLUMN_NUMBER) > 0) {
                buttons_per_row++;
            }
        }

        int inset_top    = 29;
        int inset_left   = 6;
        int inset_right  = 6;
        int inset_bottom = 6;
        int button_width = 28;
        int button_height = 27;

        if (_parent_frame != null) {
            Insets insets = _parent_frame.getInsets();
            inset_top    = insets.top;
            inset_left   = insets.left;
            inset_right  = insets.right;
            inset_bottom = insets.bottom;
        }

        if (button_size != null) {
            button_width = ( int )button_size.getWidth();
            button_height = ( int )button_size.getHeight();
        }

        int width = inset_left + inset_right + 
                     (buttons_per_row * (button_width + 14));
        int height = inset_top + inset_bottom + 
                    (COLUMN_NUMBER * (button_height + 16));

        setSize(width, height);
    }

    public boolean isToolbarSelected () {
        return m_toolbar.hasButtonSelected();
    }

    public PrimitivesToolbar getToolbar () {
        return m_toolbar;
    }
}
