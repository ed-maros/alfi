package edu.caltech.ligo.alfi.common;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.Locale;

import javax.swing.Icon;

/**    
  * <pre>
  * The top level Actions
  *
  * </pre>
  * This is a Map of BasicActionIF Objects for a windowing component.
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see javax.swing.AbstractAction
  */
public class Actions {
	private HashMap m_actions;
	protected UIResourceBundle m_resources;

    /**
      * Resources for this action to use.
      *
      * @see edu.caltech.ligo.alfi.common.BasicResourceBundle
      */
	public Actions (UIResourceBundle _resources) {
        m_resources = _resources;
		m_actions = new HashMap(10);
		loadActions();
	}

    /**
      * Returns the resource bundle associted with these actions.
      *
      * @see edu.caltech.ligo.alfi.common.BasicResourceBundle
      */
	public UIResourceBundle getResources () {
		return m_resources;
	}

	
    /**
      * Retuns the Map of BasicAction(s) associated with this 
      * Action.
      *
      * @see java.util.Map
      */
	public Map getActionsMap () {
		return m_actions;
	}

	
    /**
      * returns the BasicActionIF associated with
      * the passed type.
      *
      * @param String _type - The action type requested.
      * @return BasicActionIF - the value mapped to the _type.
      *
      * @see edu.caltech.ligo.alfi.common.BasicActionIF
      */
	public BasicActionIF getAction (String _type) {
		return (BasicActionIF) m_actions.get(_type);
	}

    /**
      * returns a map of keys in this map and the associated
      * BasicAction names.
      *
      * @see edu.caltech.ligo.alfi.common.BasicActionIF#getName
      */
	public Map getNames () {
		HashMap rval = new HashMap(m_actions.size());
		Iterator iter = m_actions.entrySet().iterator();
		Map.Entry entry;

		while (iter.hasNext()) {
			entry = (Map.Entry) iter.next();
			try {
				rval.put(entry.getKey(), 
                ((BasicActionIF) entry.getValue()).getName());
			}
			catch (NullPointerException npe) { 
				//thrown by a possibly null value.
			}
		}
		return rval;
	}

	
    /**
      * returns the (possibly null) BasicActionIF name associated 
      * with the passed type.
      *
      * @param String _type - the action type whose name is requested.
      * @return String - the name associated with the actio type.
      * @see edu.caltech.ligo.alfi.common.BasicActionIF#getName
      */
	public String getName (String _type) {
		BasicActionIF action = (BasicActionIF) m_actions.get(_type);
		if (action != null) {
			return action.getName();
		}
		return null;
	}

    /**
      * returns a map of the descriptions associated 
      * with the BasicAction.
      *
      * @see edu.caltech.ligo.alfi.common.BasicActionIF
      */
	public Map getDescriptions (){
		HashMap rval = new HashMap(m_actions.size());
		Iterator iter = m_actions.entrySet().iterator();
		Map.Entry entry;

		while(iter.hasNext()){
			entry = (Map.Entry) iter.next();
			try {
				rval.put(entry.getKey(),
                ((BasicActionIF)entry.getValue()).getLongDescription());
			}
			catch (Exception ex) {
				//null pointer.
			}
		}
		return rval;
	}

    /**
      * Returns the discription associated with the type.
      *
      *
      * @param String _type - The action type to return the description.
      * @return String - the "human readable" description.
      *
      * @see edu.caltech.ligo.alfi.common.BasicActionIF
      */
	public String getDescription (String _type) {
		try {
			return ((BasicActionIF)
                m_actions.get(_type)).getLongDescription();
		}
		catch (Exception ex) {
			//null pointer.
			return AlfiConstants.UNDEFINED;
		}
	}
	
    /**
      * Returns the default icons associted with this Actions.
      *
      * @return Map - the map of the icons.
      * @see edu.caltech.ligo.alfi.common.BasicActionIF
      */
	public Map getDefaultIcons () {
		HashMap rval = new HashMap(m_actions.size());
		Iterator iter = m_actions.entrySet().iterator();
		Map.Entry entry;

		while (iter.hasNext()){
			entry = (Map.Entry) iter.next();
			try {
				rval.put(entry.getKey(), 
                    ((BasicActionIF)entry.getValue()).getDefaultIcon());
			}
			catch (Exception ex) {
				//null pointer.
			}
		}
		return rval;
	}

    /**
      * returns the Icon associated with the passed type.
      *
      *
      * @param String _type - the action type to select.
      * @return Icon - the Icon associated witht the passed _type.
      *
      * @see javax.swing.Icon
      */
	public Icon getDefaultIcon (String _type){
		try {
			return ((BasicActionIF) 
                m_actions.get(_type)).getDefaultIcon();
		}
		catch (Exception ex) {
			//null pointer.
			return null;
		}
	}

    /**
      * Returns the disabled icons associted with this Actions.
      *
      * @return Map - the map of the icons.
      * @see edu.caltech.ligo.alfi.common.BasicActionIF
      */
	public Map getDisabledIcons (){
		HashMap rval = new HashMap(m_actions.size());
		Iterator iter = m_actions.entrySet().iterator();
		Map.Entry entry;

		while (iter.hasNext()){
			entry = (Map.Entry)iter.next();
			try {
				rval.put(entry.getKey(), 
                    ((BasicActionIF) entry.getValue()).getDisabledIcon());
			}
			catch (Exception ex) {
				//null pointer.
			}
		}
		return rval;
	}

    /**
      * returns the Icon associated with the passed type.
      *
      *
      * @param String _type - the action type to select.
      * @return Icon - the Icon associated witht the passed _type.
      *
      * @see javax.swing.Icon
      */
	public Icon getDisabledIcon (String _type){
		try {
			return ((BasicActionIF) 
                m_actions.get(_type)).getDefaultIcon();
		}
		catch (Exception ex) {
			//null pointer.
			return null;
		}
	}

	protected void loadActions (){}

    /**
      * Loads the specified action type into this map of actions.
      * This typically would be called while loading actions.
      *
      * @param String _type - the actions type.
      * @param BasicActionIF _action - the action.
      * @return BasicAction - the possibly null BasicAction 
      * that was associated with the type before.
      * @see edu.caltech.ligo.alfi.common.BasicAction
      */
	public BasicActionIF loadAction (String _type, BasicActionIF _action) {
		return (BasicActionIF) m_actions.put(_type, _action);
	}

    /**
      * loads the actions contained in the passed Actions into this Actions.
      *
      * @param Actions _actions - the actions to load.
      */
	public void loadActions (Actions _actions){
		Map map = _actions.getActionsMap();
		Iterator iter = map.keySet().iterator();
		String key;
		
		while(iter.hasNext()){
			try {
				key = (String) iter.next();
				m_actions.put(key, map.get(key));
			}
			catch (Exception ex) {
				//possible null mapping.
			}
		}
	}	
	
				
}
