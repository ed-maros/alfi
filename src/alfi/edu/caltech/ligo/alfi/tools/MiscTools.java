package edu.caltech.ligo.alfi.tools;

import java.util.*;
import java.io.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.file.*;

    /**
     * This class contains static methods used to do small misc operations.
     */
public class MiscTools {
    public static ArrayList JGoSelectionToList(JGoSelection _selection,
                                               boolean _b_top_level_only) {
        ArrayList list = new ArrayList();
        if (_selection != null) {
            JGoListPosition pos = _selection.getFirstObjectPos();
            while (pos != null) {
                list.add(_selection.getObjectAtPos(pos));
                pos = (_b_top_level_only) ?
                                 _selection.getNextObjectPosAtTop(pos) :
                                 _selection.getNextObjectPos(pos);
            }
        }
        return list;
    }

        /** Keys will be elements of _selection, values will be null. */
    public static HashMap JGoSelectionToMap(JGoSelection _selection,
                                               boolean _b_top_level_only) {
        HashMap map = new HashMap();
        if (_selection != null) {
            JGoListPosition pos = _selection.getFirstObjectPos();
            while (pos != null) {
                map.put(_selection.getObjectAtPos(pos), null);
                pos = (_b_top_level_only) ?
                                 _selection.getNextObjectPosAtTop(pos) :
                                 _selection.getNextObjectPos(pos);
            }
        }
        return map;
    }

    public static void stackTrace(String _message) {
        stackTrace(_message, 2, 6);
    }

    public static void stackTrace(String _message, int _end_depth) {
        stackTrace(_message, 2, _end_depth);
    }
        
    public static void stackTrace(String _message, int _start_depth,
                                                   int _end_depth) {
        try { throw new Exception(); }
        catch(Exception _e) {
            StackTraceElement[] stack_trace_elements = _e.getStackTrace();
            String message = "\n" + _message + "\n";
            for (int i = _start_depth; i < stack_trace_elements.length; i++) {
                String element_string = stack_trace_elements[i].toString();
                if ((i > _end_depth) || element_string.startsWith("java")) {
                    break;
                }
                else if (element_string.startsWith("edu.caltech.ligo.alfi.")) {
                    element_string = element_string.substring(22);
                }

                message += "\tat " + element_string + "\n";
            }
            System.out.println(message);
        }
    }

    public static String treeToString(Hashtable _tree_map) {
        return treeToString(_tree_map, "");
    }

    private static String treeToString(Hashtable _tree_map, String _indent) {
        StringBuffer sb = new StringBuffer();
        final String STANDARD_INDENT = "    ";
        String indent = new String(_indent);

        ArrayList key_list = new ArrayList(_tree_map.keySet());
        Collections.sort(key_list, new Comparator() {
                public int compare(Object _o1, Object _o2) {
                    boolean b_o1_is_node = (_o1 instanceof Hashtable);
                    boolean b_o2_is_node = (_o2 instanceof Hashtable);
                    if (b_o1_is_node && (! b_o2_is_node)) { return 1; }
                    else if ((! b_o1_is_node) && b_o2_is_node) { return -1; }
                    else {
                        return _o1.toString().compareToIgnoreCase(
                                                               _o2.toString());
                    }
                }

                public boolean equals(Object _o) { return (_o == this); }
            });

        String sub_indent = indent + STANDARD_INDENT;
        for (Iterator i = key_list.iterator(); i.hasNext(); ) {
            Object o = i.next();
            sb.append(indent);
            sb.append(o.toString());

            Object value = _tree_map.get(o);
            if (value instanceof Hashtable) {
                sb.append(" --\n");
                sb.append(treeToString((Hashtable) value, sub_indent));
            }
            sb.append("\n");
        }

        return sb.toString();
    }

        /** Finds a file, given a name and a search path. */
    public static File findFile(String _file_name, String _search_path) {
        File file = null;
        if (_file_name.startsWith(File.separator)) {
            file = new File(_file_name);
        }
        else {
            String[] directories = _search_path.split(File.pathSeparator);
            for (int i = 0; i < directories.length; i++) {
                file = new File(directories[i], _file_name);
                if (file.exists()) { break; }
            }
        }
        return (file.exists()) ? file : null;
    }

        /** Returns the lines in a simple text file.  This should only be used
          * for reading relatively smal files.
          */
    public static String[] readSmallTextFile(File _file) {
        ArrayList line_list = new ArrayList();
        BufferedReader reader = null;
        try {
            FileReader r = new FileReader(_file);
            reader = new BufferedReader(r);
        }
        catch(FileNotFoundException _e) { return null; }    // blank

        if (reader != null) {
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    line_list.add(line);
                }
                reader.close();
            }
            catch(IOException _e) {
                System.err.println("Problem reading " + _file + ":\n" +
                    "\t" + line);
            }
        }
        else { return null; }

        String[] lines = new String[line_list.size()];
        line_list.toArray(lines);

        return lines;
    }

    public static boolean writeSmallTextFile(File _file, String _content) {
        try {
            FileWriter writer = new FileWriter(_file);
            writer.write(_content, 0, _content.length());
            writer.flush();
            writer.close();
        }
        catch(IOException _e) {
            System.err.println(
                "MiscTools.writeSmallTextFile():\n" +
                "\tProblem occured when attempting to write " + _file + ".");
            return false;
        }

        return true;
    }

        /** Expects lines in the format:
          *
          *     key1
          *     {
          *     line1 of value1
          *     line2 of value1
          *             .
          *             .
          *             .
          *     }
          *     key2
          *     {
          *     line1 of value2
          *     line2 of value2
          *             .
          *             .
          *             .
          *     }
          *         .
          *         .
          *         .
          *
          * Returns a HashMap of { String, String[] } pairs with the brackets
          * enclosing the value lines stripped away.
          */
    public static HashMap getKeyAndBracketedValuePairs(String[] _lines) {
        final String BEGIN = Parser.SECTION_BEGIN_MARKER;
        final String END = Parser.SECTION_END_MARKER;
        HashMap key_value_map = new HashMap();

        String key = null;
        int depth = 0;
        for (int i = 0; i < _lines.length; i++) {
            if (key == null) {

                    // skip empty lines preceeding a new key
                if (_lines[i].matches("\\s*")) { continue; }

                key = _lines[i++];
                ArrayList value_lines_list = new ArrayList();
                if (key.matches("\\w+") &&
                                    _lines[i++].equals(BEGIN)) {
                    depth = 1;

                    while (true) {
                        if (_lines[i].equals(BEGIN)) { depth++; }
                        else if (_lines[i].equals(END)){ depth--; }
                        if (depth < 1) { break; }

                        value_lines_list.add(_lines[i++]);
                    }

                    String[] value_lines = new String[value_lines_list.size()];
                    value_lines_list.toArray(value_lines);
                    key_value_map.put(key, value_lines);
                    key = null;
                }
                else { return null; }    // format problem
            }
        }

        return key_value_map;
    }

        /** See getKeyAndBracketedValuePairs(String[]) */
    public static HashMap getKeyAndBracketedValuePairs(ArrayList _line_list) {
        String[] lines = new String[_line_list.size()];
        _line_list.toArray(lines);
        return getKeyAndBracketedValuePairs(lines);
    }
}
