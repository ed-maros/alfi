package edu.caltech.ligo.alfi.tools;

import java.util.*;

    /** This class has static methods used to format text for messages. */
public class TextFormatter {
    public static String formatMessage(String _message, String _prefix,
                                                        int _line_length) {
        int length = _line_length - _prefix.length();
        String message = formatMessage(_message, length);
        StringTokenizer st = new StringTokenizer(message, "\n");
        message = new String();
        while (st.hasMoreTokens()) {
            message += _prefix + st.nextToken() + "\n";
        }
        return message;
    }

    public static String formatMessage(String _message) {
        return formatMessage(_message, 60);
    }

    public static String formatMessage(String _message, int _line_length) {
        if (_message.startsWith("<PRE>") && _message.endsWith("</PRE>")) {
            String message = _message.substring(5, _message.length() - 6);
            return message;
        }

        String message = new String();
        StringTokenizer st = new StringTokenizer(_message);
        String line = new String();
        while (st.hasMoreTokens()) {
            String next_word = st.nextToken();
            if (line.length() == 0) {
                line += next_word;
            }
            else if ((line.length() + 1 + next_word.length()) <= _line_length) {
                line += " " + next_word;
            }
            else {
                if (message.length() != 0) { message += "\n"; }
                message += line;
                line = next_word;
            }
        }
        message += "\n" + line;
        return message;
    }

    public static String formatMessageInHTML(String _message, int _line_length){
        String message = formatMessage(_message, _line_length);
        StringTokenizer st = new StringTokenizer(message, "\n");
        message = "<HTML>\n";
        while (st.hasMoreTokens()) {
            message += st.nextToken() + "<BR>\n";
        }
        message += "</HTML>\n";
        return message;
    }
}
