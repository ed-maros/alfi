// For licensing, see: http://helium.knownspace.org/license.html
package org.datamanager.simpleton;

import java.io.IOException;

import java.net.MalformedURLException;
import java.net.URL;

import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;

import org.datamanager.constraint.EventConstraint;
import org.datamanager.constraint.EventTypeIsConstraint;

import org.datamanager.event.DataManagerEvent;
import org.datamanager.event.LaunchBrowserWithURLEvent;

import org.datamanager.exception.DataManagerException;

import org.datamanager.kernel.EventHandler;
import org.datamanager.kernel.Pool;
import org.datamanager.kernel.Simpleton;

import org.datamanager.tools.BrowserLauncher;
import org.datamanager.tools.Debug;
import org.datamanager.tools.kernelproxy.KernelProxy;


/**
 *
 * This class will handle the LaunchBrowserWithURLEvent by 
 * using JNLP to launch the default browser with the URL
 * contained in the Event.
 *
 * @author Chris Dent
 */
public class BrowserLauncherSimpleton extends Simpleton
    implements EventHandler {
    private Pool pool;
    private KernelProxy kernelProxy = KernelProxy.getInstance();
    private BasicService basicJNLPService = getBasicService();

    /**
     * Establishes the JNLP BasicService if JNLP is available.
     */
    private static BasicService getBasicService() {
        try {
            return (BasicService) ServiceManager.lookup(
                           "javax.jnlp.BasicService");
        } catch (UnavailableServiceException exception) {
            return null;
        }
    }

    /**
     * Subscribes to the pool to hear the LaunchBrowserWithURLEvent.
     */
    public void process() {
        try {
            pool = Pool.getDefaultPool();

            EventConstraint eventConstraint = new EventTypeIsConstraint(
                                                      LaunchBrowserWithURLEvent.class);
            pool.subscribe(this, eventConstraint);
        } catch (Exception everything) {
            everything.printStackTrace();
        }
    }

    /**
     * Handles the LaunchBrowserWithURLEvent, calling launchBrowser(URL)
     * with the URL found in the event.
     *
     * @param dataManagerEvent the LaunchBrowserWithURLEvent
     */
    public void handle(DataManagerEvent dataManagerEvent) {
        LaunchBrowserWithURLEvent launchBrowserEvent = 
                (LaunchBrowserWithURLEvent) dataManagerEvent;
        URL url = launchBrowserEvent.getURL();

        launchBrowser(url);
    }

    /**
     * Launches the default browser with the given URL by
     * using JNLP.
     */
    private void launchBrowser(URL url) {
        if ((basicJNLPService != null) && 
                basicJNLPService.isWebBrowserSupported()) {
            System.err.println("Using JNLP for browser launching");
            basicJNLPService.showDocument(url);
        } else {
            System.err.println("Using BrowserLauncher for browser launching");

            try {
                BrowserLauncher.openURL(url.toString());
            } catch (IOException exception) {
                throw new RuntimeException(exception);
            }
        }
    }
}