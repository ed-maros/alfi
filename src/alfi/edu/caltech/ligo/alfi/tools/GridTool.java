package edu.caltech.ligo.alfi.tools;

import java.util.*;
import java.awt.*;

    /**
     * This class contains static methods used to get information about the
     * grid.
     */
public class GridTool {
    public static final int GRID_SPACING = 6;

        /** Finds the nearest grid point. */
    public static Point getNearestGridPoint(Point _raw_point) {
        return getNearestGridPoint(_raw_point, 1);
    }

        /** Finds the nearest grip point which is also an integral number of
          * grid spacings _grid_space_increment along both the x and y axes.
          */
    public static Point getNearestGridPoint(Point _raw_point,
                                                   int _grid_space_increment) {
        return getNearestGridPoint(_raw_point, _grid_space_increment,
                                               _grid_space_increment);
    }

        /** Usually gets nearest point on grid, but this call specifies getting
          * a point on the grid which also satisfies being an integral number of
          * grid spacings along either/both the x or/and y axes.
          *
          * (A call to getNearestGridPoint(Point _raw_point) is just a call to
          * this method as getNearestGridPoint(_raw_point, 1, 1).)
          */
    public static Point getNearestGridPoint(Point _raw_point,
                    int _grid_space_increment_x, int _grid_space_increment_y) {
        int increment_space_x = GRID_SPACING * _grid_space_increment_x;
        int increment_space_y = GRID_SPACING * _grid_space_increment_x;

        Point grid_point = new Point(_raw_point);

        int offset_x = grid_point.x % increment_space_x;
        if (offset_x < (increment_space_x / 2)) {
            grid_point.x -= offset_x;
        }
        else { grid_point.x += (increment_space_x - offset_x); }

        int offset_y = grid_point.y % increment_space_y;
        if (offset_y < (increment_space_y / 2)) {
            grid_point.y -= offset_y;
        }
        else { grid_point.y += (increment_space_y - offset_y); }

        return grid_point;
    }

    public static boolean isGridPoint(Point _test_point) {
        if (            ((_test_point.x % GRID_SPACING) == 0) &&
                        ((_test_point.y % GRID_SPACING) == 0)) {
            return true;
        }
        else { return false; }
    }

    public static Dimension getDimensionBestFitToGrid(Dimension _dim) {
        return getDimensionBestFitToGrid(_dim, 1);
    }

        /** Related funtionality to call
          * getNearestGridPoint(Point _raw_point, int _grid_space_increment).
          */
    public static Dimension getDimensionBestFitToGrid(Dimension _dim,
                                                   int _grid_space_increment) {
        return getDimensionBestFitToGrid(_dim, _grid_space_increment,
                                               _grid_space_increment);
    }

        /** Related funtionality to call
          * getNearestGridPoint(Point _raw_point, int _grid_space_increment_x,
          *                                       int _grid_space_increment_y).
          */
    public static Dimension getDimensionBestFitToGrid(Dimension _dim,
                    int _grid_space_increment_x, int _grid_space_increment_y) {
        Point p_fit = getNearestGridPoint(new Point(_dim.width, _dim.height),
                             _grid_space_increment_x, _grid_space_increment_y);
        return new Dimension(p_fit.x, p_fit.y);
    }
}
