package edu.caltech.ligo.alfi.tools;

import java.io.IOException;

import edu.caltech.ligo.alfi.Alfi;
/**
* A simple, static class to display a URL in the system browser.


*
* Under Unix, the system browser is hard-coded to be 'netscape'.
* Netscape must be in your PATH for this to work.  This has been
* tested with the following platforms: AIX, HP-UX and Solaris.


*
* Under Windows, this will bring up the default browser under windows,
* usually either Netscape or Microsoft IE.  The default browser is
* determined by the OS.  This has been tested under Windows 95/98/NT.


*
* Examples:


    * * BrowserControl.displayURL("http://www.javaworld.com")
      *
    * BrowserControl.displayURL("file://c:\\docs\\index.html")
      *
    * BrowserContorl.displayURL("file:///user/joe/index.html");
      *


* Note - you must include the url type -- either "http://" or
* "file://".
*/
public class BrowserControl
{
    /**
     * Display a file in the system browser.  If you want to display a
     * file, you must include the absolute path name.
     *
     * @param url the file's url (the url must start with either "http://"
or
     * "file://").
     */
    public static void displayURL(String url)
    {
        boolean windows = isWindowsPlatform();
        String cmd = null;
        try
        {
            String default_browser = Alfi.getDefaultBrowser();
            if (default_browser.indexOf(EXPLORER) >= 0)
            {
                // cmd = 'rundll32 url.dll,FileProtocolHandler http://...'
                cmd = default_browser + " " + WIN_FLAG + " " + url;
                Process p = Runtime.getRuntime().exec(cmd);
            }
            else
            {
                if (default_browser.indexOf(NETSCAPE) < 0) {
                    cmd = default_browser + " "  + url;
                    Process p = Runtime.getRuntime().exec(cmd);
                }
                else {
                    // Under Unix, Netscape has to be running for the "-remote"
                    // command to work.  So, we try sending the command and
                    // check for an exit value.  If the exit command is 0,
                    // it worked, otherwise we need to start the browser.
                    // cmd = 'netscape -remote openURL(http://www.javaworld.com)'
                    cmd =  default_browser + " " + UNIX_FLAG + "(" + url + ")";
                    Process p = Runtime.getRuntime().exec(cmd);
                    try
                    {
                        // wait for exit code -- if it's 0, command worked,
                        // otherwise we need to start the browser up.
                        int exitCode = p.waitFor();
                        if (exitCode != 0)
                        {
                            // Command failed, start up the browser
                            // cmd = 'netscape http://www.javaworld.com'
                            cmd = default_browser + " "  + url;
                            p = Runtime.getRuntime().exec(cmd);
                        }
                    }
                    catch(InterruptedException x)
                    {
                        System.err.println("Error bringing up browser, cmd='" +
                                            cmd + "'");
                        System.err.println("Caught: " + x);
                    }
                }
            }
        }
        catch(IOException x)
        {
            // couldn't exec browser
            System.err.println("Could not invoke browser, command=" + cmd);
            System.err.println("Caught: " + x);
            System.err.println("Please change your default " +
                         "browser setting in the Alfi Preferences Dialog.");
        }
    }

    public static String getDefaultBrowser( )
    {
        boolean windows = isWindowsPlatform();
        if (windows)
        {
            return EXPLORER;
        }
        else
        {
            if (isLinuxPlatform()) {
                return MOZILLA;
            }
            else {
                return NETSCAPE;
            }
        }
    }

    /**
     * Try to determine whether this application is running under Windows
     * or some other platform by examing the "os.name" property.
     *
     * @return true if this application is running under a Windows OS
     */
    public static boolean isWindowsPlatform()
    {
        String os = System.getProperty("os.name");
        if ( os != null && os.startsWith(WIN_ID))
            return true;
        else
            return false;

    }
    /**
     * Try to determine whether this application is running under Linux
     * or some other platform by examing the "os.name" property.
     *
     * @return true if this application is running under a Linux OS
     */
    public static boolean isLinuxPlatform()
    {
        String os = System.getProperty("os.name");
        if ( os != null && os.startsWith(LINUX_ID))
            return true;
        else
            return false;

    }
    /**
     * Simple example.
     */
/*
    public static void main(String[] args)
    {
        displayURL("http://www.javaworld.com");
    }
*/
    // Used to identify the windows platform.
    private static final String WIN_ID = "Windows";
    // Used to identify the windows platform.
    private static final String LINUX_ID = "Linux";
    // The default system browser under windows.
    private static final String EXPLORER = "rundll32";
    // The flag to display a url.
    private static final String WIN_FLAG = "url.dll,FileProtocolHandler";
    // The default browser under unix.
    private static final String NETSCAPE = "netscape";
    // The default browser under Linux.
    private static final String MOZILLA = "mozilla";
    // The flag to display a url.
    private static final String UNIX_FLAG = "-remote openURL";
}
