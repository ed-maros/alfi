package edu.caltech.ligo.alfi.widgets;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.nwoods.jgo.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.widgets.*;

    /**
     * The ConnectionBuildingWidget is a temporary helper object which is added
     * to the view, not to the document.  When completed, it will be removed,
     * and a ConnectionWidget will be built from its info, and added to the
     * document.  If the building is cancelled, this widget will just be removed
     * from the view.
     */
public class ConnectionBuildingWidget extends JGoLink
                                         implements AlfiWidgetIF, KeyListener {

    ////////////////////////////////////////////////////////////////////////////
    ///// constants ////////////////////////////////////////////////////////////

        // connection building hints
    final static int HINT_NOT_SET          = 0;
    final static int STANDARD_BUILD        = 1;
    final static int BUNDLER_PRIMARY_IN    = 2;
    final static int BUNDLER_SECONDARY_IN  = 3;
    final static int BUNDLER_PRIMARY_OUT   = 4;
    final static int BUNDLER_SECONDARY_OUT = 5;

        // possible connection building configurations
    private final static int BUNDLER_PRIMARY_TO_BUNDLER_PRIMARY     = 0;
    private final static int BUNDLER_PRIMARY_TO_BUNDLER_SECONDARY   = 1;
    private final static int BUNDLER_PRIMARY_TO_NONBUNDLER          = 2;
    private final static int BUNDLER_SECONDARY_TO_BUNDLER_PRIMARY   = 3;
    private final static int BUNDLER_SECONDARY_TO_BUNDLER_SECONDARY = 4;
    private final static int BUNDLER_SECONDARY_TO_NONBUNDLER        = 5;
    private final static int NONBUNDLER_TO_BUNDLER_PRIMARY          = 6;
    private final static int NONBUNDLER_TO_BUNDLER_SECONDARY        = 7;
    private final static int NONBUNDLER_TO_NONBUNDLER               = 8;


    //**************************************************************************
    //***** member fields ******************************************************

    private final EditorFrame m_editor;

        /**
         * During mouse moves, the current mouse location and the point prior to
         * it are set as points on the path, but they are overwritten if the
         * mouse is moved elsewhere with no mouseclick at that location.
         */
    private boolean mb_last_2_path_points_are_temporary;

        /**
         * Utility field used in conjunction with
         * mb_last_2_path_points_are_temporary.
         */
    private Point m_last_permanent_point;

    private final GenericPortWidget m_initiating_terminal;

        // these fields need to be filled before this ConnectionBuildingWidget
        // is used to build a new connection.
    private GenericPortWidget m_source_widget;
    private GenericPortWidget m_sink_widget;
    private int m_data_type;
    private int m_source_build_hint;
    private boolean mb_source_hint_externally_set;
    private int m_sink_build_hint;
    private boolean mb_sink_hint_externally_set;
    private Vector m_corrected_path_points_vector;

    private HashMap m_secondary_out_name_map;
    private String m_secondary_in_name;
    private boolean mb_unwraps_secondary_input;

        /**
         * As the mouse moves, the path can be built either up/down (dy) and
         * then across (dx), or vice-versa.  This value is just toggled back and
         * forth by the user by hitting the spacebar.
         */
    private boolean mb_build_path_dy_then_dx;

        /**
         * For ports, the initial direction is set by port pointing direction,
         * I/O type, etc.  For junctions, though, the initial direction remains
         * unset and can change as the user moves the mouse until the first
         * click along the path.
         */
    private boolean mb_initial_direction_set;

    //**************************************************************************
    //***** constructors *******************************************************

        /**
         * Constructor.
         */
    public ConnectionBuildingWidget(GenericPortWidget _initiating_terminal,
                                                            ALFIView _view) {
        super();

            // final members
        m_editor = _view.getOwnerFrame();
        m_initiating_terminal = _initiating_terminal;

            // other members
        m_data_type = GenericPortProxy.DATA_TYPE_X_UNRESOLVED;
        m_source_build_hint = HINT_NOT_SET;
        m_sink_build_hint = HINT_NOT_SET;

        m_source_widget = null;
        m_sink_widget = null;

        m_secondary_out_name_map = null;
        m_secondary_in_name = null;
        mb_unwraps_secondary_input = false;

        if (_initiating_terminal instanceof PortWidget) {
            PortWidget initiating_port = (PortWidget) _initiating_terminal;
            if (initiating_port.isSinkPort()) {
                addPoint(initiating_port.getToLinkPoint());
            }
            else { addPoint(initiating_port.getFromLinkPoint()); }

                // set initial connection direction
            int direction = initiating_port.getInitialConnectionDirection();
            if ((direction == PortWidget.NORTH_POINTING) ||
                                    (direction == PortWidget.SOUTH_POINTING)) {
                mb_build_path_dy_then_dx = true;
            }
            else { mb_build_path_dy_then_dx = false; }
            mb_initial_direction_set = true;
        }
        else if ((_initiating_terminal instanceof JunctionWidget) ||
                             (_initiating_terminal instanceof BundlerWidget)) {
            addPoint(_initiating_terminal.getFromLinkPoint());
            mb_build_path_dy_then_dx = false;    // arbitrary setting here
            mb_initial_direction_set = false;
        }

        mb_last_2_path_points_are_temporary = false;
        m_last_permanent_point = getEndPoint();

        setOrthogonal(false);
        setPen(new JGoPen(JGoPen.DASHED,
                      ConnectionWidget.LOCAL_CONNECTION_WIDTH, Color.blue));

        _view.addKeyListener(this);
    }

    //**************************************************************************
    //***** methods ************************************************************

        /** If _source_widget is a valid source for a new connection whose sink
          * is _sink_widget, true is returned.  Otherwise false.
          */
    private boolean haveCongruentDataTypes(
            GenericPortWidget _source_widget, GenericPortWidget _sink_widget) {

        int[][] congruent_data_types =
                     determineCongruentDataTypes(_source_widget, _sink_widget);
        for (int i = 0; i < congruent_data_types.length; i++) {
            if (congruent_data_types[i] != null) { return true; }
        }

        return false;
    }

        /** Returns the data types and hints after sifting through all the
          * possibilities.  int[4] is returned, with the same format as shown in
          * determineCongruentDataTypes(), or if no valid configurations exist,
          * null is returned.
          */
    private int[] determineConfiguration(GenericPortWidget _source_widget,
                                              GenericPortWidget _sink_widget) {

        int[][] configs =
                     determineCongruentDataTypes(_source_widget, _sink_widget);

            // top priority if possible
        if (configs[BUNDLER_PRIMARY_TO_BUNDLER_PRIMARY] != null) {
            return configs[BUNDLER_PRIMARY_TO_BUNDLER_PRIMARY];
        }

            // equal priorities, must be selected unless only 1 config possible
        boolean b_1_2 = (configs[BUNDLER_PRIMARY_TO_BUNDLER_SECONDARY] != null);
        boolean b_2_1 = (configs[BUNDLER_SECONDARY_TO_BUNDLER_PRIMARY] != null);
        boolean b_2_2 = (configs[BUNDLER_SECONDARY_TO_BUNDLER_SECONDARY]!=null);
        if (b_1_2 || b_2_1 || b_2_2) {
            if (b_1_2 && (! b_2_1) && (! b_2_2)) {
                return configs[BUNDLER_PRIMARY_TO_BUNDLER_SECONDARY];
            }

            if ((! b_1_2) && b_2_1 && (! b_2_2)) {
                return configs[BUNDLER_SECONDARY_TO_BUNDLER_PRIMARY];
            }

            if ((! b_1_2) && (! b_2_1) && b_2_2) {
                return configs[BUNDLER_SECONDARY_TO_BUNDLER_SECONDARY];
            }

                // now user must select between possibles
            ArrayList option_list = new ArrayList();
            String s_1_2 = "Primary Output to Secondary Input";
            String s_2_1 = "Secondary Output to Primary Input";
            String s_2_2 = "Secondary Output to Secondary Input";
            if (b_1_2) { option_list.add(s_1_2); }
            if (b_2_1) { option_list.add(s_2_1); }
            if (b_2_2) { option_list.add(s_2_2); }
            option_list.add("Cancel");
            String[] options = new String[option_list.size()];
            option_list.toArray(options);
            String title = "Select Bundler I/O Configuration";
            String message = "The configuration of the connection from source" +
               " bundler [" + _source_widget + "] to sink bundler [" +
               _sink_widget + "] is indeterminate.  Please select the " +
               "configuration desired.";
            message = TextFormatter.formatMessage(message, 100);
            int choice = JOptionPane.showOptionDialog(m_editor, message, title,
                JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                options, null);

            if (options[choice].equals(s_1_2)) {
                return configs[BUNDLER_PRIMARY_TO_BUNDLER_SECONDARY];
            }
            else if (options[choice].equals(s_2_1)) {
                return configs[BUNDLER_SECONDARY_TO_BUNDLER_PRIMARY];
            }
            else if (options[choice].equals(s_2_2)) {
                return configs[BUNDLER_SECONDARY_TO_BUNDLER_SECONDARY];
            }
            else { return null; }
        }

            // the rest of the configs go in determinate order
        for (int i = 0; i < configs.length; i++) {
            if (configs[i] != null) { return configs[i]; }
        }

        return null;
    }

        /** Tests for data type congruency between a source and sink.  This test
          * takes any already set source/sink hints into account. The returned
          * int[][] has an entry for each configuration priority, (e.g.,
          * ConnectionBuildingWidget.BUNDLER_PRIMARY_TO_BUNDLER_PRIMARY.)  These
          * entries will be null for invalid configurations (the usual case,
          * actually), and for valid configurations, the int[priority][4]
          * contains the following information:
          *
          *     { source_data_type, source_hint, sink_data_type, sink_hint }
          *
          * If any connection building hints have already been set, they are
          * taken into account where appropriate.
          */
    private int[][] determineCongruentDataTypes(
            GenericPortWidget _source_widget, GenericPortWidget _sink_widget) {

        int[][] possible_source_types =
                _source_widget.getPossibleDataTypesForNewConnectionBuild(true);
        int[][] possible_sink_types =
                 _sink_widget.getPossibleDataTypesForNewConnectionBuild(false);
        int[][] build_info = new int[NONBUNDLER_TO_NONBUNDLER + 1][];
        for (int i = 0; i < build_info.length; i++) { build_info[i] = null; }

            // utility constants
        final int X_UNRESOLVED = GenericPortProxy.DATA_TYPE_X_UNRESOLVED;
        final int BUNDLE = GenericPortProxy.DATA_TYPE_ALFI_BUNDLE;
        final int UNKNOWN_1 = GenericPortProxy.DATA_TYPE_1_UNKNOWN;
        final int UNRESOLVED_1 = GenericPortProxy.DATA_TYPE_1_UNRESOLVED;

        for (int i = 0; i < possible_source_types.length; i++) {
            int source_type = possible_source_types[i][0];
            int source_hint = possible_source_types[i][1];

            if (mb_source_hint_externally_set &&
                                        (source_hint != m_source_build_hint)) {
                continue;
            }

            for (int j = 0; j < possible_sink_types.length; j++) {
                int sink_type = possible_sink_types[j][0];
                int sink_hint = possible_sink_types[j][1];

                if (mb_sink_hint_externally_set &&
                                            (sink_hint != m_sink_build_hint)) {
                    continue;
                }

                boolean b_identical_data_types = (source_type == sink_type);
                boolean b_unresolved_source_or_sink =
                    ((source_type==X_UNRESOLVED) || (sink_type==X_UNRESOLVED));
                boolean b_unknown_single_to_nonbundle =
                    (((source_type == UNKNOWN_1) && (sink_type != BUNDLE)) ||
                     ((sink_type == UNKNOWN_1) && (source_type != BUNDLE)));
                boolean b_unresolved_single_to_nonbundle =
                    (((source_type == UNRESOLVED_1) && (sink_type != BUNDLE)) ||
                     ((sink_type == UNRESOLVED_1) && (source_type != BUNDLE)));
                

                    // do data types check out?
                if (b_identical_data_types || b_unresolved_source_or_sink ||
                                            b_unknown_single_to_nonbundle ||
                                            b_unresolved_single_to_nonbundle) {
                    int[] types_and_hints =
                            { source_type, source_hint, sink_type, sink_hint };

                        // map by priority key (see key constants for order)
                    int priority = NONBUNDLER_TO_NONBUNDLER;
                    if (_source_widget instanceof BundlerWidget) {
                        if (_sink_widget instanceof BundlerWidget) {
                            if (source_hint == BUNDLER_PRIMARY_OUT) {
                                if (sink_hint == BUNDLER_PRIMARY_IN) {
                                    priority =
                                            BUNDLER_PRIMARY_TO_BUNDLER_PRIMARY;
                                }
                                else {
                                    priority =
                                          BUNDLER_PRIMARY_TO_BUNDLER_SECONDARY;
                                }
                            }
                            else if (sink_hint == BUNDLER_PRIMARY_IN) {
                                priority = BUNDLER_SECONDARY_TO_BUNDLER_PRIMARY;
                            }
                            else {
                                priority =
                                        BUNDLER_SECONDARY_TO_BUNDLER_SECONDARY;
                            }
                        }
                        else if (source_hint == BUNDLER_PRIMARY_OUT) {
                            priority = BUNDLER_PRIMARY_TO_NONBUNDLER;
                        }
                        else { priority = BUNDLER_SECONDARY_TO_NONBUNDLER; }
                    }
                    else if (_sink_widget instanceof BundlerWidget) {
                        if (sink_hint == BUNDLER_PRIMARY_IN) {
                            priority = NONBUNDLER_TO_BUNDLER_PRIMARY;
                        }
                        else { priority = NONBUNDLER_TO_BUNDLER_SECONDARY; }
                    }

                    build_info[priority] = types_and_hints;
                }
            }
        }

        return build_info;
    }

    public GenericPortWidget getSourceWidget() { return m_source_widget; }

    public GenericPortWidget getSinkWidget() { return m_sink_widget; }

        /** May be set external to default ConnectionBuildingWidget processing.
          */
    public void setSourceHint(int _hint) {
        mb_source_hint_externally_set = true;
        m_source_build_hint = _hint;
    }

        /** May be set external to default ConnectionBuildingWidget processing.
          */
    public void setSinkHint(int _hint) {
        mb_sink_hint_externally_set = true;
        m_sink_build_hint = _hint;
    }

    public Vector getCorrectedPathPoints() {
        return m_corrected_path_points_vector;
    }

        /** Fill in the fields which will be queried in order to create the
          * new connection.  A false return signals abort.
          */
    private boolean completeBuildConfiguration(
                                        GenericPortWidget[] _source_and_sink) {
        m_source_widget = _source_and_sink[0];
        m_sink_widget = _source_and_sink[1];

        Vector path_points = copyPoints();
        if (m_source_widget != m_initiating_terminal) {
            Collections.reverse(path_points);
        }
        m_corrected_path_points_vector = path_points;

        int[] config = determineConfiguration(m_source_widget, m_sink_widget);
        if (config == null) { return false; }

        m_data_type = config[0];    // may be unresolved yet
        m_source_build_hint = config[1];
        int downstream_data_type = config[2];    // may be unresolved yet
        m_sink_build_hint = config[3];

            // can change the data type
        if (m_source_build_hint == BUNDLER_SECONDARY_OUT) {
            BundlerProxy source_bundler =
                (BundlerProxy) m_source_widget.getAssociatedGenericPortProxy();

            BundlerOutputSelectionDialog dialog =
                BundlerOutputSelectionDialog.executeDialog(m_editor,
                    source_bundler, m_editor.getNodeInEdit(),
                    downstream_data_type, m_sink_widget.generateDescription());
            if (dialog.getExitStatus() == BundlerOutputSelectionDialog.OK) {
                Object[][] output_info = dialog.getSelections();
                m_secondary_out_name_map = new HashMap();
                for (int i = 0; i < output_info.length; i++) {
                    String secondary_out_name = (String) output_info[i][0];
                    Boolean B_is_bundle = (Boolean) output_info[i][1];
                    m_secondary_out_name_map.put(secondary_out_name,
                                                                  B_is_bundle);
                }

                if (dialog.outputIsBundle()) {
                    m_data_type = GenericPortProxy.DATA_TYPE_ALFI_BUNDLE;
                }
                else {
                    BundlerProxy bundler = (BundlerProxy) output_info[0][2];
                    ALFINode container = (ALFINode) output_info[0][3];
                    if (bundler.hasSecondaryInput(container)) {
                        ConnectionProxy in =
                                          bundler.getSecondaryInput(container);
                            // may still be single unresolved after this...
                        m_data_type = in.determineUpstreamDataType(container);
                    }
                    else {
                            // this occurs when the associated input bundler
                            // is not yet connected upstream.
                        m_data_type = GenericPortProxy.DATA_TYPE_1_UNRESOLVED;
                    }
                }
            }
            else { return false; }
        }

        if (m_sink_build_hint == BUNDLER_SECONDARY_IN) {
            BundlerProxy sink_bundler =
                  (BundlerProxy) m_sink_widget.getAssociatedGenericPortProxy();
            boolean b_is_bundle =
                       (m_data_type == GenericPortProxy.DATA_TYPE_ALFI_BUNDLE);

            BundlerInputDialog dialog = m_editor.showBundlerInputDialog(
                                         sink_bundler, b_is_bundle, "", false);
            if (dialog.getExitStatus() == BundlerInputDialog.OK) {
                m_secondary_in_name = dialog.getInputName();
                mb_unwraps_secondary_input = dialog.getUnwrapsInput();
            }
            else { return false; }
        }
        else if (m_data_type == GenericPortProxy.DATA_TYPE_1_UNRESOLVED) {
            m_data_type = downstream_data_type;
        }

        return true;
    }

    public boolean isBundle() {
        return (m_data_type == GenericPortProxy.DATA_TYPE_ALFI_BUNDLE);
    }

    private boolean isSourceBundlerPrimaryOutput() {
        return (m_source_build_hint == BUNDLER_PRIMARY_OUT);
    }

    private boolean isSinkBundlerPrimaryInput() {
        return (m_sink_build_hint == BUNDLER_PRIMARY_IN);
    }

    public TerminalBundlersConfiguration generateTerminalBundlersConfig() {
        return new TerminalBundlersConfiguration(
            isSourceBundlerPrimaryOutput(),
                m_secondary_out_name_map,

            isSinkBundlerPrimaryInput(),
                m_secondary_in_name, mb_unwraps_secondary_input);
    }

    public EditorFrame getEditor() { return m_editor; }

        /** There is no proxy object for this temporary widget. */
    public Object getAssociatedProxyObject() { return null; }

        /** Currently, if the first port clicked on has a congruent data type
          * with the second as sink, then the flow is assumed to go from _gpw_1
          * (source) to _gpw_2 (sink).  This is reversed if there are no
          * possibilities for _gpw_1 -> _gpw_2 but possibilities for the reverse
          * direction exist.  If no possibilities at all exist, null is
          * returned.
          */
    private GenericPortWidget[] determineFlowDirection(
                          GenericPortWidget _gpw_1, GenericPortWidget _gpw_2) {
        GenericPortWidget[] source_and_sink = new GenericPortWidget[2];
        if (haveCongruentDataTypes(_gpw_1, _gpw_2)) {
            source_and_sink[0] = _gpw_1;
            source_and_sink[1] = _gpw_2;
        }
        else if (haveCongruentDataTypes(_gpw_2, _gpw_1)) {
            source_and_sink[0] = _gpw_2;
            source_and_sink[1] = _gpw_1;
        }
        else { source_and_sink = null; }

        return source_and_sink;
    }

        /** AlfiWidgetIF covenience method. */
    public boolean isLocal() { return true; }

        // mouse event methods in AlfiWidgetIF interface which are not already
        // implemented in JGoObject

    public boolean doMouseDown(int _modifiers, Point _dc, Point _vc,
                                                               JGoView _view) {
        return false;
    }

    public boolean doMouseUp(int _modifiers, Point _dc, Point _vc,
                                                               JGoView _view) {
        return false;
    }

    public boolean doMouseClick(int _modifiers, Point _dc, Point _vc,
                                                               JGoView _view) {
        if (! (_view instanceof ALFIView)) { return false; }
        ALFIView alfiview = (ALFIView) _view;

            // LMB operation
        if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
            JGoObject click_object = _view.pickDocObject(_dc, false);
            if (click_object instanceof GenericPortWidget) {
                GenericPortWidget other_terminal =
                                         (GenericPortWidget) click_object;

                GenericPortWidget[] source_and_sink = determineFlowDirection(
                                        m_initiating_terminal, other_terminal);

                if (source_and_sink != null) {
                    boolean b_ok = completeBuildConfiguration(source_and_sink);
                    if (b_ok) {
                            // build the connection
                        m_editor.addConnection(this);

                        alfiview.cancelActiveObjectAction();
                    }
                }
            }
            else {
                m_last_permanent_point = getEndPoint();
                mb_last_2_path_points_are_temporary = false;
                mb_build_path_dy_then_dx = !mb_build_path_dy_then_dx;  // toggle
                mb_initial_direction_set = true;
            }

            return true;
        }
            // MMB operation
        else if ((_modifiers & InputEvent.BUTTON2_MASK) != 0) {
            alfiview.cancelActiveObjectAction();
            return true;
        }
            // RMB operation
        else if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
            alfiview.cancelActiveObjectAction();
            return true;
        }
        else { return false; }
    }

    public boolean doMouseMove(int _modifiers, Point _dc, Point _vc,
                                                               JGoView _view) {
        return false;
    }

    public boolean doUncapturedMouseMove(int _modifiers, Point _dc, Point _vc,
                                                               JGoView _view) {
        Point grid_point = GridTool.getNearestGridPoint(_dc);
        if (! mb_initial_direction_set) {
            Point init_point = getStartPoint();
            mb_build_path_dy_then_dx = (Math.abs(grid_point.y - init_point.y) >
                                        Math.abs(grid_point.x - init_point.x));
        }

        if (mb_last_2_path_points_are_temporary) {
                // set new values for the last 2 points due to the mouse move
            setLast2Points(grid_point);
        }
        else {
            addPoint(grid_point);    // dummy to be changed in a moment
            addPoint(grid_point);
            mb_last_2_path_points_are_temporary = true;
            setLast2Points(grid_point);
        }

        JGoObject mouseover_object = _view.pickDocObject(_dc, false);
        if (mouseover_object instanceof AlfiWidgetIF) {
            ((AlfiWidgetIF) mouseover_object).doUncapturedMouseMove(_modifiers,
                                                               _dc, _vc, _view);
        }

        return true;
    }

    /////////////////////////////////
    // KeyListener methods //////////

        /** Unused KeyListener method. */
    public void keyPressed(KeyEvent e) { ; }

        /** Unused KeyListener method. */
    public void keyReleased(KeyEvent e) { ; }

        /** KeyListener method.  Spacebar hit toggles the path direction. */
    public void keyTyped(KeyEvent e) {
        if (e.getKeyChar() == ' ') {
            mb_build_path_dy_then_dx = !mb_build_path_dy_then_dx;
            mb_initial_direction_set = true;
            setLast2Points(getEndPoint());
        }
    }

    private void setLast2Points(Point _new_last_point) {
        Point new_second_to_last_point = (mb_build_path_dy_then_dx) ?
                        new Point(m_last_permanent_point.x, _new_last_point.y) :
                        new Point(_new_last_point.x, m_last_permanent_point.y);
        int index_of_last_point = getNumPoints() - 1;
        //setPoint(index_of_last_point - 1, new_second_to_last_point);
        setPoint(index_of_last_point, new_second_to_last_point);
        setPoint(index_of_last_point, _new_last_point);
    }
}
