package edu.caltech.ligo.alfi.widgets;

import java.awt.*;
import javax.swing.event.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.editor.*;

/**
  * This interface defines constants used in this package relating
  * to the JGo package as well as event processing methods and modification
  * notification.
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public interface AlfiWidgetIF {

    // Event hints

    public static final int VIEW_EVENTS = JGoDocumentEvent.LAST + 100;
    public static final int NODE_EVENTS = JGoDocumentEvent.LAST + 200;
    public static final int NODE_PORT_EVENTS = JGoDocumentEvent.LAST + 300;
    public static final int CONNECTION_EVENTS = JGoDocumentEvent.LAST + 400;
    public static final int JUNCTION_EVENTS = JGoDocumentEvent.LAST + 500;
    public static final int COMMENT_EVENTS = JGoDocumentEvent.LAST + 500;


        /** All widgets know what editor object they exist inside. */
    public EditorFrame getEditor();

        /** All widgets have a bookkeeping proxy object associated with them. */
    public Object getAssociatedProxyObject();

        /** All widgets know if they are local or inherited objects. */
    public boolean isLocal();


    // These are mouse event methods which all alfi widgets must implement.
    // But some of them already have default implementations from the JGoObject
    // inheritance which all widgets also have.  We want to be able to pass
    // AlfiWidgetIF objects around, though, which can respond to any of the
    // following methods.

        /** No default implementation in JGoObject.  Must be implemented. */
    public abstract boolean doMouseDown(int _modifiers,
                               Point _dc, Point _vc, JGoView _view);

        /** No default implementation in JGoObject.  Must be implemented. */
    public abstract boolean doMouseUp(int _modifiers,
                             Point _dc, Point _vc, JGoView _view);

        /** No default implementation in JGoObject.  Must be implemented. */
    public abstract boolean doMouseMove(int _modifiers,
                               Point _dc, Point _vc, JGoView _view);

        /** Default implementation in JGoObject exists. */
    public abstract boolean doMouseClick(int _modifiers,
                                Point _dc, Point _vc, JGoView _view);

        /** Default implementation in JGoObject exists. */
    public abstract boolean doMouseDblClick(int _modifiers,
                                   Point _dc, Point _vc, JGoView _view);

        /** Default implementation in JGoObject exists. */
    public abstract boolean doUncapturedMouseMove(int _modifiers,
                                         Point _dc, Point _vc, JGoView _view);
}

