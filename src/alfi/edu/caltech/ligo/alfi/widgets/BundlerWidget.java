package edu.caltech.ligo.alfi.widgets;

import java.util.*;
import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.event.*;
import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.widgets.*;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.editor.*;
import com.nwoods.jgo.*;

    /**
     * This class is the widget which represents bundlers in an Alfi edit
     * window.
     */
public class BundlerWidget extends GenericPortWidget {

    //**************************************************************************
    //***** constants **********************************************************

    private static final JGoPen PEN__NO_2ND_IO =
                                  new JGoPen(JGoPen.SOLID, 1, Color.lightGray);
    private static final JGoPen PEN__HAS_2ND_IN =
                                      new JGoPen(JGoPen.SOLID, 1, Color.green);
    private static final JGoPen PEN__HAS_2ND_OUT =
                                       new JGoPen(JGoPen.SOLID, 1, Color.pink);
    private static final JGoBrush NORMAL_BRUSH =
                                     new JGoBrush(JGoBrush.SOLID, Color.white);
    private static final JGoBrush MOD_BRUSH =
                                 new JGoBrush(JGoBrush.SOLID, Color.lightGray);

        // cant set these until inside constructor
    public static final int BUNDLER_SIDE =
                                ConnectionWidget.LOCAL_CONNECTION_WIDTH * 2;
    private final Point POSITION_OFFSET =
        new Point((int) (-1 * BUNDLER_SIDE/2), (int) (-1 * BUNDLER_SIDE/2));

        /** Used only to speed up paint().  Do not use for other purposes. */
    private ConnectionWidget m_primary_in_widget;

        /** Used only to speed up paint().  Do not use for other purposes. */
    private ConnectionWidget m_primary_out_widget;

    //**************************************************************************
    //***** member fields ******************************************************

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public BundlerWidget(EditorFrame _editor,
                          BundlerProxy _associated_proxy) {
        super(_editor, _associated_proxy);

        m_primary_in_widget = null;
        m_primary_out_widget = null;

        setStyle(StyleRectangle);
        setFromSpot(Center);
        setToSpot(Center);

        setLocationOffset(_associated_proxy.getLocation(), POSITION_OFFSET);
        setSize(BUNDLER_SIDE, BUNDLER_SIDE);

        setPen(PEN__NO_2ND_IO);
        setBrush(NORMAL_BRUSH);

        setSelectable(true);
        setResizable(false);
        setDraggable(true);

        getAssociatedProxy().addProxyChangeListener(this);

        updateBundlerAppearanceState();
    }


    //**************************************************************************
    //***** methods ************************************************************

    //////////////////////////////////////////////////////////
    ////////// member field access ///////////////////////////

    public BundlerProxy getAssociatedProxy() {
        return ((BundlerProxy) m_associated_proxy);
    }

    public String toString() {
        BundlerProxy bundler = getAssociatedProxy();
        ALFINode container = getEditor().getNodeInEdit();

        String s = bundler.getName();
        if (bundler.hasSecondaryInput(container)) {
            s += " (secondary in: " + bundler.getSecondaryInputName(container);
            if (bundler.secondaryInputUnwraps(container)) {
                s += " (unwraps)";
            }

            s += ")";
        }

        if (bundler.hasSecondaryOutput(container)) {
            HashMap output_name_map = 
                             bundler.getSecondaryOutputChannelNames(container);
            if (output_name_map.size() > 0) {
                Iterator i = output_name_map.keySet().iterator();
                s += " (secondary out: " + i.next();

                while (i.hasNext()) { s += ", " + i.next(); }
            
                s += ")";
            }
        }

        return s;
    }

    /////////////////////////////////////////////////////////////////////
    ////////// implementations of GenericPortWidget abstract methods ////

        /** Implementation of
          * GenericPortWidget.getPossibleDataTypesForNewConnectionBuild().
          *
          * Here are the rules for the types bundlers will return:
          *
          * As a data source:
          *   If the bundler has no secondary I/O,
          *   DATA_TYPE_X_UNRESOLVED / BUNDLER_SECONDARY_OUT will be in the list
          *   returned.  Additionally, if the bundler has no primary output
          *   DATA_TYPE_ALFI_BUNDLE / BUNDLER_PRIMARY_OUT will also be listed.
          *   If both a primary output and any secondary I/O exists, the list
          *   will be empty.
          *   
          * As a data sink:
          *   If the bundler has no secondary I/O,
          *   DATA_TYPE_X_UNRESOLVED / BUNDLER_SECONDARY_IN will be in the list
          *   returned.  Additionally, if the bundler has no primary input
          *   DATA_TYPE_ALFI_BUNDLE / BUNDLER_PRIMARY_IN will also be listed.
          *   If both a primary input and any secondary I/O exists, the list
          *   will be empty.
          *
          * In general, the return value will be int[n][2] where [i][0] is the
          * ith possible data type, and [i][1] is the corresponding hint.  A
          * value of int[0][0] is returned when no possibilities exist.
          */
    int[][] getPossibleDataTypesForNewConnectionBuild(
                                                   boolean _b_as_source_port) {
        BundlerProxy bundler = getAssociatedProxy();
        ALFINode container = m_editor.getNodeInEdit();

        ArrayList type_and_hint_list = new ArrayList();

        boolean b_has_primary_out = bundler.hasPrimaryOutput(container);
        boolean b_has_primary_in = bundler.hasPrimaryInput(container);
        boolean b_has_secondary = (bundler.hasSecondaryOutput(container) ||
                                   bundler.hasSecondaryInput(container));

            // util data type constants
        final int BUNDLE = GenericPortProxy.DATA_TYPE_ALFI_BUNDLE;
        final int X_UNRESOLVED = GenericPortProxy.DATA_TYPE_X_UNRESOLVED;

            // util hint constants
        final int PRIMARY_IN = ConnectionBuildingWidget.BUNDLER_PRIMARY_IN;
        final int PRIMARY_OUT = ConnectionBuildingWidget.BUNDLER_PRIMARY_OUT;
        final int SECONDARY_IN = ConnectionBuildingWidget.BUNDLER_SECONDARY_IN;
        final int SECONDARY_OUT= ConnectionBuildingWidget.BUNDLER_SECONDARY_OUT;

        if (_b_as_source_port) {
            if (! b_has_primary_out) {
                int[] type_and_hint = { BUNDLE, PRIMARY_OUT };
                type_and_hint_list.add(type_and_hint);
            }

            if (! b_has_secondary) {
                {
                    int[] type_and_hint = { X_UNRESOLVED, SECONDARY_OUT };
                    type_and_hint_list.add(type_and_hint);
                }
            }
        }
        else {
            if (! b_has_primary_in) {
                int[] type_and_hint = { BUNDLE, PRIMARY_IN };
                type_and_hint_list.add(type_and_hint);
            }

            if (! b_has_secondary) {
                {
                    int[] type_and_hint = { X_UNRESOLVED, SECONDARY_IN };
                    type_and_hint_list.add(type_and_hint);
                }
            }
        }

        int[][] types_and_hints = new int[type_and_hint_list.size()][];
        type_and_hint_list.toArray(types_and_hints);
        return types_and_hints;
    }

    public String generateDescription() {
        return "(Bundler) " + getAssociatedProxy().getName();
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////// implementations of AlfiWidgetIF methods /////////////////////////

        /** AlfiWidgetIF method. */
    public EditorFrame getEditor() { return m_editor; }

        /** AlfiWidgetIF method. */
    public Object getAssociatedProxyObject() { return getAssociatedProxy(); }

    ////////////////////////////////////////
    ////////// event processing ////////////

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseDown(int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
        ALFIView alfiview = (ALFIView) _view;

            // RMB operations
        if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
            alfiview.pickObject(this);
            alfiview.doWidgetPopupMenu(this, _modifiers, _dc, _vc);
            return true;
        }
        else { return false; }
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseUp(int _modifiers,
                             Point _dc, Point _vc, JGoView _view) {
        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseMove(int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
        return false;
    }

        /**
         * A left mouse click on a bundler either starts the construction of a
         * connection, or completes the construction of a connection which is
         * currently under construction.
         */
    public boolean doMouseClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        final ALFIView alfi_view = (ALFIView) _view;

        _view.getSelection().clearSelection();
            // LMB operations
        if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
            ALFINode container = m_editor.getNodeInEdit();
            BundlerProxy bundler = getAssociatedProxy();
                // already has max number of connections?
            if (isLocal() &&
                        (bundler.getConnections(container, null).length < 3)) {

                    // a temp object added only to the view
                final ConnectionBuildingWidget cbw =
                                new ConnectionBuildingWidget(this, alfi_view);
                alfi_view.setActiveObject(cbw);
                alfi_view.addObjectAtHead(cbw);

                    // wait a moment before changing cursor

                java.util.Timer timer = new java.util.Timer();
                timer.schedule(
                    new TimerTask() {
                        public void run() {
                            if (alfi_view.getActiveObject() == cbw) {
                                alfi_view.setCursor(new Cursor(
                                                      Cursor.CROSSHAIR_CURSOR));
                            }
                        }
                    }, 250);

                return true;    // consume event
            }
            if ((bundler.group_get()) != null &&
                    (_modifiers & InputEvent.CTRL_MASK) != 0) {
                    
                    // This bundler is a part of a group.
                    // Send out an event that all elements need to be selected
                GroupElementModEvent event = 
                        new GroupElementModEvent(this, 
                            GroupElementModEvent.GROUP_SELECT,  
                            bundler.group_get()); 
                bundler.fireGroupElementModEvent(event);
                return false;
            }
        }
        else if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
            alfi_view.pickObject(this);
            alfi_view.doWidgetPopupMenu(this, _modifiers, _dc, _vc);
        }

        return false;
    }

        /** Diagnostics only. */
    public boolean doMouseDblClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        BundlerProxy bundler = getAssociatedProxy();
        ALFINode node = m_editor.getNodeInEdit();
        System.err.println(bundler.generateParserRepresentation(node, ""));

        return true;    // consume event
    }

    public boolean doUncapturedMouseMove(int _flags, Point _dc, Point _vc,
                                                                JGoView _view) {
        m_editor.setStatusText(toString());
        return true;    // consume event
    }

    ////////////////////////////////////////
    ////////// other methods  //////////////

        /** Returns the primary input connection widget (may be null.) */
    public ConnectionWidget getPrimaryInputWidget() {
        return getPrimaryInOutAndSecondary()[0];
    }

        /** Returns the primary output connection widget (may be null.) */
    public ConnectionWidget getPrimaryOutputWidget() {
        return getPrimaryInOutAndSecondary()[1];
    }

        /** Returns secondary input/output connection widget (may be null.) */
    public ConnectionWidget getSecondaryWidget() {
        return getPrimaryInOutAndSecondary()[2];
    }

        /** Returns a ConnectionWidget[3] where [0] is the primary input,
          * [1] is the primary output, and [2] is the secondary in/out.  Any of
          * these may be null independent of the others.
          */
    private ConnectionWidget[] getPrimaryInOutAndSecondary() {
        ConnectionWidget[] in_out_secondary = new ConnectionWidget[3];
        ALFINode container = m_editor.getNodeInEdit();
        BundlerProxy bundler = getAssociatedProxy();
        BundleProxy primary_in = bundler.getPrimaryInput(container);
        BundleProxy primary_out = bundler.getPrimaryOutput(container);
        ConnectionProxy secondary = (bundler.hasSecondaryInput(container)) ?
                                         bundler.getSecondaryInput(container) :
                                         bundler.getSecondaryOutput(container);

        in_out_secondary[0] = (primary_in != null) ?
                        m_editor.determineConnectionWidget(primary_in) : null;
        in_out_secondary[1] = (primary_out != null) ?
                        m_editor.determineConnectionWidget(primary_out) : null;
        in_out_secondary[2] = (secondary != null) ?
                        m_editor.determineConnectionWidget(secondary) : null;

        return in_out_secondary;
    }

    public void linkChange() {
        super.linkChange();

        updatePrimaryIOWidgetObjects();
    }

        /** Updates m_primary_in_widget and m_primary_out_widget, solely for
          * the speed of the paint() method.  Do not use m_primary_in_widget or
          * m_primary_out_widget for anything else.  (I.e., get such stuff from
          * the bookkeeepping, not here.
          */
    private void updatePrimaryIOWidgetObjects() {
        BundlerProxy bundler = getAssociatedProxy();
        BundleProxy primary_in =
                             bundler.getPrimaryInput(m_editor.getNodeInEdit());
        BundleProxy primary_out =
                            bundler.getPrimaryOutput(m_editor.getNodeInEdit());

        m_primary_in_widget = (primary_in != null) ?
                         m_editor.determineConnectionWidget(primary_in) : null;
        m_primary_out_widget = (primary_out != null) ?
                        m_editor.determineConnectionWidget(primary_out) : null;
    }

    private void updateSecondaryIOStateAppearance() {
        JGoPen pen = PEN__NO_2ND_IO;
        BundlerProxy bundler = getAssociatedProxy();
        ALFINode container = m_editor.getNodeInEdit();

        if (bundler.hasSecondaryInput(container)) {
            pen = PEN__HAS_2ND_IN;
        }
        else if (bundler.hasSecondaryOutput(container)) {
            pen = PEN__HAS_2ND_OUT;
        }

        setPen(pen);
    }

    private void updateBundlerAppearanceState() {
        ALFINode container = m_editor.getNodeInEdit();
        JGoBrush brush = NORMAL_BRUSH;
            // inherited bundlers with local io settings are "special"
        BundlerProxy bundler = (BundlerProxy) m_associated_proxy;
        if ((! container.bundler_containsLocal(bundler)) &&
                           (container.bundlerSetting_containsLocal(bundler))) {
            brush = MOD_BRUSH;
        }
        setBrush(brush);

        updatePrimaryIOWidgetObjects();
        updateSecondaryIOStateAppearance();
    }

        /** Paint is overridden to draw primary paths on widget. */
    public void paint(Graphics2D _g, JGoView _view) {
        super.paint(_g, _view);

        final Color bundle_color = PortWidget.getDataTypeColor(
                                       GenericPortProxy.DATA_TYPE_ALFI_BUNDLE);
        final float outline_width = 3.0f;
        final float inside_width = 1.0f;

        if ((m_primary_in_widget == null) || (m_primary_out_widget == null)) {
            updatePrimaryIOWidgetObjects();
        }

        Point center = getToLinkPoint();
        if (m_primary_in_widget != null) {
            int direction = m_primary_in_widget.determineLastDirection();
            Shape segment = createPrimaryFlowIndicator(center, direction, true);
            drawPrimaryPath(_g, segment, outline_width, inside_width,
                                                  bundle_color, Color.black);
        }

        if (m_primary_out_widget != null) {
            int direction = m_primary_out_widget.determineInitialDirection();
            Shape segment = createPrimaryFlowIndicator(center, direction,false);
            drawPrimaryPath(_g, segment, outline_width, inside_width,
                                                  bundle_color, Color.black);
        }
    }

    private void drawPrimaryPath(Graphics2D _g, Shape _segment,
                                 float _outer_width, float _inner_width,
                                 Color _outer_color, Color _inner_color) {
        _g.setPaint(_inner_color);
        _g.setStroke(new BasicStroke(_inner_width));
        _g.draw(_segment);
    }

        /** Creates line segment shape to show flow of primaries. */
    private Shape createPrimaryFlowIndicator(Point _center, int _direction,
                                                         boolean _b_is_input) {
        int length = getSize().width / 2;
        double x2 = _center.x;
        double y2 = _center.y;
        switch (_direction) {
          case ConnectionWidget.SEGMENT_DATA_FLOW_NORTH:
            y2 += (_b_is_input) ? length : -length;
            break;
          case ConnectionWidget.SEGMENT_DATA_FLOW_SOUTH:
            y2 += (_b_is_input) ? -length : length;
            break;
          case ConnectionWidget.SEGMENT_DATA_FLOW_EAST:
            x2 += (_b_is_input) ? -length : length;
            break;
          case ConnectionWidget.SEGMENT_DATA_FLOW_WEST:
            x2 += (_b_is_input) ? length : -length;
            break;
        }
        return new Line2D.Double(_center.x, _center.y, x2, y2);
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////// ProxyChangeEventListener implementations ////////////////////////

        /** ProxyChangeEventListener method. */
    public void proxyChangePerformed(ProxyChangeEvent e) {
        if (e.getSource() == getAssociatedProxy()) {
            setLocationOffset(getAssociatedProxy().getLocation(),
                                                              POSITION_OFFSET);
            updateBundlerAppearanceState();
        }
        else { Alfi.warn("Invalid ProxyChangeEvent source."); }
    }
}
