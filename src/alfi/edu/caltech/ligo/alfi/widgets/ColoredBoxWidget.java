package edu.caltech.ligo.alfi.widgets;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.common.*;
import com.nwoods.jgo.*;

    /**
     * This class is the widget which represents colored highlights
     * in an Alfi edit window.
     */
public class ColoredBoxWidget extends JGoArea implements AlfiWidgetIF,
                 ProxyChangeEventListener {



    //**************************************************************************
    //***** member fields ******************************************************

    final protected EditorFrame m_editor;

    final protected ColoredBox m_colored_box;

    final protected boolean m_is_unfrozen = true;

    final protected boolean m_is_grouped = false;

    private JGoPolygon m_background = null;

    final protected boolean mb_is_local_comment;

    private int m_mouse_down_modifiers;
    private Point m_mouse_down_point;
    private Point m_resizing_initial_mouse_down_point;
 
    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor for Comment
          */
    public ColoredBoxWidget( EditorFrame _editor, ColoredBox _colored_box) {
        this(_editor, _colored_box, false);
    }

    public ColoredBoxWidget( EditorFrame _editor, ColoredBox _colored_box,
                    boolean _b_raw_external_view) {

        m_editor = _editor;

        m_colored_box = _colored_box;

        mb_is_local_comment =
           _editor.getNodeInEdit().coloredBox_containsLocal(_colored_box);


        initialize();
        m_mouse_down_point = null;

        ///////////////////////////////////////
        // REMEMBER TO REMOVE LISTENERS TOO!!//
        ///////////////////////////////////////

        if (! _b_raw_external_view ) {
            m_colored_box.addProxyChangeListener(this);
        }
     }



    private void initialize() {

        setSelectable(m_is_unfrozen);
        //setGrabChildSelection(m_is_unfrozen);
        setPickableBackground(m_is_unfrozen);
        setDraggable(m_is_unfrozen);
         
        layoutContent(m_colored_box);

        setLocation(m_colored_box.getLocation());

        //setEditable(m_is_unfrozen);
        setResizable(m_is_unfrozen);
    }

    public void layoutContent(ColoredBox _colored_box) {
      
        AlfiColors alfi_colors = new AlfiColors(); 
        int color_id = _colored_box.getColor();
        Color background = alfi_colors.getColor(color_id);

        m_background = new JGoPolygon();
        m_background.setSelectable(false);
        m_background.setBrush(JGoBrush.makeStockBrush(background));
        m_background.setPen(JGoPen.make(JGoPen.SOLID, 1, background));
        m_background.addPoint(new Point(0, 0));
        m_background.addPoint(new Point(0, 50));
        m_background.addPoint(new Point(30, 50));
        m_background.addPoint(new Point(30,  0));

        Dimension comment_size = _colored_box.getSize();
        m_background.setBoundingRect(0, 0, 
                                    ( int )comment_size.getWidth(),
                                    ( int )comment_size.getHeight());
        setSize(m_background.getSize());

        addObjectAtTail(m_background);

        setLocation(_colored_box.getLocation());
    }

    /////////////////////////////////
    // member field access //////////

    // get access to the parts of the node
    public JGoPolygon getBackground() { return m_background; }

/*
    public boolean isEditable () { return getLabel().isEditable(); }
    public void setEditable (boolean e) { getLabel().setEditable(e); }
*/

    //**************************************************************************
    //***** methods ************************************************************

    public static Color getColor(int _color_id) {
        if ((_color_id >= 0) &&
                               (_color_id < AlfiColors.ALFI_LAST_COLOR)) {
            return AlfiColors.getColor(_color_id);
        }
        else { return AlfiColors.getColor(AlfiColors.ALFI_YELLOW); }
    }

    public void refresh() {

        // Clear the objects
        if (m_background != null) {
            removeObject(m_background);
            m_background = null;
        }

        layoutContent(m_colored_box);
        
        //setEditable(m_is_unfrozen);
        setResizable(m_is_unfrozen);
    }


    
    //////////////////////////////////////////////////////////
    ////////// AlfiWidgetIF methods //////////////////////////

        /** AlfiWidgetIF method. */
    public EditorFrame getEditor() { return m_editor; }

        /** AlfiWidgetIF method. */
    public Object getAssociatedProxyObject() { return getAssociatedProxy(); }

        /** AlfiWidgetIF covenience method. */
    public boolean isLocal() {
        return true;
    }

        
    public ColoredBox getAssociatedProxy() { return m_colored_box; }

     //////////////////////////////
    // event processing //////////

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseDown(int _modifiers, Point _dc, Point _vc,
                                                           JGoView _view) {
        m_mouse_down_point = null;
        final ALFIView view = (ALFIView) _view;

        m_mouse_down_modifiers = _modifiers;
        if ((_modifiers & InputEvent.BUTTON3_MASK) != 0){
            view.doHighlightPopupMenu(this, _modifiers, _dc, _vc);
            return true;
        }
        else if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
            // The ColoredBox is not a part of a group
            if (m_colored_box.group_get() == null)  {
                view.pickObject(this, true);
                m_mouse_down_point = _dc;
                return false;
            }
            else {
                if ((_modifiers & InputEvent.CTRL_MASK) != 0) {
                    m_mouse_down_point = _dc;
                    return true;
                }
                else {
                    // This ColoredBox is a part of a group.
                    // Send out an event that all elements need to be selected
                    GroupElementModEvent event = 
                        new GroupElementModEvent(this, 
                            GroupElementModEvent.GROUP_SELECT,  
                            m_colored_box.group_get()); 
                    m_colored_box.fireGroupElementModEvent(event);
                    return false;
                }
            }
        }

        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseUp(int _modifiers, Point _dc, Point _vc, JGoView _view) {
        ALFIView view = (ALFIView) _view;

        if (m_is_unfrozen) {
            view.pickObject(this, true);

            if ((_modifiers & InputEvent.BUTTON1_MASK) != 0){
                // check if the node has been moved
                if ((m_mouse_down_point != null) &&
                            (! _dc.equals(m_mouse_down_point))) {

                    view.performInternalComponentsMove(KeyEvent.VK_UNDEFINED);

                    if (( (_modifiers & InputEvent.CTRL_MASK) != 0) &&
                          m_colored_box.group_get() != null  ){
                        int x_offset = _dc.x - m_mouse_down_point.x;
                        int y_offset = _dc.y - m_mouse_down_point.y;
                        view.moveSelection(view.getSelection(), 0, 
                                       x_offset, y_offset, 0);
                        view.setState(JGoView.MouseStateSelection);
                     }
                }
            }

            m_mouse_down_point = null;
        }
        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseMove(int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
        return false;
    }

        /**
         * A mouse click on a comment that is not frozen
         *
         */
    public boolean doMouseClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        ALFIView view = (ALFIView)_view;
        if (m_is_unfrozen) {
            view.pickObject(this, true);
        }

        if ((_modifiers & InputEvent.BUTTON3_MASK) != 0){
            view.doHighlightPopupMenu(this, _modifiers, _dc, _vc);
            return true;
        }
        else if ((_modifiers & InputEvent.BUTTON1_MASK) != 0){
            if (m_is_unfrozen) {  
                return false;
            }
        }
        return false;
    }

    public boolean doMouseDblClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        return false;
    }

    public boolean doUncapturedMouseMove(int _flags, Point _dc, Point _vc,
                                                                JGoView _view) {
        return false;
    }
  
    //////////////////////////////////////////////////////////
    //////////  ProxyChangeEventListener methods /////////////

        /** ProxyChangeEventListener method. */
    public void proxyChangePerformed(ProxyChangeEvent _event) {
        refresh();
    }


    //////////////////
    // misc //////////


    public String toString() {
        if (m_is_unfrozen) {
            return m_colored_box.getName();
        }
        return null;
    }

}
