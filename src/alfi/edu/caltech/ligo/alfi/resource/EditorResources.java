package edu.caltech.ligo.alfi.resource;

import java.util.Locale;
import java.util.ResourceBundle; 

import java.awt.Event;
import java.awt.event.KeyEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import edu.caltech.ligo.alfi.common.UIResourceBundle;
import edu.caltech.ligo.alfi.common.GuiConstants;
import edu.caltech.ligo.alfi.editor.EditorActionsIF;
/**    
  * <pre>
  * Resource strings for implementing the Actions
  * </pre>
  *
  * each array is composed of :<BR>
  * index 0: Key <BR>
  * index 1: Name <BR>
  * index 2: Description <BR>
  * index 3: Mnemonic<BR>
  * index 4: KeyStroke<BR>
  * index 5: Primary Image<BR>
  * index 6: Rollover Image<BR>
  * index 7: Pressed Image<BR>

  * outside of the bounds can be used by individual windows.
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class EditorResources extends UIResourceBundle {
	private static Object[][] editorResources = {
    { EditorActionsIF.EDIT, "Edit", "Edit", new Character('E')},
    { EditorActionsIF.CUT, "Cut", "Cut", new Character('T'),
            KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.CTRL_MASK)},
    { EditorActionsIF.COPY, "Copy", "Copy", new Character('C'),
            KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK)},
    { EditorActionsIF.DUPLICATE, "Duplicate", "Duplicate", new Character('D'),
            KeyStroke.getKeyStroke(KeyEvent.VK_D, Event.CTRL_MASK)},
    { EditorActionsIF.PASTE, "Paste", "Paste", new Character('P'),
            KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK)},
    { EditorActionsIF.DELETE, "Delete", "Delete", new Character('L'),
            KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0)},
    { EditorActionsIF.SELECT_ALL, "Select All", "Select All",new Character('A'),
            KeyStroke.getKeyStroke(KeyEvent.VK_A, Event.CTRL_MASK)},
    { EditorActionsIF.GROUP, "Group", "Group", new Character('G'),
            KeyStroke.getKeyStroke(KeyEvent.VK_G, Event.CTRL_MASK)},
    { EditorActionsIF.UNGROUP, "Ungroup", "Ungroup", new Character('U'),
            KeyStroke.getKeyStroke(KeyEvent.VK_U, Event.CTRL_MASK)},

    { EditorActionsIF.ALIGN, "Align Nodes", "Align Nodes", new Character('A')},
    { EditorActionsIF.ALIGN_LEFT, "Align Left Sides", "Align Left Sides", 
            new Character('L'), KeyStroke.getKeyStroke(KeyEvent.VK_L, Event.CTRL_MASK)},
    { EditorActionsIF.ALIGN_RIGHT, "Align Right Sides", "Align Right Sides", 
            new Character('R'), KeyStroke.getKeyStroke(KeyEvent.VK_R, Event.CTRL_MASK)},
    { EditorActionsIF.ALIGN_TOP, "Align Tops", "Align Tops", 
            new Character('T'), KeyStroke.getKeyStroke(KeyEvent.VK_T, Event.CTRL_MASK)},
    { EditorActionsIF.ALIGN_BOTTOM, "Align Bottoms", "Align Bottoms", 
            new Character('B'), KeyStroke.getKeyStroke(KeyEvent.VK_B, Event.CTRL_MASK)},
    { EditorActionsIF.ALIGN_VERTICAL_CENTER, "Align Vertical Center", 
            "Align Vertical Center" },
    { EditorActionsIF.ALIGN_HORIZONTAL_CENTER, "Align Horizontal Center", 
            "Align Horizontal Center" },

    { EditorActionsIF.NEW_BUNDLER, "Add Bundler", "Adds a new bundler" },
    { EditorActionsIF.NEW_TEXT_COMMENT, "Add Text Comment", "Adds a text comment" },
    { EditorActionsIF.NEW_COLORED_BOX, "Add Background Highlight", 
            "Adds a background highlight" },
    { EditorActionsIF.EDIT_COMMENT, "Edit Comment...", "Edits the comment" },
    { EditorActionsIF.EDIT_MACROS, "Edit Macros...", "Edits macros" },
    { EditorActionsIF.BOX_SETTINGS, "Box Settings...", "Edits box settings" },
    { EditorActionsIF.VIEW_EXTERNAL, "External View", "External View" },
    { EditorActionsIF.VIEW_INTERNAL, "Internal View", "Internal View" },
    { EditorActionsIF.RESET_VIEW, "Reset View", "Reset View" },
    { EditorActionsIF.VALIDATE_BUNDLES,
            "Validate Bundle Connections", "Validate Bundle Connections" },
    { EditorActionsIF.SMOOTH_CONNS, "Smooth Connections","Smooth Connections" },
    { EditorActionsIF.TRIM_CONNS, "Trim Connections", "Trim Connections" },
    { EditorActionsIF.VIEW_BASE_BOX, "View Base Box", "View Base Box" },
    { EditorActionsIF.SHOW_MAIN, "Show Main Window", "Show Main Window" },
    { EditorActionsIF.EXPORT_MODELER_FILE, "Export Modeler File...", 
            "Export the file used in the simulation program" },
    { EditorActionsIF.SAVE, "Save", "Force save of this box" },
    { EditorActionsIF.SAVE_COPY_AS,
            "Save Copy As...", "Save box file to another name" },
    { EditorActionsIF.RELOAD, "Reload Node", "Reload Node from Disk" },
    { EditorActionsIF.CLOSE, "Close", "Close Window" },
    { EditorActionsIF.PRINT_TO_SCALE, "Print...", "Print using selected scale"},
    { EditorActionsIF.PRINT_FIT_PAGE,
            "Print - Fit in Page...", "Print - Fit in Page" },

    { EditorActionsIF.DEBUG, "Debug", "Debug" },
    { EditorActionsIF.LOCATE_FILE, "Locate Box File", "Locate Box File" },
    { EditorActionsIF.NODE_INFO,
            "Debug Information", "Show the node information" },
    { EditorActionsIF.NODE_EDIT, "Edit", "Edit", new Character('E')},
    { EditorActionsIF.NODE_ADD, "Add", "Add a node" },
    { EditorActionsIF.NODE_DELETE, "Delete", "Delete the node" },
    { EditorActionsIF.NODE_DUPLICATE, "Duplicate", "Duplicate the node" },
    { EditorActionsIF.NODE_RENAME, "Rename", "Rename the node" },
    { EditorActionsIF.NODE_SWAP, "Swap Nodes", "Swap the nodes." },

    { EditorActionsIF.SETTINGS, "Settings", "Edit the settings" },

    { EditorActionsIF.CENTER_PORTS, "Center Ports",
                                            "Set Center External View Ports" },
    { EditorActionsIF.ROTATE, "Rotate", "Rotate the icon" },
    { EditorActionsIF.ROTATE_NORTH,
            "Remove Rotation Operation", "Remove Rotation Operation" },
    { EditorActionsIF.ROTATE_SOUTH,
            "Set Rotation To 180 Degrees", "Set Rotation To 180 Degrees" },
    { EditorActionsIF.ROTATE_EAST,
            "Set Rotation To 90 Degrees CW", "Set Rotation To 90 Degrees CW" },
    { EditorActionsIF.ROTATE_WEST,
            "Set Rotation To 90 Degrees CCW","Set Rotation To 90 Degrees CCW" },
    { EditorActionsIF.SWAP, "Swap", "Swap" },
    { EditorActionsIF.SWAP_X,
            "Set To Swap Across X-Axis", "Set To Swap Across X-Axis" },
    { EditorActionsIF.SWAP_Y,
            "Set To Swap Across Y-Axis", "Set To Swap Across Y-Axis" },
    { EditorActionsIF.SWAP_NONE,
            "Remove Swap Operation", "Remove Swap Operation" },
    { EditorActionsIF.SYMMETRY, "Symmetry", "Symmetry" },
    { EditorActionsIF.SYMMETRY_X,
            "Set To Symmetry Across X-Axis", "Set To Symmetry Across X-Axis" },
    { EditorActionsIF.SYMMETRY_Y,
            "Set To Symmetry Across Y-Axis", "Set To Symmetry Across Y-Axis" },
    { EditorActionsIF.SYMMETRY_PLUS_XY, "Set To Symmetry Across +XY Line",
            "Set To Symmetry Across +XY Line" },
    { EditorActionsIF.SYMMETRY_MINUS_XY, "Set To Symmetry Across -XY Line",
            "Set To Symmetry Across -XY Line" },
    { EditorActionsIF.SYMMETRY_NONE,
            "Remove Symmetry Operation", "Remove Symmetry Operation" },

    { EditorActionsIF.PORT_MODIFY, "Modify", "Modify the port" },
    { EditorActionsIF.PORT_DELETE, "Delete Port", "Delete the port" },
    { EditorActionsIF.PORT_RENAME, "Rename", "Rename the port" },
    { EditorActionsIF.IMPORT_CHANNELS, "Import Channels", "Import Channels" },

    { EditorActionsIF.CONN_DELETE,
            "Delete Connection", "Delete the Connection" },
    { EditorActionsIF.CONN_SMOOTH, "Smooth", "Smooth" },
    { EditorActionsIF.CONN_CREATE_JUNCTION,
            "Create Junction", "Create a junction" },
    { EditorActionsIF.CONN_CREATE_BUNDLER,
            "Create Bundler", "Create a bundler" },
    { EditorActionsIF.CONN_TRIM, "Trim Connection", "Trim Connection" },
    { EditorActionsIF.SHOW_BUNDLE_CONTENT, "Show Bundle Content",
                                                       "Show Bundle Content" },

    { EditorActionsIF.JUNCTION_DELETE, "Delete Junction", "Delete Junction" },
    { EditorActionsIF.BUNDLER_DELETE, "Delete Bundler", "Delete Bundler" },
    { EditorActionsIF.BUNDLER_IO_RENAME,
            "Rename Bundler Secondary I/O", "Rename Bundler Secondary I/O" },
    { EditorActionsIF.BUNDLER_PRIMARY_IN_TO_SECONDARY,
            "Change Primary In to Secondary In",
            "Change Primary In to Secondary In" },
    { EditorActionsIF.BUNDLER_PRIMARY_OUT_TO_SECONDARY,
            "Change Primary Out to Secondary Out",
            "Change Primary Out to Secondary Out" },

    { EditorActionsIF.TEXT_FONT_SIZE, "Set Font Size", "Set Font Size" },
    { EditorActionsIF.TEXT_FONT_XSMALL, "Extra Small", "Extra Small" },
    { EditorActionsIF.TEXT_FONT_SMALL,  "Small", "Small" },
    { EditorActionsIF.TEXT_FONT_MEDIUM, "Medium", "Medium" },
    { EditorActionsIF.TEXT_FONT_LARGE,  "Large", "Large" },
    { EditorActionsIF.TEXT_FONT_XLARGE,  "Extra Large", "Extra Large" },
    { EditorActionsIF.TEXT_FONT_HEADING, "Heading", "Heading" },
    { EditorActionsIF.TEXT_VALUE, "Set Text Value", "Set Text Value" },
    { EditorActionsIF.TEXT_COLOR, "Set Text Color", "Set Text Color" },
    { EditorActionsIF.TEXT_COLOR_BLACK, "Black", "Black" },
    { EditorActionsIF.TEXT_COLOR_WHITE, "White", "White" },
    { EditorActionsIF.TEXT_COLOR_BLUE, "Blue", "Blue" },
    { EditorActionsIF.TEXT_COLOR_CYAN, "Cyan", "Cyan" },
    { EditorActionsIF.TEXT_COLOR_GREEN, "Green", "Green" },
    { EditorActionsIF.TEXT_COLOR_LIGHT_GRAY, "Light Gray", "Light Gray" },
    { EditorActionsIF.TEXT_COLOR_MAGENTA, "Magenta", "Magenta" },
    { EditorActionsIF.TEXT_COLOR_ORANGE, "Orange", "Orange" },
    { EditorActionsIF.TEXT_COLOR_PINK, "Pink", "Pink" },
    { EditorActionsIF.TEXT_COLOR_RED, "Red", "Red" },
    { EditorActionsIF.TEXT_COLOR_YELLOW, "Yellow", "Yellow" },
    { EditorActionsIF.TEXT_FONT_BOLD, "Toggle Bold", "Toggle Bold"}, 
    { EditorActionsIF.TEXT_FONT_UNDERLINE, "Toggle Underline", "Toggle Underline"},
    { EditorActionsIF.TEXT_FONT_ITALIC, "Toggle Italic", "Toggle Italic"},
     
    { EditorActionsIF.HILITE_COLOR_MODIFY, "Set Highlight Color", 
                                                        "Set Highlight Color" },
    { EditorActionsIF.HILITE_COLOR_BLUE, "Blue", "Blue" },
    { EditorActionsIF.HILITE_COLOR_CYAN, "Cyan", "Cyan" },
    { EditorActionsIF.HILITE_COLOR_GREEN, "Green", "Green" },
    { EditorActionsIF.HILITE_COLOR_LIGHT_GRAY, "Light Gray", "Light Gray" },
    { EditorActionsIF.HILITE_COLOR_MAGENTA, "Magenta", "Magenta" },
    { EditorActionsIF.HILITE_COLOR_ORANGE, "Orange", "Orange" },
    { EditorActionsIF.HILITE_COLOR_PINK, "Pink", "Pink" },
    { EditorActionsIF.HILITE_COLOR_RED, "Red", "Red" },
    { EditorActionsIF.HILITE_COLOR_YELLOW, "Yellow", "Yellow" },
    { EditorActionsIF.HILITE_COLOR_DARK_BLUE, "Dark Blue", "Dark Blue" },
    { EditorActionsIF.HILITE_COLOR_DARK_CYAN, "Dark Cyan", "Dark Cyan" },
    { EditorActionsIF.HILITE_COLOR_DARK_GREEN, "Dark Green", "Dark Green" },
    { EditorActionsIF.HILITE_COLOR_DARK_GRAY, "Dark Gray", "Dark Gray" },
    { EditorActionsIF.HILITE_COLOR_DARK_MAGENTA, "Dark Magenta", "Dark Magenta" },
    { EditorActionsIF.HILITE_COLOR_DARK_OLIVE_GREEN, "Dark Olive Green", "Dark Olive Green" },
    { EditorActionsIF.HILITE_COLOR_DARK_ORANGE, "Dark Orange", "Dark Orange" },
    { EditorActionsIF.HILITE_COLOR_DARK_RED, "Dark Red", "Dark Red" },
    { EditorActionsIF.HILITE_COLOR_DARK_YELLOW, "Dark Yellow", "Dark Yellow" },
    { EditorActionsIF.HILITE_LAYER_MODIFY, "Switch Layers", 
                                                        "Switch Layers" },
    { EditorActionsIF.HILITE_SEND_TO_BACK, "Send Object to the Back", 
                            "Send Object to the Back (of the existing layer)" },
    { EditorActionsIF.HILITE_SEND_TO_FRONT, "Send Object to the Front", 
                            "Send Object to the Front (of the existing layer)" }

    };

    public EditorResources () { super (); }

    public Object[][] getContents () { return editorResources; }
}

