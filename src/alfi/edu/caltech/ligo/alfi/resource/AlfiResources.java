package edu.caltech.ligo.alfi.resource;

import java.util.Locale;
import java.util.ResourceBundle; 

import java.awt.Event;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import edu.caltech.ligo.alfi.common.UIResourceBundle;
import edu.caltech.ligo.alfi.common.GuiConstants;

import edu.caltech.ligo.alfi.editor.EditorActionsIF;

import edu.caltech.ligo.alfi.summary.WindowIF;
import edu.caltech.ligo.alfi.summary.ActionsIF;
import edu.caltech.ligo.alfi.summary.AlfiMainFrameIF;

/**    
  * <pre>
  * Resource strings for implementing the Actions
  * </pre>
  *
  * each array is composed of :<BR>
  * index 0: Key <BR>
  * index 1: Name <BR>
  * index 2: Description <BR>
  * index 3: Mnemonic<BR>
  * index 4: KeyStroke<BR>
  * index 5: Primary Image<BR>
  * index 6: Rollover Image<BR>
  * index 7: Pressed Image<BR>

  * outside of the bounds can be used by individual windows.
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class AlfiResources extends UIResourceBundle {
  private static Object[][] alfiResources = {

    {AlfiMainFrameIF.PARENT_FRAME, "ALFI - AdLib Friendly Interface", "ALFI"},
    {ActionsIF.FILE, "File", "File", new Character('F')},
    {ActionsIF.NEW_BOX, "New Box . . .", "Create a new box file",
                                                           new Character('N')},
    {ActionsIF.LOAD, "Load . . .", "Load . . .", new Character('L')},
    {ActionsIF.RELOAD, "Reload Selected Node", "Reload Selected Node",
                                                           new Character('R')},
    {ActionsIF.UNLOAD, "Unload Selected Node", "Unload Selected Node",
                                                           new Character('U')},
    {ActionsIF.CREATE_UNIVERSAL_PRIMITIVE, "New UDP...", 
            "Create User-Defined Primitive", new Character('D')},
    {ActionsIF.OPEN_UNIVERSAL_PRIMITIVE, "Open UDP...", 
            "Open an Existing User-Defined Primitive", new Character('O')},
    {ActionsIF.SHOW_PREFERENCES, "Preferences . . .",
                                      "Preferences . . .", new Character('P')},
    {ActionsIF.SAVE, "Save", "Save", new Character('S')},
    {ActionsIF.FORCE_SAVE_ALL, "Force Save All", "Force Save All"},
    {ActionsIF.QUIT, "Quit", "Quit", new Character('Q')},
    {ActionsIF.EDIT, "Edit", "Edit", new Character('E')},
    {ActionsIF.SHOW_CLIPBOARD, "Show Clipboard",
                                         "Show Clipboard", new Character('B')},
    {ActionsIF.FIND, "Find", "Find", new Character('F')},
    {ActionsIF.VIEW, "View", "View", new Character('V')},
    {ActionsIF.PRIMITIVE_TB, "Primitive Toolbar", 
                          "Display the primitive toolbar", new Character('P')},
    {ActionsIF.OVERVIEW_WINDOW, "Vista", 
        "Show all open windows", new Character('I'), 
        KeyStroke.getKeyStroke(KeyEvent.VK_I, Event.CTRL_MASK)},
    {ActionsIF.OPTIONS, "Options", "Options", new Character('O')},
    {ActionsIF.TIP, "Tips", "Tips", new Character('T')},
    {ActionsIF.TIP_INHERITED, "  - Include Inherited Tips",
                             "  - Include Inherited Tips", new Character('I')},
    {ActionsIF.MACRO_INFO_IN_TREE, "Show Macro Info in Tree",
                             "Show Macro Info in Tree", new Character('T')},
    {ActionsIF.MACRO_INFO_ON_NODE, "Show Macro Info on Nodes",
                             "Show Macro Info on Nodes", new Character('N')},
    {ActionsIF.AUTO_SAVE, "Auto Save", "Auto Save", new Character('S')},
    {ActionsIF.CONN_ON_TOP, "Connections on Top", "Connections on Top", 
                                                           new Character('C')},
    {ActionsIF.RESTORE_WINDOWS, "Restore Windows", "Restore Windows", 
                                                           new Character('W')},
    {ActionsIF.TRIM_CONNECTIONS, "Auto Trim Connections",
                           "Auto Trim Connections", new Character('A')},
    {ActionsIF.PERSISTENT_CLIPBOARD, "Persistent Clipboard",
                                   "Persistent Clipboard", new Character('P')},
    {ActionsIF.HELP, "Help", "Help", new Character('H')},
    {ActionsIF.ABOUT, "About", "About", new Character('A')},
    {ActionsIF.UPDATES, "Updates", "Updates", new Character('U')},
    {ActionsIF.DOCUMENTATION, "Documentation", "Documentation", new Character('D')},
    {ActionsIF.BUG_REPORT, "Report Bugs", "Report Bugs", new Character('R')},

    {WindowIF.WINDOW, "Window", "Window layout/selection options", 
                                                           new Character('W')},

/*
    {WindowIF.CASCADE, "Cascade", "Arrange open windows in cascading fashion", 
                                                           new Character('C')},
    {WindowIF.LAYER, "Layer",
        "Maximize all open windows and layer them, one on top of the other", 
                                                           new Character('L')},
    {WindowIF.TILE, "Tile", "Arrange open windows so that they do not overlap",
                                                            new Character('T')},
*/

  };

  public AlfiResources () { super (); }

  public Object[][] getContents() { return alfiResources; }
}
