package edu.caltech.ligo.alfi.summary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.tools.*;

/**    
  *
  * <pre>


  * The Overview Dialog
  * </pre>
  *
  * @author  Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.AlfiFrame
  */
public class OverviewDialog extends JDialog{

    private Dimension m_screen_size;

    private AlfiDesktopPane m_overview;

    //**************************************************************************
    //********* Constructors ***************************************************
    public OverviewDialog (AlfiMainFrame _parent) {
        super(_parent, "Display All Open Windows", true); 

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        m_screen_size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(m_screen_size);
        setResizable(false);
        initialize();
    }

    /**
      * initializes this frame. This is called after
      * init() on this frame by the window manager.
      */
    public void initialize() {
        m_overview = new AlfiDesktopPane(this);
        getContentPane().add(m_overview);
        
    }


    public ALFINode getNodeToDisplay () {
        return m_overview.getNodeToDisplay();
    }


}
