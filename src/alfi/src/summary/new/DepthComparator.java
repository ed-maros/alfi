package edu.caltech.ligo.alfi.summary;

import java.util.*;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;

public class DepthComparator implements Comparator {

    static public final String DELIMITER = new String(".");   

    // Compare two elements
    public int compare (Object element1, Object element2) {
        String node_name1 = 
            (String) ((ALFINode) element1).generateFullNodePathName();
        String node_name2 = 
            (String) ((ALFINode) element2).generateFullNodePathName();

        StringTokenizer tokenizer1 = new StringTokenizer(node_name1, DELIMITER);
        StringTokenizer tokenizer2 = new StringTokenizer(node_name2, DELIMITER);
        
        int depth1 = tokenizer1.countTokens();
        int depth2 = tokenizer2.countTokens();
        
        if (depth1 < depth2) { 
            return -1;
        } else if (depth1 > depth2) {
            return 1;
        } else {
            // Sort by the name
            return node_name1.compareToIgnoreCase(node_name2);
        }
  }
}
