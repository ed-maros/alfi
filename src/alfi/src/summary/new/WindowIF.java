package edu.caltech.ligo.alfi.summary;

/**    
  *
  * <pre>
  * Interface for window related events.
  *
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * <P>
  */

public interface WindowIF {
    /** constant for the Window resources. */
	public static final String WINDOW = "windowif.window";
     

    /**
      * constant for the tile resources.
    public static final String TILE = "windowif.tile";   
    /**
      * constant for the layer resources.
    public static final String LAYER = "windowif.layer";
    /**
      * constant for the cascade resources.
    public static final String CASCADE = "windowif.cascade";

    /**
      *
      * forces the windows to tile.
	public void tile ();


    /**
      *
      * forces the windows to cascade.
	public void cascade ();

    /**
      *
      * forces the windows to layer.
	public void layer ();


      */
}
