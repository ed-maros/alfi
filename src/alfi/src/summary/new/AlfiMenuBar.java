package edu.caltech.ligo.alfi.summary;

import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.Box;

import edu.caltech.ligo.alfi.common.*;

/**    
  * <pre>
  * The menu bar for the main frame 
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see BasicMenuBar
  */
public class AlfiMenuBar extends BasicMenuBar { 
    private BasicMenu file_menu;
    private BasicMenu edit_menu;
    private BasicMenu view_menu;
    private WindowMenu window_menu;
    private BasicMenu options_menu;
    private BasicMenu help_menu;


    public AlfiMenuBar (Actions _actions) {
        super(_actions);

        file_menu = (BasicMenu) createMenu(ActionsIF.FILE);
        createMenuItem(file_menu, ActionsIF.NEW_BOX);
        file_menu.addSeparator();
        createMenuItem(file_menu, ActionsIF.LOAD);
        createMenuItem(file_menu, ActionsIF.RELOAD);
        createMenuItem(file_menu, ActionsIF.UNLOAD);
        file_menu.addSeparator();
        createMenuItem(file_menu, ActionsIF.SHOW_PREFERENCES);
        file_menu.addSeparator();
        createMenuItem(file_menu, ActionsIF.SAVE);
        createMenuItem(file_menu, ActionsIF.FORCE_SAVE_ALL);
        file_menu.addSeparator();
        createMenuItem(file_menu, ActionsIF.QUIT);
 
        edit_menu = (BasicMenu) createMenu(ActionsIF.EDIT); 
        createMenuItem(edit_menu, ActionsIF.SHOW_CLIPBOARD);
        edit_menu.addSeparator();
        createMenuItem(edit_menu, ActionsIF.FIND);

        view_menu = (BasicMenu) createMenu (ActionsIF.VIEW);
        createCheckBoxMenuItem(view_menu, ActionsIF.PRIMITIVE_TB).
                                                               setState(false);
        createMenuItem(view_menu, ActionsIF.OVERVIEW_WINDOW);

        options_menu = (BasicMenu) createMenu (ActionsIF.OPTIONS);
        createCheckBoxMenuItem(options_menu, ActionsIF.TIP).setState(true);
        createCheckBoxMenuItem(options_menu,
                               ActionsIF.TIP_INHERITED).setState(true);
        createCheckBoxMenuItem(options_menu,
                               ActionsIF.MACRO_INFO_IN_TREE).setState(false);
        createCheckBoxMenuItem(options_menu,
                               ActionsIF.MACRO_INFO_ON_NODE).setState(false);
        createCheckBoxMenuItem(options_menu,
                               ActionsIF.AUTO_SAVE).setState(false);
        createCheckBoxMenuItem(options_menu, ActionsIF.CONN_ON_TOP);
        createCheckBoxMenuItem(options_menu,
                               ActionsIF.RESTORE_WINDOWS).setState(false);
        createCheckBoxMenuItem(options_menu,
                            ActionsIF.TRIM_CONNECTIONS).setState(false);
        createCheckBoxMenuItem(options_menu,
                               ActionsIF.PERSISTENT_CLIPBOARD).setState(false);
        options_menu.addSeparator();
        createCheckBoxMenuItem(options_menu,
                               ActionsIF.AUTO_VALIDATE_NODES).setState(false);
        createMenuItem(options_menu, ActionsIF.VALIDATE_NODES);

        window_menu = createWindowMenu(); 
/*
        createMenuItem(window_menu, WindowIF.TILE);
        createMenuItem(window_menu, WindowIF.CASCADE);
        createMenuItem(window_menu, WindowIF.LAYER);
*/
        window_menu.addSeparator();

        help_menu = (BasicMenu) createMenu (ActionsIF.HELP);
        createMenuItem(help_menu, ActionsIF.ABOUT); 
        createMenuItem(help_menu, ActionsIF.UPDATES); 
        help_menu.addSeparator();
        createMenuItem(help_menu, ActionsIF.DOCUMENTATION); 
        createMenuItem(help_menu, ActionsIF.BUG_REPORT); 

        add(file_menu);
        add(edit_menu);
        add(view_menu);
        add(options_menu);
        add(window_menu);
        add(help_menu);
    }


    /**
      * Creates the Window Menu
      */
    public WindowMenu createWindowMenu () { 
		String name = null;
		String desc = null;
        final String key = new String(WindowIF.WINDOW);

		try {
			name = (String) getActions().getResources().
                   get(key,BasicResourceBundle.NAME);
		}
		catch (Exception ex) {
			//missing resource exception
			name = AlfiConstants.UNDEFINED;
		}
		
		WindowMenu rval = new WindowMenu(name);

		try {
			Character mnemonic =  null;
			mnemonic =  (Character) getActions().getResources().
                         get(key,UIResourceBundle.MNEMONIC);
			rval.setMnemonic(mnemonic.charValue());
		}
		catch (Exception ex) {
			//missing resource means no mnemonic
		}

		return rval;
    }

    /**
      * Returns the options menu for this menu bar.
      * @return the options menu for this menubar.
      */
    public BasicMenu getOptionsMenu () { return options_menu; }

    /**
      * Returns the window menu for this menu bar.
      * @return the window menu for this menubar.
      */
    public WindowMenu getWindowMenu () { return window_menu; }

    /**
      * Returns the file menu for this menu bar.
      * @return the file menu for this menubar.
      */
    public BasicMenu getFileMenu() { return file_menu; }

    /**
      * Returns the help menu for this menu bar.
      * @return the help menu for this menubar.
      */
    public BasicMenu getHelpMenuBar () { return help_menu; }

    /**
      * Returns the edit menu for this menu bar.
      * @return the edit menu for this menubar.
      */
    public BasicMenu getEditMenu () { return edit_menu; }

    /**
      * Returns the view menu for this menu bar.
      * @return the view menu for this menubar.
      */
    public BasicMenu getViewMenu () { return view_menu; }
}

