package edu.caltech.ligo.alfi.summary;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.editor.*;

    /** <pre>
      * A BasicMenu item for the application
      * </pre>
      *
      * @author Melody Araya
      * @version %I%, %G%
      */
public class WindowMenu extends BasicMenu
                        implements EditorWindowListener, ActionListener {
    private Hashtable m_editor_vs_menu_item_map;

        /** Constructor. */
    public WindowMenu (String _menu_name) {
        super(_menu_name);
        m_editor_vs_menu_item_map = new Hashtable();
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////// instance methods ////////////////////////////////////////////////

    public void addMenuItem(EditorFrame _editor) {
        if (! m_editor_vs_menu_item_map.containsValue(_editor)) {
            String item_text = _editor.getTitle();
            JMenuItem item = new JMenuItem(item_text);
            item.setBorderPainted (false);
            item.setContentAreaFilled(false);

            add(item);
            m_editor_vs_menu_item_map.put(item, _editor);

            item.addActionListener(this);
            _editor.addEditorWindowListener(this);
        }
    }	

    private void removeMenuItem(EditorFrame _editor) {
        if (m_editor_vs_menu_item_map.containsValue(_editor)) {
            for (Iterator i = m_editor_vs_menu_item_map.keySet().iterator();
                                                               i.hasNext(); ) {
                JMenuItem item = (JMenuItem) i.next();
                if (m_editor_vs_menu_item_map.get(item) == _editor) {
                    item.removeActionListener(this);    // cleanliness blahblah
                    remove(item);    // remove from menu
                    i.remove();      // remove from map
                    break;
                }
            }
        }
    }

    private void updateItemText(EditorFrame _editor) {
        if (m_editor_vs_menu_item_map.containsValue(_editor)) {
            for (Iterator i = m_editor_vs_menu_item_map.keySet().iterator();
                                                               i.hasNext(); ) {
                JMenuItem item = (JMenuItem) i.next();
                if (m_editor_vs_menu_item_map.get(item) == _editor) {
                    item.setText(_editor.getTitle());
                }
            }
        }
    }

    public void processEditorWindowEvent(EditorWindowEvent _event) {
        EditorFrame editor = (EditorFrame) _event.getSource();
        switch (_event.getID()) {
          case EditorWindowEvent.TITLE_CHANGE:
            updateItemText(editor);
            break;
          case EditorWindowEvent.WINDOW_CLOSING:
          case EditorWindowEvent.EDITOR_WINDOW_HIDING:
            removeMenuItem(editor);
            break;
          case EditorWindowEvent.EDITOR_WINDOW_ACTIVATED:
            addMenuItem(editor);
            break;
        }
    }

    public void actionPerformed(ActionEvent _event) {
        JMenuItem item = (JMenuItem) _event.getSource();
        EditorFrame editor = (EditorFrame) m_editor_vs_menu_item_map.get(item);
        if (editor.getExtendedState() == Frame.ICONIFIED) {
            editor.setExtendedState(Frame.NORMAL);
        }
        editor.toFront();
        editor.requestFocus();
/*
            // Kludge to fix focus problem
        editor.setVisible(false);
        editor.setVisible(true);
*/
    }

}
