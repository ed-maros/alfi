package edu.caltech.ligo.alfi.summary;

import java.util.EventObject;

import javax.swing.JCheckBoxMenuItem;

import edu.caltech.ligo.alfi.common.AlfiFrame;

/**    
  * <pre>
  * An event for depicting state changes in window menus.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class WindowMenuStateChangedEvent extends EventObject{
	private AlfiFrame m_frame;

	public WindowMenuStateChangedEvent (JCheckBoxMenuItem _src, 
                                        AlfiFrame _wnd){
		super(_src);
		m_frame = _wnd;
	} 

	public AlfiFrame getWindow (){
		return m_frame;
	}

}
 
