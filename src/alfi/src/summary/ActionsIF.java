package edu.caltech.ligo.alfi.summary;

/**    
  * <pre>
  * The interface for the parent frame actions
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public interface ActionsIF {

    /**
      * The keys for the actions
      */
    public static final String FILE= "actionsif.file";
    public static final String NEW_BOX = "actionsif.newbox";
    public static final String LOAD = "actionsif.load";
    public static final String RELOAD = "actionsif.reload";
    public static final String UNLOAD = "actionsif.unload";
    public static final String CREATE_UNIVERSAL_PRIMITIVE =
                                      "actionsif.create.universal.primitive";
    public static final String OPEN_UNIVERSAL_PRIMITIVE =
                                      "actionsif.open.universal.primitive";
    public static final String SHOW_PREFERENCES = "actionsif.show.preferences";
    public static final String SAVE = "actionsif.save";
    public static final String FORCE_SAVE_ALL = "actionsif.force.save.all";
    public static final String QUIT = "actionsif.quit";

    public static final String EDIT = "actionsif.edit";
    public static final String SHOW_CLIPBOARD = "actionsif.show.clipboard";
    public static final String FIND = "actionsif.find";

    public static final String VIEW = "actionsif.view";
    public static final String PRIMITIVE_TB = "actionsif.primitive.tb";
    public static final String OVERVIEW_WINDOW = "actionsif.overview.window";

    public static final String OPTIONS = "actionsif.options";
    public static final String TIP = "actionsif.tip";
    public static final String TIP_INHERITED = "actionsif.tip.inherited";
    public static final String MACRO_INFO_IN_TREE = "actionsif.macro.info.tree";
    public static final String MACRO_INFO_ON_NODE = "actionsif.macro.info.node";
    public static final String AUTO_SAVE = "actionsif.auto.save";
    public static final String CONN_ON_TOP = "actionsif.conn.on.top";
    public static final String RESTORE_WINDOWS = "actionsif.restore.wnds";
    public static final String TRIM_CONNECTIONS = "actionsif.trim.cons";
    public static final String PERSISTENT_CLIPBOARD =
                                              "actionsif.persistent.clipboard";
    public static final String DEBUG = "actionsif.debug";
    public static final String DEBUG_TO_STDERR = "actionsif.debug.to.stderr";

    public static final String HELP = "actionsif.help";
    public static final String ABOUT = "actionsif.about";
    public static final String UPDATES = "actionsif.updates";
    public static final String DOCUMENTATION = "actionsif.documentation";
    public static final String BUG_REPORT = "actionsif.bug.report";

    /**
      * the commands associated with the actions
      *
      */
    public void newBox ();
    public void load ();
    public void reload (boolean _b_inform_user);
    public void unload (boolean _b_inform_user);
    public void showPreferences ();
    public void save ();
    public void quit ();
    public void showClipboard ();
    public void find ();
    public void showPrimitiveToolbar (boolean show);
    public void showOverviewWindow ();
    public void updateTipDisplayStatus ();
    public void updateNodeMacroInfoDisplayStatus ();
    public void updateTreeMacroInfoDisplayStatus ();
    public void autoSmooth (boolean isAutoSmooth);
    public void transparentBitmaps (boolean isTransparent);
    public void transparentIcons (boolean isIcons);
    public void saveAlways (boolean flag);
    public void autoSave (boolean flag);
    public void drawConnectionsOnTop (boolean flag);
    public void restoreWindows (boolean flag);
    public void setAutoTrimConnections (boolean flag);
    public void setPersistentClipboard (boolean flag);
    public void debugALFI (boolean flag);
    public void debugToStdErr (boolean flag);
    public void helpAbout ();
    public void helpUpdates (String _version_last_used);
    public void helpDocumentation ();
    public void helpReportBugs ();
}

