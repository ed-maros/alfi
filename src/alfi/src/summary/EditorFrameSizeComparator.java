package edu.caltech.ligo.alfi.summary;

import java.util.*;
import edu.caltech.ligo.alfi.editor.EditorFrame;

public class EditorFrameSizeComparator implements Comparator {

    // Compare two elements
    public int compare(Object element1, Object element2) {
        EditorFrame editor1 = (EditorFrame) element1;
        EditorFrame editor2 = (EditorFrame) element2;

        int side_1 = Math.max((editor1.getWidth()/AlfiDesktopPane.FRAME_UNITS), 
                             (editor1.getHeight()/AlfiDesktopPane.FRAME_UNITS));
        int side_2 = Math.max((editor2.getWidth()/AlfiDesktopPane.FRAME_UNITS),
                             (editor2.getHeight()/AlfiDesktopPane.FRAME_UNITS));
        
        if (side_1 < side_2) { 
            return -1; 
        }
        else if (side_1 > side_2) { 
            return 1; 
        }
        else { 
            int area_1 = editor1.getWidth() * editor1.getHeight();
            int area_2 = editor2.getWidth() * editor2.getHeight();
        
            if (area_1 < area_2) { 
                return -1; 
            }
            else if (area_1 > area_2) { 
                return 1; 
            }
            else { 
                return 0; 
            }
            
        }
    }

}       

 
