package edu.caltech.ligo.alfi.summary;

/**    
  * <pre>
  * A Listener for WindowMenuChange events.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public interface WindowMenuListener{
	public void windowMenuStateChanged (WindowMenuStateChangedEvent 
                                            _event);
}
