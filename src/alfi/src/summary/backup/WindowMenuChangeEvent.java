package edu.caltech.ligo.alfi.summary;

import java.util.EventObject;

import edu.caltech.ligo.alfi.common.AlfiFrame;

/**    
  * <pre>
  * The event that is fired when a WindowMenu is changed.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class WindowMenuChangeEvent extends EventObject{

	private boolean m_adding;
	private String m_menu_text;
	private WindowMenuListener m_wnd_listener;	

	public WindowMenuChangeEvent (AlfiFrame _src, 
                                  WindowMenuListener _wnd_listener, 
                                  boolean _adding, 
                                  String _menu_text){ 
		super(_src);
		m_adding = _adding;
		m_menu_text = _menu_text;
		m_wnd_listener = _wnd_listener;
	} 

	public WindowMenuChangeEvent (AlfiFrame _src){
		super(_src);
		m_adding = false;
		m_menu_text = null;
	}

	public WindowMenuListener getWindowMenuListener (){
		return m_wnd_listener;
	}

	public boolean adding (){
		return m_adding;
	}

	public String getMenuText (){
		return m_menu_text;
	}
}
 
