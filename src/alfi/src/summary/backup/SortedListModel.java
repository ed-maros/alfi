package edu.caltech.ligo.alfi.summary;

import javax.swing.*;
import java.util.*;

public class SortedListModel extends AbstractListModel {

    // Define a SortedSet
    TreeSet m_model;

    public SortedListModel () {
        // Create a TreeSet
        // Store it in SortedSet variable
        m_model = new TreeSet();
    }

    public SortedListModel (Comparator _comparator) {
        // Create a TreeSet using the passed comparator
        m_model = new TreeSet(_comparator);
    }

    // ListModel methods
    public int getSize () {
        // Return the model size
        return m_model.size();
    }

    public Object getElementAt (int _index) {
        // Return the appropriate element
        return m_model.toArray()[_index];
    }

    // Other methods
    public void add (Object _element) {
        if (m_model.add(_element)) {
            fireContentsChanged(this, 0, getSize());
        }
    }

    public void addAll (Object _elements[]) {
        Collection c = Arrays.asList(_elements);
        m_model.addAll(c);
        fireContentsChanged(this, 0, getSize());
    }
    
    public Object[] toArray () {
        return m_model.toArray(); 
    } 

    public void clear () {
        m_model.clear();
        fireContentsChanged(this, 0, getSize());
    }

    public boolean contains (Object _element) {
        return m_model.contains(_element);
    }

    public Object firstElement () {
        // Return the appropriate element
        return m_model.first();
    }

    public Iterator iterator () {
        return m_model.iterator();
    }

    public Object lastElement() {
        // Return the appropriate element
        return m_model.last();
    }

    public boolean removeElement(Object _element) {
        boolean removed = m_model.remove(_element);
        if (removed) {
            fireContentsChanged(this, 0, getSize());
        }
        return removed;   
    }

}
