package edu.caltech.ligo.alfi.summary;


import java.awt.*;
import java.awt.event.*;
import java.awt.dnd.*;
import javax.swing.*;
import javax.swing.event.*;

import com.nwoods.jgo.*;


/**
 * A overview window, displaying all the EditorFrames view at a
 * reduced scale, and allowing navigation by the user.
 * dragging the rectangle or clicking.
 */

public class AlfiOverview extends JGoOverview {

    String m_tooltip_text;

    public AlfiOverview (String _tooltip) {
        super();
        ToolTipManager.sharedInstance().registerComponent(this);
        m_tooltip_text = _tooltip;

        setToolTipText(_tooltip);
     }

    /**
     * Show tooltips, so users might get a clue about which window is which
     * even though the objects are so small.
     */
    public String getToolTipText(MouseEvent evt)
    {    
        if (getObserved() == null) return null;

/*
        Point p = evt.getPoint();
        convertViewToDoc(p);

        JGoObject obj = getObserved().pickDocObject(p, false);

        while (obj != null) {
            String tip = obj.getToolTipText();
            if (tip != null) {
                return tip;
            } else {
            obj = obj.getParent();
            }
        }

*/
        return m_tooltip_text;
    }
}
