package edu.caltech.ligo.alfi.summary;

import java.io.*;
import java.util.*;
import java.net.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.editor.EditorFrame;
import edu.caltech.ligo.alfi.tools.*;

/**    
  * <pre>
  * The desktop pane that displays all the windows tiled and
  * proportional to the full sized window.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class AlfiDesktopPane extends JDesktopPane 
                             implements InternalFrameListener { //, MouseListener {


    /** A map of JInternalFrames vs ALFINode */ 
    private HashMap m_frame_vs_node_map; 

    private ALFINode m_node_to_display;

    private JDialog m_parent;

    private int m_window_count;
    private int m_rows;
    private int m_columns;
                    
    private final Insets m_insets;

    ////////////////////////////////////////////////////////////////////////////
    ////////// constructors ////////////////////////////////////////////////////

    public AlfiDesktopPane (JDialog _parent) {
        super();

        putClientProperty("JDesktopPane.dragMode", "outline");

        m_parent = _parent;
        m_window_count = 0;
        m_node_to_display = null;

        m_frame_vs_node_map = new HashMap();
        m_insets = Alfi.getMainWindow().getInsets();

        ToolTipManager.sharedInstance().setEnabled(true);

        EditorFrame[] editors = Alfi.getTheNodeCache().getEditSessions();

        for (int i = 0; i < editors.length; i++) {
            if (editors[i].isShowing()) {
                m_window_count++;
            }
        }
        int square_root = ( int )Math.sqrt(m_window_count);
        m_rows = square_root;
        m_columns = m_window_count/m_rows;
        if (m_window_count % (m_rows * m_columns) > 0) {
            m_columns++;
        }
        Dimension content_pane = Toolkit.getDefaultToolkit().getScreenSize(); 

	    Dimension window_size = new Dimension();
        double window_size_width = 
            (content_pane.getWidth() - m_insets.left - m_insets.right) / ( double )m_columns; 
        double window_size_height = 
            (content_pane.getHeight() - m_insets.top - m_insets.bottom)/ ( double )m_rows; 
        window_size.setSize(window_size_width, window_size_height);
        //System.out.println("**********************************************************");
        //System.out.println("m_insets " + m_insets);
        //System.out.println("contentpane size " + content_pane);
        //System.out.println("cols "+ m_columns+ " rows " + m_rows +  " window size " + window_size);

        createFrames(window_size);

    }

    private void createFrames (Dimension _window_size) {
        EditorFrame[] editor_frames = Alfi.getTheNodeCache().getEditSessions();
        
        Point location = new Point();

        int row = 0;
        int column = 0;
        int windows_created = 0;    

        Dimension view_size = _window_size;

        if (m_window_count > 0) {
            double view_width = _window_size.getWidth() - m_insets.left - m_insets.right; 
            double view_height = _window_size.getHeight() - m_insets.top - m_insets.bottom;
            view_size.setSize(view_width, view_height);
        }

        for (int i = 0; i < editor_frames.length; i++) {
            int inset_width = 0;
            int inset_height = 0;

            if (editor_frames[i].isShowing()) {
                if (m_columns == 0) {
                    column = windows_created;
                }
                else {
                    row = windows_created / m_columns;
                    column = windows_created % m_columns;
                }
                if (column > 0) {
                    inset_width += m_insets.right;
                    //if (column > 1) {
                        inset_width += m_insets.left;
                    //}
                }
                if (row > 0) {
                    inset_height += m_insets.bottom;
                    //if (row > 1) {
                        inset_height += m_insets.top;
                    //}
                }
                location.setLocation((inset_width + _window_size.width) * column, 
                                     (inset_height + _window_size.height) * row);
                //System.out.println("\nRow " + row + " column " + column);
                //System.out.println(editor_frames[i].getName());
                //System.out.println("location " + location);
                //System.out.println("view size " + view_size);
                //System.out.println("inset width " + inset_width + " inset height " + inset_height);

	            createFrame(editor_frames[i], view_size, location);
                windows_created++;
                
            }
        }
          
    }

    private void createFrame (EditorFrame _editor, Dimension _max_view_size, Point _location) {

        final JInternalFrame frame = new JInternalFrame(_editor.getName(), 
            false, false, false);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addInternalFrameListener(this);

        ALFINode node = _editor.getNodeInEdit();
        final JGoOverview overview = new AlfiOverview(node.generateFullNodePathName());  
        overview.setObserved(_editor.getView());

        // Determine the optimal scale for the overview
        double scale = 1.0d;
        double width_scale = 1.0d;
        double height_scale = 1.0d;

        Dimension view_size = _editor.getView().getSize();

        double view_width = view_size.getWidth(); 
        double view_height = view_size.getHeight();
        double internal_view_width = _max_view_size.getWidth();  
        double internal_view_height = _max_view_size.getHeight(); 
        
        if (view_width  > internal_view_width) {
            width_scale = internal_view_width/view_width;
        }
        if (view_height > internal_view_height) { 
            height_scale = internal_view_height/view_height;
        }
        
        scale = Math.min(width_scale, height_scale);
    
        // Adjust the frame, add the m_insets
/*
        double internal_frame_width = (view_width * scale) + 7; 
        double internal_frame_height = (view_height * scale) + 30;
*/
        double internal_frame_width = (view_width * scale) + m_insets.left + m_insets.right;
        double internal_frame_height = (view_height * scale) + m_insets.top + m_insets.bottom;
        overview.setScale(scale);

        frame.getContentPane().add(overview, BorderLayout.CENTER);

        Dimension frame_size = new Dimension();
        frame_size.setSize(internal_frame_width,  internal_frame_height);
        frame.setSize(frame_size);
        frame.setLocation(_location);
        //System.out.println(" internal frame size " + frame_size);
        
        m_frame_vs_node_map.put(frame, node);
        add(frame);
        frame.show();
        
    }


    public ALFINode getNodeToDisplay () {
        return m_node_to_display;
    }

    //////////////////////////////////////////////////////////
    ////////// InternalFrameListener methods//////////////////
    //////////////////////////////////////////////////////////
    public void internalFrameClosing(InternalFrameEvent e) { }

    public void internalFrameClosed(InternalFrameEvent e) { }

    public void internalFrameOpened(InternalFrameEvent e) { }

    public void internalFrameIconified(InternalFrameEvent e) { }

    public void internalFrameDeiconified(InternalFrameEvent e) { }

    public void internalFrameActivated(InternalFrameEvent e) {
        m_node_to_display = 
            ( ALFINode ) m_frame_vs_node_map.get(e.getInternalFrame());
        m_parent.setVisible(false);
    }

    public void internalFrameDeactivated(InternalFrameEvent e) { }


}     

