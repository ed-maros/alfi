package edu.caltech.ligo.alfi.summary;

import java.util.*;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;

public class ReverseNameComparator implements Comparator {

    static public final String DELIMITER = new String(".");   
    static public final int MAX_DEPTH = 10;

    // Compare two elements
    public int compare(Object element1, Object element2) {
        String node_name1 = 
            (String) ((ALFINode) element1).generateFullNodePathName();
        String node_name2 = 
            (String) ((ALFINode) element2).generateFullNodePathName();

        ArrayList node1 = fillWithTokens(node_name1);
        ArrayList node2 = fillWithTokens(node_name2);
    
        int result = 0;
        int num_tokens1 = node1.size() - 1;
        int num_tokens2 = node2.size() - 1;

        while (result == 0) {
            String name1 = (String) node1.get(num_tokens1--);
            String name2 = (String) node2.get(num_tokens2--);

            result = name1.compareToIgnoreCase(name2);

            if (result == 0) {  
                if (num_tokens1 < 0) {
                    result = -1;
                } else if (num_tokens2 < 0) {
                    result = 1;
                }
            }
        }
        return result;
    }

    private ArrayList fillWithTokens (String _node_name) {
        StringTokenizer tokenizer = new StringTokenizer(_node_name, DELIMITER);
        ArrayList name_tokens = new ArrayList();

        int ii = 0;
        while (tokenizer.hasMoreElements()) {
            String name = (String) tokenizer.nextElement();
            name_tokens.add(ii++, name);
        }
        
        return name_tokens;
    } 
}
