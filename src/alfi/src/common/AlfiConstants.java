package edu.caltech.ligo.alfi.common;

import java.lang.String;
/**    
  *
  * <pre>
  * Constants required for the application
  *
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class AlfiConstants{
    /**
      * Text for UNDEFINED resources keys
      */
    public static final String UNDEFINED = new String("Undefined");
    /**
      * Text for EMPTY resources keys
      */
    public static final String EMPTY = new String("");
    /**
      * Default dialog title 
      */
    public static final String DEFAULT_DIALOG_TITLE = 
        new String("ALFI");
    /**
      * Default new box name 
      */
    public static final String DEFAULT_BOX_TITLE = 
        new String("Untitled");
    /**
      * Default file suffix
      */
    public static final String DEFAULT_FILE_SUFFIX = 
        new String("box");
    /**
      * Default file descriptor
      */
    public static final String DEFAULT_FILE_DESCRIPTOR = 
        new String("Box files");
}
