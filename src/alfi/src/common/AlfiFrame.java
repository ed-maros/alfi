package edu.caltech.ligo.alfi.common;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.summary.*;

/**    
  * <pre>
  * The frame for all windows in this application
  * </pre>

  * @author Melody Araya
  * @version %I%, %G%
  */
public abstract class AlfiFrame extends JFrame implements FocusListener {

    /** Flag for informational status. */
    protected static final int INFO = 0;
    /** Flag to indicate a warning. */
    protected static final int WARN = 1;
    /** Flag to indicate an error. */
    protected static final int ERROR = 2;
    /** Flag to indicate a script failure. */
    protected static final int FAILURE = 3;
    /** Prefixes for different types of status messages. */
    private static final String[] m_status_prefix =
        { "", "Warning: ", "Error: ", "Failure: " };
 
    private Dimension m_default_window_size = GuiConstants.DEFAULT_WINDOW_SIZE;
    private StatusBar m_status_bar;
    private AlfiFrame m_parent_frame;
    private JPanel m_working_panel;
    private Cursor m_wait_cursor;
    private Cursor m_default_cursor;
    private boolean m_closing = false;
    private boolean m_waiting = false;

    protected UIResourceBundle m_resources;
    protected Actions m_actions;
    protected JPopupMenu m_popup_menu = null;
    protected String m_title;

    /**
      *
      * Constructor takes the titile and the ResourceBundle that it will use.
      *
      * @see edu.caltech.ligo.alfi.common.UIResourceBundle
      */
    public AlfiFrame (AlfiFrame _parent, String _title,
                                         UIResourceBundle _resources) {
        super(_title);

        m_parent_frame = _parent;

        // The title is saved here because the default implementation
        // of JFrame does not display the full title passed in.
        // For instance, instead of Han2k.2kDetector, it only displays
        // 2kDetector.
        m_title = new String(_title);

        m_resources = _resources;
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        m_wait_cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);    
        m_default_cursor = Cursor.getDefaultCursor();

        m_status_bar = new StatusBar(_title,
            getStatusTextMinimumWidth());

        m_working_panel = new JPanel(new BorderLayout(), true);
        getContentPane().add(m_working_panel, BorderLayout.CENTER);
        getContentPane().add(m_status_bar, BorderLayout.SOUTH);

        addFocusListener(this);
    }

    /**
      * Gets the "working" panel
      */
    public JPanel getWorkingPanel() {
        return m_working_panel;
    }

    /**
      * Sets the frame's title as the default text
      */
    public String getDefaultStatusText () {
        return (AlfiConstants.EMPTY);
    }

    /**
      * Assigns the text for the status bar
      */
    public void setStatusText (String _text) {
        setStatusText(_text, INFO);
    }

    /**
      * Assigns the text for the status bar
      */
    public synchronized void setStatusText (String _text, final int _type) {
        final String text = m_status_prefix[_type] + _text;
        
        m_status_bar.setStatusText(text);
    }

    /**
      * Assigns the window size to return to after tiling or some
      * other auto-arrangement of windows.
      */
    public void setDefaultWindowSize (Dimension _window_size) {
        m_default_window_size = _window_size;
    }

        /** _position_string must be \d+x\d+x\d+x\d+ (i.e., xxyxwxh).  _offset
          * is used for variations due to window managers, etc.  Returns true
          * if _position_string is valid (and is thus used to restore the window
          * position.
          */
    public boolean restorePosition(String _position_string, Point _offset) {
        if (_position_string == null) { return false; }

        if (_offset == null) { _offset = new Point(0, 0); }
        String[] parts = _position_string.split("x");
        if (parts.length == 4) {
            int[] specs = { -1, -1, -1, -1 };
            for (int i = 0; i < 4; i++) {
                try { specs[i] = Integer.parseInt(parts[i]); }
                catch(NumberFormatException _nfe) {
                    specs = null;
                    break;
                }
            }

            if (specs != null) {
                int x = specs[0] + _offset.x;
                int y = specs[1] + _offset.y;
                int w = specs[2];
                int h = specs[3];
                setBounds(x, y, w, h);
                return true;
            }
        }
        return false;
    }

    /**
      * initializes this frame. This is called after
      * init() on this frame by the window manager.
      */
    public void initializeFrame () {
        m_actions = initializeActions();
    }

    public Dimension getPreferredSize () {
        return GuiConstants.DEFAULT_WINDOW_SIZE;
    }

    protected int getStatusTextMinimumWidth (){
        return GuiConstants.MAXIMUM_STATUS_WIDTH;
    }

    public void centerFrame () {
        Dimension screen_size = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frame_size = getSize(); 
        int x = (screen_size.width - frame_size.width)/2;
        int y = (screen_size.height - frame_size.height)/2;
        Rectangle bounds = getBounds();
        setBounds(x, y, frame_size.width, frame_size.height);
    }
        
    public void setParentFrame (AlfiFrame _parent_frame){
        m_parent_frame = _parent_frame;
    }

    protected abstract Actions initializeActions ();

    public AlfiFrame getParentFrame (){
        return m_parent_frame;
    }
    
    public UIResourceBundle getResources (){
        return m_resources;
    }

        /** Reset the window size to its current default size. */
    public void resetSize () {
        setSize(m_default_window_size);
    }

    public StatusBar getStatusBar (){
        return m_status_bar;
    }

    public String getTitle (){
        return new String(m_title);
    }

    public void setFrameTitle (String _new_title){
        m_title = _new_title;
        setTitle(m_title);
    }

    protected void setDefaultPopupMenu () {
    }

    public JPopupMenu getDefaultPopupMenu () {
        return m_popup_menu;
    }

    public void close() {
        setVisible(false);
        dispose();
    }

    public Actions getActions (){
        return m_actions;
    }

    public void exit () {
        //stubbed out 
    }

    /////////////////////////////////////////////
    // FocusListener methods ////////////////////

    public void focusGained(FocusEvent _event) { ; }

    public void focusLost(FocusEvent _event) { ; }
}     
