package edu.caltech.ligo.alfi.common;

import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;

/**    
  * <pre>
  * A basic javax.swing.Menu with BasicAction and BasicResource support.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.BasicActionIF
  * @see edu.caltech.ligo.alfi.common.BasicResourceBundle
  */
public interface BasicMenuIF  {

    /**
      * A maping key for property change events.
      */
	public static final String ENABLED = "enabled";

    /**
      * A maping key for property change events.
      */
	public static final String DISABLED = "disabled";

    /**
      * Adds the passed action to this menu.
      * The action contains all needed information and binds all necessary 
      * listeners.
      *
      * @param BasicActionIF _action - the action to assign to the menu.
      * @return JCheckBoxMenuItem - the completed CheckBoxMenuItem
      * @see edu.caltech.ligo.alfi.common.BasicActionIF 
      */
	public JCheckBoxMenuItem addCheckBoxMenuItem (BasicActionIF _action);

    /**
      * Adds the passed action and associated attributes to
      * a standard JMenuItem. All necessary listeners are bound.
      *
      * @param BasicActionIF _action - the action used to enable the JMenuItem.
      * @return JMenuItem - the completed JMenuItem.
      *
      * @see javax.swing.JMenu
      */
	public JMenuItem add (BasicActionIF _action);

    /**
      * add the MenuItem specified by the action in the specified position.
      *
      * @see javax.swing.JMenu
      */
	public JMenuItem insert (BasicActionIF _action, int _position);

    /**
      * Sets associated menu icon to the value (true or false) of 
      * passed constraint. 
      *
      */
	public void setMenuItemIconsVisible (Actions action, boolean visible);
}
