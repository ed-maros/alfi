package edu.caltech.ligo.alfi.common;

import java.lang.String;
/**    
  *
  * <pre>
  * Fonts used in ALFI
  *
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class AlfiFonts {

    public static final int FONT_XSMALL      = 0;
    public static final int FONT_SMALL       = 1;
    public static final int FONT_MEDIUM      = 2;
    public static final int FONT_LARGE       = 3; 
    public static final int FONT_XLARGE      = 4; 
    public static final int FONT_HEADING     = 5; 

    public static final int FONT_LAST        = 6;

    public static final int[] FONT_BY_SIZE_ARRAY =
    { 10, 12, 14, 16, 18, 20 };

    // Font styles available
    public static final String FONT_STYLE_BOLD   = "BoldFont";
    public static final String FONT_STYLE_ITALIC = "ItalicFont";
    public static final String FONT_STYLE_UNDERLINED = "UnderlinedFont" ;
    
    public static int getFontSize (int _font_id) {
        if  ((_font_id >= 0) && (_font_id < FONT_LAST)) {
            return FONT_BY_SIZE_ARRAY[_font_id];
        }
        else {
            return FONT_BY_SIZE_ARRAY[FONT_MEDIUM];
        }
    }

}
