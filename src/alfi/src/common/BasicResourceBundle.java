package edu.caltech.ligo.alfi.common;

import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.MissingResourceException;

/**    
  * <pre>
  * The abstract implementation of the BasicResourceBundle interface.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.UIResourceBundle
  */

public abstract class BasicResourceBundle extends ResourceBundle {
    /**
      * a useful string for locating the system variable $JAVA_HOME.
      *
      */
	protected static final String javahome  = System.getProperty("java.home");
    /**
      * This position is reserved for the key of this
      * resources reference. This should be a String.
      */ 
    public static final int KEY = 0;
    /**
      * This position is reserved for the name of this
      * resources reference. This should be a String.
      */ 
    public static final int NAME = 1;
    /**
      * This position is reserved for a description or set of possible arguments for this
      * resources reference. This should be a String.
      */ 
    public static final int DESCRIPTION = 2;

	private BasicResourceBundle parentBundle;

    /**
      * Top Level constructor. All desendent classes must add this as 
      * or some decendent as the parent.
      * 
      * @see java.util.ResourceBundle#setParent(ResourceBundle)
      */
	public BasicResourceBundle () {
		super();
		parentBundle = null;
	}

	protected void setParent (ResourceBundle _parentBundle) {
		try {
			parentBundle = (BasicResourceBundle)_parentBundle;
            super.setParent(_parentBundle);
		}
		catch (Exception ex) {}
	}
		
	
    /**
      * Returns an Object[] referenced by the passed key. This method exists 
      * for historical reasons. This requires the caller of a
      * BasicResourceBundle to cast the retun to an Object[]. 
      * The public method <CODE>getValueList</CODE> is the method that 
      * should be called.
      *
      * @param String _key - the key to return values for.
      * @return Object - actually an Object[]. 
      *                  This is the complete list of values stored.
      * @exception MissingResourceException - 
      *                  If there is no mapping to the specified key.
      *
      * @see #getValueList
      */
	protected final Object handleGetObject (String _key) 
	throws MissingResourceException {
		Object[][] cts = getContents();
		for (int ii = 0;ii < cts.length;ii++) {
			if (_key.equals (cts[ii][BasicResourceBundle.KEY])) {
				return cts[ii];
			}
		}

		if (parentBundle != null) {
			return parentBundle.getObject(_key);
		}
		else {
			return null;
		}
	}

    /**
      * Returns an java.util.Enumeration of all of the Keys in this 
      * ResourceBundle.
      *
      * @see java.util.ResourceBundle
      */
	public final Enumeration getKeys () {
		try {
			Object[][] cts = getContents();
			Vector rval = new Vector (cts.length);
			for (int ii = 0;ii < cts.length;ii++) {
				rval.add (cts[ii][BasicResourceBundle.KEY]);
			}
			return rval.elements();
		}
		catch (Exception ex) {
			return new StringTokenizer("");
		}
	}
				
    /**
      * Returns a java.util.List of the values associated with the passed key.
      *
      * @param String _key - the the key whose values are to be returned.
      * @return java.util.List - The list with the (possibly null) values stored
      * @exception MissingResourceException - if the mapping doesn't exist.
      *
      * @see java.util.List
      */
	public final List getValueList (String _key)
	throws MissingResourceException {
		try {
			Object[] vals = (Object[])getObject(_key);
			ArrayList rval = new ArrayList (vals.length);
			for (int ii = 0;ii < vals.length;ii++) {
				rval.add (vals[ii]);
			}
			return rval;
		}
		catch (Exception ex) {
			if (ex instanceof MissingResourceException) {
				throw (MissingResourceException)ex;
			}
			throw new MissingResourceException (ex.getMessage(),
                                                getClass().getName(),_key);
		}
	}

	
    /**
      * Returns the resource at the specified index of the given key.
      *
      * @param String _key - the set of resources to retrieve.
      * @param int _value - the index of the resource to be returned.
      * @return Object - the resource itself.
      * @exception MissingResourceException - If there are no mappings.
      */
	public final Object get (String _key, int _value)
	throws MissingResourceException {
		try {
			Object[] rval = (Object[])getObject(_key);
			return rval[_value];
		}
		catch (Exception ex) {
			if (ex instanceof MissingResourceException) {
				throw (MissingResourceException)ex;
			}
			throw new MissingResourceException ("failed to locate resource", 
                                                 getClass().getName(),_key);
		}
	}

    /**
      * Accessor for returning the NAME value.
      * 
      *
      * @param _key is the key to the contents of this resource bundle.
      * @return is presumed to be a string value located in this position.
      * @exception MissingResourcesException is the resource is not available.
      * @exception ClassCastException is the resource is not a String.
      */
	public String getName (String _key) 
	throws MissingResourceException, ClassCastException {
		return (String)get (_key, NAME);
	}

    /**
      * Accessor for returning the DESCRIPTION value.
      * 
      *
      * @param _key is the key to the contents of this resource bundle.
      * @return is presumed to be a string value located in this position.
      * @exception MissingResourcesException is the resource is not available.
      * @exception ClassCastException is the resource is not a String.
      */
	public String getDescription (String _key)
	throws MissingResourceException, ClassCastException {
		return (String)get(_key,DESCRIPTION);
	}

	public abstract Object[][] getContents ();
}
