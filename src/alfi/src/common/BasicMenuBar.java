package edu.caltech.ligo.alfi.common;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.KeyStroke;


/**    
  * <pre>
  * The basic menu bar for the application
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see javax.swing.JMenuBar
  */
public class BasicMenuBar extends JMenuBar{ 

	private Actions m_actions;

    /**
      * Constructor only requires Actions.
      *
      * @see edu.caltech.ligo.alfi.common.Actions
      */
	public BasicMenuBar (Actions _actions){
		super();
		m_actions = _actions;
	}

    /**
      * returns the Actions associated with this BasicMenuBar.
      *
      * @see edu.caltech.ligo.alfi.common.Actions
      */
	public Actions getActions (){
		return m_actions;
	}

    /**
      * Protected accesor for creating a Menu.
      * @param _key - the key that maps to the resource 
      * discriptions.
      * @return javax.swing.JMenu
      */
	protected JMenu createMenu (String _key){
		String name = null;
		String desc = null;

		try {
			name = (String) m_actions.getResources().
                   get(_key,BasicResourceBundle.NAME);
		}
		catch (Exception ex) {
			//missing resource exception
			name = AlfiConstants.UNDEFINED;
		}
		

		BasicMenu rval = new BasicMenu(name);

		try {
			Character mnemonic =  null;
			mnemonic =  (Character) m_actions.getResources().
                         get(_key,UIResourceBundle.MNEMONIC);
			rval.setMnemonic(mnemonic.charValue());
		}
		catch (Exception ex) {
			//missing resource means no mnemonic
		}

		return rval;
	}

    /**
      *
      * Protected accessor method for creating a JMenuItem
      *
      * @param _menu The menu interface to add a Menu item to.
      * @return The created and bound menu item.
      * @see javax.swing.JMenuItem
      */
	protected JMenuItem createMenuItem (BasicMenuIF _menu, String _key){
		BasicActionIF action = m_actions.getAction(_key);

		JMenuItem rval = _menu.add(action);
		try {
			Character mnemonic =  null;
			mnemonic =  (Character) m_actions.getResources().
                         get(_key,UIResourceBundle.MNEMONIC);
			rval.setMnemonic(mnemonic.charValue());
		}
		catch (Exception ex) {
			//missing resource means no mnemonic
		}

		try {
			KeyStroke keyStroke = (KeyStroke) m_actions.getResources().
                                  get(_key,UIResourceBundle.KEY_STROKE);
			rval.setAccelerator(keyStroke);
		}
		catch (Exception ex) {
			//missing resource means no mnemonic
		}
		return rval;
	}

    /**
      * creates a Menu item based on the passed action key.
      * THe menu item is inserted in the menu at the specified position.
      *
      * @param _menu - the menu to inset the new item into.
      * @param _key - The key for the mapped actions.
      * @param _pos - the position in the menu to insert the item.
      * @return JMenuItem - the created menu item.
      */
	protected JMenuItem createMenuItem (BasicMenuIF menu, 
                                        String _key, 
                                        int _pos){

		BasicActionIF action = m_actions.getAction(_key);

		JMenuItem rval = menu.insert(action, _pos);
		try {
			Character mnemonic =  null;
			mnemonic =  (Character) m_actions.getResources().
                        get(_key,UIResourceBundle.MNEMONIC);
			rval.setMnemonic(mnemonic.charValue());
		}
		catch (Exception ex) {
			//missing resource means no mnemonic
		}

		try {
			KeyStroke keyStroke = (KeyStroke) m_actions.getResources().
                                  get(_key,UIResourceBundle.KEY_STROKE);
			rval.setAccelerator(keyStroke);
		}
		catch (Exception ex) {
			//missing resource means no mnemonic
		}
			
		return rval;
	}

    /**
      * created a check box menu item based on the apssed parameters.
      *
      * @param _menu - to bind the new checkbox to.
      * @param _key - for the actions to bind to the menuitem
      * @return JCheckBoxMenuItem - the newly created menu item.
      */
	protected JCheckBoxMenuItem createCheckBoxMenuItem (
                                                    BasicMenuIF _menu, 
                                                    String _key){

		BasicActionIF action = m_actions.getAction(_key);
	
		JCheckBoxMenuItem rval = _menu.addCheckBoxMenuItem(action);
		try {
			Character mnemonic =  null;
			mnemonic =  (Character) m_actions.getResources().
                        get(_key,UIResourceBundle.MNEMONIC);
			rval.setMnemonic(mnemonic.charValue());
		}
		catch (Exception ex) {
			//missing resource means no mnemonic
		}

		try {
			KeyStroke keyStroke = (KeyStroke) m_actions.getResources().
                                  get(_key,UIResourceBundle.KEY_STROKE);
			rval.setAccelerator(keyStroke);
		}
		catch (Exception ex) {
			//missing resource means no mnemonic
		}

		return rval;
	}
}
