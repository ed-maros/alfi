package edu.caltech.ligo.alfi.common;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.KeyStroke;

/**    
  * <pre>
  * The base class for all actions 
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see javax.swing.AbstractAction
  * @see edu.caltech.ligo.alfi.common.UIResourceBundle
  */
public interface BasicActionIF extends Comparable, Action {

    /**
      * Mapping action type key for the actions type.
      */
	public static final String ACTION_TYPE = "basicaction.action.type";
    /**
      * Mapping action type key for the disabled icon.
      */
	public static final String DISABLED_ICON = "basicaction.disabled.icon";
    /**
      * Mapping action type key for the pressed icon.
      */
	public static final String PRESSED_ICON = "basicaction.pressed.icon";
    /**
      * Mapping action type key for the rollover icon.
      */
	public static final String ROLLOVER_ICON = "basicaction.rollover.icon";
    /**
      * Mapping action type key for the selected icon.
      */
	public static final String SELECTED = "basicaction.selected";

    /**
      * A convinence method for retrieving this actions name.
      * same as <CODE>(String)getValue(Action.NAME)</CODE)
      *
      * @return The name of this action.
      * @see edu.caltech.ligo.alfi.common.BasicResourceBundle#NAME
      */
	public String getName ();

    /**
      * A convinence method for seting this actions name.
      * same as <CODE> putValue(Action.NAME, _name);</CODE>
      * @param String _name - the name of this action.
      */
	public void setName (String _name);

	
    /**
      * Returns the key stroke for this action.
      * @return int - the value of the key stroke.
      *
      * @see UIResourceBundle#KEY_STROKE
      */
	public KeyStroke getKeyStroke ();

    /**
      * Returns the mnemonic for this action.
      * @return char - the value of the mnemonic.
      *
      * @see UIResourceBundle#MNEMONIC
      */
	public Character getMnemonic ();
	
    /**
      * sets the value of the selected attibute of this action.
      *
      * @param boolean - the new value of the attribute.
      */
	public void setSelected (boolean b);

    /**
      * gets the value of the selected attibute of this action.
      *
      */
	public boolean getSelected ();

    /**
      * convienence method to return the value of the long discription of this action.
      * same as <CODE>getValue(Action.LONG_DESCRIPTION);</CODE>
      *
      * @return String - The long description.
      */
	public String getLongDescription ();

    /**
      * convienence method to set the value of the long discription of this action.
      * same as <CODE>putValue(Action.LONG_DESCRIPTION);</CODE>
      *
      * @param String - The long description.
      */
	public void setLongDescription (String desc);

    /**
      * convienence method to return the value of the short discription of this action.
      * same as <CODE>getValue(Action.SHORT_DESCRIPTION);</CODE>
      *
      * @return String - The short description.
      */
	public String getShortDescription ();

    /**
      * convienence method to set the value of the short discription of this action.
      * same as <CODE>putValue(Action.SHORT_DESCRIPTION);</CODE>
      *
      * @param String - The short description.
      */
	public void setShortDescription (String desc);

    /**
      * convienence method to return the value of the rollover icon of this action.
      * same as <CODE>getValue(BasicActionIF.ROLLOVER_ICON);</CODE>
      *
      * @return Icon - The default icon.
      */
	public Icon getRolloverIcon ();

    /**
      * convienence method to return the value of the pressed icon of this action.
      * same as <CODE>getValue(BasicActionIF.PRESSED_ICON);</CODE>
      *
      * @return Icon - The default icon.
      */
	public Icon getPressedIcon ();

    /**
      * convienence method to return the value of the default icon of this action.
      * same as <CODE>getValue(Action.SMALL_ICON);</CODE>
      *
      * @return Icon - The default icon.
      */
	public Icon getDefaultIcon ();

    /**
      * convienence method to set the value of the default icon of this action.
      * same as <CODE>putValue(Action.SMALL_ICON);</CODE>
      *
      * @param Icon - The default icon.
      */
	public void setDefaultIcon (Icon icon);

    /**
      * convienence method to return the value of the disabled icon of this action.
      * same as <CODE>getValue(BasicActionIF.DISABLED_ICON)</CODE>
      *
      * @return Icon - The disabled icon.
      */
	public Icon getDisabledIcon ();

    /**
      * convienence method to set the value of the disabled icon of this action.
      * same as <CODE>putValue(BasicActionIF.DISABLED_ICON)</CODE>
      *
      * @param Icon - The disabled icon.
      */
	public void setDisabledIcon (Icon icon);
	
    /**
      * Causes the action to switch its boolean representation.
      *
      */
	public void toggle ();

    /**
      * returns true is this action
      * has no supporting icons and is texted based.
      */
	public boolean isTextBasedAction ();
}
