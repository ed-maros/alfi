package edu.caltech.ligo.alfi.common;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.Action;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import javax.swing.GrayFilter;


/**    
  * <pre>
  * The base class for all actions 
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see javax.swing.AbstractAction
  * @see edu.caltech.ligo.alfi.common.UIResourceBundle
  */
public abstract class BasicAction extends AbstractAction 
                                  implements BasicActionIF{

	protected UIResourceBundle m_resources;
	private boolean m_selected;

	private static final String TEXT = "basic.resource.text";
	
    /**
      * Constructs and loads all necessary resources for this action.
      * @param String _type - the type of action.
      * @param UIResourceBundle _resources - the resources for this action.
      */
	public BasicAction (String _type, UIResourceBundle _resources){
		super();
		m_resources  = _resources;
		m_selected = true;

		putValue(ACTION_TYPE,_type);
		try {
			putValue(Action.NAME,
                m_resources.get(_type, BasicResourceBundle.NAME));
		}
		catch (Exception ex) {
			//sets to default.
			putValue(Action.NAME,AlfiConstants.UNDEFINED);
		}

		try {
			putValue(Action.SHORT_DESCRIPTION,
                m_resources.get (_type, BasicResourceBundle.NAME));
		}
		catch (Exception ex) {
			//sets to default.
			putValue(Action.SHORT_DESCRIPTION,AlfiConstants.UNDEFINED);
		}

		try {
			putValue(Action.LONG_DESCRIPTION,
                m_resources.get(_type, BasicResourceBundle.DESCRIPTION));
		}
		catch (Exception ex) {
			//sets to default.
			putValue(Action.LONG_DESCRIPTION,AlfiConstants.UNDEFINED);
		}

		try{
			putValue(Action.SMALL_ICON, m_resources.getPrimaryImage(_type));
			putValue(TEXT,new Boolean (false));
		}
		catch(Exception ex){
			//its not defined and not there.
			//it is assumed if no image you want a "button" on the tool bar
			putValue(TEXT,new Boolean (true));
		}

		try{
			putValue(BasicActionIF.DISABLED_ICON, 
                m_resources.getPressedImage (_type));
		}
		catch(Exception mre){
			//its not defined and not there.
		}

		try{
			putValue(BasicActionIF.PRESSED_ICON, 
                m_resources.getPressedImage (_type));
		}
		catch(Exception mre){
			//its not defined and not there.
		}

		try{
			putValue(BasicActionIF.ROLLOVER_ICON, 
                m_resources.getRolloverImage (_type));
		}
		catch(Exception mre){
			//its not defined and not there.
		}
	}

    /**
      * A convinence method for retrieving this actions name.
      * same as <CODE>(String)getValue(Action.NAME)</CODE)
      *
      * @return The name of this action.
      * @see edu.caltech.ligo.alfi.common.BasicResourceBundle#NAME
      */
	public String getName () {
		return (String) getValue(Action.NAME);
	}

    /**
      * A convinence method for seting this actions name.
      * same as <CODE> putValue(Action.NAME, _name);</CODE>
      * @param String _name - the name of this action.
      */
	public void setName(String _name){
		putValue(Action.NAME, _name);
	}

	
    /**
      * Returns the key stroke for this action.
      * @return int - the value of the key stroke.
      *
      * @see edu.caltech.ligo.alfi.common.UIResourceBundle#KEY_STROKE
      */
	public KeyStroke getKeyStroke(){
		try {
			return (KeyStroke) m_resources.get ((String) getValue(ACTION_TYPE),
                UIResourceBundle.KEY_STROKE);
		}
		catch (Exception ex) {
			return null;
		}
	}

    /**
      * Returns the mnemonic for this action.
      * @return char - the value of the mnemonic.
      *
      * @see edu.caltech.ligo.alfi.common.UIResourceBundle#MNEMONIC
      */
	public Character getMnemonic (){
		try {
			return (Character) m_resources.get((String) getValue(ACTION_TYPE),
                UIResourceBundle.MNEMONIC);
		}
		catch (Exception ex) {
			return null;
		}
	}
	
    /**
      * sets the value of the selected attibute of this action.
      *
      * @param boolean - the new value of the attribute.
      */
	public void setSelected (boolean b){
		boolean old_val = m_selected;
		m_selected = b;
		if (changeSupport != null) {
			changeSupport.firePropertyChange(BasicActionIF.SELECTED, 
                new Boolean (old_val), new Boolean (b));
		}
	}

    /**
      * gets the value of the selected attibute of this action.
      *
      */
	public boolean getSelected (){
		return m_selected;
	}

    /**
      * convienence method to return the value of the long discription
      * of this action.
      * same as <CODE>getValue(Action.LONG_DESCRIPTION);</CODE>
      *
      * @return String - The long description.
      */
	public String getLongDescription (){
		return (String) getValue(Action.LONG_DESCRIPTION);
	}

    /**
      * convienence method to set the value of the long discription
      * of this action.
      * same as <CODE>putValue(Action.LONG_DESCRIPTION);</CODE>
      *
      * @param String - The long description.
      */
	public void setLongDescription (String _desc){
		putValue(Action.LONG_DESCRIPTION, _desc);
	}

    /**
      * convienence method to return the value of the short discription 
      * of this action.
      * same as <CODE>getValue(Action.SHORT_DESCRIPTION);</CODE>
      *
      * @return String - The short description.
      */
	public String getShortDescription (){
		return (String) getValue(Action.SHORT_DESCRIPTION);
	}

    /**
      * convienence method to set the value of the short discription 
      * of this action.
      * same as <CODE>putValue(Action.SHORT_DESCRIPTION);</CODE>
      *
      * @param String - The short description.
      */
	public void setShortDescription (String desc){
		putValue(Action.SHORT_DESCRIPTION, desc);
	}

    /**
      * convienence method to return the value of the rollover icon 
      * of this action.
      * same as <CODE>getValue(BasicActionIF.ROLLOVER_ICON);</CODE>
      *
      * @return Icon - The default icon.
      */
	public Icon getRolloverIcon (){
		return (Icon) getValue(BasicActionIF.ROLLOVER_ICON);
	}

    /**
      * convienence method to return the value of the pressed icon 
      * of this action.
      * same as <CODE>getValue(BasicActionIF.PRESSED_ICON);</CODE>
      *
      * @return Icon - The default icon.
      */
	public Icon getPressedIcon (){
		return (Icon) getValue(BasicActionIF.PRESSED_ICON);
	}

    /**
      * convienence method to return the value of the default icon 
      * of this action.
      * same as <CODE>getValue(Action.SMALL_ICON);</CODE>
      *
      * @return Icon - The default icon.
      */
	public Icon getDefaultIcon (){
		return (Icon)getValue(Action.SMALL_ICON);
	}

    /**
      * convienence method to set the value of the default icon 
      * of this action.
      * same as <CODE>putValue(Action.SMALL_ICON);</CODE>
      *
      * @param Icon - The default icon.
      */
	public void setDefaultIcon (Icon icon) {
		putValue(Action.SMALL_ICON, icon);
	}

    /**
      * convienence method to return the value of the disabled icon 
      * of this action.
      * same as <CODE>getValue(BasicActionIF.DISABLED_ICON)</CODE>
      *
      * @return Icon - The disabled icon.
      */
	public Icon getDisabledIcon (){
		return (Icon) getValue(DISABLED_ICON); 
	}

    /**
      * convienence method to set the value of the disabled icon 
      * of this action.
      * same as <CODE>getValue(BasicActionIF.DISABLED_ICON)</CODE>
      *
      * @param Icon - The disabled icon.
      */
	public void setDisabledIcon (Icon icon){
		putValue(BasicActionIF.DISABLED_ICON, icon);
	}
	
    /**
      * toggles the boolean selected.
      *
      */
    public void toggle (){
        if(m_selected){
            setSelected(false);
        }      
        else{
            setSelected(true);
        }
    }

	public boolean isTextBasedAction () {
		Boolean textBased = (Boolean)getValue (TEXT);
		return textBased.booleanValue();
	}


	/**
      * Compares the action keys if the actions are identical.
      *
      * @param Object o - another BasicAction    
      * @return int - (-1) is less than; 1 is greater than; 0 is equals.   
      * @exception ClassCastException - thrown if an object other than a  
      *  BasicAction is passed to this method.            
      */

	public int compareTo(Object o) throws ClassCastException{
        BasicActionIF action = (BasicActionIF)o;
        
        String actionName = action.getName();

        return (actionName.compareTo(getName()));
	}


}
