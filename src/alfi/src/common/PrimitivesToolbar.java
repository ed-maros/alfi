package edu.caltech.ligo.alfi.common;

import java.lang.Boolean;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.accessibility.*;

import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.summary.AlfiMainFrame;
import edu.caltech.ligo.alfi.editor.EditorFrame;

/**    
  * <pre>
  * The Editor Frames' toolbar menu to create primitives
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class PrimitivesToolbar extends JToolBar { 

    public final String TOOLBAR_TITLE = new String("Primitives");
    private AlfiMainFrame m_parent_frame;
    private JButton m_selected_button = null;
    private String m_selected_primitive_name = null;
    private ALFINode m_selected_primitive_node = null;

    private HashMap m_button_map = new HashMap();

    private static final int IMAGE_WIDTH = 35;
    private static final int IMAGE_HEIGHT = 35;
    private static final Dimension IMAGE_STD_SIZE = 
            new Dimension(IMAGE_WIDTH, IMAGE_HEIGHT);

    private static final int XBM_HEIGHT = 16;

    public PrimitivesToolbar (ALFINode[] _primitives, 
                              AlfiMainFrame _parent_frame) {
        super("Primitives", JToolBar.HORIZONTAL);

        m_parent_frame = _parent_frame;                    
        setFloatable(false);
        setLayout(new FlowLayout());

        for (int ii = 0; ii < _primitives.length; ii++) {    
            ALFINode node = _primitives[ii];
            
            if (node.getIconInfo() != null) {
                String icon_file_path = node.getIconFilePath();
                if (icon_file_path != null) {
                    Image image = new ImageIcon(icon_file_path).getImage();
                    if (image.getHeight(null) > XBM_HEIGHT) {
                        Image scaled_image = image.getScaledInstance(
                               IMAGE_WIDTH, IMAGE_HEIGHT, Image.SCALE_DEFAULT);
                        image = scaled_image;
                    }
                    ImageIcon image_icon = 
                               new ImageIcon(image, node.determineLocalName());
                    String node_name = node.determineLocalName();
    
                    AddPrimitiveAction action =
                                       new AddPrimitiveAction(node_name, node);

                    JButton button = add(action);
                    m_button_map.put(node_name, button);
                    button.setIcon(image_icon);
                    button.setPreferredSize(IMAGE_STD_SIZE);
                    button.setText("");
                    button.setActionCommand(node_name);
                    button.setToolTipText(node_name);
                }
            }
        }
    }

    public int getButtonCount () {
        return m_button_map.size();
    }

    public Dimension getButtonSize () {
        int button_count = getButtonCount();
        if (button_count > 0) {
            Set node_names = m_button_map.keySet();
            String[] name_string = new String[button_count];
            node_names.toArray(name_string);
            JButton button = ( JButton ) m_button_map.get(name_string[0]);   
            return button.getMaximumSize();
        }
        else {
            return null;
        }
    }

    public boolean hasButtonSelected () {
        return (m_selected_button != null);
    }
    
    public void clearSelection () {
        if (m_selected_button != null) {
            m_selected_button.setSelected(false);
            m_selected_button.setBackground(null);
            m_selected_button = null;
        }
    }

    public void addNode (EditorFrame _editor) {
        if (hasButtonSelected()) {
            String new_name =
                    _editor.getNodeInEdit().generateUniqueLocalMemberNodeName(
                                                     m_selected_primitive_name);

            _editor.addMemberNode(new_name, m_selected_primitive_node, null);
        }
    }

    class AddPrimitiveAction extends AbstractAction {
        
        private String m_primitive_name;
        private ALFINode m_primitive_node;

        AddPrimitiveAction (String _name, ALFINode _node) {
            super(_name);
            m_primitive_name = _name;
            m_primitive_node = _node;
        }

        public void actionPerformed(ActionEvent _event) {
            if (m_selected_button != null) {
                m_selected_button.setBackground(null);
                m_selected_button.setSelected(false);
            }

            String text = (String) _event.getActionCommand();
            m_selected_button = (JButton) m_button_map.get(text);
            m_selected_button.setSelected(true);
            m_selected_button.setBackground(Color.gray);
            m_selected_primitive_name = m_primitive_name;
            m_selected_primitive_node = m_primitive_node;

        }
    }
}

