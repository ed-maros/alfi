package edu.caltech.ligo.alfi.resource;

import java.util.Locale;
import java.util.ResourceBundle; 

import java.awt.Event;
import java.awt.event.KeyEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import edu.caltech.ligo.alfi.common.UIResourceBundle;
import edu.caltech.ligo.alfi.common.GuiConstants;
import edu.caltech.ligo.alfi.editor.PortPlacementEditorActionsIF;
/**    
  * <pre>
  * Resource strings for implementing the Actions
  * </pre>
  *
  * each array is composed of :<BR>
  * index 0: Key <BR>
  * index 1: Name <BR>
  * index 2: Description <BR>
  * index 3: Mnemonic<BR>
  * index 4: KeyStroke<BR>
  * index 5: Primary Image<BR>
  * index 6: Rollover Image<BR>
  * index 7: Pressed Image<BR>

  * outside of the bounds can be used by individual windows.
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class PortPlacementEditorResources extends UIResourceBundle {
	private static Object[][] editorResources = {
    };

    public PortPlacementEditorResources () { super (); }

    public Object[][] getContents () { return editorResources; }
}

