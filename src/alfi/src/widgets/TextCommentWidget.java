package edu.caltech.ligo.alfi.widgets;

import java.util.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import com.nwoods.jgo.*;

    /**
     * This class is the widget which represents ports in an Alfi edit window.
     */
public class TextCommentWidget extends JGoText implements AlfiWidgetIF,
                 ProxyChangeEventListener {


    //**************************************************************************
    //***** member fields ******************************************************

    final protected EditorFrame m_editor;

    final protected TextComment m_text_comment;

    protected boolean m_is_unfrozen = true;

    final protected boolean m_is_grouped = false;

    final protected boolean mb_is_local_comment;
 
    private int m_mouse_down_modifiers;
    private Point m_mouse_down_point;
    private Point m_resizing_initial_mouse_down_point;

    /** Used in conjuction with GroupElementModEventSource */
    protected HashMap m_group_element_event_listeners_map;
 
    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor for Comment
          */
    public TextCommentWidget( EditorFrame _editor, TextComment _window_comment) {
        this(_editor, _window_comment, false);
    }

    public TextCommentWidget( EditorFrame _editor, TextComment _window_comment,
                    boolean _b_raw_external_view) {

        m_editor = _editor;

        m_text_comment = _window_comment;

        ALFINode container = m_editor.getNodeInEdit();
     
        mb_is_local_comment = 
                        container.textComment_containsLocal(_window_comment);

        initialize();
        m_mouse_down_point = null;

        ///////////////////////////////////////
        // REMEMBER TO REMOVE LISTENERS TOO!!//
        ///////////////////////////////////////

        if (! _b_raw_external_view ) {
            m_text_comment.addProxyChangeListener(this);
        }

        if (!mb_is_local_comment) {
            m_is_unfrozen = false;
        }
     }



    private void initialize() {

        setSelectable(m_is_unfrozen);
        //setGrabChildSelection(m_is_unfrozen);
        setSelectBackground(m_is_unfrozen);
        setDraggable(m_is_unfrozen);
         
        layoutContent(m_text_comment);

        setLocation(m_text_comment.getLocation());

        setEditable(m_is_unfrozen);
        setResizable(m_is_unfrozen);
        m_editor.getView().getDoc().setModifiable(true);
    }

    public void layoutContent(TextComment _text_comment) {

        AlfiColors alfi_colors = new AlfiColors(); 

        String s = _text_comment.getCommentValue();
        int color_id = _text_comment.getColor();
        Color text_color = alfi_colors.getColor(color_id);
        setText(s);
        setTextColor(text_color);
        setMultiline(true);
        setSelectable(true);
        setResizable(m_is_unfrozen);
        setDraggable(true);
        setEditable(true);
        setEditOnSingleClick(false);  // in case it becomes editable
        setTransparent(true);
        setFontSize(_text_comment.getFontSize());
        setLocation(_text_comment.getLocation());
        setBold(_text_comment.isBold());
        setItalic(_text_comment.isItalic());
        setUnderline(_text_comment.isUnderlined());

    }


    //**************************************************************************
    //***** methods ************************************************************

    public void refresh() {

        layoutContent(m_text_comment);
        
        setEditable(m_is_unfrozen);
        setResizable(m_is_unfrozen);
    }


    
    //////////////////////////////////////////////////////////
    ////////// AlfiWidgetIF methods //////////////////////////

        /** AlfiWidgetIF method. */
    public EditorFrame getEditor() { return m_editor; }

        /** AlfiWidgetIF method. */
    public Object getAssociatedProxyObject() { return getAssociatedProxy(); }

        /** AlfiWidgetIF covenience method. */
    public boolean isLocal() {
        return true;
    }

        
    public TextComment getAssociatedProxy() { return m_text_comment; }

     //////////////////////////////
    // event processing //////////

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseDown(int _modifiers, Point _dc, Point _vc,
                                                           JGoView _view) {
        m_mouse_down_point = null;
        final ALFIView view = (ALFIView) _view;

        m_mouse_down_modifiers = _modifiers;
        if ((_modifiers & InputEvent.BUTTON3_MASK) != 0){
            view.doCommentPopupMenu(this, _modifiers, _dc, _vc);
            return true;
        }
        else if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
            // The TextComment is not a part of a group
            if (m_text_comment.group_get() == null)  {
                view.pickObject(this, true);
                m_mouse_down_point = _dc;
                return false;
            }
            else {
                // Text is a part of a group
                if ((_modifiers & InputEvent.CTRL_MASK) != 0) {
                    m_mouse_down_point = _dc;
                    return true;
                }
                else {
                    // This TextComment is a part of a group.
                    // Send out an event that all elements need to be selected
                    GroupElementModEvent event = 
                        new GroupElementModEvent(this, 
                            GroupElementModEvent.GROUP_SELECT,  
                            m_text_comment.group_get()); 
                    m_text_comment.fireGroupElementModEvent(event);
                    return false;
                }
            }
        }


        return false;


    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseUp(int _modifiers, 
                             Point _dc, Point _vc, JGoView _view) {
        ALFIView view = (ALFIView) _view;

        if (m_is_unfrozen) {

            if ((_modifiers & InputEvent.BUTTON1_MASK) != 0){
                // check if the node has been moved
                if ((m_mouse_down_point != null) &&
                            (! _dc.equals(m_mouse_down_point))) {

                    view.performInternalComponentsMove(KeyEvent.VK_UNDEFINED);

                    if (( (_modifiers & InputEvent.CTRL_MASK) != 0) &&
                          m_text_comment.group_get() != null  ){
                        int x_offset = _dc.x - m_mouse_down_point.x;
                        int y_offset = _dc.y - m_mouse_down_point.y;
                        view.moveSelection(view.getSelection(), 0, 
                                       x_offset, y_offset, 0);
                        view.setState(JGoView.MouseStateSelection);
                   }
                }
            }
        }

        m_mouse_down_point = null;
        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseMove(int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
        return false;
    }

        /**
         * A mouse click on a comment that is not frozen
         *
         */
    public boolean doMouseClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        ALFIView view = (ALFIView)_view;
        if (m_is_unfrozen) {
            view.pickObject(this, true);
        }

        if ((_modifiers & InputEvent.BUTTON3_MASK) != 0){
            view.doCommentPopupMenu(this, _modifiers, _dc, _vc);
            return true;
        }
        else if ((_modifiers & InputEvent.BUTTON1_MASK) != 0){
            if (m_is_unfrozen) {  
                return false;
            }
        }
        return false;
    }

    public boolean doMouseDblClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        m_editor.editTextComment(m_editor, m_text_comment);
        return true;
    }

    public boolean doUncapturedMouseMove(int _flags, Point _dc, Point _vc,
                                                                JGoView _view) {
        return false;
    }

    //////////////////////////////////////////////////////////
    //////////  ProxyChangeEventListener methods /////////////

        /** ProxyChangeEventListener method. */
    public void proxyChangePerformed(ProxyChangeEvent _event) {
        refresh();
    }


    //////////////////
    // misc //////////


    public String toString() {
        if (m_is_unfrozen) {
            return m_text_comment.getName();
        }
        return null;
    }

}
