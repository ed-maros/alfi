package edu.caltech.ligo.alfi.widgets; 
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.bookkeeper.PortProxy;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.tools.*;

/**
  *
  *
  * @author Melody Araya
  * @version %I%, %G%
  */

public class DummyNodeWidget extends JGoArea {

    public static final int SPOT_OFFSET = 1;
    public static final int OFFSET = PortWidget.PORT_WIDTH + SPOT_OFFSET;
    public static final int BORDER = 3;
    public static final int GRID_SIZE = OFFSET/2;

    private static final Dimension RECT_MIN_SIZE = new Dimension(19, 19); 

    protected JGoText m_label;
    protected JGoRectangle m_rectangle;
    
    protected String m_node_name;
    protected PortProxy[][] m_ports;
    
    public DummyNodeWidget(String _node_name, PortProxy[][] _ports){
        super();        

        m_node_name = _node_name;
        m_ports = _ports;

        m_label = null;
        m_rectangle = null;
        
        initialize();
    }
    
    /**
      * Returns the center location based on the given top-left coordinate
      * 
      * @param _top_left_loc top left location
      * 
      */
    protected Point getCenterLocation (Point _top_left_loc) {
        int width = getWidth();
        int height = getHeight();  

        Point new_location = getTopLeft();

        if (_top_left_loc != null){
            new_location.x = _top_left_loc.x;
            new_location.y = _top_left_loc.y;
        }
        
        new_location.x += width/2;
        new_location.y += height/2;

        return new Point(new_location);
    }

    /**
      * Returns the center location based on widget's top-left coordinate
      * 
      * @param _top_left_loc top left location
      * 
      */
    protected Point getCenterLocation () {
        return getCenterLocation(null);
    }

    protected void initialize () {
        createAreaObjectsExceptingPorts();

            // determine the size of the rectangle 
        m_rectangle.setBoundingRect(determineNodeRectangle(m_ports));

        createPortWidgets();

        layoutContent();
    }

    
    protected void createAreaObjectsExceptingPorts () {

        m_rectangle = createRectangle();
        addObjectAtHead(m_rectangle);

        m_label = createLabel();
        addObjectAtTail(m_label);

        Dimension label_size = m_label.getSize();
        setSize(label_size);
        m_label.setSize(label_size);
    }

    /**
      * Position all the parts of the area relative to the text label.
      */
    protected void layoutContent() {
        Rectangle content_rect = determineNodeLayoutRects(m_ports)[4];

        JGoObject content = getContent();
        content.setLocation(content_rect.x, content_rect.y);
    }

        /**
         * Clears all child objects except the ports which we don't want losing
         * their connections.
         */
    protected void clearObjectsExceptingPorts () {
        if (m_rectangle != null) { 
            removeObject(m_rectangle); 
            m_rectangle = null;
        }
    
        removeObject(m_label); 
        m_label = null;
    }

    private JGoObject getContent () {
        return m_label;
    }

    private Dimension getContentSize () {
        return m_label.getSize();
    }

    private Dimension getMinimumValidContentSize () {
        return  new Dimension(RECT_MIN_SIZE);
    }


    // get access to the parts of the node
    public JGoText getLabel() { return m_label; }
    public void setLabel (JGoText lab)
    {
        JGoText oldLabel = getLabel();
        if (oldLabel != lab) {
            if (oldLabel != null) {
                removeObject(oldLabel);
            }
            m_label = lab;
            if (lab != null) {
                addObjectAtTail(lab);
            }
        }
    }

    // These are convenience methods for getting the Text property
    public String getText() { return getLabel().getText(); }
    public void setText(String s) { getLabel().setText(s); }

    public JGoRectangle getRect() { return m_rectangle; }
    public void setRect(JGoRectangle rect)
    {
        JGoRectangle oldRect = getRect();
        if (oldRect != rect) {
            if (oldRect != null) {
                removeObject(oldRect);
            }
            m_rectangle = rect;
            if (rect != null) {
                addObjectAtHead(rect);
            }
        }
    }




    private JGoRectangle createRectangle() {
        JGo3DRect rectangle = new JGo3DRect();
        rectangle.setSelectable(false);
        rectangle.setPen(JGoPen.lightGray); 

        Color brush_color = GuiConstants.ROOT_NODE_COLOR;
        rectangle.setBrush(JGoBrush.makeStockBrush(brush_color));

        return rectangle;
    }


    private JGoText createLabel() {

        JGoText text_label = new JGoText(m_node_name);
        text_label.setTransparent(true);

        text_label.setMultiline(true);
        text_label.setSelectable(false);
        text_label.setResizable(false);
        text_label.setDraggable(false);
        text_label.setEditable(false);
        text_label.setFaceName(GuiConstants.NODE_TEXT_FACE_NAME);
        
        return text_label;
    }

        /**
         * Returns the dimensions needed to contain all the ports on the edges.
         */
    protected Dimension determineMaxPortsSize (PortProxy[][] _port_order) {
        int max_horizontal_count =
                Math.max(_port_order[0].length, _port_order[1].length);
        int max_vertical_count =
                Math.max(_port_order[2].length, _port_order[3].length);

        int slot_width = PortWidget.PORT_WIDTH + 1;
        return new Dimension(max_horizontal_count * slot_width,
                             max_vertical_count * slot_width);
    }

        /**
         * This method returns Rectangles of the right size to contain the ports
         * on each side of the widget, a Rectangle[4] (N, S, E, W sides).
         * Returned rects which have area = 0 have no ports in them.
         * The positions of the rects only take into account the size and
         * position of the other port edge rects, and may still need to be
         * adjusted further in order to fit in a minumum content area.
         */
    private Rectangle[] determinePortAreaRects(PortProxy[][] _port_order) {
        final int N = PortProxy.EDGE_NORTH;  final int S = PortProxy.EDGE_SOUTH;
        final int E = PortProxy.EDGE_EAST;   final int W = PortProxy.EDGE_WEST;

        Rectangle[] port_rects = new Rectangle[_port_order.length];
        for (int edge = 0; edge < _port_order.length; edge++) {
            int width, height;
            switch(edge) {
              case N:
              case S:
                width = OFFSET * _port_order[edge].length;
                height = (width != 0) ? OFFSET : 0;
                break;
              case E:
              case W:
                height = OFFSET * _port_order[edge].length;
                width = (height != 0) ? OFFSET : 0;
                break;
              default:
                return null;    // should never get here
            }
            port_rects[edge] = new Rectangle(0, 0, width, height);
        }

        int v_offset = port_rects[N].isEmpty() ? 0 : OFFSET;
        int h_offset = port_rects[W].isEmpty() ? 0 : OFFSET;

        port_rects[E].translate(0, v_offset);
        port_rects[W].translate(0, v_offset);

        v_offset += Math.max(port_rects[W].height, port_rects[E].height);

        port_rects[N].translate(h_offset, 0);
        port_rects[S].translate(h_offset, v_offset);

        h_offset += Math.max(port_rects[N].width, port_rects[S].width);

            // note vertical translation already done above
        port_rects[E].translate(h_offset, 0);

            // expand widths and heights as needed to match north/south widths
            // and east/west heights
        if (port_rects[N].width > port_rects[S].width) {
            port_rects[S].width = port_rects[N].width;
        }
        else { port_rects[N].width = port_rects[S].width; }

        if (port_rects[E].height > port_rects[W].height) {
            port_rects[W].height = port_rects[E].height;
        }
        else { port_rects[E].height = port_rects[W].height; }

        return port_rects;
    }

         /**
          * This method takes the size of the content object (label or graphic)
          * and adjusts the port edge rectangles if there is not enough room
          * given the initial size and position of the port edge rects.  The
          * return value is a Rectangle[5] with the first 4 corresponding to
          * the N, S, E, & W edges, and the fifth is the rectangle which shows
          * where to place the content object.
          */
    private Rectangle[] adjustEdgePortRectsForContent(Dimension _content_size,
                                             Rectangle[] _raw_edge_port_rects) {
        final int N = PortProxy.EDGE_NORTH;  final int S = PortProxy.EDGE_SOUTH;
        final int E = PortProxy.EDGE_EAST;   final int W = PortProxy.EDGE_WEST;

        Rectangle[] rects = { _raw_edge_port_rects[0], _raw_edge_port_rects[1],
                              _raw_edge_port_rects[2], _raw_edge_port_rects[3],
                              null };

            // this is the full area size just taking into account the ports.
            // very likely this will already make enough space for the content.
        Rectangle full_area = new Rectangle(0, 0,
                          rects[W].width + rects[N].width + rects[E].width,
                          rects[N].height + rects[W].height + rects[S].height);

            // this is the basic content area available to start with
        Rectangle content_area = new Rectangle(rects[W].width, rects[N].height,
                                               rects[N].width, rects[W].height);

            // first just try centering the content object and see if it's ok
        Point ul = new Point((full_area.width - _content_size.width) / 2,
                             (full_area.height - _content_size.height) / 2);
        Rectangle content_object_rect = new Rectangle(ul, _content_size);

            // but may need to adjust port rects to fit the content
        if (! content_area.contains(content_object_rect)) {
            int width_needed = content_object_rect.width - content_area.width;
            if (width_needed > 0) {
                if ((width_needed % OFFSET) != 0) {
                    width_needed += OFFSET - (width_needed % OFFSET);
                }
                rects[N].width += width_needed;
                rects[S].width += width_needed;
                rects[E].x += width_needed;
            }
            int height_needed= content_object_rect.height - content_area.height;
            if (height_needed > 0) {
                if ((height_needed % OFFSET) != 0) {
                    height_needed += OFFSET - (height_needed % OFFSET);
                }
                rects[W].height += height_needed;
                rects[E].height += height_needed;
                rects[S].y += height_needed;
            }

                // redo content_object_rect determination

                // revised content area
            content_area = new Rectangle(rects[W].width, rects[N].height,
                                         rects[N].width, rects[W].height);
            ul = new Point(
                content_area.x+ (content_area.width - _content_size.width)/2,
                content_area.y+ (content_area.height - _content_size.height)/2);
            content_object_rect = new Rectangle(ul, _content_size);
        }
        rects[4] = content_object_rect;

        return rects;
    }

   private void createPortWidgets() {
        Rectangle[] layout_rects = determineNodeLayoutRects(m_ports);
        int[] NSEW_port_offsets =
                    determinePossiblePortOffsets(m_ports, layout_rects);
        if (_create_all_ports) {
            createPorts(m_ports, layout_rects, NSEW_port_offsets);
        }
        else {
                // move the ports to the proper location       
            movePorts(m_ports, layout_rects, NSEW_port_offsets);

                // flagged to add a new port?
            if (m_new_port_proxy != null) {
                addPort(m_new_port_proxy, m_ports, NSEW_port_offsets);
                m_new_port_proxy = null;
            }
        }
    }


    private void createPorts(PortProxy[][] _port_order,
                    Rectangle[] _layout_rects, int[] _NSEW_port_offsets) {

            // determine the size of the rectangle 
        m_rectangle.setBoundingRect(_layout_rects[5]);

            // Create the ports
        for (int edge = 0; edge < _port_order.length; edge++) {
            for (int position = 0; position < _port_order[edge].length;
                                                                  position++) {
                PortProxy port_proxy = _port_order[edge][position];
                if (port_proxy != null) {
                    Point location = determinePortLocationFromQueuePosition(
                         edge, position, _port_order, _NSEW_port_offsets);
                    createPortWidget(port_proxy, location);
                }
            }
        }
    }


        /** Reposition all the port locations when the rectangle is resized. */
    private void movePorts(PortProxy[][] _port_order,
                    Rectangle[] _layout_rects, int[] _NSEW_port_offsets) {

            // Determine the size of the rectangle 
        m_rectangle.setBoundingRect(_layout_rects[5]);

            // Set the location of the ports
        MemberNodeProxy mnp = getAssociatedProxy();
        PortWidget[] port_widgets = getPortWidgets();
        for (int i = 0; i < port_widgets.length; i++) {
            PortProxy port = port_widgets[i].getAssociatedProxy();
            if (m_member_node.port_contains(port)) {
                int edge = PortProxy.EDGE_INVALID;
                int position = PortProxy.EDGE_POSITION_INVALID;;
                OUTER_LOOP:
                  for (edge = 0; edge < _port_order.length; edge++) {
                    for (position = 0; position < _port_order[edge].length;
                                                                  position++) {
                        if (_port_order[edge][position] == port) {
                            break OUTER_LOOP;
                        }
                    }
                }

                Point location = determinePortLocationFromQueuePosition(edge,
                               position, _port_order, _NSEW_port_offsets);
                setPortLocation(port_widgets[i], location);
                port_widgets[i].setPortShape();
            }
            else { deletePort(port); }
        }
    } 


        /** Port position offsets may occur because of either port centering or
          * symmetry operations.  The port order passed into this method always
          * has already determined the correct order and edge for the ports, but
          * centering and symmetry ops can push them to the center or to the
          * other end of a particular edge's port slots, i.e., centering can
          * cause this:
          *
          *        ____________       ____________
          *        VVV           -->      VVV
          *        012                    012
          *
          * (note the port order did not change)
          *
          * and sometimes a symmetry op might cause this:
          *
          *        ____________       ____________
          *        VVV           -->           VVV
          *        012                         012
          *
          * (note the port order did not change, which may seem strange here,
          * but the reverse ordering of the ports needed for a symmetry op has
          * already occurred (when needed) in the determination of _port_order.)
          *
          * Centering ALWAYS overrides any offset due to a symmetry op.
          */
    private int[] determinePossiblePortOffsets(PortProxy[][] _port_order,
                                                   Rectangle[] _layout_rects) {
        boolean b_center_ports = getBaseNode().getCentersPortsFlag();

        MemberNodeProxy mnp = (MemberNodeProxy) getAssociatedProxyObject();
        boolean b_symmetry_op_exists = ((mnp != null) &&
                         (mnp.getSymmetry() != MemberNodeProxy.SYMMETRY_NONE));

        if (b_center_ports || b_symmetry_op_exists) {
            int[] offsets_NSEW = { 0, 0, 0, 0 };
                // edge = 0 -> 3 (in order: N, S, E, W)
            for (int edge = 0; edge < offsets_NSEW.length; edge++) {
                int port_edge_length = (edge < 2) ?
                        _layout_rects[edge].width : _layout_rects[edge].height;
                int port_slots_available =
                               port_edge_length / PortWidget.PORT_SLOT_SPACING;
                int port_slots_needed = _port_order[edge].length;
                if (b_center_ports) {
                    int empty_starting_slots = 0;
                    for (int edge_pos = 0; edge_pos < _port_order[edge].length;
                                                                  edge_pos++) {
                        if (_port_order[edge][edge_pos] == null) {
                            port_slots_needed--;
                            empty_starting_slots++;
                        }
                        else { break; }
                    }
                    int centering_slot_shift =
                                (port_slots_available - port_slots_needed) / 2;
                    centering_slot_shift -= empty_starting_slots;
                    offsets_NSEW[edge] =
                           centering_slot_shift * PortWidget.PORT_SLOT_SPACING;
                }
                else {
                    int symmetry_op = mnp.getSymmetry();
                    int empty_trailing_slots =
                                      port_slots_available - port_slots_needed;
                    boolean b_slide_ports_to_end = false;
                    switch(edge) {
                      case PortProxy.EDGE_NORTH:
                                            // originally on north edge
                        if ((symmetry_op == MemberNodeProxy.SYMMETRY_Y) ||
                                           // originally on east edge
                                      (symmetry_op ==
                                           MemberNodeProxy.SYMMETRY_PLUS_XY)) {
                            b_slide_ports_to_end = true;
                        }
                        break;
                      case PortProxy.EDGE_SOUTH:
                                            // originally on south edge
                        if ((symmetry_op == MemberNodeProxy.SYMMETRY_Y) ||
                                           // originally on west edge
                                      (symmetry_op ==
                                           MemberNodeProxy.SYMMETRY_PLUS_XY)) {
                            b_slide_ports_to_end = true;
                        }
                        break;
                      case PortProxy.EDGE_EAST:
                                            // originally on east edge
                        if ((symmetry_op == MemberNodeProxy.SYMMETRY_X) ||
                                      (symmetry_op ==
                                           // originally on north edge
                                           MemberNodeProxy.SYMMETRY_PLUS_XY)) {
                            b_slide_ports_to_end = true;
                        }
                        break;
                      case PortProxy.EDGE_WEST:
                                            // originally on west edge
                        if ((symmetry_op == MemberNodeProxy.SYMMETRY_X) ||
                                      (symmetry_op ==
                                           // originally on south edge
                                           MemberNodeProxy.SYMMETRY_PLUS_XY)) {
                            b_slide_ports_to_end = true;
                        }
                        break;
                    }
                    if (b_slide_ports_to_end) {
                        offsets_NSEW[edge] =
                            empty_trailing_slots * PortWidget.PORT_SLOT_SPACING;
                    }
                }
            }

            return offsets_NSEW;
        }

        return null;
    }

        /** Get the port location based on the edge and the position.  This
          * method uses the _NSEW_port_offsets to adjust the port locations
          * in cases where port centering is on or where a symmetry op may have
          * caused such an offset.
          */
    private Point determinePortLocationFromQueuePosition(int _edge,
                                      int _position, PortProxy[][] _port_order,
                                      int[] _NSEW_port_offsets) {
        int port_slot_width = PortWidget.PORT_WIDTH + SPOT_OFFSET;
        Rectangle rect = new Rectangle(m_rectangle.getBoundingRect());

        int[] port_offsets = { 0, 0, 0, 0 };
        if (_NSEW_port_offsets != null) {
            port_offsets =  _NSEW_port_offsets;
        }

        boolean b_has_north_ports=(_port_order[PortProxy.EDGE_NORTH].length> 0);
        boolean b_has_east_ports= (_port_order[PortProxy.EDGE_EAST].length > 0);
        boolean b_has_west_ports= (_port_order[PortProxy.EDGE_WEST].length > 0);
    
            // Initialize the point to be the top-left corner.
        Point point = new Point(rect.x, rect.y);
        
        int start_pos = _position;
        switch (_edge) {
          case PortProxy.EDGE_NORTH: 
            if (b_has_west_ports) { start_pos++; }

            point.x += start_pos * port_slot_width;
            point.y += 1;

            if ((! b_has_east_ports) && (! b_has_west_ports)) {
                point.x += port_slot_width / 2;
            }
            point.x += port_offsets[0];
            break;
          case PortProxy.EDGE_SOUTH: 
            if (b_has_west_ports) { start_pos++; }

            point.x += start_pos * port_slot_width;
            point.y += rect.height - PortWidget.PORT_WIDTH;

            if ((! b_has_east_ports) && (! b_has_west_ports)) {
                point.x += port_slot_width / 2;
            }
            point.x += port_offsets[1];
            break;
          case PortProxy.EDGE_EAST: 
            if (b_has_north_ports) { start_pos++; }

            point.x += rect.width - PortWidget.PORT_WIDTH;
            point.y += start_pos * port_slot_width;

            if (! b_has_north_ports) { point.y += port_slot_width / 2; }
            point.y += port_offsets[2];
            break;
          case PortProxy.EDGE_WEST: 
            if (b_has_north_ports) { start_pos++; }

            point.x += 1;
            point.y += start_pos * port_slot_width;

            if (! b_has_north_ports) { point.y += port_slot_width / 2; }
            point.y += port_offsets[3];
            break;
        }      

        return point;
    }


    private void createPortWidget(PortProxy _new_port, Point _location) {
        PortWidget port_widget = new PortWidget(m_editor, _location,
                                                              this, _new_port);
        setPortLocation(port_widget, _location);
        addObjectAtTail(port_widget);

        port_widget.setSelectable(true);
    }


    private void addPort(PortProxy _new_port, PortProxy[][] _port_order,
                                               int[] _NSEW_port_offsets) {
            // Figure out where this port is supposed to be added
        int[] queue_position =
                            determinePortQueueLocation(_new_port, _port_order);
        if (queue_position[0] != PortProxy.EDGE_INVALID) {
            int edge = queue_position[0];
            int position = queue_position[1];
            Point location = determinePortLocationFromQueuePosition(edge,
                               position, _port_order, _NSEW_port_offsets);
            createPortWidget(_new_port, location);
        }
    }

        /** Returns the current edge and position of the port. */
    private int[] determinePortQueueLocation(PortProxy _port,
                                                   PortProxy[][] _port_order) {
        int[] queue_location = { PortProxy.EDGE_INVALID,
                                 PortProxy.EDGE_POSITION_INVALID };
        MemberNodeProxy mnp = getAssociatedProxy();
        for (int i = 0; i < _port_order.length; i++) {
            for (int j = 0; j < _port_order[i].length; j++) {
                if ((_port_order[i][j] != null) &&
                                                (_port == _port_order[i][j])) {
                    queue_location[0] = i;
                    queue_location[1] = j;
                    return queue_location;
                }
            }
        }
        return queue_location;
    }

    protected void setPortLocation (PortWidget _port_widget, Point _location) {
        _port_widget.setLocation(_location.x, _location.y);
        _port_widget.setBoundingRect(_location, 
                new Dimension(PortWidget.PORT_WIDTH + SPOT_OFFSET, 
                              PortWidget.PORT_WIDTH + SPOT_OFFSET));
    }

    public PortWidget[] getPortWidgets() {
        ArrayList port_widget_list = new ArrayList();
        for (JGoListPosition pos = getFirstObjectPos(); pos != null;
                                            pos = getNextObjectPos(pos)) {
            JGoObject o = getObjectAtPos(pos);
            if (o instanceof PortWidget) { port_widget_list.add(o); }
        }
        PortWidget[] port_widgets = new PortWidget[port_widget_list.size()];
        port_widget_list.toArray(port_widgets);
        return port_widgets;
    }
    

  
    /**
      * Figure out the dimensions of the rectangle based on
      * the maximum number of ports on the sides
      */
    protected Rectangle determineNodeRectangle (PortProxy[][] _port_order) {
        return determineNodeLayoutRects(_port_order)[5];
    }

        /** This method determines all the associated rectangles needed for the
          * layout of the node widget.  The returned Rectangle[6] contains, in
          * order: { north_edge_port_rect, south_edge_port_rect,
          *          east_edge_port_rect,  west_edge_port_rect,
          *          content_rect,         full_node_rect }.
          */
    Rectangle[] determineNodeLayoutRects(PortProxy[][] _port_order) {
        Dimension content_size = getContentSize();
        Dimension minimum_valid_size = getMinimumValidContentSize();
        content_size.width =
                Math.max(content_size.width, minimum_valid_size.width);
        content_size.height =
                Math.max(content_size.height, minimum_valid_size.height);

        Rectangle[] port_area_rects = determinePortAreaRects(_port_order);

        Rectangle[] port_area_and_content_rects =
                adjustEdgePortRectsForContent(content_size, port_area_rects);

        Rectangle node_rect = new Rectangle(port_area_and_content_rects[0]);
        for (int i = 1; i < port_area_and_content_rects.length; i++) {
            node_rect = node_rect.union(port_area_and_content_rects[i]);
        }
            // this call results in node_rect having a position = (-BORDER,
            // -BORDER), which i believe is as it should be since the borders
            // should not impinge on where the ports get layed out on the grid.
        node_rect.grow(BORDER, BORDER);

        Rectangle[] layout_rects = new Rectangle[6];
        for (int i = 0; i < 5; i++) {
            layout_rects[i] = port_area_and_content_rects[i];
        }
        layout_rects[5] = node_rect;

        return layout_rects;
    }

    public String toString () {
        StringBuffer info = new StringBuffer(m_node_name);
        
        return info.toString();
    }
}

