package edu.caltech.ligo.alfi.widgets;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.widgets.*;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.editor.*;
import com.nwoods.jgo.*;

    /**
     * This class is the widget which represents junctions in an Alfi edit
     * window.
     */
public class JunctionWidget extends GenericPortWidget {

    //**************************************************************************
    //***** constants **********************************************************

    private static final JGoPen JUNCTION_PEN =
                                new JGoPen(JGoPen.SOLID, 1, Color.black);

        // cant set these until inside constructor
    public static final int JUNCTION_SIDE =
                                ConnectionWidget.LOCAL_CONNECTION_WIDTH * 2;
    private final Point POSITION_OFFSET =
        new Point((int) (-1 * JUNCTION_SIDE/2), (int) (-1 * JUNCTION_SIDE/2));

    //**************************************************************************
    //***** member fields ******************************************************

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public JunctionWidget(EditorFrame _editor,
                          JunctionProxy _associated_proxy) {
        super(_editor, _associated_proxy);

        setStyle(StyleEllipse);
        setFromSpot(Center);
        setToSpot(Center);

        setLocationOffset(_associated_proxy.getLocation(), POSITION_OFFSET);
        setSize(JUNCTION_SIDE, JUNCTION_SIDE);

        setPen(JUNCTION_PEN);
        setBrush(new JGoBrush(JGoBrush.SOLID, Color.white));

        setSelectable(true);
        setResizable(false);
        setDraggable(true);

        getAssociatedProxy().addProxyChangeListener(this);
    }


    //**************************************************************************
    //***** methods ************************************************************

    //////////////////////////////////////////////////////////
    ////////// member field access ///////////////////////////

    public JunctionProxy getAssociatedProxy() {
        return ((JunctionProxy) m_associated_proxy);
    }

    public String toString() {
        return getAssociatedProxy().getName();
    }

    public void prepareForSourceConnectionChange() {
        ALFINode container = m_editor.getNodeInEdit();
        JunctionProxy junction = getAssociatedProxy();
        ConnectionProxy source_connection =
                                       junction.getSourceConnection(container);
        if (source_connection != null) {
            GenericPortProxy old_source_gpp = source_connection.getSource();

                // something is wrong if old_source_gpp is not a junction!
            JunctionWidget old_source_jw = m_editor.determineJunctionWidget(
                                               (JunctionProxy) old_source_gpp);
            old_source_jw.prepareForSourceConnectionChange();

                // get path points from the source connection to create reverse
            ConnectionWidget source_cw =
                         m_editor.determineConnectionWidget(source_connection);
            source_cw.reverseDataFlowDirection(m_editor.getView());
        }
    }

    /////////////////////////////////////////////////////////////////////
    ////////// implementations of GenericPortWidget abstract methods ////

        /** Implementation of
          * GenericPortWidget.getPossibleDataTypesForNewConnectionBuild().
          *
          * Here are the rules for the types junctions will return:
          *
          * As a data source:
          *   If there is a source connection, the data type returned will be
          *   the same as the source connection's upstream data type.  If no
          *   source connection exists, then the data type returned is the same
          *   as the data type demanded by the first existing sink connection
          *   which returns a non-DATA_TYPE_1_UNKNOWN data type.  If no such
          *   connection exists, the returned type is DATA_TYPE_1_UNKNOWN.
          *   The hint will always be STANDARD_BUILD.
          *
          * As a data sink:
          *   If there is already a source connection no data types will be
          *   returned as a junction can only support a single source
          *   connection.  Otherwise, the data type returned is the same
          *   as the data type demanded by the first existing sink connection
          *   which returns a non-DATA_TYPE_1_UNKNOWN data type.  If no such
          *   connection exists, the returned type is DATA_TYPE_1_UNKNOWN.
          *   The hint will always be STANDARD_BUILD.
          *
          * In general, the return value will be int[1][2] where [0][0] is the
          * possible data type, and [0][1] is the corresponding hint.  A
          * value of int[0][0] is returned when no possibilities exist.
          */
    int[][] getPossibleDataTypesForNewConnectionBuild(
                                                   boolean _b_as_source_port) {
        JunctionProxy junction = getAssociatedProxy();
        ALFINode container = m_editor.getNodeInEdit();
        ConnectionProxy source_connection =
                                      junction.getSourceConnection(container);
        GenericPortProxy origin = (source_connection != null) ?
                                 source_connection.getOrigin(container) : null;

        int[][] types_and_hints =
                               { { GenericPortProxy.DATA_TYPE_1_UNKNOWN,
                                   ConnectionBuildingWidget.STANDARD_BUILD } };

        if (origin == null) {
            ConnectionProxy[] sink_connections =
                                      junction.getConnections(container, null);
            for (int i = 0; i < sink_connections.length; i++) {
                int data_type =
                    sink_connections[i].determineDownstreamDataType(container);
                if (data_type != GenericPortProxy.DATA_TYPE_1_UNKNOWN) {
                    types_and_hints[0][0] = data_type;
                    break;
                }
            }
        }
        else if (_b_as_source_port) {
            types_and_hints[0][0] =
                        source_connection.determineUpstreamDataType(container);
        }
        else { types_and_hints = new int[0][0]; }

        return types_and_hints;
    }

    public String generateDescription() {
        return "(Junction) " + getAssociatedProxy().getName();
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////// implementations of AlfiWidgetIF methods /////////////////////////

        /** AlfiWidgetIF method. */
    public EditorFrame getEditor() { return m_editor; }

        /** AlfiWidgetIF method. */
    public Object getAssociatedProxyObject() { return getAssociatedProxy(); }

    ////////////////////////////////////////
    ////////// event processing ////////////

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseDown(int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
        if (_view instanceof ALFIView) {
            ALFIView alfiview = (ALFIView) _view;

                // LMB ops
            if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {


                JunctionProxy junction = getAssociatedProxy();
                if ((junction.group_get()) != null &&
                    (_modifiers & InputEvent.CTRL_MASK) != 0) {
                    
                    // This junction is a part of a group.
                    // Send out an event that all elements need to be selected
                    GroupElementModEvent event = 
                        new GroupElementModEvent(this, 
                            GroupElementModEvent.GROUP_SELECT,  
                            junction.group_get()); 
                    junction.fireGroupElementModEvent(event);
                    return false;
                }
   
            }
                // MMB ops
            else if ((_modifiers & InputEvent.BUTTON2_MASK) != 0) {
                JunctionProxy[][] junction_groups = alfiview.getOwnerFrame().
                              getNodeInEdit().junction_getAllJunctionNetworks();
                for (int i = 0; i < junction_groups.length; i++) {
                    for (int j = 0; j < junction_groups[i].length; j++) {
                        System.err.println("junction_groups[" + i + "][" + j +
                            "] = " + junction_groups[i][j]);
                    }
                }
                return true;
            }
                // RMB operations
            else if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
                alfiview.pickObject(this);
                alfiview.doWidgetPopupMenu(this, _modifiers, _dc, _vc);
                return true;
            }
        }
        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseUp(int _modifiers,
                             Point _dc, Point _vc, JGoView _view) {
        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseMove(int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
        return false;
    }

        /**
         * A left mouse click on a junction either starts the construction of a
         * connection, or completes the construction of a connection which is
         * currently under construction.
         */
    public boolean doMouseClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        if (_view instanceof ALFIView) {
            final ALFIView alfi_view = (ALFIView) _view;
            _view.getSelection().clearSelection();

                // LMB operations
            if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {

                    // a temp object added only to the view
                final ConnectionBuildingWidget cbw =
                        new ConnectionBuildingWidget(this, alfi_view);
                alfi_view.setActiveObject(cbw);
                alfi_view.addObjectAtHead(cbw);

                    // wait a moment before changing cursor

                java.util.Timer timer = new java.util.Timer();
                timer.schedule(
                    new TimerTask() {
                        public void run() {
                            if (alfi_view.getActiveObject() == cbw) {
                                alfi_view.setCursor(new Cursor(
                                                      Cursor.CROSSHAIR_CURSOR));
                            }
                        }
                    }, 250);

                return true;    // consume event
            }
            else if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
                alfi_view.pickObject(this);
                alfi_view.doWidgetPopupMenu(this, _modifiers, _dc, _vc);
            }

        }

        return false;
    }

        /** Diagnostics only. */
    public boolean doMouseDblClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        Point center = new Point(getLocation());
        center.translate(JUNCTION_SIDE/2, JUNCTION_SIDE/2);
        System.err.println("(" + this + ").doMouseDblClick(...): Centered at " +
            center);

        JGoListPosition pos = getFirstLinkPos();
        while (pos != null) {
            System.err.println("  " + getLinkAtPos(pos));
            pos = getNextLinkPos(pos);
        }

        return true;
    }

    public boolean doUncapturedMouseMove(int _flags, Point _dc, Point _vc,
                                                                JGoView _view) {
        if (_view instanceof ALFIView) {
            ((ALFIView) _view).getOwnerFrame().setStatusText(toString());
        }
        return true;    // consume event
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////// ProxyChangeEventListener implementations ////////////////////////

        /** ProxyChangeEventListener method. */
    public void proxyChangePerformed(ProxyChangeEvent e) {
        if (e.getSource() == getAssociatedProxy()) {
            setLocationOffset(getAssociatedProxy().getLocation(),
                                                              POSITION_OFFSET);
        }
        else { Alfi.warn("Invalid ProxyChangeEvent source."); }
    }
}
