package edu.caltech.ligo.alfi.widgets;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.event.*;
import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.widgets.*;
import edu.caltech.ligo.alfi.tools.*;
import com.nwoods.jgo.*;

    /**
     * This class is the widget which represents connections in an Alfi edit
     * window.
     */
public class ConnectionWidget extends JGoLink implements AlfiWidgetIF,
                                       ProxyChangeEventListener,
                                       UpstreamDataChannelChangeEventListener,
                                       ConnectionRedrawnEventSource {

    //**************************************************************************
    //***** constants **********************************************************

    public static final int LOCAL_CONNECTION_WIDTH = 3;
    public static final int INHERITED_CONNECTION_WIDTH = 1;

    public static final int SEGMENT_DATA_FLOW_NORTH    = 0;
    public static final int SEGMENT_DATA_FLOW_SOUTH    = 1;
    public static final int SEGMENT_DATA_FLOW_EAST     = 2;
    public static final int SEGMENT_DATA_FLOW_WEST     = 3;
    public static final int SEGMENT_DATA_FLOW_INVALID  = 4;

    public static final int ARROW_LENGTH = 9;
    public static final int ARROW_WIDTH = 7;

    //**************************************************************************
    //***** local exception classes ********************************************


    //**************************************************************************
    //***** member fields ******************************************************

    final EditorFrame m_editor;

        /** The bookkeeper element signaling the existance of this connection.*/
    final ConnectionProxy m_associated_proxy;

        /** Different for locally added connections vs inherited ones. */
    final int m_connection_width;

        /** Flags which if any of this connections segments is being moved. */
    private int m_active_segment;

        /** Used if/when blink() is called. */
    private java.util.Timer m_blink_timer;

        /** Stores a copy of the path points.  This might be queried just before
          * any connection adjustment to help make a good new path.
          */
    private Vector m_previous_path_points_vector;

        /**
         * The two booleans mb_mouse_drag_is_armed and mb_mouse_drag_has_occured
         * are used to create a workaround on a "feature" in JGo which sometimes
         * places the Shift and/or Ctrl modifiers on a mouse event even if they
         * weren't actually used.  We assume this is used internally in JGo
         * event processing and do not want to try to fix it in case we break
         * other things.  Basically, we just want to know when a mouse drag has
         * occured.  These two booleans are set and unset in doMouseUp,
         * doMouseDown, and doMouseMove.  They should not be read or written
         * anywhere else.
         */
    private boolean mb_mouse_drag_is_armed;

        /** See notes on mb_mouse_drag_is_armed. */
    private boolean mb_mouse_drag_has_occured;

        /**
         * After a connection is selected, the view is set in gainedSelection
         * and is used in order to track whether this connection is currently
         * selected.  Before such time, this is null.  It should not be used
         * for other purposes.
         *
         * gainedSelection and lostSelection seem otherwise unreliable for this
         * tracking when used on their own.
         */
    private JGoView m_view = null;

        /** See portChange(). */
    private HashMap m_portChange_timer_map;
    private java.util.Timer m_portChange_timer;
    private boolean mb_suspend_portChange_calls;

    private boolean mb_disable_updates;

        /** Used in conjunction with ConnectionRedrawnEventSource. */
    private HashMap m_connection_redrawn_event_listeners_map;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public ConnectionWidget(EditorFrame _editor,
                                           ConnectionProxy _associated_proxy) {
        super(_editor.determineSourceWidget(_associated_proxy),
                               _editor.determineSinkWidget(_associated_proxy));

        assert ((_editor != null) && (_associated_proxy != null));

            // init final members
        m_editor = _editor;
        m_associated_proxy = _associated_proxy;
        ALFINode container = m_editor.getNodeInEdit();
        boolean b_is_local =
                        container.connection_containsLocal(m_associated_proxy);
        m_connection_width = (b_is_local) ? LOCAL_CONNECTION_WIDTH :
                                            INHERITED_CONNECTION_WIDTH;

        syncPathPoints();
        m_previous_path_points_vector = null;

        m_associated_proxy.addProxyChangeListener(this);
        m_associated_proxy.addUpstreamDataChannelChangeEventListener(this);
        m_connection_redrawn_event_listeners_map = new HashMap();

        m_active_segment = -1;
        m_portChange_timer_map = new HashMap();
        m_portChange_timer = new java.util.Timer();
        mb_suspend_portChange_calls = false;

        mb_disable_updates = false;
        updateDataType();

        setOrthogonal(false);
        setSelectable(false);
//        setArrowHeads(true, false);	// at least 1 of these needs to be true
        setArrowHeads(false, true);	// at least 1 of these needs to be true
    }

    //**************************************************************************
    //***** static methods *****************************************************


    //**************************************************************************
    //***** methods ************************************************************

        /** See blink(boolean, Color.red, -1). */
    public void blink(boolean _blink) { blink(_blink, null, -1); }

        /** Cause (or stop) the connection to blink.  If _blink_color is null,
          * the default color is red.  The blinks are at 1/2 second intervals,
          * and _blink_time specifies how long the blinking should occur before
          * stopping.  If _blink_time <= 0, then there is no termination time.
          */
    public void blink(boolean _blink, Color _blink_color, int _blink_time) {
        updateDataType();
        final JGoPen normal_pen = getPen();
        if (m_blink_timer != null) {
            m_blink_timer.cancel();
            m_blink_timer = null;
        }   

        if (_blink) {
            Color color = (_blink_color != null) ? _blink_color : Color.red;
            if (normal_pen.getColor().equals(color)) { color = Color.white; }
            final JGoPen blink_pen = new JGoPen(normal_pen.getStroke(), color);
            final ConnectionWidget cw = this;

            m_blink_timer = new java.util.Timer(true);

            m_blink_timer.schedule(
                new TimerTask() {
                    public void run() {
                        JGoPen old_pen = cw.getPen();
                        JGoPen new_pen =
                              (old_pen == normal_pen) ? blink_pen : normal_pen;
                        cw.setPen(new_pen);
                    }
                }, 0L, 750L);

            if (_blink_time > 0) {
                java.util.Timer blink_cancel_timer = new java.util.Timer(true);
                blink_cancel_timer.schedule(
                    new TimerTask() {
                        public void run() { cw.blink(false); }
                    }, _blink_time);
            }
        }
    }

        /** See getPreviousPathPoints(). */
    public void capturePathPoints() {
        m_previous_path_points_vector = copyPoints();
    }

        /** Will return whatever path was last saved by a call to
         * capturePathPoints().  Returns null if no such call has been made.
         */
    public Point[] getPreviousPathPoints() {
        if (m_previous_path_points_vector == null) { return null; }
        else {
            Point[] points = new Point[m_previous_path_points_vector.size()];
            m_previous_path_points_vector.toArray(points);
            return points;
        }
    }

        /** Use when previous path points should not be taken into account on
          * next path recalculation.
          */
    public void clearPreviousPathPoints() {
        m_previous_path_points_vector = null;
    }

        /** WARNING!: The JGo "From" port is not always the Alfi source port.
          * You must always refer back to the bookkeeping (the ConnectionProxy)
          * to make this determination.
          */
    public GenericPortWidget getSourceWidget() {
        GenericPortProxy source = m_associated_proxy.getSource();
        MemberNodeProxy source_member = m_associated_proxy.getSourceMember();

        return m_editor.determineGenericPortWidget(source, source_member);
    }

        /** WARNING!: The JGo "To" port is not always the Alfi sink port.  You
          * must always refer back to the bookkeeping (the ConnectionProxy) to
          * make this determination.
          */
    public GenericPortWidget getSinkWidget() {
        GenericPortProxy sink = m_associated_proxy.getSink();
        MemberNodeProxy sink_member = m_associated_proxy.getSinkMember();

        return m_editor.determineGenericPortWidget(sink, sink_member);
    }

        /** JGo's "To" port is not always the connection's sink port.  See
          * getSinkWidget().  So this method is confusing and should not be used
          * in Alfi.  But since this method is a core JGo method, it cannot
          * be trivially overridden.  This override merely returns
          * super.getToPort(), and is here just to provide this documentation.
          */
    public JGoPort getToPort() { return super.getToPort(); }

        /** JGo's "From" port is not always the connection's source port.  See
          * getSourceWidget().  So this method is confusing and should not be
          * used in Alfi.  But since this method is a core JGo method, it cannot
          * be trivially overridden.  This override merely returns
          * super.getFromPort(), and is here just to provide this documentation.
          */
    public JGoPort getFromPort() { return super.getFromPort(); }

        /** Determines the initial data flow direction out of the source port.*/
    public int determineInitialDirection() {
        Point p0 = getPoint(0);
        Point p1 = getPoint(1);
        if (p1.equals(p0)) {
            for (int i = 2; i < getNumPoints(); i++) {
                p1 = getPoint(i);
                if (! p1.equals(p0)) { break; }
            }
        }
        return determineSegmentDirection(p0, p1);
    }

        /** Determines the final data flow direction into the sink port.*/
    public int determineLastDirection() {
        int last_index = getNumPoints() - 1;
        Point p_last = getPoint(last_index);
        Point p_next_to_last = getPoint(last_index - 1);
        if (p_next_to_last.equals(p_last)) {
            for (int i = last_index - 2; i >= 0; i--) {
                p_next_to_last = getPoint(i);
                if (! p_next_to_last.equals(p_last)) { break; }
            }
        }
        return determineSegmentDirection(p_next_to_last, p_last);
    }

        /** Creates a set of directional arrows to be pasted onto this widget.*/
    private Polygon[] createDirectionalStickers() {
        Vector points_vec = copyPoints();
        ArrayList sticker_list = new ArrayList();
        Iterator i = points_vec.iterator();
        Point p_1 = (Point) i.next();
        while (i.hasNext()) {
            Point p_0 = p_1;
            p_1 = (Point) i.next();
            int direction = determineSegmentDirection(p_0, p_1);
            Polygon sticker = createDirectionalSticker(p_0, p_1, direction);
            if (sticker != null) { sticker_list.add(sticker); }
        }

        Polygon[] stickers = new Polygon[sticker_list.size()];
        sticker_list.toArray(stickers);
        return stickers;
    }

    public static int determineSegmentDirection(Point _p1, Point _p2) {
        int direction = SEGMENT_DATA_FLOW_INVALID;
        if (_p1.x == _p2.x) {
            if (_p1.y > _p2.y) { direction = SEGMENT_DATA_FLOW_NORTH; }
            else if (_p1.y < _p2.y) { direction = SEGMENT_DATA_FLOW_SOUTH; }
        }
        else if (_p1.y == _p2.y) {
            if (_p1.x > _p2.x) { direction = SEGMENT_DATA_FLOW_WEST; }
            else if (_p1.x < _p2.x) { direction = SEGMENT_DATA_FLOW_EAST; }
        }
        return direction;
    }

    private Polygon createDirectionalSticker(Point _p_0, Point _p_1,
                                                              int _direction) {
        int MINIMUM_SEGMENT_LENGTH = GridTool.GRID_SPACING * 4;
        Polygon sticker = new Polygon();
        int x = -1, y = -1, dx = -1, dy = -1;

        if (_direction == SEGMENT_DATA_FLOW_NORTH) {
            dy = _p_0.y - _p_1.y;
            if (dy > MINIMUM_SEGMENT_LENGTH) {
                sticker.addPoint(0, ARROW_LENGTH - 1);
                sticker.addPoint(ARROW_WIDTH - 1, ARROW_LENGTH - 1);
                sticker.addPoint((ARROW_WIDTH - 1) / 2, 0);

                x = _p_0.x - (ARROW_WIDTH / 2);
                y = _p_0.y - (dy / 2) - (ARROW_LENGTH / 2);
            }
            else { sticker = null; }
        }
        else if (_direction == SEGMENT_DATA_FLOW_SOUTH) {
            dy = _p_1.y - _p_0.y;
            if (dy > MINIMUM_SEGMENT_LENGTH) {
                sticker.addPoint(0, 0);
                sticker.addPoint((ARROW_WIDTH - 1) / 2, ARROW_LENGTH - 1);
                sticker.addPoint((ARROW_WIDTH - 1), 0);

                x = _p_0.x - (ARROW_WIDTH / 2);
                y = _p_0.y + (dy / 2) - (ARROW_LENGTH / 2);
            }
            else { sticker = null; }
        }
        else if (_direction == SEGMENT_DATA_FLOW_EAST) {
            dx = _p_1.x - _p_0.x;
            if (dx > MINIMUM_SEGMENT_LENGTH) {
                sticker.addPoint(0, 0);
                sticker.addPoint(0, (ARROW_WIDTH - 1));
                sticker.addPoint(ARROW_LENGTH - 1, (ARROW_WIDTH - 1) / 2);

                x = _p_0.x + (dx / 2) - (ARROW_LENGTH / 2);
                y = _p_0.y - (ARROW_WIDTH / 2);
            }
            else { sticker = null; }
        }
        else if (_direction == SEGMENT_DATA_FLOW_WEST) {
            dx = _p_0.x - _p_1.x;
            if (dx > MINIMUM_SEGMENT_LENGTH) {
                sticker.addPoint(0, (ARROW_WIDTH - 1) / 2);
                sticker.addPoint(ARROW_LENGTH - 1, ARROW_WIDTH - 1);
                sticker.addPoint(ARROW_LENGTH - 1, 0);

                x = _p_0.x - (dx / 2) - (ARROW_LENGTH / 2);
                y = _p_0.y - (ARROW_WIDTH / 2);
            }
            else { sticker = null; }
        }
        else { sticker = null; }

        if (sticker != null) { sticker.translate(x, y); }
        return sticker;
    }

        /** After connections are dead somehow, disable any updating. */
    public void disableUpdates() { mb_disable_updates = true; }

        /** Data type of a connection may change in rare instances. */
    public void updateDataType() {
        if (mb_disable_updates) { return; }

        final int UNRESOLVED = GenericPortProxy.DATA_TYPE_1_UNRESOLVED;
        ALFINode container = m_editor.getNodeInEdit();
        ConnectionProxy connection = m_associated_proxy;

        int data_type = UNRESOLVED;
        if (connection instanceof BundleProxy) {
            data_type = GenericPortProxy.DATA_TYPE_ALFI_BUNDLE;
        }
        else { data_type = connection.determineUpstreamDataType(container); }

        int color_type = (data_type != UNRESOLVED) ? data_type :
                             connection.determineDownstreamDataType(container);
        Color line_color = PortWidget.getDataTypeColor(color_type);

        Stroke stroke = null;
        if (data_type == UNRESOLVED) {
            int spacer = Math.max(m_connection_width, 3);
            float[] dash_array = { m_connection_width, spacer };
            stroke = new BasicStroke(m_connection_width, BasicStroke.CAP_BUTT,
                                   BasicStroke.JOIN_BEVEL, 10f, dash_array, 0);
        }
        else {
            stroke = new BasicStroke(m_connection_width, BasicStroke.CAP_BUTT,
                                                       BasicStroke.JOIN_BEVEL);
        }
        setPen(new JGoPen(stroke, line_color));
    }

        /**
         * This method takes a point somewhere along the connection route and
         * returns the "upper" and "lower" paths, the former, a vector of points
         * describing the path from the connection source to the split point,
         * and the latter a vector of points describing the path from the split
         * point to the connection sink.  The point _raw_split_point can be any
         * point, and the actual split point is determined via the method
         * getPathSegmentAndCanonicalPathPoint().  The actual split point will
         * always be a grid point lying directly on the connection path.
         */
    public Vector[] determineSplitPaths(Point _raw_split_point) {
        Point[] segment_and_split_point =
                    getPathSegmentAndCanonicalPathPoint(_raw_split_point);
        Point segment_upper_point = segment_and_split_point[0];
        Point split_point =
                    GridTool.getNearestGridPoint(segment_and_split_point[2]);

        Vector full_path_vector = copyPoints();
        Vector upper_path_vector = new Vector();
        Vector lower_path_vector = new Vector();

        Iterator i = full_path_vector.iterator();

            // determine the upper path
        while (i.hasNext()) {
            Point p = (Point) i.next();
            upper_path_vector.add(p);

                // is p at the start of the segment that split_point lies on?
            if (p.equals(segment_upper_point)) {
                    // special case if split_point is at a corner: in this case,
                    //   segment_upper_point, segment_lower_point (not used
                    //   here), and split_point are all equal.
                if (! split_point.equals(p)) {
                    upper_path_vector.add(split_point);
                }

                break;
            }
        }

            // the rest of the points are in the lower path
        lower_path_vector.add(split_point);
        while (i.hasNext()) { lower_path_vector.add((Point) i.next()); }

        Vector[] upper_and_lower = { upper_path_vector, lower_path_vector };
        return upper_and_lower;
    }

        /**
         * Given a point somewhere in the rectangle formed by a length between
         * the two endpoints of a path segment, and m_connection_width,
         * determine the point that would be in the center of the width.  I.e.,
         * it would lie at some place on the single-pixel wide line between the
         * two endpoints.  The array of points returned include the two end
         * points of the segment the passed point lies within (upstream point
         * first), and in the third position, the "canonical" path point.  For
         * example, if _raw_point = (23,67), first the two endpoints would be
         * determined, say they are points (21,15) and (21,81), then the
         * so-called canonical path point would be determined to be (21,67), and
         * this would be the third and final point in the array returned.  An
         * overriding instance occurs if _raw_point is close to one of the
         * endpoints.  If within a square of sides m_connection_width centered
         * on either endpoint, then the canonical point returned is defined as
         * the corresponding endpoint itself, and since the actual containing
         * segment is ambiguous in this case, the first two values in the array
         * are set equal to the that same endpoint (I.e., all three points in
         * the array would be the same.)
         */
    public Point[] getPathSegmentAndCanonicalPathPoint(Point _raw_point) {
        Point[] endpoints_and_canonical_path_point = { null, null, null };

        int segment_no = getSegmentNearPoint(_raw_point);
        Point p1 = getPoint(segment_no);
        Point p2 = getPoint(segment_no + 1);
        if (segmentIsVertical(p1, p2)) {
            if (Math.abs(p1.y - _raw_point.y) <= m_connection_width) {
                endpoints_and_canonical_path_point[0] = new Point(p1);
                endpoints_and_canonical_path_point[1] = new Point(p1);
                endpoints_and_canonical_path_point[2] = new Point(p1);
            }
            else if (Math.abs(p2.y - _raw_point.y) <= m_connection_width) {
                endpoints_and_canonical_path_point[0] = new Point(p2);
                endpoints_and_canonical_path_point[1] = new Point(p2);
                endpoints_and_canonical_path_point[2] = new Point(p2);
            }
            else {
                endpoints_and_canonical_path_point[0] = new Point(p1);
                endpoints_and_canonical_path_point[1] = new Point(p2);
                endpoints_and_canonical_path_point[2] =
                            new Point(p1.x, _raw_point.y);
            }
        }
        else {
            if (Math.abs(p1.x - _raw_point.x) <= m_connection_width) {
                endpoints_and_canonical_path_point[0] = new Point(p1);
                endpoints_and_canonical_path_point[1] = new Point(p1);
                endpoints_and_canonical_path_point[2] = new Point(p1);
            }
            else if (Math.abs(p2.x - _raw_point.x) <= m_connection_width) {
                endpoints_and_canonical_path_point[0] = new Point(p2);
                endpoints_and_canonical_path_point[1] = new Point(p2);
                endpoints_and_canonical_path_point[2] = new Point(p2);
            }
            else {
                endpoints_and_canonical_path_point[0] = new Point(p1);
                endpoints_and_canonical_path_point[1] = new Point(p2);
                endpoints_and_canonical_path_point[2] =
                            new Point(_raw_point.x, p1.y);
            }
        }
        return endpoints_and_canonical_path_point;
    }

    public String toString() {
        ALFINode container = m_editor.getNodeInEdit();
        ConnectionProxy connection = m_associated_proxy;
        StringBuffer description_sb = new StringBuffer();

        GenericPortProxy source = connection.getSource();
        MemberNodeProxy source_owner = connection.getSourceMember();
        GenericPortProxy sink = connection.getSink();
        MemberNodeProxy sink_owner = connection.getSinkMember();

        description_sb.append(source.generateSnippetForConnectionDescription(
                                                container, connection, false));

        String _arrow_ = (connection instanceof BundleProxy) ?
                                                   " => " : connection._ARROW_;
        description_sb.append(_arrow_);

        description_sb.append(sink.generateSnippetForConnectionDescription(
                                                container, connection, false));

        return description_sb.toString();
    }

    public String toString(boolean _b_verbose) {
        ALFINode container = m_editor.getNodeInEdit();
        String s = null;
        if (_b_verbose) {
            s = "  getSourceWidget() = " + getSourceWidget() + "\n";
            s += "  getSinkWidget() = " + getSinkWidget() + "\n";
            s += "\twidget path points:\n";
            Vector v = copyPoints();
            for (int i = 0; i < v.size(); i++) {
                s += "\t\tp[" + i + "] = " + v.elementAt(i) + "\n";
            }

            s += "\t" + getAssociatedProxy().
                         generateParserRepresentation_guiInfo("", true) + "\n";

            Vector path_points_vector =
                   getAssociatedProxy().calculatePathPoints(
                   m_editor.getNodeInEdit(), getSourcePoint(), getSinkPoint());
            Point[] path_points = new Point[path_points_vector.size()];
            path_points_vector.toArray(path_points);

            s += "\tproxy path points:\n";
            for (int i = 0; i < path_points.length; i++) {
               s += "\t\tpath_points[" + i + "] = " + path_points[i] + "\n";
            }
        }
        else { s = toString(); }

        return s;
    }

        /** Smoothes the connection. */
    public void smooth() { getAssociatedProxy().setPathDefinition(null); }

        /** Gets the sink JGoPort and gets its "to" point. */
    private Point getSinkPoint() { return getToPort().getToLinkPoint(); }

        /** Gets the source JGoPort and gets its "from" point. */
    private Point getSourcePoint() {
        return getFromPort().getFromLinkPoint();
    }

    private static boolean segmentIsHorizontal(Point2D p1, Point2D p2) {
        if (p1.getY() == p2.getY()) { return true; }
        else { return false; }
    }

    private static boolean segmentIsHorizontal(Line2D _line) {
        if (_line.getY1() == _line.getY2()) { return true; }
        else { return false; }
    }

    private static boolean segmentIsVertical(Point2D p1, Point2D p2) {
        if (p1.getX() == p2.getX()) { return true; }
        else { return false; }
    }

    private static boolean segmentIsVertical(Line2D _line) {
        if (_line.getX1() == _line.getX2()) { return true; }
        else { return false; }
    }

    private static boolean segmentsAreParallel(Line2D _seg_1, Line2D _seg_2) {
        boolean b_seg_1_horizontal = segmentIsHorizontal(_seg_1);
        boolean b_seg_2_horizontal = segmentIsHorizontal(_seg_2);
        if ((b_seg_1_horizontal && b_seg_2_horizontal) ||
                                (!b_seg_1_horizontal && !b_seg_2_horizontal)) {
            return true;
        }
        else { return false; }
    }

    private static boolean segmentsAreOrthogonal(Line2D _seg_1, Line2D _seg_2) {
        return (! segmentsAreParallel(_seg_1, _seg_2));
    }

        /**
         * AlfiWidgetIF method- Was this connection locally added to its
         * container, or has it been inherited?
         */
    public boolean isLocal() {
        ALFINode container = m_editor.getNodeInEdit();
        return container.connection_containsLocal(getAssociatedProxy());
    }

        /** Picks which resize cursor is appropriate for the given segment. */
    public Cursor determineResizeCursor(Point _dc) {
        if (m_active_segment != -1) {
            if (segmentIsHorizontal(getPoint(m_active_segment),
                                    getPoint(m_active_segment + 1))) {
                return Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);
            }
            else { return Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR); }
        }
        else {
            System.err.println("(" + this + ").determineResizeCursor(" + _dc +
                "):\n\tWARNING: Invalid call to this method.");
            return Cursor.getDefaultCursor();
        }
    }

        /**
         * This method determines the rectangle which bounds all the parts of
         * the connection excepting any segment directly attached to a port
         * on the edge of the box.  The reason for this exception is that these
         * segments will shrink or lengthen when an edit window resize occurs.
         */
    public Rectangle determineAdjustedBoundingRect() {
        ConnectionProxy proxy = getAssociatedProxy();
        Vector path_points_vector = proxy.calculatePathPoints(
                   m_editor.getNodeInEdit(), getSourcePoint(), getSinkPoint());
        Point[] path_points = new Point[path_points_vector.size()];
        path_points_vector.toArray(path_points);

        int start_i = 0;
        if (proxy.getSourceMember() == null) { start_i += 1; }
        int end_i = path_points.length - 1;
        if (proxy.getSinkMember() == null) { end_i -= 1; }

            // case of only 2 points, straight line from sink to source
        if (start_i > end_i) { return null; }
        else {
            Point ul = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
            Point lr = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
            for (int i = start_i; i <= end_i; i++) {
                ul.x = Math.min(ul.x, path_points[i].x);
                ul.y = Math.min(ul.y, path_points[i].y);
                lr.x = Math.max(lr.x, path_points[i].x);
                lr.y = Math.max(lr.y, path_points[i].y);
            }
            Rectangle r = new Rectangle(ul);
            r.width = lr.x - ul.x + 1;
            r.height = lr.y - ul.y + 1;
            r.translate(-1 * m_connection_width / 2, -1 * m_connection_width/2);
            r.grow(m_connection_width, m_connection_width);
            return r;
        }
    }

        /**
         * Sometimes multiple calls to portChange are being made, and the
         * initial calls sometimes have bad points.  This method is used to
         * stop the overlapping calls and only process the final call.
         */
    public void suspendPortChangeCalls(boolean _b_suspend_calls) {
        mb_suspend_portChange_calls = _b_suspend_calls;
    }

        /** Overrides JGoLink method.  Called when source/sink port is moved. */
    public void portChange(JGoPort _port, int _hint, int _previous_int,
                                                      Object _previous_object) {
            // this is a kluge to prevent overlapping calls to portChange,
            // which occurs during a rename, and the initial calls place the
            // port at entirely incorrect locations (??!!??)
        if (mb_suspend_portChange_calls) {
            if (! m_portChange_timer_map.containsKey(_port)) {
                final JGoPort port_widget = _port;
                m_portChange_timer_map.put(port_widget, null);
                TimerTask task = new TimerTask() {
                    public void run() {
                        if (! mb_suspend_portChange_calls) {
                            syncPathPoints();
                            m_portChange_timer_map.remove(port_widget);
                            cancel();
                        }
                    }
                };
                m_portChange_timer.schedule(task, 0, 50);
            }
        }
        else { syncPathPoints(); }
    }

        /** Uses the definition in the associated ConnectionProxy and the
         * locations of the to and from ports to set the path points.
         */
    private void syncPathPoints() {
        Point new_source_p = getSourcePoint();
        Point new_sink_p = getSinkPoint();

        Vector path_points = getAssociatedProxy().calculatePathPoints(
                           m_editor.getNodeInEdit(), new_source_p, new_sink_p);
        setPoints(path_points);
    }

    public boolean isSelected() {
        if (m_view == null) { return false; }
        else { return m_view.getSelection().isInSelection(this); }
    }

        /**
         * Only used to get a handle on the view responsible for the selection.
         */
    protected void gainedSelection(JGoSelection _selection) {
        m_view = _selection.getView();
        super.gainedSelection(_selection);
    }

        /**
         * Checks if any part of the path doubles back on itself.  Probably not
         * something a user would want.
         */
    private boolean pathDoublesBackOnSelf() {
        Vector path_points = copyPoints();
        for (int i = 0; i < (path_points.size() - 2); i++) {
            Point p_1 = (Point) path_points.get(i);
            Point p_2 = (Point) path_points.get(i + 1);
            Point p_3 = (Point) path_points.get(i + 2);
            if ((p_3.x == p_1.x) && (p_2.x == p_1.x)) {
                if (p_3.y >= p_1.y) {
                    if ((p_2.y >= p_3.y) || (p_2.y <= p_1.y)) { return true; }
                }
                else {
                    if ((p_2.y <= p_3.y) || (p_2.y >= p_1.y)) { return true; }
                }
            }
            else if ((p_3.y == p_1.y) && (p_2.y == p_1.y)) {
                if (p_3.x >= p_1.x) {
                    if ((p_2.x >= p_3.x) || (p_2.x <= p_1.x)) { return true; }
                }
                else {
                    if ((p_2.x <= p_3.x) || (p_2.x >= p_1.x)) { return true; }
                }
            }
        }
        return false;
    }

        /** This method determines all intersection points occuring in a set
          * of connection widgets.  Unfortunately, as implemented, this method
          * also returns shared corners where there is no actual deviation in
          * paths.  But to fix this would make it less efficient, so leaving
          * until it can't be worked around.
          */
    public static Point[] getAllIntersections(ConnectionWidget[] _cws) {
        HashMap point_map = new HashMap();

        for (int i = 0; i < _cws.length - 1; i++) {
            Line2D[] segments_1 = _cws[i].getPathSegments();
            for (int j = i + 1; j < _cws.length; j++) {
                Line2D[] segments_2 = _cws[j].getPathSegments();
                for (int k = 0; k < segments_1.length; k++) {
                    Line2D seg_1 = segments_1[k];
                    for (int m = 0; m < segments_2.length; m++) {
                        Line2D seg_2 = segments_2[m];
                        if (seg_1.intersectsLine(seg_2) &&
                                         segmentsAreOrthogonal(seg_1, seg_2)) {
                            Rectangle2D bounds_1 = seg_1.getBounds2D();
                            Rectangle2D bounds_2 = seg_2.getBounds2D();
                            Rectangle2D intersect =
                                         bounds_1.createIntersection(bounds_2);
                            Point intersection =
                                           intersect.getBounds().getLocation();

                                // ensures each point only has a single entry
                            String key = intersection.toString();
                            point_map.put(key, intersection);
                        }
                    }
                }
            }
        }
        Point[] intersections = new Point[point_map.size()];
        point_map.values().toArray(intersections);
        return intersections;
    }

        /** This method determines the furthest (in distance from this
          * ConnectionWidget's source) intersection point (if any) between this
          * and any of the ConnectionWidgets in _other_cws.  If
          * _b_including_end_point is false, any intersection point at the end
          * of this ConnectionWidget`s path is not taken into account.  The
          * same logic applies to the use of _b_including_start_point.
          *
          * Returns null if no intersection is found.
          */
    public Object[] determineLastIntersection(ConnectionWidget[] _other_cws,
            boolean _b_including_start_point, boolean _b_including_end_point) {
        Line2D[] segments_1 = getPathSegments();
        Point start = getStartPoint();
        Point end = getEndPoint();
        int max_distance_along_segment = -1;
        Point last_intersection_point = null;
        ConnectionWidget intersected_cw = null;
        for (int i = segments_1.length - 1; i >= 0; i--) {
            Rectangle2D bounds_1 = segments_1[i].getBounds2D();
            for (int j = 0; j < _other_cws.length; j++) {
                Line2D[] other_cw_segments = _other_cws[j].getPathSegments();
                for (int k = 0 ; k < other_cw_segments.length; k++) {
                    Rectangle2D bounds_2 = other_cw_segments[k].getBounds2D();
                    Rectangle2D intersection =
                                         bounds_1.createIntersection(bounds_2);
                    Point crossing = intersection.getBounds().getLocation();
                    if ((crossing.equals(end) && (! _b_including_end_point)) ||
                                  (crossing.equals(start) &&
                                               (! _b_including_start_point))) {
                        continue;
                    }
                    else {
                        int distance = calculateDistanceAlongPath(crossing);
                        if (distance > max_distance_along_segment) {
                            last_intersection_point = crossing;
                            intersected_cw = _other_cws[j];
                            max_distance_along_segment = distance;
                        }
                    }
                }
            }
            if (last_intersection_point != null) { break; }
        }

        if ((last_intersection_point != null) && (intersected_cw != null)) {
            Object[] info = { last_intersection_point, intersected_cw };
            return info;
        }
        else { return null; }
    }

        /** This method determines the distance (in pixels) down this
          * ConnectionWidget's path that a point somewhere on that path sits.
          * -1 is returned if the point is not on the path.
          */
    private int calculateDistanceAlongPath(Point _point_on_path) {
        int length = 0;
        Line2D[] segments = getPathSegments();
        for (int i = 0; i < segments.length; i++) {
            if (segments[i].ptSegDist(_point_on_path) == 0) {
                length += _point_on_path.distance(segments[i].getP1());
                return length;
            }
            else {
                length += segments[i].getP2().distance(segments[i].getP1());
            }
        }

            // _point_on_path was actually not on the path anywhere
        return -1;
    }

        /** This method returns an array of Line2Ds which are the segments of
          * the connection's path from source to sink.
          */
    private Line2D[] getPathSegments() {
        Vector points = getAssociatedProxy().calculatePathPoints(
                   m_editor.getNodeInEdit(), getSourcePoint(), getSinkPoint());
        Line2D[] segments = new Line2D[points.size() - 1];
        Iterator i = points.iterator();
        Point p_1 = (Point) i.next();
        int count = 0;
        while (i.hasNext()) {
            Point p_2 = (Point) i.next();
            segments[count] = new Line2D.Double(p_1, p_2);
            count++;
            p_1 = p_2;
        }
        return segments;
    }


    /////////////////////////////////
    // member field access //////////

    public ConnectionProxy getAssociatedProxy() {
        return m_associated_proxy;
    }

        /** AlfiWidgetIF method. */
    public Object getAssociatedProxyObject() { return getAssociatedProxy(); }

        /** AlfiWidgetIF method. */
    public EditorFrame getEditor() { return m_editor; }

    //////////////////////////////
    // event processing //////////

        /** Start a connection segment reposition. */
    public boolean doMouseDown(int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
        if (_view instanceof ALFIView) {
            ALFIView alfiview = (ALFIView) _view;

                // See notes on mb_mouse_drag_is_armed.
            mb_mouse_drag_is_armed = true;
            mb_mouse_drag_has_occured = false;

            alfiview.pickObject(this);

                // LMB operations
            if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
                if (! isLocal()) { return true; }
                else {
                        // make sure previous path points ignored for this op
                    clearPreviousPathPoints();

                    m_active_segment = getSegmentNearPoint(_dc);
                    if (m_active_segment != -1) {
                        alfiview.setActiveObject(this);
                        alfiview.setCursor(determineResizeCursor(_dc));

                            // add extra start or end point if needed
                        if (m_active_segment == 0) {
                            insertPoint(0, new Point(getStartPoint()));
                            m_active_segment++;
                        }
                        if (m_active_segment == (getNumPoints() - 2)) {
                            addPoint(new Point(getEndPoint()));
                        }

                        return true;
                    }
                }
            }
            else if ((_modifiers & InputEvent.BUTTON2_MASK) != 0) {
                     // diagnostics only
                // reverseDataFlowDirection(alfiview);
            }
            else if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
                alfiview.doWidgetPopupMenu(this, _modifiers, _dc, _vc);
                return true;
            }
        }
        return false;
    }

        /** End a connection segment reposition. */
    public boolean doMouseUp(int _modifiers,
                             Point _dc, Point _vc, JGoView _view) {
        if (_view instanceof ALFIView) {
            ALFIView alfiview = (ALFIView) _view;

                // See notes on mb_mouse_drag_is_armed.
            boolean b_mouse_drag_has_occured = mb_mouse_drag_has_occured;
            mb_mouse_drag_is_armed = false;    // reset before return
            mb_mouse_drag_has_occured = false; // reset before return

            if (m_active_segment != -1) {
                alfiview.clearActiveObject();

                    // this should not be needed.  ConnectionWidgets
                    // are set to not selectable in their constructor!
                alfiview.getSelection().clearSelection();

                alfiview.setCursor(Cursor.getDefaultCursor());
                m_active_segment = -1;

                if (b_mouse_drag_has_occured) {
                    Integer[][] path_definition_and_hint =
                       ConnectionProxy.pathPointsToPathDefinition(copyPoints());
                    Integer[] path_definition = path_definition_and_hint[0];
                    getAssociatedProxy().setPathDefinition(path_definition);
                    int dog_leg_hint= path_definition_and_hint[1][0].intValue();
                    getAssociatedProxy().setDogLegHint(dog_leg_hint, true);

                    fireConnectionRedrawnEvent(
                                             new ConnectionRedrawnEvent(this));

                    return true;
                }
                else { return doMouseClick(_modifiers, _dc, _vc, _view); }
            }
        }
        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseMove(int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
            // See notes on mb_mouse_drag_is_armed.
        if (mb_mouse_drag_is_armed) {
            mb_mouse_drag_has_occured = true;
            mb_mouse_drag_is_armed = false;    // reset
        }

        Point grid_point = GridTool.getNearestGridPoint(_dc);
        if (m_active_segment != -1) {
            Point start_pt = new Point(getPoint(m_active_segment));
            Point end_pt = new Point(getPoint(m_active_segment + 1));

            if (segmentIsHorizontal(start_pt, end_pt)) {
                start_pt.y = grid_point.y;
                end_pt.y = grid_point.y;
            }
            else {
                start_pt.x = grid_point.x;
                end_pt.x = grid_point.x;
            }
            setPoint(m_active_segment, start_pt);
            setPoint(m_active_segment + 1, end_pt);
            return true;
        }
        else { return false; }
    }

        /** LMB places a junction and starts a connection at _dc.
          * MMB places a bundler if the data type is DATA_TYPE_ALFI_BUNDLE.
          */
    public boolean doMouseClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        assert(_view instanceof ALFIView);
        final ALFIView alfiview = (ALFIView) _view;

        if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
            EditorFrame editor = alfiview.getOwnerFrame();
            JunctionProxy junction = editor.addJunction(_dc);
            if (junction != null) {
                JunctionWidget[] jws =
                                  editor.findJunctions(junction.getLocation());
                final ConnectionBuildingWidget cbw =
                            new ConnectionBuildingWidget(jws[0], alfiview);
                alfiview.setActiveObject(cbw);
                alfiview.addObjectAtHead(cbw);


                    // wait a moment before changing cursor
                java.util.Timer timer = new java.util.Timer();
                timer.schedule(
                    new TimerTask() {
                        public void run() {
                            if (alfiview.getActiveObject() == cbw) {
                                alfiview.setCursor(new Cursor(
                                                     Cursor.CROSSHAIR_CURSOR));
                            }
                        }
                    }, 250);

                return true;
            }
        }
        else if ((_modifiers & InputEvent.BUTTON2_MASK) != 0) {
            ALFINode node = m_editor.getNodeInEdit();
            if (m_associated_proxy.determineUpstreamDataType(node) ==
                                      GenericPortProxy.DATA_TYPE_ALFI_BUNDLE) {
                m_editor.addBundler(_dc);
            }
            return true;
        }

        return false;
    }

        /** Diagnostics only. */
    public boolean doMouseDblClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        System.err.println("(" + this + ").doMouseDblClick(...):");
        System.err.println(toString(true));

        return true;
    }

    public boolean doUncapturedMouseMove(int _flags, Point _dc, Point _vc,
                                                                JGoView _view) {
        if (_view instanceof ALFIView) {
            m_editor.setStatusText(toString());
        }
        return true;    // consume event
    }

    //**************************************************************************
    //**** JGo Patches *********************************************************

        /** Patch for annoying JGo "feature" of fuzzy connection mouseovers. */
    public int getSegmentNearPoint(Point pnt) {
            // make sure that the point's in the bounding box
            // unlike Rectangle.contains(), we handle zero width or height
            // as being OK to be in, plus we include the line width
        Rectangle rect = computeBoundingRect();
        int line_width = ((getPen() != null) ? getPen().getWidth() : 1);
        if (pnt.x < rect.x - line_width ||
                pnt.x > rect.x + rect.width + line_width ||
                pnt.y < rect.y - line_width||
                pnt.y > rect.y + rect.height + line_width) {
            return -1;
        }

            // if just a point, then no hit.
        if (getNumPoints() <= 1) { return -1; }

        //line_width += LINE_FUZZ;    // get rid of this.
        line_width -= 1;

            // for all the segments
        for (int i = 0; i < getNumPoints() - 1; i++) {
            Point start = getPoint(i);
            Point end = getPoint(i + 1);

                // if we're in the x-range, check to see if we're
                // near the y-coordinate
            int xrange_high = Math.max(start.x, end.x) + line_width;
            int xrange_low = Math.min(start.x, end.x) - line_width;
            if ((xrange_low <= pnt.x) && (pnt.x <= xrange_high)) {

                    // and check the y-range
                int yrange_high = Math.max(start.y, end.y) + line_width;
                int yrange_low = Math.min(start.y, end.y) - line_width;
                if ((yrange_low <= pnt.y) && (pnt.y <= yrange_high)) {

                        // see if we should compute the X or Y coordinate
                    if (xrange_high - xrange_low > yrange_high - yrange_low) {
                        if (Math.abs(start.x - end.x) > line_width) {
                            double slope = (double)(end.y - start.y) /
                                                  (double)(end.x - start.x);
                            int guess_y = (int) (slope * (pnt.x - start.x) +
                                                                      start.y);

                            if ((guess_y - line_width <= pnt.y) &&
                                             (pnt.y <= guess_y + line_width)) {
                                return i;
                            }
                        }
                        else { return i; }
                    }
                    else {
                        if (Math.abs(start.y - end.y) > line_width) {
                            double slope = (double)(end.x - start.x) /
                                                    (double)(end.y - start.y);
                            int guess_x = (int) (slope * (pnt.y - start.y) +
                                                                      start.x);

                            if ((guess_x - line_width <= pnt.x) &&
                                             (pnt.x <= guess_x + line_width)) {
                                return i;
                            }
                        }
                        else { return i; }
                    }
                }
            }
        }
        return -1;
    }

        /**
         * Finds the closest point to the passed point along the length of the
         * connection.
         */
    public Point getClosestPointOnConnection(Point _point) {
        Vector path_point_vector = copyPoints();
        Point[] points = new Point[path_point_vector.size()];
        path_point_vector.toArray(points);

        int closest_segment = -1;
        double minimum_distance = Double.MAX_VALUE;
        for (int i = 1; i < points.length; i++) {
            Line2D.Double segment = new Line2D.Double(points[i - 1], points[i]);
            double distance = segment.ptSegDist(_point);
            if (distance < minimum_distance) {
                minimum_distance = distance;
                closest_segment = i - 1;
            }
        }

        Point p_0 = points[closest_segment];
        Point p_1 = points[closest_segment + 1];
        if (segmentIsHorizontal(p_0, p_1)) {
            if (_point.x < Math.min(p_0.x, p_1.x)) {
                return new Point(Math.min(p_0.x, p_1.x), p_0.y);
            }
            else if (_point.x > Math.max(p_0.x, p_1.x)) {
                return new Point(Math.max(p_0.x, p_1.x), p_0.y);
            }
            else { return new Point(_point.x, p_0.y); }
        }
        else {
            if (_point.y < Math.min(p_0.y, p_1.y)) {
                return new Point(p_0.x, Math.min(p_0.y, p_1.y));
            }
            else if (_point.y > Math.max(p_0.y, p_1.y)) {
                return new Point(p_0.x, Math.max(p_0.y, p_1.y));
            }
            else { return new Point(p_0.x, _point.y); }
        }
    }

        /** Overridden to kill off some very annoying behavior. */
    public void showSelectionHandles(JGoSelection selection) { }

        /** Overrides the directional arrow drawing routine.  All the passed
          * parameters are ignored except the graphics object.  We implement the
          * directionals very differently than JGo.
          */
    protected void drawArrowhead(Graphics2D _graphics, boolean _atend,
                                                  int[] _headx, int[] _heady) {
//        _graphics.setStroke(new BasicStroke(3.0f));
          super.drawArrowhead(_graphics, _atend, _headx, _heady);
/*
        _graphics.setStroke(new BasicStroke(3.0f));
        if (_graphics.getPaint() instanceof Color) {
            Color link_color = (Color) _graphics.getPaint();
            Color fill_color = (link_color.equals(Color.yellow)) ?
                                                 Color.lightGray : Color.white;
            Polygon[] stickers = createDirectionalStickers();
            for (int i = 0; i < stickers.length; i++) {
                _graphics.setPaint(link_color);
                _graphics.draw(stickers[i]);
                _graphics.setPaint(fill_color);
                _graphics.fill(stickers[i]);
            }
        }
*/
    }

        /**
         * Data flow direction can reverse on connections between two junctions
         * when the user adds and deletes other connections in a network of
         * junctions.  See notes in updateTerminalWidgets() on why it is
         * necessary to use the ALFIView to implement this...
         */
    public void reverseDataFlowDirection(ALFIView _view) {
        _view.reverseConnectionDirection(this);
    }

        /** This method determines the source and sink widgets using only
          * information from the associated ConnectionProxy, and checking said
          * information against all JGoPorts in the document to determine the
          * matching widgets.  The to and from ports are updated if not current.
          *
          * Unfortunately, it seems that this method FAILS if both the to and
          * from ports need to be changed (as in a reverse of data flow
          * direction.  For this reason, reverseDataFlowDirection() will work
          * through ALFIView to just replace the widget completely.
          */
    private void updateTerminalWidgets() {
        ConnectionProxy connection = getAssociatedProxy();
        GenericPortProxy source = connection.getSource();
        MemberNodeProxy source_member = connection.getSourceMember();
        GenericPortProxy sink = connection.getSink();
        MemberNodeProxy sink_member = connection.getSinkMember();

        if (! fromWidgetIsCurrent()) {
            GenericPortWidget source_widget =
                    m_editor.determineGenericPortWidget(source, source_member);
            if (source_widget != null) {
                setFromPort(source_widget);
            }
            else { Alfi.warn("Source port not found."); }
        }

        if (! toWidgetIsCurrent()) {
            GenericPortWidget sink_widget =
                        m_editor.determineGenericPortWidget(sink, sink_member);
            if (sink_widget != null) {
                setToPort(sink_widget);
            }
            else { Alfi.warn("Sink port not found."); }
        }
    }

        /** Checks if the currently connected widget agrees with bookkeeping. */
    private boolean fromWidgetIsCurrent() {
        ConnectionProxy connection = getAssociatedProxy();
        GenericPortProxy source = connection.getSource();
        MemberNodeProxy source_member = connection.getSourceMember();

        GenericPortWidget source_widget = (GenericPortWidget) getFromPort();

        return genericPortWidgetIsConsistentWithTheseProxys(source_widget,
                                                        source, source_member);
    }

        /** Checks if the currently connected widget agrees with bookkeeping. */
    private boolean toWidgetIsCurrent() {
        ConnectionProxy connection = getAssociatedProxy();
        GenericPortProxy sink = connection.getSink();
        MemberNodeProxy sink_member = connection.getSourceMember();

        GenericPortWidget sink_widget = (GenericPortWidget) getToPort();

        return genericPortWidgetIsConsistentWithTheseProxys(sink_widget,
                                                            sink, sink_member);
    }

        /** Are the proxies and the widget consistent? */
    private boolean genericPortWidgetIsConsistentWithTheseProxys(
                        GenericPortWidget _gpw,
                        GenericPortProxy _gpp, MemberNodeProxy _owner_member) {

        if (_gpw.getAssociatedGenericPortProxy() == _gpp) {
            MemberNodeWidget gpw_owner = _gpw.getOwnerMemberWidget();
            if (gpw_owner == null) {
                if (_owner_member == null) {
                    return true;
                }
            }
            else if (gpw_owner.getAssociatedProxy() == _owner_member) {
                return true;
            }
        }

        return false;
    }

    //////////////////////////////////////////////////////////
    //////////  ProxyChangeEventListener methods /////////////

        /** ProxyChangeEventListener method. */
    public void proxyChangePerformed(ProxyChangeEvent e) {
        Object o = e.getTempProxyObjectContainingChanges();
        if (o instanceof ConnectionProxy) {
            ConnectionProxy changes_info = (ConnectionProxy) o;

                // don't redraw until both ports and path are set
            setSuspendUpdates(true);

            updateTerminalWidgets();

                // reset path
            syncPathPoints();

                // reenable and do the update
            setSuspendUpdates(false);
            update(JGoObject.ChangedGeometry, -1,(Object)computeBoundingRect());
            updateDataType();
        }
        else {
            System.err.println("(" + this + ").proxyChangePerformed(" +
                e + "):\n\tWARNING: Invalid temp proxy type passed in event.");
        }
    }

    //////////////////////////////////////////////////////////
    //////////  UpstreamDataChannelChangeEventListener methods

        /** UpstreamDataChannelChangeEventListener method. */
    public void upstreamChangeOccurred(UpstreamDataChannelChangeEvent _e) {
        updateDataType();
    }

    /////////////////////////////////////////////////////////////
    ////////// ConnectionRedrawnEventSource methods /////////////

        /** ConnectionRedrawnEventSource method.  */
    public void addConnectionRedrawnEventListener(
                                    ConnectionRedrawnEventListener _listener) {
        m_connection_redrawn_event_listeners_map.put(_listener, null);
    }

        /** ConnectionRedrawnEventSource method. */
    public void removeConnectionRedrawnEventListener(
                                    ConnectionRedrawnEventListener _listener) {
        m_connection_redrawn_event_listeners_map.remove(_listener);
    }

        /** Used in conjunction with ConnectionRedrawnEventSource. */
    protected void fireConnectionRedrawnEvent(ConnectionRedrawnEvent _event) {
        for (Iterator i =
                m_connection_redrawn_event_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ConnectionRedrawnEventListener) i.next()).
                                                     connectionRedrawn(_event);
        }
    }
}
