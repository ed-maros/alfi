package edu.caltech.ligo.alfi.widgets;

import java.awt.*;
import java.awt.datatransfer.*;
import java.util.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.tools.*;

/**
  * This interface defines constants and methods which must be implemented
  * in all Alfi port widgets.
  */
public abstract class GenericPortWidget extends JGoPort
                            implements AlfiWidgetIF, ProxyChangeEventListener {

    ////////////////////////////////////////////////////////////////////////////
    ///// constants ////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////
    ///// member fields ////////////////////////////////////////////////////////

        /** The associated bookkeeping object. */
    final protected GenericPortProxy m_associated_proxy;

    final protected EditorFrame m_editor;

    ////////////////////////////////////////////////////////////////////////////
    ///// constructors /////////////////////////////////////////////////////////

    protected GenericPortWidget(EditorFrame _editor,
                                GenericPortProxy _associated_proxy) {

        assert((_editor != null) && (_associated_proxy != null));

        m_editor = _editor;
        m_associated_proxy = _associated_proxy;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///// class methods ////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    ///// abstract methods /////////////////////////////////////////////////////

        /** Returns a list of possible data types this port is currently
          * capable of supporting for a new connection to be made in
          * _container_node.  _b_as_source_port is true for a query about this
          * port as a source port, and false if it is regarding this port as a
          * sink port.  For each data type returned, there is also a connection
          * build hint associated with the data type.  The format of the
          * returned array is:
          *
          *   [i][1] = data_type
          *   [i][2] = connection_build_hint
          */
    abstract int[][] getPossibleDataTypesForNewConnectionBuild(
                                                    boolean _b_as_source_port);

    public abstract String generateDescription();

        /** Abstract ProxyChangeEventListener method. */
    public abstract void proxyChangePerformed(ProxyChangeEvent e);

    ////////////////////////////////////////////////////////////////////////////
    ///// implemented methods //////////////////////////////////////////////////

        /** AlfiWidgetIF method. */
    public Object getAssociatedProxyObject() { return m_associated_proxy; }

        /** AlfiWidgetIF method. */
    public EditorFrame getEditor() { return m_editor; }

        /** AlfiWidgetIF covenience method. */
    public boolean isLocal() {
        ALFINode container = m_editor.getNodeInEdit();
        return container.genericPort_containsLocal(m_associated_proxy);
    }

    public GenericPortProxy getAssociatedGenericPortProxy() {
        return m_associated_proxy;
    }

        /** If this object lies on a member node, return the widget.  This
          * implementation merely returns null.  This method should be
          * overridden in subclasses which may be associated with a member node.
          */
    public MemberNodeWidget getOwnerMemberWidget() { return null; }

        /** Covenience method. */
    public MemberNodeProxy getOwnerMemberProxy() {
        MemberNodeWidget mnw = getOwnerMemberWidget();
        return ((mnw != null) ? mnw.getAssociatedProxy() : null);
    }

        /** Covenience method.  Uses similar GenericPortProxy method. */
    public ConnectionWidget[] getConnections() {
        ALFINode container = m_editor.getNodeInEdit();
        GenericPortProxy generic_port = getAssociatedGenericPortProxy();
        MemberNodeProxy owner = getOwnerMemberProxy();
        ConnectionProxy[] connections =
                                 generic_port.getConnections(container, owner);

        ConnectionWidget[] cws = new ConnectionWidget[connections.length];
        for (int i = 0; i < cws.length; i++) {
            cws[i] = m_editor.determineConnectionWidget(connections[i]);
        }
        return cws;
    }

        /** Covenience method.  Uses similar GenericPortProxy method. */
    public ConnectionWidget[] getConnections_local() {
        ConnectionWidget[] cws = getConnections();
        ArrayList cw_list = new ArrayList();
        for (int i = 0; i < cws.length; i++) {
            if (cws[i].isLocal()) { cw_list.add(cws[i]); }
        }
        cws = new ConnectionWidget[cw_list.size()];
        cw_list.toArray(cws);
        return cws;
    }

        /** Covenience method.  Uses similar GenericPortProxy method. */
    public boolean hasConnections() {
        ALFINode container = m_editor.getNodeInEdit();
        MemberNodeProxy owner_member = getOwnerMemberProxy();
        return getAssociatedGenericPortProxy().hasConnections(container,
                                                              owner_member);
    }
}
