package edu.caltech.ligo.alfi.widgets;

import java.util.*;


    /**
     * The interface for objects who wish to register as listeners for
     * ProxyChangeEvents.
     */
public interface ConnectionRedrawnEventListener extends EventListener {
    public void connectionRedrawn(ConnectionRedrawnEvent _event);
}
