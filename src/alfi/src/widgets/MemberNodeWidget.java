package edu.caltech.ligo.alfi.widgets; 
import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import java.util.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.ALFIFile;
import edu.caltech.ligo.alfi.tools.*;


/**
  *
  * A MemberNodeWidget has an icon (or a label if an icon does not exist) and 
  * a variable number of input ports, and a variable number of output ports.
  *   
  * <p>
  * Resizing a node only resizes the icon, not the label or ports.
  * Furthermore, resizing the icon maintains its original aspect ratio.
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class MemberNodeWidget extends NodeWidget implements AlfiWidgetIF,
                ProxyChangeEventListener,
                PortOrderChangeEventListener,
                NodeContentChangeEventListener,
                ContainedElementModEventListener {

    public static final String NODE_INHERITED = new String(
        "Inherited nodes cannot be repositioned.");
    public static final String ILLEGAL_OPERATION = new String(
        "Illegal Operation");

    private final boolean mb_inherited;

    private Point m_mouse_down_point;
    private Dimension m_icon_node_size = null;

    private final EditorFrame m_editor;
    private PortProxy m_new_port_proxy = null;

        /** Create a MemberNodeWidget. */ 
    public MemberNodeWidget(EditorFrame _editor, ALFINode _member_node) {
        this(_editor, _member_node, false);
    }

    public MemberNodeWidget(EditorFrame _editor, ALFINode _member_node, 
                                                boolean _b_raw_external_view) {
        super(_member_node, _b_raw_external_view);

        m_editor = _editor;
        ALFINode container = m_editor.getNodeInEdit();
        MemberNodeProxy member = _member_node.memberNodeProxy_getAssociated();
        mb_inherited = container.memberNodeProxy_containsInherited(member);

        m_mouse_down_point = null;

        ///////////////////////////////////////
        // REMEMBER TO REMOVE LISTENERS TOO!!//
        ///////////////////////////////////////

        if (! _b_raw_external_view) {
            getAssociatedProxy().addProxyChangeListener(this);
        }

            // listen to m_member_node and all it's base nodes for port order
            // changes and for NodeContentsChanges for port adds/deletes
        ALFINode[] base_nodes = m_member_node.baseNodes_getHierarchyPath();
        for (int i = 0; i < base_nodes.length; i++) {
            base_nodes[i].addPortOrderChangeListener(this);
        }

        PortProxy[] ports = m_member_node.ports_get();
        for (int i =0; i < ports.length; i++) {
            ports[i].addProxyChangeListener(this);
        }

        m_member_node.addContainedElementModListener(this);
        m_member_node.addNodeContentChangeListener(this);
    }

    public void cleanUp() {
        if (getAssociatedProxy() != null) {
            getAssociatedProxy().removeProxyChangeListener(this);
        }

        ALFINode[] base_nodes = m_member_node.baseNodes_getHierarchyPath();
        for (int i = 0; i < base_nodes.length; i++) {
            base_nodes[i].removePortOrderChangeListener(this);
        }

        PortProxy[] ports = m_member_node.ports_get();
        for (int i =0; i < ports.length; i++) {
            ports[i].removeProxyChangeListener(this);
        }

        m_member_node.removeContainedElementModListener(this);
        m_member_node.removeNodeContentChangeListener(this);
    }

    public boolean isModifiable() {
        // For now the only criteria of its modifiability
        // is if it's a local node
        return (!mb_inherited);
    }

    public boolean isInherited() { return mb_inherited; }

        /** Called before realizing the object. */
    public void initialize () {
            // The area as a whole is not selectable using a mouse,
            // but the area can be selected by trying to select 
            // any of its children (ports).
        setSelectable(! mb_raw_external_view);
        //setGrabChildSelection(! mb_raw_external_view);
        setPickableBackground(! mb_raw_external_view);
        setDraggable(! mb_raw_external_view);
        setResizable(false);
        //set4ResizeHandles(true);

        createAreaObjectsExceptingPorts();
        createPortWidgets(true);
        layoutContent();

        if (mb_raw_external_view) {
            setLocation(EditorFrame.EXTERNAL_VIEW_WIDGET_OFFSET);
        }
        else { setLocation(getAssociatedProxy().getLocation()); }
    }


    private void createPortWidgets(boolean _create_all_ports) {
        PortProxy[][] port_order = getPortOrder();
        Rectangle[] layout_rects = determineNodeLayoutRects(port_order);
        int[] NSEW_port_offsets =
                    determinePossiblePortOffsets(port_order, layout_rects);
        if (_create_all_ports) {
            createPorts(port_order, layout_rects, NSEW_port_offsets);
        }
        else {
                // move the ports to the proper location       
            movePorts(port_order, layout_rects, NSEW_port_offsets);

                // flagged to add a new port?
            if (m_new_port_proxy != null) {
                addPort(m_new_port_proxy, port_order, NSEW_port_offsets);
                m_new_port_proxy = null;
            }
        }
    }


    private void createPorts(PortProxy[][] _port_order,
                    Rectangle[] _layout_rects, int[] _NSEW_port_offsets) {

            // determine the size of the rectangle 
        m_rectangle.setBoundingRect(_layout_rects[5]);

            // Create the ports
        for (int edge = 0; edge < _port_order.length; edge++) {
            for (int position = 0; position < _port_order[edge].length;
                                                                  position++) {
                PortProxy port_proxy = _port_order[edge][position];
                if (port_proxy != null) {
                    Point location = determinePortLocationFromQueuePosition(
                         edge, position, _port_order, _NSEW_port_offsets);
                    createPortWidget(port_proxy, location);
                }
            }
        }
    }

        /** Reposition all the port locations when the rectangle is resized. */
    private void movePorts(PortProxy[][] _port_order,
                    Rectangle[] _layout_rects, int[] _NSEW_port_offsets) {

            // Determine the size of the rectangle 
        m_rectangle.setBoundingRect(_layout_rects[5]);

            // Set the location of the ports
        MemberNodeProxy mnp = getAssociatedProxy();
        PortWidget[] port_widgets = getPortWidgets();
        for (int i = 0; i < port_widgets.length; i++) {
            PortProxy port = port_widgets[i].getAssociatedProxy();
            if (m_member_node.port_contains(port)) {
                int edge = PortProxy.EDGE_INVALID;
                int position = PortProxy.EDGE_POSITION_INVALID;;
                OUTER_LOOP:
                  for (edge = 0; edge < _port_order.length; edge++) {
                    for (position = 0; position < _port_order[edge].length;
                                                                  position++) {
                        if (_port_order[edge][position] == port) {
                            break OUTER_LOOP;
                        }
                    }
                }

                Point location = determinePortLocationFromQueuePosition(edge,
                               position, _port_order, _NSEW_port_offsets);
                setPortLocation(port_widgets[i], location);
                port_widgets[i].setPortShape();
            }
            else { deletePort(port); }
        }
    } 

        /** Port position offsets may occur because of either port centering or
          * symmetry operations.  The port order passed into this method always
          * has already determined the correct order and edge for the ports, but
          * centering and symmetry ops can push them to the center or to the
          * other end of a particular edge's port slots, i.e., centering can
          * cause this:
          *
          *        ____________       ____________
          *        VVV           -->      VVV
          *        012                    012
          *
          * (note the port order did not change)
          *
          * and sometimes a symmetry op might cause this:
          *
          *        ____________       ____________
          *        VVV           -->           VVV
          *        012                         012
          *
          * (note the port order did not change, which may seem strange here,
          * but the reverse ordering of the ports needed for a symmetry op has
          * already occurred (when needed) in the determination of _port_order.)
          *
          * Centering ALWAYS overrides any offset due to a symmetry op.
          */
    private int[] determinePossiblePortOffsets(PortProxy[][] _port_order,
                                                   Rectangle[] _layout_rects) {
        boolean b_center_ports = getBaseNode().getCentersPortsFlag();

        MemberNodeProxy mnp = (MemberNodeProxy) getAssociatedProxyObject();
        boolean b_symmetry_op_exists = ((mnp != null) &&
                         (mnp.getSymmetry() != MemberNodeProxy.SYMMETRY_NONE));

        if (b_center_ports || b_symmetry_op_exists) {
            int[] offsets_NSEW = { 0, 0, 0, 0 };
                // edge = 0 -> 3 (in order: N, S, E, W)
            for (int edge = 0; edge < offsets_NSEW.length; edge++) {
                int port_edge_length = (edge < 2) ?
                        _layout_rects[edge].width : _layout_rects[edge].height;
                int port_slots_available =
                               port_edge_length / PortWidget.PORT_SLOT_SPACING;
                int port_slots_needed = _port_order[edge].length;
                if (b_center_ports) {
                    int empty_starting_slots = 0;
                    for (int edge_pos = 0; edge_pos < _port_order[edge].length;
                                                                  edge_pos++) {
                        if (_port_order[edge][edge_pos] == null) {
                            port_slots_needed--;
                            empty_starting_slots++;
                        }
                        else { break; }
                    }
                    int centering_slot_shift =
                                (port_slots_available - port_slots_needed) / 2;
                    centering_slot_shift -= empty_starting_slots;
                    offsets_NSEW[edge] =
                           centering_slot_shift * PortWidget.PORT_SLOT_SPACING;
                }
                else {
                    int symmetry_op = mnp.getSymmetry();
                    int empty_trailing_slots =
                                      port_slots_available - port_slots_needed;
                    boolean b_slide_ports_to_end = false;
                    switch(edge) {
                      case PortProxy.EDGE_NORTH:
                                            // originally on north edge
                        if ((symmetry_op == MemberNodeProxy.SYMMETRY_Y) ||
                                           // originally on east edge
                                      (symmetry_op ==
                                           MemberNodeProxy.SYMMETRY_PLUS_XY)) {
                            b_slide_ports_to_end = true;
                        }
                        break;
                      case PortProxy.EDGE_SOUTH:
                                            // originally on south edge
                        if ((symmetry_op == MemberNodeProxy.SYMMETRY_Y) ||
                                           // originally on west edge
                                      (symmetry_op ==
                                           MemberNodeProxy.SYMMETRY_PLUS_XY)) {
                            b_slide_ports_to_end = true;
                        }
                        break;
                      case PortProxy.EDGE_EAST:
                                            // originally on east edge
                        if ((symmetry_op == MemberNodeProxy.SYMMETRY_X) ||
                                      (symmetry_op ==
                                           // originally on north edge
                                           MemberNodeProxy.SYMMETRY_PLUS_XY)) {
                            b_slide_ports_to_end = true;
                        }
                        break;
                      case PortProxy.EDGE_WEST:
                                            // originally on west edge
                        if ((symmetry_op == MemberNodeProxy.SYMMETRY_X) ||
                                      (symmetry_op ==
                                           // originally on south edge
                                           MemberNodeProxy.SYMMETRY_PLUS_XY)) {
                            b_slide_ports_to_end = true;
                        }
                        break;
                    }
                    if (b_slide_ports_to_end) {
                        offsets_NSEW[edge] =
                            empty_trailing_slots * PortWidget.PORT_SLOT_SPACING;
                    }
                }
            }

            return offsets_NSEW;
        }

        return null;
    }

    public void refresh() {
        Point starting_location = getLocation();
        clearObjectsExceptingPorts();
        createAreaObjectsExceptingPorts();
        createPortWidgets(false);
        layoutContent();

        if (mb_raw_external_view) {
            setLocation(starting_location);
        }
        else { setLocation(getAssociatedProxy().getLocation()); }
    }

        /** returns the upper left coordinate based on the center location. */
    public static Point determineULLocationFromCenterLocation(
                        MemberNodeProxy _proxy, int _center_x, int _center_y) {
        ALFINode node = _proxy.getDirectlyAssociatedMemberNode();
        
        NodeWidget node_widget = new NodeWidget(node, false);
        node_widget.initialize();

        int width = node_widget.getWidth();
        int height = node_widget.getHeight();
        Point center = new Point(_center_x, _center_y);
        int new_x = center.x - width/2;
        int new_y = center.y - height/2;

        node_widget.clearObjectsExceptingPorts();

        return new Point(new_x, new_y);
    }

        /** Returns the dimension of the widget before it has been created. */
    public static Dimension determineWidgetSize(ALFINode _member_node,
                                                  boolean _raw_external_view) {
        NodeWidget node_widget =
                              new NodeWidget(_member_node, _raw_external_view);
        node_widget.initialize();
        node_widget.setVisible(false);

        Dimension dimension = new Dimension(node_widget.getWidth(), 
                                            node_widget.getHeight());

        return dimension;
    } 

        /** Get and set some user-defined data associated with this node. */
    public ALFINode getObject() { return m_member_node; }

    private void deletePort (PortProxy _port_proxy) {
        PortWidget port_widget = findPortWidget(_port_proxy);
        if (port_widget != null) {
            removeObject(port_widget);
        }
    }

    private void createPortWidget(PortProxy _new_port, Point _location) {
        PortWidget port_widget = new PortWidget(m_editor, _location,
                                                              this, _new_port);
        setPortLocation(port_widget, _location);
        addObjectAtTail(port_widget);

            // this doesn't seem to be needed
        if (isRawExternalView()) {
            port_widget.setSelectable(true);
        }
    }

        /** Get the port location based on the edge and the position.  This
          * method uses the _NSEW_port_offsets to adjust the port locations
          * in cases where port centering is on or where a symmetry op may have
          * caused such an offset.
          */
    private Point determinePortLocationFromQueuePosition(int _edge,
                                      int _position, PortProxy[][] _port_order,
                                      int[] _NSEW_port_offsets) {
        int port_slot_width = PortWidget.PORT_WIDTH + SPOT_OFFSET;
        Rectangle rect = new Rectangle(m_rectangle.getBoundingRect());

        int[] port_offsets = { 0, 0, 0, 0 };
        if (_NSEW_port_offsets != null) {
            port_offsets =  _NSEW_port_offsets;
        }

        boolean b_has_north_ports=(_port_order[PortProxy.EDGE_NORTH].length> 0);
        boolean b_has_east_ports= (_port_order[PortProxy.EDGE_EAST].length > 0);
        boolean b_has_west_ports= (_port_order[PortProxy.EDGE_WEST].length > 0);
    
            // Initialize the point to be the top-left corner.
        Point point = new Point(rect.x, rect.y);
        
        int start_pos = _position;
        switch (_edge) {
          case PortProxy.EDGE_NORTH: 
            if (b_has_west_ports) { start_pos++; }

            point.x += start_pos * port_slot_width;
            point.y += 1;

            if ((! b_has_east_ports) && (! b_has_west_ports)) {
                point.x += port_slot_width / 2;
            }
            point.x += port_offsets[0];
            break;
          case PortProxy.EDGE_SOUTH: 
            if (b_has_west_ports) { start_pos++; }

            point.x += start_pos * port_slot_width;
            point.y += rect.height - PortWidget.PORT_WIDTH;

            if ((! b_has_east_ports) && (! b_has_west_ports)) {
                point.x += port_slot_width / 2;
            }
            point.x += port_offsets[1];
            break;
          case PortProxy.EDGE_EAST: 
            if (b_has_north_ports) { start_pos++; }

            point.x += rect.width - PortWidget.PORT_WIDTH;
            point.y += start_pos * port_slot_width;

            if (! b_has_north_ports) { point.y += port_slot_width / 2; }
            point.y += port_offsets[2];
            break;
          case PortProxy.EDGE_WEST: 
            if (b_has_north_ports) { start_pos++; }

            point.x += 1;
            point.y += start_pos * port_slot_width;

            if (! b_has_north_ports) { point.y += port_slot_width / 2; }
            point.y += port_offsets[3];
            break;
        }      

        return point;
    }

    private PortWidget findPortWidget (PortProxy _port_proxy) {
        PortWidget port_widget = null;
        boolean found = false;

        for (JGoListPosition pos = getFirstObjectPos(); pos != null;
            pos = getNextObjectPos(pos)) {
            JGoObject object = getObjectAtPos(pos);
            if (object instanceof PortWidget) {
                PortWidget test_port_widget = (PortWidget) object;
                PortProxy proxy = test_port_widget.getAssociatedProxy();
                if (proxy == _port_proxy) {
                    port_widget = test_port_widget;
                    return port_widget;
                }
            }
        }
        return port_widget;
    }

    private void addPort(PortProxy _new_port, PortProxy[][] _port_order,
                                               int[] _NSEW_port_offsets) {
            // Figure out where this port is supposed to be added
        int[] queue_position =
                            determinePortQueueLocation(_new_port, _port_order);
        if (queue_position[0] != PortProxy.EDGE_INVALID) {
            int edge = queue_position[0];
            int position = queue_position[1];
            Point location = determinePortLocationFromQueuePosition(edge,
                               position, _port_order, _NSEW_port_offsets);
            createPortWidget(_new_port, location);
        }
    }

        /** Returns the current edge and position of the port. */
    private int[] determinePortQueueLocation(PortProxy _port,
                                                   PortProxy[][] _port_order) {
        int[] queue_location = { PortProxy.EDGE_INVALID,
                                 PortProxy.EDGE_POSITION_INVALID };
        MemberNodeProxy mnp = getAssociatedProxy();
        for (int i = 0; i < _port_order.length; i++) {
            for (int j = 0; j < _port_order[i].length; j++) {
                if ((_port_order[i][j] != null) &&
                                                (_port == _port_order[i][j])) {
                    queue_location[0] = i;
                    queue_location[1] = j;
                    return queue_location;
                }
            }
        }
        return queue_location;
    }

    protected void setPortLocation (PortWidget _port_widget, Point _location) {
        _port_widget.setLocation(_location.x, _location.y);
        _port_widget.setBoundingRect(_location, 
                new Dimension(PortWidget.PORT_WIDTH + SPOT_OFFSET, 
                              PortWidget.PORT_WIDTH + SPOT_OFFSET));
    }

    public PortWidget[] getPortWidgets() {
        ArrayList port_widget_list = new ArrayList();
        for (JGoListPosition pos = getFirstObjectPos(); pos != null;
                                            pos = getNextObjectPos(pos)) {
            JGoObject o = getObjectAtPos(pos);
            if (o instanceof PortWidget) { port_widget_list.add(o); }
        }
        PortWidget[] port_widgets = new PortWidget[port_widget_list.size()];
        port_widget_list.toArray(port_widgets);
        return port_widgets;
    }
    

    /////////////////////////////////////////////////
    // AlfiWidgetIF methods not implemented by JGoObject

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseDown (int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
        m_mouse_down_point = null;
        ALFIView view = (ALFIView)_view;
         
        if ((_modifiers & InputEvent.BUTTON3_MASK) != 0){
            view.doWidgetPopupMenu(this, _modifiers, _dc, _vc);
            m_mouse_down_point = _dc;
            return true;
        }
        else if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
            // The TextComment is not a part of a group
            if (m_member_node.group_get() == null)  {
                view.pickObject(this, true);
                m_mouse_down_point = _dc;
                return false;
            }
            else {
                if ((_modifiers & InputEvent.CTRL_MASK) != 0) {
                    m_mouse_down_point = _dc;
                    return true;
                }
                else {
                    // This MemberNodeWidget is a part of a group.
                    // Send out an event that all elements need to be selected
                    GroupElementModEvent event = 
                        new GroupElementModEvent(this, 
                            GroupElementModEvent.GROUP_SELECT,  
                            m_member_node.group_get()); 
                    m_member_node.fireGroupElementModEvent(event);
                }
            }
        }

        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseUp (int _modifiers,
                             Point _dc, Point _vc, JGoView _view) {

        ALFIView view = (ALFIView)_view;
        view.pickObject(this, true);
        if ((_modifiers & InputEvent.BUTTON1_MASK) != 0){
            // check if the node has been moved
            if ((m_mouse_down_point != null) &&
                            (! _dc.equals(m_mouse_down_point))) {
                //
                // Move the widget with (0, 0) offset.  Basically,
                // just save the proxy's location to it's current
                // location.  We're calling the view's performMove
                // method because it goes through the JGoSelection
                // and saves the location information.
                //
                view.performInternalComponentsMove(KeyEvent.VK_UNDEFINED);

                if ((_modifiers & InputEvent.CTRL_MASK) != 0) {
                    int x_offset = _dc.x - m_mouse_down_point.x;
                    int y_offset = _dc.y - m_mouse_down_point.y;
                    view.moveSelection(view.getSelection(), 0, 
                                                    x_offset, y_offset, 0);
                    view.setState(JGoView.MouseStateSelection);
                }
            }
        }

        m_mouse_down_point = null;

        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseMove (int _modifiers,
                                Point _dc, Point _vc, JGoView _view) {
        return false;
    }

    public boolean doMouseClick (int _modifiers, Point _dc, Point _vc, 
                                JGoView _view)
    {
        ALFIView view = (ALFIView)_view;
        view.pickObject(this, true);

        if ((_modifiers & InputEvent.BUTTON3_MASK) != 0){
            view.doWidgetPopupMenu(this, _modifiers, _dc, _vc);
            return true;
        }
        else if ((_modifiers & InputEvent.BUTTON1_MASK) != 0){
            if (mb_inherited) {  
                return true;
            }
        }
        return false;
    }

    public boolean doMouseDblClick (int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
            EditorFrame editor = ((ALFIView) _view).getOwnerFrame();
            ALFINode member_node = getNode();

            if ( member_node.isRootNode() &&
                 member_node instanceof UserDefinedPrimitive) {
                return false;
            }

            if (member_node.isPrimitive() || member_node.isOpaque()) {
                editor.editSettings();
            }
            else { editor.viewInternal(); }

            return true;
        }
        return false;
    }


    public boolean doUncapturedMouseMove (int _flags, Point _dc, Point _vc,
                                          JGoView _view) {
        ((ALFIView) _view).getOwnerFrame().setStatusText(toString());
        return true;
    }

        /** Provides a tooltip for mouseovers. */
    public String getToolTipText() {
        String tip = m_member_node.comment_getLocal();
        if (tip != null) { tip = "<PRE>" + tip + "</PRE>"; }

        if (Alfi.showInheritedTips()) {
            ALFINode base_node = m_member_node.baseNode_getDirect();
            while (base_node != null) {
                String comment = base_node.comment_getLocal();
                if (comment != null) {
                    StringTokenizer st = new StringTokenizer(comment, "\n");
                    while (st.hasMoreTokens()) {
                        if (st.nextToken().length() > 60) {
                            comment = TextFormatter.formatMessage(comment, 60);
                            break;
                        }
                    }
                    if (comment.length() > 0) {
                        if (tip == null) { tip = "No local comment"; }
                        tip += "<BR>-------------------<BR><PRE><B>" +
                            base_node.generateFullNodePathName() + " :</B> " +
                            comment + "</PRE>";
                    }
                }
                base_node = base_node.baseNode_getDirect();
            }
        }

        if (tip != null) {
            tip = "<HTML>" + tip.trim() + "</HTML>";
            if (tip.length() == 0) { tip = null; }
        }
        return tip;
    }

    //////////////////////////////////////////////////////////
    //////////  ProxyChangeEventListener methods /////////////

        /** ProxyChangeEventListener method. */
    public void proxyChangePerformed(ProxyChangeEvent _event) {
        Object o = _event.getTempProxyObjectContainingChanges();
        if (o instanceof MemberNodeProxy) {
            refresh();
        } else if (o instanceof PortProxy) {
            refresh();

        } else {
            System.err.println("(" + this + ").proxyChangePerformed(" + _event +                "):\n\tWARNING: Invalid temp proxy type passed in event.");
        }
    }

    //////////////////////////////////////////////////////////
    ////////// AlfiWidgetIF methods //////////////////////////

        /** AlfiWidgetIF method. */
    public EditorFrame getEditor() { return m_editor; }

        /** AlfiWidgetIF method. */
    public Object getAssociatedProxyObject() { return getAssociatedProxy(); }

        /** AlfiWidgetIF covenience method. */
    public boolean isLocal() {
        ALFINode container = m_editor.getNodeInEdit();
        return container.memberNodeProxy_containsLocal(getAssociatedProxy());
    }

    //////////////////////////////////////////////////////////
    ////////// PortOrderChangeEventListener methods //////////

        /** PortOrderChangeEventListener method. */
    public void portOrderChange(PortOrderChangeEvent _event) {
        refresh();
    }

    ///////////////////////////////////////////////////////////////////
    ////////// NodeContentChangeEventListener methods /////////////////

        /** NodeContentChangeEventListener method. */
    public void nodeContentChanged(NodeContentChangeEvent _event) {
        ALFINode source_node = (ALFINode) _event.getSource();
        if (_event.getAction() == NodeContentChangeEvent.PORT_ADD) {
            m_new_port_proxy = (PortProxy) _event.getAssociatedElement();
            m_new_port_proxy.addProxyChangeListener(this);
        }
        else if (_event.getAction() == NodeContentChangeEvent.PORT_DELETE) {
            PortProxy port_proxy = (PortProxy) _event.getAssociatedElement();
            port_proxy.removeProxyChangeListener(this);
            deletePort(port_proxy);
        }
        refresh();
    }

    ///////////////////////////////////////////////////////////////////
    ////////// ContainedElementModEventListener methods ///////////////

        /** ContainedElementModEventListener method. */
    public void containedElementModified(ContainedElementModEvent e) {
        refresh();
    }
}


/**
  *
  * A NodeWidget is the super class of MemberNodeWidget.  It contains
  * the JGoObjects necessary for displaying a node.  This class was
  * created in the initial conversion from the center location to
  * the top left position.  In order to find out the dimensions 
  * of the widget, an instance of it was needed without any of the 
  * event listeners.  
  * The method
  * <P>
  *  public static Point 
  *      determineULLocationFromCenterLocation (MemberNodeProxy _proxy, 
  *          int _center_x, int _center_y);
  * <P>
  * should be the ONLY place that calls the NodeWidget constructor.
  * This is called from the parser the first time the coordinates are 
  * converted and never called again anywhere else but by the MemberNodeWidget.
  *
  * @author Melody Araya
  * @version %I%, %G%
  */

class NodeWidget extends JGoArea {

    public static final int SPOT_OFFSET = 1;
    public static final int OFFSET = PortWidget.PORT_WIDTH + SPOT_OFFSET;
    public static final int BORDER = 3;
    public static final int GRID_SIZE = OFFSET/2;

    private static final Dimension RECT_MIN_SIZE = new Dimension(19, 19); 
    private static final Dimension IMAGE_STD_SIZE = new Dimension(15, 15);

    protected JGoText m_label;
    protected JGoRectangle m_rectangle;
    protected Image m_base_image;
    protected JGoImage m_image;
    protected final boolean mb_raw_external_view;
    
    protected boolean m_icon_displayed;

    protected final ALFINode m_member_node;

    NodeWidget(ALFINode _member_node, boolean _raw_external_view) {
        super();        
        m_member_node = _member_node;
        mb_raw_external_view = _raw_external_view;

        m_label = null;
        m_rectangle = null;
        m_base_image = null;
        m_image = null;

        m_icon_displayed = false;
    }

    public MemberNodeProxy getAssociatedProxy () {
        return m_member_node.memberNodeProxy_getAssociated();
    }

    public ALFINode getNode () { return m_member_node; }

    private boolean isInherited() {
        ALFINode container_node = m_member_node.containerNode_get();
        if (container_node != null) {
            return container_node.memberNodeProxy_containsInherited(
                                                         getAssociatedProxy());
        }
        else { return false; }
    }

        /**
         * This is true if the member node widget is supposed to be shown
         * without any rotation, swap, or symmetry effects, i.e., the "external"
         * view in the popup menu.  This is poorly named, though, since all
         * member node widgets are always representing an external view of a
         * node, but usually inside another containing node's internal view.
         */
    public boolean isRawExternalView() { return mb_raw_external_view; }

    protected ALFINode getBaseNode() {
        ALFINode base_node = m_member_node;

        if (!m_member_node.isRootNode()){
            base_node = m_member_node.baseNode_getRoot();
        }

        return base_node;
    }

    
    /**
      * Returns the center location based on the given top-left coordinate
      * 
      * @param _top_left_loc top left location
      * 
      */
    protected Point getCenterLocation (Point _top_left_loc) {
        int width = getWidth();
        int height = getHeight();  

        Point new_location = getTopLeft();

        if (_top_left_loc != null){
            new_location.x = _top_left_loc.x;
            new_location.y = _top_left_loc.y;
        }
        
        new_location.x += width/2;
        new_location.y += height/2;

        return new Point(new_location);
    }

    /**
      * Returns the center location based on widget's top-left coordinate
      * 
      * @param _top_left_loc top left location
      * 
      */
    protected Point getCenterLocation () {
        return getCenterLocation(null);
    }

    protected void initialize () {
        createAreaObjectsExceptingPorts(false);

            // determine the size of the rectangle 
        PortProxy[][] port_order = getPortOrder();
        m_rectangle.setBoundingRect(determineNodeRectangle(port_order));

        layoutContent();
    }

    protected void createAreaObjectsExceptingPorts () {
        createAreaObjectsExceptingPorts(true);
    }
    
    protected void createAreaObjectsExceptingPorts (boolean _create_image) {

        m_rectangle = createRectangle();
        addObjectAtHead(m_rectangle);

        if (m_member_node.getLabelType() == ALFINode.LABEL_TYPE_ICON) {
            m_image = createImageIcon(getBaseNode());
            
            addObjectAtTail(m_image);
            m_icon_displayed = true;
            
        } else  {
            
            m_label = createLabel();
            addObjectAtTail(m_label);
            Dimension label_size = m_label.getSize();
            setSize(label_size);
            m_label.setSize(label_size);
            m_icon_displayed = false;
        }
    }

    /**
      * Position all the parts of the area relative to the text label.
      */
    protected void layoutContent() {
        PortProxy[][] port_order = getPortOrder();
        Rectangle content_rect = determineNodeLayoutRects(port_order)[4];

        JGoObject content = getContent();
        content.setLocation(content_rect.x, content_rect.y);
    }

        /**
         * Clears all child objects except the ports which we don't want losing
         * their connections.
         */
    protected void clearObjectsExceptingPorts () {
        if (m_rectangle != null) { 
            removeObject(m_rectangle); 
            m_rectangle = null;
        }
    
        if (m_icon_displayed) { 
            removeObject(m_image); 
            m_image = null;
        }
        else { 
            removeObject(m_label); 
            m_label = null;
        }
    }

    private JGoObject getContent () {
        JGoText label = getLabel();
        if (label != null) {
            return m_label;
        } else {
            return m_image;
        }
    }

    private Dimension getContentSize () {
        JGoText label = getLabel();
        if (label != null) {
            return label.getSize();
        } else {
            return getImageIcon().getSize();
        }
    }

    private Dimension getMinimumValidContentSize () {
        JGoText label = getLabel();

        if (label != null) {
            return  new Dimension(RECT_MIN_SIZE);
        } else {
            return  new Dimension(IMAGE_STD_SIZE);
        }
    }


    // get access to the parts of the node
    public JGoText getLabel() { return m_label; }
    public void setLabel (JGoText lab)
    {
        JGoText oldLabel = getLabel();
        if (oldLabel != lab) {
            if (oldLabel != null) {
                removeObject(oldLabel);
            }
            m_label = lab;
            if (lab != null) {
                addObjectAtTail(lab);
            }
        }
    }

    // These are convenience methods for getting the Text property
    public String getText() { return getLabel().getText(); }
    public void setText(String s) { getLabel().setText(s); }

    public JGoImage getImageIcon() { return m_image; }

    public JGoRectangle getRect() { return m_rectangle; }
    public void setRect(JGoRectangle rect)
    {
        JGoRectangle oldRect = getRect();
        if (oldRect != rect) {
            if (oldRect != null) {
                removeObject(oldRect);
            }
            m_rectangle = rect;
            if (rect != null) {
                addObjectAtHead(rect);
            }
        }
    }

    protected PortProxy[][] getPortOrder () {
        PortProxy[][] port_order = null;
        MemberNodeProxy mnp = getAssociatedProxy();
        if (mb_raw_external_view) {
            port_order = getNode().ports_getByPosition(false);
        }
        else {
            port_order = mnp.determinePortPositionsAfterRotateEtcOps(
                                                getNode().containerNode_get());
        }

        return port_order;
    }

    private Image imageRotateSwapSymmetryOps(Image _base_image) {
        BufferedImage buffered_image = new BufferedImage(IMAGE_STD_SIZE.width,
                             IMAGE_STD_SIZE.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = buffered_image.createGraphics();

            // this is a kluge because the color behind the image when we
            // draw it through this means turns black when JGo draws it, so
            // the transparency shows through to black (grrr!!)
        graphics.setPaint(AlfiMainFrame.determineNodeDisplayColor(
                                                           m_member_node));
        graphics.fillRect(0, 0, IMAGE_STD_SIZE.width, IMAGE_STD_SIZE.height);

        graphics.drawImage(_base_image, 0, 0, null);

            // rotation
        MemberNodeProxy proxy = getAssociatedProxy();
        BufferedImageOp op = null;
        Point draw_point = null;
        switch (proxy.getRotation()) {
          case MemberNodeProxy.ROTATION_EAST:
            op = new AffineTransformOp(
                    AffineTransform.getRotateInstance(Math.PI / 2),
                    AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            draw_point = new Point(IMAGE_STD_SIZE.width, 0);
            break;
          case MemberNodeProxy.ROTATION_SOUTH:
            op = new AffineTransformOp(
                    AffineTransform.getRotateInstance(Math.PI),
                    AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            draw_point = new Point(IMAGE_STD_SIZE.width, IMAGE_STD_SIZE.height);
            break;
          case MemberNodeProxy.ROTATION_WEST:
            op = new AffineTransformOp(
                    AffineTransform.getRotateInstance(-1 * Math.PI / 2),
                    AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            draw_point = new Point(0, IMAGE_STD_SIZE.height);
            break;
        }
        if (op != null) {
            BufferedImage temp = new BufferedImage(IMAGE_STD_SIZE.width,
                             IMAGE_STD_SIZE.height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = temp.createGraphics();
            g.drawImage(buffered_image, op, draw_point.x, draw_point.y);
            buffered_image = temp;
        }

            // swap
        op = null;
        draw_point = null;
        switch (proxy.getSwap()) {
          case MemberNodeProxy.SWAP_X:
            op = new AffineTransformOp(
                    AffineTransform.getScaleInstance(1.0, -1.0),
                    AffineTransformOp.TYPE_BILINEAR);
            draw_point = new Point(0, IMAGE_STD_SIZE.height);
            break;
          case MemberNodeProxy.SWAP_Y:
            op = new AffineTransformOp(
                    AffineTransform.getScaleInstance(-1.0, 1.0),
                    AffineTransformOp.TYPE_BILINEAR);
            draw_point = new Point(IMAGE_STD_SIZE.width, 0);
            break;
        }
        if (op != null) {
            BufferedImage temp = new BufferedImage(IMAGE_STD_SIZE.width,
                             IMAGE_STD_SIZE.height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = temp.createGraphics();
            g.drawImage(buffered_image, op, draw_point.x, draw_point.y);
            buffered_image = temp;
        }

            // symmetry
        op = null;
        draw_point = null;
        switch (proxy.getSymmetry()) {
          case MemberNodeProxy.SYMMETRY_X:    // does same as SWAP_X here
            op = new AffineTransformOp(
                    AffineTransform.getScaleInstance(1.0, -1.0),
                    AffineTransformOp.TYPE_BILINEAR);
            draw_point = new Point(0, IMAGE_STD_SIZE.height);
            break;
          case MemberNodeProxy.SYMMETRY_Y:    // does same as SWAP_Y here
            op = new AffineTransformOp(
                    AffineTransform.getScaleInstance(-1.0, 1.0),
                    AffineTransformOp.TYPE_BILINEAR);
            draw_point = new Point(IMAGE_STD_SIZE.width, 0);
            break;
          case MemberNodeProxy.SYMMETRY_PLUS_XY:
          case MemberNodeProxy.SYMMETRY_MINUS_XY:
            String message = "Icon image transform not implemented.";
            String title = "Unimplemented Image Transform";
            JOptionPane.showMessageDialog(null, message, title,
                                    JOptionPane.WARNING_MESSAGE);
            break;
        }
        if (op != null) {
            BufferedImage temp = new BufferedImage(IMAGE_STD_SIZE.width,
                             IMAGE_STD_SIZE.height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = temp.createGraphics();
            g.drawImage(buffered_image, op, draw_point.x, draw_point.y);
            buffered_image = temp;
        }

        return buffered_image;
    }

    private JGoImage createImageIcon (ALFINode base_node) {
        m_base_image =
            Toolkit.getDefaultToolkit().getImage(base_node.getIconFilePath());
        MediaTracker tracker = new MediaTracker(new Canvas());
        tracker.addImage(m_base_image, 0);
        try { tracker.waitForID(0); }
        catch (InterruptedException e) { System.err.println(e); }

        Image image = m_base_image;
        if (getAssociatedProxy() != null) {
            MemberNodeProxy proxy = getAssociatedProxy();

            if (m_member_node.getIconRotates() &&
                  ( (proxy.getRotation() != MemberNodeProxy.ROTATION_NONE) ||
                    (proxy.getSwap() != MemberNodeProxy.SWAP_NONE)  ||
                    (proxy.getSymmetry() != MemberNodeProxy.SYMMETRY_NONE) ) ) {
                image = imageRotateSwapSymmetryOps(m_base_image);
            }
        }

        JGoImage image_icon;
        ImageIcon icon = new ImageIcon(image); 
        image_icon = new JGoImage(new Rectangle(icon.getIconWidth(),
                                                icon.getIconHeight()));
        image_icon.loadImage(image, true);
        image_icon.setTransparentColor(null);

        image_icon.setSelectable(false);
        image_icon.setResizable(false);

        return image_icon;
    }

    private JGoRectangle createRectangle() {
        JGo3DRect rectangle = new JGo3DRect();
        rectangle.setSelectable(false);
        rectangle.setPen(JGoPen.lightGray); 

        Color brush_color =
                  AlfiMainFrame.determineNodeDisplayColor(m_member_node);
        rectangle.setBrush(JGoBrush.makeStockBrush(brush_color));

        return rectangle;
    }


    private JGoText createLabel() {

        String label = m_member_node.getTextLabel();
        JGoText text_label = new JGoText(label);
        text_label.setTransparent(true);

        if (Alfi.getMainWindow().getCheckBoxItemState(
                                               ActionsIF.MACRO_INFO_ON_NODE)) {
            boolean[] macro_info = m_member_node.macro_getContainmentInfo();
            if (macro_info[0] || macro_info[1]) {
                text_label.setBkColor(Color.white);
                text_label.setTransparent(false);
            }
            else if (macro_info[2] || macro_info[3]) {
                text_label.setBkColor(Color.yellow);
                text_label.setTransparent(false);
            }
        }

        text_label.setMultiline(true);
        text_label.setSelectable(false);
        text_label.setResizable(false);
        text_label.setDraggable(false);
        text_label.setEditable(false);
        text_label.setFaceName(GuiConstants.NODE_TEXT_FACE_NAME);
        
        return text_label;
    }

        /**
         * Returns the dimensions needed to contain all the ports on the edges.
         */
    protected Dimension determineMaxPortsSize (PortProxy[][] _port_order) {
        int max_horizontal_count =
                Math.max(_port_order[0].length, _port_order[1].length);
        int max_vertical_count =
                Math.max(_port_order[2].length, _port_order[3].length);

        int slot_width = PortWidget.PORT_WIDTH + 1;
        return new Dimension(max_horizontal_count * slot_width,
                             max_vertical_count * slot_width);
    }

        /**
         * This method returns Rectangles of the right size to contain the ports
         * on each side of the widget, a Rectangle[4] (N, S, E, W sides).
         * Returned rects which have area = 0 have no ports in them.
         * The positions of the rects only take into account the size and
         * position of the other port edge rects, and may still need to be
         * adjusted further in order to fit in a minumum content area.
         */
    private Rectangle[] determinePortAreaRects(PortProxy[][] _port_order) {
        final int N = PortProxy.EDGE_NORTH;  final int S = PortProxy.EDGE_SOUTH;
        final int E = PortProxy.EDGE_EAST;   final int W = PortProxy.EDGE_WEST;

        Rectangle[] port_rects = new Rectangle[_port_order.length];
        for (int edge = 0; edge < _port_order.length; edge++) {
            int width, height;
            switch(edge) {
              case N:
              case S:
                width = OFFSET * _port_order[edge].length;
                height = (width != 0) ? OFFSET : 0;
                break;
              case E:
              case W:
                height = OFFSET * _port_order[edge].length;
                width = (height != 0) ? OFFSET : 0;
                break;
              default:
                return null;    // should never get here
            }
            port_rects[edge] = new Rectangle(0, 0, width, height);
        }

        int v_offset = port_rects[N].isEmpty() ? 0 : OFFSET;
        int h_offset = port_rects[W].isEmpty() ? 0 : OFFSET;

        port_rects[E].translate(0, v_offset);
        port_rects[W].translate(0, v_offset);

        v_offset += Math.max(port_rects[W].height, port_rects[E].height);

        port_rects[N].translate(h_offset, 0);
        port_rects[S].translate(h_offset, v_offset);

        h_offset += Math.max(port_rects[N].width, port_rects[S].width);

            // note vertical translation already done above
        port_rects[E].translate(h_offset, 0);

            // expand widths and heights as needed to match north/south widths
            // and east/west heights
        if (port_rects[N].width > port_rects[S].width) {
            port_rects[S].width = port_rects[N].width;
        }
        else { port_rects[N].width = port_rects[S].width; }

        if (port_rects[E].height > port_rects[W].height) {
            port_rects[W].height = port_rects[E].height;
        }
        else { port_rects[E].height = port_rects[W].height; }

        return port_rects;
    }

         /**
          * This method takes the size of the content object (label or graphic)
          * and adjusts the port edge rectangles if there is not enough room
          * given the initial size and position of the port edge rects.  The
          * return value is a Rectangle[5] with the first 4 corresponding to
          * the N, S, E, & W edges, and the fifth is the rectangle which shows
          * where to place the content object.
          */
    private Rectangle[] adjustEdgePortRectsForContent(Dimension _content_size,
                                             Rectangle[] _raw_edge_port_rects) {
        final int N = PortProxy.EDGE_NORTH;  final int S = PortProxy.EDGE_SOUTH;
        final int E = PortProxy.EDGE_EAST;   final int W = PortProxy.EDGE_WEST;

        Rectangle[] rects = { _raw_edge_port_rects[0], _raw_edge_port_rects[1],
                              _raw_edge_port_rects[2], _raw_edge_port_rects[3],
                              null };

            // this is the full area size just taking into account the ports.
            // very likely this will already make enough space for the content.
        Rectangle full_area = new Rectangle(0, 0,
                          rects[W].width + rects[N].width + rects[E].width,
                          rects[N].height + rects[W].height + rects[S].height);

            // this is the basic content area available to start with
        Rectangle content_area = new Rectangle(rects[W].width, rects[N].height,
                                               rects[N].width, rects[W].height);

            // first just try centering the content object and see if it's ok
        Point ul = new Point((full_area.width - _content_size.width) / 2,
                             (full_area.height - _content_size.height) / 2);
        Rectangle content_object_rect = new Rectangle(ul, _content_size);

            // but may need to adjust port rects to fit the content
        if (! content_area.contains(content_object_rect)) {
            int width_needed = content_object_rect.width - content_area.width;
            if (width_needed > 0) {
                if ((width_needed % OFFSET) != 0) {
                    width_needed += OFFSET - (width_needed % OFFSET);
                }
                rects[N].width += width_needed;
                rects[S].width += width_needed;
                rects[E].x += width_needed;
            }
            int height_needed= content_object_rect.height - content_area.height;
            if (height_needed > 0) {
                if ((height_needed % OFFSET) != 0) {
                    height_needed += OFFSET - (height_needed % OFFSET);
                }
                rects[W].height += height_needed;
                rects[E].height += height_needed;
                rects[S].y += height_needed;
            }

                // redo content_object_rect determination

                // revised content area
            content_area = new Rectangle(rects[W].width, rects[N].height,
                                         rects[N].width, rects[W].height);
            ul = new Point(
                content_area.x+ (content_area.width - _content_size.width)/2,
                content_area.y+ (content_area.height - _content_size.height)/2);
            content_object_rect = new Rectangle(ul, _content_size);
        }
        rects[4] = content_object_rect;

        return rects;
    }

    /**
      * Figure out the dimensions of the rectangle based on
      * the maximum number of ports on the sides
      */
    protected Rectangle determineNodeRectangle (PortProxy[][] _port_order) {
        return determineNodeLayoutRects(_port_order)[5];
    }

        /** This method determines all the associated rectangles needed for the
          * layout of the node widget.  The returned Rectangle[6] contains, in
          * order: { north_edge_port_rect, south_edge_port_rect,
          *          east_edge_port_rect,  west_edge_port_rect,
          *          content_rect,         full_node_rect }.
          */
    Rectangle[] determineNodeLayoutRects(PortProxy[][] _port_order) {
        Dimension content_size = getContentSize();
        Dimension minimum_valid_size = getMinimumValidContentSize();
        content_size.width =
                Math.max(content_size.width, minimum_valid_size.width);
        content_size.height =
                Math.max(content_size.height, minimum_valid_size.height);

        Rectangle[] port_area_rects = determinePortAreaRects(_port_order);

        Rectangle[] port_area_and_content_rects =
                adjustEdgePortRectsForContent(content_size, port_area_rects);

        Rectangle node_rect = new Rectangle(port_area_and_content_rects[0]);
        for (int i = 1; i < port_area_and_content_rects.length; i++) {
            node_rect = node_rect.union(port_area_and_content_rects[i]);
        }
            // this call results in node_rect having a position = (-BORDER,
            // -BORDER), which i believe is as it should be since the borders
            // should not impinge on where the ports get layed out on the grid.
        node_rect.grow(BORDER, BORDER);

        Rectangle[] layout_rects = new Rectangle[6];
        for (int i = 0; i < 5; i++) {
            layout_rects[i] = port_area_and_content_rects[i];
        }
        layout_rects[5] = node_rect;

        return layout_rects;
    }

    public String toString () {
        StringBuffer info = new StringBuffer(m_member_node.
            generateFullNodePathName());
        
        info.append(" [");
        if (m_member_node.isRootNode()) {
            info.append(m_member_node.determineLocalName());
        }
        else {
            info.append(m_member_node.baseNode_getRoot().determineLocalName());
        }
        if (m_member_node.isBox()) {
            info.append(ALFIFile.BOX_NODE_FILE_SUFFIX);
        }
        info.append("]");

        return info.toString();
    }
}

