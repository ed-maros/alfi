package edu.caltech.ligo.alfi.widgets;

/** This interface defines fields and methods used by sources of 
  * ConnectionRedrawnEvent sources.
  */
public interface ConnectionRedrawnEventSource {
    public void addConnectionRedrawnEventListener(
                                     ConnectionRedrawnEventListener _listener);
    public void removeConnectionRedrawnEventListener(
                                     ConnectionRedrawnEventListener _listener);
}

