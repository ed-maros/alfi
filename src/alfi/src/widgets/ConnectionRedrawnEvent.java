package edu.caltech.ligo.alfi.widgets;

import java.util.*;


    /** This simple event is sent from ConnectionWidgets to EditorFrames after
      * the connection has been redrawn.  The editor may want to clean it up.
      */
public class ConnectionRedrawnEvent extends EventObject {

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public ConnectionRedrawnEvent(Object _source) { super(_source); }
}
