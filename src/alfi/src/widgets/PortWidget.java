package edu.caltech.ligo.alfi.widgets;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import com.nwoods.jgo.*;

    /**
     * This class is the widget which represents ports in an Alfi edit window.
     */
public class PortWidget extends GenericPortWidget {

    //**************************************************************************
    //***** constants **********************************************************

    public static final int PORT_POSITION__MOVE_PORT   = 0;
    public static final int PORT_POSITION__SIDE_CHANGE = 1;
    public static final int PORT_POSITION__PUSH_PORTS  = 2;

    public static final int PORT_MOVE_AUX_INFO__INCREMENT    = 0;
    public static final int PORT_MOVE_AUX_INFO__DECREMENT    = 1;
    public static final int PORT_MOVE_AUX_INFO__GO_TO_NORTH  = 2;
    public static final int PORT_MOVE_AUX_INFO__GO_TO_SOUTH  = 3;
    public static final int PORT_MOVE_AUX_INFO__GO_TO_EAST   = 4;
    public static final int PORT_MOVE_AUX_INFO__GO_TO_WEST   = 5;

    static final int NORTH_POINTING   = 0;
    static final int SOUTH_POINTING   = 1;
    static final int EAST_POINTING    = 2;
    static final int WEST_POINTING    = 3;
    static final int INVALID_POINTING = 4;

    static final Color[] COLOR_VS_DATA = new Color[PortProxy.DATA_TYPE_INVALID];
    {
        COLOR_VS_DATA[PortProxy.DATA_TYPE_BOOLEAN] = new Color(176, 224, 230);
        COLOR_VS_DATA[PortProxy.DATA_TYPE_BUNDLE] = Color.lightGray;
        COLOR_VS_DATA[PortProxy.DATA_TYPE_ALFI_BUNDLE] = Color.blue;
        COLOR_VS_DATA[PortProxy.DATA_TYPE_CLAMP] = new Color(255, 215, 0);
        COLOR_VS_DATA[PortProxy.DATA_TYPE_COMPLEX] = Color.yellow;
        COLOR_VS_DATA[PortProxy.DATA_TYPE_FIELD] = new Color(230, 130, 238);
        COLOR_VS_DATA[PortProxy.DATA_TYPE_INTEGER] = new Color(165, 42, 42);
        COLOR_VS_DATA[PortProxy.DATA_TYPE_REAL] = new Color(0, 0, 128);
        COLOR_VS_DATA[PortProxy.DATA_TYPE_SINGLE_MODE_FIELD] =
                                      COLOR_VS_DATA[PortProxy.DATA_TYPE_FIELD];
        COLOR_VS_DATA[PortProxy.DATA_TYPE_STRING] = Color.gray;
        COLOR_VS_DATA[PortProxy.DATA_TYPE_1_UNKNOWN] = Color.red;
        COLOR_VS_DATA[PortProxy.DATA_TYPE_1_UNRESOLVED] = Color.red;
        COLOR_VS_DATA[PortProxy.DATA_TYPE_USER_DEFINED] = Color.pink;
        COLOR_VS_DATA[PortProxy.DATA_TYPE_VECTOR_COMPLEX] =
                                                       new Color(64, 224, 208);
        COLOR_VS_DATA[PortProxy.DATA_TYPE_VECTOR_INTEGER] =
                                                      new Color(210, 180, 140);
        COLOR_VS_DATA[PortProxy.DATA_TYPE_VECTOR_REAL] = new Color(50, 205, 50);
        COLOR_VS_DATA[PortProxy.DATA_TYPE_MATRIX_REAL] =
                                                       new Color(205, 205, 50);
        COLOR_VS_DATA[PortProxy.DATA_TYPE_MATRIX_COMPLEX] =
                                                        new Color(255, 215, 0);
    }

    public static final int PORT_WIDTH = 11;    // needs to be an odd number
    public static final int PORT_SLOT_SPACING = PORT_WIDTH + 1;

    //**************************************************************************
    //***** member fields ******************************************************

        /**
         * The MemberNodeWidget this port is attached to.  Null if the port is
         * actually on the container node itself.
         */
    final protected MemberNodeWidget m_owner_member_widget;

    final protected boolean mb_is_local_port;

        /** This is a kluge because the key modifiers are lost in subsequent
          * related event calls like doMouseMove() etc.
          */
    private int m_mouse_down_modifiers;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor for external ports (ports on the edges of the container
          * node being edited.)  _location_in_container is the location of the
          * port inside the container.
          */
    public PortWidget(EditorFrame _editor, Point _location_in_container,
                                                 PortProxy _associated_proxy) {
        super(_editor, _associated_proxy);

            // init final members
        m_owner_member_widget = null;
        mb_is_local_port =
                 _editor.getNodeInEdit().port_containsLocal(_associated_proxy);

        initCommon(_location_in_container, null);
    }

        /** Constructor for ports on member nodes.  _location_wrt_member is the
          * location with respect to the member node widget it is sitting on.
          */
    public PortWidget(EditorFrame _editor, Point _location_wrt_member,
                      MemberNodeWidget _owner_member_widget,
                      PortProxy _associated_proxy) {

        super(_editor, _associated_proxy);

        assert(_owner_member_widget != null);

            // init final members
        m_owner_member_widget = _owner_member_widget;
        mb_is_local_port = _owner_member_widget.getNode().port_containsLocal(
                                                             _associated_proxy);

        initCommon(_location_wrt_member,
                   _owner_member_widget.getAssociatedProxy());
    }

    private void initCommon(Point _location, MemberNodeProxy _owner) {
            // init others
        setStyle(StyleObject);
        setLocation(_location);
        setSize(PORT_WIDTH, PORT_WIDTH);
        setSelectable(false);
        setResizable(false);
        setDraggable(false);

        setPortShape();

        getAssociatedProxy().addProxyChangeListener(this);

            // must listen to owner member as well for rotations, etc.
        if (_owner != null) {
            _owner.addProxyChangeListener(this);
        }
    }

    //**************************************************************************
    //***** methods ************************************************************

    public static Color getDataTypeColor(int _data_type_id) {
        if ((_data_type_id >= 0) &&
                               (_data_type_id < PortProxy.DATA_TYPE_INVALID)) {
            return COLOR_VS_DATA[_data_type_id];
        }
        else { return COLOR_VS_DATA[PortProxy.DATA_TYPE_1_UNKNOWN]; }
    }

    public void setPortShape() {
        PortProxy port = getAssociatedProxy();

        boolean b_has_local_default_value = false;
        if (m_owner_member_widget != null) {
            ALFINode owner_node = m_owner_member_widget.getNode();
            if (owner_node.isPrimitive()) {
                b_has_local_default_value =
                    (port.getIOType() == PortProxy.IO_TYPE_INPUT) &&
                            owner_node.parameterSetting_containsLocal(
                                               getAssociatedProxy().getName());
            }
        }

        PortTriangle port_shape = new PortTriangle(b_has_local_default_value);
        setPortObject(port_shape);
        setFromSpot(port_shape.getLinkSpot());
        setToSpot(port_shape.getLinkSpot());
    }

        /** Reposition a port via a mouse drag. */
    private boolean reposition(Point _mouse_point) {
        PortProxy port = getAssociatedProxy();
        ALFINode node = m_editor.getNodeInEdit();
        boolean b_is_internal_view = m_editor.isInternalView();
        int[] position = port.getPosition();
        int edge = (b_is_internal_view) ? position[0] : position[2];

            // if external view port centering is on and there is only one port
            // on the edge, ignore this call.  the port shouldn't move, and this
            // method will otherwise perform an endless loop trying to make it.
        if ((! b_is_internal_view) && node.getCentersPortsFlag()) {
            PortProxy[][] port_positions = node.ports_getByPosition(false);
            if (port_positions[edge].length < 2) { return false; }
        }

        boolean b_port_moved = false;

        int move_mode = PORT_POSITION__MOVE_PORT;
        switch (edge) {
          case PortProxy.EDGE_NORTH:
          case PortProxy.EDGE_SOUTH:
            while (_mouse_point.x < getLocation().x) {
                if (! reposition(move_mode, PORT_MOVE_AUX_INFO__DECREMENT)) {
                    break;
                }
                b_port_moved = true;
            }
            while (_mouse_point.x > (getLocation().x + getSize().width)) {
                if (! reposition(move_mode, PORT_MOVE_AUX_INFO__INCREMENT)) {
                    break;
                }
                b_port_moved = true;
            }
            break;

          case PortProxy.EDGE_EAST:
          case PortProxy.EDGE_WEST:
            while (_mouse_point.y < getLocation().y) {
                if (! reposition(move_mode, PORT_MOVE_AUX_INFO__DECREMENT)) {
                    break;
                }
                b_port_moved = true;
            }
            while (_mouse_point.y > (getLocation().y + getSize().height)) {
                if (! reposition(move_mode, PORT_MOVE_AUX_INFO__INCREMENT)) {
                    break;
                }
                b_port_moved = true;
            }
            break;
        }

        return b_port_moved;
    }

        /** See static fields PORT_POSITION__XXX for _move_mode, and fields
          * PORT_MOVE_AUX_INFO__XXX for _mode_aux_info.  This method is called
          * from both mouse drag and key press methods of port movement.  (The
          * key press method is ALFIView.performPortMove().)
          */
    public boolean reposition(int _move_mode, int _mode_aux_info) {
        PortProxy port = getAssociatedProxy();
        ALFINode node = m_editor.getNodeInEdit();
        boolean b_is_internal_view = m_editor.isInternalView();
        int[] position = port.getPosition();
        int edge = (b_is_internal_view) ? position[0] : position[2];
        int operation = -1;
        if (_move_mode == PORT_POSITION__MOVE_PORT) {
            if (_mode_aux_info == PORT_MOVE_AUX_INFO__INCREMENT) {
                operation = PortProxy.MOVE_INCREMENT;
            }
            else { operation = PortProxy.MOVE_DECREMENT; }
        }
        else if (_move_mode == PORT_POSITION__PUSH_PORTS) {
            if (_mode_aux_info == PORT_MOVE_AUX_INFO__INCREMENT) {
                operation = PortProxy.PUSH_INCREMENT;
            }
            else { operation = PortProxy.PUSH_DECREMENT; }
        }
        else if (_move_mode == PORT_POSITION__SIDE_CHANGE) {
            if (_mode_aux_info == PORT_MOVE_AUX_INFO__GO_TO_NORTH) {
                operation = PortProxy.MOVE_NORTH;
            }
            else if (_mode_aux_info == PORT_MOVE_AUX_INFO__GO_TO_SOUTH) {
                operation = PortProxy.MOVE_SOUTH;
            }
            else if (_mode_aux_info == PORT_MOVE_AUX_INFO__GO_TO_EAST) {
                operation = PortProxy.MOVE_EAST;
            }
            else { operation = PortProxy.MOVE_WEST; }
        }

        return node.port_move(port, operation, b_is_internal_view);
    }

        /** Checks to see if the port can support another connection to it. */
    private boolean canSupportNewConnection() {
            // as source?
        int[][] types = getPossibleDataTypesForNewConnectionBuild(true);
        if (types.length > 0) { return true; }

            // as sink?
        types = getPossibleDataTypesForNewConnectionBuild(false);
        return (types.length > 0);
    }

    /////////////////////////////////////////////////////////////////////
    ////////// implementations of AlfiWidgetIF methods //////////////////

        /** AlfiWidgetIF method. */
    public EditorFrame getEditor() { return m_editor; }

        /** AlfiWidgetIF method. */
    public Object getAssociatedProxyObject() { return getAssociatedProxy(); }

    /////////////////////////////////////////////////////////////////////
    ////////// implementations of GenericPortWidget abstract methods ////

        /** Implementation of
          * GenericPortWidget.getPossibleDataTypesForNewConnectionBuild().
          *
          * Here are the rules for the types ports will return:
          *
          * As a data source:
          *   If this port is a source port, it returns its own data type and a
          *   STANDARD_BUILD hint.  If this port is a sink port, no data types
          *   are returned.
          *
          * As a data sink:
          *   If this port is a sink port, and has no currently existing
          *   connection, it returns its own data type and a STANDARD_BUILD
          *   hint.  Otherwise no data types will be returned.
          *
          * In general, the return value will be int[1][2] where [0][0] is the
          * possible data type, and [0][1] is the corresponding hint.  A
          * value of int[0][0] is returned when no possibilities exist.
          */
    int[][] getPossibleDataTypesForNewConnectionBuild(
                                                   boolean _b_as_source_port) {
        PortProxy port = getAssociatedProxy();
        ALFINode container = m_editor.getNodeInEdit();
 
        int[][] types_and_hints = { { port.getDataType(),
                                   ConnectionBuildingWidget.STANDARD_BUILD } };

        if (_b_as_source_port) {
            if (! isSourcePort()) {
                types_and_hints = new int[0][0];
            }
        }
        else {
            if (! isSinkPort()) {
                types_and_hints = new int[0][0];
            }
            else if (port.hasConnections(container, getOwnerMemberProxy())) {
                types_and_hints = new int[0][0];
            }
        }

        return types_and_hints;
    }

    public String generateDescription() {
        MemberNodeWidget mnw = getOwnerMemberWidget();
        String desc = (mnw != null) ?
                              mnw.getAssociatedProxy().getLocalName() : "this";
        desc += ":" + getAssociatedProxy().getName();
        return desc;
    }

    //////////////////////////////
    // event processing //////////

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseDown(int _modifiers, Point _dc, Point _vc,
                                                           JGoView _view) {
        if (_view instanceof ALFIView) {
            final ALFIView alfi_view = (ALFIView) _view;
            m_mouse_down_modifiers = _modifiers;

            if ((m_owner_member_widget == null) ||
                                   m_owner_member_widget.isRawExternalView()) {
                if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
                    alfi_view.setActiveObject(this);
                    if ((_modifiers & (InputEvent.CTRL_MASK |
                                       InputEvent.SHIFT_MASK)) != 0) {
                        alfi_view.selectObject(this);
                        return true;
                    }
                }
                else if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
                    alfi_view.pickObject(this); 
                    alfi_view.doWidgetPopupMenu(this, _modifiers, _dc, _vc);
                    return true;
                }
            }
            else { return false; }
        }
        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseUp(int _modifiers, 
                             Point _dc, Point _vc, JGoView _view) {
        ALFIView view = (ALFIView) _view;
        if (view.getActiveObject() == this) {
            view.clearActiveObject();
        }

        m_mouse_down_modifiers = 0;

        return false;
    }

        /** Needed for implementation of WidgetIF interface. */
    public boolean doMouseMove(int _modifiers,
                               Point _dc, Point _vc, JGoView _view) {
        if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
            if (mb_is_local_port && (_view instanceof ALFIView)) {
                reposition(_dc);
            }
            return true;
        }

        return false;
    }

        /**
         * A mouse click on a port either starts the construction of a
         * connection, or completes the construction of a connection which is
         * currently under construction.
         */
    public boolean doMouseClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        if (_view instanceof ALFIView) {
            final ALFIView alfi_view = (ALFIView) _view;
            _view.getSelection().clearSelection();
            boolean b_is_internal_view =
                                alfi_view.getOwnerFrame().isInternalView();

                // LMB operations
            if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
                if (b_is_internal_view) {
                    if (canSupportNewConnection()) {
                            // a temp object added only to the view
                        final ConnectionBuildingWidget cbw =
                                new ConnectionBuildingWidget(this, alfi_view);
                        alfi_view.setActiveObject(cbw);
                        alfi_view.addObjectAtHead(cbw);

                            // wait a moment before changing cursor
                        java.util.Timer timer = new java.util.Timer();
                        timer.schedule(
                            new TimerTask() {
                                public void run() {
                                    if (alfi_view.getActiveObject() == cbw) {
                                        alfi_view.setCursor(new Cursor(
                                                      Cursor.CROSSHAIR_CURSOR));
                                    }
                                }
                            }, 250);
                    }

                    return true;    // consume event
                }
                else {
                        // preparation for repositioning
                    _view.selectObject(this);
                    return true;
                }
            }
            else if ((_modifiers & InputEvent.BUTTON2_MASK) != 0) {
                    // preparation for repositioning
                _view.selectObject(this);
                return true;
            }
            else if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
                if (m_owner_member_widget != null) {
                    return false;
                }
                alfi_view.pickObject(this); 
                alfi_view.doWidgetPopupMenu(this, _modifiers, _dc, _vc);
            }

        }

        return false;
    }

    public boolean doMouseDblClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
        if (_view instanceof ALFIView) {
            ALFIView alfi_view = (ALFIView) _view; 
            alfi_view.cancelActiveObjectAction();
            boolean b_is_internal_view =
                                alfi_view.getOwnerFrame().isInternalView();

            ALFINode container_node = alfi_view.getOwnerFrame().getNodeInEdit();
            ALFINode port_owner_node =
                ((! b_is_internal_view) || (m_owner_member_widget == null)) ?
                    container_node : m_owner_member_widget.getAssociatedProxy().
                                        getAssociatedMemberNode(container_node);

                // bring up port default value dialog (primitives only for now)
            if ((getAssociatedProxy().getIOType() == PortProxy.IO_TYPE_INPUT) &&
                                                port_owner_node.isPrimitive()) {
                String port_name = getAssociatedProxy().getName();
                String current_value = null;
                boolean b_default_is_local = false;
                if (port_owner_node.parameterSetting_containsLocal(port_name)){
                    current_value = port_owner_node.parameterSetting_get(
                                                         port_name).getValue();
                    b_default_is_local = true;
                }
                else if (port_owner_node.parameterSetting_containsInherited(
                                                                   port_name)) {
                    current_value = port_owner_node.parameterSetting_get(
                                                         port_name).getValue();
                    b_default_is_local = false;
                }

                String message = "Enter the local default value for input " +
                    "port " + port_name + " (currently [" + current_value + 
                    "]) :";
                String title = "Set Input Port Local Default Value";

                String set = "Set Local Value";
                String unset = "Unset Local Value";
                String cancel = "Cancel";
                String[] option_set_1 = { set, unset, cancel };
                String[] option_set_2 = { set, cancel };
                String[] options =
                        ((current_value != null) && b_default_is_local) ?
                                                 option_set_1 : option_set_2;
                JOptionPane pane = new JOptionPane(message,
                    JOptionPane.QUESTION_MESSAGE,
                    JOptionPane.YES_NO_CANCEL_OPTION, null, options);
                pane.setWantsInput(true);

                JDialog dialog = pane.createDialog(alfi_view, title);
                dialog.setModal(true);
                dialog.setVisible(true);

                if (pane.getValue() != cancel) {
                    if (port_owner_node.parameterSetting_containsLocal(
                                                                  port_name)) {
                       port_owner_node.parameterSetting_removeLocal(port_name);
                    }

                    if (pane.getValue() == set) {
                        String new_value = (String) pane.getInputValue();
                        new_value.trim();
                        if (new_value.length() > 0) {
                            port_owner_node.parameterSetting_add(port_name,
                                                              new_value, false);
                        }
                    }

                    setPortShape();
                }
            }
        }

        return true;
    }

    public boolean doUncapturedMouseMove(int _flags, Point _dc, Point _vc,
                                                                JGoView _view) {
        if (_view instanceof ALFIView) {
            ((ALFIView) _view).getOwnerFrame().setStatusText(toString());
        }
        return true;    // consume event
    }

    //////////////////
    // misc //////////

    int getInitialConnectionDirection() {
        int direction = INVALID_POINTING;

        int edge = getEdge();
        boolean b_external_port = isExternalPort();
        int io_type = getAssociatedProxy().getIOType();

            // first assume external input port
        if (edge == PortProxy.EDGE_NORTH) { direction = SOUTH_POINTING; }
        else if (edge == PortProxy.EDGE_SOUTH) { direction = NORTH_POINTING; }
        else if (edge == PortProxy.EDGE_EAST) { direction = WEST_POINTING; }
        else if (edge == PortProxy.EDGE_WEST) { direction = EAST_POINTING; }

            // now take into account if port is on a member node instead
        if (! b_external_port) {
            direction = reverseDirection(direction);
        }

            // take into account I/O type
        if (io_type == PortProxy.IO_TYPE_OUTPUT) {
            direction = reverseDirection(direction);
        }

        return direction;
    }

    private int reverseDirection(int _original_direction) {
        if (_original_direction == NORTH_POINTING) { return SOUTH_POINTING; }
        if (_original_direction == SOUTH_POINTING) { return NORTH_POINTING; }
        if (_original_direction == EAST_POINTING) { return WEST_POINTING; }
        if (_original_direction == WEST_POINTING) { return EAST_POINTING; }
        return INVALID_POINTING;
    }

        /** If port is on the container (aka an external port.) */
    public boolean isExternalPort() { return (m_owner_member_widget == null); }

        /** If port is on a member node. */
    public boolean isPortOnMemberNode() { return (! isExternalPort()); }

        /**
         * Input ports on member nodes and output ports on the container are
         * sink ports for connections.  A sink port can only take a single
         * connection.
         */
    public boolean isSinkPort() {
        if (getAssociatedProxy().getIOType() == PortProxy.IO_TYPE_OUTPUT) {
            if (isExternalPort()) { return true; }
            else { return false; }
        }
        else if (getAssociatedProxy().getIOType() == PortProxy.IO_TYPE_INPUT) {
            if (isPortOnMemberNode()) { return true; }
            else { return false; }
        }
        else { return false; }
    }

        /**
         * Output ports on member nodes and input ports on the container are
         * source ports for connections.  A source port can take multiple
         * connections.
         */
    public boolean isSourcePort() {
        if (getAssociatedProxy().getIOType() == PortProxy.IO_TYPE_OUTPUT) {
            if (isPortOnMemberNode()) { return true; }
            else { return false; }
        }
        else if (getAssociatedProxy().getIOType() == PortProxy.IO_TYPE_INPUT) {
            if (isExternalPort()) { return true; }
            else { return false; }
        }
        else { return false; }
    }

        /** Returns the edge of the container or member this port sits on. */
    public int getEdge() {
        if (isPortOnMemberNode()) {
            MemberNodeProxy member = m_owner_member_widget.getAssociatedProxy();
            PortProxy port = this.getAssociatedProxy();
            return member.determinePortEdgeAfterOps(port);
        }
        else { return getAssociatedProxy().getPosition()[0]; }
    }

    public String toString() {
        String info = getAssociatedProxy().getName() + " (" +
            PortProxy.DATA_TYPE_BY_STRING_ARRAY[
                                    getAssociatedProxy().getDataType()] + " " +
            PortProxy.IO_TYPE_BY_STRING_ARRAY[
                                    getAssociatedProxy().getIOType()] + ")";

        String current_value = null;
        boolean b_is_internal_view = m_editor.isInternalView();
        ALFINode container_node = m_editor.getNodeInEdit();
        ALFINode port_owner_node =
                ((! b_is_internal_view) || (m_owner_member_widget == null)) ?
                    container_node : m_owner_member_widget.getAssociatedProxy().
                                        getAssociatedMemberNode(container_node);
        if ((getAssociatedProxy().getIOType() == PortProxy.IO_TYPE_INPUT) &&
                                                port_owner_node.isPrimitive()) {
            String port_name = getAssociatedProxy().getName();
            if (port_owner_node.parameterSetting_containsLocal(port_name) ||
               (port_owner_node.parameterSetting_containsInherited(port_name)
                                                                           ))  {
                current_value = port_owner_node.parameterSetting_get(
                                                         port_name).getValue();
            }
        }

        if (current_value != null) {
            info += " [ " +  current_value + " ] ";        
        }

        return info;
    }

    /////////////////////////////////
    // member field access //////////

    public PortProxy getAssociatedProxy() {
        return ((PortProxy) m_associated_proxy);
    }

        /** Overrides GenericPortProxy method which merely returned null. */
    public MemberNodeWidget getOwnerMemberWidget() {
        return m_owner_member_widget;
    }

    //////////////////////////////////////////////////////////
    //////////  ProxyChangeEventListener methods /////////////

        /** ProxyChangeEventListener method. */
    public void proxyChangePerformed(ProxyChangeEvent e) {
        Object o = e.getTempProxyObjectContainingChanges();
        MemberNodeProxy owner_mnp = (m_owner_member_widget != null) ?
                             m_owner_member_widget.getAssociatedProxy() : null;

        if (o instanceof PortProxy) {
                // this takes care of the color and pointing direction of a
                // changed port.  the change event is also delivered to the
                // EditorFrame where the port position is set if need be.
            setPortShape();
        }
        else if (o instanceof MemberNodeProxy) {
            MemberNodeProxy mnp = (MemberNodeProxy) o;
            if (    (mnp.getRotation() != MemberNodeProxy.ROTATION_INVALID) ||
                    (mnp.getSymmetry() != MemberNodeProxy.SYMMETRY_INVALID) ||
                    (mnp.getSwap() != MemberNodeProxy.SWAP_INVALID) ) {
                setPortShape();
            }
        }
        else {
            System.err.println("(" + this + ").proxyChangePerformed(" +
                e + "):\n\tWARNING: Invalid temp proxy type passed in event.");
        }
    }

    //**************************************************************************
    //***** member classes *****************************************************

    private class PortTriangle extends JGoPolygon {
        private int m_link_spot;

        public PortTriangle(boolean _b_has_local_setting) {
            super();

            MemberNodeProxy owner_member = (m_owner_member_widget != null) ?
                            m_owner_member_widget.getAssociatedProxy() : null;
            boolean b_is_on_a_member_node = (m_owner_member_widget != null);

            int io_type = getAssociatedProxy().getIOType();
            int data_type = getAssociatedProxy().getDataType();

            int edge_port_is_on =  PortProxy.EDGE_INVALID;
            if (m_owner_member_widget == null) {
                edge_port_is_on = getAssociatedProxy().getPosition()[0];
            }
            else if (m_owner_member_widget.isRawExternalView()) {
                edge_port_is_on = getAssociatedProxy().getPosition()[2];
            }
            else {
                edge_port_is_on =
                    owner_member.determinePortEdgeAfterOps(getAssociatedProxy());
            }

            m_link_spot =
                    determineLinkSpot(b_is_on_a_member_node, edge_port_is_on);

            int pointing_direction =
                    determinePortPointingDirection(io_type, edge_port_is_on);
            Vector point_vector = getPortPoints(pointing_direction);
            setPoints(point_vector);

            int width = (mb_is_local_port) ? 2 : 1;
            Color color = (_b_has_local_setting) ? Color.black : Color.white;
            setPen(new JGoPen(JGoPen.SOLID, width, color));

            color = PortWidget.getDataTypeColor(data_type);
            setBrush(new JGoBrush(JGoBrush.SOLID, color));
        }

        public int getLinkSpot() { return m_link_spot; }

        private int determineLinkSpot(boolean b_is_on_a_member_node,
                                                         int _edge_port_is_on) {
            if (_edge_port_is_on == PortProxy.EDGE_NORTH) {
                if (b_is_on_a_member_node) { return JGoObject.TopCenter; }
                else { return JGoObject.BottomCenter; }
            }
            else if (_edge_port_is_on == PortProxy.EDGE_SOUTH) {
                if (b_is_on_a_member_node) { return JGoObject.BottomCenter; }
                else { return JGoObject.TopCenter; }
            }
            else if (_edge_port_is_on == PortProxy.EDGE_WEST) {
                if (b_is_on_a_member_node) { return JGoObject.LeftCenter; }
                else { return JGoObject.RightCenter; }
            }
            else if (_edge_port_is_on == PortProxy.EDGE_EAST) {
                if (b_is_on_a_member_node) { return JGoObject.RightCenter; }
                else { return JGoObject.LeftCenter; }
            }
            else {
                System.err.println("PortTriangle.determineLinkSpot(" +
                    b_is_on_a_member_node + ", " + _edge_port_is_on + "):\n" +
                    "\tWARNING: Invalid edge.");
                return JGoObject.NoSpot;
            }
        }

        private int determinePortPointingDirection(
                                           int _io_type, int _edge_port_is_on) {

            int pointing_direction = INVALID_POINTING;

            if (_io_type == PortProxy.IO_TYPE_INPUT) {
                if (_edge_port_is_on == PortProxy.EDGE_NORTH) {
                    pointing_direction = SOUTH_POINTING;
                }
                else if (_edge_port_is_on == PortProxy.EDGE_SOUTH) {
                    pointing_direction = NORTH_POINTING;
                }
                else if (_edge_port_is_on == PortProxy.EDGE_EAST) {
                    pointing_direction = WEST_POINTING;
                }
                else if (_edge_port_is_on == PortProxy.EDGE_WEST) {
                    pointing_direction = EAST_POINTING;
                }
            }
            else if (_io_type == PortProxy.IO_TYPE_OUTPUT) {
                if (_edge_port_is_on == PortProxy.EDGE_NORTH) {
                    pointing_direction = NORTH_POINTING;
                }
                else if (_edge_port_is_on == PortProxy.EDGE_SOUTH) {
                    pointing_direction = SOUTH_POINTING;
                }
                else if (_edge_port_is_on == PortProxy.EDGE_EAST) {
                    pointing_direction = EAST_POINTING;
                }
                else if (_edge_port_is_on == PortProxy.EDGE_WEST) {
                    pointing_direction = WEST_POINTING;
                }
            }
            return pointing_direction;
        }

        private Vector getPortPoints(int _pointing_direction) {
            Point[] points = new Point[3];
            if (_pointing_direction == NORTH_POINTING) {
                points[0] = new Point(((PORT_WIDTH - 1) / 2), 0);
                points[1] = new Point(0, PORT_WIDTH - 1);
                points[2] = new Point(PORT_WIDTH - 1, PORT_WIDTH - 1);
            }
            else if (_pointing_direction == SOUTH_POINTING) {
                points[0] = new Point(0, 0);
                points[1] = new Point(((PORT_WIDTH - 1) / 2), PORT_WIDTH - 1);
                points[2] = new Point(PORT_WIDTH - 1, 0);
            }
            else if (_pointing_direction == EAST_POINTING) {
                points[0] = new Point(0, 0);
                points[1] = new Point(0, PORT_WIDTH - 1);
                points[2] = new Point(PORT_WIDTH - 1, ((PORT_WIDTH - 1) / 2));
            }
            else if (_pointing_direction == WEST_POINTING) {
                points[0] = new Point(PORT_WIDTH - 1, 0);
                points[1] = new Point(0, ((PORT_WIDTH - 1) / 2));
                points[2] = new Point(PORT_WIDTH - 1, PORT_WIDTH - 1);
            }
            Vector point_vector = new Vector();
            for (int i = 0; i < points.length; i++) {
                point_vector.add(points[i]);
            }
            return point_vector;
        }
    }
}
