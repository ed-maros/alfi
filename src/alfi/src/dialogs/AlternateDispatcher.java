package edu.caltech.ligo.alfi.dialogs;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import javax.swing.JComponent;
import java.awt.event.KeyEvent;


/**
  * <pre>
  * The class sends the KeyEvents to the appropriate JDialog
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class AlternateDispatcher  implements KeyEventDispatcher{

    private JComponent m_send_to_component;
    private KeyboardFocusManager m_manager;        

    public AlternateDispatcher( KeyboardFocusManager _manager ) {
        m_manager = _manager;
    }
        
    public void sendToComponent( JComponent _component ) {
        m_send_to_component = _component;
    }

    public boolean dispatchKeyEvent(KeyEvent e){

        if (e.getSource() != m_send_to_component && 
            m_send_to_component != null){
            System.out.println("redispatching");
            m_manager.redispatchEvent(m_send_to_component,e);
        }
        return true;
    }//end dispatchKeyEvent

}
