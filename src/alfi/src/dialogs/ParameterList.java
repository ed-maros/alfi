package edu.caltech.ligo.alfi.dialogs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Dialog;
import java.awt.event.*;
import java.awt.Window;


import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ChangeEvent;


import edu.caltech.ligo.alfi.common.GuiConstants;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.bookkeeper.ParameterDeclaration;


/**
  * <pre>
  * The list that displays the parameters and values
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */


public class ParameterList extends JList
                            implements ParameterTableIF, MouseListener,
                                       ListSelectionListener{

    Dialog m_parent;

    public ParameterList(Dialog _parent, DefaultListModel _model) { 
        super(_model);

        m_parent = _parent;

            // Setup the selection model 
        getSelectionModel().setSelectionMode(
                                          ListSelectionModel.SINGLE_SELECTION);

            // Add the different listeners
        addMouseListener(this);

        
    }
    

    private void editValue(int _row_index) {
        displayValueEditor(_row_index);
    }

    private void displayValueEditor(int _row_index) {
        DefaultListModel model = ( DefaultListModel )getModel();

        ParameterDeclaration param = 
                          ( ParameterDeclaration )model.getElementAt(_row_index);

        displayValueEditor(_row_index, param);
    }

    private void displayValueEditor(int _row_index, 
                                    ParameterDeclaration _value) {

        MultilineEditorDialog edit_dialog = 
            new MultilineEditorDialog(m_parent, true, _row_index, this); 

        edit_dialog.setTitle(_value.getName()
            + MultilineEditorDialog.DIALOG_TITLE);

        edit_dialog.setValue(_value.getValue());
        
        edit_dialog.setVisible(true);
    }


    public void updateCell (String _text_value, int _row) {
        String trimmed_value = _text_value.trim();
        
        DefaultListModel model = ( DefaultListModel )getModel();
        ParameterDeclaration param = 
                         ( ParameterDeclaration )model.getElementAt(_row);
        
        param.setValue(trimmed_value);
        
        model.setElementAt(param, _row);
        
    }

    // MouseListener methods
    public void mousePressed (MouseEvent _event) {

        if (SwingUtilities.isRightMouseButton(_event)) {
            int index = locationToIndex(_event.getPoint());

            if (index >= 0) {
                displayValueEditor(index);
            }

        }
    }

    public void mouseReleased(MouseEvent e) { ; }
    public void mouseEntered(MouseEvent e) { ; }
    public void mouseExited(MouseEvent e) { ; }
    public void mouseClicked(MouseEvent e) { ; }
    public void valueChanged(ListSelectionEvent e) {
        System.out.println("value changed " + e);
    }

}
