package edu.caltech.ligo.alfi.dialogs;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.tree.*;
import java.util.*;

import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;

public class ShowBundleContentDialog extends JDialog {

    ShowBundleContentDialog(Frame _owner_frame, Hashtable _content) {
        super(_owner_frame, "Bundle Content Dialog");

        setVisible(false);
        setModal(true);

        Box tree_box = Box.createHorizontalBox();
        tree_box.setBorder(BorderFactory.createTitledBorder(
                       BorderFactory.createLoweredBevelBorder(),
                       "Available Channels"));

        JTree content_tree = new JTree(_content) {
            public String convertValueToText(Object _value, boolean _sel,
                                             boolean _expanded, boolean _leaf,
                                             int _row, boolean _hasFocus) {
                Object key = ((DefaultMutableTreeNode) _value).getUserObject();
                return (key instanceof BundleContentMapKey) ?
                        //((BundleContentMapKey) key).toString() : "error";
                        ((BundleContentMapKey) key).getChannelName() : "error";
            }
          };

        content_tree.expandRow(0);
        JScrollPane tree_scroller = new JScrollPane(content_tree,
                              ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                              ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        tree_box.add(tree_scroller);

            // OK and cancel buttons
        JPanel button_panel = new JPanel();
        button_panel.setLayout(new BoxLayout(button_panel, BoxLayout.X_AXIS));
        button_panel.setBorder(BorderFactory.createEtchedBorder());
        JButton ok_button = new JButton("OK");
        ok_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                }
            });
        button_panel.add(ok_button);
        getRootPane().setDefaultButton(ok_button);

        Box main_box =  Box.createVerticalBox();
        main_box.add(Box.createVerticalStrut(5));
        main_box.add(tree_box);
        main_box.add(Box.createVerticalStrut(10));
        main_box.add(button_panel);
        main_box.add(Box.createVerticalStrut(5));

        Container content_pane = getContentPane();
        content_pane.add(main_box);
    }

    ////////////////////////////////////////////////////////////////////////////
    ///// static methods ///////////////////////////////////////////////////////

    public static void showDialog(Frame _owner_frame, Hashtable _content) {

        ShowBundleContentDialog dialog =
                           new ShowBundleContentDialog(_owner_frame, _content);

        dialog.pack();
        dialog.setSize(500, 600);
        EditorFrame.centerDialog(dialog);

        dialog.setVisible(true);
    }
}
