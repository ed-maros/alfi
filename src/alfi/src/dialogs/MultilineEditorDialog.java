package edu.caltech.ligo.alfi.dialogs;

import java.lang.String;
import java.awt.*;
import java.awt.event.*;
import java.util.Hashtable;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.*;
import javax.swing.event.*;
import javax.swing.undo.*;

import edu.caltech.ligo.alfi.bookkeeper.ALFINode;

public class MultilineEditorDialog extends AlfiDialog 
                                   implements KeyListener{
    
    // Strings used for the dialog 
    public static final String DIALOG_TITLE = new String(" - Value Dialog");

    // Menu item strings
    private static final String FILE = new String("File");
    private static final String SAVE_CLOSE = new String("Close");
    private static final String CANCEL_CLOSE = new String("Cancel");

    private static final String EDIT = new String("Edit"); 
    private static final String UNDO = new String("Undo"); 
    private static final String REDO = new String("Redo"); 

    // undo helpers
    protected UndoAction m_undo_action;
    protected RedoAction m_redo_action;
    protected UndoManager m_undo_manager = null;
    private  ALFIUndoableEditListener m_undo_listener = null; 

    // Swing components
    protected JTextPane m_text_panel;    
    protected JPanel m_button_panel;    
    
    // saved values
    protected String m_value = new String("");
    protected String m_original_value = new String("");

    private DefaultStyledDocument m_document;
    private Hashtable m_actions;
    private boolean m_closing;
    private boolean m_read_only = false;
    private JMenuItem m_close_button;

    private int m_row;
    protected ParameterTableIF m_table;

    /**
      * Constructs a MultilineEditorDialog with a Dialog as its parent 
      * component
      */
    public MultilineEditorDialog ( Dialog _parent, 
                                   boolean _modal,
                                   int _row,
                                   ParameterTableIF _table
                                    ) {
        super(_parent, "Value Editor", _modal, 500, 300);

        m_table = _table;
        m_row = _row;

        init();
        setResizable(true);
    }

    /**
      * Constructs a MultilineEditorDialog with a Frame as its parent 
      * component
      */
    public MultilineEditorDialog ( Frame _parent,
                                   boolean _modal, 
                                   int _row,
                                   ParameterTableIF _table) {
        super(_parent, "Parameter Value Editor", _modal, 500, 300);

        m_table = _table;
        m_row = _row;

        init();
        setResizable(true);
    }


    /**
      * Initializes the components, sets the proper size, and places
      * the editor in the middle of the screen.
      * Placing the dialog in the middle of the screen is a kludge
      * to a problem encountered with the Sun FVWM, which places all
      * java dialogs and frames at the top left corner with the title
      * bar and button difficult to grab.
      */
    private void init() {
        // Initialize the GUI components
        initComponents();

        // Add some key bindings to the keymap.
        addKeymapBindings();

        // Initialize the document used to detect changes
        initDocument();

    }

    /**
      * Disallows saving the value
      */
    public void setReadOnly (boolean  _read_only) {
        super.setReadOnly(_read_only);
        m_close_button.setEnabled(!_read_only);
    }

    
    
    /**
      * Sets the value of the text displayed in the edit panel.
      */
    public void setValue (String _value) {

        m_closing = false;
        m_value = _value;
        m_original_value = _value;

        // Start watching for undoable edits
        m_undo_manager = new UndoManager();
        m_undo_listener =  new ALFIUndoableEditListener();
        m_document.addUndoableEditListener(m_undo_listener);
        m_undo_action.updateUndoState();
        m_redo_action.updateRedoState();

        m_text_panel.setText(m_value);
        m_text_panel.moveCaretPosition(0);

    }

    /**
      * Gets the text in the edit panel.
      */
    public String getValue () {
        return m_value;
    }


    /**
      *  Sends an OK message to the dialog
      *
      */
    public void performOK ( ) {
        m_closing = true;
        closeActionPerformed(null);
    } 
    
    /**
      *  Sends a cancel message to the dialog
      *
      */
    public void performCancel ( ) {
        cancelButtonActionPerformed(null);
    } 

    /**
      * Initializes the GUI components of the dialog.
      */
    protected void initComponents () {
        getContentPane().setLayout(new BorderLayout());

        m_document = new DefaultStyledDocument();

        m_text_panel = new JTextPane(m_document);
        m_text_panel.setBorder(new EtchedBorder());  
        m_text_panel.setCaretPosition(0);
        m_text_panel.setMargin(new Insets(5,5,5,5));
        m_text_panel.addKeyListener(this);
  
        JScrollPane scroll_pane = new JScrollPane(m_text_panel,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll_pane.setPreferredSize(new Dimension(m_width, m_height));

        getContentPane().add(scroll_pane, BorderLayout.CENTER);

        // Set up the menu bar
        createActionTable(m_text_panel);
        JMenu file_menu = createFileMenu();
        JMenu edit_menu = createEditMenu();
        JMenuBar menu_bar = new JMenuBar();
        menu_bar.add(file_menu);
        menu_bar.add(edit_menu);
        setJMenuBar(menu_bar);

        super.initComponents();

        addComponentListener(
            new ComponentAdapter()
            {
               public void componentShown(ComponentEvent e)
                {
                    m_text_panel.requestFocus();
                }
            }
        );
    }
    
    /**
      * Initializes the document which tracts the changes in the document
      */
    protected void initDocument () {
        SimpleAttributeSet attrib = initAttributes();

        try {
            m_document.insertString(0, m_value, attrib);
        } catch (BadLocationException ble) {
            System.err.println("Couldn't insert initial text");
        }
    }

    /**
      * Initializes the attributes of the text
      */
    protected SimpleAttributeSet initAttributes () {

        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attrs, "Courier");
        StyleConstants.setFontSize(attrs, 12);
        //StyleConstants.setForeground(attrs, Color.red);

        return attrs;
    }

    /**
      * Updates the data value of the dialog
      */
    protected void updateData () {
        m_value = m_text_panel.getText();

        if (m_table != null) {  
            m_table.updateCell(m_value, m_row);
        }
    }

    /**
      * Saves the current text.
      */
    protected void saveActionPerformed (ActionEvent _event) {
        try {
            updateData();
        } catch (Exception e) {
        }
    }

    /**
      * Checks if there is any text which need saving
      */
    protected boolean hasUnsavedData () {
        String text_value = m_text_panel.getText();
        return (!m_value.equals(text_value));
    }
    
    /**
      * Checks if the user has made any modifications
      */
    protected boolean hasChangedData () {
        String text_value = m_text_panel.getText();
        return (!m_original_value.equals(text_value));
    }
    
    /**
      * Closing this dialog by updating the return status and hiding the 
      * dialog
      */
    protected void doClose (int _return_status){
        super.doClose(_return_status);
        m_document.removeUndoableEditListener(m_undo_listener);
        m_undo_listener = null;

        if (m_undo_manager != null) {
            m_undo_manager.end();
            m_undo_manager.die();
            m_undo_manager = null;
        }
    }   
     
    /**
      * Defines a couple of emacs key bindings to the key map for navigation.
      */
    protected void addKeymapBindings() {
        //Add a new key map to the keymap hierarchy.
        Keymap keymap = m_text_panel.addKeymap("MyEmacsBindings",
                                           m_text_panel.getKeymap());

        //Ctrl-b to go backward one character
        Action action = getActionByName(DefaultEditorKit.backwardAction);
        KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_B, Event.CTRL_MASK);
        keymap.addActionForKeyStroke(key, action);

        //Ctrl-f to go forward one character
        action = getActionByName(DefaultEditorKit.forwardAction);
        key = KeyStroke.getKeyStroke(KeyEvent.VK_F, Event.CTRL_MASK);
        keymap.addActionForKeyStroke(key, action);

        //Ctrl-p to go up one line
        action = getActionByName(DefaultEditorKit.upAction);
        key = KeyStroke.getKeyStroke(KeyEvent.VK_P, Event.CTRL_MASK);
        keymap.addActionForKeyStroke(key, action);

        //Ctrl-n to go down one line
        action = getActionByName(DefaultEditorKit.downAction);
        key = KeyStroke.getKeyStroke(KeyEvent.VK_N, Event.CTRL_MASK);
        keymap.addActionForKeyStroke(key, action);

        //Ctrl-a to go to the beginning of the line
        action = getActionByName(DefaultEditorKit.beginLineAction);
        key = KeyStroke.getKeyStroke(KeyEvent.VK_A, Event.CTRL_MASK);
        keymap.addActionForKeyStroke(key, action);

        //Ctrl-e to go to the end of the line
        action = getActionByName(DefaultEditorKit.endLineAction);
        key = KeyStroke.getKeyStroke(KeyEvent.VK_E, Event.CTRL_MASK);
        keymap.addActionForKeyStroke(key, action);

        m_text_panel.setKeymap(keymap);
    }

    /**
      * Creates the file menu and its menu items
      */
    protected JMenu createFileMenu () {
        JMenu menu = new JMenu(FILE);
        menu.setMnemonic('F');
                
        SaveAction save_action = new SaveAction();
        m_close_button = menu.add(save_action);
        m_close_button.setMnemonic('C');

        CancelAction cancel_action = new CancelAction();
        JMenuItem cancel = menu.add(cancel_action); 
        cancel.setMnemonic('A');

        return menu;
    }

    /**
      * Create the edit menu and its menu items.
      */
    protected JMenu createEditMenu() {
        JMenu menu = new JMenu(EDIT);
        menu.setMnemonic('E');

        m_undo_action = new UndoAction();
        JMenuItem undo = menu.add(m_undo_action);
        undo.setMnemonic('U');

        m_redo_action = new RedoAction();
        JMenuItem redo = menu.add(m_redo_action);
        redo.setMnemonic('R');

        menu.addSeparator();

        // These actions come from the default editor kit.
   
        JMenuItem cut = menu.add(getActionByName(DefaultEditorKit.cutAction));
        cut.setMnemonic('T');
        JMenuItem copy = menu.add(getActionByName(DefaultEditorKit.copyAction));
        copy.setMnemonic('C');
        JMenuItem paste = menu.add(
                          getActionByName(DefaultEditorKit.pasteAction));
        paste.setMnemonic('P');

        menu.addSeparator();

        JMenuItem select_all = menu.add(
                           getActionByName(DefaultEditorKit.selectAllAction));
        select_all.setMnemonic('A');
        return menu;
    }

    public JComponent getTextPane ( ) {
        return (JComponent) m_text_panel;
    }

    /**
      * Create an action table to allow us to find an action provided
      * by the editor kit by its name.
      */
    private void createActionTable(JTextComponent _text_component) {
        m_actions = new Hashtable();
        Action[] actionsArray = _text_component.getActions();
        for (int i = 0; i < actionsArray.length; i++) {
            Action a = actionsArray[i];
            m_actions.put(a.getValue(Action.NAME), a);
        }
    }

    /**
      * Get the editor kit's action by its name.
      */
    private Action getActionByName (String _name) {
        return (Action)(m_actions.get(_name));
    }

    /**
      * Defines a listener for edits that can be undone.
      */
    protected class ALFIUndoableEditListener
                    implements UndoableEditListener {
        public void undoableEditHappened(UndoableEditEvent e) {
            m_undo_manager.addEdit(e.getEdit());
            m_undo_action.updateUndoState();
            m_redo_action.updateRedoState();
        }
    }


    ////////////////////////////////////////////
    //     Key Listener methods ////////////////
    ////////////////////////////////////////////


    public void keyPressed (KeyEvent _event) {
        //String text = KeyEvent.getKeyText(_event.getKeyCode());
        //System.err.println("editor pane " + m_text_panel.getName() +
        //                           " key: " + text);
    }

    public void keyTyped (KeyEvent _event) {;}

    public void keyReleased (KeyEvent _event) {
        int code = _event.getKeyCode();
        int modifier = _event.getModifiers();

        if (code == KeyEvent.VK_ESCAPE) {
            closeActionPerformed(null);
        } else if (code == KeyEvent.VK_S && _event.isControlDown()) {
            okButtonActionPerformed(null);
        }
    }


    ////////////////////////////////////////////
    //     Menu Actions     ////////////////////
    ////////////////////////////////////////////


    class SaveAction extends AbstractAction {
        public SaveAction() {
            super(SAVE_CLOSE);
        }
        public void actionPerformed (ActionEvent _event) {
            okButtonActionPerformed(_event);
        }
    }

    class CancelAction extends AbstractAction {
        public CancelAction() {
            super(CANCEL_CLOSE);
        }
        public void actionPerformed (ActionEvent _event) {
            cancelButtonActionPerformed(_event);
        }
    }

    class UndoAction extends AbstractAction {
        public UndoAction() {
            super(UNDO);
            setEnabled(false);
        }
          
        public void actionPerformed(ActionEvent e) {
            if (m_undo_manager.canUndo()) { 
                try {
                    m_undo_manager.undo();
                } catch (CannotUndoException ex) {
                    System.err.println("\nUnable to undo: " + ex);
                    m_undo_manager.discardAllEdits();
                    //ex.printStackTrace();
                }
            }
            updateUndoState();
            m_redo_action.updateRedoState();
        }
          
        protected void updateUndoState() {
            if (m_undo_manager.canUndo()) {
                setEnabled(true);
                putValue(Action.NAME, m_undo_manager.getUndoPresentationName());
            } else {
                setEnabled(false);
                putValue(Action.NAME, UNDO);
            }
        }      
    }    

    class RedoAction extends AbstractAction {
        public RedoAction() {
            super(REDO);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
            if (m_undo_manager.canRedo()) {
                try {
                    m_undo_manager.redo();
                } catch (CannotRedoException ex) {
                    System.err.println("Unable to redo: " + ex);
                    ex.printStackTrace();
                }
            }
            updateRedoState();
            m_undo_action.updateUndoState();
        }

        protected void updateRedoState() {
            if (m_undo_manager.canRedo()) {
                setEnabled(true);
                putValue(Action.NAME, m_undo_manager.getRedoPresentationName());
            } else {
                setEnabled(false);
                putValue(Action.NAME, REDO);
            }
        }
    }    

}
