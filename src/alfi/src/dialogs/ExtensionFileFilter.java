package edu.caltech.ligo.alfi.dialogs;

import java.io.File;
import java.util.ArrayList;
import javax.swing.filechooser.FileFilter;

/**    
  * <pre>
  * The file filter which matches all files with a given set of 
  * extension
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class ExtensionFileFilter extends FileFilter {
	
    private String m_description = "";
    private ArrayList m_extensions = new ArrayList();

    /**
      * Adds an extension that this file filter recognizes.
      *
      * @param _extension a file extension (such as ".box")
      */
	public void addExtension (String _extension) {
        if (!_extension.startsWith("."))
            _extension = "." + _extension;
        m_extensions.add(_extension.toLowerCase());
	}

    /**
      * Sets a description for the file set that this file filter
      * recognizes.
      * 
      * @param _description a description for the file set
      */
	public void setDescription (String _description) {
        m_description = _description;
    }

    /**
      * Returns a description for the file set that this file filter
      * recognizes.
      * 
      * @return a description for the file set
      */
	public String getDescription () {
        return m_description;
    }

    /**
      * Defines the abstract method.
      * 
      * @return whether the given file is accepted by this filter
      */
	public boolean accept (File _file) {

        if (_file.isDirectory())
            return true;

        String name = _file.getName().toLowerCase();
        // 
        // check if the file ends with any of the extensions 
        // 
        for (int ii = 0; ii < m_extensions.size(); ii++) {
            if (name.endsWith((String) m_extensions.get(ii)))
                return true;
        }
        return false;
    }
    
}
