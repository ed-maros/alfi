
package edu.caltech.ligo.alfi.dialogs;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.ListModel;
import javax.swing.AbstractListModel;
import javax.swing.table.*;
import javax.swing.event.TableModelListener;
import javax.swing.event.TableModelEvent;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.file.Parser;
import edu.caltech.ligo.alfi.common.AlfiConstants;
import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.bookkeeper.ParameterValueModification;
import edu.caltech.ligo.alfi.bookkeeper.ParameterDeclaration;
import edu.caltech.ligo.alfi.bookkeeper.PortProxy;

/**
  * <pre>
  * The table model that is displayed in the ParameterTable.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */

public class ParameterTableModel extends DefaultTableModel
                                 implements TableModelListener {

    public static final String DEFAULT_VALUE = "DEFAULT";
    public static final String PARAMETER_TYPE = "parameter";
    public static final String INPUT_PORT_TYPE = "input port";
    public static final String FILE_TYPE = " ";
    public static final String LINE_DELIMETERS = "\n\r\f"; 
                                                //newline, CR, formfeed
    public static final String SET_IN = "set in ";

    public static final int LOCAL_VALUE_INDEX = 0;
    public static final int TYPE_INDEX = 1;
    public static final int DATA_TYPE_INDEX = 2;
    public static final int LOCAL_COMMENT_INDEX = 3;
    public static final int DEFAULT_VALUE_INDEX = 4;
    public static final int DEFAULT_COMMENT_INDEX = 5;

    private HashMap m_parameter_map = new HashMap();
    private HashMap m_original_param_values_map = new HashMap();
    private ArrayList m_modified_values_list;
    private ArrayList m_header_name_list;
    private ListModel m_header_name_list_model;

    private ALFINode m_node;
    
    public ParameterTableModel (ALFINode _node, int _row_count, 
                                String[] _column_names) {
        super(_column_names, _row_count);

        m_node = _node;
        m_modified_values_list = new ArrayList(_row_count);
        m_header_name_list = new ArrayList(_row_count);

        setTableValues();

        m_header_name_list_model = new AbstractListModel() {
            public int getSize () { return m_header_name_list.size();}
            public Object getElementAt (int _index) {
                return m_header_name_list.get(_index);
            }
        };
    }
      
    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
        if (col == LOCAL_VALUE_INDEX) { 
            return true;
        } else {
            return false;
        }
    }

   /**
     * When setting the value in the LOCAL_VALUE_INDEX, create
     * a 'Modified' entry in the 
     */
    public void setValueAt(Object _value, int _row, int _col) {
        super.setValueAt(_value, _row, _col);  
        String value = (String) _value;

        if (_col == LOCAL_VALUE_INDEX) {
            boolean has_default_value = true;

            if (m_modified_values_list.size() <= _row) {
                String data_type = (String) getValueAt(_row, TYPE_INDEX);

                if (data_type.equals(PARAMETER_TYPE)) {
                    m_modified_values_list.add( 
                              new ValueAttributes(has_default_value));

                } else if (data_type.equals(INPUT_PORT_TYPE)) {
                    m_modified_values_list.add( 
                        new ValueAttributes(false, has_default_value, 
                            ValueAttributes.INPUT_PORT_TYPE));

                } else if (data_type.equals(FILE_TYPE)) {
                    m_modified_values_list.add( 
                        new ValueAttributes(false, true, 
                            ValueAttributes.FILE_TYPE));
                }
            }
            else {
                boolean is_value_modified = false;
                if (!value.equals(DEFAULT_VALUE)) {
                    if (value.length() > 0) {
                        is_value_modified = true;
                    }
                }

                ValueAttributes flags = 
                    (ValueAttributes) m_modified_values_list.get(_row);

                flags.setValueModified(is_value_modified);
                fireTableCellUpdated(_row, _col);
            }
        } else if (_col == DEFAULT_VALUE_INDEX) {
            String data_type = (String) getValueAt(_row, TYPE_INDEX);
            boolean has_default_value = true;

            if (data_type.equals(PARAMETER_TYPE)) {
                String default_value = (String) getValueAt(_row,
                    DEFAULT_VALUE_INDEX);
                has_default_value = (default_value.length() > 0);
                ValueAttributes flags = 
                    (ValueAttributes) m_modified_values_list.get(_row);

                flags.setDefaultValue(has_default_value);
                fireTableCellUpdated(_row, _col);
            }
        }
    }

    /**
      * Static method to determine if the text is multilined
      */
    public static boolean isMultilineText (String _text) {
        StringTokenizer tokenizer = new StringTokenizer(_text, 
                ParameterTableModel.LINE_DELIMETERS);
        return (tokenizer.countTokens() > 1); 
    }

    /**
      * Returns the header list model
      */
    public ListModel getHeaderNameListModel () {
        return m_header_name_list_model; 
    }

    /**
      * Sets the values in the table
      */
    public void setTableValues () {
        ALFINode base_node;
        if (m_node.isRootNode()) {
            base_node = m_node;
        } else {
            base_node = m_node.baseNode_getRoot();
        }

        setDefaultValues(base_node);
        setModifiedValues();
    }

    /**
      * Sets the default value 'DEFAULT' in the row specified.
      */
    public void setDefaultValue (int _row, SimpleCellRenderer _text) {
        setValueAt(DEFAULT_VALUE, _row, LOCAL_VALUE_INDEX);
        fireTableCellUpdated(_row, LOCAL_VALUE_INDEX);

        _text.setCellValue(DEFAULT_VALUE);
    }

    /**
      * Sets the default values of the table
      */
    private void setDefaultValues (ALFINode _base_node) {
        ParameterDeclaration declaration[] = 
            _base_node.parameterDeclarations_getLocal();

        int ii = 0;
        for ( ; ii < declaration.length; ii++) {
            String name = declaration[ii].getName();
            //String value = declaration[ii].getValue();

            // Save the row position of the parameter in the map
            m_parameter_map.put(name, new Integer(ii));
   
            // Save the original value of the parameter in the map
            m_original_param_values_map.put(name, DEFAULT_VALUE);

            // Create a list of header names so it can be used to create
            // the ListModel.
            m_header_name_list.add(ii, name);

            // Set the data type first
            // Initially set everything to "DEFAULT"
            setValueAt(PARAMETER_TYPE, ii, TYPE_INDEX);  
            setValueAt(DEFAULT_VALUE, ii, LOCAL_VALUE_INDEX);  
            setValueAt(declaration[ii].getType(), ii, DATA_TYPE_INDEX);  
            setValueAt(AlfiConstants.EMPTY, ii, LOCAL_COMMENT_INDEX);  
            setValueAt(AlfiConstants.EMPTY, ii, DEFAULT_VALUE_INDEX);  
        }

        // #include line
        m_parameter_map.put(Parser.FILE_INCLUDE_MARKER, new Integer(ii));
        m_original_param_values_map.put(Parser.FILE_INCLUDE_MARKER,
                                                                DEFAULT_VALUE);
        m_header_name_list.add(ii, Parser.FILE_INCLUDE_MARKER);
        setValueAt(FILE_TYPE, ii, TYPE_INDEX);
        setValueAt(DEFAULT_VALUE, ii, LOCAL_VALUE_INDEX);
        setValueAt(FILE_TYPE, ii, DATA_TYPE_INDEX);
        setValueAt(AlfiConstants.EMPTY, ii, LOCAL_COMMENT_INDEX);
        setValueAt(AlfiConstants.EMPTY, ii, DEFAULT_VALUE_INDEX);
        setValueAt(AlfiConstants.EMPTY, ii, DEFAULT_COMMENT_INDEX);

        setBaseNodesValueMods(m_node.baseNode_getDirect());
        setBaseNodesParameterValues(m_node.baseNode_getRoot());
    }

    /**
      * Iterates through the base nodes and sets the value in
      * DEFAULT_VALUE_INDEX
      */
    private void setBaseNodesValueMods (ALFINode _base_node) {
        
        ParameterValueModification mods[] = 
            _base_node.parameterValueMods_getLocal();

        for (int ii = 0; ii < mods.length; ii++) {
            String name = mods[ii].getName();
            if (isInputPortSetting(name)) { continue; }

            String value = mods[ii].getValue();
            String node_name = _base_node.generateFullNodePathName();
            Integer table_index = (Integer) m_parameter_map.get(name);
            if (table_index != null) {
                int index = table_index.intValue();
                String cell_value =
                            (String) getValueAt(index, DEFAULT_VALUE_INDEX);
                if (cell_value.equals(AlfiConstants.EMPTY)) {
                    setValueAt(value, index, DEFAULT_VALUE_INDEX);
                    StringBuffer notes = new StringBuffer(SET_IN);
                    notes.append(node_name);
                    setValueAt(notes.toString(), index, DEFAULT_COMMENT_INDEX);
                }
            }
            else {
                String warning = "In node " + m_node.generateFullNodePathName()+
                    ", a modification for a setting named \"" + name + "\" " +
                    "was found.  No such setting exists in this node.  " +
                    "Setting ignored.";
                Alfi.warn(warning, false);
            }
        }
        
        ALFINode node = _base_node.baseNode_getDirect();
        if (node != null) {
            setBaseNodesValueMods(node);
        }
    }

    private void setBaseNodesParameterValues (ALFINode _root_node) {
        
        ParameterDeclaration  declaration[] = 
            _root_node.parameterDeclarations_getLocal();

        for (int ii = 0; ii < declaration.length; ii++) {
            String name = declaration[ii].getName();
            String value = declaration[ii].getValue();
            String node_name = _root_node.generateFullNodePathName();
            Integer table_index = (Integer) m_parameter_map.get(name);
            int index = table_index.intValue();
            String cell_value = (String) getValueAt(index, DEFAULT_VALUE_INDEX);
            if (cell_value.equals(AlfiConstants.EMPTY)) {
                setValueAt(value, index, DEFAULT_VALUE_INDEX);
                StringBuffer notes = new StringBuffer(SET_IN);
                notes.append(node_name);
                setValueAt(notes.toString(), index, DEFAULT_COMMENT_INDEX);
            }
        }
    }

    private boolean isInputPortSetting(String _setting_name) {
        PortProxy[] ports = m_node.ports_get();
        for (int i = 0; i < ports.length; i++) {
            if (        (ports[i].getIOType() == PortProxy.IO_TYPE_INPUT) &&
                        (ports[i].getName().equals(_setting_name)) ) {
                return true;
            }
        }
        return false;
    }

    public String getOriginalParameterValue (String _name) {
        return (String) m_original_param_values_map.get(_name);
    }

    private void setModifiedValues () {
        ParameterValueModification mods[] = 
            m_node.parameterValueMods_getLocal();

        for (int ii = 0; ii < mods.length; ii++) {
            String param_name = mods[ii].getName();
            String value = new String(mods[ii].getValue());
            Integer table_index = (Integer) m_parameter_map.get(param_name);

            if (table_index == null) {
                //System.err.println("cannot find parameter " + param_name);
            } 
            else {
                // Store the original values of the parameter value mods
                // and create a new entry in the map if the key does not
                // exist
                m_original_param_values_map.put(param_name, value);
                int index = table_index.intValue();
                this.setValueAt(value, index, LOCAL_VALUE_INDEX);
            }
        }
    }




    public ValueModification[] getDataChanges() {

        ArrayList value_list = new ArrayList();

        int num_rows = getRowCount();
        for (int ii = 0; ii < num_rows; ii++) {
            String name  = (String) m_header_name_list_model.
                getElementAt(ii); 
            String value = (String) getValueAt(ii, LOCAL_VALUE_INDEX);
            String comment = (String) getValueAt(ii, LOCAL_COMMENT_INDEX);
            if (!value.equals(getOriginalParameterValue(name))) {
                ValueModification changed_value = 
                    new ValueModification(name, value, comment);
                value_list.add(changed_value);
            }            
        }

        ValueModification[] value_mods_array = 
            new ValueModification[value_list.size()];
        value_list.toArray(value_mods_array);
        
        return value_mods_array;
    } 

    public boolean hasDefaultValue (int _row) {
        ValueAttributes flags = 
            (ValueAttributes) m_modified_values_list.get(_row);
        return flags.hasDefaultValue();
    }

    public boolean isDefaultValue (int row, int column) {
        if (column == DEFAULT_VALUE_INDEX ||
            column == DEFAULT_COMMENT_INDEX) {
            return true;
        } else {
            ValueAttributes flags = 
                (ValueAttributes) m_modified_values_list.get(row);
            return (!flags.isValueModified());
        }
    }

    public boolean isParameterType (int _row) {
        ValueAttributes flags = 
            (ValueAttributes) m_modified_values_list.get(_row);
        return flags.isParameterType();
    }

    public boolean isInputPortType (int _row) {
        ValueAttributes flags = 
            (ValueAttributes) m_modified_values_list.get(_row);
        return flags.isInputPortType();
    }
        

    public boolean isFileType (int _row) {
        ValueAttributes flags = 
            (ValueAttributes) m_modified_values_list.get(_row);
        return flags.isFileType();
    }

    // By default forward all events to all the listeners. 
    public void tableChanged(TableModelEvent e) {
        fireTableChanged(e);
    }
    
    class ValueAttributes extends Object {

        public static final int PARAMETER_TYPE = 0;
        public static final int INPUT_PORT_TYPE = 1;
        public static final int FILE_TYPE = 2;

        private boolean m_value_modified;
        private boolean m_has_default_value;
        private int m_parameter_type;
        private String  m_node_containing_default_value;
    
        public ValueAttributes (boolean _has_default_value) {
            this(false, _has_default_value, PARAMETER_TYPE);
        }

        public ValueAttributes (boolean _value_modified, 
                                boolean _has_default_value,
                                int _parameter_type){
            m_value_modified = _value_modified;
            m_has_default_value = _has_default_value;
            m_parameter_type = _parameter_type;
        }

        public void setValueModified (boolean _modified) {
            m_value_modified = _modified;
        }
        
        public boolean isValueModified () {
            return m_value_modified;
        }

        public boolean hasDefaultValue () {
            return m_has_default_value;
        }

        public void setDefaultValue (boolean _has_default) {
            m_has_default_value = _has_default;
        }
        
        public void setParameterType (int _type) {
            m_parameter_type = _type;
        }

        public boolean isParameterType () {
            return (m_parameter_type == PARAMETER_TYPE);
        }

        public boolean isInputPortType () {
            return (m_parameter_type == INPUT_PORT_TYPE);
        }
        public boolean isFileType () {
            return (m_parameter_type == FILE_TYPE);
        }
        
    } 
}
