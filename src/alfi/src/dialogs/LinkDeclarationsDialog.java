package edu.caltech.ligo.alfi.dialogs;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;

public class LinkDeclarationsDialog extends JDialog {

    private final static Dimension WINDOW_SIZE = new Dimension(750, 600);

    private ALFINode m_node;
    private JScrollPane m_main_container;

        /** Key = name, value = Object[3] = { value, target_desc[], LD } */
    private HashMap m_link_declarations_map;

    private HashMap m_target_description_list_map;
    private HashMap m_value_field_map;

    public LinkDeclarationsDialog(Frame _parent, ALFINode _root_box_node) {
        super(_parent, "Links: " + _root_box_node, true);
        assert(_root_box_node.isBox());
        assert(_root_box_node.isRootNode());

        setVisible(false);
        setBounds(determineDialogBounds(_parent));
        setResizable(true);

        m_node = _root_box_node;

        m_link_declarations_map = new HashMap();
        m_target_description_list_map = new HashMap();
        m_value_field_map = new HashMap();

        LinkDeclaration[] link_decs = m_node.linkDeclarations_getLocal();
        for (int i = 0; i < link_decs.length; i++) {
            String name = link_decs[i].getName();
            Object[] info = { link_decs[i].getValue(),
                              link_decs[i].getLinkTargetDescriptions() };
            m_link_declarations_map.put(name, info);
        }
        m_main_container = new JScrollPane(
                           ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                           ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        updateMainView();

        JPanel button_panel = createButtonPanel();

        Container content_pane = getContentPane();
        content_pane.setLayout(new BorderLayout(10, 10));
        content_pane.add("Center", m_main_container);
        content_pane.add("South", button_panel);
    }

    private void updateMainView() {
        JViewport view_port = m_main_container.getViewport();
        Component view = view_port.getView();
        if (view != null) { view_port.remove(view); }

        JPanel links_panel = new JPanel();
        links_panel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 2, 0, 2);

        c.gridy = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        if (m_link_declarations_map.size() > 0) {
            c.gridx = 1;
            links_panel.add(new JLabel("Link Name", JLabel.LEFT), c);
            c.gridx = 2;
            links_panel.add(new JLabel("Value", JLabel.LEFT), c);
            c.gridx = 3;
            links_panel.add(new JLabel("Linked Targets", JLabel.LEFT), c);
            c.gridx = 4;
            links_panel.add(new JLabel("Edit Links", JLabel.LEFT), c);
        }

        for (Iterator i = m_link_declarations_map.keySet().iterator();
                                                               i.hasNext(); ) {
            String name = (String) i.next();
            Object[] info = (Object[]) m_link_declarations_map.get(name);
            c.anchor = GridBagConstraints.CENTER;
            c.gridx = 0;
            c.gridy++;

            String value = (String) info[0];
            String[] target_descriptions = (String[]) info[1];
            addRow(links_panel, name, value, target_descriptions, c);
        }

        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = GridBagConstraints.REMAINDER;
        JButton create_button = new JButton("Create New Link");
        create_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _e) {
                    createNewLinkListing();
                }
            });
        links_panel.add(create_button, c);

        view_port.setView(links_panel);
    }

    private void addRow(JPanel _links_panel, final String _name, String _value,
                        String[] _target_descriptions, GridBagConstraints _c) {
        _c.gridheight = 1;
        JButton delete_button = new JButton("Delete Link");
        delete_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    deleteLink(_name);
                }
            });
        _links_panel.add(delete_button, _c);

        _c.gridy++;
        JButton rename_button = new JButton("Rename Link");
        rename_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    renameLink(_name);
                }
            });
        _links_panel.add(rename_button, _c);

        _c.gridx++;
        _c.gridy--;
        _c.gridheight = 2;
        _links_panel.add(new JLabel(_name, JLabel.LEFT), _c);

        _c.gridx++;
        if (_value == null) { _value = ""; }
        JTextField value_field = new JTextField(_value, 15);
        m_value_field_map.put(_name, value_field);
        _links_panel.add(value_field, _c);

        _c.gridx++;
        _c.fill = GridBagConstraints.HORIZONTAL;
        _c.weightx = 10.0;
        JList list = new JList(_target_descriptions);
        list.setBackground(Color.white);
        list.setVisibleRowCount(3);
        m_target_description_list_map.put(_name, list);
        JScrollPane list_scroll_pane = new JScrollPane(list,
                           ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                           ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        _links_panel.add(list_scroll_pane, _c);
        _c.fill = GridBagConstraints.NONE;
        _c.weightx = 0.0;

        _c.gridx++;
        _c.gridheight = 1;
        _c.anchor = GridBagConstraints.SOUTH;
        JButton delete_target_desc_button = new JButton("Delete Selected");
        delete_target_desc_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    deleteSelectedTargets(_name);
                }
            });
        _links_panel.add(delete_target_desc_button, _c);

        _c.gridy++;
        _c.anchor = GridBagConstraints.NORTH;
        JButton add_target_button = new JButton("Add Target");
        add_target_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    addLinkTargets(_name);
                }
            });
        _links_panel.add(add_target_button, _c);
    }

        /** Updates the contents of m_link_declarations_map to what is showing.
          */
    private void syncInfoMapContents() {
        for (Iterator i = m_link_declarations_map.keySet().iterator();
                                                               i.hasNext(); ) {
            String name = (String) i.next();
            String value = ((JTextField) m_value_field_map.get(name)).getText();

            JList list = (JList) m_target_description_list_map.get(name);
            ListModel model = list.getModel();
            ArrayList data_list = new ArrayList();
            for (int j = 0; j < model.getSize(); j++) {
                data_list.add(model.getElementAt(j));
            }
            String[] descriptions = new String[data_list.size()];
            data_list.toArray(descriptions);

            Object[] info = { value, descriptions };
            m_link_declarations_map.put(name, info);
        }
    }

    private void addLinkTargets(String _name) {
        JList list = (JList) m_target_description_list_map.get(_name);

        ListModel model = list.getModel();
        int size = model.getSize();
        Vector data_list = new Vector(size);
        for (int i = 0; i < size; i++) { data_list.add(model.getElementAt(i)); }

        int existing_data_type = determineExistingDataType(data_list);
        LinkTargetSelectionDialog link_target_selection_dialog =
               new LinkTargetSelectionDialog(this, m_node, existing_data_type);
        link_target_selection_dialog.setVisible(true);
        String[][] selections_info =
                                  link_target_selection_dialog.getSelections();
        boolean b_list_changed = false;
        for (int i = 0; i < selections_info.length; i++) {
            String desc = selections_info[i][0] + " " + selections_info[i][1];

            boolean b_desc_exists = false;
            for (Iterator j = data_list.iterator(); j.hasNext(); ) {
                String existing_desc = j.next().toString();
                if (existing_desc.equals(desc)) {
                    b_desc_exists = true;
                    break;
                }
            }

            if (! b_desc_exists) {
                data_list.add(desc);
                b_list_changed = true;
            }
        }

        if (b_list_changed) { list.setListData(data_list); }
    }

        /** Determines the data types of targets listed in _data_list and
          * returns the first non GenericPortProxy.DATA_TYPE_1_UNKNOWN type
          * found.  Otherwise returns GenericPortProxy.DATA_TYPE_1_UNKNOWN.
          */
    private int determineExistingDataType(Vector _data_list) {
        for (Iterator i = _data_list.iterator(); i.hasNext(); ) {
            String[] parts = ((String) i.next()).split("\\s");
            String node_name = m_node.determineLocalName() + "." + parts[0];
            ALFINode[] members = m_node.memberNodes_getAllGenerations();
            for (int j = 0; j < members.length; j++) {
                if (members[j].isPrimitive()) {
                    if (node_name.equals(
                                      members[j].generateFullNodePathName())) {
                        ALFINode base_node = members[j].baseNode_getRoot();
                        ParameterDeclaration param_dec = base_node.
                                       parameterDeclaration_getLocal(parts[1]);
                        if (param_dec != null) {
                            String data_type_string = param_dec.getType();
                            int type = GenericPortProxy.
                                     getDataTypeIdFromString(data_type_string);
                            if (type != GenericPortProxy.DATA_TYPE_1_UNKNOWN) {
                                return type;
                            }
                        }

                        break;
                    }
                }
            }
        }

        return GenericPortProxy.DATA_TYPE_1_UNKNOWN;
    }

    private void deleteSelectedTargets(String _link_name) {
        JList list = (JList) m_target_description_list_map.get(_link_name);

        ListModel model = list.getModel();
        int size = model.getSize();
        Vector data_list = new Vector(size);
        for (int i = 0; i < size; i++) { data_list.add(model.getElementAt(i)); }

        Object[] selections = list.getSelectedValues();
        for (int i = 0; i < selections.length; i++) {
            data_list.remove(selections[i]);
        }

        list.setListData(data_list);
    }

    private JPanel createButtonPanel() {
        JPanel button_panel = new JPanel();
        button_panel.setLayout(new BoxLayout(button_panel,
                                   BoxLayout.X_AXIS));
        button_panel.setBorder(BorderFactory.createEtchedBorder());
        button_panel.add(Box.createHorizontalGlue());

        JButton ok_button = new JButton("OK");
        ok_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    submit();
                }
            });
        button_panel.add(ok_button);
        getRootPane().setDefaultButton(ok_button);

        JButton cancel_button = new JButton("Cancel");
        cancel_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                }
            });
        button_panel.add(Box.createHorizontalStrut(10));
        button_panel.add(cancel_button);
        button_panel.add(Box.createHorizontalGlue());
        
        return button_panel;
    }

    private void submit() {
        syncInfoMapContents();

            // update, leave, or remove existing declarations
        LinkDeclaration[] current_decs = m_node.linkDeclarations_getLocal();
        for (int i = 0; i < current_decs.length; i++) {
            LinkDeclaration dec = current_decs[i];
            String name = dec.getName();
            if (m_link_declarations_map.containsKey(name)) {
                Object[] info = (Object[]) m_link_declarations_map.get(name);
                String value = (String) info[0];
                String[] target_descriptions = (String[]) info[1];
                if (linkHasBeenModified(dec, value, target_descriptions)) {
                    m_node.linkDeclaration_removeLocal(name);

                    if (target_descriptions.length > 0) {
                        HashMap link_map = buildLinkMap(target_descriptions);
                        m_node.linkDeclaration_add(name, value, link_map, "");
                    }
                }
                m_link_declarations_map.remove(name);
            }
            else { m_node.linkDeclaration_removeLocal(name); }
        }

            // the remaining entries are new declarations to add
        for (Iterator i = m_link_declarations_map.keySet().iterator();
                                                               i.hasNext(); ) {
            String name = (String) i.next();
            Object[] info = (Object[]) m_link_declarations_map.get(name);
            String value = (String) info[0];
            String[] target_descriptions = (String[]) info[1];
            if (target_descriptions.length > 0) {
                HashMap link_map = buildLinkMap(target_descriptions);
                m_node.linkDeclaration_add(name, value, link_map, "");
            }
        }
    }

    private Rectangle determineDialogBounds(Frame _parent) {
        Rectangle rect = _parent.getBounds();
        Point p = new Point(rect.x + (rect.width/2), rect.y + (rect.height/2));
        rect = new Rectangle(p, WINDOW_SIZE);

        Dimension screen_size = _parent.getToolkit().getScreenSize();
        while (rect.x < Alfi.WINDOW_MANAGER_OFFSET.x) {
            rect.translate(10, 0);
        }

        while (rect.y < Alfi.WINDOW_MANAGER_OFFSET.y) {
            rect.translate(0, 10);
        }

        while ((rect.x + rect.width) > (screen_size.width +
                                        Alfi.WINDOW_MANAGER_OFFSET.x)) {
            rect.translate(-10, 0);
        }

        while ((rect.y + rect.height) > (screen_size.height +
                                         Alfi.WINDOW_MANAGER_OFFSET.y)) {
            rect.translate(0, -10);
        }

        return rect;
    }

    private void createNewLinkListing() {
        syncInfoMapContents();

        String title = "New Link: Name";
        String message = "Enter name of new link.";
        String name = JOptionPane.showInputDialog(this, message, title,
                                                 JOptionPane.QUESTION_MESSAGE);

            // jump through hoops
        if ((name != null) && (! m_link_declarations_map.containsKey(name)) &&
                                           LinkDeclaration.isValidName(name)) {
            Object[] info = { "", new String[0] };
            m_link_declarations_map.put(name, info);

            updateMainView();

                // scroll to bottom
            JViewport port = m_main_container.getViewport();
            Dimension dim = port.getExtentSize();
            port.setViewPosition(new Point(0, dim.height));
        }
    }

    private void deleteLink(String _link_name) {
        syncInfoMapContents();

        String title = "Confirm Delete Link: " + _link_name;
        String message =
            "Warning: The deletion of Link Declarations in this\n" +
            "    base box will break any boxes which\n\n" +
            "        1) are currently not loaded, and\n" +
            "        2) contain box settings associated with this link.\n\n" +
            "    Currently loaded boxes which have settings associated with\n" +
            "    this link will have such settings automatically removed.\n\n" +
            "Do you wish to remove the declaration for this link?";
        int response = JOptionPane.showConfirmDialog(this, message, title,
                      JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (response == JOptionPane.YES_OPTION) {
            m_link_declarations_map.remove(_link_name);
            m_target_description_list_map.remove(_link_name);
            m_value_field_map.remove(_link_name);
            updateMainView();
        }
    }

    private void renameLink(String _link_name) {
        syncInfoMapContents();

        String title = "Rename Link";
        String message =
            "Warning: The renaming of a Link in this base box will break\n" +
            "    any boxes which\n\n" +
            "        1) are currently not loaded, and\n" +
            "        2) contain box settings associated with this link.\n\n" +
            "    Currently loaded boxes which have settings associated with\n" +
            "    this link will have such settings automatically updated.\n\n" +
            "Do you wish to rename this link?";
        int response = JOptionPane.showConfirmDialog(this, message, title,
                      JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (response == JOptionPane.YES_OPTION) {
            title = "New Link Name";
            message = "Input new Link name:";
            String new_name = JOptionPane.showInputDialog(this, message, title,
                      JOptionPane.QUESTION_MESSAGE);
            if (new_name != null) {
                new_name = new_name.replaceAll("\\s", "_");

                LinkDeclaration dec =
                                   m_node.linkDeclaration_getLocal(_link_name);
                if (dec != null) { dec.rename(new_name); }

                m_link_declarations_map.put(new_name,
                                      m_link_declarations_map.get(_link_name));
                
                m_link_declarations_map.remove(_link_name);
                m_target_description_list_map.remove(_link_name);
                m_value_field_map.remove(_link_name);

                updateMainView();
            }
        }
    }

    private boolean linkHasBeenModified(LinkDeclaration _link,
                   String _test_value, String[] _test_target_descriptions) {
        if (! _link.getValue().equals(_test_value)) {
            return true;
        }

        String[] descriptions = _link.getLinkTargetDescriptions();
        if (descriptions.length != _test_target_descriptions.length) {
            return true;
        }

        for (int i = 0; i < descriptions.length; i++) {
            boolean b_found_match = false;
            for (int j = 0; j < _test_target_descriptions.length; j++) {
                if (_test_target_descriptions[j].equals(descriptions[i])) {
                    b_found_match = true;
                    break;
                }
            }
            if (! b_found_match) { return true; }
        }

        return false;
    }

        /** This method creates a map of parameter names to primitives in the
          * format required by the LinkDeclaration constructor.
          */
    private HashMap buildLinkMap(String[] _target_descriptions) {
        ALFINode[] members = m_node.memberNodes_getAllGenerations();
        HashMap target_names_vs_node_map = new HashMap();
        for (int i = 0; i < _target_descriptions.length; i++) {
                // parts[0] is relative node name, [1] is target name
            String[] parts = _target_descriptions[i].split("\\s+");
            String node_path = m_node.determineLocalName() + "." + parts[0];
            String target_name = parts[1];
            for (int j = 0; j < members.length; j++) {
                if (members[j].generateFullNodePathName().equals(node_path)) {
                    ArrayList target_name_list =
                          (ArrayList) target_names_vs_node_map.get(members[j]);
                    if (target_name_list == null) {
                        target_name_list = new ArrayList();
                        target_names_vs_node_map.put(members[j],
                                                     target_name_list);
                    }
                    target_name_list.add(target_name);
                    break;
                }
            }
        }

        HashMap link_map = new HashMap(target_names_vs_node_map.size());
        for (Iterator i = target_names_vs_node_map.keySet().iterator();
                                                               i.hasNext(); ) {
            ALFINode member = (ALFINode) i.next();
            ArrayList target_name_list =
                            (ArrayList) target_names_vs_node_map.get(member);
            String[] target_names = new String[target_name_list.size()];
            target_name_list.toArray(target_names);
            link_map.put(member, target_names);
        }

        return link_map;
    }
}
