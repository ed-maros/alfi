package edu.caltech.ligo.alfi.dialogs;

import java.util.*;

import edu.caltech.ligo.alfi.bookkeeper.*;

/**
  * <pre>
  * The ListModel to manage a list of PortProxies
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */

public class PortProxyListModel  extends DefaultListModel
                                 implements ListModelListener {

    private ArrayList m_original_port_proxies = new ArrayList();

    public PortProxyListModel (PortProxy[] _port_proxies) {

        for (int i = 0; i < _port_proxies.size(); i++) {
            m_original_port_proxies.add(_port_proxies[i]); 
        }
    }
}
