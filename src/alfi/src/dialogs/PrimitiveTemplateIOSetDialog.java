package edu.caltech.ligo.alfi.dialogs;

import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.editor.*;

public class PrimitiveTemplateIOSetDialog extends JDialog {
    public final static int OK     = 0;
    public final static int CANCEL = 1;

    private int m_choice;
    private int[] m_io_set_counts;
    private JTable m_table;

    public PrimitiveTemplateIOSetDialog(JFrame _parent, PrimitiveTemplate _pt) {
        super(_parent, "Primitive Template I/O Set Count Dialog", true);

        m_choice = CANCEL;
        m_io_set_counts = null;

        PortProxy[][] input_sets = _pt.getInputSetPorts();
        PortProxy[][] output_sets = _pt.getOutputSetPorts();
            // be careful not to confuse the total number of i/o sets with the
            // count that the user will assign to each set of ports
        int io_set_length = input_sets.length + output_sets.length;

            // init completed after table init
        m_io_set_counts = new int[io_set_length];

            // general layout manager
        getContentPane().setLayout(
                            new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

            // set up the table
        JScrollPane table_scroller;
        {
            int row_count = input_sets.length + output_sets.length;
            String[] column_names =
                     { "Ports in this Set", "I/O State", "Set Count Desired" };
            Object[][] table_data = new Object[row_count][];

                // initialize table data
            {
                final String INPUT_SET = "input set";
                final String OUTPUT_SET = "output set";

                for (int i = 0; i < input_sets.length; i++) {
                    PortProxy[] set_ports = input_sets[i];
                    table_data[i] = generateIOSetTableData(set_ports,INPUT_SET);
                }

                for (int i = 0; i < output_sets.length; i++) {
                    PortProxy[] set_ports = output_sets[i];
                    table_data[input_sets.length + i] =
                                 generateIOSetTableData(set_ports, OUTPUT_SET);
                }
            }

            m_table = new JTable(table_data, column_names);
            m_table.setRowHeight(m_table.getRowHeight() + 5);
            m_table.setRowSelectionAllowed(false);

            table_scroller = new JScrollPane(m_table);
            getContentPane().add(table_scroller);
        }

            // complete init of m_io_set_counts to agree with table data
        updateIOSetCounts();

            // ok and cancel buttons
        JPanel button_panel;
        {
            button_panel = new JPanel();
            button_panel.setLayout(new FlowLayout (FlowLayout.CENTER, 5, 5));

            JButton ok_button;
            ok_button = new JButton();
            ok_button.setText("OK");
            ok_button.setMnemonic('O');
            ok_button.addActionListener(new ActionListener() {
                public void actionPerformed (ActionEvent _event) {
                    okButtonActionPerformed(_event);
                }
            });
            button_panel.add(ok_button);
            getRootPane().setDefaultButton(ok_button);

            JButton cancel_button;
            cancel_button = new JButton();
            cancel_button.setText("Cancel");
            cancel_button.setMnemonic('C');
            cancel_button.addActionListener(new ActionListener() {
                public void actionPerformed (ActionEvent _event) {
                    cancelButtonActionPerformed(_event);
                }
            });
            button_panel.add(cancel_button);

            getContentPane().add(button_panel);
        }

            // pack is used only to update component sizes.  dialog "manually"
            // resized after due to pack making the table scroll pane so big (!)
        pack();

        int width = Math.max(m_table.getSize().width,
                                             button_panel.getSize().width) + 5;
        int height = m_table.getSize().height +
                                            button_panel.getSize().height + 60;
        setSize(width, height);
        EditorFrame.centerDialog(this);
        setVisible(true);
    }

    private void updateIOSetCounts() {
        if (m_table.isEditing()) {
            m_table.getCellEditor().stopCellEditing();
        }

        for (int i = 0; i < m_io_set_counts.length; i++) {
            Object cell_value = m_table.getValueAt(i, 2);
            if (cell_value instanceof Integer) {
                m_io_set_counts[i] = ((Integer) cell_value).intValue();
            }
            else if (cell_value instanceof String) {
                try {
                    m_io_set_counts[i] = Integer.parseInt((String) cell_value);
                }
                catch(NumberFormatException _e) { m_io_set_counts[i] = 0; }
            }
            else { m_io_set_counts[i] = 0; }
        }
    }

    private Object[] generateIOSetTableData(PortProxy[] _set_ports,
                                                      String _io_description) {
        String port_names = new String();
        for (int i = 0; i < _set_ports.length; i++) {
            port_names += _set_ports[i].toString();
            if (i < (_set_ports.length - 1)) { port_names += ", "; }
        }

        return new Object[] { port_names, _io_description, new Integer(0) };
    }

    public int getChoice() { return m_choice; }

    public int[] getIOSetCounts() { return m_io_set_counts; }

    private void okButtonActionPerformed(ActionEvent _event) {
        m_choice = OK;
        updateIOSetCounts();
        setVisible(false);
    }

    private void cancelButtonActionPerformed(ActionEvent _event) {
        m_choice = CANCEL;
        m_io_set_counts = null;
        setVisible(false);
    }
}
