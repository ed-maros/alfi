package edu.caltech.ligo.alfi.dialogs;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.summary.AlfiMainFrame;
import edu.caltech.ligo.alfi.file.ALFIFile;

public class UDPSelectionDialog extends JDialog implements MouseListener {
    private JList m_list;
    private DefaultListModel m_list_model;

    private static final String open_string = "Open";
    private static final String copy_string = "Open As...";

    public static final int CANCEL  = 0;
    public static final int OPEN    = 1;
    public static final int OPEN_AS = 2;

    private String m_selected_udp_name;
    private int m_return_state;
    
    public UDPSelectionDialog(Frame _parent_frame) {
        super(_parent_frame, "User Defined Primitives", true);
        
        initComponents();
        pack();

        // Center the dialog
        Dimension size = getSize();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - (int) size.getWidth())/2, 
                    (screenSize.height - (int) size.getHeight())/2);
        
        }

    protected void initComponents() {
    
        JPanel dialog_panel = new JPanel(new BorderLayout());
        m_list_model = new DefaultListModel();

        UserDefinedPrimitive[] udp_list = 
                              Alfi.getTheNodeCache().getUserDefinedPrimitives();
        for (int i = 0; i < udp_list.length; i++) {
           m_list_model.addElement(udp_list[i].determineLocalName());
        }
        // Sort the list
        //m_list_model = new SortedListModel(list_model);

        //Create the list and put it in a scroll pane.
        m_list = new JList(m_list_model);
        m_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_list.setSelectedIndex(0);
        m_list.addMouseListener(this);
        m_list.setVisibleRowCount(15);

        JScrollPane list_scroll_pane = new JScrollPane(m_list);

        // OPEN button
        JButton open_button = new JButton(open_string);
        OpenListener open_listener = new OpenListener();
        open_button.setActionCommand(open_string);
        open_button.addActionListener(open_listener);
        open_button.setEnabled(true);

        // COPY button
        JButton copy_button = new JButton(copy_string);
        copy_button.setActionCommand(copy_string);
        CopyListener copy_listener = new CopyListener();
        copy_button.addActionListener(copy_listener);

        JButton cancel_button;    
        cancel_button = new JButton();        
        cancel_button.setText("Cancel");
        cancel_button.setMnemonic('C');
        cancel_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                m_selected_udp_name = null;
                m_return_state = CANCEL;
                setVisible(false);
            }
        });   

        //Create a panel that uses BoxLayout.
        JPanel button_pane = new JPanel();
        button_pane.setLayout(new BoxLayout(button_pane,
                                           BoxLayout.LINE_AXIS));
        button_pane.add(open_button);
        button_pane.add(copy_button);
        button_pane.add(cancel_button);
        button_pane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        add(list_scroll_pane, BorderLayout.CENTER);
        add(button_pane, BorderLayout.PAGE_END);

    }


    public String getUDPNameSelection() {
        return m_selected_udp_name;
    }

    public int getReturnState() { return m_return_state; }

    class OpenListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            m_selected_udp_name = (String) m_list.getSelectedValue();
            if (m_selected_udp_name != null) {
                m_return_state = OPEN;
                setVisible(false);
            }
        }
    }

    class CopyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            m_selected_udp_name = (String) m_list.getSelectedValue();
            if (m_selected_udp_name != null) {
                 m_return_state = OPEN_AS;
                 setVisible(false);
            }
        }
    }


    //////////////////////////////////////////////////////////
    ////////////////// MouseListener methods /////////////////

    public void mouseClicked(MouseEvent _event) {

        if (_event.getClickCount() == 2) {
             m_selected_udp_name = (String) m_list.getSelectedValue();
            if (m_selected_udp_name != null) {
                 m_return_state = OPEN;
                 setVisible(false);
            }
        }
    }

    public void mouseEntered(MouseEvent _event) {}
    public void mouseExited(MouseEvent _event) {}
    public void mousePressed(MouseEvent _event) {}
    public void mouseReleased(MouseEvent _event) {}
}
