package edu.caltech.ligo.alfi.dialogs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.common.AlfiConstants;
import edu.caltech.ligo.alfi.editor.EditorFrame;
import edu.caltech.ligo.alfi.widgets.DummyNodeWidget;
import edu.caltech.ligo.alfi.bookkeeper.PortProxy;

public class PortPositionDialog extends AlfiDialog {

    public static String m_instructions = 
       "To Move Ports:\n" +
       "  1.  Select the port by clicking on it with the left mouse button.\n" +
       "  2.  To move the port : \n"+
       "          to the TOP edge, press the CONTROL + UP arrow key \n"+
       "          to the BOTTOM edge, press the CONTROL + UP arrow keys \n"+
       "          to the LEFT edge, press the CONTROL + LEFT arrow keys \n"+
       "          to the RIGHT edge, press the CONTROL + RIGHT arrow keys \n";
       public PortPositionDialog (UserDefinedNodeDialog _dialog, 
                               String _node_name, PortProxy[][] _ports) {
        super(_dialog, _node_name, true, 500, 500);

        initComponents(_node_name, _ports);
        pack();
        Dimension screen_size = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dialog_size = getSize();
        setBounds((screen_size.width - dialog_size.width)/2,
                  (screen_size.height - dialog_size.height)/2,
                   dialog_size.width, dialog_size.height);
    }

    private void initComponents (String _node_name, PortProxy[][] _ports) {
        
        Container container_pane = getContentPane();
        JTextArea instruction_label = new JTextArea(m_instructions);
        //container_pane.add(instruction_label, BorderLayout.NORTH);

        DummyNodeWidget node_widget = new DummyNodeWidget(_node_name, _ports);
        //Dimension node_size = node_widget.
        JGoView view = new JGoView();
        view.setSize(200,200);
        view.addObjectAtTail(node_widget);
        
        container_pane.add(view, BorderLayout.CENTER);

        createCommandButtons(container_pane);

        super.initComponents();
    }
    
    /** Create OK and Cancel buttons.*/

    private void createCommandButtons(Container _container) {
        JPanel button_panel;    
        button_panel = new JPanel();        
        button_panel.setLayout(new FlowLayout (FlowLayout.CENTER, 5, 5));
        button_panel.setBackground(Color.lightGray);

        JButton ok_button;    
        ok_button = new JButton();        
        ok_button.setText(OK);
        ok_button.setMnemonic('O');
        ok_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                okButtonActionPerformed(_event);
            }
        });   
        ok_button.addFocusListener(new FocusAdapter() {
            public void focusGained (FocusEvent _event) {
            }
        });
        button_panel.add(ok_button);

        JButton cancel_button;    
        cancel_button = new JButton();        
        cancel_button.setText(CANCEL);
        cancel_button.setMnemonic('C');
        cancel_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                cancelButtonActionPerformed(_event);
            }
        });   

        button_panel.add(cancel_button);

        _container.add(button_panel, BorderLayout.SOUTH);

    }


}
