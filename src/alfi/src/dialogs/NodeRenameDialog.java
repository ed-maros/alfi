package edu.caltech.ligo.alfi.dialogs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import edu.caltech.ligo.alfi.bookkeeper.ALFINode;
import edu.caltech.ligo.alfi.common.AlfiConstants;
import edu.caltech.ligo.alfi.editor.EditorFrame;

public class NodeRenameDialog extends JDialog implements ActionListener{

    private static final int WIDTH =  400;
    private static final int HEIGHT = 300;
    private static final int TEXT_WIDTH = 30;
    public static final String NODE_DISPLAY = new String("DISPLAY");
    public static final String NAME_DISPLAY = new String("Node Name");
    public static final String DESCRIPTION_DISPLAY = new String("Description");
    public static final String ICON_DISPLAY = new String("Icon");
    private static final String SPACE = new String(" ");
    private static final String UNDERSCORE = new String("_");
    private static final String TEXTFIELD = "JTextField";
   
    public static final int RET_CANCEL = 0;
    public static final int RET_OK = 1;
    public static final int RET_HELP = 2;

    private static final String CANCEL = new String("Cancel");
    private static final String OK = new String("OK");
    private static final String HELP = new String("Help");

    private int m_return_status = RET_CANCEL;
    private int m_new_label_type;

    private ALFINode m_node;
    private String m_typed_name;
    private String m_original_name;

    private String m_typed_description;
    private String m_original_description;

    private JTextField m_node_name;
    private JTextArea  m_node_description;

    private boolean m_able_to_rename;

    public NodeRenameDialog (JFrame _parent_frame, ALFINode _node, 
                                       String _title, boolean _able_to_rename) {
        super(_parent_frame, _title, true);
        m_node = _node;
        m_able_to_rename = _able_to_rename;
        m_original_name = m_node.determineLocalName();
        m_original_description = m_node.getDescriptionLabel();
        m_new_label_type = m_node.getLabelType();

        initComponents("RENAME NODE USING: ");
        pack();

        setSize(new Dimension (WIDTH, HEIGHT));
        Dimension dialog_size = getSize();
        Dimension screen_size = Toolkit.getDefaultToolkit().getScreenSize();

        setBounds((screen_size.width - dialog_size.width)/2,
                  (screen_size.height - dialog_size.height)/2,
                   dialog_size.width, dialog_size.height);
        setResizable(false);
     }

    private void initComponents (String _prompt_label) {
        getContentPane().setLayout(
            new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
 
        HorizontalPanel prompt_panel = 
            new HorizontalPanel(this, null, 
                              WIDTH, HEIGHT * 15/100);

        HorizontalPanel name_panel = 
            new HorizontalPanel(this, null, 
                              WIDTH, HEIGHT * 20/100);

        HorizontalPanel description_panel = 
            new HorizontalPanel(this, null, 
                              WIDTH, HEIGHT * 30/100);

        HorizontalPanel icon_panel = 
            new HorizontalPanel(this, null, 
                              WIDTH, HEIGHT * 30/100);
        
        boolean is_root = true;
        ALFINode node = m_node;
        if (!m_node.isRootNode()) {
            node = m_node.baseNode_getRoot();
            is_root = false;
        }

        JLabel label = new JLabel(_prompt_label);
        prompt_panel.add(label);

        // Elements of the name_panel
        JRadioButton name_button = new JRadioButton(NAME_DISPLAY,
        (m_node.getLabelType() == ALFINode.LABEL_TYPE_NAME)); 
        name_button.setMnemonic(KeyEvent.VK_N);
        name_button.setActionCommand(NAME_DISPLAY);

        m_node_name = new JTextField(m_original_name);
        if (is_root && m_able_to_rename) {
            m_node_name.setActionCommand(TEXTFIELD);
            m_node_name.addActionListener(this);
        }
        m_node_name.setEditable(m_able_to_rename);

        name_panel.add(name_button);
        name_panel.add(m_node_name);

        
        // Elements of the description_panel
        JRadioButton description_button = new JRadioButton(DESCRIPTION_DISPLAY,
        (m_node.getLabelType() == ALFINode.LABEL_TYPE_DESCRIPTION)); 
        description_button.setMnemonic(KeyEvent.VK_D);
        description_button.setActionCommand(DESCRIPTION_DISPLAY);

        if (m_original_description != null) {
            m_node_description = new JTextArea(m_original_description);
        }
        else {
            m_node_description = new JTextArea();
        }

        JScrollPane scroll_pane = new JScrollPane(m_node_description,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll_pane.setBorder(BorderFactory.createLoweredBevelBorder());
         
        description_panel.add(description_button);
        description_panel.add(scroll_pane);

        boolean icon_exists = false;
        if (node.getIconInfo() != null) {
            icon_exists = true;
        }

        // Elements of the icon_panel 
        JRadioButton icon_button = new JRadioButton(ICON_DISPLAY,
            (m_node.getLabelType() == ALFINode.LABEL_TYPE_ICON));
        icon_button.setMnemonic(KeyEvent.VK_I);
        icon_button.setActionCommand(ICON_DISPLAY);
        icon_button.setEnabled(icon_exists);
        icon_panel.add(icon_button);

        ButtonGroup button_group = new ButtonGroup();
        button_group.add(name_button);
        button_group.add(description_button);
        button_group.add(icon_button);

        // Register a listener for the radiobuttons
        name_button.addActionListener(this);
        description_button.addActionListener(this);
        icon_button.addActionListener(this);

        getContentPane().add(prompt_panel);
        getContentPane().add(name_panel);
        getContentPane().add(description_panel);
        getContentPane().add(icon_panel);

        JPanel button_panel;    
        button_panel = new JPanel();        
        button_panel.setLayout(new FlowLayout (FlowLayout.CENTER, 5, 5));

        JButton ok_button;    
        ok_button = new JButton();        
        ok_button.setText(OK);
        ok_button.setMnemonic('O');
        ok_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                updateData();
                doClose(RET_OK);
            }
        });   

        JButton cancel_button;    
        cancel_button = new JButton();        
        cancel_button.setText(CANCEL);
        cancel_button.setMnemonic('C');
        cancel_button.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent _event) {
                doClose(RET_CANCEL);
            }
        });   

        button_panel.add(ok_button);
        button_panel.add(cancel_button);
        getContentPane().add(button_panel);
        getRootPane().setDefaultButton(ok_button);

        switch (m_node.getLabelType()) {  
            case ALFINode.LABEL_TYPE_NAME:
            {
                name_button.doClick();
                break;
            }
            case ALFINode.LABEL_TYPE_DESCRIPTION:
            {
                description_button.doClick();
                break;
            }
            default:
                icon_button.doClick();
                break;
        }

    }


    public String getValidatedText() {
        String validated_text = null;
        // Replace all spaces with underscores
        if (m_typed_name != null) { 
            validated_text = m_typed_name.replaceAll(SPACE, UNDERSCORE);
        }
        return validated_text;
    }

    public int getNodeLabelType() {
        return m_node.getLabelType();
    }

    public boolean hasNodeNameChanged() {
        if (m_typed_name != null) {
            return (!m_typed_name.equals(m_original_name));
        }
        else {
            return false;
        }
    }

    /** Listens to the radio buttons. */
    public void actionPerformed(ActionEvent e) {
        String action_command = e.getActionCommand(); 
        if (action_command.equals(NAME_DISPLAY)) {
            m_new_label_type = ALFINode.LABEL_TYPE_NAME; 
        } 
        else if (action_command.equals(ICON_DISPLAY)) {
            m_new_label_type = ALFINode.LABEL_TYPE_ICON; 
        } 
        else if (action_command.equals(DESCRIPTION_DISPLAY)) {
            m_new_label_type = ALFINode.LABEL_TYPE_DESCRIPTION; 
        } 
        else if (action_command.equals(TEXTFIELD)) {
        }
    }

    private void updateData() {
        m_typed_description = m_node_description.getText();
        m_typed_name = m_node_name.getText();

        if (!m_typed_description.equals(m_original_description)) {
            m_node.setDescriptionLabel(m_typed_description);
        }

        m_node.setLabelType(m_new_label_type);
    }


    private void doClose (int _return_status) {
        m_return_status = _return_status;
        setVisible(false);
    }   
    

      

}
