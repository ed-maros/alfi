package edu.caltech.ligo.alfi.dialogs;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;

public class BundlerOutputSelectionDialog extends JDialog {
        // exit status values
    public static final int CANCEL = 0;
    public static final int OK     = 1;

    final private ALFINode m_container;
    final private BundlerProxy m_secondary_output_bundler;
    final private ALFINode m_bundler_root_container;
    final private int m_data_type_out;
    private int m_exit_status;
    private JTree m_tree;
    private boolean mb_multiple_select;
    private JList m_list;
    private Hashtable m_bundle_contents;
    private JTextField m_manual_add_field;
    private JCheckBox m_output_as_bundle_checkbox;
    private int m_dummy_count;
    private HashMap m_manual_list_adds_map;
    private JButton m_bundle_add_button;
    private JButton m_ok_button;
    private boolean mb_propagate_names_to_root_bundler;
    private JTextArea m_message_area;
    private String m_sink_desc;

    BundlerOutputSelectionDialog(
          Frame _owner_frame, BundlerProxy _bundler, final ALFINode _container,
          final int _data_type_out, String _sink_desc) {

        super(_owner_frame, "Output Channel Selection Dialog");

        assert(_container.bundler_contains(_bundler));

        m_container = _container;
        m_secondary_output_bundler = _bundler;
        m_bundler_root_container = _bundler.getDirectContainerNode();
        m_data_type_out = _data_type_out;
        m_sink_desc = _sink_desc;
        mb_propagate_names_to_root_bundler = false;

        setVisible(false);
        setModal(true);

        Box message_box = Box.createHorizontalBox();
        message_box.setBorder(BorderFactory.createTitledBorder(
             BorderFactory.createLoweredBevelBorder(), "General Information"));
        m_message_area = new JTextArea(4, 50);
        m_message_area.setBackground(Color.white);
        m_message_area.setEditable(false);
        m_message_area.setMargin(new Insets(5, 10, 5, 10));
        m_message_area.setLineWrap(true);
        m_message_area.setWrapStyleWord(true);
        m_message_area.setForeground(Color.red);
        message_box.add(m_message_area);

        Box output_as_bundle_box = Box.createHorizontalBox();
        output_as_bundle_box.setBorder(BorderFactory.createTitledBorder(
                  BorderFactory.createLoweredBevelBorder(), "Bundle Output?"));
        m_output_as_bundle_checkbox = new JCheckBox("Output will be a Bundle");
        if (_data_type_out == GenericPortProxy.DATA_TYPE_X_UNRESOLVED) {
                // user may still decide if data type is bundle or not
            m_output_as_bundle_checkbox.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        updateInterface();
                    }
                });
        }
        else {
                // bundle vs non-bundle is definite.  user may not change it.
            m_output_as_bundle_checkbox.setSelected(
                     _data_type_out == GenericPortProxy.DATA_TYPE_ALFI_BUNDLE);
            m_output_as_bundle_checkbox.setEnabled(false);
        }
        output_as_bundle_box.add(m_output_as_bundle_checkbox);
        output_as_bundle_box.add(Box.createHorizontalGlue());

        Box tree_box = Box.createHorizontalBox();
        tree_box.setBorder(BorderFactory.createTitledBorder(
                       BorderFactory.createLoweredBevelBorder(),
                       "Currently Available Output Channels  (click to add)"));

        BundleProxy primary_input =
                       m_secondary_output_bundler.getPrimaryInput(m_container);
        m_bundle_contents = (primary_input != null) ?
                 primary_input.getContent(m_container, true) : new Hashtable();

        m_tree = createOutputContentChoiceTree(_data_type_out);
        JScrollPane tree_scroller = new JScrollPane(m_tree,
                              ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                              ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        tree_box.add(tree_scroller);

            // create choice list
        m_list = new JList();
        m_list.getSelectionModel().addListSelectionListener(
            new ListSelectionListener() {
                private boolean mb_event_processing_enabled = true;
                public void valueChanged(ListSelectionEvent _event) {
                        // ignores events fired while doing remove
                    if (mb_event_processing_enabled) {
                            // only want last of possible series of events
                        if (! _event.getValueIsAdjusting()) {
                            int index = _event.getFirstIndex();
                            if (index < m_list.getModel().getSize()) {
                                mb_event_processing_enabled = false;
                                removeListSelection();
                                mb_event_processing_enabled = true;
                            }
                        }
                    }
                }
            });

        Box list_box = Box.createHorizontalBox();
        list_box.setBorder(BorderFactory.createTitledBorder(
                            BorderFactory.createLoweredBevelBorder(),
                            "Selected Output Channel(s)   (click to remove)"));
        JScrollPane list_scroller = new JScrollPane(m_list,
                              ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                              ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        list_box.add(list_scroller);
        

        m_manual_list_adds_map = new HashMap();
        Box manual_add_box = Box.createHorizontalBox();
        manual_add_box.setBorder(BorderFactory.createTitledBorder(
                                  BorderFactory.createLoweredBevelBorder(),
                                                 "Manual Output Channel Add"));
        manual_add_box.add(Box.createHorizontalGlue());
        m_dummy_count = 0;
        m_manual_add_field = new JTextField("DUMMY_" + m_dummy_count++, 20);
        Box field_box = Box.createVerticalBox();
        field_box.add(Box.createVerticalStrut(5));
        field_box.add(m_manual_add_field);
        field_box.add(Box.createVerticalStrut(5));
        manual_add_box.add(field_box);

        manual_add_box.add(Box.createHorizontalStrut(10));
        JButton channel_add_button = new JButton("Add Single Channel");
        manual_add_box.add(channel_add_button);
        manual_add_box.add(Box.createHorizontalGlue());
        channel_add_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String name = m_manual_add_field.getText();
                    if (BundlerProxy.outputChannelPathNameIsValid(name)) {
                        if (! mb_multiple_select) {
                            m_tree.clearSelection();
                            m_manual_list_adds_map.clear();
                        }
                        m_manual_list_adds_map.put(name, new Boolean(false));
                        updateSelectionList();
                    }
                    String new_text =
                        mb_multiple_select ? ("DUMMY_" + m_dummy_count++) : "";
                    m_manual_add_field.setText(new_text);
                }
            });

        manual_add_box.add(Box.createHorizontalStrut(10));
        m_bundle_add_button = new JButton("Add Bundle");
        manual_add_box.add(m_bundle_add_button);
        manual_add_box.add(Box.createHorizontalGlue());
        m_bundle_add_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String name = m_manual_add_field.getText();
                    if (BundlerProxy.outputChannelPathNameIsValid(name)) {
                        if (! mb_multiple_select) {
                            m_tree.clearSelection();
                            m_manual_list_adds_map.clear();
                        }
                        m_manual_list_adds_map.put(name, new Boolean(true));
                        updateSelectionList();
                    }
                    String new_text =
                        mb_multiple_select ? ("DUMMY_" + m_dummy_count++) : "";
                    m_manual_add_field.setText(new_text);
                }
            });

            // OK and cancel buttons
        JPanel button_panel = createButtonPanel();

        Box main_box =  Box.createVerticalBox();
        if (message_box != null) {
            main_box.add(Box.createVerticalStrut(5));
            main_box.add(message_box);
        }
        main_box.add(Box.createVerticalStrut(5));
        main_box.add(output_as_bundle_box);
        main_box.add(Box.createVerticalStrut(5));
        main_box.add(tree_box);
        main_box.add(Box.createVerticalStrut(5));
        main_box.add(manual_add_box);
        main_box.add(Box.createVerticalStrut(5));
        main_box.add(list_box);
        main_box.add(Box.createVerticalStrut(10));
        main_box.add(button_panel);
        main_box.add(Box.createVerticalStrut(5));

        Container content_pane = getContentPane();
        content_pane.add(main_box);

        m_exit_status = CANCEL;

        updateInterface();
    }

    private JPanel createButtonPanel() {
        JPanel button_panel = new JPanel();
        button_panel.setLayout(new BoxLayout(button_panel, BoxLayout.X_AXIS));
        m_ok_button = new JButton("OK");
        m_ok_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    m_exit_status = OK;
                }
            });
        button_panel.add(m_ok_button);
        getRootPane().setDefaultButton(m_ok_button);

        button_panel.add(Box.createHorizontalStrut(2));

        JButton cancel_button = new JButton("Cancel");
        cancel_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    m_exit_status = CANCEL;
                }
            });
        button_panel.add(cancel_button);

        button_panel.add(Box.createHorizontalStrut(8));

        if (! m_container.bundler_containsLocal(m_secondary_output_bundler)) {
            String s = "Set names in root bundler container [" +
                                              m_bundler_root_container + "]";
            final JCheckBox propagate_checkbox =
                          new JCheckBox(s, mb_propagate_names_to_root_bundler);
            propagate_checkbox.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        mb_propagate_names_to_root_bundler =
                                               propagate_checkbox.isSelected();
                        updatePropagateToRootWarning();
                    }
                });
            button_panel.add(propagate_checkbox);

            updatePropagateToRootWarning();
        }

        return button_panel;
    }

    private void updatePropagateToRootWarning() {
        String message = "";
        if (mb_propagate_names_to_root_bundler) {
            message =
                "WARNING: This channel change will be propogated to bundler " +
                "output [" + m_secondary_output_bundler + " -> " + m_sink_desc +
                "] in the bundler root container node [" +
                m_bundler_root_container + "]!";

            ALFINode[] affected_boxes = getAffectedBoxes();
            if (affected_boxes.length > 0) {
                message += "\n\nNote: The following boxes will need to be " +
                    "resaved to incorporate this change:\n\n";

                for (int i = 0; i < affected_boxes.length; i++) {
                    message += "\t" +
                        affected_boxes[i].getSourceFile().getCanonicalPath() +
                        "\n";
                }
            }
        }
        m_message_area.setText(message);
    }

    public boolean propagateChannelNamesToRootBaseBundler() {
        return mb_propagate_names_to_root_bundler;
    }

    private void updateInterface() {
            // only bundle output may allow multiple channel out selections
        mb_multiple_select = m_output_as_bundle_checkbox.isSelected();
        m_bundle_add_button.setEnabled(mb_multiple_select);
        m_tree.treeDidChange();
        if ((! mb_multiple_select) && (getSelections().length > 1)) {
            m_tree.clearSelection();
            m_manual_list_adds_map.clear();
            updateSelectionList();
        }
        int mode = (mb_multiple_select) ?
                              TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION :
                              TreeSelectionModel.SINGLE_TREE_SELECTION;
        m_tree.getSelectionModel().setSelectionMode(mode);

        m_ok_button.setEnabled(getSelections().length > 0);
    }

    public boolean outputIsBundle() {
        return m_output_as_bundle_checkbox.isSelected();
    }

    public ALFINode[] getAffectedBoxes() {
        ALFINode[] derived_nodes=m_bundler_root_container.derivedNodes_getAll();
        HashMap box_map = new HashMap();
        box_map.put(m_bundler_root_container, null);
        for (int i = 0; i < derived_nodes.length; i++) {
            ALFINode box = (derived_nodes[i].isRootNode()) ?
                   derived_nodes[i] : derived_nodes[i].containerNode_getRoot();
            box_map.put(box, null);
        }

        ArrayList box_list = new ArrayList(box_map.keySet());
        Collections.sort(box_list);
        ALFINode[] boxes = new ALFINode[box_list.size()];
        box_list.toArray(boxes);

        return boxes;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///// static methods ///////////////////////////////////////////////////////

    public static BundlerOutputSelectionDialog executeDialog(Frame _owner_frame,
                              BundlerProxy _bundler, final ALFINode _container,
                              final int _data_type_out, String _sink_desc) {

        BundlerOutputSelectionDialog dialog = new BundlerOutputSelectionDialog(
                _owner_frame, _bundler, _container, _data_type_out, _sink_desc);

        dialog.pack();
        dialog.setSize(500, 800);
        EditorFrame.centerDialog(dialog);

        dialog.setVisible(true);

        return dialog;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///// instance methods /////////////////////////////////////////////////////

    private void updateSelectionList() {
        Object[][] tree_selections = getTreeSelections();
        TreeMap name_map = new TreeMap();
        for (int i = 0; i < tree_selections.length; i++) {
            name_map.put(tree_selections[i][0], null);
        }

        name_map.putAll(m_manual_list_adds_map);

        String[] names = new String[name_map.size()];
        name_map.keySet().toArray(names);

        m_list.setListData(names);

        updateInterface();
    }

    private void removeListSelection() {
        String name = m_list.getSelectedValue().toString();

        m_manual_list_adds_map.remove(name);

        Object[][] tree_selections = getTreeSelections();
        for (int i = 0; i < tree_selections.length; i++) {
            if (tree_selections[i][0].equals(name)) {
                m_tree.getSelectionModel().removeSelectionPath(
                                             (TreePath) tree_selections[i][2]);
            }
        }

        updateSelectionList();
    }

    public int getExitStatus() { return m_exit_status; }

        /** Returns an array of Object[5] = { String (full input name relative
          *   to output bundler location), Boolean (true if output channel is
          *   a bundle), BundleContentSource (input bundler, or input bundle
          *   port with no external bundle input), ALFINode (input source
          *   container), TreePath (selected element in tree (for internal use
          *   in this object)) }.
          *
          * In cases where the user enters a channel name manually, the last
          * value in the associated Object[5] is null.
          */
    public Object[][] getSelections() {
        Object[][] tree_selections = getTreeSelections();

        ListModel list_model = m_list.getModel();
        Object[][] all_selections = new Object[list_model.getSize()][];
        for (int i = 0; i < all_selections.length; i++) {
            String full_name = list_model.getElementAt(i).toString();

                // defaults to . . .
            BundleContentSource content_source = m_secondary_output_bundler;
            ALFINode container = m_container;

            TreePath tree_path = null;
            for (int j = 0; j < tree_selections.length; j++) {
                if (full_name.equals(tree_selections[j][0])) {
                    BundleContentMapKey key =
                                   (BundleContentMapKey) tree_selections[j][1];
                    content_source = key.getChannelSource();
                    container = key.getChannelSourceContainer();
                    tree_path = (TreePath) tree_selections[j][2];
                    break;
                }
            }

            Boolean B_is_bundle = new Boolean(channelOutIsBundle(full_name,
                                                   content_source, container));
            Object[] info_set =
                {full_name, B_is_bundle, content_source, container, tree_path};
            all_selections[i] = info_set;
        }

        return all_selections;
    }

    private boolean channelOutIsBundle(String _channel_name,
                    BundleContentSource _content_source, ALFINode _container) {
        if (_content_source != null) {
            if (_content_source instanceof BundlerProxy) {
                BundlerProxy bundler = (BundlerProxy) _content_source;
                ConnectionProxy input = bundler.getSecondaryInput(_container);
                return (input instanceof BundleProxy);
            }
            else if (_content_source instanceof BundlePortProxy) {
                BundlePortProxy bundle_port = (BundlePortProxy) _content_source;
                int data_type =
                          bundle_port.getDefaultChannelDataType(_channel_name);
                return (data_type == GenericPortProxy.DATA_TYPE_ALFI_BUNDLE);
            }
            else {
                Alfi.warn("BundlerOutputSelectionDialog.channelOutIsBundle():\n"
                        + "\tInvalid call.");
                return false;
            }
        }
        else {
            Boolean B_is_bundle =
                      (Boolean) m_manual_list_adds_map.get(_channel_name);
            return ((B_is_bundle != null) ? B_is_bundle.booleanValue() : false);
        }
    }

        /** Returns an array of Object[3] = { String (full input name relative
          *   to output bundler location), BundleContentMapKey, TreePath
          *   (selected element in tree (for internal use in this object)) }.
          */
    private Object[][] getTreeSelections() {
        ArrayList output_info_list = new ArrayList();
        TreePath[] selections = m_tree.getSelectionPaths();
        if (selections != null) {
            for (int i = 0; i < selections.length; i++) {
                Object[] path_parts = selections[i].getPath();
                String full_output_name = new String();
                BundleContentMapKey key = null;

                    // we don't care about tree root (j = 0)
                for (int j = 1; j < path_parts.length; j++) {
                    DefaultMutableTreeNode tree_node =
                                        (DefaultMutableTreeNode) path_parts[j];
                    key = (BundleContentMapKey) tree_node.getUserObject();
                    if (j > 1) { full_output_name += "."; }
                    full_output_name += key.getChannelName();
                }
                if (full_output_name.length() > 0) {
                    Object[] output_info =
                                      { full_output_name, key, selections[i] };
                    output_info_list.add(output_info);
                }
            }
        }

        Object[][] outputs = new Object[output_info_list.size()][];
        output_info_list.toArray(outputs);

        return outputs;
    }

    private String createKeyText(BundleContentMapKey _key) {
        return _key.getChannelName();
    }

    private JTree createOutputContentChoiceTree(final int _data_type_out) {
            // customizes the tree node label text
        final BundlerOutputSelectionDialog dialog = this;
        JTree tree = new JTree(m_bundle_contents) {
            public String convertValueToText(Object _value, boolean _sel,
                                             boolean _expanded, boolean _leaf,
                                             int _row, boolean _hasFocus) {
                Object key = ((DefaultMutableTreeNode) _value).getUserObject();
                return (key instanceof BundleContentMapKey) ?
                     dialog.createKeyText((BundleContentMapKey) key) : "error";
            }
          };

        mb_multiple_select = false;    // dummy init.  mode updated in update()
        tree.getSelectionModel().addTreeSelectionListener(
                                                  new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent _event) {
                if (! mb_multiple_select) { m_manual_list_adds_map.clear(); }
                updateSelectionList();
            }
        });

            // non-matching data types cannot be selected
        final int BUNDLE = GenericPortProxy.DATA_TYPE_ALFI_BUNDLE;
        final int UNKNOWN = GenericPortProxy.DATA_TYPE_1_UNKNOWN;
        final int UNRESOLVED = GenericPortProxy.DATA_TYPE_1_UNRESOLVED;
        final int X_UNRESOLVED = GenericPortProxy.DATA_TYPE_X_UNRESOLVED;
        final int INVALID = GenericPortProxy.DATA_TYPE_INVALID;

        tree.setCellRenderer(new DefaultTreeCellRenderer() {
            public Component getTreeCellRendererComponent(JTree _tree,
                                Object _value, boolean _sel, boolean _expanded,
                                boolean _leaf, int _row, boolean _has_focus) {
                boolean b_selectable = true;
                if (_value instanceof DefaultMutableTreeNode) {
                    DefaultMutableTreeNode tree_node =
                                               (DefaultMutableTreeNode) _value;
                    DefaultTreeModel tree_model =
                                           (DefaultTreeModel) _tree.getModel();
                    if (tree_node != tree_model.getRoot()) {
                        BundleContentMapKey key =
                               (BundleContentMapKey) tree_node.getUserObject();
                        int data_type_in = key.getDataType();

                            // data_type_in is the data type coming into the
                            // bundler from upstream.  _data_type_out is the
                            // data type downstream the connection to be.
                        if (
                                 (! key.isContentHolderOnly())

                                              &&

                                 (
                                    outputIsBundle() ||

                                    (data_type_in == _data_type_out) ||

                                    ( (data_type_in != BUNDLE) &&
                                        ((_data_type_out == UNKNOWN) ||
                                         (_data_type_out == UNRESOLVED) ||
                                         (_data_type_out == X_UNRESOLVED)) ) ||

                                    ( (_data_type_out != BUNDLE) &&
                                        ((data_type_in == UNKNOWN) ||
                                         (data_type_in == UNRESOLVED)) )
                                 )
                                                                            ) {
                            ;  // do nothing
                        }
                        else {
                            TreePath path = new TreePath(
                                          tree_model.getPathToRoot(tree_node));
                            _tree.removeSelectionPath(path);
                            b_selectable = false;
                        }
                    }
                    else { b_selectable = false; }
                }

                boolean b_selected = b_selectable && _sel;
                boolean b_has_focus = b_selectable && _has_focus;

                Component component = super.getTreeCellRendererComponent(_tree,
                     _value, b_selected,  _expanded, _leaf, _row, b_has_focus);

                component.setEnabled(b_selectable);

                return component;
            }
          });

        return tree;
    }
}
