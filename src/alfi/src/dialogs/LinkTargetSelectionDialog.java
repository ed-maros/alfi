package edu.caltech.ligo.alfi.dialogs;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.widgets.*;

public class LinkTargetSelectionDialog extends JDialog {

    private final static Dimension WINDOW_SIZE = new Dimension(500, 600);

    private ALFINode m_root_node;
    private JTree m_tree;
    private HashMap m_data_types_vs_link_target_map;
    private int m_valid_data_type;

        /** _valid_data_type cannot be a bundle. */
    LinkTargetSelectionDialog(JDialog _parent, ALFINode _root_box_node,
                                                        int _valid_data_type) {
        super(_parent, "Link Target Selection: " + _root_box_node, true);
        assert(_root_box_node.isBox());
        assert(_root_box_node.isRootNode());
        assert(_valid_data_type != GenericPortProxy.DATA_TYPE_ALFI_BUNDLE);

        m_root_node = _root_box_node;
            // if any targets have already been added to this link...
        m_valid_data_type = _valid_data_type;

        setVisible(false);
        setBounds(determineDialogBounds(_parent));
        setResizable(true);

        initializeTree();
        JScrollPane scroll_pane = new JScrollPane(m_tree,
                           ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                           ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        JPanel button_panel = createButtonPanel();

        Container content_pane = getContentPane();
        content_pane.setLayout(new BorderLayout(10, 10));
        content_pane.add("Center", scroll_pane);
        content_pane.add("South", button_panel);
    }

        /** Returns String[n][2] with n elements of
          * { node_name, target_name }.
          */
    String[][] getSelections() {
        ArrayList selection_list = new ArrayList();
        TreePath[] paths = m_tree.getSelectionPaths();
        if (paths != null) {
            for (int i = 0; i < paths.length; i++) {
                Object[] path = paths[i].getPath();
                TreeNode tree_node = (TreeNode) path[path.length - 1];
                if (tree_node.isLeaf() && (path.length > 2)) {
                    String target_name = tree_node.toString();
                    String node_name = path[1].toString();
                    for (int j = 2; j < (path.length - 1); j++) {
                        node_name += "." + path[j].toString();
                    }
                    String[] target_info = { node_name, target_name };
                    selection_list.add(target_info);
                }
            }
        }

        String[][] targets_info = new String[selection_list.size()][];
        selection_list.toArray(targets_info);
        return targets_info;
    }

    private void initializeTree() {
        initializeTargetDataTypes();
        Hashtable node_targets_map = determineTargetTreeData(m_root_node, true);

        m_tree = new JTree(node_targets_map);
        m_tree.setEditable(false);
        m_tree.setBackground(Color.white);
        final int UNKNOWN_SINGLE = GenericPortProxy.DATA_TYPE_1_UNKNOWN;
        m_tree.setSelectionModel(
            new DefaultTreeSelectionModel() {
                public void setSelectionPath(TreePath _path) {
                    TreeNode tree_node = (TreeNode)_path.getLastPathComponent();
                    if (tree_node.isLeaf()) {
                        String full_name = createPathName(tree_node);
                        if ((m_valid_data_type == UNKNOWN_SINGLE) ||
                                (getDataType(full_name) == m_valid_data_type)) {
                            super.setSelectionPath(_path);
                        }
                    }
                }

                public void setSelectionPaths(TreePath[] _paths) {
                    ArrayList path_list = new ArrayList(Arrays.asList(_paths));
                    for (Iterator i = path_list.iterator(); i.hasNext(); ) {
                        TreePath path = (TreePath) i.next();
                        TreeNode tree_node =
                                        (TreeNode) path.getLastPathComponent();
                        if (tree_node.isLeaf()) {
                            String full_name = createPathName(tree_node);
                            int type = getDataType(full_name);
                            if (! ((m_valid_data_type == UNKNOWN_SINGLE) ||
                                                (type == m_valid_data_type))) {
                                i.remove();
                            }
                        }
                        else { i.remove(); }
                    }
                    TreePath[] paths = new TreePath[path_list.size()];
                    path_list.toArray(paths);
                    super.setSelectionPaths(paths);
                }

                public void addSelectionPath(TreePath _path) {
                    TreeNode tree_node = (TreeNode)_path.getLastPathComponent();
                    if (tree_node.isLeaf()) {
                        String full_name = createPathName(tree_node);
                        if ((m_valid_data_type == UNKNOWN_SINGLE) ||
                                (getDataType(full_name) == m_valid_data_type)) {
                            super.addSelectionPath(_path);
                        }
                    }
                }

                public void addSelectionPaths(TreePath[] _paths) {
                    ArrayList path_list = new ArrayList(Arrays.asList(_paths));
                    for (Iterator i = path_list.iterator(); i.hasNext(); ) {
                        TreePath path = (TreePath) i.next();
                        TreeNode tree_node =
                                        (TreeNode) path.getLastPathComponent();
                        if (tree_node.isLeaf()) {
                            String full_name = createPathName(tree_node);
                            int type = getDataType(full_name);
                            if (! ((m_valid_data_type == UNKNOWN_SINGLE) ||
                                                (type == m_valid_data_type))) {
                                i.remove();
                            }
                        }
                        else { i.remove(); }
                    }
                    TreePath[] paths = new TreePath[path_list.size()];
                    path_list.toArray(paths);
                    super.addSelectionPaths(paths);
                }
            });

        m_tree.setCellRenderer(
            new DefaultTreeCellRenderer() {
                public Component getTreeCellRendererComponent(JTree _tree,
                            Object _value, boolean _b_sel, boolean _b_expanded,
                            boolean _b_leaf, int _row, boolean _b_has_focus) {
                    setBackgroundNonSelectionColor(Color.white);
                    setBackgroundSelectionColor(Color.lightGray);
                    Component c = super.getTreeCellRendererComponent(_tree,
                        _value, _b_sel, _b_expanded, _b_leaf,_row,_b_has_focus);
                    if (_b_leaf) {
                        TreeNode tree_node = (TreeNode) _value;
                        String full_name = createPathName(tree_node);
                        c.setForeground(determineDataTypeColor(full_name));
                        if (m_valid_data_type !=
                                        GenericPortProxy.DATA_TYPE_1_UNKNOWN) {
                            if (getDataType(full_name) != m_valid_data_type) {
                                c.setEnabled(false);
                            }
                        }
                    }
                    return c;
                }
            });
    }

        /** Returns something like "root_box.box_a.box_b.prim_1.param_name" */
    private String createPathName(TreeNode _tree_node) {
        String full_name = "." + _tree_node.toString();
        TreeNode parent = _tree_node.getParent();
        while (! parent.toString().equals("root")) {
            full_name = "." + parent + full_name;
            parent = parent.getParent();
        }
        full_name = m_root_node.determineLocalName() + full_name;
        return full_name;
    }

    private void initializeTargetDataTypes() {
        m_data_types_vs_link_target_map = new HashMap();

        LinkDeclaration[] link_decs = m_root_node.linkDeclarations_getLocal();
        for (int i = 0; i < link_decs.length; i++) {
            String desc = m_root_node.generateFullNodePathName() + "." +
                                                        link_decs[i].getName();
            int data_type = GenericPortProxy.getDataTypeIdFromString(
                                                       link_decs[i].getType());
            m_data_types_vs_link_target_map.put(desc, new Integer(data_type));
        }

        ALFINode[] members = m_root_node.memberNodes_getAllGenerations();
        for (int i = 0; i < members.length; i++) {
            String node_path = members[i].generateFullNodePathName();
            ALFINode member_base_node = members[i].baseNode_getRoot();
            NodeSettingDeclarationIF[] setting_decs = members[i].isBox() ?
                    (NodeSettingDeclarationIF[])
                             member_base_node.linkDeclarations_getLocal() :
                    (NodeSettingDeclarationIF[])
                             member_base_node.parameterDeclarations_getLocal();

            for (int j = 0; j < setting_decs.length; j++) {
                String desc = node_path + "." + setting_decs[j].getName();
                int data_type = GenericPortProxy.getDataTypeIdFromString(
                                                    setting_decs[j].getType());
                m_data_types_vs_link_target_map.put(desc,
                                                       new Integer(data_type));
            }
        }
    }

    private int getDataType(String _target_full_name) {
        Integer type_Integer =
              (Integer) m_data_types_vs_link_target_map.get(_target_full_name);
        return type_Integer.intValue();
    }

    private Color determineDataTypeColor(String _target_full_name) {
        return PortWidget.getDataTypeColor(getDataType(_target_full_name));
    }

    private Hashtable determineTargetTreeData(ALFINode _node,
                                              boolean _b_is_top_level_call) {
        Hashtable map = new Hashtable();

        if (! _b_is_top_level_call) {
            LinkDeclaration[] link_decs =
                          _node.baseNode_getRoot().linkDeclarations_getLocal();
            for (int i = 0; i < link_decs.length; i++) {
                map.put(link_decs[i].getName(), "dummy");
            }
        }

        ALFINode[] members = _node.memberNodes_getDirectlyContained();
        for (int i = 0; i < members.length; i++) {
            ALFINode member = members[i];
            String member_name = member.determineLocalName();
            if (member.isPrimitive()) {
                ALFINode base_primitive = member.baseNode_getRoot();
                ParameterDeclaration[] parameter_decs =
                               base_primitive.parameterDeclarations_getLocal();
                Hashtable param_map = new Hashtable();
                for (int j = 0; j < parameter_decs.length; j++) {
                    param_map.put(parameter_decs[j].getName(), "dummy");
                }
                map.put(member_name, param_map);
            }
            else {
                map.put(member_name, determineTargetTreeData(member, false));
            }
        }

        return map;
    }

    private JPanel createButtonPanel() {
        JPanel button_panel = new JPanel();
        button_panel.setLayout(new BoxLayout(button_panel,
                                   BoxLayout.X_AXIS));
        button_panel.setBorder(BorderFactory.createEtchedBorder());
        button_panel.add(Box.createHorizontalGlue());

        JButton ok_button = new JButton("OK");
        ok_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (selectionsAreValid()) {
                        setVisible(false);
                    }
                    else {
                        String title = "Inconsistent Data Types Message";
                        String message =
                            "The selection contains multiple data types.  A\n" +
                            "link may be associated only with targets of\n" +
                            "like data types.\n\n" +
                            "Please deselect targets until only those of\n" +
                            "one type exist in the selection.";
                        JOptionPane.showMessageDialog(null, message, title,
                                                  JOptionPane.WARNING_MESSAGE);
                    }
                }
            });
        button_panel.add(ok_button);
        getRootPane().setDefaultButton(ok_button);

        JButton cancel_button = new JButton("Cancel");
        cancel_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                }
            });
        button_panel.add(Box.createHorizontalStrut(10));
        button_panel.add(cancel_button);
        button_panel.add(Box.createHorizontalGlue());
        
        return button_panel;
    }

    private boolean selectionsAreValid() {
        String[][] selections = getSelections();
        int test_type = GenericPortProxy.DATA_TYPE_1_UNKNOWN;
        for (int i = 0; i < selections.length; i++) {
            String full_name = m_root_node.determineLocalName() + "." +
                                     selections[i][0] + "." + selections[i][1];
            int type = getDataType(full_name);
            if (type != GenericPortProxy.DATA_TYPE_1_UNKNOWN) {
                if (test_type == GenericPortProxy.DATA_TYPE_1_UNKNOWN) {
                    test_type = type;
                }
                else if (type != test_type) { return false; }
            }
        }

        return true;
    }

    private Rectangle determineDialogBounds(JDialog _parent) {
        Rectangle rect = _parent.getBounds();
        Point p = new Point(rect.x + (rect.width/2), rect.y + (rect.height/2));
        rect = new Rectangle(p, WINDOW_SIZE);

        Dimension screen_size = _parent.getToolkit().getScreenSize();
        while (rect.x < Alfi.WINDOW_MANAGER_OFFSET.x) {
            rect.translate(10, 0);
        }

        while (rect.y < Alfi.WINDOW_MANAGER_OFFSET.y) {
            rect.translate(0, 10);
        }

        while ((rect.x + rect.width) > (screen_size.width +
                                        Alfi.WINDOW_MANAGER_OFFSET.x)) {
            rect.translate(-10, 0);
        }

        while ((rect.y + rect.height) > (screen_size.height +
                                         Alfi.WINDOW_MANAGER_OFFSET.y)) {
            rect.translate(0, -10);
        }

        return rect;
    }
}
