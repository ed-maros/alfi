
package edu.caltech.ligo.alfi.dialogs;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.bookkeeper.*;

/**
  * <pre>
  * The table model that is displayed in the ParameterTable.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */

public class ParameterTableModel extends DefaultTableModel
                                 implements TableModelListener {

    public static final String DEFAULT_VALUE = "DEFAULT";
    public static final String PARAMETER_TYPE = "parameter";
    public static final String INPUT_PORT_TYPE = "input port";
    public static final String FILE_TYPE = " ";
    public static final String LINE_DELIMETERS = "\n\r\f"; 
                                                //newline, CR, formfeed
    public static final String SET_IN = "set in ";

    public static final int LOCAL_VALUE_INDEX = 0;
    public static final int TYPE_INDEX = 1;
    public static final int DATA_TYPE_INDEX = 2;
    public static final int LOCAL_COMMENT_INDEX = 3;
    public static final int DEFAULT_VALUE_INDEX = 4;
    public static final int DEFAULT_COMMENT_INDEX = 5;

    private HashMap m_parameter_map = new HashMap();
    private HashMap m_original_param_values_map = new HashMap();
    private ArrayList m_modified_values_list;
    private ArrayList m_header_name_list;
    private ArrayList m_row_declaration;
    private ArrayList m_declaration_location;

    // For FUNCX_Nodes only
    private ArrayList m_funcx_variables;

    private ListModel m_header_name_list_model;

    private ALFINode m_node;
    
    public ParameterTableModel (ALFINode _node,
                                String[] _column_names) {
        super(_column_names, 1);

        m_node = _node;
        
        getDeclarations();
        setRowCount(getNumOfVariables());

        int table_model_row_count =  getRowCount();

        m_modified_values_list = new ArrayList(table_model_row_count);
        m_header_name_list = new ArrayList(table_model_row_count);
        
        setTableValues();

        m_header_name_list_model = new AbstractListModel() {
            public int getSize () { return m_header_name_list.size();}
            public Object getElementAt (int _index) {
                return m_header_name_list.get(_index);
            }
        };
    }

    private int getNumOfVariables() {
        if (m_node instanceof FUNCX_Node) {
            return  m_funcx_variables.size();
        }
        else {
            return m_row_declaration.size();
        }
    }
      
    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
        if (col == LOCAL_VALUE_INDEX) { 
            return true;
        } else {
            return false;
        }
    }

   /**
     * When setting the value in the LOCAL_VALUE_INDEX, create
     * a 'Modified' entry in the 
     */
    public void setValueAt(Object _value, int _row, int _col) {
        super.setValueAt(_value, _row, _col);  
        String value = (String) _value;

        if (_col == LOCAL_VALUE_INDEX) {
            boolean has_default_value = true;

            if (m_modified_values_list.size() <= _row) {
                String data_type = (String) getValueAt(_row, TYPE_INDEX);

                if (data_type.equals(PARAMETER_TYPE)) {
                    m_modified_values_list.add( 
                              new ValueAttributes(has_default_value));

                } else if (data_type.equals(INPUT_PORT_TYPE)) {
                    m_modified_values_list.add( 
                        new ValueAttributes(false, has_default_value, 
                            ValueAttributes.INPUT_PORT_TYPE));

                } else if (data_type.equals(FILE_TYPE)) {
                    m_modified_values_list.add( 
                        new ValueAttributes(false, true, 
                            ValueAttributes.FILE_TYPE));
                }
            }
            else {
                boolean is_value_modified = false;
                if (!value.equals(DEFAULT_VALUE)) {
                    value = value.trim();
                    if (value.length() > 0) {
                        is_value_modified = true;
                    }
                }

                ValueAttributes flags = 
                    (ValueAttributes) m_modified_values_list.get(_row);

                flags.setValueModified(is_value_modified);
                fireTableCellUpdated(_row, _col);
            }
        }
        else if (_col == DEFAULT_VALUE_INDEX) {
            String data_type = (String) getValueAt(_row, TYPE_INDEX);
            boolean has_default_value = true;

            if (data_type.equals(PARAMETER_TYPE)) {
                String default_value = (String) getValueAt(_row,
                    DEFAULT_VALUE_INDEX);
                has_default_value = (default_value.length() > 0);
                ValueAttributes flags = 
                    (ValueAttributes) m_modified_values_list.get(_row);

                flags.setDefaultValue(has_default_value);
                fireTableCellUpdated(_row, _col);
            }
        }
    }

    /**
      * Static method to determine if the text is multilined
      */
    public static boolean isMultilineText (String _text) {
        StringTokenizer tokenizer = new StringTokenizer(_text, 
                ParameterTableModel.LINE_DELIMETERS);
        return (tokenizer.countTokens() > 1); 
    }

    /**
      * Returns the header list model
      */
    public ListModel getHeaderNameListModel () {
        return m_header_name_list_model; 
    }

        /** Sets the values in the table */
    private void setTableValues () {

        if ((m_node instanceof FUNCX_Node)) {

            for (int i = 0; i < m_funcx_variables.size(); i++) {
                String[] table_item  = (String[]) m_funcx_variables.get(i);
                String name = table_item[FUNCX_Node.LIST_NAME];
                String type = 
                    (table_item[FUNCX_Node.LIST_TYPE] != null) ?
                     table_item[FUNCX_Node.LIST_TYPE] :
                     AlfiConstants.EMPTY;
                String local_value = 
                    (table_item[FUNCX_Node.LIST_LOCAL_VALUE] != null) ?
                     table_item[FUNCX_Node.LIST_LOCAL_VALUE] :
                    AlfiConstants.EMPTY;
                String local_comment = 
                    (table_item[FUNCX_Node.LIST_LOCAL_COMMENT] != null) ?
                     table_item[FUNCX_Node.LIST_LOCAL_COMMENT] :
                    AlfiConstants.EMPTY;
                String default_value = 
                    (table_item[FUNCX_Node.LIST_DEFAULT_VALUE] != null) ?
                     table_item[FUNCX_Node.LIST_DEFAULT_VALUE] :
                    AlfiConstants.EMPTY;
                String default_location = 
                    (table_item[FUNCX_Node.LIST_DEFAULT_LOCATION] != null) ?
                     table_item[FUNCX_Node.LIST_DEFAULT_LOCATION] :
                     AlfiConstants.EMPTY;

                setDefaultValue(i, name, type, default_value);
                displayDefaultSetting(default_location, name, type, default_value);

                if (local_value.length() > 0) {      
                    // Store the original values of the parameter value mods
                    // and create a new entry in the map if the key does not
                    // exist
                    String key = name + type;
                    local_value = local_value.trim();
                    m_original_param_values_map.put(key, local_value);
                    this.setValueAt(local_value, i, LOCAL_VALUE_INDEX);
                    this.setValueAt(local_comment, i, LOCAL_COMMENT_INDEX);
                }
            }

        }
        else {
            NodeSettingDeclarationIF[] declarations =
                              new NodeSettingDeclarationIF[m_row_declaration.size()];
            m_row_declaration.toArray(declarations);

            String[] locations = new String[m_declaration_location.size()];
            m_declaration_location.toArray(locations);

            int map_index = 0;
            for (int i = 0; i < declarations.length; i++) {
                String name = declarations[i].getName();
                String type = declarations[i].getType();
                String default_value = declarations[i].getValue();
                setDefaultValue(i, name, type, default_value);
                displayDefaultSetting(locations[i], name, type, default_value);
            }

            // now update the default settings (if needed) by looking through
            // the possible settings in more direct base nodes.  we start with
            // the least direct to the most direct base nodes, so that the
            // latest setting takes precedence.
            ALFINode[] base_nodes = m_node.baseNodes_getHierarchyPath();
            for (int i = 1; i <=(base_nodes.length - 1); i++) {
                adjustDefaultsDueToBaseNodeSettings(base_nodes[i]);
            }

            setModifiedValues();
        }
    }

    /**
      * Sets the default value 'DEFAULT' in the row specified.
      */
    public void setDefaultValue (int _row, SimpleCellRenderer _text) {
        setValueAt(DEFAULT_VALUE, _row, LOCAL_VALUE_INDEX);
        fireTableCellUpdated(_row, LOCAL_VALUE_INDEX);

        _text.setCellValue(DEFAULT_VALUE);
    }



    /**
      * Sets the default values of the table
      */
    private void setDefaultValue ( int _row_number, String _name, String _type, 
                                    String _default_value) {

        String key = _name + _type;
        if (! m_parameter_map.containsKey(key) ){
            m_parameter_map.put(key, new Integer(_row_number));
  
            // Save the original value of the parameter in the map
            m_original_param_values_map.put(key, DEFAULT_VALUE);

            // Create a list of header names so it can be used to create
            // the ListModel.
            m_header_name_list.add(_row_number, _name);

            // Set the data type first
            setValueAt(PARAMETER_TYPE, _row_number, TYPE_INDEX);  
            // Initially set everything to "DEFAULT"
            setValueAt(DEFAULT_VALUE, _row_number, LOCAL_VALUE_INDEX);  

            setValueAt(_type, _row_number, DATA_TYPE_INDEX);  
    
            setValueAt(AlfiConstants.EMPTY, _row_number, LOCAL_COMMENT_INDEX);  

            setValueAt(AlfiConstants.EMPTY, _row_number, DEFAULT_VALUE_INDEX);  
       }
    }



        /** This method looks at the settings in a base nodes of m_node
          * and resets the defaults for any settings it finds.  _base_node will
          * always be one of the base nodes in the inheritance hierarchy between
          * m_node down to the root base node, but not including either.  E.g.,
          * m_node = A.B.C.x is based on B.C.x is based on C.x is based on x.
          * The calls to this method will be only for the intermediate base
          * nodes B.C.x and C.x to see if they have more "recent" settings than
          * the base node x to use as defaults.
          */
    private void adjustDefaultsDueToBaseNodeSettings(ALFINode _base_node) {
        

        if (m_node.isBasedOn(_base_node) && (! _base_node.isRootNode())) {
            String node_name = _base_node.generateFullNodePathName();
            NodeSettingIF settings[] = _base_node.settings_getLocal();

            for (int ii = 0; ii < settings.length; ii++) {
                String name = settings[ii].getName();
                String type = settings[ii].getType();
                if (isInputPortSetting(name)) { continue; }

                String value = settings[ii].getValue();

                displayDefaultSetting(node_name, name, type, value);
            }
        }
    }

        /** Updates the values shown in the default value and comment columns.*/
    private void displayDefaultSetting(String _node_name, String _name, 
                                       String _type, String _value) {
        String key = _name + _type;

        Integer table_index = (Integer) m_parameter_map.get(key);
        if (table_index != null) {
            int index = table_index.intValue();
            if (!_node_name.equals(m_node.generateFullNodePathName())) {
                setValueAt(_value, index, DEFAULT_VALUE_INDEX);
                String note = SET_IN + _node_name;
                setValueAt(note, index, DEFAULT_COMMENT_INDEX);
            }
        }
        else {
/*
            String warning = "ParameterTableModel.displayDefaultSetting(): " +
                "In node " + _node_name + ", a modification for " +
                "a setting named \"" + _parameter_name + "\" was found.  No " +
                "such setting exists in this node.  Setting ignored.";
            Alfi.warn(warning, false);
*/
        }
    }

    private boolean isInputPortSetting(String _setting_name) {
        PortProxy[] ports = m_node.ports_get();
        for (int i = 0; i < ports.length; i++) {
            if (        (ports[i].getIOType() == PortProxy.IO_TYPE_INPUT) &&
                        (ports[i].getName().equals(_setting_name)) ) {
                return true;
            }
        }
        return false;
    }

    public String getOriginalParameterValue (String _key) {
        return (String) m_original_param_values_map.get(_key);
    }

    private void setModifiedValues () {
        
        NodeSettingIF settings[] = m_node.settings_getLocal();
        
        for (int ii = 0; ii < settings.length; ii++) {
            String param_name = settings[ii].getName();
            String type = settings[ii].getType();
            String key = "";

            if (type != null) {
                key = param_name+type;
            }
            else {
                key = param_name;
            }
            String value = new String(settings[ii].getValue());
            if (value.length() != 0) {
                Integer table_index = (Integer) m_parameter_map.get(key);

                if ( table_index == null ) {
                    //System.err.println("cannot find parameter " + key);
                } 
                else {
                    // Store the original values of the parameter value mods
                    // and create a new entry in the map if the key does not
                    // exist
                    m_original_param_values_map.put(key, value);
                    int index = table_index.intValue();
                    this.setValueAt(value, index, LOCAL_VALUE_INDEX);
                }
            }
        }
        
    }

    private void getDeclarations () {

        if (m_node instanceof FUNCX_Node) { 
            FUNCX_Node funcx_node = (FUNCX_Node) m_node;
            m_funcx_variables = funcx_node.getRowDeclarations();
            return; 
        }

        if (m_row_declaration == null) {
            m_row_declaration = new ArrayList();
        }
        else { m_row_declaration.clear(); }

        if (m_declaration_location == null) {
            m_declaration_location = new ArrayList();
        }
        else { m_declaration_location.clear(); }
        
        ALFINode root_node = (m_node.isRootNode()) ? m_node :
                                                      m_node.baseNode_getRoot();
        String node_name = root_node.generateFullNodePathName();

        NodeSettingDeclarationIF[] declarations =
                                       root_node.settingDeclarations_getLocal();
        m_row_declaration.addAll(Arrays.asList(declarations));

        int ii = 0;
        for (; ii < m_row_declaration.size(); ii++) {
            m_declaration_location.add(ii, node_name);
        }
        
    }


    public ValueModification[] getDataChanges() {
        ArrayList value_list = new ArrayList();

        int num_rows = getRowCount();
        for (int ii = 0; ii < num_rows; ii++) {
            String name  = (String) m_header_name_list_model.getElementAt(ii);
            String type= (String) getValueAt(ii, DATA_TYPE_INDEX);
            String key = name+type;
            String value = (String) getValueAt(ii, LOCAL_VALUE_INDEX);
            value = value.trim();
            String comment = (String) getValueAt(ii, LOCAL_COMMENT_INDEX);
            if ( (value.length() > 0) && (! value.equals(getOriginalParameterValue(key)))) {
                ValueModification changed_value = 
                                   new ValueModification(name, value, comment);
                value_list.add(changed_value);
            }            
        }

        ValueModification[] value_mods_array = 
                                      new ValueModification[value_list.size()];
        value_list.toArray(value_mods_array);
        
        return value_mods_array;
    } 

    public boolean hasDefaultValue (int _row) {
        ValueAttributes flags = 
                            (ValueAttributes) m_modified_values_list.get(_row);
        return flags.hasDefaultValue();
    }

    public boolean isDefaultValue (int row, int column) {
        if (column == DEFAULT_VALUE_INDEX ||
            column == DEFAULT_COMMENT_INDEX) {
            return true;
        } else {
            ValueAttributes flags = 
                             (ValueAttributes) m_modified_values_list.get(row);
            return (! flags.isValueModified());
        }
    }

    public boolean isParameterType (int _row) {
        ValueAttributes flags = 
                            (ValueAttributes) m_modified_values_list.get(_row);
        return flags.isParameterType();
    }

    public boolean isInputPortType (int _row) {
        ValueAttributes flags = 
                            (ValueAttributes) m_modified_values_list.get(_row);
        return flags.isInputPortType();
    }
        

    public boolean isFileType (int _row) {
        ValueAttributes flags = 
                            (ValueAttributes) m_modified_values_list.get(_row);
        return flags.isFileType();
    }

        // By default forward all events to all the listeners. 
    public void tableChanged(TableModelEvent e) {
        fireTableChanged(e);
    }
    
    class ValueAttributes extends Object {
        public static final int PARAMETER_TYPE = 0;
        public static final int INPUT_PORT_TYPE = 1;
        public static final int FILE_TYPE = 2;

        private boolean m_value_modified;
        private boolean m_has_default_value;
        private int m_parameter_type;
        private String m_node_containing_default_value;
    
        public ValueAttributes (boolean _has_default_value) {
            this(false, _has_default_value, PARAMETER_TYPE);
        }

        public ValueAttributes (boolean _value_modified, 
                                boolean _has_default_value,
                                int _parameter_type){
            m_value_modified = _value_modified;
            m_has_default_value = _has_default_value;
            m_parameter_type = _parameter_type;
        }

        public void setValueModified (boolean _modified) {
            m_value_modified = _modified;
        }
        
        public boolean isValueModified () {
            return m_value_modified;
        }

        public boolean hasDefaultValue () {
            return m_has_default_value;
        }

        public void setDefaultValue (boolean _has_default) {
            m_has_default_value = _has_default;
        }
        
        public void setParameterType (int _type) {
            m_parameter_type = _type;
        }

        public boolean isParameterType () {
            return (m_parameter_type == PARAMETER_TYPE);
        }

        public boolean isInputPortType () {
            return (m_parameter_type == INPUT_PORT_TYPE);
        }
        public boolean isFileType () {
            return (m_parameter_type == FILE_TYPE);
        }
    } 
}
