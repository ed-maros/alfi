/* 
 * ColorEditor.java (compiles with releases 1.3 and 1.4) is used by 
 * TableDialogEditDemo.java.
 */

import javax.swing.AbstractCellEditor;
import javax.swing.table.TableCellEditor;
import javax.swing.JTextField;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JTable;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ParameterCellEditor extends AbstractCellEditor
                                 implements TableCellEditor,
			                                ActionListener {
    String m_current_setting;
    JTextField m_textfield;
    MultilineEditorDialog m_edit_dialog;

    protected static final String PARAMETER_EDIT = "parametercelleditor.edit";

    public ParameterCellEditor(Dialog _parent) {
        //Set up the editor (from the table's point of view),
        //which is a textfield.
        //It brings up the MultilineDialogEditor
        //This button brings up the MultilineEditorDialog
        //which is the editor from the user's point of view.
        m_textfield = new JTextField();

        //Set up the MultilineEditorDialog
        m_edit_dialog = new MultilineEditorDialog(_parent,  
                                                  this,    /* ok button handler */
                                                  false);  /* modal? */
    }

    /**
     * Handles events from the editor button and from
     * the dialog's OK button.
     */
    public void actionPerformed(ActionEvent e) {

        //System.out.println(e.toString());
        m_current_setting = m_edit_dialog.getValue();
    }

    //Implement the one CellEditor method that AbstractCellEditor doesn't.
    public Object getCellEditorValue() {
        return currentColor;
    }

    //Implement the one method defined by TableCellEditor.
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {
        currentColor = (Color)value;
        return button;
    }
}

