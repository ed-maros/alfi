package edu.caltech.ligo.alfi.editor;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.filechooser.*;
import javax.swing.border.*;
import javax.swing.tree.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.resource.*;
import edu.caltech.ligo.alfi.common.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.widgets.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;

/**    
  *
  * <pre>
  * The Editor Frame.
  * </pre>
  *
  * @author  Melody Araya
  * @version %I%, %G%
  * @see edu.caltech.ligo.alfi.common.AlfiFrame
  */
public class EditorFrame extends AlfiFrame
         implements EditorActionsIF, ProxyChangeEventListener,
                    GroupElementModEventListener,
                    NodeContentChangeEventListener,
                    NodeToBeDestroyedEventListener,
                    PortOrderChangeEventListener,
                    ComponentListener,
                    ConnectionRedrawnEventListener,
                    EditorWindowEventSource {

    public static final int COMMENT_LAYER       = 0;
    public static final int BACK_HIGHLIGHT_LAYER  = 1;
    public static final int CONNECTION_LAYER    = 2;
    public static final int JUNCTION_LAYER      = 3;
    public static final int BUNDLER_LAYER       = 4;
    public static final int MEMBER_NODE_LAYER   = 5;
    public static final int BORDER_LAYER        = 6;
    public static final int EXTERNAL_PORT_LAYER = 7;
    public static final int TOP_HIGHLIGHT_LAYER = 8;
    public static final int TEXT_LAYER          = 9;
    public static final int INVALID_LAYER_ID    = 10;

    public static final int GRID_SIZE = 1;

    public static final int DEFAULT_FONT_SIZE = 
                            AlfiFonts.getFontSize(AlfiFonts.FONT_LARGE);
    public static final int DEFAULT_FONT_COLOR = AlfiColors.ALFI_BLACK;
    public static final int DEFAULT_BOX_COLOR = AlfiColors.ALFI_YELLOW;
    public static final Dimension DEFAULT_BOX_SIZE = new Dimension(100, 100);

    public static final Point EXTERNAL_VIEW_WIDGET_OFFSET = new Point(50, 50);

    public static final String INTERNAL_VIEW = " - internal view";
    public static final String EXTERNAL_VIEW = " - external view";
    public static final String SHOW_ALWAYS = "Always show this warning.";
    public static final String DONT_SHOW = "Do not show this warning anymore.";

    private final ALFINode m_node;
    private final ALFIView m_view;
    private final JGoLayer[] m_layers;

    private NodePopupMenu m_node_menu = null;
    private PortPopupMenu m_port_menu = null;
    private ConnectionPopupMenu m_connection_menu = null;
    private JunctionPopupMenu m_junction_menu = null;
    private BundlerPopupMenu m_bundler_menu = null;
    private BoxFilesPopupMenu m_boxes_menu = null;
    private CommentPopupMenu m_comment_menu = null;
    private HighlightPopupMenu m_highlight_menu = null;

    private boolean m_internal_view = true;
    private Point m_previous_screen_position = null;

    private PortBorder[] m_port_borders = { new PortBorder(), new PortBorder(),
                                          new PortBorder(), new PortBorder() };

    private static AlfiClipboard ms_clipboard;

        /** To speed up the finding of connection widgets. */
    private HashMap m_connections_map;

        /** Used in conjunction with EditorWindowEventSource. */
    private HashMap m_editor_window_listeners_map;

    //**************************************************************************
    //********* Constructors ***************************************************

    public EditorFrame (AlfiMainFrame _parent, ALFINode _node) {
        this(_parent, _node, true);
    }

    public EditorFrame (AlfiMainFrame _parent, ALFINode _node, 
                                                     boolean _internal_view) {
        this(_parent, _node, _internal_view, true);
    }


    protected EditorFrame (AlfiMainFrame _parent, ALFINode _node, 
                           boolean _internal_view, boolean _add_to_node_cache) {

        super(_parent, new String(_node.generateFullNodePathName()), 
                (UIResourceBundle) ResourceBundle.getBundle(
                            "edu.caltech.ligo.alfi.resource.EditorResources"));

        m_node = _node;
        m_internal_view  = _internal_view;
        m_connections_map = new HashMap();

        m_view = new ALFIView(this);

            // create the layers of the document
        JGoDocument the_doc = m_view.getDocument();

            // suspend all view updates until document is full.
            // update restoration occurs in AlfiMainFrame.displayNodeWindow()
        the_doc.setSuspendUpdates(true);

        m_layers = new JGoLayer[INVALID_LAYER_ID];
        m_layers[0] = the_doc.getDefaultLayer();
        for (int i = 1; i < INVALID_LAYER_ID; i++) {
            m_layers[i] = the_doc.addLayerAfter(m_layers[i - 1]);
            m_layers[i].setIdentifier(new Integer(i));
        }

        StringBuffer new_title = new StringBuffer(getTitle()); 
        if (m_internal_view) {
            new_title.append(INTERNAL_VIEW);
        } else {
            new_title.append(EXTERNAL_VIEW);
        }
        m_title = new String(new_title);

        m_editor_window_listeners_map = new HashMap();

        setConnectionsOnTop(_parent.getConnectionsOnTopState());

        /////////////////////////////////////////////////////
        // If added as listener, be sure to remove it too! //
        /////////////////////////////////////////////////////

            // register this to listen to the associated node and its base nodes
        ALFINode node = m_node;
        while (node != null) {
            node.addPortOrderChangeListener(this);
            node = node.baseNode_getDirect();
        }

            // register this edit session to listen for content changes
        m_node.addNodeContentChangeListener(this);

            // register to listen if this session's node is to be destroyed
        m_node.addNodeToBeDestroyedEventListener(this);

            // listen to the base JFrame component for resize events
        addComponentListener(this);

            // listen for some window events
        addWindowListener(
            new WindowAdapter() {
                    // only hide window (until reopened or quit)
                public void windowClosing(WindowEvent _we) { 
                    setVisible(false); 
                }
                public void windowActivated(WindowEvent _we) { 
                    EditorWindowEvent activated_event = new EditorWindowEvent(
                                    EditorFrame.this,
                                    EditorWindowEvent.EDITOR_WINDOW_ACTIVATED);
                    fireEditorWindowEvent(activated_event);
                }
            });

        Alfi.getTheNodeCache().addEditSession(m_node, this);
    }

    //**************************************************************************
    //********* static methods *************************************************

        /** Clipboard should be set only once, by Alfi at startup. */
    public static void setClipboard(AlfiClipboard _clipboard) {
        if (ms_clipboard == null) { ms_clipboard = _clipboard; }
        else {
            Alfi.warn("Invalid call to EditorFrame.setClipboard():\n" +
                      "\t Clipboard already set.");
        }
    }

    public static AlfiClipboard getClipboard() { return ms_clipboard; }

    //**************************************************************************
    //********* methods ********************************************************

    /**
      * Create the ALFIView inside the Frame
      *
      * @see edu.caltech.ligo.alfi.editor.ALFIView
      */
    public void initializeFrame() {
        super.initializeFrame();

        setVisible(false);

        getWorkingPanel().add(m_view, BorderLayout.CENTER);

        setDefaultPopupMenu();

            // determine the requested size of the edit frame
        Dimension requested_frame_size = (m_internal_view) ?
            new Dimension(m_node.getFrameSize()) : ALFINode.DEFAULT_SCREEN_SIZE;

            // the minumum rectangle encompassing all fixed position components.
        Rectangle minimum_view_rect = m_view.determineMinimumRect();

        Dimension ideal_frame_size = determineIdealFrameSize(minimum_view_rect);

            // if the requested size is the standard default, adjust frame size
            // according to size needed for full display of view (unless it will
            // be too big for the screen.)
            //
            // do the same for pre 5.2.27 files.
        int[] new_size_implementation_version = { 5, 2, 27 };
        ALFIFile source_file = m_node.containerNode_getRoot().getSourceFile();
        boolean b_old_file = ! ((source_file == null) || source_file.
               fileVersionEqualOrGreaterThan(new_size_implementation_version));
        if (b_old_file ||
                   requested_frame_size.equals(ALFINode.DEFAULT_SCREEN_SIZE)) {
            if (!m_internal_view) {
                requested_frame_size = ideal_frame_size;
            }
        }

        Dimension final_size = new Dimension(requested_frame_size);

            // finally, the size of this window should never be smaller than
            // the window of this node's root base node's window.  this possible
            // change in size does not affect the saved size of the window.
        if (! m_node.isRootNode() && m_internal_view) {
            Dimension root_node_size = m_node.baseNode_getRoot().getFrameSize();
            final_size.width = Math.max(final_size.width, root_node_size.width);
            final_size.height =
                            Math.max(final_size.height, root_node_size.height);
        }


            // save the frame size if it is not the default (ideal) size
        Dimension frame_size_to_save = null;
        if (! requested_frame_size.equals(ideal_frame_size)) {
            frame_size_to_save = requested_frame_size;
        }

        if (m_internal_view) { 
            setDefaultWindowSize(final_size);
            resetSize();    // apply the current default window size
            m_node.setFrameSize(frame_size_to_save);
        }

            // set the paper color
        if (m_node.isPrimitive() && m_node.isRootNode()) {
            m_view.getDoc().setPaperColor(Color.lightGray);
        }
        else {
            m_view.getDoc().setPaperColor(Color.white);
        }

            // will this fit inside the frame without need for scrollbars?
        Dimension working_panel_size = getWorkingPanel().getSize();
        

        if ( (working_panel_size.getWidth() <= 0) || 
             (working_panel_size.getHeight() <= 0) ) {
            setDefaultWindowSize(final_size);
            resetSize();    // apply the current default window size
        }

        m_view.setViewPosition(minimum_view_rect.x, minimum_view_rect.y);

        if (m_internal_view) {
                // determine where the port borders should be
            Rectangle border_rect = new Rectangle(
                minimum_view_rect.x,
                minimum_view_rect.y,
                Math.max(minimum_view_rect.width, (working_panel_size.width -
                                     m_view.getVerticalScrollBar().getWidth())),
                Math.max(minimum_view_rect.height, (working_panel_size.height -
                                m_view.getHorizontalScrollBar().getHeight())) );
            replaceBorders(border_rect);
        }

        Point new_window_position = ((Component) this).getLocation();

        initializeViewObjects();

        requestFocus();
    }

    /**
      * Creates the view widgets 
      */ 
    private void initializeViewObjects () {
        if (m_internal_view) {
            initializeInternalView();
        }
        else {
            initializeExternalView();
        }
    }

    /**
      * Creates the internal view widgets 
      */ 
    private void initializeInternalView () {

            // add ports to port layer
        initializeInternalView_addPorts();

            // add colored boxes in the back layer
        initializeInternalView_addBottomColoredBoxes(); 

            // add member nodes to member nodes layer
        initializeInternalView_addMemberNodes();

            // add junctions to junction layer
        initializeInternalView_addJunctions();

            // add bundlers to bundler layer
        initializeInternalView_addBundlers();

            // add connections to connection layer
        initializeInternalView_addConnections();

            // add colored boxes in the top layer
        initializeInternalView_addTopColoredBoxes(); 

            // add text comments 
        initializeInternalView_addTextComments(); 

            // initialize the groupings
        initializeInternalView_registerGroups();
    }

    /**
      * Creates the external view widgets 
      */ 
    private void initializeExternalView () {
        JGoLayer layer_to_add_to = m_layers[MEMBER_NODE_LAYER];

        MemberNodeWidget mnw = new MemberNodeWidget(this, m_node, true);
        mnw.initialize();

        layer_to_add_to.addObjectAtTail(mnw);
    }

    public void centerFrame ( ) {
        Dimension screen_size = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension this_size = getSize();
        setBounds((screen_size.width - this_size.width)/2,
                   (screen_size.height - this_size.height)/2,
                   this_size.width, this_size.height);
 
    }
    /**
      * Gets the center location of the view
      */
    private Point getCenterLocation (Dimension _node_size) {
        // Center the node within the view
        double node_center_x = _node_size.getWidth()/2;
        double node_center_y = _node_size.getHeight()/2;
        Dimension view_size = getWorkingPanel().getSize();
        double x = (view_size.getWidth()/2) - node_center_x; 
        double y = (view_size.getHeight()/2) - node_center_y;

        return new Point((int)x, (int)y);
    }

    private ALFINode getBaseNode (ALFINode _node, boolean _root_node) {
        
        ALFINode base_node = _node;

        if (!_node.isRootNode()){
            if (_root_node) {
                base_node = _node.baseNode_getRoot();
            }
            else {
                base_node = _node.baseNode_getDirect();
            }
        }

        return base_node;
 
    }

    public boolean isInternalView () {
        return m_internal_view;
    }

    public String getName () {
        if (m_node == null) {
            return getTitle();
        } else {
            return m_node.determineLocalName();
        }
    }

    public ALFINode getNodeInEdit () { return m_node; }

    public ALFIView getView () { return m_view; }

    public boolean isModifiable () {
        if (!m_internal_view) {
            // External view is never modifiable
            return false;
        }
        else {
            JGoObject current_widget =
                                   m_view.getSelection().getPrimarySelection();

            if (current_widget instanceof MemberNodeWidget) {
                return ((MemberNodeWidget)current_widget).isModifiable();
            }
            return true;
        }
    }    

    public void setConnectionsOnTop(boolean _b_put_connections_on_top) {
        JGoDocument doc = m_view.getDocument();

            // comment layer is usually last, but might have been moved up
        boolean b_comments_last =
                    (doc.getFirstLayer() == m_layers[COMMENT_LAYER]);

        if (_b_put_connections_on_top) {
            doc.bringLayerToFront(m_layers[CONNECTION_LAYER]);
            doc.bringLayerToFront(m_layers[JUNCTION_LAYER]);
            doc.bringLayerToFront(m_layers[BUNDLER_LAYER]);
        }
        else {
            doc.sendLayerToBack(m_layers[BUNDLER_LAYER]);
            doc.sendLayerToBack(m_layers[JUNCTION_LAYER]);
            doc.sendLayerToBack(m_layers[CONNECTION_LAYER]);
            if (b_comments_last) {
                doc.sendLayerToBack(m_layers[COMMENT_LAYER]);
            }
        }
        m_view.getSelection().clearSelection();
    }

    /**
      * Initializes the Editor Actions
      *
      * @see edu.caltech.ligo.alfi.editor.EditorActions
      */
    protected Actions initializeActions () {
        m_actions = new EditorActions(getResources(), this);

        BasicActionIF action =
                      (BasicActionIF) m_actions.getAction(EditorActionsIF.CUT);
        action.setEnabled(false);

        action = (BasicActionIF) m_actions.getAction(EditorActionsIF.COPY);
        action.setEnabled(false);

        action = (BasicActionIF) m_actions.getAction(EditorActionsIF.PASTE);
        action.setEnabled(false);

        action = (BasicActionIF) m_actions.getAction(EditorActionsIF.DELETE);
        action.setEnabled(false);

        action = (BasicActionIF) m_actions.getAction(EditorActionsIF.DUPLICATE);
        action.setEnabled(false);

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.NEW_BUNDLER);
        if (m_internal_view) { action.setEnabled(true); }
        else { action.setEnabled(false); }

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.EDIT_COMMENT);
        action.setEnabled(true);

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.EDIT_MACROS);
        action.setEnabled(true);

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.BOX_SETTINGS);
        action.setEnabled(m_node.isRootNode() ||
                        m_node.baseNode_getRoot().linkDeclarations_hasLocal());

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.VIEW_EXTERNAL);
        if (m_internal_view) { action.setEnabled(true); }
        else { action.setEnabled(false); }

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.VIEW_INTERNAL);
        if (m_internal_view) { action.setEnabled(false); }
        else { action.setEnabled(true); }

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.RESET_VIEW);
        action.setEnabled(true);

        action = (BasicActionIF)
                         m_actions.getAction(EditorActionsIF.VALIDATE_BUNDLES);
        if (m_internal_view) { action.setEnabled(true); }
        else { action.setEnabled(false); }

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.SMOOTH_CONNS);
        if (m_internal_view) { action.setEnabled(true); }
        else { action.setEnabled(false); }

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.TRIM_CONNS);
        if (m_internal_view) { action.setEnabled(true); }
        else { action.setEnabled(false); }

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.VIEW_BASE_BOX);
        if (m_node.isRootNode()) { action.setEnabled(false); }
        else { action.setEnabled(true); }

        action = (BasicActionIF) m_actions.getAction(EditorActionsIF.SHOW_MAIN);
        action.setEnabled(true);

        action = (BasicActionIF)
                      m_actions.getAction(EditorActionsIF.EXPORT_MODELER_FILE);
        if (m_node.isBox() && m_node.isRootNode()) { action.setEnabled(true); }
        else { action.setEnabled(false); }

        action = (BasicActionIF) m_actions.getAction(EditorActionsIF.SAVE);
        if (m_node.isBox() && m_node.isRootNode()) { action.setEnabled(true); }
        else { action.setEnabled(false); }

        action =
            (BasicActionIF) m_actions.getAction(EditorActionsIF.SAVE_COPY_AS);
        if (m_node.isBox() && m_node.isRootNode()) { action.setEnabled(true); }
        else { action.setEnabled(false); }

        action = (BasicActionIF) m_actions.getAction(EditorActionsIF.RELOAD);
        if (m_node.isRootNode()) { action.setEnabled(true); }
        else { action.setEnabled(false); }

        action = (BasicActionIF) m_actions.getAction(EditorActionsIF.CLOSE);
        action.setEnabled(true);

        action =
              (BasicActionIF) m_actions.getAction(EditorActionsIF.LOCATE_FILE);
        action.setEnabled(true);

        action = (BasicActionIF) m_actions.getAction(EditorActionsIF.NODE_INFO);
        action.setEnabled(true);

        return m_actions;
    }

    /**
      * Initializes the command popup menu
      *
      * @see edu.caltech.ligo.alfi.editor.CommandPopupMenu
      */
    protected void setDefaultPopupMenu () {
        m_popup_menu = (JPopupMenu) new CommandPopupMenu (getActions());
        return;
    }

    public JPopupMenu getPrimitivesMenu () {
        AlfiMainFrame parent = (AlfiMainFrame)getParentFrame();
        return parent.getPrimitivesMenu();
    }

    public boolean isToolbarSelected () {
        AlfiMainFrame parent = (AlfiMainFrame)getParentFrame();
        return parent.isToolbarSelected();
    }
      
    /**
      * Initializes the box files menu.   The menu contains the box files
      * in the directory of the container node.
      *
      * @see edu.caltech.ligo.alfi.common.BoxFilesPopupMenu
      */
    public BoxFilesPopupMenu getBoxFilesMenu () {
        String alfi_path = new String(".");
        ALFIFile alfi_file = m_node.getSourceFile();
        if (alfi_file == null) {
            alfi_file = m_node.baseNode_getRoot().getSourceFile();
        }

        try {
            alfi_path = alfi_file.getCanonicalDirectory().getCanonicalPath();
        } catch (IOException ex) {
            System.err.println(ex);
        }

        if (m_boxes_menu != null) {
            m_boxes_menu.cleanUp(); 
        }
        m_boxes_menu = new BoxFilesPopupMenu(
                (AlfiMainFrame) getParentFrame(), alfi_path);

        return m_boxes_menu;
    }
   
    /**
      * Returns the ALFINode that is being modified.  If the action
      * was performed on the background, the container node is
      * returned.  Otherwise, it's the child node that is returned.
      *
      * @see edu.caltech.ligo.alfi.bookkeeper.ALFINode
      */
    private ALFINode determineNode() {
        ALFINode node = m_view.getMemberNodeAssociatedWithMostRecentClick();
        if (node == null) { node = m_node; }
        return node;
    }

        // Port Actions

        /** Called via a PortBorder click.  _raw_add_position refers to the
          * a port position on the _edge which has not yet been adjusted to take
          * inherited port positions into account.
          */
    void addPort(int _edge, int _raw_add_position) {
        String name = createTemporaryPortName();
        int data_type = GenericPortProxy.DATA_TYPE_REAL;
        int io_type = ((_edge == PortProxy.EDGE_WEST) ||
                       (_edge == PortProxy.EDGE_NORTH)) ?
                            PortProxy.IO_TYPE_INPUT : PortProxy.IO_TYPE_OUTPUT;

        int local_add_position = _raw_add_position;
        if (! m_node.isRootNode()) {
            PortProxy[][] inheritedPortPositions =
                         m_node.baseNode_getDirect().ports_getByPosition(true);
            local_add_position -= inheritedPortPositions[_edge].length;
            if (local_add_position < 0) { local_add_position = 0; }
        }

        int[] position = { _edge, local_add_position,
                           _edge, PortProxy.EDGE_POSITION_INVALID };

        PortProxy port = m_node.port_add(name, io_type, data_type, position);
        PortWidget port_widget = determinePortWidget(port, null);

        m_view.getSelection().selectObject(port_widget);

        boolean b_ok_selected = modifyPort();
        if (! b_ok_selected) {
            deletePort(port_widget);
        }

        m_view.getSelection().clearSelection();
    }

    private String createTemporaryPortName() {
        PortWidget[] port_widgets = getPortWidgets_onContainer();
        HashMap name_map = new HashMap();
        for (int j = 0; j < port_widgets.length; j++) {
            name_map.put(port_widgets[j].getAssociatedProxy().getName(), null);
        }

        int i = 0;
        String prefix = "port_";
        String name = prefix + i++;
        while (name_map.containsKey(name)) { name = prefix + i++; }

        return name;
    }

        /** Returns false if Cancel chosen in dialog. */
    public boolean modifyPort () {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();

        if (current_widget instanceof PortWidget) {
            PortWidget port_widget = (PortWidget) current_widget;
            PortProxy port_proxy = port_widget.getAssociatedProxy();

            PortPropertiesDialog modify_dialog =
                    new PortPropertiesDialog(this, port_proxy);
            modify_dialog.pack();
            m_view.update(m_view.getGraphics());    // kluge to remove popup
            centerDialog(modify_dialog);
            modify_dialog.setVisible(true);

            if (modify_dialog.getExitStatus() == PortPropertiesDialog.OK) {

                String new_name = modify_dialog.getPortName();
                int new_io_type = modify_dialog.getPortIOType();

                    // name changed?
                if (port_proxy.getName().equals(new_name)) {
                    new_name = null;    // signals no change
                }

                if (new_name != null) {
                    if (! m_node.port_nameAndSameIOExists(new_name,
                                                          new_io_type)) {
                        port_proxy.setName(new_name);
                    }
                    else {
                        JOptionPane.showMessageDialog(this,
                            "Port name \"" + new_name + "\" already taken.\n\n"+
                            "Please choose another port name.");
                        modifyPort();
                    }
                }

                    // I/O status changed?
                if (new_io_type != port_proxy.getIOType()) {
                    if (checkForInternalConnections(port_proxy) ||
                                     checkForExternalConnections(port_proxy)) {
                        new_io_type = PortProxy.IO_TYPE_INVALID;
                    }

                    if (new_io_type != PortProxy.IO_TYPE_INVALID) {
                        String name = (new_name != null) ?
                                               new_name : port_proxy.getName();
                        if (! m_node.port_nameAndSameIOExists(name,
                                                              new_io_type)) {
                            port_proxy.setIOType(new_io_type);
                        }
                        else {
                            JOptionPane.showMessageDialog(this,
                                "Port name \"" + name + "\" already taken.\n\n"+
                                "Please choose another port name.");
                            modifyPort();
                        }
                    }
                }

                    // data type changed?
                int new_type = modify_dialog.getPortDataType();
                int current_type = port_proxy.getDataType();
                if (new_type != current_type) {
                    if (checkForInternalConnections(port_proxy) ||
                                     checkForExternalConnections(port_proxy)) {
                        new_type = PortProxy.DATA_TYPE_INVALID;
                    }

                    if (new_type != PortProxy.DATA_TYPE_INVALID) {
                            // deal with PortProxy <-> BundlePortProxy switches
                        if ((current_type == PortProxy.DATA_TYPE_ALFI_BUNDLE) ||
                                (new_type == PortProxy.DATA_TYPE_ALFI_BUNDLE)) {

                            String name = port_proxy.getName();
                            int io_type = port_proxy.getIOType();
                            int data_type = new_type;
                            int[] position = port_proxy.getPosition();

                            deletePort(port_widget);

                            port_proxy = m_node.port_add(name, io_type,
                                                          data_type, position);
                        }
                        else { port_proxy.setDataType(new_type); }
                    }
                }

                boolean b_port_position_modified = false;
                int[] port_position = port_proxy.getPosition();
                    // internal view orientation changed?
                int internal_edge = modify_dialog.getPortInternalOrientation();
                if (internal_edge != port_position[0]) {
                    port_position[0] = internal_edge;
                    int edge_boundary;
                    if ( internal_edge == PortProxy.EDGE_NORTH ||
                         internal_edge == PortProxy.EDGE_SOUTH ) {
                        edge_boundary = ( int ) m_port_borders[internal_edge].getWidth();
                    }
                    else {
                        edge_boundary =  ( int ) m_port_borders[internal_edge].getHeight();
                    }
                        
                    if ( (port_position[1] * (PortWidget.PORT_WIDTH + 2))  >= 
                         (edge_boundary - (2 * PortWidget.PORT_WIDTH))){
                        Point edge_point = new Point(0,0);
                        switch (internal_edge) {
                            case PortProxy.EDGE_NORTH:
                                edge_point = 
                                m_port_borders[internal_edge].getSpotLocation(
                                                            JGoObject.TopRight);
                                break;
                            case PortProxy.EDGE_SOUTH:
                                edge_point = 
                                m_port_borders[internal_edge].getSpotLocation(
                                                         JGoObject.BottomRight);
                                break;
                            case PortProxy.EDGE_EAST:
                                edge_point = 
                                m_port_borders[internal_edge].getSpotLocation(
                                                         JGoObject.BottomRight);
                                break;
                            case PortProxy.EDGE_WEST:
                                edge_point = 
                                m_port_borders[internal_edge].getSpotLocation(
                                                          JGoObject.BottomLeft);
                                break;
                        }
                        port_position[1] = getClosestPortBorder(edge_point);
                    }

                    if ( (port_position[1] * (PortWidget.PORT_WIDTH + 2))  >= (edge_boundary - (2 * PortWidget.PORT_WIDTH))){
                        resetView();
                    }
                    b_port_position_modified = true;
                }
                    // external view orientation changed?
                int external_edge = modify_dialog.getPortExternalOrientation();
                if (external_edge != port_position[2]) {
                    port_position[2] = external_edge;
                    b_port_position_modified = true;
                }

                if (b_port_position_modified) {
                    port_proxy.setPosition(port_position);
                }

                return true;
            }
        }
        return false;
    }

        /**
         * Gives a warning if internal connections in m_node are present on
         * this port.
         */
    private boolean checkForInternalConnections(PortProxy _port) {
        if (_port.hasConnections(m_node, null)) {
            String title = "Internal Connections Exist";
            String message =
                "Neither the I/O type nor the data type of an already\n" +
                "connected port may be changed.  You must first delete\n" +
                "all connections to this port before changing the port's\n" +
                "I/O type or its data type.";
            JOptionPane.showMessageDialog(this, message, title,
                                          JOptionPane.INFORMATION_MESSAGE);
            return true;
        }
        else { return false; }
    }

        /** Returns false if no such connections exist. */
    private boolean checkForExternalConnections(PortProxy _port) {
        ConnectionProxy[] connections = _port.getExternalConnections();
        ALFINode[] nodes_to_open = null;
        if (connections.length > 0) {
            HashMap node_map = new HashMap();
            for (int i = 0; i < connections.length; i++) {
                node_map.put(connections[i].getDirectContainerNode(), null);
            }
            Set keys = node_map.keySet();
            nodes_to_open = new ALFINode[keys.size()];
            keys.toArray(nodes_to_open);

            String title = "External Connections Exist";
            String message =
                "The following nodes contain external connections\n" +
                "to the port being modified:\n\n";
            for (int i = 0; i < nodes_to_open.length; i++) {     
                message += nodes_to_open[i] + "\n";
            }
            message += "\n\n" +
                "The connections to this port in these nodes must\n" +
                "be removed before these modifications are allowed\n" +
                "on this port.\n\n" +
                "Do you wish to open edit windows for these node(s)?";

            int response = JOptionPane.showConfirmDialog(this, message, title,
                       JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (response == JOptionPane.YES_OPTION) {
                ((AlfiMainFrame) getParentFrame()).show(nodes_to_open,
                                                                   null, null);
            }
            return true;
        }
        else { return false; }
    }

    public void deletePort () {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();
        if (current_widget != null) { deletePort(current_widget); }
    }

    public void deletePort (JGoObject _widget) {
        m_view.update(m_view.getGraphics());    // kluge to remove popup

        if (_widget instanceof PortWidget) {
            PortWidget port_widget = (PortWidget) _widget;
            PortProxy port = port_widget.getAssociatedProxy();

                // if any nodes that are derived from m_node have a _local_
                // connection to this port, then don't allow port to be deleted
            ALFINode offending_node = null;
            ConnectionProxy offending_connection = null;
            ALFINode[] derived_nodes = m_node.derivedNodes_getAll();
            for (int i = 0; i < derived_nodes.length; i++) {
                ConnectionProxy[] local_connections =
                                derived_nodes[i].connections_getLocal();
                for (int j = 0; j < local_connections.length; j++) {
                    if ((local_connections[j].getSource() == port) ||
                                    (local_connections[j].getSink() == port)) {
                        offending_connection = local_connections[j];
                        offending_node = derived_nodes[i];
                        break;
                    }
                }
                if (offending_connection != null) { break; }
            }

                // if no problem found so far, check the containers of all
                // derived nodes to see if there is a local connection made
                // to this port as it exists on the associated MemberNodeProxy
                // of the derived node.
            if (offending_connection == null) {
                for (int i = 0; i < derived_nodes.length; i++) {
                    MemberNodeProxy mnp =
                           derived_nodes[i].memberNodeProxy_getAssociated();
                    ALFINode container_node_of_mnp =
                           derived_nodes[i].containerNode_get();
                    ConnectionProxy[] local_connections =
                           container_node_of_mnp.connections_getLocal();
                    for (int j = 0; j < local_connections.length; j++) {
                        ConnectionProxy c = local_connections[j];
                        if (         ((c.getSource() == port) &&
                                      (c.getSourceMember() == mnp)) ||
                                     ((c.getSink() == port) &&
                                      (c.getSinkMember() == mnp)) ) {
                            offending_connection = c;
                            offending_node = container_node_of_mnp;
                            break;
                        }
                    }
                    if (offending_connection != null) { break; }
                }
            }

                // finally, check the container node of m_node (if such
                // exists) for the same situation.
            if ((offending_connection == null) && (! m_node.isRootNode())) {
                MemberNodeProxy mnp = m_node.memberNodeProxy_getAssociated();
                ALFINode container_node = m_node.containerNode_get();
                ConnectionProxy[] local_connections =
                            container_node.connections_getLocal();
                for (int j = 0; j < local_connections.length; j++) {
                    ConnectionProxy c = local_connections[j];
                    if (         ((c.getSource() == port) &&
                                  (c.getSourceMember() == mnp)) ||
                                 ((c.getSink() == port) &&
                                  (c.getSinkMember() == mnp)) ) {
                        offending_connection = c;
                        offending_node = container_node;
                        break;
                    }   
                }   
            }

                // if a connection was found, alert user and disallow delete
            if (offending_connection != null) {
                String container = offending_node.generateFullNodePathName();
                String message =
                    "Currently, the node " + container + " contains a locally "+
                    "added connection (" + offending_connection + ") which " +
                    "connects to this port.\nSuch connections must first be " +
                    "deleted by the user before the port may be deleted.\n\n" +
                    "Do you wish to open an edit session for node " +
                    container + "?";
                String title = "Port Has Outside Connection";
                int response = JOptionPane.showConfirmDialog(this, message,
                                          title, JOptionPane.YES_NO_OPTION);
                if (response == JOptionPane.YES_OPTION) {
                    ((AlfiMainFrame) getParentFrame()).show(offending_node,
                                                                   null, true);
                }

                return;
            }

                // jumped through all the hoops.  remove the port.
            m_node.port_remove(port);
        }
    }

    public void renamePort () {
            // this is an operation which requires that all ProxyChangeEvent
            // listeners are finished registering.  check this.
        if (Alfi.getTheProxyListenerRegistrar().isBusy()) {
            String title = "Waiting . . .";
            String message = "Port renaming requires that all loaded boxes " +
                "and primitives have completed registration as listeners " +
                "for such changes.  This process can take several minutes " +
                "after the loading of complex systems, and takes place in " +
                "the background because few operations require such " +
                "notification.  In the present case, this registration has " +
                "not yet completed (it is " +
                Alfi.getTheProxyListenerRegistrar().getPercentComplete() +
                "% completed.)  Please try again after a short wait.";
            message = TextFormatter.formatMessage(message);
            JOptionPane.showMessageDialog(this, message, title,
                                           JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();

        if (current_widget instanceof PortWidget) {
            PortWidget port_widget = (PortWidget) current_widget;
            PortProxy port_proxy = port_widget.getAssociatedProxy();
            String current_name = port_proxy.getName();
            int io_type = port_proxy.getIOType();

            EntryDialog rename_dialog = new EntryDialog(this, "Rename Port",
                                                        port_proxy.getName());
            rename_dialog.pack();
            m_view.update(m_view.getGraphics());    // kluge to remove popup
            centerDialog(rename_dialog);
            rename_dialog.setVisible(true);

            String new_name = rename_dialog.getValidatedText();
            if (new_name != null) {
                if (! new_name.equals(current_name)) {
                    if (! m_node.port_nameAndSameIOExists(new_name, io_type)) {
                        port_proxy.setName(new_name);
                        Date this_node_modified_time = 
                                                  m_node.getTimeLastModified(); 
                        
                            // Make sure that the derived nodes get the
                            // portname change
                        propogateChangeToDerivedNodesWhereNeeded();
                    }
                    else {
                        JOptionPane.showMessageDialog(this,
                            "Port name \"" + new_name + "\" already taken.\n\n"+
                            "Please choose another port name.");
                        renamePort();
                    }
                }
            }
        }
    }

    protected boolean portNameIsValid (String _name) {
        if (        (_name == null) ||
                    (_name.length() == 0) ||
                    (_name.indexOf(' ') >= 0)) {
            return false;
        }
        else { return true; }
    }

    public void importChannels() {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();
        m_view.update(m_view.getGraphics());    // kluge to remove popup

        if (current_widget instanceof PortWidget) {
            BundlePortProxy bundle_port = (BundlePortProxy)
                            ((PortWidget) current_widget).getAssociatedProxy();

            ALFINode[] derived_instances = m_node.derivedNodes_getAll();
            Object[] choices = new Object[derived_instances.length + 1];
            choices[0] = "Erase Default Channels";
            System.arraycopy(derived_instances, 0, choices, 1,
                                                     derived_instances.length);
            String title = "Import Channels From Node";
            if (derived_instances.length > 0) {
                String message= "Import the data channel names from this node:";
                Object choice = JOptionPane.showInputDialog(this, message,
                                           title, JOptionPane.QUESTION_MESSAGE,
                                           null, choices, choices[0]);
                if (choice != null) {
                    if (choice instanceof ALFINode) {
                        ALFINode instance_to_copy_from = (ALFINode) choice;
                        bundle_port.setDefaultChannelMap(instance_to_copy_from);
                    }
                        // clears the default map
                    else { bundle_port.setDefaultChannelMap(new HashMap()); }

                        // update downstream content caches
                    bundle_port.doContentUpdate(m_node, null);
                }
            }
            else {
                String message = "No derived instances of nodes containing\n" +
                                 "this bundle port are currently loaded.";
                JOptionPane.showMessageDialog(this, message, title,
                                                    JOptionPane.PLAIN_MESSAGE);
            }
        }
    }

    // Connection actions

        /** Adds a new connection. */
    public void addConnection(ConnectionBuildingWidget _builder) {
        GenericPortWidget source_widget = _builder.getSourceWidget();
        GenericPortProxy source = source_widget.getAssociatedGenericPortProxy();
        MemberNodeProxy source_member = source_widget.getOwnerMemberProxy();

        GenericPortWidget sink_widget = _builder.getSinkWidget();
        GenericPortProxy sink = sink_widget.getAssociatedGenericPortProxy();
        MemberNodeProxy sink_member = sink_widget.getOwnerMemberProxy();

        Vector path_points_vector = _builder.getCorrectedPathPoints();

        Integer[][] path_definition_and_hint =
                ConnectionProxy.pathPointsToPathDefinition(path_points_vector);
        Integer[] path_definition = path_definition_and_hint[0];
        int dog_leg_hint = path_definition_and_hint[1][0].intValue();

            // null unless source or sink is a bundler
        TerminalBundlersConfiguration bundlers_config =
                                     _builder.generateTerminalBundlersConfig();


        addConnection(source, source_member, sink, sink_member, path_definition,
                           dog_leg_hint, _builder.isBundle(), bundlers_config);

    }

    public void addConnection(GenericPortProxy _source,
                      MemberNodeProxy _source_member, GenericPortProxy _sink,
                      MemberNodeProxy _sink_member, Integer[] _path_definition,
                      int _dog_leg_hint, boolean _b_is_bundle,
                      TerminalBundlersConfiguration _bundlers_config) {


            // correct data flow in a network of junctions if needed
        if (_sink instanceof JunctionProxy) {
            JunctionProxy sink_junction = (JunctionProxy) _sink;
            if (sink_junction.getSourceConnection(m_node) != null) {
                ((JunctionWidget) determineJunctionWidget(sink_junction)).
                                            prepareForSourceConnectionChange();
            }
        }

        ConnectionProxy connection = m_node.connection_add(_source,
                             _source_member, _sink, _sink_member,
                             _path_definition, _b_is_bundle, _bundlers_config);

        if (_dog_leg_hint != ConnectionProxy.DOG_LEG_HINT_NONE) {
            connection.setDogLegHint(_dog_leg_hint, true);
        }

        assert(connection != null);

        if (((AlfiMainFrame) getParentFrame()).isAutoTrimConnections()) {
            trimConnectionPath(determineConnectionWidget(connection));
        }

            // adding connection may have caused bundle content changes
        connection.doContentUpdate(m_node);
    }


        /** Cleans up all connection paths in the window, placing junctions
          * wherever needed to connect overlapping connections.
          *
          * Ignores inherited connections.
          */
    public void trimAllConnectionPaths() {
            // remove all local junctions (those with source connections)
        JunctionProxy[] local_junctions = m_node.junctions_getLocal();
        for (int i = 0; i < local_junctions.length; i++) {
            if (local_junctions[i].getSourceConnection(m_node) != null) {
                deleteJunction(determineJunctionWidget(local_junctions[i]));
            }
        }

        PortWidget[] pws = getPortWidgets_all();
        for (int i = 0; i < pws.length; i++) {
            if (pws[i].isSourcePort()) {
                trimConnectionPaths(pws[i], false);
            }
        }

        BundlerWidget[] bws = getBundlerWidgets();
        for (int i = 0; i < bws.length; i++) {
            trimConnectionPaths(bws[i], false);
        }
    }

        /** This method trims the path of the connection clicked upon.  See
          * trimConnectionPath(ConnectionWidget _cw) for more detail.
          *
          * Ignores inherited connections.
          */
    public void trimConnectionPath() {
        JGoObject o = m_view.pickDocObject(
                                  m_view.getLastMouseDownLocation_dc(), false);
        if (o instanceof ConnectionWidget) {
            trimConnectionPath((ConnectionWidget) o);
        }
    }

    private void trimConnectionPath(ConnectionWidget _cw_to_trim) {
        assert(_cw_to_trim != null);

        if (! _cw_to_trim.isLocal()) { return; }

        GenericPortWidget origin_widget = determineOriginWidget(_cw_to_trim);
        if (origin_widget == null) { return; }

        trimConnectionPaths(origin_widget, true);
    }

        /** Cleans up connection paths having _origin_widget as their origin.
          * _b_remove_junctions is used to increase efficiency, and should
          * always be set to true unless it is known all local junctions
          * associated with _origin_widget have already been removed.
          */
    private void trimConnectionPaths(GenericPortWidget _origin_widget,
                                                 boolean _b_remove_junctions) {

            // do not trim connections from bundlers
        if (_origin_widget instanceof BundlerWidget) { return; }

        userWait(true);

        GenericPortProxy origin= _origin_widget.getAssociatedGenericPortProxy();
        MemberNodeProxy origin_member= _origin_widget.getOwnerMemberProxy();

        if (_b_remove_junctions) {
                // remove any local junctions associated with this origin
            JunctionProxy[] local_junctions = m_node.junctions_getLocal();
            for (int i = 0; i < local_junctions.length; i++) {
                ConnectionProxy source_connection =
                                local_junctions[i].getSourceConnection(m_node);
                if (source_connection == null) { continue; }

                if ((source_connection.getOriginMember(m_node)==origin_member)&&
                             (source_connection.getOrigin(m_node) == origin)) {
                    deleteJunction(determineJunctionWidget(local_junctions[i]));
                }
            }
        }

            // trim all connections with this origin
        ConnectionWidget[] cws = _origin_widget.getConnections_local();
        Point[] intersections = (cws.length > 1) ?
                      ConnectionWidget.getAllIntersections(cws) : new Point[0];
        for (int j = 0; j < intersections.length; j++) {
            cws = findConnections(_origin_widget, intersections[j]);
            if ((cws != null) && (cws.length > 1)) {
                joinConnections(cws, _origin_widget, intersections[j]);
            }
        }

        userWait(false);
    }

        /** This method takes an array of local connection widgets, all of which
          * have a common intersection and origin, and sets their source and
          * paths to originate from a local junction at that point (newly added
          * if needed.)  If a newly added junction is used, a source connection
          * is also added using the original path of the first connection in
          * the array.
          */
    private void joinConnections(ConnectionWidget[] _cws,
                   GenericPortWidget _origin_widget, Point _new_source_point) {

            // ignore if _origin_widget from point is same as _new_source_point
        if (_origin_widget.getFromLinkPoint().equals(_new_source_point)) {
            return;
        }

        int cw_processing_index = 0;

            // find or create a junction at _new_source_point
        JunctionProxy new_source = null;
        JunctionWidget[] existing_junction_widgets =
                              findJunctions(_origin_widget, _new_source_point);
        for (int i = 0; i < existing_junction_widgets.length; i++) {
            JunctionProxy junction =
                             existing_junction_widgets[i].getAssociatedProxy();
            if (m_node.junction_containsLocal(junction)) {
                new_source = junction;
                break;
            }
        }
        if (new_source == null) {
            new_source = addJunction(_new_source_point, _cws[0]);
            cw_processing_index = 1;    // 0th connection already processed now
        }

            // process remaining connection sources and paths
        JunctionWidget source_jw = determineJunctionWidget(new_source);
        for (int i = cw_processing_index; i < _cws.length; i++) {
            joinConnection(_cws[i], source_jw);
        }
    }

        /** This method joins a connection to a junction. */
    private void joinConnection(ConnectionWidget _cw, JunctionWidget _jw) {
        JunctionProxy junction = _jw.getAssociatedProxy();
        ConnectionProxy connection = _cw.getAssociatedProxy();

        Vector[] split_path_points = 
                               _cw.determineSplitPaths(junction.getLocation());
        Integer[][] downstream_definition_and_hint =
               ConnectionProxy.pathPointsToPathDefinition(split_path_points[1]);
        Integer[] downstream_definition = downstream_definition_and_hint[0];
        int dog_leg_hint = downstream_definition_and_hint[1][0].intValue();

        GenericPortWidget old_source_widget = _cw.getSourceWidget();
        connection.setSource(m_node, junction, null, downstream_definition);
        connection.setDogLegHint(dog_leg_hint, true);

            // check original source junctions for obsolescence
        if (old_source_widget instanceof JunctionWidget) {
            JunctionWidget old_jw = (JunctionWidget) old_source_widget;
            JunctionProxy old_junction = old_jw.getAssociatedProxy();
            ConnectionProxy[] connections =
                                     old_junction.getConnections(m_node, null);
            if (old_junction.getSourceConnection(m_node) != null) {
                if (connections.length < 3) { deleteJunction(old_jw); }
            }
            else if (connections.length < 1) { deleteJunction(old_jw); }
        }
    }

    private GenericPortWidget determineOriginWidget(ConnectionWidget _cw) {
        ConnectionProxy connection = _cw.getAssociatedProxy();
        GenericPortProxy origin = connection.getOrigin(m_node);
        MemberNodeProxy origin_member = connection.getOriginMember(m_node);
        return determineGenericPortWidget(origin, origin_member);
    }

        /** This method finds any ConnectionWidgets in window who's data
          * originates from _origin_port_widget.
          */
    private ConnectionWidget[] findConnections(
                                            GenericPortWidget _origin_widget) {
        assert (_origin_widget != null);

        GenericPortProxy origin= _origin_widget.getAssociatedGenericPortProxy();
        MemberNodeProxy origin_member = _origin_widget.getOwnerMemberProxy();

        ConnectionWidget[] cws = getConnectionWidgets();
        ArrayList cw_list = new ArrayList();
        for (int i = 0; i < cws.length; i++) {
            ConnectionProxy connection = cws[i].getAssociatedProxy();
            if (       (connection != null) &&
                       (connection.getOrigin(m_node) == origin) &&
                       (connection.getOriginMember(m_node) == origin_member)) {
                cw_list.add(cws[i]);
            }
        }

        ConnectionWidget[] matching_cws = new ConnectionWidget[cw_list.size()];
        cw_list.toArray(matching_cws);
        return matching_cws;
    }

        /** This method finds any ConnectionWidgets in window that pass through
          * the point _location.
          */
    private ConnectionWidget[] findConnections(Point _passing_through_point) {
        assert (_passing_through_point != null);

        ConnectionWidget[] cws = getConnectionWidgets();
        ArrayList cw_list = new ArrayList();
        for (int i = 0; i < cws.length; i++) {
            Point p= cws[i].getClosestPointOnConnection(_passing_through_point);
            if (p.equals(_passing_through_point)) {
                cw_list.add(cws[i]);
            }
        }

        ConnectionWidget[] matching_cws = new ConnectionWidget[cw_list.size()];
        cw_list.toArray(matching_cws);
        return matching_cws;
    }

        /** This method finds any ConnectionWidgets in window who's data
          * originates from _origin_port_widget and that pass through the point
          * _location.
          */
    private ConnectionWidget[] findConnections(GenericPortWidget _origin_widget,
                                               Point _passing_through_point) {
        ConnectionWidget[] cws_via_origin = findConnections(_origin_widget);
        ConnectionWidget[] cws_via_pass_through_point =
                                        findConnections(_passing_through_point);
        ArrayList cw_list = new ArrayList();
        for (int i = 0; i < cws_via_origin.length; i++) {
            for (int j = 0; j < cws_via_pass_through_point.length; j++) {
                if (cws_via_origin[i] == cws_via_pass_through_point[j]) {
                    cw_list.add(cws_via_origin[i]);
                }
            }
        }

        ConnectionWidget[] matching_cws = new ConnectionWidget[cw_list.size()];
        cw_list.toArray(matching_cws);
        return matching_cws;
    }

    // Junction Actions

        /** Called via popup on junction. */
    public void deleteJunction () {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();

        if (current_widget instanceof JunctionWidget) {
            deleteJunction((JunctionWidget) current_widget);
        }
    }

    public void deleteJunction (JunctionWidget _junction_widget) {
        JunctionProxy junction = _junction_widget.getAssociatedProxy();
        ConnectionProxy[] connections = junction.getConnections(m_node, null);
        ConnectionProxy source_connection= junction.getSourceConnection(m_node);

            // a special case to deal with when true
        boolean b_set_as_primary_out_bundle = false;

            // if junction has no source connection or no sink connections,
            // delete all connections.
        if ((source_connection == null) || (connections.length == 1)) {
            for (int i = 0; i < connections.length; i++) {
                m_node.connection_remove(connections[i]);
            }
            return;
        }
            // case with single source con and single sink con.
        else if (connections.length == 2) {
                // will be true only if source_connection is a bundle, the
                // source port of source_connection is a bundler, and
                // source_connection is the primary output of this bundler.
            b_set_as_primary_out_bundle =
                                 source_connection.isSourceBundlerPrimaryOut();
        }
            // bundlers cannot have multiple primary or secondary ouputs (unlike
            // the unlimited number of outputs a junction can have.
        else if (source_connection.getSource() instanceof BundlerProxy) {
            String message = "EditorFrame.deleteJunction():\n\n" +
                        "\tCannot delete this junction.  Upstream bundler\n" +
                        "\tsource does not support multiple primary or\n" +
                        "\tsecondary output connections.  Operation cancelled.";
            JOptionPane.showMessageDialog(this, message);
            return;
        }

            // will get here only if there is a source connection and 1 or more
            // sink connections...
        GenericPortProxy source_con_source = source_connection.getSource();
        ConnectionWidget source_cw =
                                  determineConnectionWidget(source_connection);
        for (int i = 0; i < connections.length; i++) {
            if (connections[i] != source_connection) {
                ConnectionWidget sink_cw =
                                     determineConnectionWidget(connections[i]);
                Vector path_points = source_cw.copyPoints();
                path_points.addAll(sink_cw.copyPoints());

                    // this tells sink_cw not to look at it's previous
                    // path points to determine special case paths.  with
                    // the joining of source_cw and sink_cw path, that old
                    // information is no longer valid.
                sink_cw.clearPreviousPathPoints();

                Integer[][] path_definition_and_hint = ConnectionProxy.
                                       pathPointsToPathDefinition(path_points);
                Integer[] path_definition = path_definition_and_hint[0];
                int dog_leg_hint = path_definition_and_hint[1][0].intValue();

                MemberNodeProxy member = source_connection.getSourceMember();
                connections[i].setSource(m_node, source_con_source,
                                                      member, path_definition);
                connections[i].setDogLegHint(dog_leg_hint, true);

                if (b_set_as_primary_out_bundle) {
                    ((BundleProxy) connections[i]).
                                          setSourceBundlerPrimaryOutFlag(true);
                }
            }
        }

        m_node.junction_remove(junction);
    }

        /** Called via popup on connection. */
    public void addJunction () {
        addJunction(m_view.getLastMouseDownLocation_dc());
    }

    public JunctionProxy addJunction(Point _raw_point) {
        Point grid_point = GridTool.getNearestGridPoint(_raw_point);
        JGoObject o = m_view.pickDocObject(grid_point, false);
        if (o instanceof ConnectionWidget) {
            return addJunction(grid_point, (ConnectionWidget) o);
        }
        return null;
    }

    private JunctionProxy addJunction(Point _grid_point, ConnectionWidget _cw) {
        ConnectionProxy original_connection = _cw.getAssociatedProxy();
        boolean b_is_bundle = (original_connection instanceof BundleProxy);

        GenericPortProxy original_source = original_connection.getSource();
        MemberNodeProxy original_source_member =
                                         original_connection.getSourceMember();
        GenericPortProxy original_sink = original_connection.getSink();
        MemberNodeProxy original_sink_member =
                                           original_connection.getSinkMember();

            // determine the path definitions for the two connections which
            //   will replace original_connection, split at _grid_point.
        Vector[] split_path_points = _cw.determineSplitPaths(_grid_point);
        Integer[][] upstream_definition_and_hint =
               ConnectionProxy.pathPointsToPathDefinition(split_path_points[0]);
        Integer[] upstream_definition = upstream_definition_and_hint[0];
        int upstream_dog_leg_hint=upstream_definition_and_hint[1][0].intValue();
        Integer[][] downstream_definition_and_hint =
               ConnectionProxy.pathPointsToPathDefinition(split_path_points[1]);
        Integer[] downstream_definition = downstream_definition_and_hint[0];
        int downstream_dog_leg_hint =
                               downstream_definition_and_hint[1][0].intValue();

            // get current bundler terminal configuration
        TerminalBundlersConfiguration original_config = original_connection.
                     generateCurrentTerminalBundlersConfigurationState(m_node);

        JunctionProxy junction = m_node.junction_add(null, _grid_point);

        deleteConnection(_cw, false);

            // create the input connection from original connection's source
        TerminalBundlersConfiguration config = null;
        if (original_source instanceof BundlerProxy) {
            config = original_config.copy();
            config.setIsSinkBundlersPrimaryInput(false);
            config.setSinkBundlerInputChannelName(null);
            config.setSinkBundlerUnwrapsInputBundleContents(false);
        }
        ConnectionProxy con = m_node.connection_add(original_source,
                                     original_source_member, junction, null,
                                     upstream_definition, b_is_bundle, config);
        con.setDogLegHint(upstream_dog_leg_hint, true);

            // create output connection to original connection's sink
        config = null;
        if (original_sink instanceof BundlerProxy) {
            config = original_config.copy();
            config.setIsSourceBundlersPrimaryOutput(false);
            config.setSourceBundlerOutputChannelNames(null);
        }
        con = m_node.connection_add(junction, null, original_sink,
            original_sink_member, downstream_definition,  b_is_bundle, config);
        con.setDogLegHint(downstream_dog_leg_hint, true);

        return junction;
    }


        /** This method finds any JunctionWidgets in window who's data
          * originates from _origin_widget.
          */
    private JunctionWidget[] findJunctions(GenericPortWidget _origin_widget) {
        GenericPortProxy origin= _origin_widget.getAssociatedGenericPortProxy();
        MemberNodeWidget origin_mnw = _origin_widget.getOwnerMemberWidget();
        MemberNodeProxy origin_member = (origin_mnw == null) ? null :
                                               origin_mnw.getAssociatedProxy();

        JunctionWidget[] jws = getJunctionWidgets();
        ArrayList jw_list = new ArrayList();
        for (int i = 0; i < jws.length; i++) {
            JunctionProxy junction = jws[i].getAssociatedProxy();
            ConnectionProxy connection = junction.getSourceConnection(m_node);
            if (       (connection != null) &&
                       (connection.getOrigin(m_node) == origin) &&
                       (connection.getOriginMember(m_node) == origin_member)) {
                jw_list.add(jws[i]);
            }
        }

        JunctionWidget[] matching_jws = new JunctionWidget[jw_list.size()];
        jw_list.toArray(matching_jws);
        return matching_jws;
    }


        /** This method finds any JunctionWidgets in window at the point
          * _location.
          */
    public JunctionWidget[] findJunctions(Point _location) {
        JunctionWidget[] jws = getJunctionWidgets();
        ArrayList jw_list = new ArrayList();
        for (int i = 0; i < jws.length; i++) {
            if (jws[i].getAssociatedProxy().getLocation().equals(_location)) {
                jw_list.add(jws[i]);
            }
        }

        JunctionWidget[] matching_jws = new JunctionWidget[jw_list.size()];
        jw_list.toArray(matching_jws);
        return matching_jws;
    }

        /** This method finds any JunctionWidgets in window who's data
          * originates from _origin_widget and who sit at the point _location.
          */
    private JunctionWidget[] findJunctions(GenericPortWidget _origin_widget,
                                                             Point _location) {
        JunctionWidget[] jws_via_location = findJunctions(_location);
        if (jws_via_location.length == 0) { return new JunctionWidget[0]; }

        JunctionWidget[] jws_via_origin = findJunctions(_origin_widget);
        ArrayList jw_list = new ArrayList();
        for (int i = 0; i < jws_via_origin.length; i++) {
            for (int j = 0; j < jws_via_location.length; j++) {
                if (jws_via_origin[i] == jws_via_location[j]) {
                    jw_list.add(jws_via_origin[i]);
                }
            }
        }

        JunctionWidget[] matching_jws = new JunctionWidget[jw_list.size()];
        jw_list.toArray(matching_jws);
        return matching_jws;
    }

        /** Called via popup on connection. */
    public void addBundler() {
        addBundler(m_view.getLastMouseDownLocation_dc());
    }

    public BundlerProxy addBundler(Point _raw_point) {
        Point grid_point = GridTool.getNearestGridPoint(_raw_point);
        JGoObject o = m_view.pickDocObject(grid_point, false);
        if (o instanceof ConnectionWidget) {
            return addBundler(grid_point, (ConnectionWidget) o);
        }
        else { return m_node.bundler_add(null, grid_point); }
    }

        /** This method is used only to add a bundler to a bundle, and
          * automatically connects the resulting input and output bundles as
          * the primary input and output of the new bundler.
          */
    private BundlerProxy addBundler(Point _grid_point, ConnectionWidget _cw) {
        ConnectionProxy original_connection = _cw.getAssociatedProxy();
        if (original_connection instanceof BundleProxy) {
            BundleProxy original_bundle = (BundleProxy) original_connection;
            GenericPortProxy original_source = original_bundle.getSource();
            MemberNodeProxy original_source_member =
                                             original_bundle.getSourceMember();
            GenericPortProxy original_sink = original_bundle.getSink();
            MemberNodeProxy original_sink_member =
                                               original_bundle.getSinkMember();

                // determine the path definitions for the two bundles which
                //   will replace original_bundle, split at _grid_point.
            Vector[] split_path_points = _cw.determineSplitPaths(_grid_point);
            Integer[][] upstream_definition_and_hint =
               ConnectionProxy.pathPointsToPathDefinition(split_path_points[0]);
            Integer[] upstream_definition = upstream_definition_and_hint[0];
            int upstream_dog_leg_hint =
                                 upstream_definition_and_hint[1][0].intValue();
            Integer[][] downstream_definition_and_hint =
               ConnectionProxy.pathPointsToPathDefinition(split_path_points[1]);
            Integer[] downstream_definition = downstream_definition_and_hint[0];
            int downstream_dog_leg_hint =
                               downstream_definition_and_hint[1][0].intValue();

                // get current bundler terminal configuration
            TerminalBundlersConfiguration original_config = original_connection.
                      generateCurrentTerminalBundlersConfigurationState(m_node);

            BundlerProxy bundler = m_node.bundler_add(null, _grid_point);

            deleteConnection(_cw, false);

                // create the input bundle from the original bundle's source
            TerminalBundlersConfiguration config = (original_config != null) ?
                  original_config.copy() : new TerminalBundlersConfiguration();
            if (config != null) {
                config.setIsSinkBundlersPrimaryInput(true);
                config.setSinkBundlerInputChannelName(null);
                config.setSinkBundlerUnwrapsInputBundleContents(false);
            }
            ConnectionProxy con = m_node.connection_add(original_source,
                                         original_source_member, bundler, null,
                                         upstream_definition, true, config);
            con.setDogLegHint(upstream_dog_leg_hint, true);

                // create the output bundle to the original bundle's sink
            config = (original_config != null) ?
                  original_config.copy() : new TerminalBundlersConfiguration();
            if (config != null) {
                config.setIsSourceBundlersPrimaryOutput(true);
                config.setSourceBundlerOutputChannelNames(null);
            }
            con = m_node.connection_add(bundler, null, original_sink,
                    original_sink_member, downstream_definition, true, config);
            con.setDogLegHint(downstream_dog_leg_hint, true);

            return bundler;
        }
        else { return null; }
    }

        /** Called via popup. */
    public void renameBundlerIO() {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();

        if (current_widget instanceof BundlerWidget) {
            renameBundlerIO((BundlerWidget) current_widget);
        }
    }

    private void renameBundlerIO(BundlerWidget _bundler_widget) {
        BundlerProxy bundler = _bundler_widget.getAssociatedProxy();
        GenericPortWidget sink_widget =
                          _bundler_widget.getSecondaryWidget().getSinkWidget();
        int data_type = _bundler_widget.getSecondaryWidget().
                      getAssociatedProxy().determineDownstreamDataType(m_node);

        if (bundler.hasSecondaryOutput(m_node)) {
            BundlerOutputSelectionDialog dialog =
                     BundlerOutputSelectionDialog.executeDialog(this, bundler,
                         m_node, data_type, sink_widget.generateDescription());
            if (dialog.getExitStatus() == BundlerOutputSelectionDialog.OK) {
                Object[][] output_info = dialog.getSelections();
                HashMap secondary_out_name_map = new HashMap();
                for (int i = 0; i < output_info.length; i++) {
                    String secondary_out_name = (String) output_info[i][0];
                    Boolean B_is_bundle = (Boolean) output_info[i][1];
                    secondary_out_name_map.put(secondary_out_name, B_is_bundle);
                }

                ALFINode container = null;
                if (dialog.propagateChannelNamesToRootBaseBundler()) {

                        // deal with possible intermediately inherited settings
                    String warning =
                                checkForIntermediateBundlerIOSettings(bundler);
                    if (warning != null) {
                        String[] options = { "Continue", "Cancel" };
                        String title= "Intermediate Inherited Settings Warning";
                        int choice = JOptionPane.showOptionDialog(this,
                                  warning, title, JOptionPane.OK_CANCEL_OPTION,
                                  JOptionPane.WARNING_MESSAGE, null, options,
                                  options[0]);
                        if (choice == 1) { return; }
                    }
                    BundlerIOSetting[] intermediate_settings =
                                     getIntermediateBundlerIOSettings(bundler);
                    for (int i = 0; i < intermediate_settings.length; i++) {
                        ALFINode intermediate_node =
                             intermediate_settings[i].getDirectContainerNode();
                        intermediate_node.bundlerSetting_removeLocal(bundler);
                    }

                        // no warning to remove local settings...
                    if (m_node.bundlerSetting_containsLocal(bundler)) {
                        m_node.bundlerSetting_removeLocal(bundler);
                    }

                        // now continuing...
                    container = bundler.getDirectContainerNode();

                        // these boxes will need resaving
                    ALFINode[] affected_boxes = dialog.getAffectedBoxes();
                    for (int i = 0; i < affected_boxes.length; i++) {
                         affected_boxes[i].setTimeLastModified();
                    }
                }
                else { container = m_node; }

                bundler.setSecondaryOutputChannelNames(container,
                                                       secondary_out_name_map);
                bundler.doContentUpdate(container, null);
            }
        }
        else if (bundler.hasSecondaryInput(m_node)) {
            boolean b_secondary_in_is_bundle =
                    (bundler.getSecondaryInput(m_node) instanceof BundleProxy);
            BundlerInputDialog dialog =
                     showBundlerInputDialog(bundler, b_secondary_in_is_bundle,
                                        bundler.getSecondaryInputName(m_node),
                                        bundler.secondaryInputUnwraps(m_node));

            if (dialog.getExitStatus() == BundlerInputDialog.OK) {
                String name = dialog.getInputName();
                boolean b_unwraps = dialog.getUnwrapsInput();
                boolean b_propogate_config =
                                     dialog.propagateConfigToRootBaseBundler();

                ALFINode container = null;
                if (b_propogate_config) {
                        // deal with possible intermediately inherited settings
                    BundlerIOSetting[] intermediate_settings =
                                     getIntermediateBundlerIOSettings(bundler);
                    for (int i = 0; i < intermediate_settings.length; i++) {
                        ALFINode intermediate_node =
                             intermediate_settings[i].getDirectContainerNode();
                        intermediate_node.bundlerSetting_removeLocal(bundler);
                    }

                        // no warning to remove local settings...
                    if (m_node.bundlerSetting_containsLocal(bundler)) {
                        m_node.bundlerSetting_removeLocal(bundler);
                    }

                        // now continuing...
                    container = bundler.getDirectContainerNode();

                        // these boxes will need resaving
                    ALFINode[] affected_boxes = dialog.getAffectedBoxes();
                    for (int i = 0; i < affected_boxes.length; i++) {
                        affected_boxes[i].setTimeLastModified();
                    }
                }
                else { container = m_node; }

                bundler.setSecondaryInputConfiguration(container,
                                                              name, b_unwraps);
                bundler.updateContentAndPropogateDownstream(container, null);

                if (b_propogate_config) {
                    ALFINode[] derived_nodes = container.derivedNodes_getAll();
                    for (int i = 0; i < derived_nodes.length; i++) {
                        bundler.updateContentAndPropogateDownstream(
                                                       derived_nodes[i], null);
                    }
                }
            }
        }
    }

        /** Provides a warning string if settings such as those found by the
          * method getIntermediateBundlerIOSettings() are found.
          */
    public String checkForIntermediateBundlerIOSettings(BundlerProxy _bundler) {
        String warning = null;
        BundlerIOSetting[] intermediate_settings =
                                    getIntermediateBundlerIOSettings(_bundler);
        if (intermediate_settings.length > 0) {
            ALFINode root_base =
                      m_node.isRootNode() ? m_node : m_node.baseNode_getRoot();
            warning = "Bundler I/O Settings have been found in intermediate\n" +
                      "nodes in the inheritance hierarchy between this node\n" +
                      "[" + m_node + "] and its root base node [" + root_base +
                                                                       "].\n" +
                      "The following settings will be removed if this\n" +
                      "operation continues:\n\n";
            for (int i = 0; i < intermediate_settings.length; i++) {
                BundlerIOSetting setting = intermediate_settings[i];
                ALFINode container = setting.getDirectContainerNode();
                warning += "\t" + setting + " in node [" + container + "]\n";
            }
        }

        return warning;
    }

        /** Searches for bundler io settings associated with _bundler which
          * have been added to nodes in the inheritance tree between m_node
          * and m_node's root base node.  Usually none such will exist...
          */
    private BundlerIOSetting[] getIntermediateBundlerIOSettings(
                                                       BundlerProxy _bundler) {
        ArrayList intermediate_setting_list = new ArrayList();
        ALFINode root_base =
                      m_node.isRootNode() ? m_node : m_node.baseNode_getRoot();
        BundlerIOSetting[] inherited_settings =
                                         m_node.bundlerSettings_getInherited();
        for (int i = 0; i < inherited_settings.length; i++) {
            if (inherited_settings[i].getDirectContainerNode() != root_base) {
                if (inherited_settings[i].getAssociatedBundler() == _bundler) {
                    intermediate_setting_list.add(inherited_settings[i]);
                }
            }
            else { break; }
        }

        BundlerIOSetting[] intermediate_settings =
                        new BundlerIOSetting[intermediate_setting_list.size()];
        intermediate_setting_list.toArray(intermediate_settings);
        return intermediate_settings;
    }

        /** Called via popup.  If _b_is_input is true, this changes the
          * bundler's primary input to its secondary input.  If false, this
          * method changes the bundler's primary output to its secondary output.
          */
    public void bundlerPrimaryToSecondary(boolean _b_is_input) {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();
        if (current_widget instanceof BundlerWidget) {
            BundlerWidget bundler_widget = (BundlerWidget) current_widget;
            BundlerProxy bundler = bundler_widget.getAssociatedProxy();
            boolean b_do_downstream_content_update = false;
            if (_b_is_input) {
                BundlerInputDialog dialog =
                              showBundlerInputDialog(bundler, true, "", false);
                if (dialog.getExitStatus() == BundlerInputDialog.OK) {
                    bundler.setPrimaryInput(m_node, null);
                    bundler.setSecondaryInputConfiguration(m_node,
                              dialog.getInputName(), dialog.getUnwrapsInput());

                    b_do_downstream_content_update = true;
                }
            }
            else {
                BundleProxy output_bundle = bundler.getPrimaryOutput(m_node);
                bundler.setPrimaryOutput(m_node, null);

                renameBundlerIO(bundler_widget);

                if (m_node.bundlerSetting_containsLocal(bundler)) {
                    b_do_downstream_content_update = true;
                }
                else {
                        // if secondary out not configured, reset primary out
                    bundler.setPrimaryOutput(m_node, output_bundle);
                    b_do_downstream_content_update = false;
                }
            }

            bundler_widget.update();

            if (b_do_downstream_content_update) {
                bundler.doContentUpdate(m_node, null);
            }
        }
    }

    public BundlerInputDialog showBundlerInputDialog(BundlerProxy _sink_bundler,
                 boolean _b_input_is_bundle, String _name, boolean _b_unwraps) {
        BundlerInputDialog dialog = new BundlerInputDialog(this, _sink_bundler,
                                m_node, _b_input_is_bundle, _name, _b_unwraps);
        dialog.pack();
        centerDialog(dialog);
        dialog.setVisible(true);

        return dialog;
    }

        /** Called via popup. */
    public void deleteBundler() {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();

        if (current_widget instanceof BundlerWidget) {
            deleteBundler((BundlerWidget) current_widget);
        }
    }

        /** Removes any secondary connection, joins primary input and output
          * bundle if both exist (otherwise deletes the one that exists, if
          * either do), and then deletes the bundler.
          */
    public void deleteBundler(BundlerWidget _bundler_widget) {
        BundlerProxy bundler = _bundler_widget.getAssociatedProxy();
        GenericPortProxy update_port = null;
        MemberNodeProxy update_member = null;

        ConnectionWidget secondary_cw = _bundler_widget.getSecondaryWidget();
        if (secondary_cw != null) {
            ConnectionProxy secondary_io = secondary_cw.getAssociatedProxy();

                // deleting 2ndry input?  content will change down primary out
            if (secondary_io.getSink() == bundler) {
                if (bundler.hasPrimaryOutput(m_node)) {
                    ConnectionProxy primary_out =
                                              bundler.getPrimaryOutput(m_node);
                    update_port = primary_out.getSink();
                    update_member = primary_out.getSinkMember();
                }
            }
                // else content change down deleted 2ndry output
            else {
                update_port = secondary_io.getSink();
                update_member = secondary_io.getSinkMember();
            }

            deleteConnection(secondary_cw, false);
        }

            // is there a through bundle connection?  if so, fix.
        ConnectionWidget input_cw = _bundler_widget.getPrimaryInputWidget();
        ConnectionWidget output_cw = _bundler_widget.getPrimaryOutputWidget();
        if ((input_cw != null) && (output_cw != null)) {
            Vector path_points = input_cw.copyPoints();
            path_points.addAll(output_cw.copyPoints());
            Integer[][] path_definition_and_hint =
                       ConnectionProxy.pathPointsToPathDefinition(path_points);
            Integer[] path_definition = path_definition_and_hint[0];
            int dog_leg_hint = path_definition_and_hint[1][0].intValue();

            BundleProxy input_bundle =
                                   (BundleProxy) input_cw.getAssociatedProxy();
            BundleProxy output_bundle =
                                  (BundleProxy) output_cw.getAssociatedProxy();
            GenericPortProxy source = input_bundle.getSource();
            MemberNodeProxy member = input_bundle.getSourceMember();

                // get info on possible previous bundler and its output
            boolean b_is_primary_out = false;
            BundlerProxy source_bundler =
                (source instanceof BundlerProxy) ? (BundlerProxy) source : null;
            if (source_bundler != null) {
                b_is_primary_out =
                     (input_bundle == source_bundler.getPrimaryOutput(m_node));

                    // this lower level call is to keep source_bundler
                    // unconfused before the call below to
                    // source_bundler.setPrimaryOutput() (if needed)
                output_bundle.setSourceBundlerPrimaryOutFlag(b_is_primary_out);
            }

                // remove input-connection to bundler-to-be-removed
            deleteConnection(input_cw, false);

                // extend output-connection back to input-connection's source
            output_bundle.setSource(m_node, source, member, path_definition);
            output_bundle.setDogLegHint(dog_leg_hint, true);

            if (b_is_primary_out) {
                source_bundler.setPrimaryOutput(m_node, output_bundle);
            }
        }
        else {
            if (output_cw != null) { deleteConnection(output_cw, false); }
            else if (input_cw != null) { deleteConnection(input_cw, false); }
        }

        m_node.bundler_remove(bundler);

        if (update_port != null) {
            update_port.doContentUpdate(m_node, update_member);
        }
    }

        /** Called via popup */
    public void addTextComment () {

        Point location = new Point(m_view.getLastMouseDownLocation_dc());
        Rectangle view_rect = m_view.getViewRect();
    
        if ((location == null) || (! view_rect.contains(location))) {
            System.err.println("EditorFrame.addTextComment(" +
                "\tDEV FLAG: location = " + location + " invalid.");
            location = new Point(view_rect.x + (view_rect.width / 2),
                                 view_rect.y + (view_rect.height / 2));
        }
        addTextComment(location);
    }

    public void addTextComment (Point _location) {
        m_node.textComment_add(null, "Comment Here", _location, 
              DEFAULT_FONT_SIZE, DEFAULT_FONT_COLOR, false, false, false);
    }

        /** Deletes the selected TextComment */
    public void deleteTextComment () {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();
        if (current_widget != null ) {
            deleteTextComment(current_widget); 
        }
    }

        /** Deletes the specified TextComment */
    public void deleteTextComment (JGoObject _widget) {
        m_view.update(m_view.getGraphics());    // kluge to remove popup

        if (_widget instanceof TextCommentWidget) {
            TextCommentWidget widget = (TextCommentWidget) _widget;
            TextComment comment = widget.getAssociatedProxy();
            m_node.textComment_remove(comment);
        }
    }

        /** Called via popup */
    public void addColoredBox () {

        Point location = new Point(m_view.getLastMouseDownLocation_dc());
        Rectangle view_rect = m_view.getViewRect();
    
        if ((location == null) || (! view_rect.contains(location))) {
            System.err.println("EditorFrame.addColoredBox(" +
                "\tDEV FLAG: location = " + location + " invalid.");
            location = new Point(view_rect.x + (view_rect.width / 2),
                                 view_rect.y + (view_rect.height / 2));
        }
        addColoredBox(location);
    }

    public void addColoredBox (Point _location) {
        m_node.coloredBox_add(null, DEFAULT_BOX_COLOR, _location,
                                DEFAULT_BOX_SIZE, ColoredBox.HILITE_LAYER_BACK);
    }
        /** Deletes the selected ColoredBox */
    public void deleteColoredBox () {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();
        if (current_widget != null) { deleteColoredBox(current_widget); }
    }

        /** Deletes the specified ColoredBox */
    public void deleteColoredBox (JGoObject _widget) {
        m_view.update(m_view.getGraphics());    // kluge to remove popup

        if (_widget instanceof ColoredBoxWidget) {
            ColoredBoxWidget widget = (ColoredBoxWidget) _widget;
            ColoredBox window_comment = widget.getAssociatedProxy();

            m_node.coloredBox_remove(window_comment);
        }
    }

        /** Deletes multiple objects in an intelligent manner. */
    public void delete(ArrayList _delete_list) {
        //BundleProxy.setBundleContentUpdatesSuspended(true);

        ArrayList junction_list = new ArrayList();
        ArrayList bundler_list = new ArrayList();
        ArrayList member_list = new ArrayList();
        ArrayList port_list = new ArrayList();
        ArrayList text_comment_list = new ArrayList();
        ArrayList comment_bg_list = new ArrayList();
        for (Iterator i = _delete_list.iterator(); i.hasNext(); ) {
            Object o = i.next();
            if (o instanceof JunctionWidget) { junction_list.add(o); }
            else if (o instanceof BundlerWidget) { bundler_list.add(o); }
            else if (o instanceof MemberNodeWidget) { member_list.add(o); }
            else if (o instanceof PortWidget) { port_list.add(o); }
            else if (o instanceof TextCommentWidget) { text_comment_list.add(o); }
            else if (o instanceof ColoredBoxWidget) { comment_bg_list.add(o); }
        }

        String box_files_affected_warning =
                                  generateAffectedBoxFilesWarning(member_list);
        if (box_files_affected_warning != null) {
            String title = "Multiple Box Files Affected Notice";
            int choice = JOptionPane.showConfirmDialog(this,
                box_files_affected_warning, title,JOptionPane.OK_CANCEL_OPTION);
            if (choice == JOptionPane.CANCEL_OPTION) { return; }
        }

        for (Iterator i = bundler_list.iterator(); i.hasNext(); ) {
             deleteBundler((BundlerWidget) i.next());
        }

        for (Iterator i = junction_list.iterator(); i.hasNext(); ) {
             deleteJunction((JunctionWidget) i.next());
        }

        for (Iterator i = member_list.iterator(); i.hasNext(); ) {
             deleteMemberNode((MemberNodeWidget) i.next(), false);
        }

        for (Iterator i = port_list.iterator(); i.hasNext(); ) {
             deletePort((PortWidget) i.next());
        }

        for (Iterator i = text_comment_list.iterator(); i.hasNext(); ) {
             deleteTextComment((TextCommentWidget) i.next());
        }

        for (Iterator i = comment_bg_list.iterator(); i.hasNext(); ) {
             deleteColoredBox((ColoredBoxWidget) i.next());
        }

        //BundleProxy.setBundleContentUpdatesSuspended(false);
    }

        /** Called via popup. */
    public void validateBundles() {
        ALFINode root_container =
               (m_node.isRootNode()) ? m_node : m_node.containerNode_getRoot();
        boolean[] flags = { true, false };
        Alfi.getMainWindow().validateNode(m_node, flags, false);
    }

    /*
     * This is a workaround for window managers like fvwm which
     * pushes all dialogs past the screen's (0, 0) coordinate.
     */
    static public void centerDialog (JDialog _dialog) {
        Dimension screen_size = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dialog_size = _dialog.getSize();
        _dialog.setBounds((screen_size.width - dialog_size.width)/2,
                  (screen_size.height - dialog_size.height)/2,
                   dialog_size.width, dialog_size.height);
 
    }
    
    // Node Actions
    public void editComment () {
        final ALFINode node = determineNode();
        editComment(this, node);
    }

    static public void editComment (JFrame _parent, final ALFINode _node) {
        
        final JDialog dialog = new JDialog(_parent, "Edit Comment", true);
        Container content_pane = dialog.getContentPane();
        content_pane.setLayout(new BorderLayout(10, 10));
    
        String comment;
        if (_node.comment_getLocal() != null) {
            comment = new String(_node.comment_getLocal() + 
                                 "\n\n\n\n\n\n\n\n\n\n");
        } else {
            comment = new String("\n\n\n\n\n\n\n\n\n\n");
        }

        final JEditorPane editor = new JEditorPane("text/plain", comment);
        editor.setSize(400, 600);
        JPanel editor_panel = new JPanel();
        editor_panel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLoweredBevelBorder(), " Edit Area "));
        editor_panel.add(editor);
        content_pane.add(BorderLayout.CENTER, editor_panel);

        JPanel button_panel = new JPanel();
        final JButton okay_button = new JButton("Okay");
        okay_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String comment = editor.getText();
                    comment = comment.trim();
                    _node.comment_clearLocal();
                    _node.comment_addLocal(comment);
                    dialog.setVisible(false);
                    dialog.dispose();
                }
            });
        button_panel.add(okay_button);
        dialog.getRootPane().setDefaultButton(okay_button);

        JButton clear_button = new JButton("Clear Entry");
        clear_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    editor.setText("\n\n\n\n\n\n\n\n\n\n");
                    dialog.getRootPane().setDefaultButton(okay_button);
                }
            });
        button_panel.add(clear_button);

        JButton cancel_button = new JButton("Cancel");
        cancel_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dialog.setVisible(false);
                    dialog.dispose();
                }
            });
        button_panel.add(cancel_button);
        content_pane.add(BorderLayout.SOUTH, button_panel);

        dialog.pack();
        centerDialog(dialog);
        dialog.setVisible(true);
    }


    static public void editTextComment (JFrame _parent, 
                                              final TextComment _text_comment) {
        
        final JDialog dialog = new JDialog(_parent, "Edit Text Comment", true);
        Container content_pane = dialog.getContentPane();
        content_pane.setLayout(new BorderLayout(10, 10));
    
        String comment;
        if (_text_comment.getCommentValue() != null) {
            comment = new String(_text_comment.getCommentValue() + 
                                 "\n\n\n\n\n\n\n\n\n\n");
        } else {
            comment = new String("\n\n\n\n\n\n\n\n\n\n");
        }

        final JEditorPane editor = new JEditorPane("text/plain", comment);
        editor.setSize(400, 600);
        JPanel editor_panel = new JPanel();
        editor_panel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLoweredBevelBorder(), " Edit Area "));
        editor_panel.add(editor);
        content_pane.add(BorderLayout.CENTER, editor_panel);

        JPanel button_panel = new JPanel();
        final JButton okay_button = new JButton("Okay");
        okay_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String comment = editor.getText();
                    comment = comment.trim();
                    if (comment.length() > 0)
                        _text_comment.setCommentValue(comment);
                    dialog.setVisible(false);
                    dialog.dispose();
                }
            });
        button_panel.add(okay_button);
        dialog.getRootPane().setDefaultButton(okay_button);

        JButton clear_button = new JButton("Clear Entry");
        clear_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    editor.setText("\n\n\n\n\n\n\n\n\n\n");
                    dialog.getRootPane().setDefaultButton(okay_button);
                }
            });
        button_panel.add(clear_button);

        JButton cancel_button = new JButton("Cancel");
        cancel_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dialog.setVisible(false);
                    dialog.dispose();
                }
            });
        button_panel.add(cancel_button);
        content_pane.add(BorderLayout.SOUTH, button_panel);

        dialog.pack();
        centerDialog(dialog);
        dialog.setVisible(true);
    }

    public void editMacros () {
        final ALFINode node = determineNode();

        editMacros(this, node);
    }

    static public void editMacros (JFrame _parent, ALFINode _node) {
        MacroDialog dialog = new MacroDialog(_parent, _node);
        dialog.setVisible(true);

        if (dialog.getReturnStatus() == MacroDialog.OK) {
            String[] data = dialog.getNewMacroData();
            if ((data != null) && (data[0] != null)) {
                String test_value = new String(data[0]);
                if (test_value.trim().length() > 0) {
                    _node.macro_set(data[0], data[1], true);
                }
                else { _node.macro_clearLocal(); }
            }
            else { _node.macro_clearLocal(); }
        }
        dialog.dispose();
    }

    public void viewExternal () {
        ((AlfiMainFrame) getParentFrame()).show(m_node, null , false);
    }

    public void viewInternal () {
        ALFINode node = determineNode();
            
        if (node.isPrimitive()) {
            editSettings();
        }
        else { 
            ((AlfiMainFrame) getParentFrame()).show(node, null, true); 
        }
    }

        /**
         * Set the size of the window to be the minimum size which shows all the
         * window contenets without the need for scrollbars.
         */
    public void resetView () {
        setDefaultWindowSize(determineIdealFrameSize());
        resetSize();    // apply the current default window size
        m_node.setFrameSize(null);
    }

       /** 
        * Place the selected widgets into a group
        */
    public void group () {
        JGoSelection selection = getView().getSelection();

            // First check any of the ALFI Groupable objects are already a 
            // part of a group
        ArrayList selected_objs = JGoSelectionToGroupList(selection, false);
        LayeredGroup[] groups_in_selection = getLayeredGroups(selected_objs);

        if (groups_in_selection.length == 0) {
                // Create a brand new group and put all the elements in the group
            ALFINode node = determineNode();
            LayeredGroup new_group = node.layeredGroup_add(null);
            addSelectionToGroup(selected_objs, new_group);
        }
        else if (groups_in_selection.length == 1) {
                // Make sure that all elements of the group are in the selection
            if (!areAllGroupElementsSelected(groups_in_selection[0], 
                                                                selected_objs)){
                return;
            }
                // Add all the elements in the group
            addSelectionToGroup(selected_objs, groups_in_selection[0]); 
        }
            // More than one group is selected
        else {
            //int num = groups_in_selection.length ;
                // Make sure that all elements of the group are in the selection
            for (int i = 0; i < groups_in_selection.length; i++) {
                LayeredGroup group = groups_in_selection[0];
                if (!areAllGroupElementsSelected(group, selected_objs)){
                    return;
                }
            }

                // Create a new group to put all the elements in
            ALFINode node = determineNode();
            LayeredGroup new_group = node.layeredGroup_add(null);

                // Remove all the elements from their existing groups
            removeSelectionFromGroups(selected_objs);
            
                // and put them in the new group
            addSelectionToGroup(selected_objs, new_group); 
        }
        
    }


    public void ungroup () {

        JGoSelection selection = getView().getSelection();

            // First check any of the ALFI Groupable objects are already a 
            // part of a group
        ArrayList selected_objs = JGoSelectionToGroupList(selection, false);

            // Remove all the elements from their existing groups
        removeSelectionFromGroups(selected_objs);
    }



        /** Get all groups in the selection */
    private LayeredGroup[] getLayeredGroups (ArrayList _selected_objs) {
        ArrayList group_list = new ArrayList();

        if (_selected_objs.size() > 0) {
            for (int i = 0; i < _selected_objs.size(); i++) {
                GroupableIF element = (GroupableIF) _selected_objs.get(i);
                LayeredGroup group = element.group_get();
                if (group != null && !group_list.contains(group)) {
                    group_list.add(group);
                }
            }
        }

        if (group_list.size() > 0) {
            LayeredGroup[] groups = new LayeredGroup[group_list.size()];
            group_list.toArray(groups);
            return groups;
        }
        else {
            return new LayeredGroup[0];
        }
    }

        /** Checks that all elements in the group are selected */
    private boolean areAllGroupElementsSelected (LayeredGroup _group,
                                                 ArrayList _selected_objs){
        
        for (int i = 0; i < _group.numElements(); i++){
            GroupableIF element_in_group = _group.getElement(i);

            int j = 0;
            for (; j < _selected_objs.size(); j++) {
                GroupableIF element_in_selection =  
                                            (GroupableIF) _selected_objs.get(j);
                if (element_in_group.equals(element_in_selection)) {
                    break; 
                }
            }
            
            if (j == _selected_objs.size()) {
                return false;
            }
        }

        return true;
    }

        /** Register all the selected objects to the group */
    private void addSelectionToGroup (ArrayList _selected_objs, 
                                      LayeredGroup _group) {
        
        for (int i = 0; i < _selected_objs.size(); i++) {
            GroupableIF element = ( GroupableIF )_selected_objs.get(i);
            if (element.group_get() != _group) {
                if (element.group_get() != null) {
                    element.group_set(null);
                }
                _group.addGroupElement(element);
                element.group_registerListener(this);
            }
        }
    }

        /** Removes all the selected objects from their groups.  Removes
            the groups from the list if it no longer has any members. */
    private void removeSelectionFromGroups (ArrayList _selected_objs) {

            // Remove all the elements from their existing groups
        for (int i = 0; i < _selected_objs.size(); i++) {
            GroupableIF element = (GroupableIF) _selected_objs.get(i);
            LayeredGroup group = element.group_get();
            if (group != null) {
                group.removeElementFromGroup(element);
                if (group.numElements() == 0) {
                    m_node.layeredGroup_remove(group);
                }
            }
        }
    }

        /** From the JGoSelection, filter for GroupableIF items and 
          * create a list with the widgets' associated proxy. 
          */
    private ArrayList JGoSelectionToGroupList (JGoSelection _selection, 
                                               boolean _b_top_level_only) {
        
        ArrayList list = new ArrayList();

        if (_selection != null) {
            JGoListPosition pos = _selection.getFirstObjectPos();
            while (pos != null) {
                JGoObject obj = _selection.getObjectAtPos(pos);
                if (obj instanceof AlfiWidgetIF) {
                    AlfiWidgetIF widget = ( AlfiWidgetIF )obj;
                    Object proxy_obj = widget.getAssociatedProxyObject();
                    if (proxy_obj instanceof GroupableIF) {
                        GroupableIF element = ( GroupableIF )proxy_obj;
                        list.add(element);
                    }
                    else if (proxy_obj instanceof MemberNodeProxy) {
                        MemberNodeProxy mnp = ( MemberNodeProxy ) proxy_obj;
                        ALFINode node = mnp.getAssociatedMemberNode(m_node);
                        list.add(node);
                    }
                }
                pos = (_b_top_level_only) ?
                                 _selection.getNextObjectPosAtTop(pos) :
                                 _selection.getNextObjectPos(pos);
            }
        }
        return list;
    }


    public void setCenterExternalViewPorts(boolean _b_on) {
        if (m_node.isRootNode()) {
            m_node.setCentersPortsFlag(_b_on);
        }
    }

    public void viewBaseBox () {
        ALFINode node = determineNode();

        ALFINode base_node = getBaseNode(node, false);

        AlfiMainFrame mainframe = (AlfiMainFrame) getParentFrame();

        boolean view_internal = true;
        if (base_node.isPrimitive()) {
            view_internal = false;
        }
        mainframe.show(base_node, null, view_internal);
    }

    public void showMainWindow () {
        Alfi.getMainWindow().toFront();
    }

    /**
      * This method is used to export a .sim file which is used for
      * the simulations application.   This type of file does not
      * include any GUI information such as node or connection locations.
      * The .sim file will be located at the same directory as the .box
      * file and with the same filename but with a .sim suffix.
      */
    public void exportModelerFile () {
        
        if (!m_node.isRootNode()) {
            return;
        }
        
        // Make sure that the box file changes are saved.
        AlfiMainFrame mainframe = (AlfiMainFrame) getParentFrame();
        mainframe.save();
         
        m_node.getSourceFile().writeModelerFile();

    }

    public void save () {
        ALFINode[] nodes_to_save = { m_node };
        Alfi.getMainWindow().save(nodes_to_save, true);
    }

        /**
         * This method is used instead of a Save As type operation because of
         * the ambiguities invloved therein.  This method merely writes the
         * current state of this node to a different file than its own source
         * file.
         */
    public void saveCopyAs() {
        String title = "Save Copy of Box As";
        String message = "Enter the new name for the box's copy to take:";
        String file_name = JOptionPane.showInputDialog(this, message, title,
                                                 JOptionPane.QUESTION_MESSAGE);

        if (file_name != null) {
            file_name = file_name.trim();
            StringTokenizer st =
                    new StringTokenizer(file_name, (" \t\n" + File.separator));
            if ((file_name.length() == 0) || (st.countTokens() != 1)) {
                Alfi.warn("Invalid characters in file name. Not Saved.", false);
                return;
            }
            if (! file_name.endsWith(ALFIFile.BOX_NODE_FILE_SUFFIX)) {
                file_name += ALFIFile.BOX_NODE_FILE_SUFFIX;
            }

            File directory = m_node.getSourceFile().getCanonicalDirectory();
            File save_as_file = new File(directory, file_name);

            m_node.getSourceFile().writeNodeToDifferentFile(save_as_file);
        }
    }

    public void setVisible(boolean _show) {

        if (!_show) {
            EditorWindowEvent hiding_event =
                 new EditorWindowEvent(this, EditorWindowEvent.EDITOR_WINDOW_HIDING);
            fireEditorWindowEvent(hiding_event);
        }

        super.setVisible(_show);
    }

    public void close() {
        EditorWindowEvent closing_event =
                 new EditorWindowEvent(this, EditorWindowEvent.WINDOW_CLOSING);
        fireEditorWindowEvent(closing_event);

        Alfi.getTheNodeCache().removeEditSession(m_node);

            // remove all widgets as event listeners
        BundlerWidget[] bundler_widgets = getBundlerWidgets();
        for (int i = 0; i < bundler_widgets.length; i++) {
            bundler_widgets[i].getAssociatedProxy().
                                 removeProxyChangeListener(bundler_widgets[i]);
        }
        JunctionWidget[] junction_widgets = getJunctionWidgets();
        for (int i = 0; i < junction_widgets.length; i++) {
            junction_widgets[i].getAssociatedProxy().
                                removeProxyChangeListener(junction_widgets[i]);
        }
        ConnectionWidget[] connection_widgets = getConnectionWidgets();
        for (int i = 0; i < connection_widgets.length; i++) {
            connection_widgets[i].disableUpdates();
            connection_widgets[i].getAssociatedProxy().
                              removeProxyChangeListener(connection_widgets[i]);
        }
        PortWidget[] port_widgets = getPortWidgets_onContainer();
        for (int i = 0; i < port_widgets.length; i++) {
            port_widgets[i].getAssociatedProxy().
                                    removeProxyChangeListener(port_widgets[i]);
        }
        TextCommentWidget[] text_comment_widgets = getTextCommentWidgets();
        for (int i = 0; i < text_comment_widgets.length; i++) {
            text_comment_widgets[i].getAssociatedProxy().
                             removeProxyChangeListener(text_comment_widgets[i]);
        }
        ColoredBoxWidget[] colored_box_widgets = getColoredBoxWidgets();
        for (int i = 0; i < colored_box_widgets.length; i++) {
            colored_box_widgets[i].getAssociatedProxy().
                             removeProxyChangeListener(colored_box_widgets[i]);
        }
        MemberNodeWidget[] member_node_widgets = getMemberNodeWidgets();
        for (int i = 0; i < member_node_widgets.length; i++) {
            MemberNodeWidget mnw = member_node_widgets[i];
            ALFINode[] widget_node_hierarchy = null;
            if (m_internal_view) {
                MemberNodeProxy node_proxy = mnw.getAssociatedProxy();

                node_proxy.removeProxyChangeListener(mnw);

                    // get the base node hierarchy for port order changes
                ALFINode member_node =
                                 node_proxy.getAssociatedMemberNode(m_node);
                widget_node_hierarchy= member_node.baseNodes_getHierarchyPath();
            }
            else {
                widget_node_hierarchy = m_node.baseNodes_getHierarchyPath();
            }

            for (int j = 0; j < widget_node_hierarchy.length; j++) {
                widget_node_hierarchy[j].removePortOrderChangeListener(mnw);
            }

                // removes widget as listener to ContainedElementMod events
            mnw.cleanUp();
        }

        m_node.removeNodeContentChangeListener(this);
        m_node.removeNodeToBeDestroyedEventListener(this);
        removeComponentListener(this);

        super.close();
    }

        /** Reload this node from disk. */
    public void reload () {
        ((AlfiMainFrame) getParentFrame()).reload(m_node, true);
    }

    public void locateFile () {
        JGoSelection selection = m_view.getSelection();
        JGoObject current_widget = selection.getPrimarySelection();

        ALFINode node = (current_widget instanceof MemberNodeWidget) ?
                        ((MemberNodeWidget) current_widget).getNode() : m_node;

        if (node.isBox()) {
            String s = null;
            if (node.isRootNode()) {
                s = "\nBase node [" + node + "] file: " +
                                node.getSourceFile().getCanonicalPath() + "\n";
            }
            else {
                ALFINode root_base = node.baseNode_getRoot();
                ALFINode root_container = node.containerNode_getRoot();

                s = "\nNode " + node.generateFullNodePathName() + ":\n" +
                    "\tRoot base node [" + root_base + "] file: " +
                          root_base.getSourceFile().getCanonicalPath() + "\n" +
                    "\tRoot container node [" + root_container + "] file: " +
                         root_container.getSourceFile().getCanonicalPath()+"\n";
            }
            System.err.println(s);
        }
    }

    public void showNodeInfo () {
        ALFINode node = determineNode();
        if (node != null)
            System.err.println(node.toString(true));
    }

    public void printToScale () {
        m_view.printToScale();
    }

    public void printFitPage () {
        m_view.printToFitPage();
    }

    public void addMemberNode (String _node_name, ALFINode _base_node,
                                                  String _include_path) {
        Point location = new Point(m_view.getLastMouseDownLocation_dc());
        Rectangle view_rect = m_view.getViewRect();
        if ((location == null) || (! view_rect.contains(location))) {
            System.err.println("EditorFrame.addMemberNode(" +
                _node_name + ", " + _base_node + "):\n" +
                "\tDEV FLAG: location = " + location + " invalid.");
            location = new Point(view_rect.x + (view_rect.width / 2),
                                 view_rect.y + (view_rect.height / 2));
        }
        addMemberNode(_node_name, _base_node, _include_path, location);
    }

    public void addMemberNode (String _node_name, ALFINode _base_node,
                               String _include_path, Point _location) {
        m_node.memberNodeProxy_add(_base_node, _node_name, _include_path,
                                                                _location);
    }

    public void updateTitle() {
        String title = m_node.generateFullNodePathName() + " - ";
        title += (isInternalView()) ? "internal view" : "external view";
        setTitle(title);

            // Now find any derived nodes
        ALFINode[] derived_nodes = m_node.derivedNodes_getDirect();
        for (int i = 0; i < derived_nodes.length; i++){
            EditorFrame editor =
                       Alfi.getTheNodeCache().getEditSession(derived_nodes[i]);
            if (editor != null) { editor.updateTitle(); }
        }
    }

    public void renameNode () {
        JGoObject current_widget = m_view.getSelection().getPrimarySelection();
        if (current_widget instanceof MemberNodeWidget) {
            MemberNodeWidget node_widget = (MemberNodeWidget) current_widget;
            MemberNodeProxy member_node_proxy= node_widget.getAssociatedProxy();
            ALFINode selected_node = determineNode();

                // temporarily suspend connection widget portChange calls.  it
                // gives confusing information during this process.
            ConnectionWidget[] cws = getConnectionWidgets();
            for (int i = 0; i < cws.length; i++) {
                cws[i].suspendPortChangeCalls(true);
            }

            renameNode(this, selected_node, m_node, isModifiable());
            updateTitle();
            setStatusText(node_widget.toString());

                // resume connection widget portChange calls.
            for (int i = 0; i < cws.length; i++) {
                cws[i].suspendPortChangeCalls(false);
            }
	    }

    }


    static public void renameNode (JFrame _parent, ALFINode _node, 
                                  ALFINode _container, boolean _is_modifiable) {

            // this is an operation which requires that all ProxyChangeEvent
            // listeners are finished registering.  check this.
        if (Alfi.getTheProxyListenerRegistrar().isBusy()) {
            String title = "Waiting . . .";
            String message = "Member node renaming requires that all loaded " +
                "boxes and primitives have completed registration as " +
                "listeners for such changes.  This process can take several " +
                "minutes after the loading of complex systems, and takes " +
                "place in the background because few operations require such " +
                "notification.  In the present case, this registration has " +
                "not yet completed (it is " +
                Alfi.getTheProxyListenerRegistrar().getPercentComplete() +
                "% complete.)  Please try again after a short wait.";
            message = TextFormatter.formatMessage(message);
            JOptionPane.showMessageDialog(_parent, message, title,
                                           JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        String original_node_name = _node.determineLocalName();
        int original_node_label_type = _node.getLabelType();
        NodeRenameDialog rename_dialog = new NodeRenameDialog(_parent, _node, 
            _container.toString(), _is_modifiable);
        rename_dialog.pack();
        rename_dialog.setVisible(true);

        String new_node_name = rename_dialog.getValidatedText();

        // Cancel on the dialog sets the ValidatedText to null
        if ( ((rename_dialog.getNodeLabelType() 
                                                == ALFINode.LABEL_TYPE_NAME) ||
               rename_dialog.hasNodeNameChanged())  &&
                                                      new_node_name  != null) {

            // Make sure that the name is unique.
            if (_container.memberNodeProxy_nameExists(new_node_name) != null) {
                if (!new_node_name.equals(original_node_name)){
                    ALFINode bad_node = 
                          _node.memberNodeProxy_nameExists(new_node_name);
                    JOptionPane.showMessageDialog(_parent, 
                        "Node " + bad_node + " has a member node with " +
                        "the name you have selected.  Please try again.");
                }
            }
            else {
                MemberNodeProxy member_node_proxy = 
                    _node.memberNodeProxy_getAssociated();

                if (member_node_proxy == null) {
                    return;
                }

                member_node_proxy.setLocalName(new_node_name);

                propogateChangeToDerivedNodesWhereNeeded(_parent,_node);

                    // the container node needs modeler connection cache update
                _container.modelerConnectionCache_update();
            }
        }
    }

        /** Specially used by the UDP dialog editor.  Users should not be able
          * to access parameter settings for a root UDP.
          */
    public void disableParameterSettingWindowAccess() {
        m_view.disableParameterSettingAccess();
    }

        /** Since static box files contain names of ports, members, etc, any
          * renaming of these elements must be updated to all such files.  This
          * includes files where this node is inherited and local changes have
          * been made to it (i,e, a reference to this node would actually exist
          * in such a box file (only) when there are local changes made to it.)
          */
    private void propogateChangeToDerivedNodesWhereNeeded() {
        propogateChangeToDerivedNodesWhereNeeded(this, m_node);
    }

    private static void propogateChangeToDerivedNodesWhereNeeded(
                                              JFrame _parent, ALFINode _node) {

        ALFINode[] derived_nodes = _node.derivedNodes_getAll();
        ArrayList affected_root_container_list = new ArrayList();

        for (int i = 0; i < derived_nodes.length; i++) {
            if (derived_nodes[i].isModifiedFromDirectBase(false)) {
               ALFINode root_container =
                                      derived_nodes[i].containerNode_getRoot();
               affected_root_container_list.add(root_container);
               derived_nodes[i].setTimeLastModified();
            }
        }

        if (! affected_root_container_list.isEmpty()) {
            String title = "Box File Changes";
            String message =
                "Your latest change has also affected the following files:\n\n";

            for (Iterator i = affected_root_container_list.iterator();
                                                               i.hasNext(); ) {
                String file_path =
                      ((ALFINode) i.next()).getSourceFile().getCanonicalPath();
                message += file_path + "\n";
            }

            message += "\nYou will be prompted to save them when exiting " +
                       "the application.\n\n";

            JOptionPane.showMessageDialog(_parent, message, title,
                                              JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void deleteMemberNode () {
        Point click_point = getView().getLastMouseDownLocation_dc();
        JGoObject widget = getView().pickDocObject(click_point, true);
        deleteMemberNode(widget, true);
    }

        /** The flag _b_show_other_boxes_modified_warning is usally true, but
          * can be set to false for cases where multiple members are being
          * deleted (see delete()), which should handle the warnings in a
          * better manner for a batch of deletions.
          */
    public void deleteMemberNode (JGoObject _widget,
                                boolean _b_show_other_boxes_modified_warning) {
        if (_widget instanceof MemberNodeWidget) {
            MemberNodeWidget member_node_widget = (MemberNodeWidget) _widget;
            MemberNodeProxy member_node_proxy =
                        member_node_widget.getAssociatedProxy();
            ALFINode node_to_remove =
                             member_node_proxy.getAssociatedMemberNode(m_node);

                // Find any boxes which have modified instances of this node.
                // This way any references to the node to be deleted is cleaned
                // up from the base box file.
            String warning_re_other_affected_files =
                _b_show_other_boxes_modified_warning ?
                    generateAffectedBoxFilesWarning(member_node_widget) : null;
            if (warning_re_other_affected_files != null) {
                String title = "Multiple Box Files Affected Notice";
                int choice = JOptionPane.showConfirmDialog(this,
                                        warning_re_other_affected_files, title,
                                        JOptionPane.OK_CANCEL_OPTION);
                if (choice == JOptionPane.CANCEL_OPTION) { return; }
            }

            ALFINode[] derived_nodes = node_to_remove.derivedNodes_getAll();
            for (int i = 0; i < derived_nodes.length; i++) {
                if (derived_nodes[i].isModifiedFromDirectBase(false)) {
                    derived_nodes[i].clear__except_listeners();
                }
            }

                // deal with connections to member in a nice way
            ConnectionProxy[] connections =
                                    member_node_proxy.getConnectionsTo(m_node);
            for (int i = 0; i < connections.length; i++) {
                boolean b_do_downstream_content_update =
                       (connections[i].getSourceMember() == member_node_proxy);
                deleteConnection(connections[i],b_do_downstream_content_update);
            }

            m_node.memberNodeProxy_remove(member_node_proxy);
        }
    }

        /** Convenience method.  See generateAffectedBoxFilesWarning(ArrayList).
          */
    private String generateAffectedBoxFilesWarning(MemberNodeWidget _widget) {
        ArrayList member_list = new ArrayList();
        member_list.add(_widget);
        return generateAffectedBoxFilesWarning(member_list);
    }

        /** Find any boxes which have modified instances of these member nodes
          * and create a warning message to display to user about the coming
          * changes if the process is continued.  Returns null if there are no
          * such box files to be affected.
          */
    private String generateAffectedBoxFilesWarning(ArrayList _member_list) {
            // find all nodes derived from any of the nodes in _member_list and
            // check if they have local changes.  if so, then a deletion of the
            // base node of this node will mean there is information in its
            // container root box file which will need to be purged...
        HashMap affected_roots_map = new HashMap();
        String message = null;
        for (Iterator i = _member_list.iterator(); i.hasNext(); ) {
            MemberNodeWidget member_node_widget = (MemberNodeWidget) i.next();
            ALFINode member_node = member_node_widget.getAssociatedProxy().
                                               getAssociatedMemberNode(m_node);
            ALFINode[] derived_nodes = member_node.derivedNodes_getAll();
            for (int j = 0; j < derived_nodes.length; j++) {
                if (derived_nodes[j].isModifiedFromDirectBase(false)) {
                    ALFINode affected_container_root =
                                      derived_nodes[j].containerNode_getRoot();
                    affected_roots_map.put(affected_container_root, null);
                }
            }
        }

        if (! affected_roots_map.isEmpty()) {
            message = "Your latest change has also affected the following " +
                      "files: \n\n";

            for (Iterator i = affected_roots_map.keySet().iterator();
                                                               i.hasNext(); ) {
                String box_path =
                      ((ALFINode) i.next()).getSourceFile().getCanonicalPath();
                message += box_path + "\n";
            }

            message += "\nYou will be prompted to save the necessary\n" +
                "changes to these files as well when exiting Alfi.\n\n\n";
        }

        return message;
    }

    public void generatePrimitiveFromTemplate(PrimitiveTemplate _template) {
        PrimitiveTemplateIOSetDialog dialog =
                             new PrimitiveTemplateIOSetDialog(this, _template);
        if (dialog.getChoice() == PrimitiveTemplateIOSetDialog.OK) {
            int[] io_set_counts = dialog.getIOSetCounts();

                // find the base node if it already exists.  if not, create it.
            ALFINode base_node = Alfi.getTheNodeCache().
                       findTemplateGeneratedBaseNode(_template, io_set_counts);
            if (base_node == null) {
                base_node = new ALFINode(_template, io_set_counts);
            }

            String member_name = m_node.generateUniqueLocalMemberNodeName(
                                                      _template.getBaseName());
            addMemberNode(member_name, base_node, null);
        }
    }

    public void duplicateMemberNode () {
        Point click_point = getView().getLastMouseDownLocation_dc();
        m_view.setDefaultLocation(click_point);
        duplicate();
    }

    public void editSettings() {
        JGoObject current_widget = m_view.getSelection().getPrimarySelection();

        ALFINode node = (current_widget instanceof MemberNodeWidget) ?
                      ((MemberNodeWidget) current_widget).getObject() : m_node;
        editSettings(node);
    }

    public void editSettings(ALFINode _node) {
        editSettings(this, _node);
    }

    static public void editSettings(JFrame _parent, ALFINode _node) {
        if (_node.isRootNode() && _node.isBox()) {
            LinkDeclarationsDialog dialog =
                                    new LinkDeclarationsDialog(_parent, _node);
            dialog.setVisible(true);
        }
        else {
            NodePropertiesDialog dialog =
                                      new NodePropertiesDialog(_parent, _node);
            dialog.setVisible(true);
        }
        
    }

    public void rotateNorth () {
        rotateMemberNode(MemberNodeProxy.ROTATION_NONE);
    }

    public void rotateSouth () {
        rotateMemberNode(MemberNodeProxy.ROTATION_SOUTH);
    }

    public void rotateEast () {
        rotateMemberNode(MemberNodeProxy.ROTATION_EAST);
    }

    public void rotateWest () {
        rotateMemberNode(MemberNodeProxy.ROTATION_WEST);
    }

        /** Implementation details for any member node rotation. */
    private void rotateMemberNode(int _new_rotation) {
        JGoObject current_widget = m_view.getSelection().getPrimarySelection();
        if (current_widget instanceof MemberNodeWidget) {
            MemberNodeWidget widget = (MemberNodeWidget) current_widget;

            widget.getAssociatedProxy().setRotation(_new_rotation);
        }
    }

    public void swapNodes() {
        JGoSelection selection = m_view.getSelection();

        ConnectionProxy[] connections = m_node.connections_get();

        JGoListPosition list_position = selection.getFirstObjectPos();
        JGoObject first_object = selection.getObjectAtPos(list_position);
        list_position = selection.getNextObjectPosAtTop(list_position);
        JGoObject second_object = selection.getObjectAtPos(list_position);
        
        MemberNodeWidget node_widget_1 = ( MemberNodeWidget ) first_object;
        ALFINode node1 = node_widget_1.getNode();
        MemberNodeProxy mnp1 = node1.memberNodeProxy_getAssociated();
        Point location1 = mnp1.getLocation();

        MemberNodeWidget node_widget_2 = ( MemberNodeWidget ) second_object;
        ALFINode node2 = node_widget_2.getNode();
        MemberNodeProxy mnp2 = node2.memberNodeProxy_getAssociated();
        Point location2 = mnp2.getLocation();
        
        // First swap the locations
        mnp1.setLocation(location2);

        boolean swap_connections = false;
        // Swap the connections
        for (int i = 0; i < connections.length; i++) {

            swap_connections = false;
            GenericPortProxy source = connections[i].getSource();
            MemberNodeProxy source_member = connections[i].getSourceMember();

            if ((source_member == mnp1) || (source_member == mnp2)) {
                MemberNodeProxy swapped_mnp = mnp1;
                ALFINode swapped_node = node1;

                if (source_member == mnp1) {
                    swapped_mnp = mnp2;
                    swapped_node = node2;
                }
                
                source_member = swapped_mnp;

                source = 
                   swap_findPortProxy(source, swapped_node, true);

                swap_connections = true;
            }

            GenericPortProxy sink = connections[i].getSink();
            MemberNodeProxy sink_member = connections[i].getSinkMember();
            if ((sink_member == mnp1) || (sink_member == mnp2)) {
                MemberNodeProxy swapped_mnp = mnp1;
                ALFINode swapped_node = node1;

                if (sink_member == mnp1) {
                    swapped_mnp = mnp2;
                    swapped_node = node2;
                }
                
                sink_member = swapped_mnp;

                sink = swap_findPortProxy(sink, swapped_node, false);

                swap_connections = true;
            }


            if (swap_connections) {
                Integer[] path_definition = connections[i].getPathDefinition();
                int dog_leg_hint = connections[i].getDogLegHint();

                if (sink != null && source != null) {

                    TerminalBundlersConfiguration terminals_config =
                        connections[i].
                            generateCurrentTerminalBundlersConfigurationState(
                                                                        m_node);
                    boolean b_is_bundle = 
                        (connections[i] instanceof BundleProxy); 

                    // Remove connection
                    m_node.connection_remove(connections[i]);
                    
                    addConnection(source, source_member, sink, sink_member, 
                                                path_definition, dog_leg_hint,
                                                b_is_bundle, terminals_config);
                }
            }
        }

        mnp2.setLocation(location1);

    }

    private GenericPortProxy swap_findPortProxy( GenericPortProxy _port,
                                        ALFINode _node, boolean _source_port) {

        String port_name = _port.getName();
        PortProxy [] ports = _node.ports_get();
        for (int i = 0; i < ports.length; i++) {
            if (ports[i].getName().equals(port_name)) {
                if ( _source_port && ports[i].isSourcePort(true) ) {
                    return (GenericPortProxy) ports[i];
                }
                if ( !_source_port && ports[i].isSinkPort(true) ) {
                    return (GenericPortProxy) ports[i];
                }
            }
        }
        return null;
    }


    public boolean allowNodesSwap () {
        JGoSelection selection = m_view.getSelection();

        if (selection.getNumObjects() != 2) {return false; }

        // Get the first node
        JGoListPosition list_position = selection.getFirstObjectPos();
        JGoObject object = selection.getObjectAtPos(list_position);

        if ( ! (object instanceof MemberNodeWidget) )
            return false;

        MemberNodeWidget node1 = ( MemberNodeWidget )object;
        if (!swap_checkNodeType(node1)) return false; 
    
        // Get the second node for the swap
        list_position = selection.getNextObjectPosAtTop(list_position);
        object = selection.getObjectAtPos(list_position);

        if ( ! (object instanceof MemberNodeWidget) )
            return false;

        MemberNodeWidget node2 = ( MemberNodeWidget )object;
        if (!swap_checkNodeType(node2)) return false; 
        
        // Check the port consistencies.
        // First check the number of ports on each node
        PortProxy[] node1_ports = node1.getNode().ports_get();
        PortProxy[] node2_ports = node2.getNode().ports_get();
        if (node1_ports.length != node2_ports.length) return false;

        // Check if the nodes have the same port names and types
        for (int i = 0; i < node1_ports.length; i++) {
            boolean match_found = false;
            for (int j = 0; j < node2_ports.length; j++) {
                if ( (node1_ports[i].getIOType() == node2_ports[j].getIOType()) &&
                     (node1_ports[i].getName().equals(node2_ports[j].getName())) &&
                     (node1_ports[i].getDataType() == node2_ports[j].getDataType()) ) {
                    match_found = true;
                }
            }
            if (!match_found) { return false; }    
        }

        return true;
    }

    private boolean swap_checkNodeType (MemberNodeWidget _node_widget) {
        ALFINode node = _node_widget.getNode();     
    
        MemberNodeProxy mnp = _node_widget.getAssociatedProxy();
        if ( !m_node.memberNodeProxy_containsLocal(mnp) )
            return false;

        return true;
    }

    public void swapX () {
        swapMemberNodeView(MemberNodeProxy.SWAP_X);
    }

    public void swapY () {
        swapMemberNodeView(MemberNodeProxy.SWAP_Y);
    }

    public void swapNone () {
        swapMemberNodeView(MemberNodeProxy.SWAP_NONE);
    }

        /** Implementation details for any member node swap operation. */
    private void swapMemberNodeView(int _new_swap) {
        JGoObject current_widget = m_view.getSelection().getPrimarySelection();
        if (current_widget instanceof MemberNodeWidget) {
            MemberNodeWidget widget = (MemberNodeWidget) current_widget;

                // proxy update
            widget.getAssociatedProxy().setSwap(_new_swap);
        }
    }

    public void symmetryX () {
        symmetryMemberNodeView(MemberNodeProxy.SYMMETRY_X);
    }

    public void symmetryY () {
        symmetryMemberNodeView(MemberNodeProxy.SYMMETRY_Y);
    }

    public void symmetryPlusXY () {
        symmetryMemberNodeView(MemberNodeProxy.SYMMETRY_PLUS_XY);
    }

    public void symmetryMinusXY () {
        symmetryMemberNodeView(MemberNodeProxy.SYMMETRY_MINUS_XY);
    }

    public void symmetryNone () {
        symmetryMemberNodeView(MemberNodeProxy.SYMMETRY_NONE);
    }

        /** Implementation details for any member node symmetry operation. */
    private void symmetryMemberNodeView(int _new_symmetry) {
        JGoObject current_widget = m_view.getSelection().getPrimarySelection();
        if (current_widget instanceof MemberNodeWidget) {
            MemberNodeWidget widget = (MemberNodeWidget) current_widget;

                // proxy update
            widget.getAssociatedProxy().setSymmetry(_new_symmetry);
        }
    }

        /** Called via popup on connection. */
    public void smoothConnections (boolean _allConnections) {
        ConnectionWidget[] connection_widgets = null;
        if (_allConnections) { connection_widgets = getConnectionWidgets(); }
        else {
            JGoObject current_widget = 
                m_view.getSelection().getPrimarySelection();

            if (current_widget instanceof ConnectionWidget) {
                connection_widgets = new ConnectionWidget[1];
                connection_widgets[0] = (ConnectionWidget) current_widget;
            } else {
                connection_widgets = null;
            }
        }

        if (connection_widgets != null) {
            for (int i = 0; i < connection_widgets.length; i++) {
                connection_widgets[i].smooth();
            }
        }
    }

        /** Called via popup on connection. */
    public void deleteConnection () {
        JGoObject current_widget = m_view.getSelection().getPrimarySelection();
        if (current_widget instanceof ConnectionWidget) {
            ConnectionWidget cw = (ConnectionWidget) current_widget;
            ConnectionProxy connection = cw.getAssociatedProxy();

            deleteConnection(cw, true);
        }
    }

    private void deleteConnection (ConnectionWidget _current_widget,
                                     boolean _b_do_downstream_content_update) {
        ConnectionProxy connection = _current_widget.getAssociatedProxy();
        deleteConnection(connection, _b_do_downstream_content_update);
    }

    private void deleteConnection(ConnectionProxy _connection,
                                     boolean _b_do_downstream_content_update) {
        GenericPortProxy source = _connection.getSource();
        if ((source instanceof BundlerProxy) &&
                                 (! _connection.isSourceBundlerPrimaryOut())) {
            BundlerProxy bundler = (BundlerProxy) source;
            if (m_node.bundlerSetting_containsLocal(bundler)) {
                m_node.bundlerSetting_removeLocal(bundler);
            }
        }

        GenericPortProxy sink = _connection.getSink();
        if ((sink instanceof BundlerProxy) &&
                                    (! _connection.isSinkBundlerPrimaryIn())) {
            BundlerProxy bundler = (BundlerProxy) sink;
            if (m_node.bundlerSetting_containsLocal(bundler)) {
                    // true parameter suppresses content cache updating here
                m_node.bundlerSetting_removeLocal(bundler, true);
            }
        }

        MemberNodeProxy owner_member = _connection.getSinkMember();

        m_node.connection_remove(_connection);

        if (_b_do_downstream_content_update) {
            sink.doContentUpdate(m_node, owner_member);
        }
    }

        /** Called via popup on connection. */
    public void showBundleContent () {
        JGoObject current_widget = m_view.getSelection().getPrimarySelection();
        if (current_widget instanceof ConnectionWidget) {
            showBundleContent((ConnectionWidget) current_widget);
        }
    }

    void showBundleContent (ConnectionWidget _current_widget) {
        ConnectionProxy connection = _current_widget.getAssociatedProxy();
        if (connection instanceof BundleProxy) {
            BundleProxy bundle = (BundleProxy) connection;

            ShowBundleContentDialog.showDialog(this,
                                              bundle.getContent(m_node, true));
        }
    }
   
    /** Get one of the document's layers. */
    public JGoLayer getDocumentLayer (int _layer_id) {
        if ((_layer_id >= 0) && (_layer_id < INVALID_LAYER_ID)) {
            return m_layers[_layer_id];
        }
        else { return null; }
    }

    private void initializeInternalView_addPorts () {
        JGoLayer layer_to_add_to = m_layers[EXTERNAL_PORT_LAYER];
            // if positions are skipped, some slots in ports will be null
        PortProxy[][] ports = m_node.ports_getByPosition(true);
        Point[][] points = calculateInternalViewPortLocations(ports);

        for (int i = 0; i < ports.length; i++) {
            for (int j = 0; j < ports[i].length; j++) {
                if (ports[i][j] != null) {
                    PortWidget port_widget =
                               new PortWidget(this, points[i][j], ports[i][j]);
                    layer_to_add_to.addObjectAtTail(port_widget);

                        // this session needs to listen for changes to edges
                    ports[i][j].addProxyChangeListener(this);
                }
            }
        }
    }

    private void updatePortPositionsAfterBorderReposition() {
        PortWidget[] widgets = getPortWidgets_onContainer();
        for (int i = 0; i < widgets.length; i++) {
            int edge_id = widgets[i].getAssociatedProxy().getPosition()[0];
            if ((edge_id == PortProxy.EDGE_NORTH) ||
                              (edge_id == PortProxy.EDGE_SOUTH)) {
                if (widgets[i].getTop() != m_port_borders[edge_id].getTop()) {
                    widgets[i].setTop(m_port_borders[edge_id].getTop());
                }
            }
            else if ((edge_id == PortProxy.EDGE_EAST) ||
                              (edge_id == PortProxy.EDGE_WEST)) {
                if (widgets[i].getLeft() != m_port_borders[edge_id].getLeft()) {
                    widgets[i].setLeft(m_port_borders[edge_id].getLeft());
                }
            }
        }
    }

        /** Checks that all ports are currently in the right postion.  If not,
          * they are moved into the correct positions.
          */
    private void updatePortPositionsAfterPortOrderChange() {
        if (m_internal_view) {
            PortProxy[][] ports = m_node.ports_getByPosition(true);
            Point[][] points = calculateInternalViewPortLocations(ports);
            PortWidget[] widgets = getPortWidgets_onContainer();
            for (int i = 0; i < ports.length; i++) {
                for (int j = 0; j < ports[i].length; j++) {
                    if (ports[i][j] == null) { continue; }

                    for (int k = 0; k < widgets.length; k++) {
                        if (ports[i][j] == widgets[k].getAssociatedProxy()) {
                            if (!widgets[k].getLocation().equals(points[i][j])){
                                widgets[k].setLocation(points[i][j]);
                                widgets[k].setPortShape();
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

        /** Calculates all possible port positions, dependent only on the
          * current dimensions of the port borders.
          */
    private Point[][] calculateInternalViewPortLocations() {
        return calculateInternalViewPortLocations(null);
    }

        /** This method calculates the locations to place the ports on the edges
          * of an internal view of this container node.  Locations are
          * calculated even if there is no port currently at a particular
          * position.  If _ports is passed as null, then all possible port
          * positions are returned, dependent only on the current dimensions of
          * the port borders.
          *
          * If the current view is external, null is returned.
          */
    private Point[][] calculateInternalViewPortLocations(PortProxy[][] _ports) {
        if (m_internal_view) {
            int port_spacer = PortWidget.PORT_SLOT_SPACING;
            int offset = GridTool.GRID_SPACING * 3 + 1;

            Point[][] points = new Point[4][];
            if (_ports != null) {
                    // make the points arrays the same size as the _ports arrays
                for (int i = 0; i < points.length; i++) {
                    points[i] = new Point[_ports[i].length];
                }
            }
            else {
                    // make points arrays large enough to encompass entire edges
                int width = m_port_borders[PortProxy.EDGE_NORTH].getWidth();
                int position_count = (width - (2 * offset)) / port_spacer;
                points[PortProxy.EDGE_NORTH] = new Point[position_count];
                points[PortProxy.EDGE_SOUTH] = new Point[position_count];

                int height = m_port_borders[PortProxy.EDGE_EAST].getHeight();
                position_count = (height - (2 * offset)) / port_spacer;
                points[PortProxy.EDGE_EAST] = new Point[position_count];
                points[PortProxy.EDGE_WEST] = new Point[position_count];
            }

            int x, y;
            for (int edge = 0; edge < points.length; edge++) {
                for (int slot = 0; slot < points[edge].length; slot++) {
                    if (edge == PortProxy.EDGE_NORTH) {
                        x = offset + (port_spacer * slot);
                        y = m_port_borders[PortProxy.EDGE_NORTH].getTop();
                    }
                    else if (edge == PortProxy.EDGE_SOUTH) {
                        x = offset + (port_spacer * slot);
                        y = m_port_borders[PortProxy.EDGE_SOUTH].getTop();
                    }
                    else if (edge == PortProxy.EDGE_EAST) {
                        x = m_port_borders[PortProxy.EDGE_EAST].getLeft();
                        y = offset + (port_spacer * slot);
                    }
                    else if (edge == PortProxy.EDGE_WEST) {
                        x = m_port_borders[PortProxy.EDGE_WEST].getLeft();
                        y = offset + (port_spacer * slot);
                    }
                    else {
                        System.err.println("EditorFrame." +
                            "calculateInternalViewPortLocations(...):\n" +
                            "\tWARNING: Invalid port placement edge: " + edge);
                        continue;
                    }

                    points[edge][slot] = new Point(x, y);
                }
            }

            return points;
        }
        else { return null; }
    }

    private void replaceBorders(Rectangle _edit_rect) {
        JGoLayer border_layer = m_layers[BORDER_LAYER];
        border_layer.removeAll();

        for (int edge = 0; edge < PortProxy.EDGE_INVALID; edge++) {
             m_port_borders[edge] = new PortBorder(m_view, _edit_rect, edge);
             border_layer.addObjectAtHead(m_port_borders[edge]);
        }
        updatePortPositionsAfterBorderReposition();
    }

    public int getClosestPortBorder(Point _point) {
        int closest_edge = PortProxy.EDGE_INVALID;
        int minimum_distance = Integer.MAX_VALUE;
        Rectangle r;
        int d;
        if (isInternalView()) {
            r = m_port_borders[PortProxy.EDGE_NORTH].getBoundingRect();
            d = Math.abs(_point.y - (r.y + r.height));
            if (d < minimum_distance) {
                minimum_distance = d;
                closest_edge = PortProxy.EDGE_NORTH;
            }

            r = m_port_borders[PortProxy.EDGE_SOUTH].getBoundingRect();
            d = Math.abs(_point.y - r.y);
            if (d < minimum_distance) {
                minimum_distance = d;
                closest_edge = PortProxy.EDGE_SOUTH;
            }

            r = m_port_borders[PortProxy.EDGE_EAST].getBoundingRect();
            d = Math.abs(_point.x - r.x);
            if (d < minimum_distance) {
                minimum_distance = d;
                closest_edge = PortProxy.EDGE_EAST;
            }

            r = m_port_borders[PortProxy.EDGE_WEST].getBoundingRect();
            d = Math.abs(_point.x - (r.x + r.width));
            if (d < minimum_distance) {
                minimum_distance = d;
                closest_edge = PortProxy.EDGE_WEST;
            }
        }
        else {
            r = getMemberNodeWidgets()[0].getBoundingRect();

            d = Math.abs(_point.y - r.y);
            if (d < minimum_distance) {
                minimum_distance = d;
                closest_edge = PortProxy.EDGE_NORTH;
            }

            d = Math.abs(_point.y - (r.y + r.height));
            if (d < minimum_distance) {
                minimum_distance = d;
                closest_edge = PortProxy.EDGE_SOUTH;
            }

            d = Math.abs(_point.x - (r.x + r.width));
            if (d < minimum_distance) {
                minimum_distance = d;
                closest_edge = PortProxy.EDGE_EAST;
            }

            d = Math.abs(_point.x - r.x);
            if (d < minimum_distance) {
                minimum_distance = d;
                closest_edge = PortProxy.EDGE_WEST;
            }
        }

        return closest_edge;
    }

    private void initializeInternalView_addBottomColoredBoxes() {
        JGoLayer back_layer_to_add_to = m_layers[BACK_HIGHLIGHT_LAYER];

        ColoredBox[] back_highlights = m_node.coloredBoxes_get();

        for (int i = 0; i < back_highlights.length; i++) {
            if (back_highlights[i].getLayer() == ColoredBox.HILITE_LAYER_BACK) {
                back_layer_to_add_to.addObjectAtTail(
                                new ColoredBoxWidget(this, back_highlights[i]));
            }
        }
    }

    private void initializeInternalView_addMemberNodes () {
        JGoLayer layer_to_add_to = m_layers[MEMBER_NODE_LAYER];

        MemberNodeProxy[] proxies = m_node.memberNodeProxies_get();
        for (int i = 0; i < proxies.length; i++) {
            MemberNodeProxy proxy = proxies[i];

            ALFINode associated_node = proxy.getAssociatedMemberNode(m_node);

            MemberNodeWidget mnw =
                            new MemberNodeWidget(this, associated_node, false);

            mnw.initialize();

            layer_to_add_to.addObjectAtTail(mnw);
        }
    }

    private void initializeInternalView_addBundlers () {
        JGoLayer layer_to_add_to = m_layers[BUNDLER_LAYER];

        BundlerProxy[] bundlers = m_node.bundlers_get();
        for (int i = 0; i < bundlers.length; i++) {
            layer_to_add_to.addObjectAtTail(
                                         new BundlerWidget(this, bundlers[i]));
        }
    }

    private void initializeInternalView_addJunctions () {
        JGoLayer layer_to_add_to = m_layers[JUNCTION_LAYER];

        JunctionProxy[] junctions = m_node.junctions_get();
        for (int i = 0; i < junctions.length; i++) {
            layer_to_add_to.addObjectAtTail(
                                     new JunctionWidget(this, junctions[i]));
        }
    }

    private void initializeInternalView_addConnections () {
        JGoLayer layer_to_add_to = m_layers[CONNECTION_LAYER];

        ConnectionProxy[] connections = m_node.connections_get();
        for (int i = 0; i < connections.length; i++) {
            ConnectionProxy connection = connections[i];

            ConnectionWidget cw = new ConnectionWidget(this, connection);
            m_connections_map.put(connection, cw);
            layer_to_add_to.addObjectAtTail(cw);
            if (cw.isLocal()) {
                cw.addConnectionRedrawnEventListener(this);
            }
        }
    }


    private void initializeInternalView_addTopColoredBoxes() {
        JGoLayer front_layer_to_add_to = m_layers[TOP_HIGHLIGHT_LAYER];

        ColoredBox[] top_highlights = m_node.coloredBoxes_get();
        for (int i = 0; i < top_highlights.length; i++) {
            if (top_highlights[i].getLayer() == ColoredBox.HILITE_LAYER_FRONT) {
                front_layer_to_add_to.addObjectAtTail(
                                 new ColoredBoxWidget(this, top_highlights[i]));
            }
        }
    }

    private void initializeInternalView_addTextComments() {
        JGoLayer text_layer_to_add_to = m_layers[TEXT_LAYER];

        TextComment[] comments = m_node.textComments_get();
        for (int i = 0; i < comments.length; i++) {
            TextCommentWidget widget = new TextCommentWidget(this, comments[i]);
            text_layer_to_add_to.addObjectAtTail(widget);
        }
    }

    private void initializeInternalView_registerGroups() {
        
        LayeredGroup[] groups = m_node.layeredGroups_get();
        
        for (int i = 0; i < groups.length; i++) {
            for (int j = 0; j < groups[i].numElements(); j++) {
                GroupableIF element = groups[i].getElement(j);     
                element.group_registerListener(this);

                if (element instanceof ColoredBox) {
                    ColoredBox colored_box = ( ColoredBox )element;
                    ColoredBoxWidget cbw = 
                                 determineColoredBoxWidget(colored_box);
                    if (colored_box.getLayer() == ColoredBox.HILITE_LAYER_FRONT) {
                        JGoListPosition pos = 
                            m_layers[TOP_HIGHLIGHT_LAYER].findObject(cbw);
                        m_layers[TOP_HIGHLIGHT_LAYER].removeObjectAtPos(pos);
                        m_layers[TOP_HIGHLIGHT_LAYER].addObjectAtHead(cbw);
                    }
                    else {
                        JGoListPosition pos = 
                            m_layers[BACK_HIGHLIGHT_LAYER].findObject(cbw);
                        m_layers[BACK_HIGHLIGHT_LAYER].removeObjectAtPos(pos);
                        m_layers[BACK_HIGHLIGHT_LAYER].addObjectAtHead(cbw);
                    }
                }
            }
        }
    }
        /** This member is used to check for window resizes via the upper left
          * corner handle.
          */
    public void initializePreviousScreenPosition() {
        m_previous_screen_position = ((Component) this).getLocation();
    }

        /**
         * Recompute the size of the view and determine if scrollbars should be
         * placed and fit everything in the current size of the edit window.
         *
         * If m_previous_screen_position has not yet been set, then ignore this
         * call.
         */
    public void updateView() {
        if (m_previous_screen_position == null) { return; }

        Point new_window_position = ((Component) this).getLocation();

        int dx_raw = m_previous_screen_position.x - new_window_position.x;
        int dy_raw = m_previous_screen_position.y - new_window_position.y;

/*  this continues to cause an amazing number of unpredictable problems

            // only return grid points 2x usual for integral external port moves
        Point dP_grid =
                    GridTool.getNearestGridPoint(new Point(dx_raw, dy_raw), 2);
        moveAll(dP_grid);
*/

            // the minumum rectangle encompassing all fixed position components.
        Rectangle minimum_view_rect = m_view.determineMinimumRect();

            // will this fit inside the frame without need for scrollbars?
        Dimension working_panel_size = getWorkingPanel().getSize();

        m_view.setViewPosition(minimum_view_rect.x, minimum_view_rect.y);

            // save the frame size
        Dimension new_frame_size = getSize();
        Dimension default_frame_size = determineIdealFrameSize();
    
        if (m_internal_view) {
                // determine where the port borders should be
            Rectangle border_rect = new Rectangle(
                minimum_view_rect.x,
                minimum_view_rect.y,
                Math.max(minimum_view_rect.width, (working_panel_size.width -
                                     m_view.getVerticalScrollBar().getWidth())),
                Math.max(minimum_view_rect.height, (working_panel_size.height -
                                m_view.getHorizontalScrollBar().getHeight())) );
            replaceBorders(border_rect);

            setDefaultWindowSize(new_frame_size);
            m_node.setFrameSize(new_frame_size);
        }
        else {
            if ( (working_panel_size.getWidth() < 0) || 
                 (working_panel_size.getHeight() < 0) ) {
                setDefaultWindowSize(default_frame_size);
                resetSize();    // apply the current default window size
            } 
        }

        m_previous_screen_position = new_window_position;
    }

        /** Translates all elements by _dP. */
/*
    private void moveAll(Point _dP) {
            // shift local edge ports
        int x_shift_mode = (_dP.x > 0) ? KeyEvent.VK_RIGHT : KeyEvent.VK_LEFT;
        int y_shift_mode = (_dP.y > 0) ? KeyEvent.VK_DOWN : KeyEvent.VK_UP;
        int[] shift_mode = new int[4];
        shift_mode[PortProxy.EDGE_EAST] = y_shift_mode;
        shift_mode[PortProxy.EDGE_WEST] = y_shift_mode;
        shift_mode[PortProxy.EDGE_NORTH] = x_shift_mode;
        shift_mode[PortProxy.EDGE_SOUTH] = x_shift_mode;

        int use_push_option = InputEvent.META_MASK;

        int x_shift = Math.abs(Math.round(
                      (float) _dP.x / (float) (GridTool.GRID_SPACING * 2) ));
        int y_shift = Math.abs(Math.round(
                      (float) _dP.y / (float) (GridTool.GRID_SPACING * 2) ));
        int[] shift = new int[4];
        shift[PortProxy.EDGE_EAST] = y_shift;
        shift[PortProxy.EDGE_WEST] = y_shift;
        shift[PortProxy.EDGE_NORTH] = x_shift;
        shift[PortProxy.EDGE_SOUTH] = x_shift;

        PortProxy[] first_ports = m_node.ports_getFirstLocalPortsByEdge(true);
        for (int edge = 0; edge < PortProxy.EDGE_INVALID; edge++) {
            if (first_ports[edge] != null) {
                for (int i = 0; i < shift[edge]; i++) {
                    m_view.performPortMove(first_ports[edge],
                                           shift_mode[edge], use_push_option);
                }
            }
        }

            // shift other local elements
        MemberNodeProxy[] members = m_node.memberNodeProxies_getLocal();
        for (int i = 0; i < members.length; i++) {
            Point location = members[i].getLocation();
            location.translate(_dP.x, _dP.y);
            members[i].setLocation(location);
        }
        JunctionProxy[] junctions = m_node.junctions_getLocal();
        for (int i = 0; i < junctions.length; i++) {
            Point location = junctions[i].getLocation();
            location.translate(_dP.x, _dP.y);
            junctions[i].setLocation(location);
        }
        BundlerProxy[] bundlers = m_node.bundlers_getLocal();
        for (int i = 0; i < bundlers.length; i++) {
            Point location = bundlers[i].getLocation();
            location.translate(_dP.x, _dP.y);
            bundlers[i].setLocation(location);
        }

            // shift local connections
        ConnectionProxy[] connections = m_node.connections_getLocal();
        for (int i = 0; i < connections.length; i++) {
            connections[i].translate(_dP.x, _dP.y);
        }
    }
*/

        /** This method determines the right frame size to just fit all the
          * Alfi widgets in the view with no need for scrollbars.  Determines
          * the default view rectangle.
          */
    private Dimension determineIdealFrameSize() {
        return determineIdealFrameSize(m_view.determineMinimumRect());
    }

        /** This method determines the right frame size to just fit all the
          * Alfi widgets in the view with no need for scrollbars.
          */
    private Dimension determineIdealFrameSize(Rectangle _minimum_view_rect) {
        Dimension main_frame_size = getParentFrame().getSize();
        Dimension content_size = getParentFrame().getContentPane().getSize();

        int extra_width = main_frame_size.width - content_size.width;
        int extra_height = main_frame_size.height - content_size.height;

        extra_height += getParentFrame().getStatusBar().getHeight();

            // add space for scroll bars
        extra_width += m_view.getVerticalScrollBar().getWidth();
        extra_height += m_view.getHorizontalScrollBar().getHeight();

        if ((_minimum_view_rect.width == 0)||(_minimum_view_rect.height == 0)) {
            _minimum_view_rect = new Rectangle(0, 0, 200, 200);
        }

        Dimension ideal_frame_size= new Dimension(_minimum_view_rect.getSize());
        ideal_frame_size.width += extra_width;
        ideal_frame_size.height += extra_height;

            // make sure it fits on the screen
        Dimension screen_size = Toolkit.getDefaultToolkit().getScreenSize();
        if (ideal_frame_size.width > screen_size.width) {
            ideal_frame_size.width = screen_size.width - 50;
        }
        if (ideal_frame_size.height > screen_size.height) {
            ideal_frame_size.height = screen_size.height - 50;
        }

        return ideal_frame_size;
    }

        /** Gets all the external ports in the external ports layer. */
    private PortWidget[] getPortWidgets_onContainer () {
        JGoLayer port_layer = m_layers[EXTERNAL_PORT_LAYER];
        ArrayList port_list = new ArrayList();

        JGoListPosition list_position = port_layer.getFirstObjectPos();
        while (list_position != null) {
            port_list.add(port_layer.getObjectAtPos(list_position));
            list_position = port_layer.getNextObjectPosAtTop(list_position);
        }

        PortWidget[] ports = new PortWidget[port_list.size()];
        port_list.toArray(ports);
        return ports;
    }

        /** Gets all the port widgets on member node widgets in this session. */
    private PortWidget[] getPortWidgets_onMemberNodes () {
        JGoLayer member_layer = m_layers[MEMBER_NODE_LAYER];
        ArrayList port_list = new ArrayList();

            // this will iterate down through all sub-objects as well in
            // JGoAreas, so it will find ports in the member node widgets
        JGoListPosition list_position = member_layer.getFirstObjectPos();
        while (list_position != null) {
            JGoObject object = member_layer.getObjectAtPos(list_position);
            if (object instanceof PortWidget) { port_list.add(object); }
            list_position = member_layer.getNextObjectPos(list_position);
        }

        PortWidget[] ports = new PortWidget[port_list.size()];
        port_list.toArray(ports);
        return ports;
    }

        /**
         * Gets all port widgets on the container and on member nodes in this
         * session.
         */
    private PortWidget[] getPortWidgets_all () {
        PortWidget[] external_ports = getPortWidgets_onContainer();
        PortWidget[] member_node_ports = getPortWidgets_onMemberNodes();

        PortWidget[] ports =
              new PortWidget[external_ports.length + member_node_ports.length];
        System.arraycopy(external_ports, 0, ports, 0, external_ports.length);
        System.arraycopy(member_node_ports, 0, ports, external_ports.length,
                                                      member_node_ports.length);
        return ports;
    }

        /** Gets all the connection widgets in the connection layer. */
    public ConnectionWidget[] getConnectionWidgets() {
        JGoLayer connection_layer = m_layers[CONNECTION_LAYER];
        ArrayList connection_list = new ArrayList();

        JGoListPosition pos = connection_layer.getFirstObjectPos();
        while (pos != null) {
            connection_list.add(connection_layer.getObjectAtPos(pos));
            pos = connection_layer.getNextObjectPosAtTop(pos);
        }

        ConnectionWidget[] connections =
                    new ConnectionWidget[connection_list.size()];
        connection_list.toArray(connections);
        return connections;
    }

        /** Gets all the junction widgets in the junction layer. */
    public JunctionWidget[] getJunctionWidgets () {
        JGoLayer junction_layer = m_layers[JUNCTION_LAYER];
        ArrayList junction_list = new ArrayList();

        JGoListPosition list_position = junction_layer.getFirstObjectPos();
        while (list_position != null) {
            junction_list.add(junction_layer.getObjectAtPos(list_position));
            list_position = junction_layer.getNextObjectPosAtTop(list_position);
        }

        JunctionWidget[] junctions = new JunctionWidget[junction_list.size()];
        junction_list.toArray(junctions);
        return junctions;
    }

        /** Gets all the bundler widgets in the bundler layer. */
    public BundlerWidget[] getBundlerWidgets () {
        JGoLayer bundler_layer = m_layers[BUNDLER_LAYER];
        ArrayList bundler_list = new ArrayList();

        JGoListPosition list_position = bundler_layer.getFirstObjectPos();
        while (list_position != null) {
            bundler_list.add(bundler_layer.getObjectAtPos(list_position));
            list_position = bundler_layer.getNextObjectPosAtTop(list_position);
        }

        BundlerWidget[] bundler_widgets= new BundlerWidget[bundler_list.size()];
        bundler_list.toArray(bundler_widgets);
        return bundler_widgets;
    }

        /** Gets all the MemberNodeWidgets in the MemberNodeWidget layer. */
    public MemberNodeWidget[] getMemberNodeWidgets () {
        JGoLayer member_node_layer = m_layers[MEMBER_NODE_LAYER];
        ArrayList member_node_list = new ArrayList();

        JGoListPosition list_position = member_node_layer.getFirstObjectPos();
        while (list_position != null) {
            JGoObject widget = member_node_layer.getObjectAtPos( list_position);
            member_node_list.add(widget);
            list_position = member_node_layer.getNextObjectPosAtTop(
                                                           list_position);
        }

        MemberNodeWidget[] member_node_widgets =
                    new MemberNodeWidget[member_node_list.size()];
        member_node_list.toArray(member_node_widgets);
        return member_node_widgets;
    }

        /** Gets all the TextCommentWidget in the Comment layers. */
    public TextCommentWidget[] getTextCommentWidgets () {
        JGoLayer text_layer = m_layers[TEXT_LAYER];
        ArrayList comment_list = new ArrayList();

        JGoListPosition list_position = text_layer.getFirstObjectPos();
        while (list_position != null) {
            JGoObject widget = text_layer.getObjectAtPos(list_position);
            comment_list.add(widget);
            list_position = text_layer.getNextObjectPosAtTop(
                                                           list_position);
        }

        TextCommentWidget[] comment_widgets =
                    new TextCommentWidget[comment_list.size()];
        comment_list.toArray(comment_widgets);
        return comment_widgets;
    }

        /** Gets all the ColoredBoxWidgets in the Comment layers. */
    public ColoredBoxWidget[] getColoredBoxWidgets () {
        JGoLayer back_highlight_layer = m_layers[BACK_HIGHLIGHT_LAYER];
        ArrayList widget_list = new ArrayList();

        JGoListPosition list_position = back_highlight_layer.getFirstObjectPos();
        while (list_position != null) {
            JGoObject widget = back_highlight_layer.getObjectAtPos(list_position);
            widget_list.add(widget);
            list_position = back_highlight_layer.getNextObjectPosAtTop(
                                                           list_position);
        }

        JGoLayer top_highlight_layer = m_layers[TOP_HIGHLIGHT_LAYER];

        list_position = top_highlight_layer.getFirstObjectPos();
        while (list_position != null) {
            JGoObject widget = top_highlight_layer.getObjectAtPos(list_position);
            widget_list.add(widget);
            list_position = top_highlight_layer.getNextObjectPosAtTop(
                                                           list_position);
        }

        ColoredBoxWidget[] cb_widgets =
                    new ColoredBoxWidget[widget_list.size()];
        widget_list.toArray(cb_widgets);
        return cb_widgets;
    }

    public JPopupMenu getNodeMenu() {
        if (m_node_menu == null) {
            m_node_menu = new NodePopupMenu(m_actions, getResources());
        }
        return m_node_menu;
    }
    public JPopupMenu getPortMenu() {
        if (m_port_menu == null) {
            m_port_menu = new PortPopupMenu(m_actions);
        }
        return m_port_menu;
    }
    public JPopupMenu getConnectionsMenu() {
        if (m_connection_menu == null) {
            m_connection_menu = new ConnectionPopupMenu(m_actions);
        }
        return m_connection_menu;
    }
    public JPopupMenu getJunctionsMenu() {
        if (m_junction_menu == null) {
            m_junction_menu = new JunctionPopupMenu(m_actions);
        }
        return m_junction_menu;
    }
    public JPopupMenu getBundlersMenu() {
        if (m_bundler_menu == null) {
            m_bundler_menu = new BundlerPopupMenu(m_actions);
        }
        return m_bundler_menu;
    }
    public JPopupMenu getCommentMenu() {
        if (m_comment_menu == null) {
            m_comment_menu = new CommentPopupMenu(m_actions, getResources());
        }
        return m_comment_menu;
    }

    public JPopupMenu getHighlightMenu() {
        if (m_highlight_menu == null) {
            m_highlight_menu = new HighlightPopupMenu(m_actions, getResources());
        }
        return m_highlight_menu;
    }

        /** Determines the source port or junction widget for a connection. */
    public GenericPortWidget determineSourceWidget(
                                                 ConnectionProxy _connection) {
        return determineGenericPortWidget(_connection.getSource(),
                                          _connection.getSourceMember());
    }

        /** Determines the sink port widget for a connection. */
    public GenericPortWidget determineSinkWidget(ConnectionProxy _connection) {
        return determineGenericPortWidget(_connection.getSink(),
                                          _connection.getSinkMember());
    }

        /** Find the member widget associated with this proxy in this node. */
    public MemberNodeWidget determineMemberWidget(MemberNodeProxy _member) {
        MemberNodeWidget[] mnws = getMemberNodeWidgets();
        for (int i = 0; i < mnws.length; i++) {
            if (mnws[i].getAssociatedProxy() == _member) {
                return mnws[i];
            }
        }
        return null;
    }

        /** Find the comment widget associated with this proxy in this node. */
    public TextCommentWidget determineTextCommentWidget(TextComment _comment) {
        TextCommentWidget[] comments = getTextCommentWidgets();
        for (int i = 0; i < comments.length; i++) {
            if (comments[i].getAssociatedProxy() == _comment) {
                return comments[i];
            }
        }
        return null;
    }

        /** Find the coloredbox widget associated with this proxy in this node. */
    public ColoredBoxWidget determineColoredBoxWidget(ColoredBox _bg) {
        ColoredBoxWidget[] bgs = getColoredBoxWidgets();
        for (int i = 0; i < bgs.length; i++) {
            if (bgs[i].getAssociatedProxy() == _bg) {
                return bgs[i];
            }
        }
        return null;
    }

        /** Determines a generic port widget (port, junction, bundler.) */
    public GenericPortWidget determineGenericPortWidget(GenericPortProxy _gpp,
                                               MemberNodeProxy _owner_member) {
        if (_gpp instanceof PortProxy) {
            return determinePortWidget((PortProxy) _gpp, _owner_member);
        }
        else if (_gpp instanceof JunctionProxy) {
            return determineJunctionWidget((JunctionProxy) _gpp);
        }
        else if (_gpp instanceof BundlerProxy) {
            return determineBundlerWidget((BundlerProxy) _gpp);
        }
        else { return null; }
    }

        /** Determines a port widget in this container from port and member
          * node proxies.
          */
    public PortWidget determinePortWidget(PortProxy _port,
                                                 MemberNodeProxy _member_node) {
        if (_member_node != null) {
            PortWidget[] port_widgets = getPortWidgets_onMemberNodes();
            for (int j = 0; j < port_widgets.length; j++) {
                if ((port_widgets[j].getOwnerMemberWidget().getAssociatedProxy()
                                                             == _member_node) &&
                              (port_widgets[j].getAssociatedProxy() == _port)) {
                    return port_widgets[j];
                }
            }
        }
        else {
            PortWidget[] port_widgets = getPortWidgets_onContainer();
            for (int j = 0; j < port_widgets.length; j++) {
                if (port_widgets[j].getAssociatedProxy() == _port) {
                    return port_widgets[j];
                }
            }
        }

        return null;
    }

        /** Find the junction widget associated with this proxy in this node. */
    public JunctionWidget determineJunctionWidget(JunctionProxy _junction) {
        JunctionWidget[] junction_widgets = getJunctionWidgets();
        for (int i = 0; i < junction_widgets.length; i++) {
            if (junction_widgets[i].getAssociatedProxy() == _junction) {
                return junction_widgets[i];
            }
        }
        return null;
    }

        /** Find the bundler widget associated with this proxy in this node. */
    public BundlerWidget determineBundlerWidget(BundlerProxy _bundler) {
        BundlerWidget[] bundler_widgets = getBundlerWidgets();
        for (int i = 0; i < bundler_widgets.length; i++) {
            if (bundler_widgets[i].getAssociatedProxy() == _bundler) {
                return bundler_widgets[i];
            }
        }
        return null;
    }

        /** Find the connection widget associated with this proxy in node. */
    public ConnectionWidget determineConnectionWidget(
                                                 ConnectionProxy _connection) {
//MiscTools.stackTrace("(" + this + ").determineConnectionWidget(" + _connection + "):", 5);

        return (ConnectionWidget) m_connections_map.get(_connection);

/*
        ConnectionWidget[] cws = getConnectionWidgets();
        for (int i = 0; i < cws.length; i++) {
            if (cws[i].getAssociatedProxy() == _connection) {
                return cws[i];
            }
        }
        return null;
*/
    }

    public Object[] JGoSelectionToProxyObjectList(JGoSelection _sel, 
                                                  boolean _b_top_level_only) {
        ArrayList list = new ArrayList();
        if (_sel!= null) {
            JGoListPosition pos = _sel.getFirstObjectPos();
            while (pos != null) {
                list.add(_sel.getObjectAtPos(pos));
                pos = (_b_top_level_only) ?
                                 _sel.getNextObjectPosAtTop(pos) :
                                 _sel.getNextObjectPos(pos);
            }
        }

        Object[] obj_array = new Object[list.size()];
        list.toArray(obj_array);
        return obj_array;
    }

    /** Selects all the widgets which are a part of the LayeredGroup */
    public void group_select(LayeredGroup _group, Object _source) {
        JGoSelection selection = getView().getSelection();

        for (int i = 0; i < _group.numElements(); i++) {
            GroupableIF element = _group.getElement(i);
            if (element instanceof TextComment) {
                TextCommentWidget widget = determineTextCommentWidget(
                                    ( TextComment )element);
                if (widget != _source) {
                    selection.extendSelection(widget);
                }
            }
            else if (element instanceof ColoredBox) {
                ColoredBoxWidget widget = determineColoredBoxWidget(
                                    ( ColoredBox )element);
                if (widget != _source) {
                    selection.extendSelection(widget);
                }
            }
            else if (element instanceof JunctionProxy) {
                JunctionWidget widget = determineJunctionWidget(
                                    ( JunctionProxy )element);
                if (widget != _source) {
                    selection.extendSelection(widget);
                }
            }
            else if (element instanceof BundlerProxy) {
                BundlerWidget widget = determineBundlerWidget(
                                    ( BundlerProxy )element);
                if (widget != _source) {
                    selection.extendSelection(widget);
                }
            }
            else if (element instanceof ALFINode) {
                ALFINode node = ( ALFINode )element;
                MemberNodeProxy proxy = node.memberNodeProxy_getAssociated();
                 
                MemberNodeWidget widget = determineMemberWidget(proxy);
                if (widget != _source) {
                    selection.extendSelection(widget);
                }
            }
        }
    }

    // TextComment Actions

    /** Changes the size of the TextComment. Called via popup */
    public void setTextSize (int _size) {

        JGoSelection selection = m_view.getSelection();

        JGoListPosition pos = selection.getFirstObjectPos();
        while (pos != null) {
            JGoObject current_widget = selection.getObjectAtPos(pos);

            if (current_widget instanceof TextCommentWidget ) {
                setTextSize((TextCommentWidget) current_widget, _size);
            }
            pos = selection.getNextObjectPos(pos);
        }
    }

    public void setTextSize (TextCommentWidget _widget, int _size) {
        
        TextComment text_comment = _widget.getAssociatedProxy();
        text_comment.setFontSize(_size);
    }

    /** Changes the value of the TextComment */
    public void setTextValue () { 
        JGoSelection selection = m_view.getSelection();

        JGoListPosition pos = selection.getFirstObjectPos();
        while (pos != null) {
            JGoObject current_widget = selection.getObjectAtPos(pos);

            if (current_widget instanceof TextCommentWidget ) {
                setTextValue((TextCommentWidget) current_widget);
            }
            pos = selection.getNextObjectPos(pos);
        }
    }

    public void setTextValue (TextCommentWidget _widget) {
        TextComment text_comment = _widget.getAssociatedProxy();
        editTextComment(this, text_comment);
    }

    /** Changes the Color of the TextComment. Called via popup */
    public void setTextColor (int _color_id) {

        JGoSelection selection = m_view.getSelection();

        JGoListPosition pos = selection.getFirstObjectPos();
        while (pos != null) {
            JGoObject current_widget = selection.getObjectAtPos(pos);

            if (current_widget instanceof TextCommentWidget ) {
                setTextColor((TextCommentWidget) current_widget, _color_id);
            }
            pos = selection.getNextObjectPos(pos);
        }
    }

    public void setTextColor (TextCommentWidget _widget, int _color_id) {
        
        TextComment text_comment = _widget.getAssociatedProxy();
        text_comment.setColor(_color_id);

    }

    /** Toggle the Boldness attribute of the TextComment. Called via popup */
    public void setTextBold () {

        JGoSelection selection = m_view.getSelection();

        JGoListPosition pos = selection.getFirstObjectPos();
        while (pos != null) {
            JGoObject current_widget = selection.getObjectAtPos(pos);

            if (current_widget instanceof TextCommentWidget ) {
                setTextBold((TextCommentWidget) current_widget);
            }
            pos = selection.getNextObjectPos(pos);
        }
    }

    public void setTextBold(TextCommentWidget _widget) {
        
        TextComment text_comment = _widget.getAssociatedProxy();
        boolean b_bold = text_comment.isBold();
        text_comment.setBold(!b_bold);

    }

    /** Toggles the Italic attribute of the TextComment. Called via popup */
    public void setTextItalic () {

        JGoSelection selection = m_view.getSelection();

        JGoListPosition pos = selection.getFirstObjectPos();
        while (pos != null) {
            JGoObject current_widget = selection.getObjectAtPos(pos);

            if (current_widget instanceof TextCommentWidget ) {
                setTextItalic((TextCommentWidget) current_widget);
            }
            pos = selection.getNextObjectPos(pos);
        }
    }

    public void setTextItalic (TextCommentWidget _widget) {
        
        TextComment text_comment = _widget.getAssociatedProxy();
        boolean b_italic = text_comment.isItalic();
        text_comment.setItalic(!b_italic);

    }

    /** Toggles the Underline attribute of the TextComment. Called via popup */
    public void setTextUnderline() {

        JGoSelection selection = m_view.getSelection();

        JGoListPosition pos = selection.getFirstObjectPos();
        while (pos != null) {
            JGoObject current_widget = selection.getObjectAtPos(pos);

            if (current_widget instanceof TextCommentWidget ) {
                setTextUnderline((TextCommentWidget) current_widget);
            }
            pos = selection.getNextObjectPos(pos);
        }
    }

    public void setTextUnderline (TextCommentWidget _widget) {
        
        TextComment text_comment = _widget.getAssociatedProxy();
        boolean b_underlined = text_comment.isUnderlined();
        text_comment.setUnderline(!b_underlined);

    }

    /** Changes the color of the ColoredBox */
    public void setHighlightColor (int _color) {

        JGoSelection selection = m_view.getSelection();

        JGoListPosition pos = selection.getFirstObjectPos();
        while (pos != null) {
            JGoObject current_widget = selection.getObjectAtPos(pos);

            if (current_widget instanceof ColoredBoxWidget) {
                setHighlightColor((ColoredBoxWidget) current_widget, _color);
            }
            pos = selection.getNextObjectPos(pos);
        }
    }

    public void setHighlightColor (ColoredBoxWidget _widget, int _color) {

        ColoredBox colored_box = _widget.getAssociatedProxy();
        colored_box.setColor(_color);
    }

    /** Changes the layer of the ColoredBox */
    public void switchHighlightLayer () {
        
        JGoSelection selection = m_view.getSelection();

        JGoListPosition pos = selection.getFirstObjectPos();
        while (pos != null) {
            JGoObject current_widget = selection.getObjectAtPos(pos);

            if (current_widget instanceof ColoredBoxWidget) {
                switchHighlightLayer((ColoredBoxWidget) current_widget);
            }
            pos = selection.getNextObjectPos(pos);
        }
        
    }


    public void switchHighlightLayer (ColoredBoxWidget _widget) {

        ColoredBox colored_box = _widget.getAssociatedProxy();

        if (colored_box.getLayer() == ColoredBox.HILITE_LAYER_FRONT) {
            m_layers[TOP_HIGHLIGHT_LAYER].removeObject(_widget);
            colored_box.setLayer(ColoredBox.HILITE_LAYER_BACK);
            m_layers[BACK_HIGHLIGHT_LAYER].addObjectAtTail(_widget);
        }
        else {
            m_layers[BACK_HIGHLIGHT_LAYER].removeObject(_widget);
            colored_box.setLayer(ColoredBox.HILITE_LAYER_FRONT);
            m_layers[TOP_HIGHLIGHT_LAYER].addObjectAtTail(_widget);
        }
    }

    
    public void sendHighlightToBack() {

        JGoObject widget = m_view.getSelection().getPrimarySelection();

        if (widget instanceof ColoredBoxWidget) {
            sendHighlightToBack(( ColoredBoxWidget ) widget);
        }
    }


    public void sendHighlightToBack (ColoredBoxWidget _cbw) {

        ColoredBox colored_box = _cbw.getAssociatedProxy();

        if (colored_box.getLayer() == ColoredBox.HILITE_LAYER_FRONT) {
            JGoListPosition pos = 
                                  m_layers[TOP_HIGHLIGHT_LAYER].findObject(_cbw);
            m_layers[TOP_HIGHLIGHT_LAYER].removeObjectAtPos(pos);
            m_layers[TOP_HIGHLIGHT_LAYER].addObjectAtHead(_cbw);
        }
        else {
            JGoListPosition pos = 
                                m_layers[BACK_HIGHLIGHT_LAYER].findObject(_cbw);
            m_layers[BACK_HIGHLIGHT_LAYER].removeObjectAtPos(pos);
            m_layers[BACK_HIGHLIGHT_LAYER].addObjectAtHead(_cbw);
        }

        LayeredGroup group = colored_box.group_get();
        if (group !=  null) {
            group.sendToLast(colored_box);
        }
    }


    public void sendHighlightToFront() {

        JGoObject widget = m_view.getSelection().getPrimarySelection();

        if (widget instanceof ColoredBoxWidget) {
            ColoredBoxWidget cbw = ( ColoredBoxWidget ) widget;
        }
    }



    public void sendHighlightToFront (ColoredBoxWidget _cbw) {
        ColoredBox colored_box = _cbw.getAssociatedProxy();

        if (colored_box.getLayer() == ColoredBox.HILITE_LAYER_FRONT) {
            JGoListPosition pos = 
                                  m_layers[TOP_HIGHLIGHT_LAYER].findObject(_cbw);
            m_layers[TOP_HIGHLIGHT_LAYER].removeObjectAtPos(pos);
            m_layers[TOP_HIGHLIGHT_LAYER].addObjectAtTail(_cbw);
        }
        else {
            JGoListPosition pos = 
                                m_layers[BACK_HIGHLIGHT_LAYER].findObject(_cbw);
            m_layers[BACK_HIGHLIGHT_LAYER].removeObjectAtPos(pos);
            m_layers[BACK_HIGHLIGHT_LAYER].addObjectAtTail(_cbw);
        }

        LayeredGroup group = colored_box.group_get();
        if (group !=  null) {
            group.sendToFirst(colored_box);
        }
    }

        /** This method brings an object to the top of its respective layer. */
    public void bringObjectToTopOfItsLayer(JGoObject _object) {
        while (_object.getParent() != null) { _object = _object.getParent(); }

        JGoLayer layer_to_adjust = null;
        if (_object instanceof MemberNodeWidget) {
            m_layers[MEMBER_NODE_LAYER].bringObjectToFront(_object);
        }
        else if (_object instanceof PortWidget) {
            m_layers[EXTERNAL_PORT_LAYER].bringObjectToFront(_object);
        }
        else if (_object instanceof ConnectionWidget) {
            m_layers[CONNECTION_LAYER].bringObjectToFront(_object);
        }
        else if (_object instanceof JunctionWidget) {
            m_layers[JUNCTION_LAYER].bringObjectToFront(_object);
        }
        else if (_object instanceof BundlerWidget) {
            m_layers[BUNDLER_LAYER].bringObjectToFront(_object);
        }
        else if (_object instanceof TextCommentWidget) {
            m_layers[TEXT_LAYER].bringObjectToFront(_object);
        }
/*
        else if (_object instanceof ColoredBoxWidget) {
            ColoredBoxWidget cb_widget = ( ColoredBoxWidget )_object;
            ColoredBox cb = cb_widget.getAssociatedProxy();
            if (cb.getLayer() == ColoredBox.HILITE_LAYER_FRONT) {
                m_layers[TOP_HIGHLIGHT_LAYER].bringObjectToFront(_object);
            }
            else {
                m_layers[BACK_HIGHLIGHT_LAYER].bringObjectToFront(_object);
            }
        }
*/
    }

    public String toString() {
        return "EditorFrame[" + m_node + "]";
    }

        /** This method disables the mouse and shows a wait cursor if _b_wait is
          * true.  If false, cursor and mouse events are set to normal again.
          */
    private void userWait(boolean _b_wait) {
        Cursor cursor = (_b_wait) ?
                           Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) :
                           m_view.getDefaultCursor();

        m_view.setMouseEnabled(! _b_wait);
        m_view.setDragDropEnabled(! _b_wait);
        m_view.setCursorImmediately(cursor);
    }

    public void cut() {
        copy();
        getView().performDelete();
    }

    public void copy() {
        JGoSelection selection = m_view.getSelection();
        ArrayList selection_list = new ArrayList();
        JGoListPosition pos = selection.getFirstObjectPos();
        while (pos != null) {
            selection_list.add(selection.getObjectAtPos(pos));
            pos = selection.getNextObjectPosAtTop(pos);
        }
        ms_clipboard.copy(m_node, selection_list);
    }

    public void duplicate() {
        copy();
        Rectangle clipboard_rect = ms_clipboard.getRectangle();
        Point location = m_view.getDefaultLocation();
        location = GridTool.getNearestGridPoint(location);

        int right = location.x + (int)clipboard_rect.getWidth();  
        int bottom = location.y + (int)clipboard_rect.getHeight(); 

        Dimension working_panel_size = getWorkingPanel().getSize();
    
        if (((int) working_panel_size.getWidth() < right) ||
            ((int) working_panel_size.getHeight() < bottom) ||
             (right < 0) || (bottom < 0)) {
            m_view.setDefaultLocation( 
                new Point(PortWidget.PORT_WIDTH, PortWidget.PORT_WIDTH));
            location = m_view.getDefaultLocation();
            location = GridTool.getNearestGridPoint(location);
        }

        paste(location);
    }

    public void paste(boolean _b_use_default_location) {
        Point location = (_b_use_default_location) ?
                    new Point(50, 50) : m_view.getLastMouseDownLocation_dc();
        location = GridTool.getNearestGridPoint(location);
        paste(location);
    }

    private void paste(Point _location) {
        HashMap include_line_problems_map =
                            ms_clipboard.checkForProblematicIncludeLines(this);
        if (! include_line_problems_map.isEmpty()) {
            if (! doProblematicIncludeLinesWarning(include_line_problems_map)) {
                return;    // user can choose to cancel paste operation
            }
        }
        ms_clipboard.paste(this, _location);
    }

    private boolean doProblematicIncludeLinesWarning(HashMap _problems_map) {
        ALFINode root_container = m_node.isRootNode() ?
                                       m_node : m_node.containerNode_getRoot();
        String warning_message =
            "WARNING: The paste operation you are about to complete will\n" +
            "cause one or more full-path include lines to be written into\n" +
            "the box file " +
                    root_container.getSourceFile().getCanonicalPath() + ".\n" +
            "These include line paths are\n\n";
        for (Iterator i = _problems_map.keySet().iterator(); i.hasNext(); ) { 
            ALFINode node = (ALFINode) i.next();
            String path = (String) _problems_map.get(node);
            warning_message += node.determineLocalName() + ": " + path + "\n";
        }
        warning_message += "\n";

        String warning_title = "Warning: Non-Modular Include Lines";
        String[] option_buttons =
                      { "Continue", "Cancel Paste", "Why is this a problem?" };
        int choice = 2;
        while (choice == 2) {
            choice = JOptionPane.showOptionDialog(this, warning_message,
                                      warning_title, JOptionPane.YES_NO_OPTION,
                                      JOptionPane.WARNING_MESSAGE, null,
                                      option_buttons, option_buttons[0]);
            if (choice == 2) {
                String why_message =
    "When an instance of a box is added to another box, the container must\n" +
    "be able to find the root base box of the added box (which it uses as a\n" +
    "template to build the instance from.)  The container box tracks this\n" +
    "location in the filesystem by creating an include path to this root box\n"+
    "file.  Usually this include path is a relative path, starting at the\n" +
    "directory in which the container's box file is located.  For example if\n"+
    "box A and box B were in the same directory, and box A contained an\n" +
    "instance of box B, then the relative path from box A to find box B\n" +
    "would just be B.box.  If the box B was stored in a subdirectory in A's\n" +
    "directory, then the relative path might be subdir_1/B.box.\n\n" +

    "Another common include line path is one that uses the E2E_PATH setting\n" +
    "to find libraries of common box files.\n\n" +

    "If box A contains include directives with only these two types of\n" +
    "paths, then A can easily be transported from one filesystem to another\n" +
    "by zipping up the contents of the directory containing A.box, as well\n" +
    "as any libraries of boxes in the E2E_PATH.\n\n" +

    "But if A contains include directives which are neither relative nor in\n" +
    "the E2E_PATH, then full paths must be used to track where the member\n" +
    "node's root box is.  And when this is the case, box A will not be\n" +
    "portable in such a manner, because if it is moved away from this\n" +
    "filesystem, then it will not be able to find the included box.  This\n" +
    "should be avoided if at all possible.";

                String why_title = "Full-Path Include Statements Info";

                JOptionPane.showMessageDialog(this, why_message, why_title,
                                              JOptionPane.INFORMATION_MESSAGE);
            }
        }
        return (choice == 0);    // true if user chooses to Continue
    }

    //////////////////////////////////////////////////////////
    ////////// PortOrderChangeEventListener methods //////////

        /** PortOrderChangeEventListener method. */
    public void portOrderChange(PortOrderChangeEvent _event) {
        updatePortPositionsAfterPortOrderChange();
    }

    //////////////////////////////////////////////////////////
    //////////  ProxyChangeEventListener methods /////////////

        /** ProxyChangeEventListener method. */
    public void proxyChangePerformed(ProxyChangeEvent e) {
        // Object proxy = e.getSource();
    }

    //////////////////////////////////////////////////////////
    //////////  NodeToBeDestroyedEventListener methods ///////

        /** NodeToBeDestroyedEventListener method. */
    public void nodeToBeDestroyed(NodeToBeDestroyedEvent _e) {
        close();
    }

    ///////////////////////////////////////////////////////////////////
    ////////// NodeContentChangeEventListener methods /////////////////

        /** NodeContentChangeEventListener method. */
    public void nodeContentChanged(NodeContentChangeEvent e) {
        int action = e.getAction();
        if (action == NodeContentChangeEvent.MEMBER_NODE_DELETE) {
            MemberNodeProxy mnp = (MemberNodeProxy) e.getAssociatedElement();

            MemberNodeWidget[] widgets = getMemberNodeWidgets();
            for (int i = 0; i < widgets.length; i++) {
                if (widgets[i].getAssociatedProxy() == mnp) {
                    mnp.removeProxyChangeListener(widgets[i]);

                    ALFINode the_node = widgets[i].getNode();
                    ALFINode[] base_nodes=the_node.baseNodes_getHierarchyPath();
                    for (int j = 0; j < base_nodes.length; j++) {
                        base_nodes[j].removePortOrderChangeListener(widgets[i]);
                    }

                    ALFINode[] contained_nodes =
                                     the_node.memberNodes_getAllGenerations();
                    for (int j = 0; j < contained_nodes.length; j++) {
                        contained_nodes[j].removeNodeContentChangeListener(
                                                                   widgets[i]);
                    }
                    the_node.removeNodeContentChangeListener(widgets[i]);

                    widgets[i].cleanUp();
                    m_layers[MEMBER_NODE_LAYER].removeObject(widgets[i]);
                    break;
                }
            }
        }
        else if (action == NodeContentChangeEvent.MEMBER_NODE_ADD) {
            MemberNodeProxy mnp = (MemberNodeProxy) e.getAssociatedElement();

            ALFINode associated_node = mnp.getAssociatedMemberNode(m_node);

                // Add the local node to the view
            MemberNodeWidget node_widget =
                            new MemberNodeWidget(this, associated_node, false);

            node_widget.initialize();
            m_layers[MEMBER_NODE_LAYER].addObjectAtTail(node_widget);
        }
        else if (action == NodeContentChangeEvent.CONNECTION_DELETE) {
            ConnectionProxy connection =
                            (ConnectionProxy) e.getAssociatedElement();
            ConnectionWidget cw =
                          (ConnectionWidget) m_connections_map.get(connection);
            m_layers[CONNECTION_LAYER].removeObject(cw);
        }
        else if (action == NodeContentChangeEvent.CONNECTION_ADD) {
            ConnectionProxy connection =
                                (ConnectionProxy) e.getAssociatedElement();

                // it is valid that these ports might have been removed
            GenericPortWidget source_widget = determineSourceWidget(connection);
            GenericPortWidget sink_widget = determineSinkWidget(connection);
            if ((source_widget != null) && (sink_widget != null)) {
                ConnectionWidget widget = new ConnectionWidget(this,connection);
                m_connections_map.put(connection, widget);
                m_layers[CONNECTION_LAYER].addObjectAtTail(widget);
                if (widget.isLocal()) {
                    widget.addConnectionRedrawnEventListener(this);
                }
            }
        }
        else if (action == NodeContentChangeEvent.PORT_DELETE) {
            PortProxy port = (PortProxy) e.getAssociatedElement();

            PortWidget[] widgets = getPortWidgets_onContainer();
            for (int i = 0; i < widgets.length; i++) {
                if (widgets[i].getAssociatedProxy() == port) {
                    port.removeProxyChangeListener(widgets[i]);
                    port.removeProxyChangeListener(this);
                    m_layers[EXTERNAL_PORT_LAYER].removeObject(widgets[i]);
                    break;
                }
            }
            updatePortPositionsAfterPortOrderChange();
        }
        else if (action == NodeContentChangeEvent.PORT_ADD) {
            if (isInternalView()) {
                PortProxy port = (PortProxy) e.getAssociatedElement();

                port.addProxyChangeListener(this);
                PortWidget port_widget =
                                   new PortWidget(this, new Point(0, 0), port);
                port_widget.setVisible(false);
                m_layers[EXTERNAL_PORT_LAYER].addObjectAtTail(port_widget);
                updatePortPositionsAfterPortOrderChange();
                port_widget.setVisible(true);
            }
        }
        else if (action == NodeContentChangeEvent.JUNCTION_DELETE) {
            JunctionProxy junction = (JunctionProxy) e.getAssociatedElement();

            JunctionWidget[] junction_widgets = getJunctionWidgets();
            for (int i = 0; i < junction_widgets.length; i++) {
                if (junction_widgets[i].getAssociatedProxy() == junction) {
                    junction.removeProxyChangeListener(junction_widgets[i]);
                    junction.removeProxyChangeListener(this);
                    m_layers[JUNCTION_LAYER].removeObject(junction_widgets[i]);
                    break;
                }
            }
        }
        else if (action == NodeContentChangeEvent.JUNCTION_ADD) {
            JunctionProxy junction = (JunctionProxy) e.getAssociatedElement();
            JunctionWidget junction_widget = new JunctionWidget(this, junction);
            m_layers[JUNCTION_LAYER].addObjectAtTail(junction_widget);
        }
        else if (action == NodeContentChangeEvent.BUNDLER_DELETE) {
            BundlerProxy bundler = (BundlerProxy) e.getAssociatedElement();

            BundlerWidget[] bundler_widgets = getBundlerWidgets();
            for (int i = 0; i < bundler_widgets.length; i++) {
                if (bundler_widgets[i].getAssociatedProxy() == bundler) {
                    bundler.removeProxyChangeListener(bundler_widgets[i]);
                    bundler.removeProxyChangeListener(this);
                    m_layers[BUNDLER_LAYER].removeObject(bundler_widgets[i]);
                    break;
                }
            }
        }
        else if (action == NodeContentChangeEvent.BUNDLER_ADD) {
            BundlerProxy bundler = (BundlerProxy) e.getAssociatedElement();
            BundlerWidget bundler_widget = new BundlerWidget(this, bundler);
            m_layers[BUNDLER_LAYER].addObjectAtTail(bundler_widget);
        }
        else if (action == NodeContentChangeEvent.TEXT_COMMENT_ADD) {
            TextComment comment = (TextComment) e.getAssociatedElement();
            TextCommentWidget comment_widget = new TextCommentWidget(this, 
                comment);
            m_layers[TEXT_LAYER].addObjectAtTail(comment_widget);
        }
        else if (action == NodeContentChangeEvent.TEXT_COMMENT_DELETE) {
            TextComment comment = (TextComment) e.getAssociatedElement();

            TextCommentWidget[] text_comment_widgets = getTextCommentWidgets();
            for (int i = 0; i < text_comment_widgets.length; i++) {
                if (text_comment_widgets[i].getAssociatedProxy() == comment) {
                    comment.removeProxyChangeListener(text_comment_widgets[i]);
                    comment.removeProxyChangeListener(this);
                    m_layers[TEXT_LAYER].removeObject( text_comment_widgets[i]);
                    break;
                }
            }
        }
        else if (action == NodeContentChangeEvent.COLORED_BOX_ADD) {
            ColoredBox hilite = (ColoredBox) e.getAssociatedElement();
            ColoredBoxWidget cb_widget = new ColoredBoxWidget(this, hilite);
            if (hilite.getLayer() ==  ColoredBox.HILITE_LAYER_FRONT) {
                m_layers[TOP_HIGHLIGHT_LAYER].addObjectAtTail(cb_widget);
            }
            else { 
                m_layers[BACK_HIGHLIGHT_LAYER].addObjectAtTail(cb_widget);
            }
        }
        else if (action == NodeContentChangeEvent.COLORED_BOX_DELETE) {
            ColoredBox hilite = (ColoredBox) e.getAssociatedElement();

            ColoredBoxWidget[] cb_widgets = getColoredBoxWidgets();
            for (int i = 0; i < cb_widgets.length; i++) {
                if (cb_widgets[i].getAssociatedProxy() == hilite) {
                    hilite.removeProxyChangeListener(cb_widgets[i]);
                    hilite.removeProxyChangeListener(this);
                    if (hilite.getLayer() ==  ColoredBox.HILITE_LAYER_FRONT) {
                        m_layers[TOP_HIGHLIGHT_LAYER].removeObject(cb_widgets[i]);
                    }
                    else { 
                        m_layers[BACK_HIGHLIGHT_LAYER].removeObject(cb_widgets[i]);
                    }
                    break;
                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////
    ////////// ComponentListener methods ////////////////////////////

        /** UNUSED ComponentListener method. */
    public void componentHidden(ComponentEvent e) { ; }

        /** Used to keep track of position so resize will know which edge/
          * corner was moved.
          */
    public void componentMoved(ComponentEvent e) {
        if (m_previous_screen_position != null) {
            m_previous_screen_position =
                                     ((Component) e.getSource()).getLocation();
        }
    }

        /**
         * ComponentListener method.  Assumes the only component/container we're
         * listening to is this itself.
         */
    public void componentResized(ComponentEvent e) {
        updateView();
    }

        /** UNUSED ComponentListener method. */
    public void componentShown(ComponentEvent e) { ; }

    /////////////////////////////////////////////
    // FocusListener methods ////////////////////

    public void focusGained(FocusEvent _event) {
        m_view.requestFocusInWindow();
    }

    /////////////////////////////////////////////
    // ConnectionRedrawnEventListener methods ///

        /** ConnectionRedrawnEventListener method. */
    public void connectionRedrawn(ConnectionRedrawnEvent _event) {
        ConnectionWidget cw = (ConnectionWidget) _event.getSource();
        if (((AlfiMainFrame) getParentFrame()).isAutoTrimConnections()) {
            trimConnectionPath(cw);
        }
    }

    /////////////////////////////////////////////
    // GroupElementModEventListener methods /////

        /** GroupElementModEventListener method. */
    public void groupElementModified(GroupElementModEvent _event) {
        
        int action = _event.getAction();
        
        LayeredGroup lg = ( LayeredGroup ) _event.getAssociatedElement();

        if (action == GroupElementModEvent.GROUP_SELECT) {
            group_select(lg, _event.getSource());
        }
    }

    /////////////////////////////////////////////
    // EditorWindowEventSource methods //////////

        /** EditorWindowEventSource method. */
    public void addEditorWindowListener(EditorWindowListener _listener) {
        m_editor_window_listeners_map.put(_listener, null);
    }

        /** EditorWindowEventSource method. */
    public void removeEditorWindowListener(EditorWindowListener _listener) {
        m_editor_window_listeners_map.remove(_listener);
    }

        /** Used in conjunction with EditorWindowEventSource. */
    protected void fireEditorWindowEvent(EditorWindowEvent _e) {
        for (Iterator i = m_editor_window_listeners_map.keySet().iterator();
                                                               i.hasNext(); ) {
            ((EditorWindowListener) i.next()).processEditorWindowEvent(_e);
        }
    }

    //**************************************************************************
    //******* helper classes ***************************************************

    class PortBorder extends JGoRectangle {
        private final int m_edge;

        PortBorder() {
            this(null, new Rectangle(0, 0), PortProxy.EDGE_INVALID);
        }

        public PortBorder(ALFIView _view, Rectangle _edit_rect, int _edge) {
            super();

            m_edge = _edge;

            Point ul = new Point(_edit_rect.getLocation());
            Dimension dim = new Dimension(_edit_rect.getSize());
            int border_width = PortWidget.PORT_WIDTH - 1;

            if (_edge == PortProxy.EDGE_NORTH) {
                dim.height = border_width;
            }
            else if (_edge == PortProxy.EDGE_SOUTH) {
                ul.y += dim.height - border_width;    // - sb_height;
                dim.height = border_width;
            }
            else if (_edge == PortProxy.EDGE_EAST) {
                ul.x += dim.width - border_width;    // - sb_width;
                dim.width = border_width;
            }
            else if (_edge == PortProxy.EDGE_WEST) {
                dim.width = border_width;
            }
            else { }

            setLocation(ul);
            setSize(dim);
            setBrush(JGoBrush.black);
            setSelectable(false);
        }

            /** Left click adds a port. */
        public boolean doMouseClick(int _modifiers, Point _dc, Point _vc,
                                                             JGoView _view) {
            if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
                int position = findPortPosition(_dc);

                ((ALFIView) _view).getOwnerFrame().addPort(m_edge, position);
            }

            return true;    // catch event
        }

        private int findPortPosition(Point _p) {
            int offset_from_center = -1 * (PortWidget.PORT_WIDTH / 2);
            _p.translate(offset_from_center, offset_from_center);

            Point[] port_points = calculateInternalViewPortLocations()[m_edge];

            double shortest_distance = Double.MAX_VALUE;
            int closest_position = PortProxy.EDGE_POSITION_INVALID;
            for (int i = 0; i < port_points.length; i++) {
                double d = port_points[i].distance(_p);
                if (d < shortest_distance) {
                    shortest_distance = d;
                    closest_position = i;
                }
            }

            return closest_position;
        }
    }
}
