package edu.caltech.ligo.alfi.editor;

import java.awt.*;
import java.io.*;
import java.util.*;

import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.widgets.*;
import edu.caltech.ligo.alfi.tools.*;

/**    
  * This class is used for the implementation of Cut/Copy/Paste.  There is only
  * one clipboard object per session, whose contained elements are changed
  * whenever the user copies or cuts elements from any editor window.  The
  * contents of this clipboard can then be pasted into any edit window.
  */
public class AlfiClipboard {

        /** Contains all the elements copied (or placed) into the clipboard. */
    private final ALFINode m_clipboard_node;

        /** The original container of elements being copied into clipboard. */
    private ALFINode m_original_container;

    //**************************************************************************
    //********** Constructors **************************************************

    public AlfiClipboard(ALFINode _clipboard_node) {
        m_clipboard_node = _clipboard_node;

        m_original_container = null;
    }

    //**************************************************************************
    //********** class methods *************************************************


    //**************************************************************************
    //********** instance methods **********************************************

    public ALFINode getClipboardNode() { return m_clipboard_node; }

        /** Sets the contents of the clipboard. */
    void copy(ALFINode _original_container, ArrayList _original_widgets) {
        m_clipboard_node.clear__except_listeners();
        m_original_container = _original_container;

        HashMap external_port_to_copy_map = new HashMap();
        HashMap member_to_copy_map = new HashMap();
        HashMap junction_to_copy_map = new HashMap();
        HashMap bundler_to_copy_map = new HashMap();
        HashMap groups_to_copy_map = new HashMap();
        for (Iterator i = _original_widgets.iterator(); i.hasNext(); ) {
            Object o = i.next();

            if (o instanceof PortWidget) {
                PortProxy port = ((PortWidget) o).getAssociatedProxy();
                PortProxy copy = m_clipboard_node.port_add(port.getName(),
                     port.getIOType(), port.getDataType(), port.getPosition());
                if (port.getDefaultValue() != null) {
                    copy.setDefaultValue(new String(port.getDefaultValue()));
                }
                external_port_to_copy_map.put(port, copy);
            }
            else if (o instanceof MemberNodeWidget) {
                MemberNodeProxy original_mnp =
                                 ((MemberNodeWidget) o).getAssociatedProxy();
                ALFINode original_node =
                    original_mnp.getAssociatedMemberNode(m_original_container);
                MemberNodeProxy copy =
                            original_node.copyToNewContainer(m_clipboard_node);
                member_to_copy_map.put(original_mnp, copy);
                ALFINode new_node =
                        copy.getAssociatedMemberNode(m_clipboard_node);

                if (original_node.group_get() != null) {
                    LayeredGroup group = original_node.group_get();
                    LayeredGroup new_group = null;
                    if (!groups_to_copy_map.containsKey(group)) {
                        new_group = 
                            m_clipboard_node.layeredGroup_add(group.getName());  
                        groups_to_copy_map.put(group, new_group);
                    }
                    else {
                        new_group = 
                                 ( LayeredGroup )groups_to_copy_map.get(group);
                    }
                    if (new_group != null) {
                        new_group.addGroupElement(new_node);
                    }
                }
            }
            else if (o instanceof JunctionWidget) {
                JunctionProxy junction =
                                 ((JunctionWidget) o).getAssociatedProxy();
                JunctionProxy copy = m_clipboard_node.junction_add(
                                   junction.getName(), junction.getLocation());
                junction_to_copy_map.put(junction, copy);

                if (junction.group_get() != null) {
                    LayeredGroup group = junction.group_get();
                    LayeredGroup new_group = null;
                    if (!groups_to_copy_map.containsKey(group)) {
                        new_group = 
                            m_clipboard_node.layeredGroup_add(group.getName());  
                        groups_to_copy_map.put(group, new_group);
                    }
                    else {
                        new_group = 
                                 ( LayeredGroup )groups_to_copy_map.get(group);
                    }
                    if (new_group != null) {
                        new_group.addGroupElement(copy);
                    }
                }
            }
            else if (o instanceof BundlerWidget) {
                BundlerProxy bundler =
                                 ((BundlerWidget) o).getAssociatedProxy();
                BundlerProxy copy = m_clipboard_node.bundler_add(
                                   bundler.getName(), bundler.getLocation());
                bundler_to_copy_map.put(bundler, copy);

                if (bundler.group_get() != null) {
                    LayeredGroup group = bundler.group_get();
                    LayeredGroup new_group = null;
                    if (!groups_to_copy_map.containsKey(group)) {
                        new_group = 
                            m_clipboard_node.layeredGroup_add(group.getName());  
                        groups_to_copy_map.put(group, new_group);
                    }
                    else {
                        new_group = 
                                 ( LayeredGroup )groups_to_copy_map.get(group);
                    }
                    if (new_group != null) {
                        new_group.addGroupElement(copy);
                    }
                }
            }
            else if (o instanceof TextCommentWidget) {
                TextComment comment =
                                 ((TextCommentWidget) o).getAssociatedProxy();
                TextComment copy = m_clipboard_node.textComment_add(
                    comment.getName(), comment.getCommentValue(), 
                    comment.getLocation(), comment.getFontSize(),
                    comment.getColor(), comment.isBold(), comment.isItalic(),
                    comment.isUnderlined() );
                if (comment.group_get() != null) {
                    LayeredGroup group = comment.group_get();
                    LayeredGroup new_group = null;
                    if (!groups_to_copy_map.containsKey(group)) {
                        new_group = 
                            m_clipboard_node.layeredGroup_add(group.getName());  
                        groups_to_copy_map.put(group, new_group);
                    }
                    else {
                        new_group = 
                                 ( LayeredGroup )groups_to_copy_map.get(group);
                    }
                    if (new_group != null) {
                        new_group.addGroupElement(copy);
                    }
                }
            }
            else if (o instanceof ColoredBoxWidget) {
                ColoredBox colored_box =
                                 ((ColoredBoxWidget) o).getAssociatedProxy();
                ColoredBox copy = m_clipboard_node.coloredBox_add(
                    colored_box.getName(), colored_box.getColor(), 
                    colored_box.getLocation(), colored_box.getSize(), 
                    colored_box.getLayer());

                if (colored_box.group_get() != null) {
                    LayeredGroup group = colored_box.group_get();
                    LayeredGroup new_group = null;
                    if (!groups_to_copy_map.containsKey(group)) {
                        new_group = 
                            m_clipboard_node.layeredGroup_add(group.getName());  
                        groups_to_copy_map.put(group, new_group);
                    }
                    else {
                        new_group = 
                                 ( LayeredGroup )groups_to_copy_map.get(group);
                    }
                    if (new_group != null) {
                        new_group.addGroupElement(copy);
                    }
                }
            }

        }
   
        copyEncompassedConnections(external_port_to_copy_map,
                member_to_copy_map, junction_to_copy_map, bundler_to_copy_map);

        
        verifyGroupOrdering(groups_to_copy_map);

        normalizeContentCoordinates();
        EditorFrame node_frame =
                       Alfi.getTheNodeCache().getEditSession(m_clipboard_node);
        if (node_frame != null) { node_frame.resetView(); }
    }


        /**
         * First determines which connections have both source and sink in
         * the keys to the hash maps _external_port_to_copy_map,
         * _member_to_copy_map, _junction_to_copy_map, and/or
         * _bundler_to_copy_map which are the original proxies involved in
         * copying a set of elements into the clipboard.  Next, it creates new
         * connections which link the copies instead of the originals.
         */
    private void copyEncompassedConnections(HashMap _external_port_to_copy_map,
                                            HashMap _member_to_copy_map,
                                            HashMap _junction_to_copy_map,
                                            HashMap _bundler_to_copy_map) {
        ConnectionProxy[] connections = m_original_container.connections_get();
        for (int i = 0; i < connections.length; i++) {
            boolean b_is_bundle = (connections[i] instanceof BundleProxy);
            TerminalBundlersConfiguration terminals_config = connections[i].
                           generateCurrentTerminalBundlersConfigurationState(
                                                         m_original_container);

            GenericPortProxy source = connections[i].getSource();
            MemberNodeProxy source_member = connections[i].getSourceMember();
            GenericPortProxy replacement_source = null;
            MemberNodeProxy replacement_source_member = null;
            if (_junction_to_copy_map.containsKey(source)) {
                replacement_source =
                          (GenericPortProxy) _junction_to_copy_map.get(source);
            }
            else if (_bundler_to_copy_map.containsKey(source)) {
                replacement_source =
                          (GenericPortProxy) _bundler_to_copy_map.get(source);
            }
            else if ((source_member != null) && 
                              _member_to_copy_map.containsKey(source_member)) {
                replacement_source_member =
                      (MemberNodeProxy) _member_to_copy_map.get(source_member);
                replacement_source = findPortOnMemberByName(
                                           replacement_source_member,
                                           m_clipboard_node, source.getName(),
                                           ((PortProxy) source).getIOType());
            }
            else if ((source_member == null) &&
                              _external_port_to_copy_map.containsKey(source)) {
                replacement_source =
                     (GenericPortProxy) _external_port_to_copy_map.get(source);
            }

                // this means the source in original was not part of selection
            if (replacement_source == null) { continue; }

            GenericPortProxy sink = connections[i].getSink();
            MemberNodeProxy sink_member = connections[i].getSinkMember();
            GenericPortProxy replacement_sink = null;
            MemberNodeProxy replacement_sink_member = null;
            if (_junction_to_copy_map.containsKey(sink)) {
                replacement_sink =
                            (GenericPortProxy) _junction_to_copy_map.get(sink);
            }
            else if (_bundler_to_copy_map.containsKey(sink)) {
                replacement_sink =
                            (GenericPortProxy) _bundler_to_copy_map.get(sink);
            }
            else if ((sink_member != null) &&
                                _member_to_copy_map.containsKey(sink_member)) {
                replacement_sink_member = (MemberNodeProxy)
                                          _member_to_copy_map.get(sink_member);
                replacement_sink = findPortOnMemberByName(
                                             replacement_sink_member,
                                             m_clipboard_node, sink.getName(),
                                             ((PortProxy) sink).getIOType());
            }
            else if ((sink_member == null) &&
                                _external_port_to_copy_map.containsKey(sink)) {
                replacement_sink =
                       (GenericPortProxy) _external_port_to_copy_map.get(sink);
            }

                // this means the sink in original was not part of selection
            if (replacement_sink == null) { continue; }

            ConnectionProxy connection = m_clipboard_node.connection_add(
                replacement_source, replacement_source_member, replacement_sink,
                replacement_sink_member, connections[i].getPathDefinition(),
                b_is_bundle, terminals_config);
        }
    }

    /**
      *  Now that the groups are created in the clipboard node, 
      *  make sure that the ordering is intact.
      */
    private void verifyGroupOrdering (HashMap _group_to_copy_map){
    
        Iterator it = _group_to_copy_map.keySet().iterator();

        while (it.hasNext()) {
            LayeredGroup source_group = ( LayeredGroup )it.next();
            LayeredGroup cb_group = ( LayeredGroup )
                                           _group_to_copy_map.get(source_group);     

            if (cb_group != null) {
                cb_group.copyGroupOrder(source_group);
            }
        }
    }

    public void paste(EditorFrame _target_editor, Point _location) {
        ALFINode target_node = _target_editor.getNodeInEdit();

            // document names to change elements back to once the past is done
        HashMap old_name_map = new HashMap();
        MemberNodeProxy[] members = m_clipboard_node.memberNodeProxies_get();
        for (int i = 0; i < members.length; i++) {
            old_name_map.put(members[i], members[i].getLocalName());
        }
        JunctionProxy[] junctions = m_clipboard_node.junctions_get();
        for (int i = 0; i < junctions.length; i++) {
            old_name_map.put(junctions[i], junctions[i].getName());
        }
        BundlerProxy[] bundlers = m_clipboard_node.bundlers_get();
        for (int i = 0; i < bundlers.length; i++) {
            old_name_map.put(bundlers[i], bundlers[i].getName());
        }
        PortProxy[] ports = m_clipboard_node.ports_get();
        for (int i = 0; i < ports.length; i++) {
            old_name_map.put(ports[i], ports[i].getName());
        }
        TextComment[] text_comments = m_clipboard_node.textComments_get();
        for (int i = 0; i < text_comments.length; i++) {
            old_name_map.put(text_comments[i], text_comments[i].getName());
        }
        ColoredBox[] colored_box = m_clipboard_node.coloredBoxes_get();
        for (int i = 0; i < colored_box.length; i++) {
            old_name_map.put(colored_box[i], colored_box[i].getName());
        }
        LayeredGroup[] layered_group = m_clipboard_node.layeredGroups_get();
        for (int i = 0; i < layered_group.length; i++) {
            old_name_map.put(layered_group[i], layered_group[i].getName());
        }

            // change any names which will conflict with elements in target_node
        changeIdenticalNames(target_node.memberNodeProxies_get());
        changeIdenticalNames(target_node.junctions_get());
        changeIdenticalNames(target_node.bundlers_get());
        changeIdenticalNames(target_node.ports_get());
        changeIdenticalNames(target_node.textComments_get());
        changeIdenticalNames(target_node.coloredBoxes_get());
        changeIdenticalNames(target_node.layeredGroups_get());

            // translate all elements in clipboard to new positions
        translateAll(_location.x, _location.y);

            // paste the clipboard's elements into target_node
        HashMap terminal_map = new HashMap();
        pasteMembers(target_node, terminal_map);
        pastePorts(target_node, terminal_map);
        pasteJunctions(target_node, terminal_map);
        pasteBundlers(target_node, terminal_map);
        pasteTextComments(target_node, terminal_map);
        pasteColoredBoxes(target_node, terminal_map);
        pasteLayeredGroups(target_node, terminal_map, _target_editor);

        pasteConnections(target_node, terminal_map);

            // normalize clipboard positions again
        normalizeContentCoordinates();

            // change clipboard elements' names back to their original names
        for (Iterator i = old_name_map.keySet().iterator(); i.hasNext();) {
            Object proxy = i.next();
            String old_name = (String) old_name_map.get(proxy);
            if (proxy instanceof MemberNodeProxy) {
                ((MemberNodeProxy) proxy).setLocalName(old_name);
            }
            else if (proxy instanceof GenericPortProxy) {
                ((GenericPortProxy) proxy).setName(old_name);
            }
        }

            // change selection to be the newly pasted elements
        JGoSelection selection = _target_editor.getView().getSelection();
        selection.clearSelection();
        for (Iterator i = terminal_map.values().iterator(); i.hasNext(); ) {
            Object element = i.next();
            JGoObject selectable_widget = null;
            if (element instanceof MemberNodeProxy) {
                selectable_widget = _target_editor.determineMemberWidget(
                                                    (MemberNodeProxy) element);
            }
            else if (element instanceof JunctionProxy) {
                selectable_widget = _target_editor.determineJunctionWidget(
                                                      (JunctionProxy) element);
            }
            else if (element instanceof BundlerProxy) {
                selectable_widget = _target_editor.determineBundlerWidget(
                                                       (BundlerProxy) element);
            }
            else if (element instanceof TextComment) {
                selectable_widget = _target_editor.determineTextCommentWidget(
                                                      (TextComment) element);
            }
            else if (element instanceof ColoredBox) {
                selectable_widget = _target_editor.determineColoredBoxWidget(
                                                   (ColoredBox) element);
            }

            if (selectable_widget != null) {
                selection.extendSelection(selectable_widget);
            }
        }
    }

    private void pasteConnections(ALFINode _new_container,
                                  HashMap _terminal_map) {
        ConnectionProxy[] connections = m_clipboard_node.connections_get();
        for (int i = 0; i < connections.length; i++) {
            boolean b_is_bundle = (connections[i] instanceof BundleProxy);
            TerminalBundlersConfiguration terminals_config = connections[i].
                           generateCurrentTerminalBundlersConfigurationState(
                                                             m_clipboard_node);

            GenericPortProxy new_source = (GenericPortProxy)
                                 _terminal_map.get(connections[i].getSource());
            MemberNodeProxy new_source_member = (MemberNodeProxy)
                           _terminal_map.get(connections[i].getSourceMember());
            GenericPortProxy new_sink = (GenericPortProxy)
                                   _terminal_map.get(connections[i].getSink());
            MemberNodeProxy new_sink_member = (MemberNodeProxy)
                           _terminal_map.get(connections[i].getSinkMember());
            Integer[] new_path_definition = connections[i].getPathDefinition();

            ConnectionProxy connection = _new_container.connection_add(
                new_source, new_source_member, new_sink, new_sink_member,
                new_path_definition, b_is_bundle, terminals_config);
        }
    }

    private void pasteJunctions(ALFINode _new_container, HashMap _terminal_map){
        JunctionProxy[] junctions = m_clipboard_node.junctions_get();
        for (int i = 0; i < junctions.length; i++) {
            JunctionProxy new_junction = _new_container.junction_add(
                           junctions[i].getName(), junctions[i].getLocation());
            _terminal_map.put(junctions[i], new_junction);
        }
    }

    private void pasteBundlers(ALFINode _new_container, HashMap _terminal_map) {
        BundlerProxy[] bundlers = m_clipboard_node.bundlers_get();
        for (int i = 0; i < bundlers.length; i++) {
            BundlerProxy new_bundler = _new_container.bundler_add(
                             bundlers[i].getName(), bundlers[i].getLocation());
            _terminal_map.put(bundlers[i], new_bundler);
        }
    }

    private void pastePorts(ALFINode _new_container, HashMap _terminal_map) {
        PortProxy[] ports = m_clipboard_node.ports_get();
        for (int i = 0; i < ports.length; i++) {
            PortProxy new_port = _new_container.port_add(
                            ports[i].getName(), ports[i].getIOType(),
                            ports[i].getDataType(), ports[i].getPosition());
            _terminal_map.put(ports[i], new_port);
        }
    }

    private void pasteTextComments(ALFINode _new_container, 
                                                        HashMap _terminal_map) {
        TextComment[] comments = m_clipboard_node.textComments_get();
        for (int i = 0; i < comments.length; i++) {
            TextComment new_comment = _new_container.textComment_add(
                    comments[i].getName(), comments[i].getCommentValue(),
                    comments[i].getLocation(), comments[i].getFontSize(),
                    comments[i].getColor(), comments[i].isBold(), 
                    comments[i].isItalic(), comments[i].isUnderlined() );
            _terminal_map.put(comments[i], new_comment);
        }
    }

    private void pasteColoredBoxes(ALFINode _new_container, 
                                                        HashMap _terminal_map) {
        ColoredBox[] boxes = m_clipboard_node.coloredBoxes_get();
        for (int i = 0; i < boxes.length; i++) {
            ColoredBox new_box = _new_container.coloredBox_add(
                boxes[i].getName(), boxes[i].getColor(), boxes[i].getLocation(),
                boxes[i].getSize(), boxes[i].getLayer());
            _terminal_map.put(boxes[i], new_box);
        }
    }

    private void pasteLayeredGroups(ALFINode _new_container, 
                            HashMap _terminal_map, EditorFrame _target_editor) {
        LayeredGroup[] groups = m_clipboard_node.layeredGroups_get();
        for (int i = 0; i < groups.length; i++) {
            LayeredGroup new_group = _new_container.layeredGroup_add(null);
            
            for (int j = 0; j < groups[i].numElements(); j++) {
                GroupableIF element = groups[i].getElement(j);
                if (element instanceof TextComment) {
                    TextComment new_comment = 
                        _new_container.textComment_findLocal( 
                                                      element.getElementName());
                    new_group.addGroupElement(new_comment);
                    new_comment.group_registerListener(_target_editor);
                }
                else if (element instanceof ColoredBox) {
                    ColoredBox new_colored_box = 
                        _new_container.coloredBox_findLocal(
                                                      element.getElementName());
                    new_group.addGroupElement(new_colored_box);
                    new_colored_box.group_registerListener(_target_editor);
                }
                else if (element instanceof JunctionProxy) {
                    JunctionProxy new_junction = 
                        _new_container.junction_findLocal(
                                                      element.getElementName());
                    new_group.addGroupElement(new_junction);
                    new_junction.group_registerListener(_target_editor);
                }
                else if (element instanceof BundlerProxy) {
                    BundlerProxy new_bundler = 
                        _new_container.bundler_findLocal(
                                                      element.getElementName());
                    new_group.addGroupElement(new_bundler);
                    new_bundler.group_registerListener(_target_editor);
                }
                else if (element instanceof ALFINode) {
                    ALFINode new_node = 
                        _new_container.memberNodeProxy_findLocal(
                                                      element.getElementName());
                    new_group.addGroupElement(new_node);
                    new_node.group_registerListener(_target_editor);
                }
            }
            _terminal_map.put(groups[i], new_group);
        }
    }

        /** Checks for any include line paths which are full unix paths (i.e.,
          * paths which start with "/".)  Such paths in include files will break
          * the modularity of a set of contained boxes.  In general, include
          * line paths should either be relative paths or "*" paths which are
          * paths which can be found in the user's E2E_PATH.   If the returned
          * HashMap is not empty, then the ensuing copy would place such full
          * paths in the container's box file.  The user should be warned about
          * what is about to occur in the copy process.  The key/value pairs are
          * { ALFINode, String } where the node is the member node about to be
          * copied, and the String is the full path to that node's root base
          * nodes's box file.
          */
    HashMap checkForProblematicIncludeLines(EditorFrame _target_editor) {
        ALFINode target_container = _target_editor.getNodeInEdit();
        HashMap include_line_problems_map = new HashMap();
        MemberNodeProxy[] members = m_clipboard_node.memberNodeProxies_get();
        for (int i = 0; i < members.length; i++) {
            ALFINode node= members[i].getAssociatedMemberNode(m_clipboard_node);
            if (node.isBox()) {
                String path = node.copy__modifyIncludeLinePathForNewContainer(
                                                             target_container);
                if (path.startsWith(File.separator)) {
                    include_line_problems_map.put(node, path);
                }
            }
        }

        return include_line_problems_map;
    }

    private void pasteMembers(ALFINode _new_container, HashMap _terminal_map) {
        MemberNodeProxy[] members = m_clipboard_node.memberNodeProxies_get();
        for (int i = 0; i < members.length; i++) {
            ALFINode node= members[i].getAssociatedMemberNode(m_clipboard_node);
            MemberNodeProxy new_mnp = node.copyToNewContainer(_new_container);
            _terminal_map.put(members[i], new_mnp);

            PortProxy[] old_ports = node.ports_get();
            PortProxy[] new_ports =
                   new_mnp.getAssociatedMemberNode(_new_container).ports_get();
            for (int j = 0; j < new_ports.length; j++) {
                for (int k = 0; k < old_ports.length; k++) {
                    if (new_ports[j].getName().equals(old_ports[k].getName()) &&
                        new_ports[j].getIOType() == old_ports[k].getIOType()) {
                        _terminal_map.put(old_ports[k], new_ports[j]);
                        break;
                    }
                }
            }
        }
    }

    private void changeIdenticalNames(MemberNodeProxy[] _existing_members) {
        if (_existing_members.length == 0) { return; }

        String[] existing_names = new String[_existing_members.length];
        for (int i = 0; i < _existing_members.length; i++) {
            existing_names[i] = _existing_members[i].getLocalName();
        }

        MemberNodeProxy[] cb_members = m_clipboard_node.memberNodeProxies_get();
        for (int i = 0; i < cb_members.length; i++) {
            while (nameIsTaken(cb_members, i, existing_names)) {
                cb_members[i].setLocalName(
                                 iterateName(cb_members[i].getLocalName()));
            }
        }
    }

    private void changeIdenticalNames(JunctionProxy[] _existing_junctions) {
        if (_existing_junctions.length == 0) { return; }

        String[] existing_names = new String[_existing_junctions.length];
        for (int i = 0; i < _existing_junctions.length; i++) {
            existing_names[i] = _existing_junctions[i].getName();
        }

        JunctionProxy[] cb_junctions = m_clipboard_node.junctions_get();
        for (int i = 0; i < cb_junctions.length; i++) {
            while (nameIsTaken(cb_junctions, i, existing_names)) {
                cb_junctions[i].setName(iterateName(cb_junctions[i].getName()));
            }
        }
    }

    private void changeIdenticalNames(BundlerProxy[] _existing_bundlers) {
        if (_existing_bundlers.length == 0) { return; }

        String[] existing_names = new String[_existing_bundlers.length];
        for (int i = 0; i < _existing_bundlers.length; i++) {
            existing_names[i] = _existing_bundlers[i].getName();
        }

        BundlerProxy[] cb_bundlers = m_clipboard_node.bundlers_get();
        for (int i = 0; i < cb_bundlers.length; i++) {
            while (nameIsTaken(cb_bundlers, i, existing_names)) {
                cb_bundlers[i].setName(iterateName(cb_bundlers[i].getName()));
            }
        }
    }

    private void changeIdenticalNames(PortProxy[] _existing_ports) {
        if (_existing_ports.length == 0) { return; }

        String[] existing_names = new String[_existing_ports.length];
        for (int i = 0; i < _existing_ports.length; i++) {
            existing_names[i] = _existing_ports[i].getName();
        }

        PortProxy[] cb_ports = m_clipboard_node.ports_get();
        for (int i = 0; i < cb_ports.length; i++) {
            while (nameIsTaken(cb_ports, i, existing_names)) {
                cb_ports[i].setName(iterateName(cb_ports[i].getName()));
            }
        }
    }

    private void changeIdenticalNames(TextComment[] _text_comments) {
        if (_text_comments.length == 0) { return; }

        String[] existing_names = new String[_text_comments.length];
        for (int i = 0; i < _text_comments.length; i++) {
            existing_names[i] = _text_comments[i].getName();
        }

        TextComment[] cb_comments = m_clipboard_node.textComments_get();
        for (int i = 0; i < cb_comments.length; i++) {
            while (nameIsTaken(cb_comments, i, existing_names)) {
                cb_comments[i].setName(iterateName(cb_comments[i].getName()));
            }
        }
    }

    private void changeIdenticalNames(ColoredBox[] _colored_boxes) {
        if (_colored_boxes.length == 0) { return; }

        String[] existing_names = new String[_colored_boxes.length];
        for (int i = 0; i < _colored_boxes.length; i++) {
            existing_names[i] = _colored_boxes[i].getName();
        }

        ColoredBox[] cb_colored_boxes = m_clipboard_node.coloredBoxes_get();
        for (int i = 0; i < cb_colored_boxes.length; i++) {
            while (nameIsTaken(cb_colored_boxes, i, existing_names)) {
                cb_colored_boxes[i].setName(
                                   iterateName(cb_colored_boxes[i].getName()));
            }
        }
    }

    private void changeIdenticalNames(LayeredGroup[] _layered_groups) {
        if (_layered_groups.length == 0) { return; }

        String[] existing_names = new String[_layered_groups.length];
        for (int i = 0; i < _layered_groups.length; i++) {
            existing_names[i] = _layered_groups[i].getName();
        }

        LayeredGroup[] cb_layered_groups = m_clipboard_node.layeredGroups_get();
        for (int i = 0; i < cb_layered_groups.length; i++) {
            while (nameIsTaken(cb_layered_groups, i, existing_names)) {
                cb_layered_groups[i].setName(
                                   iterateName(cb_layered_groups[i].getName()));
            }
        }
    }

        /**
         * Checks if MemberNodeProxy _members[_index_of_testee] has an identical
         * name to any name of any other members or to any name in
         * _other_disallowed_names.
         */
    private boolean nameIsTaken(MemberNodeProxy[] _members,
                      int _index_of_testee, String[] _other_disallowed_names) {
        String name_to_test = _members[_index_of_testee].getLocalName();
        for (int i = 0; i < _members.length; i++) {
            if (i == _index_of_testee) { continue; }
            if (_members[i].getLocalName().equals(name_to_test)) {
                return true;
            }
        }

        for (int i = 0; i < _other_disallowed_names.length; i++) {
            if (_other_disallowed_names[i].equals(name_to_test)) {
                return true;
            }
        }

        return false;
    }

        /**
         * Checks if PortProxy _ports[_index_of_testee] has an identical
         * name to any name of any other ports or to any name in
         * _other_disallowed_names.
         */
    private boolean nameIsTaken(PortProxy[] _ports,
                      int _index_of_testee, String[] _other_disallowed_names) {
        String name_to_test = _ports[_index_of_testee].getName();
        for (int i = 0; i < _ports.length; i++) {
            if (i == _index_of_testee) { continue; }
            if (_ports[i].getName().equals(name_to_test)) {
                return true;
            }
        }

        for (int i = 0; i < _other_disallowed_names.length; i++) {
            if (_other_disallowed_names[i].equals(name_to_test)) {
                return true;
            }
        }

        return false;
    }

        /**
         * Checks if JunctionProxy _junctions[_index_of_testee] has an identical
         * name to any name of any other junctions or to any name in
         * _other_disallowed_names.
         */
    private boolean nameIsTaken(JunctionProxy[] _junctions,
                      int _index_of_testee, String[] _other_disallowed_names) {
        String name_to_test = _junctions[_index_of_testee].getName();
        for (int i = 0; i < _junctions.length; i++) {
            if (i == _index_of_testee) { continue; }
            if (_junctions[i].getName().equals(name_to_test)) {
                return true;
            }
        }

        for (int i = 0; i < _other_disallowed_names.length; i++) {
            if (_other_disallowed_names[i].equals(name_to_test)) {
                return true;
            }
        }

        return false;
    }

        /**
         * Checks if BundlerProxy _bundlers[_index_of_testee] has an identical
         * name to any name of any other bundler or to any name in
         * _other_disallowed_names.
         */
    private boolean nameIsTaken(BundlerProxy[] _bundlers,
                      int _index_of_testee, String[] _other_disallowed_names) {
        String name_to_test = _bundlers[_index_of_testee].getName();
        for (int i = 0; i < _bundlers.length; i++) {
            if (i == _index_of_testee) { continue; }
            if (_bundlers[i].getName().equals(name_to_test)) {
                return true;
            }
        }

        for (int i = 0; i < _other_disallowed_names.length; i++) {
            if (_other_disallowed_names[i].equals(name_to_test)) {
                return true;
            }
        }

        return false;
    }

        /**
         * Checks if TextComment _comments[_index_of_testee] has an identical
         * name to any name of any other text comments or to any name in
         * _other_disallowed_names.
         */
    private boolean nameIsTaken(TextComment[] _comments,
                      int _index_of_testee, String[] _other_disallowed_names) {
        String name_to_test = _comments[_index_of_testee].getName();
        for (int i = 0; i < _comments.length; i++) {
            if (i == _index_of_testee) { continue; }
            if (_comments[i].getName().equals(name_to_test)) {
                return true;
            }
        }

        for (int i = 0; i < _other_disallowed_names.length; i++) {
            if (_other_disallowed_names[i].equals(name_to_test)) {
                return true;
            }
        }

        return false;
    }

        /**
         * Checks if ColoredBox _boxes[_index_of_testee] has an identical
         * name to any name of any other colored boxes or to any name in
         * _other_disallowed_names.
         */
    private boolean nameIsTaken(ColoredBox[] _boxes,
                      int _index_of_testee, String[] _other_disallowed_names) {
        String name_to_test = _boxes[_index_of_testee].getName();
        for (int i = 0; i < _boxes.length; i++) {
            if (i == _index_of_testee) { continue; }
            if (_boxes[i].getName().equals(name_to_test)) {
                return true;
            }
        }

        for (int i = 0; i < _other_disallowed_names.length; i++) {
            if (_other_disallowed_names[i].equals(name_to_test)) {
                return true;
            }
        }

        return false;
    }

        /**
         * Checks if LayeredGroup _groups[_index_of_testee] has an identical
         * name to any name of any other layered groups or to any name in
         * _other_disallowed_names.
         */
    private boolean nameIsTaken(LayeredGroup[] _groups,
                      int _index_of_testee, String[] _other_disallowed_names) {
        String name_to_test = _groups[_index_of_testee].getName();
        for (int i = 0; i < _groups.length; i++) {
            if (i == _index_of_testee) { continue; }
            if (_groups[i].getName().equals(name_to_test)) {
                return true;
            }
        }

        for (int i = 0; i < _other_disallowed_names.length; i++) {
            if (_other_disallowed_names[i].equals(name_to_test)) {
                return true;
            }
        }

        return false;
    }

        /**
         * Advances the integer on the end of the name by 1, or adds "_1" if
         * there is no such construct yet.  E.g., ABMatrix_2 -> ABMatrix_3, and
         * ABMatrix -> ABMatrix_1
         */
    private String iterateName(String _name) {
        String iterated_name = null;
        if (_name.matches("^.*_\\d+$")) {
            try {
                String[] parts = _name.split("_");
                int counter = Integer.parseInt(parts[parts.length - 1]);
                counter++;
                parts[parts.length - 1] = Integer.toString(counter);
                iterated_name = new String(parts[0]);
                for (int i = 1; i < parts.length; i++) {
                    iterated_name += "_" + parts[i];
                }
            }
            catch(NumberFormatException e) { ; }
        }

        if (iterated_name == null) { iterated_name = _name + "_1"; }

        return iterated_name;
    }

    private PortProxy findPortOnMemberByName(MemberNodeProxy _member,
                                      ALFINode _container, String _port_name,
                                      int _io_type) {
        ALFINode member_node = _member.getAssociatedMemberNode(_container);
        PortProxy[] ports = member_node.ports_get();
        for (int i = 0; i < ports.length; i++) {
            if (ports[i].getName().equals(_port_name) &&
                ports[i].getIOType() == _io_type) { 
                return ports[i]; 
            }
        }
        return null;
    }

        /**
         * This method looks at the locations of all elements contained in the
         * clipboard and translates their positions such that the element
         * closest to the left edge is pushed to the left edge, and the
         * element closest to the top edge is pushed to the top edge.  All other
         * elements are translated in the same manner so they keep their
         * positions with respect to each-other.
         */
    private void normalizeContentCoordinates() {
        Rectangle original_container_rect =
            ALFIView.determineMinimumRect(m_clipboard_node, false, true, false);
        int dx = - original_container_rect.x;
        int dy = - original_container_rect.y;
        translateAll(dx, dy);
    }

    public Rectangle getRectangle() {
        return ALFIView.determineMinimumRect(m_clipboard_node, false, true, 
                                                                        false);
    }
        /**
         * This method moves all moveable elements in m_clipboard_node by _dx
         * and _dy.
         */
    private void translateAll(int _dx, int _dy) {
            // translation needs to be in grid spacing increments
        Point dp = GridTool.getNearestGridPoint(new Point(_dx, _dy));
        int dx = dp.x;
        int dy = dp.y;

        MemberNodeProxy[] members = m_clipboard_node.memberNodeProxies_get();
        for (int i = 0; i < members.length; i++) {
            Point original_location = members[i].getLocation();
            Point location = new Point(original_location);
            location.translate(dx, dy);
            members[i].setLocation(location);
        }

        JunctionProxy[] junctions = m_clipboard_node.junctions_get();
        for (int i = 0; i < junctions.length; i++) {
            Point original_location = junctions[i].getLocation();
            Point location = new Point(original_location);
            location.translate(dx, dy);
            junctions[i].setLocation(location);
        }

        BundlerProxy[] bundlers = m_clipboard_node.bundlers_get();
        for (int i = 0; i < bundlers.length; i++) {
            Point original_location = bundlers[i].getLocation();
            Point location = new Point(original_location);
            location.translate(dx, dy);
            bundlers[i].setLocation(location);
        }

        ConnectionProxy[] connections = m_clipboard_node.connections_get();
        for (int i = 0; i < connections.length; i++) {
            connections[i].translate(dx, dy);
        }

        TextComment[] comments = m_clipboard_node.textComments_get();
        for (int i = 0; i < comments.length; i++) {
            Point original_location = comments[i].getLocation();
            Point location = new Point(original_location);
            location.translate(dx, dy);
            comments[i].setLocation(location);
        }

        ColoredBox[] boxes = m_clipboard_node.coloredBoxes_get();
        for (int i = 0; i < boxes.length; i++) {
            Point original_location = boxes[i].getLocation();
            Point location = new Point(original_location);
            location.translate(dx, dy);
            boxes[i].setLocation(location);
        }
    }
}
