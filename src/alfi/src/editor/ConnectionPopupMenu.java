package edu.caltech.ligo.alfi.editor;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.Box;

import edu.caltech.ligo.alfi.common.BasicPopupMenu;
import edu.caltech.ligo.alfi.common.Actions;
import edu.caltech.ligo.alfi.common.UIResourceBundle;

/**    
  * <pre>
  * The command popup menu for nodes
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see BasicPopupMenu
  */
public class ConnectionPopupMenu extends BasicPopupMenu { 
    public ConnectionPopupMenu (Actions _actions) {
        super(_actions);

        createMenu(EditorActionsIF.CONN_CREATE_BUNDLER);
        createMenu(EditorActionsIF.CONN_CREATE_JUNCTION);
        createMenu(EditorActionsIF.CONN_TRIM);
        createMenu(EditorActionsIF.CONN_SMOOTH);
        createMenu(EditorActionsIF.CONN_DELETE);
        createMenu(EditorActionsIF.SHOW_BUNDLE_CONTENT);
    }
}

