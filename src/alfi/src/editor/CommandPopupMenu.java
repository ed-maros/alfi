package edu.caltech.ligo.alfi.editor;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.Box;

import edu.caltech.ligo.alfi.common.BasicPopupMenu;
import edu.caltech.ligo.alfi.common.Actions;

/**    
  * <pre>
  * The menu bar for the main frame 
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see BasicPopupMenu
  */
public class CommandPopupMenu extends BasicPopupMenu { 

    public CommandPopupMenu (Actions _actions) {
        super(_actions);

        JMenu edit = createCascadeMenu(EditorActionsIF.EDIT);        
        createMenuItem(edit, EditorActionsIF.CUT);
        createMenuItem(edit, EditorActionsIF.COPY);
        createMenuItem(edit, EditorActionsIF.DUPLICATE);
        createMenuItem(edit, EditorActionsIF.PASTE);
        edit.addSeparator();
        createMenuItem(edit, EditorActionsIF.DELETE);
        edit.addSeparator();
        createMenuItem(edit, EditorActionsIF.SELECT_ALL);
        edit.addSeparator();
        createMenuItem(edit, EditorActionsIF.GROUP);
        createMenuItem(edit, EditorActionsIF.UNGROUP);

        addSeparator();

        JMenu align = createCascadeMenu(EditorActionsIF.ALIGN);        
        createMenuItem(align, EditorActionsIF.ALIGN_LEFT);
        createMenuItem(align, EditorActionsIF.ALIGN_RIGHT);
        createMenuItem(align, EditorActionsIF.ALIGN_TOP);
        createMenuItem(align, EditorActionsIF.ALIGN_BOTTOM);
        createMenuItem(align, EditorActionsIF.ALIGN_VERTICAL_CENTER);
        createMenuItem(align, EditorActionsIF.ALIGN_HORIZONTAL_CENTER);

        addSeparator();

        createMenu(EditorActionsIF.NEW_BUNDLER);
        createMenu(EditorActionsIF.NEW_TEXT_COMMENT);
        createMenu(EditorActionsIF.NEW_COLORED_BOX);
        createMenu(EditorActionsIF.EDIT_COMMENT);
        createMenu(EditorActionsIF.EDIT_MACROS);
        createMenu(EditorActionsIF.BOX_SETTINGS);
        createMenu(EditorActionsIF.VIEW_EXTERNAL);
        createMenu(EditorActionsIF.VIEW_INTERNAL);
        createMenu(EditorActionsIF.RESET_VIEW);
        createMenu(EditorActionsIF.VALIDATE_BUNDLES);
        createMenu(EditorActionsIF.SMOOTH_CONNS);
        createMenu(EditorActionsIF.TRIM_CONNS);

        addSeparator();

        createMenu(EditorActionsIF.VIEW_BASE_BOX);
        createMenu(EditorActionsIF.SHOW_MAIN);
        JMenu debug = createCascadeMenu(EditorActionsIF.DEBUG);
        createMenuItem(debug, EditorActionsIF.LOCATE_FILE);
        createMenuItem(debug, EditorActionsIF.NODE_INFO);

        addSeparator();

        createMenu(EditorActionsIF.EXPORT_MODELER_FILE);

        addSeparator();

        createMenu(EditorActionsIF.SAVE);
        createMenu(EditorActionsIF.SAVE_COPY_AS);

        addSeparator();

        createMenu(EditorActionsIF.PRINT_TO_SCALE);
        createMenu(EditorActionsIF.PRINT_FIT_PAGE);

        addSeparator();

        createMenu(EditorActionsIF.RELOAD);
        createMenu(EditorActionsIF.CLOSE);
    }

}

