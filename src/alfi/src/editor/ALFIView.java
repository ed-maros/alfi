package edu.caltech.ligo.alfi.editor;

import java.util.*;
import java.awt.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.awt.print.*;
import java.awt.dnd.DragGestureEvent;
import javax.swing.*;
import com.nwoods.jgo.*;

import edu.caltech.ligo.alfi.Alfi;

import edu.caltech.ligo.alfi.common.*;

import edu.caltech.ligo.alfi.summary.AlfiMainFrame;
import edu.caltech.ligo.alfi.resource.EditorResources;

import edu.caltech.ligo.alfi.widgets.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.tools.*;

/**    
  * <pre>
  * The ALFIView provides the view for the ALFIDocument.
  * 
  * Important notes on the event processing:
  *
  * Because the JGo default event processing often does things we don't want,
  * we have mostly suppressed it in all of the doMouseXxx methods.  First
  * these methods should see if there is an active alfi widget (i.e. something
  * that implements the AlfiWidgetIF interface).  If so, the event should be
  * first handled by this active object.  If the active object returns false,
  * then the event is passed to JGoView for processing.
  *
  * Next, if the mouse is over an alfi widget (or an object whose parent is an
  * alfi widget), then this object gets the next chance at handling the event,
  * Again, if this objects method of the same name returns a false value, then
  * JGoView's event processing is done.
  *
  * Only after neither of the above cases are in effect, is the default JGo
  * event processing called upon.  One of these default actions is to actually
  * call on the objects as in the second case above, but if any processing was
  * to take place in that object, then said object's event processing method
  * should have returned a true value, and thusly this default event processing
  * would not have occurred.  
  *
  * Non-mouse event processing may need to be dealt with in a similar manner
  * if the need arises.
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public class ALFIView extends JGoView
                      implements JGoViewListener, MouseListener {
    // attributes
    protected Point m_default_location = new Point(10, 10);
    protected final EditorFrame m_frame;
    protected static final int GRID_SIZE = 6;
    private static final int PRINT_TO_SCALE = 0;
    private static final int PRINT_FIT_PAGE = 1;

        /** The latest mouse down sets this (document coordinates.) */
    private Point m_mouse_location_dc;

    /** If this is not null, then events on the view are re this object. */
    private AlfiWidgetIF m_active_object;

    /** The object that was clicked on (selected) */
    private AlfiWidgetIF m_selected_widget = null;

    private int m_print_type = PRINT_TO_SCALE;

    private static final int ALIGN_LEFT = 0;
    private static final int ALIGN_RIGHT = 1;
    private static final int ALIGN_TOP = 2;
    private static final int ALIGN_BOTTOM = 3;
    private static final int ALIGN_VERT_CENTER = 4;
    private static final int ALIGN_HORIZ_CENTER = 5;

    /**
      * This is set whenever the mouse passes over any JGoObject in the view.
      * This is only set by doUncapturedMouseMove currently!
      */
    private JGoObject m_object_mouse_is_over;

    private boolean m_multiple_select;

        /**
         * The two booleans mb_mouse_drag_is_armed and mb_mouse_drag_has_occured
         * are used to create a workaround on a "feature" in JGo which sometimes
         * places the Shift and/or Ctrl modifiers on a mouse event even if they
         * weren't actually used.  We assume this is used internally in JGo
         * event processing and do not want to try to fix it in case we break
         * other things.  Basically, we just want to know when a mouse drag has
         * occured.  These two booleans are set and unset in doMouseUp,
         * doMouseDown, and doMouseMove.  They should not be read or written
         * anywhere else.
         */
    private boolean mb_mouse_drag_is_armed;

        /** See notes on mb_mouse_drag_is_armed. */
    private boolean mb_mouse_drag_has_occured;

        /** Used in editor associated with UDP dialog to suppress unwanted
          * behavior for that case specifically.
          */
    private boolean mb_settings_access_disabled;


    //**************************************************************************
    //********** Constructors **************************************************

    public ALFIView (EditorFrame _frame) {
        super();

        m_frame = _frame;
        mb_settings_access_disabled = false;

        addViewListener(
            new JGoViewListener() {
                public void viewChanged(JGoViewEvent _e) {
                    if (_e.getHint() == JGoViewEvent.SELECTION_GAINED) {
                        onSelect();
                    }
                }
            });
    }

    public void initialize() {
        setDoubleBuffered(true);
        m_active_object = null;

        addViewListener(this);
        setGridWidth(GRID_SIZE);
        setGridHeight(GRID_SIZE);
        setSnapMove(JGoView.SnapJump);
        setIncludingNegativeCoords(true);
        setAutoscrollInsets(new Insets(0, 0, 0, 0));    // disable autoscroll

            // this is to get and transfer focus on mouse enters and exits
        m_frame.addMouseListener(this);
    }

    /** Convenience method */
    public JGoDocument getDoc() {
        return getDocument();
    }

    /** Handle DELETE, HOME, and arrow keys as well as the page up/down keys. */
    public void onKeyEvent(KeyEvent evt) {
        int t = evt.getKeyCode();
        if (t == KeyEvent.VK_DELETE) { performDelete(); }
        else if ((t == KeyEvent.VK_RIGHT) || (t == KeyEvent.VK_LEFT) ||
                            (t == KeyEvent.VK_DOWN) || (t == KeyEvent.VK_UP)) {
            JGoObject primary = getSelection().getPrimarySelection();
            if (primary instanceof PortWidget) {
                PortWidget port_widget = (PortWidget) primary;
                performPortMove(port_widget, t, evt.getModifiers());
            }
            else { performInternalComponentsMove(t); }
        }
        else if (t == KeyEvent.VK_HOME) { setViewPosition(0, 0); }
        else if (t == KeyEvent.VK_ESCAPE) {
            getSelection().clearSelection();
            m_selected_widget = null;
        }
        else if (t == KeyEvent.VK_F1) { zoomIn(); }
        else if (t == KeyEvent.VK_F2) { zoomOut(); }
        else if (t == KeyEvent.VK_F3) { zoomNormal(); }
        else if (t == KeyEvent.VK_F4) { zoomToFit(); }
        else if (t == KeyEvent.VK_F11) { printToFitPage(); }
        else if (t == KeyEvent.VK_F12) { printToScale(); }
        else if (evt.isControlDown()) {
            if (t == KeyEvent.VK_C) { m_frame.copy(); }
            else if (t == KeyEvent.VK_X) { m_frame.cut(); }
            else if (t == KeyEvent.VK_V) { m_frame.paste(true); }
            else if (t == KeyEvent.VK_D) { m_frame.duplicate(); }
            else if (t == KeyEvent.VK_A) { selectAll(); }
            else if (t == KeyEvent.VK_L) { alignLeft(); }
            else if (t == KeyEvent.VK_R) { alignRight(); }
            else if (t == KeyEvent.VK_T) { alignTop(); }
            else if (t == KeyEvent.VK_B) { alignBottom(); }
            else if (t == KeyEvent.VK_G) { m_frame.group(); }
            else if (t == KeyEvent.VK_U) { m_frame.ungroup(); }
            else if (t == KeyEvent.VK_I) { Alfi.getMainWindow().showOverviewWindow(); }

            else { super.onKeyEvent(evt); }
        }
        else { super.onKeyEvent(evt); }
    }

        /** Deletes all selected items without affecting the clipboard. */
    public void performDelete() {
        ArrayList selection_list =
                            MiscTools.JGoSelectionToList(getSelection(), true);
        getOwnerFrame().delete(selection_list);
    }

    public void printToScale () {
        m_print_type = PRINT_TO_SCALE; 
        print();
    }

    public void printToFitPage () {
        m_print_type = PRINT_FIT_PAGE; 
        print();
    }

        /** For port repositioning in external view. */
    private boolean performPortMove(PortWidget _port_widget, int _key,
                                                              int _modifiers) {
    boolean b_push_ports = ((_modifiers & InputEvent.SHIFT_MASK) != 0);
    boolean b_edge_change = ((_modifiers & InputEvent.CTRL_MASK) != 0);
    boolean b_internal_view = m_frame.isInternalView();

    PortProxy port = _port_widget.getAssociatedProxy();
    Boolean B_increment_position = (b_edge_change) ? null :
          determineIfPositionIncrementOrDecrement(port, _key, b_internal_view);
    int mode_aux_info = -1;
    if (B_increment_position != null) {
        mode_aux_info = (B_increment_position.booleanValue()) ?
                                     PortWidget.PORT_MOVE_AUX_INFO__INCREMENT :
                                     PortWidget.PORT_MOVE_AUX_INFO__DECREMENT;
    }

    int move_mode = PortWidget.PORT_POSITION__MOVE_PORT;
    if (b_push_ports) {
        move_mode = PortWidget.PORT_POSITION__PUSH_PORTS;
    }
    else if (b_edge_change) {
        move_mode = PortWidget.PORT_POSITION__SIDE_CHANGE;
        if (_key == KeyEvent.VK_RIGHT) {
            mode_aux_info = PortWidget.PORT_MOVE_AUX_INFO__GO_TO_EAST;
        }
        else if (_key == KeyEvent.VK_LEFT) {
            mode_aux_info = PortWidget.PORT_MOVE_AUX_INFO__GO_TO_WEST;
        }
        else if (_key == KeyEvent.VK_UP) {
            mode_aux_info = PortWidget.PORT_MOVE_AUX_INFO__GO_TO_NORTH;
        }
        else if (_key == KeyEvent.VK_DOWN) {
            mode_aux_info = PortWidget.PORT_MOVE_AUX_INFO__GO_TO_SOUTH;
        }
    }

    boolean b_port_moved = false;
    if (mode_aux_info != -1) {
        _port_widget.reposition(move_mode, mode_aux_info);
        b_port_moved = true;
    }

    return b_port_moved;
}

        /**
         * Ports to be repositioned are going to be either incremented one open
         * position on the edge they are on, or decremented one.  Invalid key
         * presses result in a return of null (thus the use of a Boolean
         * return as opposed to just a boolean.)
         */
    private Boolean determineIfPositionIncrementOrDecrement(PortProxy _port,
                                          int _key, boolean _b_internal_view) {
        ALFINode node = m_frame.getNodeInEdit();
        if (! node.port_containsLocal(_port)) { return null; }

        int[] position = _port.getPosition();
        int edge = (_b_internal_view) ? position[0] : position[2];
        boolean b_increment_position;
        if ((edge == PortProxy.EDGE_NORTH) || (edge == PortProxy.EDGE_SOUTH)) {
            if (_key == KeyEvent.VK_LEFT) {
                b_increment_position = false;
            }
            else if (_key == KeyEvent.VK_RIGHT) {
                b_increment_position = true;
            }
            else { return null; }
        }
        else if ((edge == PortProxy.EDGE_EAST)||(edge == PortProxy.EDGE_WEST)) {
            if (_key == KeyEvent.VK_UP) {
                b_increment_position = false;
            }
            else if (_key == KeyEvent.VK_DOWN) {
                b_increment_position = true;
            }
            else { return null; }
        }
        else {
            Alfi.warn("ALFIView.determineIfPositionIncrementOrDecrement(): " +
                "Invalid edge.");
            return null;
        }
        return new Boolean(b_increment_position);
    }

        /** If _key == VK_UNDEFINED, then dx and dy will just be 0. */
    public void performInternalComponentsMove (int _key) {
        if (m_frame.isInternalView()) {
            int dx = 0;
            if (_key == KeyEvent.VK_RIGHT) { dx = GridTool.GRID_SPACING; }
            else if (_key == KeyEvent.VK_LEFT) { dx = - GridTool.GRID_SPACING; }
            int dy = 0;
            if (_key == KeyEvent.VK_DOWN) { dy = GridTool.GRID_SPACING; }
            else if (_key == KeyEvent.VK_UP) { dy = - GridTool.GRID_SPACING; }

            moveSelection(getSelection(), 0, dx, dy, 0);
        }
    }

    public boolean doWidgetPopupMenu (JGoObject _object, int _modifiers, 
                                      Point _dc, Point _vc) {
        BasicPopupMenu popup = null;
        pickObject(_object);
        ALFINode container = m_frame.getNodeInEdit();

        if (_object instanceof MemberNodeWidget) {
            popup = (BasicPopupMenu) m_frame.getNodeMenu();

                // usual state
            popup.setEnabled(EditorActionsIF.CUT, true);
            popup.setEnabled(EditorActionsIF.COPY, true);
            popup.setEnabled(EditorActionsIF.PASTE, true);
            popup.setEnabled(EditorActionsIF.DELETE, true);
            popup.setEnabled(EditorActionsIF.DUPLICATE, true);
            popup.setEnabled(EditorActionsIF.NODE_RENAME, true);
            popup.setEnabled(EditorActionsIF.NODE_SWAP, false);

            MemberNodeWidget mnw = (MemberNodeWidget) _object;
            ALFINode node = null;

            if (m_frame.isInternalView()) {
                MemberNodeProxy proxy = mnw.getAssociatedProxy();
                node = proxy.getAssociatedMemberNode(container);

                popup.setEnabled(EditorActionsIF.CENTER_PORTS, false);
            }
            else { 
                node = container; 
                popup.setEnabled(EditorActionsIF.CUT, false);
                popup.setEnabled(EditorActionsIF.COPY, false);
                popup.setEnabled(EditorActionsIF.PASTE, false);
                popup.setEnabled(EditorActionsIF.DELETE, false);
                popup.setEnabled(EditorActionsIF.DUPLICATE, false);

                popup.setEnabled(EditorActionsIF.ROTATE, false);
                popup.setEnabled(EditorActionsIF.SWAP, false);
                popup.setEnabled(EditorActionsIF.SYMMETRY, false);

                popup.setEnabled(EditorActionsIF.CENTER_PORTS,
                                                            node.isRootNode());
            }

                // set state of port centering checkbox
            boolean b_is_root_node = node.isRootNode();
            ALFINode root_base_node =
                             (b_is_root_node) ? node : node.baseNode_getRoot();
            boolean b_center_external_view_ports =
                                          root_base_node.getCentersPortsFlag();
            ((JCheckBoxMenuItem) ((NodePopupMenu) popup).
               getCenterPortsCheckbox()).setState(b_center_external_view_ports);

            if (getSelection().isEmpty()) {
                popup.setEnabled(EditorActionsIF.CUT, false);
                popup.setEnabled(EditorActionsIF.COPY, false);
                popup.setEnabled(EditorActionsIF.DELETE, false);
                popup.setEnabled(EditorActionsIF.DUPLICATE, false);
            }
            if (m_frame.allowNodesSwap()) {
                popup.setEnabled(EditorActionsIF.NODE_SWAP, true);
            }

            ALFINode clipboard_node = m_frame.getClipboard().getClipboardNode();
            if (! clipboard_node.hasLocalChanges(false)) {
                popup.setEnabled(EditorActionsIF.PASTE, false);
            }
 
            if (node.isPrimitive()) {
                if (mb_settings_access_disabled) { 
                    popup.setEnabled(EditorActionsIF.SETTINGS, false);
                    popup.setEnabled(EditorActionsIF.NODE_RENAME, false);
                }
                else {
                    popup.setEnabled(EditorActionsIF.SETTINGS, true);
                }
                //popup.setEnabled(EditorActionsIF.EDIT_MACROS, false);
                popup.setEnabled(EditorActionsIF.VIEW_INTERNAL, false);
                popup.setEnabled(EditorActionsIF.VIEW_BASE_BOX, false);
            }
            else {
                boolean b_enable_settings = b_is_root_node ||
                           node.baseNode_getRoot().linkDeclarations_hasLocal();
                popup.setEnabled(EditorActionsIF.SETTINGS, b_enable_settings);

                //popup.setEnabled(EditorActionsIF.EDIT_MACROS, true);
                popup.setEnabled(EditorActionsIF.VIEW_INTERNAL, true);
                popup.setEnabled(EditorActionsIF.VIEW_BASE_BOX, true);
            }

                // ops allowed for local member nodes only
            boolean b_is_local = (! mnw.isInherited());
            popup.setEnabled(EditorActionsIF.SWAP_X, b_is_local);
            popup.setEnabled(EditorActionsIF.SWAP_Y, b_is_local);
            popup.setEnabled(EditorActionsIF.SWAP_NONE, b_is_local);
            popup.setEnabled(EditorActionsIF.SYMMETRY_X, b_is_local);
            popup.setEnabled(EditorActionsIF.SYMMETRY_Y, b_is_local);
            popup.setEnabled(EditorActionsIF.SYMMETRY_PLUS_XY, b_is_local);
            popup.setEnabled(EditorActionsIF.SYMMETRY_MINUS_XY, b_is_local);
            popup.setEnabled(EditorActionsIF.SYMMETRY_NONE, b_is_local);
            popup.setEnabled(EditorActionsIF.ROTATE_NORTH, b_is_local);
            popup.setEnabled(EditorActionsIF.ROTATE_SOUTH, b_is_local);
            popup.setEnabled(EditorActionsIF.ROTATE_EAST, b_is_local);
            popup.setEnabled(EditorActionsIF.ROTATE_WEST, b_is_local);
            popup.setEnabled(EditorActionsIF.ALIGN, b_is_local);
        }
        else if (_object instanceof PortWidget) {
            PortWidget widget = (PortWidget) _object;
            PortProxy port = widget.getAssociatedProxy();
            popup = (BasicPopupMenu) m_frame.getPortMenu();

            boolean b_is_local_port = container.port_containsLocal(port);
            boolean b_port_has_connections = widget.hasConnections();
            boolean b_is_bundle_port =
                (port.getDataType() == GenericPortProxy.DATA_TYPE_ALFI_BUNDLE);
            boolean b_is_in_root_box = container.isRootNode();
            boolean b_is_input_port =
                                 (port.getIOType() == PortProxy.IO_TYPE_INPUT);

            popup.setEnabled(EditorActionsIF.PORT_MODIFY,
                              (b_is_local_port && (! b_port_has_connections)));

            popup.setEnabled(EditorActionsIF.PORT_DELETE, b_is_local_port);

            popup.setEnabled(EditorActionsIF.PORT_RENAME, b_is_local_port);

            popup.setEnabled(EditorActionsIF.IMPORT_CHANNELS, (b_is_local_port
                  && b_is_bundle_port && b_is_in_root_box && b_is_input_port));
        }
        else if (_object instanceof ConnectionWidget) {
            ConnectionWidget cw = (ConnectionWidget) _object;
            popup = (BasicPopupMenu) m_frame.getConnectionsMenu();

            ConnectionProxy connection = cw.getAssociatedProxy();
            boolean b_inherited_connection =
                            container.connection_containsInherited(connection);
            if (b_inherited_connection) {
                if ((! (connection.getSink() instanceof PortProxy)) ||
                                         (connection instanceof BundleProxy)) {
                    popup.setEnabled(EditorActionsIF.CONN_DELETE, false);
                }
                else { popup.setEnabled(EditorActionsIF.CONN_DELETE, true); }

                popup.setEnabled(EditorActionsIF.CONN_SMOOTH, false);
                popup.setEnabled(EditorActionsIF.CONN_CREATE_JUNCTION, false);
                popup.setEnabled(EditorActionsIF.CONN_CREATE_BUNDLER, false);
                popup.setEnabled(EditorActionsIF.CONN_TRIM, false);
            }
            else {
                popup.setEnabled(EditorActionsIF.CONN_DELETE, true);
                popup.setEnabled(EditorActionsIF.CONN_SMOOTH, true);
                popup.setEnabled(EditorActionsIF.CONN_CREATE_JUNCTION, true);
                popup.setEnabled(EditorActionsIF.CONN_CREATE_BUNDLER,
                                          (connection instanceof BundleProxy));
                popup.setEnabled(EditorActionsIF.CONN_TRIM, true);
            }

            popup.setEnabled(EditorActionsIF.SHOW_BUNDLE_CONTENT,
                                          (connection instanceof BundleProxy));
        }
        else if (_object instanceof JunctionWidget) {
            JunctionProxy junction =
                              ((JunctionWidget) _object).getAssociatedProxy();
            popup = (BasicPopupMenu) m_frame.getJunctionsMenu();
            if (m_frame.getNodeInEdit().junction_containsLocal(junction)) {
                popup.setEnabled(EditorActionsIF.JUNCTION_DELETE, true);
            }
            else { popup.setEnabled(EditorActionsIF.JUNCTION_DELETE, false); }
        }
        else if (_object instanceof BundlerWidget) {
            BundlerProxy bundler =
                                ((BundlerWidget) _object).getAssociatedProxy();
            popup = (BasicPopupMenu) m_frame.getBundlersMenu();
            if (container.bundler_containsLocal(bundler)) {
                popup.setEnabled(EditorActionsIF.BUNDLER_DELETE, true);
            }
            else {
                popup.setEnabled(EditorActionsIF.BUNDLER_DELETE, false);
            }
            popup.setEnabled(EditorActionsIF.BUNDLER_IO_RENAME,
                                            bundler.hasSecondaryIO(container));

            boolean b_enable_1st_in_to_2nd = false;
            boolean b_enable_1st_out_to_2nd = false;
            if (container.bundler_containsLocal(bundler) &&
                                       (! bundler.hasSecondaryIO(container))) {
                b_enable_1st_in_to_2nd = bundler.hasPrimaryInput(container);
                b_enable_1st_out_to_2nd = bundler.hasPrimaryOutput(container);
            }

            popup.setEnabled(EditorActionsIF.BUNDLER_PRIMARY_IN_TO_SECONDARY,
                                                      b_enable_1st_in_to_2nd);
            popup.setEnabled(EditorActionsIF.BUNDLER_PRIMARY_OUT_TO_SECONDARY,
                                                      b_enable_1st_out_to_2nd);
        }

        if (popup != null) {    
            popup.show(this, _vc.x, _vc.y);
            return true;
        }
        else { return false; }
    }

    private boolean displayBackgroundMenu (int _modifiers, Point _dc, 
                                           Point _vc){
    
        m_default_location = _dc;
        if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
            return doCommandPopupMenu(_modifiers, _dc, _vc);
        }
        else if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
            if ((_modifiers & InputEvent.CTRL_MASK) != 0) {
                return doBoxFilesPopupMenu(_modifiers, _dc, _vc);
            }
            else { 
                if (!m_frame.isToolbarSelected()) {
                    return doPrimitivesMenu(_modifiers, _dc, _vc); 
                }
            }
        }
        return false;
    }

    public boolean doCommandPopupMenu (int _modifiers, Point _dc, Point _vc) {
        JPopupMenu popup = (JPopupMenu) m_frame.getDefaultPopupMenu(); 

            // disable cut and copy in clipboard window
        if (popup instanceof BasicPopupMenu) {
            BasicPopupMenu basic_popup = (BasicPopupMenu) popup;

                // usual state
            basic_popup.setEnabled(EditorActionsIF.CUT, true);
            basic_popup.setEnabled(EditorActionsIF.COPY, true);
            basic_popup.setEnabled(EditorActionsIF.PASTE, true);
            basic_popup.setEnabled(EditorActionsIF.DELETE, true);
            basic_popup.setEnabled(EditorActionsIF.DUPLICATE, true);
            basic_popup.setEnabled(EditorActionsIF.SELECT_ALL, true);

                // "vetoes" //

            if (! m_frame.isInternalView()) {
                basic_popup.setEnabled(EditorActionsIF.CUT, false);
                basic_popup.setEnabled(EditorActionsIF.COPY, false);
                basic_popup.setEnabled(EditorActionsIF.PASTE, false);
                basic_popup.setEnabled(EditorActionsIF.DELETE, false);
                basic_popup.setEnabled(EditorActionsIF.DUPLICATE, false);
                basic_popup.setEnabled(EditorActionsIF.SELECT_ALL, false);
            }

            if (getSelection().isEmpty()) {
                basic_popup.setEnabled(EditorActionsIF.CUT, false);
                basic_popup.setEnabled(EditorActionsIF.COPY, false);
                basic_popup.setEnabled(EditorActionsIF.DELETE, false);
                basic_popup.setEnabled(EditorActionsIF.DUPLICATE, false);
            }

            ALFINode root_container_node =
                         m_frame.getNodeInEdit().containerNode_getRoot();
            ALFINode clipboard_node = m_frame.getClipboard().getClipboardNode();
            if (root_container_node == clipboard_node) {
                basic_popup.setEnabled(EditorActionsIF.CUT, false);
                basic_popup.setEnabled(EditorActionsIF.COPY, false);
                basic_popup.setEnabled(EditorActionsIF.PASTE, false);
                basic_popup.setEnabled(EditorActionsIF.DUPLICATE, false);
                basic_popup.setEnabled(EditorActionsIF.SELECT_ALL, false);
                basic_popup.setEnabled(EditorActionsIF.ALIGN, false);
            }

            if (! clipboard_node.hasLocalChanges(false)) {
                basic_popup.setEnabled(EditorActionsIF.PASTE, false);
            }
        }

        popup.show(this, _vc.x, _vc.y);
        return true;
    }
    
    public boolean doPrimitivesMenu (int _modifiers, Point _dc, Point _vc) {
        if (!m_frame.isInternalView()){
            return false; 
        }
        AlfiMainFrame parent = (AlfiMainFrame)m_frame.getParentFrame();
        JPopupMenu popup = parent.getPrimitivesMenu();
        popup.show(this, _vc.x, _vc.y);
        return true;
    }

    public boolean doBoxFilesPopupMenu(int _modifiers, Point _dc, Point _vc) {
        if (! m_frame.isInternalView()) {
            return false; 
        }
        BoxFilesPopupMenu popup = m_frame.getBoxFilesMenu();
        popup.show(this, _vc.x, _vc.y);
        return true;
    }

    public boolean doCommentPopupMenu (JGoObject _object, int _modifiers, 
                                      Point _dc, Point _vc) {
        if (! m_frame.isInternalView()) {
            return false; 
        }
        BasicPopupMenu popup = ( BasicPopupMenu )m_frame.getCommentMenu();

            // usual state
        popup.setEnabled(EditorActionsIF.CUT, true);
        popup.setEnabled(EditorActionsIF.COPY, true);
        popup.setEnabled(EditorActionsIF.PASTE, true);
        popup.setEnabled(EditorActionsIF.DELETE, true);
        popup.setEnabled(EditorActionsIF.DUPLICATE, true);

        popup.show(this, _vc.x, _vc.y);
        return true;
    }

    public boolean doHighlightPopupMenu (JGoObject _object, int _modifiers, 
                                      Point _dc, Point _vc) {
        if (! m_frame.isInternalView()) {
            return false; 
        }

        BasicPopupMenu popup = ( BasicPopupMenu )m_frame.getHighlightMenu();

            // usual state
        popup.setEnabled(EditorActionsIF.CUT, true);
        popup.setEnabled(EditorActionsIF.COPY, true);
        popup.setEnabled(EditorActionsIF.PASTE, true);
        popup.setEnabled(EditorActionsIF.DELETE, true);
        popup.setEnabled(EditorActionsIF.DUPLICATE, true);

        popup.show(this, _vc.x, _vc.y);
        return true;
    }


    
    // implement JGoViewListener
    // just need to keep the actions enabled appropriately
    // depending on the selection
    public void viewChanged(JGoViewEvent e) {
        // if the selection changed, maybe some commands need to
        // be disabled or re-enabled
        switch(e.getHint()) {
          case JGoViewEvent.UPDATE_ALL:
          case JGoViewEvent.SELECTION_GAINED:
          case JGoViewEvent.SELECTION_LOST:
          case JGoViewEvent.SCALE_CHANGED:
            //AppAction.updateAllActions();
            break;
        }
    }
    
    // implement JGoDocumentListener
    // here we just need to keep the title bar up-to-date
    public void documentChanged(JGoDocumentEvent evt) {
        if ((evt != null) &&
                      (evt.getHint() == JGoDocumentEvent.MODIFIABLE_CHANGED)) {
              updateTitle();
        }
        super.documentChanged(evt);
    }

    // have the title bar for the internal frame include the name
    // of the document and whether it's read-only
    public void updateTitle() {
        if (m_frame != null) {
            String title = m_frame.getName();
            if (!getDocument().isModifiable()) { title += " (read-only)"; }
            m_frame.setTitle(title);
            m_frame.repaint();
        }
    }
    
    // the default place to put stuff if not dragged there
    public Point getDefaultLocation() {
        // to avoid constantly putting things in the same place,
        // keep shifting the default location
        if (m_default_location != null) {
            m_default_location.x += 10;
            m_default_location.y += 10;
        }
        return m_default_location;
    }

    public void setDefaultLocation (Point loc) {
        m_default_location = loc;
    }

    // toggle the grid appearance
    void showGrid() {
        int style = getGridStyle();
        if (style == JGoView.GridInvisible) {
            style = JGoView.GridDot;
            setGridPen(JGoPen.black);
        }
        else { style = JGoView.GridInvisible; }

        setGridStyle(style);
    }

        /** This method removes a resize handle object in the view if there is
          * one located at _point.
          */
    public void disableResizeHandleAt(Point _point) {
        JGoHandle handle = pickHandle(_point);
        if (handle != null) {
            removeObject(handle);
        }
    }

    /**
      * Printing support
      */
    public Rectangle2D.Double getPrintPageRect (Graphics2D _g2, PageFormat _pf)
    {
        // leave some space at the top of the image for the node name.
        Rectangle2D.Double rect = new Rectangle2D.Double(_pf.getImageableX(), 
            _pf.getImageableY() + 20,
            _pf.getImageableWidth(), 
            _pf.getImageableHeight() - 20);
        return rect;
    }

    public double getPrintScale (Graphics2D _g2, PageFormat _pf)
    {
        if (m_print_type == PRINT_TO_SCALE) {
            //
            // print using the view's currently selected scale
            //
            return getScale();
        } else {
            //
            // scale to fit printed page
            //
            Rectangle2D.Double pageRect = getPrintPageRect(_g2, _pf);
            Dimension docSize = getPrintDocumentSize();
            //
            // make sure it doesn't get scaled too much! 
            // (especially if no objects in document)
            //
            docSize.width = Math.max(docSize.width, 50);
            docSize.height = Math.max(docSize.height, 50);
            // 
            // Make sure that it fits within the page rectangle.
            // 
            double hratio = (pageRect.width  - 5) / docSize.width;
            double vratio = (pageRect.height - 5) / docSize.height;
            return Math.min(hratio, vratio);
        }
    }

    public void printDecoration (Graphics2D _g2, PageFormat _pf, 
                                 int _hpnum, int _vpnum)
    {
        // draw corners around the getPrintPageRect area
        // super.printDecoration(_g2, _pf, _hpnum, _vpnum);

        // print the n,m page number in the footer
        String title = m_frame.getNodeInEdit().toString();

        Paint oldpaint = _g2.getPaint();
        _g2.setPaint(Color.black);
        Font oldfont = _g2.getFont();
        _g2.setFont(new Font(JGoText.getDefaultFontFaceName(), Font.PLAIN, 10));
        _g2.drawString(title, 
            (int)(_pf.getImageableX() + 10),
            (int)(_pf.getImageableY() + 10 ));
        _g2.setPaint(oldpaint);
        _g2.setFont(oldfont);
    }


    void zoomIn() {
        double newscale = Math.rint(getScale() / 0.9f * 100f) / 100f;
        setScale(newscale);
    }

    void zoomOut() {
        double newscale = Math.rint(getScale() * 0.9f * 100f) / 100f;
        setScale(newscale);
    }

    void zoomNormal() {
        setScale(1.0d);
    }

    void zoomToFit() {
        double newscale = 1;
        if (!getDocument().isEmpty()) {
          double extentWidth = getExtentSize().width;
          double printWidth = getPrintDocumentSize().width;
          double extentHeight = getExtentSize().height;
          double printHeight = getPrintDocumentSize().height;
          newscale = Math.min((extentWidth / printWidth),
                              (extentHeight / printHeight));
        }
        if (newscale > 2)
            newscale = 1;
        newscale *= getScale();
        setScale(newscale);
        setViewPosition(0, 0);
    }

    public EditorFrame getOwnerFrame () {
        return m_frame;
    }

    /** Get one of the layers in the document. */
    public JGoLayer getDocumentLayer (int _layer_id) {
        return m_frame.getDocumentLayer(_layer_id);
    }

        /**
         * Returns the location (in document coords) of the most recent mouse
         * down event in this view.
         */
    public Point getLastMouseDownLocation_dc() {
        return new Point(m_mouse_location_dc);
    }

    public void pickObject (JGoObject _object) {

        boolean display_handles = false;

        if (_object instanceof MemberNodeWidget || 
            _object instanceof TextCommentWidget || 
            _object instanceof ColoredBoxWidget) {
            display_handles = true;
        }    

        this.pickObject(_object, display_handles);
    }

    public void pickObject (JGoObject _object, boolean _displayHandles) {
        JGoSelection selection = getSelection();
        if (!selection.isInSelection(_object)){
            selection.clearSelection();
            selectObject(_object);
        } 

        if (!_displayHandles) {
            selection.clearSelectionHandles(_object); 
        }
    }

    public JGoObject getObject (Point _view_coordinates) {
        // Get the document coordinates from the view coordinates
        Point doc_coordinates = viewToDocCoords(_view_coordinates);
         
        if (doc_coordinates != null) {
            JGoObject object = pickDocObject(doc_coordinates, false);
            if (object != null) {
                while (object != null) {
                    if (object instanceof AlfiWidgetIF) {
                        return object;
                    } 
                    object = object.getParent();
                } 
            }
        }
        return null;
    }

        /** See mb_settings_access_disabled. */
    void disableParameterSettingAccess() {
        mb_settings_access_disabled = true;
    }

        /** See event processing notes in the general notes for this class. */
    public boolean doMouseDown (int _modifiers, Point _dc, Point _vc) {
        m_mouse_location_dc = new Point(_dc);

            // if there is an active object, allow it first shot at processing
            // the event.
        if (activeObjectExists()) {
            if (getActiveObject().doMouseDown(_modifiers, _dc, _vc, this)) {
                return true;
            }
        }
        else {
            JGoObject doc_object = pickDocObject(_dc, false);
            JGoHandle handle = pickHandle(_vc);
            if (doc_object != null) {
                m_frame.bringObjectToTopOfItsLayer(doc_object);

                    // See notes on mb_mouse_drag_is_armed.
                mb_mouse_drag_is_armed = true;
                mb_mouse_drag_has_occured = false;

                if (keyExtendSelection(_modifiers)) {
                    return  super.doMouseDown(_modifiers, _dc, _vc);
                }

                m_selected_widget = null;
                    // if there is an alfi widget under the mouse, give it a
                    // chance to process the event.
                while (doc_object != null) {
                    if (doc_object instanceof AlfiWidgetIF) {
                        m_selected_widget = (AlfiWidgetIF) doc_object;
                        if ((_modifiers & (InputEvent.BUTTON1_MASK |
                                           InputEvent.BUTTON3_MASK)) != 0) {
                            if (m_frame.isToolbarSelected()) {
                                  Alfi.getPrimitivesToolbar().clearSelection();
                            }
                            pickObject(doc_object);
                        }
                        boolean ret = m_selected_widget.doMouseDown(
                                                  _modifiers, _dc, _vc, this); 
                        if (ret) return true;
                    }
                    doc_object = doc_object.getParent();
                }
            }
                // external view resize handles should work
            else if ((! getOwnerFrame().isInternalView()) && (handle != null)) {
                // do nothing to inhibit JGo default response here (but see
                // override of JGo's handleResizing() herein.)
                //super.doMouseDown(_modifiers, _dc, _vc); 
            }
            else {
                    // first check if there are objects selected.  the popup
                    // menus should not be activated if objects are selected.
                JGoSelection selections = getSelection();
                if ((selections == null) || (selections.getNumObjects() == 0)) {
                    if (((_modifiers & InputEvent.BUTTON1_MASK) != 0) &&
                                  ((_modifiers & InputEvent.CTRL_MASK) == 0) &&
                                  m_frame.isToolbarSelected()) {
                         Alfi.getPrimitivesToolbar().addNode(m_frame);
                         return true;
                    }
                    else {
                        if (mb_settings_access_disabled) {
                                // do nothing
                            return true;
                        }
                        else {
                            boolean displayed_menu = 
                                   displayBackgroundMenu(_modifiers, _dc, _vc);
                            if (displayed_menu) { return true; }
                        }
                    }
                }
                else {
                        // left and right clicks in an empty space when
                        // something is selected should merely deselect all.
                    if ((_modifiers & InputEvent.BUTTON1_MASK) != 0) {
                        getSelection().clearSelection();
                        m_selected_widget = null;
                        if (m_frame.isToolbarSelected()) {
                            Alfi.getPrimitivesToolbar().addNode(m_frame);
                            return true;
                        }
                    }
                    else if ((_modifiers & InputEvent.BUTTON3_MASK) != 0) {
                        Alfi.getPrimitivesToolbar().clearSelection();
                        if (mb_settings_access_disabled) {
                                // do nothing
                            return true;
                        }
                        else {
                            boolean displayed_menu =
                                displayBackgroundMenu(_modifiers, _dc, _vc); 
                            if (displayed_menu) {return true;}
                        }
                    }
                }
            }
        }

            // otherwise implement the default behavior
        return super.doMouseDown(_modifiers, _dc, _vc);
    }

        /** See event processing notes in the general notes for this class. */
    public boolean doMouseUp(int _modifiers, Point _dc, Point _vc) {
        Boolean super_return = null;

            // See notes on mb_mouse_drag_is_armed.
        boolean b_mouse_drag_has_occured = mb_mouse_drag_has_occured;
        mb_mouse_drag_is_armed = false;    // reset before return
        mb_mouse_drag_has_occured = false; // reset before return

            // if there is an active object, allow it first shot at processing
            // the event.
        if (activeObjectExists()) {
            if (getActiveObject().doMouseUp(_modifiers, _dc, _vc, this)) {
                processConnectionsBetweenSelectedObjects();
                return true;
            }
        }
        else if (m_selected_widget != null) {
            if (keyExtendSelection(_modifiers)) {
                super_return = new Boolean(super.doMouseUp(_modifiers,_dc,_vc));

                    // See notes on mb_mouse_drag_is_armed.
                if (! b_mouse_drag_has_occured) {
                    processConnectionsBetweenSelectedObjects();
                    return super_return.booleanValue();
                }
            }

                // if there is an alfi widget under the mouse, give it a chance
                // to process the event.
            if (m_selected_widget != null && m_selected_widget.doMouseUp(_modifiers,_dc,_vc,this)) {
                processConnectionsBetweenSelectedObjects();
                return true;
            }
        }

            // otherwise implement the default behavior (if it wasn't already)
        if (super_return == null) {
            boolean super_return_value = super.doMouseUp(_modifiers, _dc, _vc);
            processConnectionsBetweenSelectedObjects();
            return super_return_value;
        }

        return false;
    }

        /** See event processing notes in the general notes for this class. */
    public boolean doMouseClick(int _modifiers, Point _dc, Point _vc) {
        m_selected_widget = null;
        JGoObject obj = pickDocObject(_dc, false);

            // if there is an active object, allow it first shot at processing
            // the event.
        if (activeObjectExists()) {
            if (getActiveObject().doMouseClick(_modifiers, _dc, _vc, this)) {
                return true;
            }
            else { clearActiveObject();
            }
        }
        else {
            // if there is an alfi widget under the mouse, give it a chance
            // to process the event.
            while (obj != null) {
                if ((obj instanceof AlfiWidgetIF) &&
                            ((AlfiWidgetIF) obj).doMouseClick(_modifiers,
                                                           _dc, _vc, this)) {
                    return true;
                }
                else if (obj instanceof EditorFrame.PortBorder) {
                    return ((EditorFrame.PortBorder) obj).
                                      doMouseClick(_modifiers, _dc, _vc, this);
                }
                else { obj = obj.getParent(); }
            }
        } 
        
        /* NOTE:  Removed superclass call, since it resulted to
         * a stack overflow with JGO 5.14 
         */
        //return super.doMouseClick(_modifiers, _dc, _vc);

        super.doBackgroundClick(_modifiers, _dc, _vc);
        return true;
    }

        /** Just send this event to doMouseClick() for general processing. */
    public void doBackgroundClick(int _modifiers, Point _dc, Point _vc) {
        m_selected_widget = null;
            // do the default behavior if doMouseClick() doesn't do anything
        if (! doMouseClick(_modifiers, _dc, _vc)) {
            super.doBackgroundClick(_modifiers, _dc, _vc);
        }
    }

        /** See event processing notes in the general notes for this class. */
    public boolean doMouseDblClick(int _modifiers, Point _dc, Point _vc) {
        m_selected_widget = null;
        JGoObject obj = pickDocObject(_dc, false);

            // if there is an active object, allow it first shot at processing
            // the event.
        if (activeObjectExists()) {
            if (getActiveObject().doMouseDblClick(_modifiers, _dc, _vc, this)) {
                return true;
            }
        }
        else if (obj != null) {
                // if there is an alfi widget under the mouse, give it a chance
                // to process the event.
            while (obj != null) {
                if ((obj instanceof AlfiWidgetIF) &&  
                    ((AlfiWidgetIF) obj).doMouseDblClick(_modifiers,
                        _dc, _vc, this)) {
                        return true;
                }
                else { obj = obj.getParent(); }
            }
        }

            // otherwise implement the default behavior
        return super.doMouseDblClick(_modifiers, _dc, _vc);
    }
    

    protected boolean keyExtendSelection(int _modifiers) {
        m_multiple_select = false;

        if ((_modifiers & InputEvent.SHIFT_MASK) != 0) {
            m_multiple_select = true;
        }
        
        return m_multiple_select;
    }

    protected void handleResizing(Graphics2D _g, Point _vc, Point _dc, int _event)
    {
        super.handleResizing(_g, _vc, _dc, _event);

            JGoObject current_object = getCurrentObject();
            if (current_object instanceof ColoredBoxWidget) {
                ColoredBoxWidget cw = ( ColoredBoxWidget ) current_object;
                ColoredBox cb_data = cw.getAssociatedProxy();

                Point cb_location = cb_data.getLocation();
                Point widget_location = cw.getLocation();
                Dimension cb_size = cb_data.getSize();
                Dimension widget_size = cw.getSize();

                if (!cb_location.equals(widget_location)) {
                            // triggers event to update widget location
                    cb_data.setLocation(widget_location);
                }

                if (!cb_size.equals(widget_size)) {
                            // triggers event to update widget size
                    cb_data.setSize(widget_size);
                }
            }
 
    }
  
    protected void onMouseDragged (MouseEvent _event) {

            // cancel LMB drag events on inherited alfi widgets
        if (SwingUtilities.isLeftMouseButton(_event)) {
            JGoObject current_object = getCurrentObject();
            if (current_object instanceof AlfiWidgetIF) {
                AlfiWidgetIF alfi_widget = (AlfiWidgetIF) current_object;

                if (! alfi_widget.isLocal()) {
                    Alfi.warn("Inherited objects cannot be moved.", false);
                    getSelection().clearSelection();
                    doCancelMouse();
                }
            
                if (alfi_widget instanceof ColoredBoxWidget) {
                    ColoredBoxWidget cbw = ( ColoredBoxWidget ) alfi_widget;
                    ColoredBox bg = cbw.getAssociatedProxy();
                    Dimension bg_size = bg.getSize();
                    Dimension widget_size = cbw.getSize();
                    if (!bg_size.equals(widget_size)) {
                            // triggers event to update widget size
                        bg.setSize(widget_size);
                    }
                }
            }
        }

/*
        if (activeObjectExists()) {
            super.onMouseDragged(_event);
        }
        else {
            Point view_coords = _event.getPoint();
            JGoObject object = getObject(view_coords);
            if (SwingUtilities.isLeftMouseButton(_event)) {
                if (! getOwnerFrame().isInternalView()) {
                    super.onMouseDragged(_event);
                }
                else if (m_frame.isModifiable()) {
                    super.onMouseDragged(_event);
                }
                else {
                        // get the view and document coordinates
                        // and save them.  These coordinates will
                        // be used after the popup is displayed.
                    Point doc_coords = new Point(view_coords);
                    convertViewToDoc(doc_coords);

                    if (m_frame.isInternalView()) {
                        JOptionPane.showMessageDialog(this, NODE_INHERITED, 
                               ILLEGAL_OPERATION, JOptionPane.WARNING_MESSAGE);
                    }

                        // Simulate a mouse click
                    int modifiers = _event.getModifiers();
                    doMouseDown(modifiers, doc_coords, view_coords);
                    doMouseUp(modifiers, doc_coords, view_coords);
                    doMouseClick(modifiers, doc_coords, view_coords);
                }
            }
            else if (object == null) {
                super.onMouseDragged(_event);
            }
            else {
                // no call to super.onMouseDragged().  i.e., avoid any JGoObject
                // movement on the right or middle buttons.
            }
        }
*/
    }

    /** See event processing notes in the general notes for this class. */
    public boolean doMouseMove(int _modifiers, Point _dc, Point _vc) {
        // See notes on mb_mouse_drag_is_armed.
        if (mb_mouse_drag_is_armed) {
            mb_mouse_drag_has_occured = true;
            mb_mouse_drag_is_armed = false;    // reset
        }

        // if there is an active object, allow it first shot at processing
        // the event.
        if (activeObjectExists()) {
            if (getActiveObject().doMouseMove(_modifiers, _dc, _vc, this)) {
                return true;
            }
        }
        // otherwise implement the default behavior
        return super.doMouseMove(_modifiers, _dc, _vc);
    }

    /** See event processing notes in the general notes for this class. */
    public void doUncapturedMouseMove(int _modifiers, Point _dc, Point _vc) {
        JGoObject obj = pickDocObject(_dc, false);

        // if there is an active object, allow it first shot at processing
        // the event.
        if (activeObjectExists()) {
            if (getActiveObject().doUncapturedMouseMove(_modifiers,
                                                        _dc, _vc, this)) {
                return;
            }
        }
        else if (obj != null) {
            // if there is an alfi widget under the mouse, give it a chance
            // to process the event.
            while (obj != null) {
                if ((obj instanceof AlfiWidgetIF) && ((AlfiWidgetIF) obj).
                           doUncapturedMouseMove(_modifiers, _dc, _vc, this)) {
                    m_object_mouse_is_over = obj;
                    return;
                }
                else { obj = obj.getParent(); }
            }
        }

        m_object_mouse_is_over = obj;

        // default processing
        m_frame.setStatusText(m_frame.getDefaultStatusText());
    }

        /**
         * This method looks for connections between selected objects, and if
         * any exist, processes the movement of the links.
         */
    private void processConnectionsBetweenSelectedObjects() {
        JGoSelection selection = getSelection();
        ArrayList selection_list =
                            MiscTools.JGoSelectionToList(selection, true);
        ConnectionWidget[] cws = getOwnerFrame().getConnectionWidgets();
        for (int i = 0; i < cws.length; i++) {
            if (sinkAndSourceAreSelected(cws[i], selection_list)) {
                selection.extendSelection(cws[i]);
                selection.hideHandles(cws[i]);
            }
        }
    }

        /** Determines if a connection has both source and sink selected. */
    private boolean sinkAndSourceAreSelected(ConnectionWidget _cw,
                                             ArrayList _selection_list) {
        JGoPort source_widget = _cw.getSourceWidget();
        boolean b_source_is_selected = false;

        if (_selection_list.contains(source_widget)) {    // junctions
            b_source_is_selected = true;
        }
        if (source_widget instanceof PortWidget) {
            MemberNodeWidget source_mnw =
                    ((PortWidget) source_widget).getOwnerMemberWidget();
            if (_selection_list.contains(source_mnw)) {
                b_source_is_selected = true;
            }
        }

        if (b_source_is_selected) {
            JGoPort sink_widget = _cw.getSinkWidget();
            boolean b_sink_is_selected = false;

            if (_selection_list.contains(sink_widget)) {    // junctions
                b_sink_is_selected = true;
            }
            if (sink_widget instanceof PortWidget) {
                MemberNodeWidget sink_mnw =
                        ((PortWidget) sink_widget).getOwnerMemberWidget();
                if (_selection_list.contains(sink_mnw)) {
                    b_sink_is_selected = true;
                }
            }
            if (b_sink_is_selected) { return true; }
        }

        return false;
    }

    /** Tests if there is a current target for events in the view. */
    public boolean activeObjectExists() { return (m_active_object != null); }

    /** Gets the current target for events, if one exists. */
    public AlfiWidgetIF getActiveObject() { return m_active_object; }

    /** Sets the current target for events. */
    public void setActiveObject(AlfiWidgetIF _object) {
        m_active_object = _object;
    }

    /** Clears the current target for events, if one exists. */
    public void clearActiveObject() { m_active_object = null; }

    /** Cancels the building of the connection widget. */
    public void cancelActiveObjectAction () {
        if (m_active_object instanceof ConnectionBuildingWidget) {
            ConnectionBuildingWidget cbw = (ConnectionBuildingWidget) 
                m_active_object;
            removeObject(cbw);
            clearActiveObject();
            setCursor(Cursor.getDefaultCursor());
        }
    }

    ALFINode getMemberNodeAssociatedWithMostRecentClick() {
        JGoObject o = getDocument().pickObject(m_mouse_location_dc, true);
        if (o instanceof MemberNodeWidget) {
            return ((MemberNodeWidget) o).getNode();
        }
        else { return null; }
    }

    /** Disables the default link creation in JGo.  Just returns false. */
    public boolean startNewLink(JGoPort _port, Point _dc) { return false; }

    /**
      * This is overridden in order to get rid of the little document icon
      * which shows up whenever a drag of any sort happens in the view.  But
      * it may need to be fiddled with more if drag and drop operations
      * require it.  I don't know for sure.
      */
    public void dragGestureRecognized(DragGestureEvent e) {
        try {
            AlfiWidgetIF active_object = getActiveObject();
            if (active_object instanceof ConnectionWidget) {
                e.startDrag( ((ConnectionWidget) active_object).
                                      determineResizeCursor(e.getDragOrigin()),
                             getSelection(), getCanvas());
            }
            else { 
                e.startDrag(getCursor(), getSelection(), getCanvas()); 
            }
        }
        catch(Exception x) { x.printStackTrace(); }
    }

        /** See determineMinimumRect(ALFINode, true, boolean, true). */
    public Rectangle determineMinimumRect() {
        return determineMinimumRect(m_frame.getNodeInEdit(),
                                    true, m_frame.isInternalView());
    }

        /** See determineMinimumRect(_container_node, _b_include_origin,
          *                          _b_internal_view, true).
          */
    public static Rectangle determineMinimumRect(ALFINode _container_node,
                         boolean _b_include_origin, boolean _b_internal_view) {
        return determineMinimumRect(_container_node, _b_include_origin,
                                    _b_internal_view, true);
    }

        /** This method determines the minimum sized rectangle that will
          * encompass all the elements inside the view.  Note that this method
          * should give correct results BEFORE the view/edit window is shown!
          */
    public static Rectangle determineMinimumRect(ALFINode _container_node,
                          boolean _b_include_origin, boolean _b_internal_view,
                          boolean _b_insert_border_space) {
        Rectangle minimum_rect = null;
        if (_b_internal_view) {
            int top = Integer.MAX_VALUE;
            int bottom = Integer.MIN_VALUE;
            int left = Integer.MAX_VALUE;
            int right = Integer.MIN_VALUE;

                // make sure all external ports are inside
            PortProxy[][] ports = _container_node.ports_getByPosition(true);
            int max_count_in_x = Math.max(ports[PortProxy.EDGE_NORTH].length,
                                          ports[PortProxy.EDGE_SOUTH].length);
            right = (max_count_in_x + 2) * (GridTool.GRID_SPACING * 2 );
            int max_count_in_y = Math.max(ports[PortProxy.EDGE_EAST].length,
                                          ports[PortProxy.EDGE_WEST].length);
            bottom = (max_count_in_y + 2) * (GridTool.GRID_SPACING * 2 );

                // make sure all junctions are inside
            JunctionProxy[] junctions = _container_node.junctions_get();
            int junction_offset = JunctionWidget.JUNCTION_SIDE / 2;
            for (int i = 0; i < junctions.length; i++) {
                Point location = junctions[i].getLocation();
                top = Math.min(top, location.y - junction_offset);
                bottom = Math.max(bottom, location.y + junction_offset);
                left = Math.min(left, location.x - junction_offset);
                right = Math.max(right, location.x + junction_offset);
            }

                // and all bundlers
            BundlerProxy[] bundlers = _container_node.bundlers_get();
            int bundler_offset = BundlerWidget.BUNDLER_SIDE / 2;
            for (int i = 0; i < bundlers.length; i++) {
                Point location = bundlers[i].getLocation();
                top = Math.min(top, location.y - bundler_offset);
                bottom = Math.max(bottom, location.y + bundler_offset);
                left = Math.min(left, location.x - bundler_offset);
                right = Math.max(right, location.x + bundler_offset);
            }

                // and all connections
            ConnectionProxy[] connections = _container_node.connections_get();
            int connection_offset = ConnectionWidget.LOCAL_CONNECTION_WIDTH / 2;
            for (int i = 0; i < connections.length; i++) {
                Integer[] path_definition = connections[i].getPathDefinition();
                for (int j = 0; j < path_definition.length; j++) {
                    if (path_definition[j] != null) {
                        if ((j % 2) == 0) {
                            int x = path_definition[j].intValue();
                            left = Math.min(left, x - connection_offset);
                            right = Math.max(right, x + connection_offset);
                        }
                        else {
                            int y = path_definition[j].intValue();
                            top = Math.min(top, y - connection_offset);
                            bottom = Math.max(bottom, y + connection_offset);
                        }
                    }
                }
            }

                // and all member nodes
            MemberNodeProxy[] member_node_proxies =
                                    _container_node.memberNodeProxies_get();
            for (int i = 0; i < member_node_proxies.length; i++) {
                ALFINode member_node = member_node_proxies[i].
                                      getAssociatedMemberNode(_container_node);
                Point location = member_node_proxies[i].getLocation();
                Dimension dims = MemberNodeWidget.determineWidgetSize(
                                                           member_node, false);
                top = Math.min(top, location.y);
                bottom = Math.max(bottom, location.y + dims.height);
                left = Math.min(left, location.x);
                right = Math.max(right, location.x + dims.width);
            }

                // and all text comments
            TextComment[] comments = _container_node.textComments_get();
            for (int i = 0; i < comments.length; i++) {
                Point location = comments[i].getLocation();
                EditorFrame frame = 
                         Alfi.getTheNodeCache().getEditSession(_container_node);
                if (frame != null) {
                    TextCommentWidget widget = 
                        frame.determineTextCommentWidget(comments[i]);
                    if (widget != null) {
                        Dimension dims = widget.getSize();
                        top = Math.min(top, location.y);
                        bottom = Math.max(bottom, location.y + dims.height);
                        left = Math.min(left, location.x);
                        right = Math.max(right, location.x + dims.width);
                    }
                }
            }

            ColoredBox[] boxes = _container_node.coloredBoxes_get();
            for (int i = 0; i < boxes.length; i++) {
                Point location = boxes[i].getLocation();
                Dimension dims = boxes[i].getSize();
                top = Math.min(top, location.y);
                bottom = Math.max(bottom, location.y + dims.height);
                left = Math.min(left, location.x);
                right = Math.max(right, location.x + dims.width);
            }

                // room for the port borders
            if (_b_insert_border_space) {
                top -= PortWidget.PORT_WIDTH - 1;
                bottom += PortWidget.PORT_WIDTH - 1;
                left -= PortWidget.PORT_WIDTH - 1;
                right += PortWidget.PORT_WIDTH - 1;
            }

                // go left and up to a gridpoint
            left -= GridTool.GRID_SPACING;
            top -= GridTool.GRID_SPACING;
            Point upper_left =
                            GridTool.getNearestGridPoint(new Point(left, top));

            if (_b_include_origin) {
                upper_left.x = Math.min(0, upper_left.x);
                upper_left.y = Math.min(0, upper_left.y);
            }

               // go right and down to a gridpoint
            right += GridTool.GRID_SPACING;
            bottom += GridTool.GRID_SPACING;
            Point lower_right =
                        GridTool.getNearestGridPoint(new Point(right, bottom));

            int width = lower_right.x - upper_left.x + 1;
            int height = lower_right.y - upper_left.y + 1;
            minimum_rect = new Rectangle(upper_left.x, upper_left.y,
                                         width, height);
        }
            // External view
        else {
            ALFINode node = _container_node;
            minimum_rect =
                new Rectangle(MemberNodeWidget.determineWidgetSize(node, true));
            Point offset = EditorFrame.EXTERNAL_VIEW_WIDGET_OFFSET;
            minimum_rect.width += offset.x * 2;
            minimum_rect.height += offset.y * 2;
        }

        return minimum_rect;
    }

        /** 
         * JGoViews seem to apply strange rules to scrollbar values whenever
         * items are added to the view's document.  This is an attempt to
         * override JGo's default behavior.
         */
    public void updateScrollbars() {
        super.updateScrollbars();

        if (m_frame != null) {
            Rectangle minimum_view_rect = determineMinimumRect();
            Dimension working_panel_size = m_frame.getWorkingPanel().getSize();

            JScrollBar h_scroll_bar = getHorizontalScrollBar();
            JScrollBar v_scroll_bar = getVerticalScrollBar();

            boolean b_h_scroll_bar_enabled = (minimum_view_rect.width >
                       (working_panel_size.width - v_scroll_bar.getWidth()) );
            boolean b_v_scroll_bar_enabled = (minimum_view_rect.height > 
                       (working_panel_size.height - h_scroll_bar.getHeight()) );

            h_scroll_bar.setEnabled(b_h_scroll_bar_enabled);
            v_scroll_bar.setEnabled(b_v_scroll_bar_enabled);

            if (b_h_scroll_bar_enabled) {
                h_scroll_bar.setMaximum(
                            minimum_view_rect.x + minimum_view_rect.width);
            }

            if (b_v_scroll_bar_enabled) {
                v_scroll_bar.setMaximum(
                            minimum_view_rect.y + minimum_view_rect.height);
            }
        }
    }

        /** Data flow direction can reverse on connections between two junctions
          * when the user deletes and adds other connections in a network of
          * junctions.  Valid only for connections between 2 junctions which is
          * also currently disconnected from an origin PortProxy.
          */
    public void reverseConnectionDirection(ConnectionWidget _cw) {
        ConnectionProxy connection = _cw.getAssociatedProxy();
        ALFINode container = getOwnerFrame().getNodeInEdit();
        GenericPortProxy original_source = connection.getSource();
        GenericPortProxy original_sink = connection.getSink();
        if ((connection.getOrigin(container) == null) &&
                                  (original_source instanceof JunctionProxy) &&
                                  (original_sink instanceof JunctionProxy)) {
            Vector pathPoints = _cw.copyPoints();
            Collections.reverse(pathPoints);
            Integer[][] new_path_definition_and_hint =
                        ConnectionProxy.pathPointsToPathDefinition(pathPoints);
            Integer[] new_path_definition = new_path_definition_and_hint[0];
            int dog_leg_hint = new_path_definition_and_hint[1][0].intValue();
            boolean b_is_bundle = (connection instanceof BundleProxy);

            container.connection_remove(connection, false);
            ConnectionProxy new_connection = container.connection_add(
                                       original_sink, null, original_source,
                                       null, new_path_definition, b_is_bundle);
            new_connection.setDogLegHint(dog_leg_hint, true);
        }
        else { Alfi.warn("Cannot reverse direction of " + connection); }
    }

        /** This method has been overridden to send widget location updates
          * through the bookkeeping first, which triggers location updates for
          * all associated widgets instead of just those in the current edit
          * window.  _flags and _event are ignored here.
          *
          * The following connections are automatically added to the selection:
          *   * Connections which have both their source and sink ports included
          *     in the selection.
          *   * Connections which have either a source or sink port included in
          *     the selection, and the other port is an external port.
          */
    public void moveSelection(JGoSelection _sel, int _flags,
                              int _offsetx, int _offsety, int _event) {
        HashMap selection_map = MiscTools.JGoSelectionToMap(_sel, true);

            // determine which connections should be part of the selection
        ConnectionWidget[] cws = m_frame.getConnectionWidgets();
        for (int i = 0; i < cws.length; i++) {
            boolean b_source_checks = false;
            boolean b_external_source_port = false;
            GenericPortWidget source_gpp = cws[i].getSourceWidget();
            if (selection_map.containsKey(source_gpp)) {
                b_source_checks = true;
            }
            else {
                MemberNodeWidget source_mnw = source_gpp.getOwnerMemberWidget();
                if (source_mnw != null) {
                    if (selection_map.containsKey(source_mnw)) {
                        b_source_checks = true;
                    }
                }
                else if (source_gpp instanceof PortWidget) {
                    b_external_source_port = true;
                }
            }

                // continue the check?
            if (b_source_checks || b_external_source_port) {
                GenericPortWidget sink_gpp = cws[i].getSinkWidget();
                if (selection_map.containsKey(sink_gpp)) {
                    selection_map.put(cws[i], null);
                }
                else {
                    MemberNodeWidget sink_mnw = sink_gpp.getOwnerMemberWidget();
                    if (sink_mnw != null) {
                        if (selection_map.containsKey(sink_mnw)) {
                            selection_map.put(cws[i], null);
                        }
                    }
                    else if (sink_gpp instanceof PortWidget) {
                        if (b_source_checks) { selection_map.put(cws[i], null);}
                    }
                }
            }
        }

        moveElements(selection_map, _offsetx, _offsety);
    }

        /** The guts of the moveSelection override. */
    void moveElements(HashMap _selection_map, int _dx_raw, int _dy_raw) {
        Point dP_raw = new Point(_dx_raw, _dy_raw);
        Point dP = GridTool.getNearestGridPoint(dP_raw);
        for (Iterator i = _selection_map.keySet().iterator(); i.hasNext();) {
            JGoObject widget = (JGoObject) i.next();
            if (widget instanceof MemberNodeWidget) {
                i.remove();

                MemberNodeProxy member =
                              ((MemberNodeWidget) widget).getAssociatedProxy();
                Point location = member.getLocation();
                location.translate(dP.x, dP.y);

                    // triggers event to update widget location!
                member.setLocation(location);
            }
            else if (widget instanceof JunctionWidget) {
                i.remove();

                JunctionProxy junction =
                                ((JunctionWidget) widget).getAssociatedProxy();
                Point location = junction.getLocation();
                location.translate(dP.x, dP.y);

                    // triggers event to update widget location!
                junction.setLocation(location);
            }
            else if (widget instanceof BundlerWidget) {
                i.remove();

                BundlerProxy bundler =
                                ((BundlerWidget) widget).getAssociatedProxy();
                Point location = bundler.getLocation();
                location.translate(dP.x, dP.y);

                    // triggers event to update widget location!
                bundler.setLocation(location);
            }
            else if (widget instanceof TextCommentWidget) {
                i.remove();

                TextComment comment_data =
                              ((TextCommentWidget) widget).getAssociatedProxy();
                Point location = comment_data.getLocation();
                location.translate(dP.x, dP.y);

                    // triggers event to update widget location!
                comment_data.setLocation(location);
            }
            else if (widget instanceof ColoredBoxWidget) {
                i.remove();

                ColoredBox bg_data =
                              ((ColoredBoxWidget) widget).getAssociatedProxy();
                Point location = bg_data.getLocation();
                location.translate(dP.x, dP.y);

                    // triggers event to update widget location!
                bg_data.setLocation(location);
            }
        }

            // now translate connections
        for (Iterator i = _selection_map.keySet().iterator(); i.hasNext(); ) {
            JGoObject widget = (JGoObject) i.next();
            if (widget instanceof ConnectionWidget) {
                ConnectionProxy connection =
                              ((ConnectionWidget) widget).getAssociatedProxy();
                connection.translate(dP.x, dP.y);
            }
        }
    }


    public void alignLeft() {
        align(ALIGN_LEFT, JGoObject.TopLeft);
   }

    public void alignRight() {
        align(ALIGN_RIGHT, JGoObject.TopRight);
    }

    public void alignTop() {
        align(ALIGN_TOP, JGoObject.Top);
    }

    public void alignBottom() {
        align(ALIGN_BOTTOM, JGoObject.Bottom);
    }

    public void alignVerticalCenter() {
        align(ALIGN_VERT_CENTER, JGoObject.Center);
    }

    public void alignHorizontalCenter() {
        align(ALIGN_HORIZ_CENTER, JGoObject.Center);
    }

    public void align( int _align_type, int _spot_location) {
        JGoSelection selection = getSelection();
        JGoObject widget = selection.getPrimarySelection();
        JGoListPosition pos = selection.getFirstObjectPos();
        while ( widget != null && 
                widget instanceof JGoLink && 
                pos != null){
            widget = selection.getObjectAtPos(pos);
            pos = selection.getNextObjectPos(pos);
        }

        if (widget == null) { return; }

        Point p = widget.getSpotLocation(_spot_location);

        boolean found_inherited_node = false;
        while (pos != null) {
            JGoObject temp = selection.getObjectAtPos(pos);
            pos = selection.getNextObjectPos(pos);

            if (temp instanceof MemberNodeWidget) {
                Point new_location;
                switch (_align_type) {
                    case ALIGN_LEFT:
                        new_location = new Point(p.x,temp.getTop());
                    break;

                    case ALIGN_RIGHT:
                        new_location = new Point(p.x,temp.getTop());
                    break;

                    case ALIGN_TOP:
                        new_location = new Point(temp.getLeft(), p.y);
                        _spot_location = JGoObject.TopLeft;
                    break;

                    case ALIGN_BOTTOM:
                        new_location = new Point(temp.getLeft(), p.y);
                        _spot_location = JGoObject.BottomLeft;
                    break;

                    case ALIGN_VERT_CENTER:
                        new_location = new Point(temp.getLeft(),p.y);
                        _spot_location = JGoObject.LeftCenter;
                    break;

                    case ALIGN_HORIZ_CENTER:
                        new_location = new Point(p.x,temp.getTop());
                        _spot_location = JGoObject.TopCenter;
                    break;
                    
                    default:
                        new_location = new Point(p.x, p.y);
                }
                MemberNodeWidget mnw = ( MemberNodeWidget ) temp;
                if (mnw.isInherited()) {
                    found_inherited_node = true;
                }
                else {
                    temp.setSpotLocation(_spot_location, new_location);
                    Point location = temp.getLocation();

                    MemberNodeProxy member =
                      ((MemberNodeWidget) temp).getAssociatedProxy();
                    
                        // triggers event to update widget location!
                    member.setLocation(location);
                }
            }
        }

        if (found_inherited_node) {
            Alfi.warn("At least one of the nodes is inherited.  " +
            "Inherited objects cannot be moved.", false); 

        }
    }



        /** Called by JGoViewListener in the constructor to signal selections.*/
    void onSelect() {
        if (m_frame.isInternalView()) {
            captureSelectionsConnectionPaths();
        }
    }

        /** This method looks at the latest member added to the selection, and
          * if it has connections attached to it, these connections capture
          * their current paths.
          */
    private void captureSelectionsConnectionPaths() {
        ArrayList selection_list =
                            MiscTools.JGoSelectionToList(getSelection(), true);
        if (selection_list.size() > 0) {
                // just look at the last object selected
            Object o = selection_list.get(selection_list.size() - 1);
            ArrayList gpw_list = new ArrayList();
            if (o instanceof MemberNodeWidget) {
                MemberNodeWidget widget = (MemberNodeWidget) o;
                PortWidget[] pws = ((MemberNodeWidget) o).getPortWidgets();
                gpw_list = new ArrayList(Arrays.asList(pws));
            }
            else if (o instanceof GenericPortWidget) {
                gpw_list.add(o);
            }

            HashMap cw_map = new HashMap();
            for (Iterator i = gpw_list.iterator(); i.hasNext(); ) {
                ConnectionWidget[] cws =
                               ((GenericPortWidget) i.next()).getConnections();
                for (int j = 0 ; j < cws.length; j++) {
                    cw_map.put(cws[j], null);
                }
            }

            for (Iterator i = cw_map.keySet().iterator(); i.hasNext(); ) {
                ((ConnectionWidget) i.next()).capturePathPoints();
            }
        }
    }

    /////////////////////////////////////////////
    // MouseListener Interface Methods //////////

    /**  Doesn't work.  Use doMouseClick() instead. */
    public void mouseClicked(MouseEvent e) { }

    public void mouseEntered(MouseEvent e) {
        m_frame.setStatusText(m_frame.getDefaultStatusText());
        m_frame.getView().requestFocus();
    }

    public void mouseExited(MouseEvent e) {
        m_frame.getView().transferFocus();
    }

    /**  Doesn't work.  Use doMouseClick() instead. */
    public void mousePressed(MouseEvent e) { }

    /**  Doesn't work.  Use doMouseClick() instead. */
    public void mouseReleased(MouseEvent e) { }
}
