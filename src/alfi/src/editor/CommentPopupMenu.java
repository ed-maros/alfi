package edu.caltech.ligo.alfi.editor;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.Box;

import edu.caltech.ligo.alfi.common.BasicPopupMenu;
import edu.caltech.ligo.alfi.common.Actions;
import edu.caltech.ligo.alfi.common.UIResourceBundle;

/**    
  * <pre>
  * The popup menu for comments  
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see BasicPopupMenu
  */
public class CommentPopupMenu extends BasicPopupMenu { 

    private JMenuItem m_grouped;

    public CommentPopupMenu (Actions _actions, UIResourceBundle _resources) {
        super(_actions);

        JMenu edit = createCascadeMenu(EditorActionsIF.EDIT);        
        createMenuItem(edit, EditorActionsIF.CUT);
        createMenuItem(edit, EditorActionsIF.COPY);
        createMenuItem(edit, EditorActionsIF.DUPLICATE);
        createMenuItem(edit, EditorActionsIF.PASTE);
        edit.addSeparator();
        createMenuItem(edit, EditorActionsIF.DELETE);
        edit.addSeparator();
        createMenuItem(edit, EditorActionsIF.GROUP);
        createMenuItem(edit, EditorActionsIF.UNGROUP);

        createMenu(EditorActionsIF.TEXT_VALUE);

        addSeparator();
        JMenu font_size = createCascadeMenu(EditorActionsIF.TEXT_FONT_SIZE);        
        createMenuItem(font_size, EditorActionsIF.TEXT_FONT_XSMALL);
        createMenuItem(font_size, EditorActionsIF.TEXT_FONT_SMALL);
        createMenuItem(font_size, EditorActionsIF.TEXT_FONT_MEDIUM);
        createMenuItem(font_size, EditorActionsIF.TEXT_FONT_LARGE);
        createMenuItem(font_size, EditorActionsIF.TEXT_FONT_XLARGE);
        createMenuItem(font_size, EditorActionsIF.TEXT_FONT_HEADING);

        addSeparator();
        JMenu text_color = createCascadeMenu(EditorActionsIF.TEXT_COLOR);        
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_BLACK);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_WHITE);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_BLUE);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_CYAN);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_GREEN);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_LIGHT_GRAY);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_MAGENTA);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_ORANGE);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_PINK);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_RED);
        createMenuItem(text_color, EditorActionsIF.TEXT_COLOR_YELLOW);
        
        addSeparator();
        createMenu(EditorActionsIF.TEXT_FONT_BOLD);
        createMenu(EditorActionsIF.TEXT_FONT_UNDERLINE);
        createMenu(EditorActionsIF.TEXT_FONT_ITALIC);
        
    }

}

