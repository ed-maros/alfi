package edu.caltech.ligo.alfi.editor;

import java.awt.*;

/**    
  * <pre>
  * The interface for the child frame actions
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  */
public interface EditorActionsIF {

    /**
      * The keys for the view actions
      */
    public static final String NEW_BUNDLER = "editoractionsif.new.bundler";
    public static final String NEW_TEXT_COMMENT = 
                                             "editoractionsif.new.text.comment";
    public static final String NEW_COLORED_BOX = 
                                              "editoractionsif.new.colored.box";
    public static final String EDIT_COMMENT = "editoractionsif.edit.comment";
    public static final String EDIT_MACROS = "editoractionsif.edit.macros";
    public static final String BOX_SETTINGS = "editoractionsif.box.settings";
    public static final String VIEW_EXTERNAL = "editoractionsif.external.view";
    public static final String VIEW_INTERNAL = "editoractionsif.interrnal.view";
    public static final String RESET_VIEW = "editoractionsif.reset.view";
    public static final String VALIDATE_BUNDLES =
                                            "editoractionsif.validate.bundles";
    public static final String SMOOTH_CONNS = "editoractionsif.smooth.conns";
    public static final String TRIM_CONNS = "editoractionsif.trim.conns";

    public static final String EDIT = "editoractionsif.edit";
    public static final String COPY = "editoractionsif.copy";
    public static final String CUT = "editoractionsif.cut";
    public static final String DUPLICATE = "editoractionsif.duplicate";
    public static final String PASTE = "editoractionsif.paste";
    public static final String DELETE = "editoractionsif.delete";
    public static final String SELECT_ALL= "editoractionsif.select_all";
    public static final String GROUP = "editoractionsif.group";
    public static final String UNGROUP = "editoractionsif.ungroup";
    public static final String VIEW_BASE_BOX = "editoractionsif.view.base.box";
    public static final String SHOW_MAIN = "editoractionsif.show.main";
    public static final String EXPORT_MODELER_FILE = 
                                         "editoractionsif.export.modeler.file";
    public static final String SAVE = "editoractionsif.save";
    public static final String SAVE_COPY_AS = "editoractionsif.save.copy.as";
    public static final String RELOAD = "editoractionsif.reload";
    public static final String CLOSE = "editoractionsif.close";
    public static final String PRINT_TO_SCALE = "editoractionsif.print.scale";
    public static final String PRINT_FIT_PAGE = "editoractionsif.print.fitpage";

    /**
      * The keys for the node actions
      */

    public static final String NODE_EDIT = "editoractionsif.node.edit";
    public static final String NODE_ADD = "editoractionsif.node.add";
    public static final String NODE_DELETE = "editoractionsif.node.delete";
    public static final String NODE_DUPLICATE= "editoractionsif.node.duplicate";
    public static final String NODE_RENAME = "editoractionsif.node.rename";
    public static final String NODE_SWAP = "editoractionsif.node.swap";
    public static final String DEBUG = "editoractionsif.debug";
    public static final String LOCATE_FILE = "editoractionsif.locate.file";
    public static final String NODE_INFO = "editoractionsif.node.info";

    public static final String ALIGN = "editoractionsif.align";
    public static final String ALIGN_LEFT = "editoractionsif.align.left";
    public static final String ALIGN_RIGHT = "editoractionsif.align.right";
    public static final String ALIGN_TOP = "editoractionsif.align.top";
    public static final String ALIGN_BOTTOM = "editoractionsif.align.bottom";
    public static final String ALIGN_HORIZONTAL_CENTER = 
                                      "editoractionsif.align horizontal.center";
    public static final String ALIGN_VERTICAL_CENTER = 
                                        "editoractionsif.align.vertical.center";

    public static final String SETTINGS = "editoractionsif.settings";
    public static final String CENTER_PORTS = "editoractionsif.center.ports";
    public static final String ROTATE = "editoractionsif.rotate";
    public static final String ROTATE_NORTH = "editoractionsif.rotate.north";
    public static final String ROTATE_SOUTH = "editoractionsif.rotate.south";
    public static final String ROTATE_EAST = "editoractionsif.rotate.east";
    public static final String ROTATE_WEST = "editoractionsif.rotate.west";
    public static final String SWAP = "editoractionsif.swap";
    public static final String SWAP_X = "editoractionsif.swap.x";
    public static final String SWAP_Y = "editoractionsif.swap.y";
    public static final String SWAP_NONE = "editoractionsif.swap.none";
    public static final String SYMMETRY = "editoractionsif.symmetry";
    public static final String SYMMETRY_X = "editoractionsif.symmetry.x";
    public static final String SYMMETRY_Y = "editoractionsif.symmetry.y";
    public static final String SYMMETRY_PLUS_XY= "editoractionsif.symmetry.+xy";
    public static final String SYMMETRY_MINUS_XY="editoractionsif.symmetry.-xy";
    public static final String SYMMETRY_NONE = "editoractionsif.symmetry.none";

    /**
      * The keys for the port actions
      */

    public static final String PORT_MODIFY = "editoractionsif.port.modify";
    public static final String PORT_DELETE = "editoractionsif.port.delete";
    public static final String PORT_RENAME = "editoractionsif.port.rename";
    public static final String IMPORT_CHANNELS =
                                             "editoractionsif.import.channels";

    /**
      * The keys for the connection actions
      */

    public static final String CONN_DELETE = 
                                  "editoractionsif.connection.delete";
    public static final String CONN_SMOOTH = 
                                  "editoractionsif.connection.smooth";
    public static final String CONN_CREATE_JUNCTION = 
                                  "editoractionsif.connection.create.junction";
    public static final String CONN_CREATE_BUNDLER = 
                                  "editoractionsif.connection.create.bundler";
    public static final String CONN_TRIM = "editoractionsif.connection.trim";
    public static final String SHOW_BUNDLE_CONTENT =
                                         "editoractionsif.show.bundle.content";

    /** The keys for the junction actions. */
    public static final String JUNCTION_DELETE =
                                  "editoractionsif.junction.delete";

    /** The keys for the bundler actions. */
    public static final String BUNDLER_DELETE =
                                  "editoractionsif.bundler.delete";
    public static final String BUNDLER_IO_RENAME =
                                  "editoractionsif.bundler.io.rename";
    public static final String BUNDLER_PRIMARY_IN_TO_SECONDARY =
                            "editoractionsif.bundler.primary.in.to.secondary";
    public static final String BUNDLER_PRIMARY_OUT_TO_SECONDARY =
                            "editoractionsif.bundler.primary.out.to.secondary";

    public static final String TEXT_FONT_SIZE = "editoractionsif.text.size";
    public static final String TEXT_FONT_XSMALL = "editoractionsif.text.xsmall";
    public static final String TEXT_FONT_SMALL = "editoractionsif.text.small";
    public static final String TEXT_FONT_MEDIUM = "editoractionsif.text.medium";
    public static final String TEXT_FONT_LARGE = "editoractionsif.text.large";
    public static final String TEXT_FONT_XLARGE = "editoractionsif.text.xlarge";
    public static final String TEXT_FONT_HEADING = "editoractionsif.text.heading";
    public static final String TEXT_VALUE = "editoractionsif.text.value";
    public static final String TEXT_FONT_STYLE = "editoractionsif.text.style";
    public static final String TEXT_FONT_BOLD = "editoractionsif.text.bold";
    public static final String TEXT_FONT_UNDERLINE = "editoractionsif.text.underline";
    public static final String TEXT_FONT_ITALIC = "editoractionsif.text.italic";

    public static final String TEXT_COLOR = "editoractionsif.text.color";
    public static final String TEXT_COLOR_BLACK = 
                                            "editoractionsif.font.color.black";
    public static final String TEXT_COLOR_WHITE = 
                                            "editoractionsif.font.color.white";
    public static final String TEXT_COLOR_BLUE = 
                                            "editoractionsif.font.color.blue";
    public static final String TEXT_COLOR_CYAN = 
                                            "editoractionsif.font.color.cyan";
    public static final String TEXT_COLOR_GREEN = 
                                           "editoractionsif.font.color.green";
    public static final String TEXT_COLOR_LIGHT_GRAY = 
                                       "editoractionsif.font.color.lightgray";
    public static final String TEXT_COLOR_MAGENTA = 
                                         "editoractionsif.font.color.magenta";
    public static final String TEXT_COLOR_ORANGE = 
                                          "editoractionsif.font.color.orange";
    public static final String TEXT_COLOR_PINK = 
                                            "editoractionsif.font.color.pink";
    public static final String TEXT_COLOR_RED = 
                                             "editoractionsif.font.color.red";
    public static final String TEXT_COLOR_YELLOW = 
                                          "editoractionsif.font.color.yellow";

        // Highlight related constants
    public static final String HILITE_LAYER_MODIFY = 
                                          "editoractionsif.hilite.layer.modify";
    public static final String HILITE_SEND_TO_BACK = 
                                          "editoractionsif.hilite.send.to.back";
    public static final String HILITE_SEND_TO_FRONT = 
                                          "editoractionsif.hilite.send.to.front";

    public static final String HILITE_COLOR_MODIFY = 
                                          "editoractionsif.hilite.color.modify";
    public static final String HILITE_COLOR_BLUE = 
                                            "editoractionsif.hilite.color.blue";
    public static final String HILITE_COLOR_CYAN = 
                                            "editoractionsif.hilite.color.cyan";
    public static final String HILITE_COLOR_GREEN = 
                                           "editoractionsif.hilite.color.green";
    public static final String HILITE_COLOR_LIGHT_GRAY = 
                                       "editoractionsif.hilite.color.lightgray";
    public static final String HILITE_COLOR_MAGENTA = 
                                         "editoractionsif.hilite.color.magenta";
    public static final String HILITE_COLOR_ORANGE = 
                                          "editoractionsif.hilite.color.orange";
    public static final String HILITE_COLOR_PINK = 
                                            "editoractionsif.hilite.color.pink";
    public static final String HILITE_COLOR_RED = 
                                             "editoractionsif.hilite.color.red";
    public static final String HILITE_COLOR_YELLOW = 
                                          "editoractionsif.hilite.color.yellow";
    public static final String HILITE_COLOR_DARK_BLUE = 
                                            "editoractionsif.hilite.color.darkblue";
    public static final String HILITE_COLOR_DARK_CYAN = 
                                            "editoractionsif.hilite.color.darkcyan";
    public static final String HILITE_COLOR_DARK_GREEN = 
                                           "editoractionsif.hilite.color.darkgreen";
    public static final String HILITE_COLOR_DARK_GRAY = 
                                       "editoractionsif.hilite.color.darkgray";
    public static final String HILITE_COLOR_DARK_MAGENTA = 
                                         "editoractionsif.hilite.color.darkmagenta";
    public static final String HILITE_COLOR_DARK_OLIVE_GREEN = 
                                         "editoractionsif.hilite.color.darkolivegreen";
    public static final String HILITE_COLOR_DARK_ORANGE = 
                                          "editoractionsif.hilite.color.darkorange";
    public static final String HILITE_COLOR_DARK_RED = 
                                             "editoractionsif.hilite.color.darkred";
    public static final String HILITE_COLOR_DARK_YELLOW = 
                                          "editoractionsif.hilite.color.darkyellow";

        /** the view commands associated with the actions */
    public void addBundler();
    public void addTextComment ();
    public void addColoredBox ();
    public void editComment ();
    public void editMacros ();
    public void viewExternal ();
    public void viewInternal ();
    public void resetView ();
    public void copy ();
    public void cut ();
    public void duplicate ();
    public void paste (boolean _b_use_default_location);
    public void viewBaseBox ();
    public void setCenterExternalViewPorts (boolean _b_centering_ports_on);
    public void save ();
    public void saveCopyAs ();
    public void reload ();
    public void close ();
    public void locateFile ();
    public void showNodeInfo ();
    public void group ();
    public void ungroup ();
    /**
      * the node commands associated with the actions
      */
    public void renameNode ();
    public void deleteMemberNode ();
    public void editSettings ();
    public void rotateNorth ();
    public void rotateSouth ();
    public void rotateEast ();
    public void rotateWest ();
    public void swapX ();
    public void swapY ();
    public void swapNone ();
    public void symmetryX ();
    public void symmetryY ();
    public void symmetryPlusXY ();
    public void symmetryMinusXY ();
    public void symmetryNone ();
    /**
      * the port commands associated with the actions
      */
    public boolean modifyPort ();
    public void deletePort ();
    public void renamePort ();
    public void importChannels ();

    /**
      * the connections commands associated with the actions
      */
    public void deleteConnection ();
    public void smoothConnections (boolean _allConnections);
    public void addJunction ();
    public void trimConnectionPath ();
    public void trimAllConnectionPaths ();
    public void showBundleContent ();

    /**
      * the junction commands associated with the actions
      */
    public void deleteJunction ();

    /**
      * the bundler commands associated with the actions
      */
    public void deleteBundler ();
    public void renameBundlerIO ();
    public void bundlerPrimaryToSecondary (boolean _b_is_input);
    public void validateBundles ();

    /**
      * the text comment commands associated with the actions
      */
    public void setTextSize (int _size);
    public void setTextValue ();
    public void setTextColor (int _color);
    public void setTextBold ();
    public void setTextItalic ();
    public void setTextUnderline ();

    /**
      * the colored box commands associated with the actions
      */
    public void setHighlightColor (int _color);
    public void switchHighlightLayer ();
    public void sendHighlightToBack ();
    public void sendHighlightToFront ();


}
