package edu.caltech.ligo.alfi.editor;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.Box;

import edu.caltech.ligo.alfi.common.BasicPopupMenu;
import edu.caltech.ligo.alfi.common.Actions;
import edu.caltech.ligo.alfi.common.UIResourceBundle;

/**    
  * <pre>
  * The command popup menu for junctions
  * </pre>
  *
  * @author Melody Araya
  * @version %I%, %G%
  * @see BasicPopupMenu
  */
public class JunctionPopupMenu extends BasicPopupMenu { 

    public JunctionPopupMenu (Actions _actions) {
        super(_actions);

        createMenu(EditorActionsIF.JUNCTION_DELETE);
    }
}

