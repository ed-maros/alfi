package edu.caltech.ligo.alfi.file;

import java.io.*;
import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;


    /** This object is used to load all primitive  templates in the E2E_PATH. */
public class PrimitiveTemplateLoader {

    private final static String PRIMITIVE_TEMPLATE_FILE_SUFFIX = ".ptm";

        /** Constructor.  Searches the search path for primitive teamplate files
          * and creates a PrimitiveTemplate for each.
          */
    public PrimitiveTemplateLoader(String _search_path) {
        //System.err.println("PrimitiveLoader(" + _search_path + "):");
        StringTokenizer st =
                          new StringTokenizer(_search_path, File.pathSeparator);

        HashMap file_vs_templateName_map = new HashMap();
        while (st.hasMoreTokens()) {
            File directory = new File(st.nextToken());
            File[] template_files = directory.listFiles(
                new FilenameFilter() {                        // anon class def
                    public boolean accept(File f, String s) {
                        return s.endsWith(PRIMITIVE_TEMPLATE_FILE_SUFFIX);
                    }
                } );
            if (template_files == null) { continue; }

            for (int i = 0; i < template_files.length; i++) {
                String name = template_files[i].getName();
                if (! file_vs_templateName_map.containsKey(name)) {
                    file_vs_templateName_map.put(name, template_files[i]);
                }
            }
        }

        Iterator i = file_vs_templateName_map.values().iterator();
        while (i.hasNext()) {
            File file = (File) i.next();
            String path = null;
            try {
                path = file.getCanonicalPath();
                PrimitiveTemplate pt = new PrimitiveTemplate(file);
            }
            catch(IOException e) { System.err.println(e); }
        }
    }
}
