package edu.caltech.ligo.alfi.file;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.tools.AlfiException;

    /**
     * This exception occurs when the parser detects an attempted cyclic
     * include.  E.g., X.box includes Y.box and Y.box includes X.box.
     */
public class CyclicAlfiFileIncludeException extends AlfiException {
    public CyclicAlfiFileIncludeException(String _root_box_path,
                                          String _include_string) {
        super(
            "In box [" + _root_box_path + "], the line [" + _include_string +
            "] specifies the inclusion of a box which either directly or " +
            "indirectly includes [" + _root_box_path + "] itself.  This will " +
            "create an endless loop of box inclusions.", ALFI_ERROR);
    }
}	
