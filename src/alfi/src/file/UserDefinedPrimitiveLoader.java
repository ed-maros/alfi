package edu.caltech.ligo.alfi.file;

import java.io.*;
import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;


    /** This object is used to load all UDPs in the E2E_PATH. */
public class UserDefinedPrimitiveLoader {

        /** Constructor.  Searches the search path for UDP files
          * and creates a UserDefinedPrimitive for each.
          */
    public UserDefinedPrimitiveLoader(String _search_path) {
        //System.err.println("UserDefinedPrimitiveLoader(" + _search_path + "):");
        StringTokenizer st =
                          new StringTokenizer(_search_path, File.pathSeparator);

        HashMap file_vs_UDPName_map = new HashMap();
        while (st.hasMoreTokens()) {
            File directory = new File(st.nextToken());
            File[] udp_files = directory.listFiles(
                new FilenameFilter() {                        // anon class def
                    public boolean accept(File f, String s) {
                        return s.endsWith(ALFIFile.UDP_FILE_SYNTAX);
                    }
                } );
            if (udp_files == null) { continue; }

            for (int i = 0; i < udp_files.length; i++) {
                String name = udp_files[i].getName();
                if (! file_vs_UDPName_map.containsKey(name)) {
                    file_vs_UDPName_map.put(name, template_files[i]);
                }
            }
        }

        Iterator i = file_vs_udpName_map.values().iterator();
        while (i.hasNext()) {
            File file = (File) i.next();
            String path = null;
            try {
                path = file.getCanonicalPath();
                PrimitiveTemplate pt = new PrimitiveTemplate(file);
            }
            catch(IOException e) { System.err.println(e); }
        }
    }
}
