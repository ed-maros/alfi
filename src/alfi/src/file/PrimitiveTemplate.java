package edu.caltech.ligo.alfi.file;

import java.io.*;
import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;


    /** This object is used to create variable I/O primitives. 
      * The file _template_file is a text file which appears very much like a
      * standard primitive file except that some or all of the I/O ports may be 
      * contained in InputSet { } and/or OutputSet { } sections.  These sections
      * are ordered in the same order as they appear in the file, and the file
      * name generally gives clues to the number of input and output sets-of-
      * ports that can be expected.  For example, the file named
      * VecSum(N1,N2xM1) might look like this:
      *
      *     % Vout = r0*V0in + r1*V1in + r2*V2in + . . .
      *     %*InputSet {
      *     %*  Port input r
      *     %*  {
      *     %*    dataType = real
      *     %*    DefaultValue = 1
      *     %*  }
      *     %*  Port input Vin
      *     %*  {
      *     %*    dataType = vector_real
      *     %*  }
      *     %*}
      *     %*InputSet {
      *     %*  Port input rr
      *     %*  {
      *     %*    dataType = real
      *     %*    DefaultValue = 1
      *     %*  }
      *     %*}
      *     %*OutputSet {
      *     %*  Port output Vout
      *     %*  {
      *     %*    dataType = vector_real
      *     %*  }
      *     %*}
      *     %*Port output Vx
      *     %*{
      *     %*  dataType = vector_real
      *     %*}
      *     %*GUI_Settings
      *     %*{
      *     %*  Group 'Vector'
      *     %*}
      *
      * The 0th input set of ports contains ports r and Vin, the 1st input
      * set of ports contains port rr, and the 0th output set of ports
      * contains the port Vout.  Port Vx is a port not affiliated with the I/O
      * sets of ports.
      *
      * An example of a node created out of this template would be a node of
      * TYPE VecSum__5_3_x_2 (and named whatever.)  I.e., in the box file, one
      * of the Add_Submodules lines would be:
      *
      *     VecSum__5_3_x_2 myVecSumNode
      *
      * The modeler understands that this VecSum type has 2 input sets and 1
      * output set of ports (along with any others, and the "5_3_x_2" tells it
      * that the ports it should expect are
      *
      * input: r0, Vin0, r1, Vin1, r2, Vin2, r3, Vin3, r4, Vin4, rr0, rr1, rr2
      * output: Vout0, Vout1, Vx
      *
      * I.e., 5 sets of { r, Vin }, 3 sets of { rr }, 2 sets of  { Vout },
      * plus the stand alone (not part of any I/O set) port, Vx.
      */
public class PrimitiveTemplate {

    private String m_base_name;
    private String m_input_set_iteration_string;
    private String m_output_set_iteration_string;
    private String m_comment;
    private PortProxy[][] m_input_sets;
    private PortProxy[][] m_output_sets;
    private PortProxy[] m_other_ports;
    private ParameterDeclaration[] m_parameter_decs;
    private String m_primitive_group;

        /** Initializes and registers itself in the AlfiNodeCache. */
    public PrimitiveTemplate(File _template_file) {
        String[] lines = MiscTools.readSmallTextFile(_template_file);

        String file_name = _template_file.getName();
        m_base_name = file_name.replaceAll("\\.ptm$", "");

        parseTemplateFileContents(lines);

        Alfi.getTheNodeCache().addPrimitiveTemplate(this);
    }

        /** The template's base name, e.g., VecSum (from VecSum.ptm). */
    public String getBaseName() { return new String(m_base_name); }

        /** The base name of a root base ALFINode modeled on this template and
          * a specific number of I/O sets, e.g., VecSum__3_5_x_2.
          */
    public String generateNameForBaseNode(int[] _io_set_counts) {
        String base_name = getBaseName() + "__";
        int input_set_count = getInputSetPorts().length;
        int output_set_count = getOutputSetPorts().length;
        int total_set_count = input_set_count + output_set_count;
        for (int i = 0; i < input_set_count; i++) {
            base_name += _io_set_counts[i];
            if (i < (input_set_count - 1)) { base_name += "_"; }
        }
        if (output_set_count > 0) {
            if (input_set_count > 0) { base_name += "_"; }
            base_name += "x_";
            for (int i = input_set_count; i < total_set_count; i++) {
                base_name += _io_set_counts[i];
                if (i < (total_set_count - 1)) { base_name += "_"; }
            }
        }

        return base_name;
    }

    public String getGroup() { return new String(m_primitive_group); }

    public PortProxy[][] getInputSetPorts() { return m_input_sets; }

    public PortProxy[][] getOutputSetPorts() { return m_output_sets; }

    public PortProxy[] getOtherPorts() { return m_other_ports; }

    public ParameterDeclaration[] getParameterDecs() { return m_parameter_decs;}

    private void parseTemplateFileContents(String[] _lines) {
        final String SECTION_BEGIN = Parser.SECTION_BEGIN_MARKER;
        final String SECTION_END = Parser.SECTION_END_MARKER;

            // prepare the lines array, especially making sure open and close
            // brackets are on their own lines.
        ArrayList line_list = new ArrayList();
        for (int i = 0; i < _lines.length; i++) {
            String line = _lines[i].replaceFirst("%\\*", "").trim();
            line = line.replaceAll("\\{", "____{____");
            line = line.replaceAll("\\}", "____}____");
            line = line.replaceAll("^____", "");
            line = line.replaceAll("____$", "");
            if (line.indexOf("____") == -1) {
                line_list.add(line);
            }
            else {
                String[] parts = line.split("____");
                for (int j = 0; j < parts.length; j++) {
                    line_list.add(parts[j].trim());
                }
            }
        }

        String[] lines = new String[line_list.size()];
        line_list.toArray(lines);

        m_comment = determineComment(lines);
        m_input_sets = determineIOSets(lines, "InputSet");
        m_output_sets = determineIOSets(lines, "OutputSet");
        m_other_ports = determineOtherPorts(lines);
        m_primitive_group = determineGroup(lines);
        m_parameter_decs = determineParameterDecs(lines);
    }

        /** Expects a string like n_m_x_k, where the number of numbers left of
          * the "x" must match the number of input sets in this template, and
          * the number of them to the right is the same as the number of output
          * sets in this template.  I.e., for 4_5_2_x_6, there must be 3 input
          * sets and 1 output set in this template.  If these do not match, null
          * is returned.  Strings like 4_7 and x_2 are also posibly valid, the
          * former indicating 2 input sets and no output sets, and the later
          * having no input sets and 1 output set.
          */
    int[] parseSetCountsString(String _set_counts_desc) {
        int[] io_set_counts = null;

        if (_set_counts_desc.matches("^[\\d_x]+$")) {
            ArrayList input_set_counts_list = new ArrayList();
            ArrayList output_set_counts_list = new ArrayList();
            boolean b_processing_input_sets = true;
            String[] parts = _set_counts_desc.split("_");
            try {
                for (int i = 0; i < parts.length; i++) {
                    if (parts[i].equals("x")) {
                        b_processing_input_sets = false;
                    }
                    else {
                        Integer count = Integer.valueOf(parts[i]);
                        if (b_processing_input_sets) {
                            input_set_counts_list.add(count);
                        }
                        else { output_set_counts_list.add(count); } 
                    }
                }

                    // check if the number of input and output sets in this
                    // template agree with the information in _set_counts_desc
                if ((input_set_counts_list.size() == m_input_sets.length) &&
                        (output_set_counts_list.size()==m_output_sets.length)) {
                    io_set_counts =
                           new int[m_input_sets.length + m_output_sets.length];
                    ArrayList list = input_set_counts_list;
                    for (int i = 0; i < list.size(); i++) {
                        io_set_counts[i] = ((Integer) list.get(i)).intValue();
                    }
                    list = output_set_counts_list;
                    for (int i = 0; i < list.size(); i++) {
                        io_set_counts[m_input_sets.length + i] =
                                           ((Integer) list.get(i)).intValue();
                    }
                }
            }
            catch(NumberFormatException _e) { ; }  // just exit loop
        }

        if (io_set_counts == null) {
            Alfi.warn("PrimitiveTemplate.parseSetCountsString(" +
                                                    _set_counts_desc + "):\n" +
                      "\tWARNING: Invalid string passed.");
        }

        return io_set_counts;
    }

    private String determineGroup(String[] _lines) {
        String group_name = null;
        String GROUP_MARKER = "Group";
        int depth = 0;
        boolean b_inside_GUI_Settings = false;
        for (int i = 0; i < _lines.length; i++) {
            if (_lines[i].equals("{")) {
                depth++;
            }
            else if (_lines[i].equals("}")) {
                depth--;
            }
            else if ((depth == 0) && _lines[i].startsWith("GUI_Settings")) {
                b_inside_GUI_Settings = true;
            }
            else if ((depth == 1) && b_inside_GUI_Settings &&
                                          _lines[i].startsWith(GROUP_MARKER)) {
                group_name = _lines[i].substring(GROUP_MARKER.length());
                group_name = group_name.replace('\'', ' ').trim();
                group_name = group_name.replace('\"', ' ').trim();
                break;
            }
        }
        return group_name;
    }

    private PortProxy[] determineOtherPorts(String[] _lines) {
        ArrayList port_list = new ArrayList();
        int depth = 0;
        for (int i = 0; i < _lines.length; i++) {
            if (_lines[i].equals("{")) {
                depth++;
            }
            else if (_lines[i].equals("}")) {
                depth--;
            }
            else if ((depth == 0) &&
                               _lines[i].startsWith(Parser.PORT_ADD_MARKER)) {
                ArrayList port_line_list = new ArrayList();
                while (! _lines[i].equals("}")) {
                    port_line_list.add(_lines[i++]);
                }
                port_line_list.add(_lines[i]);
                PortProxy port = parsePort(port_line_list);
                port_list.add(port);
            }
        }

        PortProxy[] ports = new PortProxy[port_list.size()];
        port_list.toArray(ports);
        return ports;
    }

    private ParameterDeclaration[] determineParameterDecs(String[] _lines) {
        ArrayList declaration_list = new ArrayList();
        for (int i = 0; i < _lines.length; i++) {
            String[] parts = _lines[i].split("\\s+");
            if (ParameterDeclaration.typeIsValid(parts[0])) {
                String[] dec_info = ParameterDeclaration.parse(_lines[i]);
                if (dec_info != null) {
                    ParameterDeclaration dec = new ParameterDeclaration(
                                        dec_info[0], dec_info[1], dec_info[2]);
                    declaration_list.add(dec);
                }
            }
        }

        ParameterDeclaration[] decs =
                             new ParameterDeclaration[declaration_list.size()];
        declaration_list.toArray(decs);
        return decs;
    }

    private PortProxy[][] determineIOSets(String[] _lines, String _set_desc) {
        ArrayList set_list = new ArrayList();
        for (int i = 0; i < _lines.length; i++) {
            if (_lines[i].startsWith(_set_desc)) {
                ArrayList port_list = new ArrayList();
                for ( ; i < _lines.length; i++) {
                    if (_lines[i].startsWith(Parser.PORT_ADD_MARKER)) {
                        ArrayList port_line_list = new ArrayList();
                        while (! _lines[i].equals("}")) {
                            port_line_list.add(_lines[i++]);
                        }
                        port_line_list.add(_lines[i]);
                        PortProxy port = parsePort(port_line_list);
                        port_list.add(port);
                    }
                    else if (_lines[i].equals("}")) { break; }
                }
                PortProxy[] ports = new PortProxy[port_list.size()];
                port_list.toArray(ports);
                set_list.add(ports);
            }
        }

        PortProxy[][] sets = new PortProxy[set_list.size()][];
        set_list.toArray(sets);
        return sets;
    }

    private PortProxy parsePort(ArrayList _port_line_list) {
        String[] lines = new String[_port_line_list.size()];
        _port_line_list.toArray(lines);

        int data_type_id = PortProxy.DATA_TYPE_INVALID;
        String default_value = null;

        String[] parts = lines[0].split("\\s+");
        int io_type_id = PortProxy.getIOTypeIdFromString(parts[1]);
        String name = parts[2];

        for (int i = 2; i < lines.length - 1; i++) {
            if (lines[i].startsWith("dataType")) {
                parts = lines[i].split("=");
                String data_type = parts[1].trim();
                data_type_id = PortProxy.getDataTypeIdFromString(data_type);
            }
            else if (lines[i].startsWith("DefaultValue")) {
                parts = lines[i].split("=");
                default_value = parts[1].trim();
            }
        }

        PortProxy port = new PortProxy(name, new Integer(io_type_id),
                                     new Integer(data_type_id), default_value);
        return port;
    }

    private String determineComment(String[] _lines) {
        boolean b_in_comment = false;
        boolean b_comment_complete = false;
        String comment = null;
        for (int i = 0; i < _lines.length; i++) {
            String line = _lines[i];

            if (! line.startsWith("%")) {
                if (comment != null) { break; }
            }
            else if (comment == null) {
                comment = line.replaceFirst("%", "").trim();
            }
            else {
                comment += "\n" + line.replaceFirst("%", "").trim();
            }
        }
        return comment;
    }

    public String toString() {
        String input_set_ports = "";
        for (int i = 0; i < m_input_sets.length; i++) {
            input_set_ports += "        {";
            for (int j = 0; j < m_input_sets[i].length; j++) {
                input_set_ports += " " + m_input_sets[i][j] + " ";
            }
            input_set_ports += "}\n";
        }
        String output_set_ports = "";
        for (int i = 0; i < m_output_sets.length; i++) {
            output_set_ports += "        {";
            for (int j = 0; j < m_output_sets[i].length; j++) {
                output_set_ports += " " + m_output_sets[i][j] + " ";
            }
            output_set_ports += "}\n";
        }
        String other_ports = "";
        for (int i = 0; i < m_other_ports.length; i++) {
            other_ports += " " + m_other_ports[i] + " ";
        }
        String decs = "";
        for (int i = 0; i < m_parameter_decs.length; i++) {
            decs += " " + m_parameter_decs[i].getName() + " ";
        }

        String s = "Primitive Template <" + m_base_name + ">\n" +
            "    m_primitive_group = " + m_primitive_group + "\n" +
            "    m_comment = " + m_comment + "\n" +
            "    m_input_set_ports = {\n" + input_set_ports + "    }\n" +
            "    m_output_set_ports = {\n" + output_set_ports + "    }\n" +
            "    m_other_ports = {" + other_ports + "}\n" +
            "    m_parameter_decs = { " + decs + "}\n";
        return s;
    }
}
