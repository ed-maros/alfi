package edu.caltech.ligo.alfi.file;

import java.io.*;

    /**
     * Some simple file I/O primitives reimplemented in java.  All methods
     * are static.  There is no state kept herein.
     *
     * Taken from O'Reilly Java Cookbook.
     */
public class FileIO {
    public static void copyFile(String _in_name, String _out_name)
                                  throws IOException, FileNotFoundException {
        BufferedInputStream in_stream =
                new BufferedInputStream(new FileInputStream(_in_name));
        BufferedOutputStream out_stream =
                new BufferedOutputStream(new FileOutputStream(_out_name));
        copyFile(in_stream, out_stream, true);
    }

    public static void copyFile(InputStream _in_stream,
           OutputStream _out_stream, boolean _b_close_file) throws IOException {
        int b;
        while ((b = _in_stream.read()) != -1) {
            _out_stream.write(b);
        }
        _in_stream.close();
        if (_b_close_file) { _out_stream.close(); }
    }

    public static void copyFile(Reader _reader, Writer _writer,
                                     boolean _b_close_file) throws IOException {
        int b;
        while ((b = _reader.read()) != -1) {
            _writer.write(b);
        }
        _reader.close();
        if (_b_close_file) { _writer.close(); }
    }

    public static void copyFile(String _in_name, PrintWriter _printer,
                                     boolean _b_close_file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(_in_name));
        copyFile(reader, _printer, _b_close_file);
    }

    public static String readLine(String _in_name)
                                  throws IOException, FileNotFoundException {
        BufferedReader reader = new BufferedReader(new FileReader(_in_name));
        String line = reader.readLine();
        reader.close();
        return line;
    }

    protected static final int BLOCK_SIZE = 8192;

    public static String readerToString(Reader _reader) throws IOException {
        StringBuffer sb = new StringBuffer();
        char[] buffer = new char[BLOCK_SIZE];

        int n;
        while ((n = _reader.read(buffer)) > 0) {
            sb.append(buffer, 0, n);
        }

        return sb.toString();
    }

    public static String inputStreamToString(InputStream _in_stream)
                                                           throws IOException {
        return readerToString(new InputStreamReader(_in_stream));
    }
}
