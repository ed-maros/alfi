package edu.caltech.ligo.alfi.file;

import java.io.*;
import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.dialogs.*;
import edu.caltech.ligo.alfi.tools.*;


    /** This object is used to load all primitives in the E2E_PATH. */
public class PrimitiveLoader {

        /** Constructor.  Searches the search path for primitive files and
          * creates an ALFIFile for each.
          */
    public PrimitiveLoader(String _search_path, FileFormatProblemManager _ffpm){
        this(_search_path, null, _ffpm);
    }

        /** Constructor.  Searches the search path for primitive files and
          * creates an ALFIFile for each.  _names_to_ignore is a list of names
          * of primitives to ignore the loading of (most likely because they've
          * already been loaded.)
          */
    public PrimitiveLoader(String _search_path, String[] _names_to_ignore,
                                              FileFormatProblemManager _ffpm) {
        //System.err.println("PrimitiveLoader(" + _search_path + "):");
        if (_names_to_ignore == null) { _names_to_ignore = new String[0]; }

        StringTokenizer st =
                          new StringTokenizer(_search_path, File.pathSeparator);

        HashMap file_vs_primitiveName_map = new HashMap();
        while (st.hasMoreTokens()) {
            File directory = new File(st.nextToken());
            File[] primitive_files = directory.listFiles(
                new FilenameFilter() {                        // anon class def
                    public boolean accept(File f, String s) {
                        return s.endsWith(ALFIFile.PRIMITIVE_NODE_FILE_SUFFIX);
                    }
                } );
            if (primitive_files == null) { continue; }

            for (int i = 0; i < primitive_files.length; i++) {
                String name = primitive_files[i].getName();
                boolean b_ignore_name = false;
                for (int j = 0; j < _names_to_ignore.length; j++) {
                    if (name.equals(_names_to_ignore[j])) {
                        b_ignore_name = true;
                        break;
                    }
                }
                if ((! b_ignore_name) &&
                             (! file_vs_primitiveName_map.containsKey(name))) {
                    file_vs_primitiveName_map.put(name, primitive_files[i]);
                }
            }
        }

        Iterator i = file_vs_primitiveName_map.values().iterator();
        while (i.hasNext()) {
            File file = (File) i.next();
            String path = null;
            try {
                path = file.getCanonicalPath();
                new ALFIFile(path, _ffpm);
            }
            catch(AlfiIncludeFileNotFoundException e) {
                String warning = "AlfiIncludeFileNotFoundException thrown " +
                    "while loading primitive " + file + " .  This should " +
                    "never occur while loading a primitive!";
                warning = TextFormatter.formatMessage(warning);
                warning += "\n\nFile not loaded.";
                Alfi.warn(warning);
            }
            catch(CyclicAlfiFileIncludeException e) {
                String warning = e.getMessage() + "\n\n" +
                    "This error should never occur while loading a primitive.";
                Alfi.warn(warning);
            }
            catch(FileLoadingCanceledException _e) {
                ALFINodeCache node_cache = Alfi.getTheNodeCache();
                String file_loading = _e.getFileLoading();
                ALFINode possibly_corrupt_node =
                                          node_cache.getRootNode(file_loading);
                if (possibly_corrupt_node != null) {
                    node_cache.removeNode(possibly_corrupt_node);
                }

                    // deal with file that set cancel flag (possibly different)
                String flagged_file_path = _e.getFlaggingFile();
                if ((flagged_file_path != null) &&
                                  (! flagged_file_path.equals(file_loading))) {
                    ALFINode another_possibly_corrupt_node =
                                     node_cache.getRootNode(flagged_file_path);
                    if (another_possibly_corrupt_node != null) {
                        node_cache.removeNode(another_possibly_corrupt_node);
                    }
                }
                break;
            }
            catch(IOException e) { System.err.println(e); }
        }
    }
}
