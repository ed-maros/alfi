package edu.caltech.ligo.alfi.file;

import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.tools.*;

    /** This exception occurs when the user cancels file loading. */
public class FileLoadingCanceledException extends AlfiException {
    private String m_file_loading_at_cancellation;
        // this is the file that was loading which set the cancel flag.  it may
        // be different (just previous) than the currently loading file.
    private String m_flagging_file_path;

    public FileLoadingCanceledException(String _file_loading,
                                                  String _flagging_file_path) {
        super("User terminated loading of files.",AlfiException.NON_ALFI_ERROR);
        m_file_loading_at_cancellation = _file_loading;
        m_flagging_file_path = _flagging_file_path;
    }

    public String getFileLoading() { return m_file_loading_at_cancellation; }

    public String getFlaggingFile() { return m_flagging_file_path; }
}	
