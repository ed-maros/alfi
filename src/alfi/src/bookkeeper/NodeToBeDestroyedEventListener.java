package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /** The interface for objects who wish to register as listeners for
      * NodeToBeDestroyedEvents.
      */
public interface NodeToBeDestroyedEventListener extends EventListener {
    public void nodeToBeDestroyed(NodeToBeDestroyedEvent _event);
}
