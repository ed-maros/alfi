package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;


    /**
     * The interface for objects who wish to register as listeners for
     * PortOrderChangeEvents.
     */
public interface PortOrderChangeEventListener extends EventListener {
    public void portOrderChange(PortOrderChangeEvent e);
}
