package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.*;
import java.util.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.*;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.file.*;


    /**
     * Instances of boxes may define macros.  The scope of such macros is
     * throughout all contained objects and similarly in all derived nodes
     * (unless redefined therein.)  These are very lightweight objects.
     */
public class Macro {

    ////////////////////////////////////////////////////////////////////////////
    ////////// Constants ///////////////////////////////////////////////////////

    private static String PARSER_COMMENT_PREFIX = "//";
    private static String PARSER_COMMENT_START_STRING =
             PARSER_COMMENT_PREFIX + "_COMMENT-START_" + PARSER_COMMENT_PREFIX;
    private static String PARSER_COMMENT_STOP_STRING =
              PARSER_COMMENT_PREFIX + "_COMMENT-STOP_" + PARSER_COMMENT_PREFIX;

    //**************************************************************************
    //***** member fields ******************************************************

        /**
         * This is the node in which this JunctionModification was locally
         * added.
         */
    private final ALFINode m_direct_container_node;

        /**
         * The value of the macro.
         */
    private final String m_value;

        /**
         * An optional comment associated with this macro.
         */
    private final String m_comment;

        /** Saves state of whether macro was quoted in the file. */
    private final boolean mb_quoted;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor.  Only ALFINode should ever construct Macros.  A node
          * may now only contain a single Macro object.
          */
    Macro(ALFINode _container_node, String _value, boolean _b_quoted) {
        this(_container_node, _value, null, _b_quoted);
    }

        /** Constructor.  Only ALFINode should ever construct Macros.  A node
          * may now only contain a single Macro object.
          */
    Macro(ALFINode _container_node, String _value, String _comment,
                                                           boolean _b_quoted) {
            // final member initializations
        m_direct_container_node = _container_node;
        m_value = _value;
        m_comment = ((_comment == null) || (_comment.trim().length() == 0)) ?
                                                               null : _comment;
        mb_quoted = _b_quoted;
    }

    //**************************************************************************
    //***** static methods *****************************************************

        /** Expects a parser representation of a Macro formatted as follows (the
          * initial comment lines are optional):
          *
          * //_COMMENT-START_//
          * //this is
          * //an optional comment...
          * //_COMMENT-STOP_//
          * x = 3
          * y = 4
          * z = 5
          * and whatever else is needed in the macro value.  the macro value
          * itself is not parsed by ALFI.  This is all just a string value to
          * ALFI.
          */
    public static String[] parse(String _parser_representation_string) {
        String[] lines = _parser_representation_string.split(Parser.NL);

        String comment = null;
        StringBuffer comment_sb = new StringBuffer();
        int i = 0;
        if (lines[0].equals(PARSER_COMMENT_START_STRING)) {
            int comment_prefix_length = PARSER_COMMENT_PREFIX.length();

            for (i = 1; i < lines.length; i++) {
                if (lines[i].equals(PARSER_COMMENT_STOP_STRING)) {
                    i++;
                    break;
                }
                else if (lines[i].startsWith(PARSER_COMMENT_PREFIX)) {
                    String line = lines[i].substring(comment_prefix_length);
                    comment_sb.append(line);
                    comment_sb.append(Parser.NL);
                }
            }

            if (comment_sb.length() > 0) {
                comment_sb = comment_sb.deleteCharAt(comment_sb.length() - 1);
                if (comment_sb.length() > 0) {
                    comment = comment_sb.toString();
                }
            }
        }

        String value = null;
        StringBuffer value_sb = new StringBuffer();
        for ( ; i < lines.length; i++) {
            value_sb.append(lines[i]);
            value_sb.append(Parser.NL);
        }
        value = value_sb.toString();

        String[] data = { value, comment };
        return data;
    }

    //**************************************************************************
    //***** methods ************************************************************

        /** Generate a description of the connection. */
    public String toString() {
        return (m_value + (mb_quoted ? " (QUOTED)" : ""));
    }

        /** Generate string for writing macro into the file. */
    public String generateParserRepresentation(String _indentation) {
        StringBuffer sb = new StringBuffer();
        if (m_comment != null) {
            sb.append(PARSER_COMMENT_START_STRING);
            sb.append(Parser.NL);
            String[] comment_lines = m_comment.split(Parser.NL);
            for (int i = 0; i < comment_lines.length; i++) {
                sb.append(PARSER_COMMENT_PREFIX);
                sb.append(comment_lines[i]);
                sb.append(Parser.NL);
            }
            sb.append(PARSER_COMMENT_STOP_STRING);
            sb.append(Parser.NL);
        }

        sb.append(m_value);
        return sb.toString();
    }

    /////////////////////////////////
    // member field access //////////

        /**
         * @returns m_direct_container_node.
         */
    ALFINode getDirectContainerNode() {
        return m_direct_container_node;
    }

    public String getValue() {
        return (m_value != null) ? new String(m_value) : null;
    }

    public String getComment() {
        return (m_comment != null) ? new String(m_comment) : null;
    }

    public boolean isQuoted() { return mb_quoted; }
}
