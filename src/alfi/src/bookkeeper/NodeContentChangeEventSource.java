package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.bookkeeper.*;

/**
  * This interface defines fields and methods used by sources of 
  * NodeContentChangeEvent sources.
  */
public interface NodeContentChangeEventSource {
    public void addNodeContentChangeListener(
                        NodeContentChangeEventListener _listener);
    public void removeNodeContentChangeListener(
                        NodeContentChangeEventListener _listener);
}

