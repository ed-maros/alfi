package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.bookkeeper.*;

/**
  * This interface defines fields and methods used by sources of 
  * PortOrderChangeEvent sources.
  */
public interface PortOrderChangeEventSource {
    public void addPortOrderChangeListener(
                        PortOrderChangeEventListener _listener);
    public void removePortOrderChangeListener(
                        PortOrderChangeEventListener _listener);
}

