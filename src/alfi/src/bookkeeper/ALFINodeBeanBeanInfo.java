package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.Color;
import java.beans.BeanInfo;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Arrays;
import java.util.ListResourceBundle;

import com.l2fprod.common.beans.BaseBeanInfo;
import com.l2fprod.common.beans.editor.ComboBoxPropertyEditor;
import com.l2fprod.common.model.DefaultBeanInfoResolver;
import com.l2fprod.common.propertysheet.Property;
import com.l2fprod.common.propertysheet.PropertySheet;
import com.l2fprod.common.propertysheet.PropertySheetPanel;
import com.l2fprod.common.swing.LookAndFeelTweaks;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;

public class ALFINodeBeanBeanInfo extends BaseBeanInfo {

    public ALFINodeBeanBeanInfo() {
      super(ALFINodeBean.class);
      addProperty("name").setCategory("General");
      addProperty("comment").setCategory("General");
      addProperty("box").setCategory("General");
    }
  }

