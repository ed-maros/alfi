package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.Alfi;

    /** This object is used as a key in bundle content maps.  It contains all
      * the information required to determine the source of a channel as well as
      * other utility methods to speed operations associated with bundle
      * content.
      */
public class BundleContentMapKey {

    //**************************************************************************
    //***** constants **********************************************************

    final static int SOURCE_HINT__INPUT_BUNDLER  = 0;
    final static int SOURCE_HINT__OUTPUT_BUNDLER = 1;
    final static int SOURCE_HINT__BUNDLE_PORT    = 2;
 
    //**************************************************************************
    //***** class methods ******************************************************


    //**************************************************************************
    //***** member fields ******************************************************

    private final ALFINode m_channel_source_container;
    private final String m_channel_name;
    private final int m_channel_source_hint;
    private final int m_data_type;
    private final BundleContentSource m_channel_source;

        /** This is used to flag a bundle which is not actually part of the
          * content, but merely a holder for an element inside this bundle which
          * is.
          */
    private boolean mb_is_content_holder_only;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    BundleContentMapKey(ALFINode _channel_source_container,
                                        BundleContentSource _channel_source,
                                        String _channel_name, int _data_type) {
            // order of these member initializations matters
        m_channel_source_container = _channel_source_container;
        m_channel_source = _channel_source;
        m_data_type = _data_type;
        m_channel_source_hint = determineChannelSourceHint();
        m_channel_name = (_channel_name != null) ? _channel_name :
                                                        determineChannelName();
        mb_is_content_holder_only = false;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// general methods ////////////////////////////////////////////////////

    private int determineChannelSourceHint() {
        if (m_channel_source instanceof BundlePortProxy) {
            return SOURCE_HINT__BUNDLE_PORT;
        }
        else {
            BundlerProxy bundler = (BundlerProxy) m_channel_source;

            return (bundler.hasSecondaryInput(m_channel_source_container)) ?
                      SOURCE_HINT__INPUT_BUNDLER : SOURCE_HINT__OUTPUT_BUNDLER;
        }
    }

    private String determineChannelName() {
        if (m_channel_source_hint == SOURCE_HINT__INPUT_BUNDLER) {
            BundlerProxy bundler = (BundlerProxy) m_channel_source;

            return bundler.getSecondaryInputName(m_channel_source_container);
        }
        else {
            Alfi.warn("BundleContentMapKey.determineChannelName():\n" +
                      "Invalid call.");
            return null;
        }
    }

    public BundleContentSource getChannelSource() { return m_channel_source; }

    public ALFINode getChannelSourceContainer() {
        return m_channel_source_container;
    }

    public String getChannelName() { return m_channel_name; }

    public int getDataType() { return m_data_type; }

    public boolean channelIsBundle() {
        return (m_data_type == GenericPortProxy.DATA_TYPE_ALFI_BUNDLE);
    }

        /** This will be true unless the source is an output bundler, in which
          * case, it meant the channel name was in the source's output names
          * list, but was not traceable back any further.
          *
          * Note that channels obtained from a source bundle port's default
          * content *are* considered resolved.
          */
    public boolean channelIsResolvable() {
        return (m_channel_source_hint != SOURCE_HINT__OUTPUT_BUNDLER);
    }

        /** Returns SOURCE_HINT__BUNDLE_PORT, SOURCE_HINT__INPUT_BUNDLER, or
          * SOURCE_HINT__OUTPUT_BUNDLER.
          */
    public int getChannelSourceHint() { return m_channel_source_hint; }

        /** Returns the channel source input bundler if it exists.  Null
          * otherwise.
          */
    public BundlerProxy getSourceInputBundler() {
        return (m_channel_source_hint == SOURCE_HINT__INPUT_BUNDLER) ?
                                      ((BundlerProxy) m_channel_source) : null;
    }

        /** Returns the channel source output bundler if it exists.  Null
          * otherwise.
          */
    public BundlerProxy getSourceOutputBundler() {
        return (m_channel_source_hint == SOURCE_HINT__OUTPUT_BUNDLER) ?
                                      ((BundlerProxy) m_channel_source) : null;
    }

        /** Returns the channel source bundle port if it exists.  Null
          * otherwise.
          */
    public PortProxy getSourceBundlePort() {
        return (m_channel_source_hint == SOURCE_HINT__BUNDLE_PORT) ?
                                   ((PortProxy) m_channel_source) : null;
    }

    public ConnectionProxy determineBundlerSecondaryInput() {
        if (getChannelSourceHint() == SOURCE_HINT__INPUT_BUNDLER) {
            return getSourceInputBundler().getSecondaryInput(
                                                   m_channel_source_container);
        }
        else { return null; }
    }

    public String toString() {
        String info = "BundleContentMapKey: (" + m_channel_source_container +
            ", " + m_channel_source + ", " + m_channel_name +
            ", " + m_data_type + ", " + mb_is_content_holder_only + ")";
        return info;
    }

        /** See mb_is_content_holder_only. */
    public boolean isContentHolderOnly() { return mb_is_content_holder_only; }

        /** See mb_is_content_holder_only. */
    public void setIsContentHolderOnly(boolean _b_is_content_holder_only) {
        mb_is_content_holder_only = _b_is_content_holder_only;
    }
}
