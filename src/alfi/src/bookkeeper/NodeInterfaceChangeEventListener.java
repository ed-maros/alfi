package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /**
     * The interface for objects who wish to register as listeners for
     * NodeInterfaceChangeEvents.
     */
public interface NodeInterfaceChangeEventListener extends EventListener {
    public void nodeInterfaceChange(NodeInterfaceChangeEvent _event);
}
