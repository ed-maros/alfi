package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.tools.*;


    /** A LinkDeclaration is a simple object which may only be added to
      * root box nodes.  They are generally designed to be used in opaque boxes,
      * but may be placed in any root box.  A LinkDeclaration stores a
      * name, value, and a list of primitives' parameters and/or other links
      * (thus creating a chain of links ultimately ending in primitives'
      * parameters) which reside in any nodes inside the root box.  These
      * objects are written and read by only Alfi (not the modeler), but are
      * also used to write the primitive settings associated with the
      * Link (or chain of Links) into modeler read primitive
      * settings sections in box files.
      */
public class LinkDeclaration implements NodeSettingDeclarationIF,
                                        NodeToBeDestroyedEventListener {

    //**************************************************************************
    //***** constants **********************************************************

    private static final String END_VALUE_STRING = "__END_VALUE__";
    private static final String END_LINKS_STRING = "__END_LINKS__";
    private static final String END_COMMENT_STRING = "__END_COMMENT__";

    //**************************************************************************
    //***** member fields ******************************************************

        /** The node in which this Link was locally added. */
    private final ALFINode m_direct_container_node;

        /** The name of the link.  E.g., a link which is associated with the
          * parameters Detector.mirror_1.rad and Detector.mirror_2.rad might
          * be given the link name mirror_radius.
          */
    private String m_name;

        /** The value which will be given to all linked parameters.  This may
          * be null, in which case there is no default value associated with the
          * link (i.e., the targets all keep their own default values if any.)
          */
    private String m_value;

        /** A utility member which is set when validateLinkMap() is called. */
    private String m_data_type;

        /** This map has ALFINode keys and String[] values.  The keys are the
          * nodes which have parameters/links associated with this link,
          * and the value strings are the names of the paramters/links inside
          * the key node which are affected by the value of this link.
          */
    private HashMap m_link_map;

        /** An optional comment associated with this modification. */
    private String m_comment;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor.  See m_link_map for the format required by _link_map.*/
    LinkDeclaration(ALFINode _container_node, String _name,
                           String _value, HashMap _link_map, String _comment) {
        assert(_container_node != null);
        assert(_name != null);

            // final member initializations
        m_direct_container_node = _container_node;

            // other members
        m_name = _name;
        m_value = _value;
        m_comment = _comment;
        m_data_type = null;    // set inside validateLinkMap().

        if (_link_map != null) {
            if (validateLinkMap(_link_map)) {
                m_link_map = _link_map;
            }
            else {
                Alfi.warn("LinkDeclaration(...): " +
                          "Invalid objects in link map.");
                m_link_map = null;
            }
        }
        else { m_link_map = new HashMap(); }
    }

    //**************************************************************************
    //***** static methods *****************************************************

        /** Expects a string argument formatted as:
          *
          *     name
          *     {
          *     value line 1
          *     value line 2
          *     value line n
          *     __END_VALUE__
          *     comment line 1
          *     comment line 2
          *     comment line n
          *     __END_COMMENT__
          *     node_relative_name parameter/link_name
          *     node_relative_name parameter/link_name
          *     node_relative_name parameter/link_name
          *     }
          *
          * Returns a String[] = { name, value, comment,
          *                        node_name_1, parameter/link_name_1,
          *                        node_name_2, parameter/link_name_2, ... }
          * unless an error occurs.
          *
          * On error, the return value is String[1] = { error_msg }.
          */
    public static String[] parse(String[] _lines) {
        ArrayList response_list = new ArrayList();

        String name = _lines[0].trim();
        if (! LinkDeclaration.isValidName(name)) {
            String error_msg =
                       "LinkDeclaration.parse(): Invalid name in\n\n";
            for (int i = 0; i < _lines.length; i++) {
                error_msg += "    " + _lines[i] + "\n";
            }
            String[] error_response = { error_msg };
            return error_response;
        }
        response_list.add(name);

        if (_lines[1].trim().equals(Parser.SECTION_BEGIN_MARKER) &&
                              _lines[_lines.length - 1].trim().equals(
                                                  Parser.SECTION_END_MARKER)) {
            int end_value_index = -1;
            int end_comment_index = -1;
            int end_links_index = _lines.length - 1;
            for (int i = 2; i < end_links_index; i++) {
                String line = _lines[i].trim();
                if (line.equals(END_VALUE_STRING)) { end_value_index = i; }
                else if (line.equals(END_COMMENT_STRING)) {
                    end_comment_index = i;
                }
            }

            String value = new String();
            for (int i = 2; i < end_value_index; i++) {
                value += _lines[i];
                if (i < (end_value_index - 1)) { value += Parser.NL; }
            }
            if (value.equals("null")) { value = null; }
            response_list.add(value);

            String comment = new String();
            for (int i = end_value_index + 1; i < end_comment_index; i++) {
                comment += _lines[i];
                if (i < (end_comment_index - 1)) { comment += Parser.NL; }
            }
            response_list.add(comment);

            for (int i = end_comment_index + 1; i < end_links_index; i++) {
                String[] parts = _lines[i].split("\\s+");
                if (parts.length == 2) {
                    response_list.add(parts[0]);
                    response_list.add(parts[1]);
                }
                else {
                    String error_msg =
                                  "Link.parse(): Invalid link in\n\n";
                    for (int j = 0; j < _lines.length; j++) {
                        error_msg += "    " + _lines[j] + "\n";
                    }
                    String[] error_response = { error_msg };
                    return error_response;
                }
            }
         
            String[] response = new String[response_list.size()];
            response_list.toArray(response);
            return response;
        }
        else { return null; }
    }

    public static boolean isValidName(String _name) {
        return _name.matches("^\\w+$");
    }


    //**************************************************************************
    //***** methods ************************************************************

        /** Checks that the parameters/links mapped in m_link_map actually
          * exist.  Also sets utility member m_data_type and registers as a
          * NodeToBeDestroyedEventListener with all target nodes.  See
          * m_link_map for what is expected in the map.
          */
    private boolean validateLinkMap(HashMap _link_map) {
        String data_type = null;
        for (Iterator i = _link_map.keySet().iterator(); i.hasNext(); ) {
            Object key = i.next();
            Object value = _link_map.get(key);
            if ( (! (key instanceof ALFINode)) ||
                                             (! (value instanceof String[]))) {
                Alfi.warn("LinkDeclaration.validateLinkMap(...):\n" +
                    "\tInvalid object(s) in link map.");
                return false;
            }
            else {
                ALFINode node = (ALFINode) key;
                ALFINode root_node = node.baseNode_getRoot();
                String[] names = (String[]) value;
                NodeSettingDeclarationIF setting = null;
                for (int j = 0; j < names.length; j++) {
                    if (node.isBox()) {
                        setting = root_node.linkDeclaration_getLocal(names[j]);
                    }
                    else {
                        setting =
                             root_node.parameterDeclaration_getLocal(names[j]);
                    }
                    if (setting != null) {
                        if (data_type == null) {
                            data_type = setting.getType();
                        }
                        else if (! setting.getType().equals(data_type)) {
                            Alfi.warn(
                                "LinkDeclaration.validateLinkMap(" +
                                "...):\n\tInconsistant data types in map.");
                        }

                        node.addNodeToBeDestroyedEventListener(this);
                    }
                    else {
                        Alfi.warn(
                            "LinkDeclaration.validateLinkMap(...):\n" +
                            "\tLink [" + names[j] + "] not " +
                            "found in node [" + root_node +"].");
                        return false;
                    }
                }
            }
        }

        m_data_type = data_type;
        return true;
    }

    /////////////////////////////////
    // member field access //////////

    public String getName() { return m_name; }

    public void rename(String _name) {
        if (isValidName(_name)) {
            m_name = _name;
        }

        m_direct_container_node.setTimeLastModified();
        ALFINode[] derived_nodes= m_direct_container_node.derivedNodes_getAll();
        for (int i = 0; i < derived_nodes.length; i++) {
            if (derived_nodes[i].linkSetting_containsLocal(m_name)) {
                derived_nodes[i].setTimeLastModified();
            }
        }
    }

    public String getValue() { return m_value; }

    public String getType() { return m_data_type; }

    public String getComment() { 
        if (m_comment != null) { return m_comment.toString(); }
        else { return new String(""); }
    }

    public boolean isQuoted() { return true; }

        /** @returns m_direct_container_node. */
    ALFINode getDirectContainerNode() { return m_direct_container_node; }

        /** See m_link_map for format of returned map. */
    public HashMap getLinks() { return m_link_map; }

    public String[] getLinkTargetDescriptions() {
        ArrayList desc_list = new ArrayList();
        for (Iterator i = m_link_map.keySet().iterator(); i.hasNext(); ) {
            ALFINode node = (ALFINode) i.next();
            String[] names = (String[]) m_link_map.get(node);
            for (int j = 0; j < names.length; j++) {
                desc_list.add(new String(
                            determineRelativeNodeName(node) + " " + names[j]));
            }
        }

        String[] descriptions = new String[desc_list.size()];
        desc_list.toArray(descriptions);
        return descriptions;
    }

        /** See m_link_map for expected _link_map format. */
    public void setLinks(HashMap _link_map) {
        if (validateLinkMap(_link_map)) { m_link_map = _link_map; }
        else {
            Alfi.warn("Link.setLinks(): Invalid link map format.");
        }
    }

    public void addLink(ALFINode _member, String _name) {
        String[] old_names = (String[]) m_link_map.get(_member);
        if (old_names != null) {
            String[] new_names = new String[old_names.length+1];
            System.arraycopy(old_names, 0, new_names, 0, old_names.length);
            new_names[new_names.length - 1] = _name;
            m_link_map.put(_member, new_names);
        }
        else {
            String[] new_names = { _name };
            m_link_map.put(_member, new_names);
        }
    }

    public void removeLink(ALFINode _member, String _name) {
        String[] old_names = (String[]) m_link_map.get(_member);
        if (old_names != null) {
            List name_list = Arrays.asList(old_names);
            boolean b_item_removed = false;
            for (Iterator i = name_list.iterator(); i.hasNext(); ) {
                if (i.next().equals(_name)) { i.remove(); }
                b_item_removed = true;
            }

            if (b_item_removed) {
                String[] names = new String[name_list.size()];
                name_list.toArray(names);
                m_link_map.put(_member, names);
            }
        }
    }

    public void clearLinks() { m_link_map.clear(); }

    public String generateParserRepresentation(String _starting_spacer) {
        final String NL = Parser.NL;

        String rep = _starting_spacer + m_name + NL;
        rep += _starting_spacer + Parser.SECTION_BEGIN_MARKER + NL;

        String full_spacer = _starting_spacer + Parser.STANDARD_SPACER;

        String[] value_lines = { "null" };
        if (m_value != null) { value_lines = m_value.split(NL); }

        for (int i = 0; i < value_lines.length; i++) {
            rep += full_spacer + value_lines[i] + NL;
        }
        rep += full_spacer + END_VALUE_STRING + NL;

        if ((m_comment != null) && (m_comment.length() > 0)) {
            String[] comment_lines = m_comment.split(NL);
            for (int i = 0; i < comment_lines.length; i++) {
                rep += full_spacer + comment_lines[i] + NL;
            }
        }
        rep += full_spacer + END_COMMENT_STRING + NL;

        for (Iterator i = m_link_map.keySet().iterator(); i.hasNext(); ) {
            ALFINode node = (ALFINode) i.next();
            String relative_node_name = determineRelativeNodeName(node);
            String[] names = (String[]) m_link_map.get(node);
            for (int j = 0; j < names.length; j++) {
                rep += full_spacer + relative_node_name + " " + names[j] + NL;
            }
        }
        rep += _starting_spacer + Parser.SECTION_END_MARKER;

        return rep;
    }

    private String determineRelativeNodeName(ALFINode _contained_node) {
        String container_full_name =
                            m_direct_container_node.generateFullNodePathName();
        String contained_node_full_name =
                                    _contained_node.generateFullNodePathName();
        if (contained_node_full_name.startsWith(container_full_name)) {
            return contained_node_full_name.substring(
                                             container_full_name.length() + 1);
        }
        else {
            Alfi.warn("Link.determineRelativeNodeName():\n" +
                "    Node [" + _contained_node + "] not found in container [" +
                m_direct_container_node + "].");
            return null;
        }
    }

    public String toString() {
        String mapped_to = "[";
        for (Iterator i = m_link_map.keySet().iterator(); i.hasNext(); ) {
            ALFINode node = (ALFINode) i.next();
            String[] names = (String[]) m_link_map.get(node);
            for (int j = 0; j < names.length; j++) {
                mapped_to += node.generateFullNodePathName() + ":" + names[j];
                if (j < (names.length - 1)) { mapped_to += ", "; }
            }
            if (i.hasNext()) { mapped_to += ", "; }
        }
        mapped_to += "]";

        return m_name + " = (" + m_value + ") linked to " + mapped_to;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///// NodeToBeDestroyedEventListener methods ///////////////////////////////

        /** Links which have targets inside nodes which are being removed need
          * to be cleaned up.
          */
    public void nodeToBeDestroyed(NodeToBeDestroyedEvent _event) {
        ALFINode node_to_be_destroyed = (ALFINode) _event.getSource();
        m_link_map.remove(node_to_be_destroyed);
    }
}
