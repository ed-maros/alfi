package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /**
     * The interface for objects who wish to register as listeners for
     * NodeContentChangeEvents.
     *
     * See the code for an skeleton of nodeContentChanged().
     */
public interface NodeContentChangeEventListener extends EventListener {
    public void nodeContentChanged(NodeContentChangeEvent e);
}

/*

Here is a skeleton for getting information from a NodeContentChangeEvent
as fired from an ALFINode:

    public void nodeContentChanged(NodeContentChangeEvent e) {
        int action = e.getAction();
        if (action == NodeContentChangeEvent.MEMBER_NODE_DELETE) {
            MemberNodeProxy mnp = (MemberNodeProxy) e.getAssociatedElement();

        }
        else if (action == NodeContentChangeEvent.MEMBER_NODE_ADD) {
            MemberNodeProxy mnp = (MemberNodeProxy) e.getAssociatedElement();

        }
        else if (action == NodeContentChangeEvent.CONNECTION_DELETE){
            ConnectionProxy con = (ConnectionProxy) e.getAssociatedElement();

        }
        else if (action == NodeContentChangeEvent.CONNECTION_ADD) {
            ConnectionProxy con = (ConnectionProxy) e.getAssociatedElement();

        }
        else if (action == NodeContentChangeEvent.PORT_DELETE) {
            PortProxy port = (PortProxy) e.getAssociatedElement();

        }
        else if (action == NodeContentChangeEvent.PORT_ADD) {
            PortProxy port = (PortProxy) e.getAssociatedElement();

        }
        else if (action == NodeContentChangeEvent.JUNCTION_DELETE) {
            JunctionProxy junction = (JunctionProxy) e.getAssociatedElement();

        }
        else if (action == NodeContentChangeEvent.JUNCTION_ADD) {
            JunctionProxy junction = (JunctionProxy) e.getAssociatedElement();

        }
    }
*/
