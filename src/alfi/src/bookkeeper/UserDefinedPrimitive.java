package edu.caltech.ligo.alfi.bookkeeper;

import java.io.*;
import java.util.*;
import java.awt.Toolkit;
import java.awt.Image;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.tools.*;

    /**
      * A UserDefinedPrimitive is a subclass of FUNCX_Node.
      * <p>
      * This primitive allows the user to create primitives of a completely
      * general nature.  The FUNCX_Node superclass already provides for the
      * definition of an action of a general nature, and this class allows the
      * user to manipulate all the I/O port numbers and types as well.
      */
public class UserDefinedPrimitive extends FUNCX_Node {

    private static final String UDP_DELAY = "Delay";
    private static final String UDP_PREACTION = "PreAction";

    private static int UDP_SETTING_NAME = 0;
    private static int UDP_SETTING_TYPE = 1;
    private static Object [][] UDPSettings = {
//      { Setting name, data type }
        { FUNC_X_EQUATIONS, ParameterDeclaration.TYPE_STRING },
        { FUNC_X_MEMBER_DECL, ParameterDeclaration.TYPE_STRING },
        { FUNC_X_MEMBER_IMPL, ParameterDeclaration.TYPE_STRING },
        { FUNC_X_GLOBAL, ParameterDeclaration.TYPE_STRING },
        { FUNC_X_HEADER, ParameterDeclaration.TYPE_STRING },
        { FUNC_X_CONSTRUCTOR, ParameterDeclaration.TYPE_STRING },
        { FUNC_X_DESTRUCTOR, ParameterDeclaration.TYPE_STRING },
        { FUNC_X_DOWHENGLOBALCHANGES,  ParameterDeclaration.TYPE_STRING },
        { UDP_PREACTION, ParameterDeclaration.TYPE_STRING },
        { UDP_DELAY, ParameterDeclaration.TYPE_REAL },
        { ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG,
                                            ParameterDeclaration.TYPE_BOOLEAN },
//      { Parser.FILE_INCLUDE_MARKER, ParameterDeclaration.TYPE_STRING }
        };  


    private static final String DEFAULT_INSTANCE_SETTING = "false";
    private static final String DEFAULT_DELAY_SETTING = "0.0";

        // This should only be used in the UDP dialog when the .udp file
        // hasn't been created yet.
    private String m_temp_udp_dialog_icon_path; 

        // show the warning once only
    private boolean mb_missing_icon_file_warning_enabled;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor used by the parser to create an empty UDP node before
          * then adding elements to it as directed in the box file currently
          * being read.
          */
    public UserDefinedPrimitive(String _node_type_name) {
        super(_node_type_name, "UDP");
        m_temp_udp_dialog_icon_path = null;
        mb_missing_icon_file_warning_enabled = true;
    }

        /** _port_position_matrix is a [4][n] array in NSEW order, and the
          * PortProxy objects in the array are temporary ports which are
          * only data holders for the creation of the real port objects to be
          * added to this node.  As an example, the [0][0] element would be
          * the initial port slot on the north side and the [2][4] element, if
          * it existed, would be the 5th slot along the east edge.  These
          * elements may be null, in order to show the slot has no port.
          *
          * _parameter_declarations is also a set of temporary data holding
          * objects associated with any ParameterDeclarations needed to be
          * added.
          */
    public UserDefinedPrimitive(String _node_type_name, String _comment,
     String _group, PortProxy[][] _ports, ParameterDeclaration[] _param_decls) {

        super(_node_type_name, _group);
        
        for (int edge = 0; edge < PortProxy.EDGE_INVALID; edge++) {
            for (int i = 0; i < _ports[edge].length; i++) {
                int[] port_position = {edge, i, edge, i};
                if ((_ports[edge][i] != null) &&
                         (! port_nameAndSameIOExists(_ports[edge][i].getName(), 
                                               _ports[edge][i].getIOType()))) {
                    port_add(_ports[edge][i].getName(),
                             _ports[edge][i].getIOType(),
                             _ports[edge][i].getDataType(), port_position); 
                }
            }
        }

        for (int i = 0; i < _param_decls.length; i++) {
            
            ParameterDeclaration param_decl = 
                parameterDeclaration_add(_param_decls[i].getType(),
                _param_decls[i].getName(), _param_decls[i].getValue(), "");  
        }


        m_comment = _comment;
        m_temp_udp_dialog_icon_path = null;
        mb_missing_icon_file_warning_enabled = true;
    }

        /** Constructor for creating a member UDP.  See deriveInstance()
          * below for "public" access to this method.  This constructor is never
          * called except by UserDefinedPrimitive.deriveInstance() and by
          * super() in any derived class's constructors.
          */
    protected UserDefinedPrimitive(ALFINode _base_node, ALFINode _container,
                                 MemberNodeProxy _associated_MemberNodeProxy) {
        super(_base_node, _container, _associated_MemberNodeProxy);
        m_temp_udp_dialog_icon_path = null;
        mb_missing_icon_file_warning_enabled = true;
    }

        /** Returns the full path to the icon image file associated with
          * this node (or null, if none exists.)  The image file is ALWAYS
          * expected to be found in the same directory as the file which
          * describes this UDP file, with 1 exception: If this UDP is currently
          * being edited, then the field m_temp_udp_dialog_icon_path may be
          * non-null, and in that case, the filepath given in
          * m_temp_udp_dialog_icon_path takes precedence over any value in
          * m_icon_info, and such a file may exist anywhere in the filesystem.
          * The editor will move the new icon file into the correct location
          * when the edit is completed.
          */
    public String getIconFilePath() {
        String icon_file_path = "";

        if (m_temp_udp_dialog_icon_path != null) {
            icon_file_path = m_temp_udp_dialog_icon_path;
        }
        else if (m_icon_info != null) {
                // find where this udp's root's file is located
            String udp_file_name = (isRootNode() ?
                    (determineLocalName() + ALFIFile.UDP_FILE_SUFFIX) :
                    (baseNode_getRoot().determineLocalName() 
                                                   + ALFIFile.UDP_FILE_SUFFIX));

                // parse the icon info
            StringTokenizer st = new StringTokenizer(m_icon_info);
            if ((st.countTokens() == 3) && (st.nextToken().equals("Icon"))) {
                String type = st.nextToken();
                String icon_file_name = st.nextToken();


                File udp_file = MiscTools.findFile(udp_file_name,Alfi.E2E_PATH);
                if (udp_file != null) {
                        // icon file should be in the same directory as udp file
                    File icon_file =
                            new File(udp_file.getParentFile(), icon_file_name);
                    if (icon_file != null && (icon_file.exists())) { 
                        icon_file_path = icon_file.getPath();
                    }
                    else if (mb_missing_icon_file_warning_enabled) {
                            // this is a problem.  the icon file should be in
                            // the same directory as the root udp file.
                        System.err.println("("+this + ").getIconFilePath():\n" +
                            TextFormatter.formatMessage("Icon file not found." +
                                "  The icon file for a UDP must be located in" +
                                " the same directory as the UDP file.  In this"+
                                " case, the icon file [" + icon_file_name +
                                "] could not be found in the directory [" +
                                udp_file.getParentFile() + "]."));
                        mb_missing_icon_file_warning_enabled = false;
                    }
                }
            }
        }
        return icon_file_path;
    }

        /** Sets the default icon path directory for this UDP */
    public void setTempIconPath(String _path) {
        m_temp_udp_dialog_icon_path = _path; 
    } 

    public String getTempIconPath() { return m_temp_udp_dialog_icon_path; } 


    //**************************************************************************
    //***** static methods *****************************************************

    public static ParameterDeclaration[] parameterDeclarations_getCoreList() {
        ParameterDeclaration[] core_list = 
            new ParameterDeclaration[UDPSettings.length];
            
        for (int i = 0; i < UDPSettings.length; i++) {
            core_list[i] = new ParameterDeclaration( 
                ( String )UDPSettings[i][UDP_SETTING_TYPE],
                ( String )UDPSettings[i][UDP_SETTING_NAME], "");
            if (UDPSettings[i][UDP_SETTING_NAME].equals(
                             ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG)) {
                core_list[i].setValue(DEFAULT_INSTANCE_SETTING);
            }
            else if (UDPSettings[i][UDP_SETTING_NAME].equals(UDP_DELAY)) {
                core_list[i].setValue(DEFAULT_DELAY_SETTING);
            } 
        }
        return core_list;
    }


    //**************************************************************************
    //***** instance methods ***************************************************

        /** Overrides the ALFINode call in order to create the correct type
          * of node.  This call should always be made by the node which is to
          * be the DIRECT base node of the node to be created.  And of course,
          * this new node will be being placed inside another container node
          * (_container_node), and will always have a MemberNodeProxy
          * (_associated_MemberNodeProxy) associated with the new node's
          * placement in _container_node.
          */
    ALFINode deriveInstance(ALFINode _container_node,
                                 MemberNodeProxy _associated_MemberNodeProxy) {
        return new UserDefinedPrimitive(this, _container_node,
                                                  _associated_MemberNodeProxy);
    }

        /** Overrides FUNCX_Node.determineAtAtIncludeFile().  If
          * FUNCX_Node.getAtAtIncludeSettingValueObject() returns a
          * ParameterSetting, then this method just returns the value gotten
          * from FUNCX_Node.determineAtAtIncludeFile().  But if a
          * ParameterDeclaration is returned, then this method searches for
          * the directory in which this node's root base's .udp sits and looks
          * there for the @@INCLUDE file.  I.e., the .udp file is where the
          * @@INCLUDE appeared, and the @@INCLUDEd file must sit relative to
          * that directory.
          */
    File determineAtAtIncludeFile(String _setting_name) {
        Object object_with_include_value;
        if (isRootNode()) {
            object_with_include_value = parameterDeclaration_getLocal(
                                                                _setting_name); 
        }
        else {
            object_with_include_value =
                               getAtAtIncludeSettingValueObject(_setting_name);
        }

        File include_file = null;

        if (object_with_include_value instanceof ParameterSetting) {
            include_file = super.determineAtAtIncludeFile(_setting_name);
        }
        else if (object_with_include_value instanceof ParameterDeclaration) {
            ParameterDeclaration declaration =
                              (ParameterDeclaration) object_with_include_value;
            String include_file_name =
                                  parseAtAtIncludeLine(declaration.getValue());

            File udp_file = findAssociatedUDPFile();

                // the @@INCLUDE file should be in the same directory
            include_file = new File(udp_file.getParentFile(),include_file_name);
        }

        return include_file;
    }

        /** Overrides FUNCX_Node.ParseContentForValue().  If
          * _parameter is UDP_PREACTION, parse the string value.
          */
    protected String parseContentForValue(String _parameter_name,
                                                      String[] _file_content) {
        if (_parameter_name.equals(UDP_PREACTION)) {
            if (_file_content != null && _file_content.length > 0) {
                String start_delimeter =
                    new String(FUNCX_Node.FUNC_X_SETTING_BEGIN + "PREACTION*/");
                String end_delimeter =
                    new String(FUNCX_Node.FUNC_X_SETTING_END + "PREACTION*/");

                return extractValueString(start_delimeter, end_delimeter, 
                                                                 _file_content);
            }
        }
        else {
            return super.parseContentForValue(_parameter_name, _file_content);
        }
        
        return null;
    }

    public File findAssociatedUDPFile() {
        String udp_file_name = (isRootNode() ?
            (determineLocalName() + ALFIFile.UDP_FILE_SUFFIX) :
            (baseNode_getRoot().determineLocalName() +
                                                    ALFIFile.UDP_FILE_SUFFIX));
        return MiscTools.findFile(udp_file_name, Alfi.E2E_PATH);
    }


        /** UDP root node external views should generate these events. */
    protected boolean shouldGeneratePortAddEvents() {
        return isRootNode();
    }

}
