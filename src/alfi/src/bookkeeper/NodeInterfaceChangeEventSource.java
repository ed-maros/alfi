package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.bookkeeper.*;

/**
  * This interface defines fields and methods used by sources of 
  * NodeInterfaceChangeEvent sources.
  */
public interface NodeInterfaceChangeEventSource {
    public void addNodeInterfaceChangeListener(
                        NodeInterfaceChangeEventListener _listener);
    public void removeNodeInterfaceChangeListener(
                        NodeInterfaceChangeEventListener _listener);
}

