package edu.caltech.ligo.alfi.bookkeeper;

import java.util.ListResourceBundle;

public class ALFINodeBeanRB extends ListResourceBundle {

    protected Object[][] getContents() {
      return new Object[][] { {"name", "Name"},
          {"comment", "Comment"}, 
          {"box", "Is Box?"}, 
          {"nodeInfo", "Node Information"}, 
        };
    }
  }


