package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.bookkeeper.*;

/**
  * This interface defines fields and methods used by sources of 
  * ProxyChangeEvent sources.
  *
  * Every widget is responsible for updating its associated proxy object in
  * the bookkeeping.  In order to do this, it will fire off ProxyChangeEvent
  * events to the listening proxy.  Such events can also be fired off to
  * other edit sessions which need to know about them.
  */
public interface ProxyChangeEventSource {
    public void addProxyChangeListener(ProxyChangeEventListener _listener);
    public void removeProxyChangeListener(ProxyChangeEventListener _listener);
}

