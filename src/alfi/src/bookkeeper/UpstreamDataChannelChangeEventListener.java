package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /** The interface for objects who wish to register as listeners for
      * UpstreamDataChannelChangeEvents.
      */
public interface UpstreamDataChannelChangeEventListener extends EventListener {
    public void upstreamChangeOccurred(UpstreamDataChannelChangeEvent _e);
}
