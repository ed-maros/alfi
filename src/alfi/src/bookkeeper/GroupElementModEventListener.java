package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /**
     * The interface for objects who wish to register as listeners for
     * GroupElementModEvents.
     *
     */
public interface GroupElementModEventListener extends EventListener {
    public void groupElementModified(GroupElementModEvent e);
}

/*

Here is a skeleton for getting information from a GroupElementModEvent
as fired from contained element (ALFINode, MemberNodeProxy, etc.):

    public void groupElementModified(GroupElementModEvent e) {
        Object source = e.getSource();    
    }
*/
