package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.tools.*;


    /** A Bundle is an extension of a connection, and is used only in
      * cases where a connection's data type is type DATA_TYPE_ALFI_BUNDLE.
      */
public class BundleProxy extends ConnectionProxy {

    //**************************************************************************
    //***** static fields ******************************************************

        /** See getBundleContentUpdatesEnabled(). */
    private static boolean sb_suspend_content_updates = false;

    //**************************************************************************
    //***** local exception classes ********************************************


    //**************************************************************************
    //***** member fields ******************************************************

    private Boolean mB_isSourceBundlerPrimaryOutput;
    private Boolean mB_isSinkBundlerPrimaryInput;


    //**************************************************************************
    //***** constructors *******************************************************

    /** Main constructor.  Should only be called from ALFINode. */
    BundleProxy(ALFINode _container_node,
                    GenericPortProxy _source, MemberNodeProxy _source_member,
                    GenericPortProxy _sink, MemberNodeProxy _sink_member,
                    Integer[] _path_definition) {
        super(_container_node, _source, _source_member,
                               _sink, _sink_member, _path_definition);

        mB_isSourceBundlerPrimaryOutput = new Boolean(false);
        mB_isSinkBundlerPrimaryInput = new Boolean(false);
    }

        /**
         * This constructor should only be called by a JunctionProxy in the
         * creation of its connection-from-source.
         */
    BundleProxy(JunctionProxy _sink_junction, GenericPortProxy _source,
                MemberNodeProxy _source_member, Integer[] _path_definition) {

        super(_sink_junction, _source, _source_member, _path_definition);

        mB_isSourceBundlerPrimaryOutput = new Boolean(false);
        mB_isSinkBundlerPrimaryInput = new Boolean(false);
    }

        /**
         * Auxilliary constructor which creates a proxy for temporary storage
         * of BundleProxy information only.  Thes objects are useless
         * elsewhere because the (final) container node field is null.
         */
    private BundleProxy(GenericPortProxy _source,
                    MemberNodeProxy _source_member, GenericPortProxy _sink,
                    MemberNodeProxy _sink_member, Integer[] _path_definition,
                    Integer _dog_leg_hint,
                    Boolean _B_is_primary_out_for_source_bundler,
                    Boolean _B_is_primary_in_for_sink_bundler) {

        super(_source, _source_member, _sink, _sink_member,
                                              _path_definition, _dog_leg_hint);

        mB_isSourceBundlerPrimaryOutput = _B_is_primary_out_for_source_bundler;
        mB_isSinkBundlerPrimaryInput = _B_is_primary_in_for_sink_bundler;
    }

    //**************************************************************************
    //***** static methods *****************************************************

        /** Bundle content caches are generally updated automatically by
          * background threads started by various objects associated with
          * bundles when these objects see a change which may have affected the
          * content.  But sometimes many changes are being made, which will
          * cause many such overlapping threads to start and such threads may
          * find a state which it deems inconsistent due to changes still being
          * made by the main thread while the background update thread is
          * progressing.  Such updates should be suspended until all the changes
          * have been made and then the state of the system should be checked to
          * see if the update still makes sense before finally doing the update.
    public static boolean getBundleContentUpdatesSuspended() {
        return sb_suspend_content_updates;
    }
          */

        /** See getSuspendBundleContentUpdates().
    public static void setBundleContentUpdatesSuspended(boolean
                                                          _b_suspend_updates) {
        sb_suspend_content_updates = _b_suspend_updates;
    }
 */

        /** Overridden version from ConnectionProxy to allow for source and/or
          * sink bundlers.  Otherwise, the call is transferred to
          * ConnectionProxy.prerequisitesExist().
          */
    protected static String prerequisitesExist(
                ALFINode _container, GenericPortProxy _terminal,
                MemberNodeProxy _terminal_member, String _terminal_desc) {
        String complaint = null;
        if (_terminal_member == null) {
            if (_terminal instanceof BundlerProxy) {
                if (! _container.bundler_contains((BundlerProxy) _terminal)) {
                    complaint = "bundler not contained in " + _container;
                }
            }
            else {
                complaint = ConnectionProxy.prerequisitesExist(_container,
                                  _terminal, _terminal_member, _terminal_desc);
            }
        }
        return complaint;
    }

        /** Creates a flattened version of the content Hashtable.  The keys
          * it creates are full channel paths, the values are
          * BundleContentMapKeys.  _current_path_prefix should be set to "" on
          * the initial call.  This parameter is used with recursive calls.
          */
    public static Hashtable contentMapToFlatChannelMap(Hashtable _content,
                                                 String _current_path_prefix) {
        if (_current_path_prefix.length() > 0) { _current_path_prefix += "."; }

        Hashtable channel_map = new Hashtable();
        for (Iterator i = _content.keySet().iterator(); i.hasNext(); ) {
            BundleContentMapKey key = (BundleContentMapKey) i.next();
            String channel_name = key.getChannelName();

            String channel_path = _current_path_prefix + channel_name;
            channel_map.put(channel_path, key);

            Object value = _content.get(key);
            if (value instanceof Hashtable) {
                Hashtable flat_sub_content = contentMapToFlatChannelMap(
                                              (Hashtable) value, channel_path);
                channel_map.putAll(flat_sub_content);
            }
        }

        return channel_map;
    }

        /** Reverses the process of contentMapToFlatChannelMap(). */
    static Hashtable flatChannelMapToContentMap(Hashtable _flat_map) {
        Hashtable content = new Hashtable();

        HashMap content_submap_vs_name_map = new HashMap();
            // prime with only existing (so far) parent container map
        content_submap_vs_name_map.put("", content);

            // ensures leaves come after branches in bundle hierarchy
        TreeMap sorted_flat_map = new TreeMap(_flat_map);

        final char BUNDLE_PATH_DELIM = '.';
        for (Iterator i = sorted_flat_map.keySet().iterator(); i.hasNext(); ) {
            String path = (String) i.next();
            BundleContentMapKey content_key =
                                     (BundleContentMapKey) _flat_map.get(path);
            Hashtable parent_content_map = content;    // top level is default
            int delim_pos = path.lastIndexOf(BUNDLE_PATH_DELIM);
            while (delim_pos > 0) {
                String parent_path = path.substring(0, delim_pos);
                if (content_submap_vs_name_map.containsKey(parent_path)) {
                    parent_content_map =
                        (Hashtable) content_submap_vs_name_map.get(parent_path);
                    break;
                }
                else { delim_pos = parent_path.lastIndexOf(BUNDLE_PATH_DELIM); }
            }

                // assumes children will be added later (cleaned later if not)
            Hashtable map_of_children =
                              (Hashtable) content_submap_vs_name_map.get(path);
            if (map_of_children == null) {
                map_of_children = new Hashtable();
                content_submap_vs_name_map.put(path, map_of_children);
            }
            parent_content_map.put(content_key, map_of_children);
        }

            // now we need to clean up the content by removing empty child
            // hashtables
        removeEmptyHashes(content);

        return content;
    }

        /** Recursively searches through a content hashtable and removes empty
          * child hashtables.
          */
    static void removeEmptyHashes(Hashtable _content) {
        for (Iterator i = _content.keySet().iterator(); i.hasNext(); ) {
            BundleContentMapKey key = (BundleContentMapKey) i.next();
            Object value = _content.get(key);
            if (value instanceof Hashtable) {
                Hashtable child_map = (Hashtable) value;
                if (child_map.isEmpty()) {
                    _content.put(key, "");
                }
                else { removeEmptyHashes(child_map); }
            }
        }
    }

    //**************************************************************************
    //***** methods ************************************************************

        /** Overrides ConnectionProxy method to give obvious result. */
    public int determineUpstreamDataType(ALFINode _container_node) {
        return GenericPortProxy.DATA_TYPE_ALFI_BUNDLE;
    }

        /** Overrides ConnectionProxy method to give obvious result. */
    public int determineDownstreamDataType(ALFINode _container_node) {
        return GenericPortProxy.DATA_TYPE_ALFI_BUNDLE;
    }

       /** Use with caution.  Usually BundlerProxy.setPrimaryOutput() should be
         * used instead.
         */
    public void setSourceBundlerPrimaryOutFlag(boolean _b_flag_as_primary_out) {
        mB_isSourceBundlerPrimaryOutput = new Boolean(_b_flag_as_primary_out);
    }

    public boolean isSourceBundlerPrimaryOut() {
        return mB_isSourceBundlerPrimaryOutput.booleanValue();
    }

       /** Use with caution.  Usually BundlerProxy.setPrimaryInput() should be
         * used instead.
         */
    void setSinkBundlerPrimaryInFlag(boolean _b_flag_as_primary_in) {
        mB_isSinkBundlerPrimaryInput = new Boolean(_b_flag_as_primary_in);
    }

    public boolean isSinkBundlerPrimaryIn() {
        return mB_isSinkBundlerPrimaryInput.booleanValue();
    }

    public Hashtable getContent(ALFINode _container,
                                               boolean _b_use_cached_content) {
        Hashtable content = new Hashtable();
        GenericPortProxy source = getSource();
        if (source instanceof BundlePortProxy) {
            content.putAll(((BundlePortProxy) source).getContent(_container,
                                    getSourceMember(), _b_use_cached_content));
        }
        else if (source instanceof JunctionProxy) {
            BundleProxy previous_bundle_segment = (BundleProxy)
                      ((JunctionProxy) source).getSourceConnection(_container);
            if (previous_bundle_segment != null) {
                content.putAll(previous_bundle_segment.getContent(
                                           _container, _b_use_cached_content));
            }
        }
        else if (source instanceof BundlerProxy) {
            BundlerProxy bundler = (BundlerProxy) source;
            if (isSourceBundlerPrimaryOut()) {
                BundleProxy primary_in = bundler.getPrimaryInput(_container);
                if (primary_in != null) {
                    content.putAll(primary_in.getContent(_container,
                                                       _b_use_cached_content));
                }

                if (bundler.hasSecondaryInput(_container)) {
                    content.putAll(bundler.getSecondaryInContent(_container,
                                                       _b_use_cached_content));
                }
            }
            else {
                content.putAll(bundler.getSecondaryOutContent(_container,
                                                       _b_use_cached_content));
            }
        }

        return content;
    }

        /** This method determines the difference in content in this bundle
          * between when it is in _container vs when it is in _container's base
          * node.  Essentially these are LOCAL additions to the content, though
          * they may have appeared solely due to content changes occuring
          * upstream of node _container.  If _container is a root base node,
          * an empty Hashtable is returned.
          */
    Hashtable getLocalContentChanges(ALFINode _container) {
        ALFINode base = _container.baseNode_getDirect();
        if (base != null) {
            Hashtable flat_content_diff = 
                  contentMapToFlatChannelMap(getContent(_container, true), "");

            Hashtable flat_base_content =
                        contentMapToFlatChannelMap(getContent(base, true), "");

            if (flat_base_content.size() != flat_content_diff.size()) {
                for (Iterator i = flat_base_content.keySet().iterator();
                                                               i.hasNext(); ) {
                    flat_content_diff.remove(i.next());
                }

                return flat_content_diff;
            }
        }
        return new Hashtable();
    }

        /** This method traces a existing channel in a bundle back to
          * a port in the node _container, if such exists.  It may be a bundle
          * port (if the channel still resides in a bundle at that point, or it
          * may be a standard single channel port if an upstream input bundler
          * inserted the channel into the bundle in _container.  This trace
          * does not leave the confines of _container.  The returned value is
          * an Object[3] = { PortProxy (source port), MemberNodeProxy (owner
          * member if it exists, otherwise null), String (channel name at source
          * port (will be null if source port is not a bundle port)) }
          */
    Object[] traceChannelBackToPort(ALFINode _container, String _channel_path) {
        GenericPortProxy source = getSource();
        if (source instanceof PortProxy) {
            MemberNodeProxy owner = getSourceMember();
            Object[] info = { source, owner, _channel_path };
            return info;
        }
        else if (source instanceof JunctionProxy) {
            BundleProxy upstream_bundle = (BundleProxy)
                      ((JunctionProxy) source).getSourceConnection(_container);
            return upstream_bundle.traceChannelBackToPort(_container,
                                                                _channel_path);
        }
        else if (source instanceof BundlerProxy) {
            BundlerProxy bundler = (BundlerProxy) source;
            String channel_path = _channel_path;
            if (this == bundler.getPrimaryOutput(_container)) {
                if (bundler.hasSecondaryInput(_container)) {
                    Hashtable secondary_in_flat_content =
                        BundleProxy.contentMapToFlatChannelMap(
                           bundler.getSecondaryInContent(_container, true), "");
                    if (secondary_in_flat_content.containsKey(channel_path)) {
                        ConnectionProxy secondary_in = 
                                         bundler.getSecondaryInput(_container);
                        if ((secondary_in instanceof BundleProxy) &&
                                (! bundler.secondaryInputUnwraps(_container))) {
                            String input_name =
                                     bundler.getSecondaryInputName(_container);
                            int chop_length = input_name.length() + 1;
                            channel_path = channel_path.substring(chop_length);
                        }
                        return secondary_in.traceChannelBackToPort(_container,
                                                                 channel_path);
                    }
                }
            }
            else {
                    // this bundle is the secondary output of bundler, and if
                    // there is only one name in the list of outputs, that name
                    // needs to be glued back onto the channel name for tracing
                    // further back upstream, as this name had been (implicitly)
                    // stripped off the secondary output bundle.
                HashMap secondary_output_name_map =
                            bundler.getSecondaryOutputChannelNames(_container);
                if (secondary_output_name_map.size() == 1) {
                    String stripped_name = (String)
                          secondary_output_name_map.keySet().iterator().next();
                    channel_path = stripped_name + "." + channel_path;
                }
            }

            BundleProxy primary_in = bundler.getPrimaryInput(_container);
            if (primary_in != null) {
                 return primary_in.traceChannelBackToPort(_container,
                                                                 channel_path);
            }
        }
        else {
            Alfi.warn("(" + this + ").traceChannelBackToPort(" + _container +
                      ", " + _channel_path + "): Source port type error.");
        }
        return null;
    }

        /** This method traces an existing channel in a bundle downstream to
          * a local port or ports (the stream may split at bundlers and
          * junctions to be delivered to multiple ports.)  This trace does not
          * leave the confines of _container.  The returned value is an
          * ArrayList where each element is an Object[2] = { PortProxy (sink
          * port), MemberNodeProxy (owner member if exists, otherwise null.) }
    ArrayList traceChannelForwardToPorts(ALFINode _container,
                                                        String _channel_path) {
        ArrayList end_port_info_list = new ArrayList();
        GenericPortProxy sink = getSink();
        if (sink instanceof PortProxy) {
            MemberNodeProxy owner = getSourceMember();
            Object[] info = { sink, owner };
            end_port_info_list.add(info);
        }
        else if (sink instanceof JunctionProxy) {
            BundleProxy[] bundles =
                         (BundleProxy[]) sink.getConnections(_container, null);
            for (int i = 0; i < bundles.length; i++) {
                if (bundles[i] != this) {
                    end_port_info_list.addAll(
                         bundles[i].traceChannelForwardToPorts(_container,
                                                               _channel_path));
                }
            }
        }
        else if (sink instanceof BundlerProxy) {
            BundlerProxy bundler = (BundlerProxy) sink;
            BundleProxy primary_out = bundler.getPrimaryOutput(_container);
            BundleProxy primary_in = bundler.getPrimaryInput(_container);
            if (this == primary_in) {
                if (primary_out != null) {
                    end_port_info_list.addAll(
                        primary_out.traceChannelForwardToPorts(_container,
                                                               _channel_path));
                }

                ConnectionProxy secondary_out =
                                        bundler.getSecondaryOutput(_container);
                if (secondary_out != null) {
                    HashMap secondary_channel_name_map =
                            bundler.getSecondaryOutputChannelNames(_container);
                    for (Iterator i = secondary_channel_name_map.
                                          keySet().iterator(); i.hasNext(); ) {
                        if (_channel_path.startsWith((String) i.next())) {
                            end_port_info_list.addAll(secondary_out.
                                    traceChannelForwardToPorts(_container,
                                                               _channel_path));
                            break;
                        }
                    }
                }
            }
            else if (primary_out != null) {
                String new_path = (bundler.secondaryInputUnwraps(_container)) ?
                           _channel_path :
                           bundler.getSecondaryInputName(_container) + "." +
                                                                 _channel_path;
                end_port_info_list.addAll(primary_out.
                             traceChannelForwardToPorts(_container, new_path));
            }
        }

        return end_port_info_list;
    }
          */

        /** Bundles have no non-gui parser representation.  Returns null. */
    public String generateParserRepresentation(ALFINode _container) {
        return null;
    }

        /** Trivial override which just returns null. */
    public ConnectionProxy generateModelerSuitableConnection(ALFINode
                                                                  _container) {
        return null;
    }

    protected String generateAuxConfigInfoLines(String _indent) {
        String lines = _indent + Parser.BUNDLE__IS_BUNDLE;
        if (mB_isSourceBundlerPrimaryOutput.booleanValue()) {
            lines += _indent + Parser.BUNDLE__IS_BUNDLER_PRIMARY_OUTPUT;
        }
        if (mB_isSinkBundlerPrimaryInput.booleanValue()) {
            lines += _indent + Parser.BUNDLE__IS_BUNDLER_PRIMARY_INPUT;
        }
        return lines;
    }

        /** Overrides the ConnectionProxy method.  Complains and returns
          * { null, null, null }.  This is not a valid call to a bundles, only
          * to non-bundle connections.
          */
    public Object[] getGenesisPort(ALFINode _container) {
        Alfi.warn("BundleProxy.getGenesisPort(" + _container + "): " +
                  "This call is not valid for BundleProxy.");
        Object[] genesis_port_and_member = { null, null, null };
        return genesis_port_and_member;
    }
}
