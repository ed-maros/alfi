package edu.caltech.ligo.alfi.bookkeeper;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import edu.caltech.ligo.alfi.*;
import edu.caltech.ligo.alfi.tools.*;
import edu.caltech.ligo.alfi.file.*;


    /**
     * When a junction is added to a container node, a JunctionProxy object is
     * created and added to the container node's add_junction list.  This object
     * represents all the junctions which appear in the container node and all
     * nodes based on the container node.  The whole line of inherited nodes
     * share the source port/junction, location, etc of this JunctionProxy.
     * This object is similar to the MemberNodeProxy object in these matters.
     */
public class JunctionProxy extends GenericPortProxy 
                           implements GroupableIF,
                                      GroupElementModEventSource {

    //**************************************************************************
    //***** static fields ******************************************************



    //**************************************************************************
    //***** local exception classes ********************************************


    //**************************************************************************
    //***** member fields ******************************************************

        /** The location of the junction. */
    private Point m_location;

        /** Used in conjunction with GroupableIF interface. */
    protected LayeredGroup m_group_container;

        /** Used in conjuction with GroupElementModEventSource */
    private HashMap m_group_element_mod_listeners_map;
 
    //**************************************************************************
    //***** constructors *******************************************************

        /** Main constructor.  Should only be called from ALFINode. */
    JunctionProxy(ALFINode _container_node, String _name, Point _location) {
        super(_container_node, _name);

        if ((_name == null) || (_name.length() < 1)) {
            m_name = generateDefaultName(_container_node);
        }
        else { m_name = new String(_name); }

        m_location = _location;
        m_group_element_mod_listeners_map = new HashMap();
     }

        /**
         * Auxilliary constructor which creates a proxy for temporary storage
         * of JunctionProxy information only.  Thes objects are useless
         * elsewhere because the (final) container node field is null.
         */
    public JunctionProxy(Point _location, String _name) {
        super();

        m_name = _name;
        m_location = _location;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// class methods //////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    /////// implementations of abstract GenericPortProxy methods ///////////////

        /** Determines the data type which is output from this port as a source
          * for connection _connection.
          */
    public int determineSourceDataTypeForConnection(ALFINode _container,
                                                 ConnectionProxy _connection) {
        assert(_container != null);
        assert(_connection.getSource() == this);

        if (_connection instanceof BundleProxy) {
            return DATA_TYPE_ALFI_BUNDLE;
        }
        else {
            ConnectionProxy source_con = getSourceConnection(_container);
            if (source_con != null) {
                return source_con.determineUpstreamDataType(_container);
            }
            else { return DATA_TYPE_1_UNRESOLVED; }
        }
    }

        /** Determines the data type which is input to this port as a sink
          * for connection _connection.
          */
    public int determineSinkDataTypeForConnection(ALFINode _container,
                                                 ConnectionProxy _connection) {
        assert(_container != null);
        assert(_connection.getSink() == this);

        if (_connection instanceof BundleProxy) {
            return DATA_TYPE_ALFI_BUNDLE;
        }
        else {
            ConnectionProxy source_con = getSourceConnection(_container);
            ConnectionProxy[] connections = getConnections(_container, null);
            int data_type = DATA_TYPE_1_UNRESOLVED;
            for (int i = 0; i < connections.length; i++) {
                if (connections[i] == source_con) { continue; }

                int type =
                        connections[i].determineDownstreamDataType(_container);
                if (type != DATA_TYPE_1_UNRESOLVED) {
                    data_type = type;
                    if (data_type != DATA_TYPE_1_UNKNOWN) { break; }
                }
            }
            return data_type;
        }
    }

        /** Implementation of GenericPortProxy method.  The returned map will
          * contain { input_source_connection, _container } if it exists.
          * [_owner_member is ignored for junctions.]
          */
    protected HashMap getUpstreamConnections(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        HashMap container_map = new HashMap();

        ConnectionProxy source_connection = getSourceConnection(_container);
        if (source_connection != null) {
            container_map.put(source_connection, _container);
        }

        return container_map;
    }

        /** Implementation of GenericPortProxy method.  The returned map will
          * contain { output_connection_i, _container } key/value pairs.  There
          * may be multiple such output connections from this junction.
          * [_owner_member is ignored for junctions.]
          */
    protected HashMap getDownstreamConnections(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        HashMap container_map = new HashMap();

        ConnectionProxy source_connection = getSourceConnection(_container);
        ConnectionProxy[] all_connections = getConnections(_container, null);
        for (int i = 0; i < all_connections.length; i++) {
            if (all_connections[i] != source_connection) {
                container_map.put(all_connections[i], _container);
            }
        }

        return container_map;
    }

        /** Any connections to this junction in _container_node?
          * _owner_member should be null and is ignored otherwise for this
          * class.
          */
    public boolean hasConnections(ALFINode _container_node,
                                  MemberNodeProxy _owner_member) {
        assert(_owner_member == null);

        return hasConnections(_container_node);
    }

        /** Returns an array of connections corresponding to each connection
          * (local or no) connected to this port in _container_node.
          */
    public ConnectionProxy[] getConnections(ALFINode _container_node,
                                            MemberNodeProxy _owner_member) {
        assert(_owner_member == null);

        return getConnections(_container_node);
    }

        /** Create a default name for the junction. */
    protected String generateDefaultName(ALFINode _container_node) {
        JunctionProxy[] junctions = _container_node.junctions_get();
        String[] taken_names = new String[junctions.length];
        for (int i = 0; i < junctions.length; i++) {
            taken_names[i] = junctions[i].getName();
        }

        String root = "Jctn_";
        int n = 0;
        String name = "";
        boolean b_name_taken = true;
        while (b_name_taken) {
            name = root + n;
            b_name_taken = false;
            for (int i = 0; i < taken_names.length; i++) {
                if (name.equals(taken_names[i])) {
                    b_name_taken = true;
                    n++;
                    break;
                }
            }
        }
        return name;
    }

        /** Creates a short and long (possibly multi-line) text description of
          * itself for use in status text and elsewhere.  This is a pretty
          * version and is not intended as something that may be parsed.
          * So it may be changed about without fear of breaking box files.
          */
    public String generateSnippetForConnectionDescription(ALFINode _container,
                        ConnectionProxy _connection, boolean _b_long_version) {
        assert(_container != null);
        assert(_connection != null);
        assert(_container.connection_contains(_connection));

        GenericPortProxy source = _connection.getSource();
        GenericPortProxy sink = _connection.getSink();
        assert((this == source) || (this == sink));

        StringBuffer sb = new StringBuffer(getName());

        if (this == source) {
            GenericPortProxy origin = _connection.getOrigin(_container);
            if (origin != null) {
                sb.append(" (Origin: ");
                if (origin instanceof PortProxy) {
                    MemberNodeProxy origin_member =
                                       _connection.getOriginMember(_container);
                    ConnectionProxy connection_from_origin =
                           origin.getConnections(_container, origin_member)[0];
                    sb.append(origin.generateSnippetForConnectionDescription(
                                   _container, connection_from_origin, false));
                }
                else if (origin instanceof BundlerProxy) {
                    sb.append("(Bundler) " + origin.getName());
                }
                sb.append(") ");
            }

        }

        return sb.toString();
    }

        /** Merely propogates update signal to downstream connections. */
    public void updateContentAndPropogateDownstream(ALFINode _container,
                                               MemberNodeProxy _owner_member) {
        ConnectionProxy source_connection = getSourceConnection(_container);
        ConnectionProxy[] connections = getConnections(_container, null);
        for (int i = 0; i < connections.length; i++) {
            if (connections[i] != source_connection) {
                connections[i].updateContentAndPropogateDownstream(_container);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// general methods ////////////////////////////////////////////////////

    public Point getLocation() { return new Point(m_location); }

    /** Sets the location of the junction in it's container at the
         * closest grid point to _new_location (i.e., auto snaps to grid.)
         */
    public boolean setLocation(Point _new_location) {
        return setLocation(_new_location, true);
    }

       /** Sets the location of the junction in it's container at the
         * closest grid point to _new_location if _b_snap_to_grid is true
         * (else exactly to the point _new_location (rarely if ever used.))
         *
         * Set private unless and until non-snap-to-grid functionality needed.
         */
    private boolean setLocation(Point _new_location, boolean _b_snap_to_grid) {
        if (! _new_location.equals(m_location)) {
            m_location = (_b_snap_to_grid) ?
                             GridTool.getNearestGridPoint(_new_location) :
                             new Point(_new_location);
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                                        new JunctionProxy(m_location, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent ceme = new ContainedElementModEvent(this);
            fireContainedElementModEvent(ceme);

            return true;
        }
        else { return false; }
    }

        /**
         * Returns the connection connecting this junction to its source
         * port/junction (if such exists, otherwise returns null.)
         */
    public ConnectionProxy getSourceConnection(ALFINode _container_node) {
        ConnectionProxy[] connections = _container_node.connections_get();
        for (int i = 0; i < connections.length; i++) {
            if (connections[i].getSink() == this) { return connections[i]; }
        }
        return null;
    }

        /** Creates a string suitable for describing this junction in file. */
    public String generateParserRepresentation() {
        return (m_name + " " + m_location.x + "x" + m_location.y);
    }

    public String toString() { return getName(); }

        /**
         * Gets the network of junctions connected to this one (directly, or
         * indirectly through 1 or more other junctions in the network.)
         */
    public JunctionProxy[] determineJunctionNetwork(ALFINode _container_node) {
        JunctionProxy[][] all_networks =
                            _container_node.junction_getAllJunctionNetworks();
        for (int i = 0; i < all_networks.length; i++) {
            for (int j = 0; j < all_networks[i].length; j++) {
                if (all_networks[i][j] == this) {
                    return all_networks[i];
                }
            }
        }

        Alfi.warn("(" + this + ").determineJunctionNetwork(" + _container_node +
            "):\n\n\tThis junction not found in any network in this node.");
        return null;
    }

        /** This method traces all connections in every direction, through all
          * intermediary junctions, and makes a list of all BundlerProxys and
          * PortProxys attached directly or indirectly to this junction.
          */
    public Object[][] findAllTerminalPorts(ALFINode _container_node) {
        JunctionProxy[] network_junctions =
                                    determineJunctionNetwork(_container_node);
        ConnectionProxy[] connections = _container_node.connections_get();
        ArrayList terminal_list = new ArrayList();
        for (int i = 0; i < connections.length; i++) {
            GenericPortProxy source = connections[i].getSource();
            GenericPortProxy sink = connections[i].getSink();
            for (int j = 0; j < network_junctions.length; j++) {
                if ((source == network_junctions[j]) &&
                                       (! (sink instanceof JunctionProxy))) {
                    Object[] terminal_and_connection = { sink, connections[i] };
                    terminal_list.add(terminal_and_connection);
                    break;
                }
                else if ((sink == network_junctions[j]) &&
                                       (! (source instanceof JunctionProxy))) {
                    Object[] terminal_and_connection = {source, connections[i]};
                    terminal_list.add(terminal_and_connection);
                    break;
                }
            }
        }

        Object[][] pairs = new Object[terminal_list.size()][];
        terminal_list.toArray(pairs);
        return pairs;
    }

    //////////////////////////////////////////////////////
    ////////// GroupableIF Methods ///////////////////////

        /** GroupableIF method */
    public LayeredGroup group_get() {return m_group_container; };

        /** GroupableIF method */
    public void group_set(LayeredGroup _group) {
        m_group_container = _group;

        if (m_group_container == null) {
            m_group_element_mod_listeners_map.clear();
        }
    }
        /** GroupableIF method */
    public String getElementName() { return getName(); }


         /** GroupableIF method */
    public void group_deleteElement( ) {
    }

        /** GroupableIF method */
    public void group_moveElement( ) {
    }
        /** GroupableIF method */
    public void group_registerListener(
                                      GroupElementModEventListener _listener) {
        addGroupElementModListener(_listener);
    }

    /////////////////////////////////////////////////////////////////
    ////////// GroupElementModEventSource methods ///////////////

        /** GroupElementModEventSource method. */
    public void addGroupElementModListener(
                        GroupElementModEventListener _listener) {
        m_group_element_mod_listeners_map.put(_listener, null);
    }

        /** GroupElementModEventSource method. */
    public void removeGroupElementModListener(
                        GroupElementModEventListener _listener) {
        m_group_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with GroupElementModEventSource. */
    public void fireGroupElementModEvent(GroupElementModEvent e) {
        for (Iterator i =
                m_group_element_mod_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((GroupElementModEventListener) i.next()).groupElementModified(e);
        }
    }

}
