package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.tools.*;


    /** A LinkSetting is a local change to the value of an inherited
      * LinkDeclaration.  Only root boxes may contain LinkDeclarations, and
      * instances may only contain LinkSettings associated with the root node's
      * declarations.
      */
public class LinkSetting implements NodeSettingIF {

    //**************************************************************************
    //***** constants **********************************************************


    //**************************************************************************
    //***** member fields ******************************************************

    final private LinkDeclaration m_associated_declaration;
    final private String m_value;
    final private boolean mb_is_quoted;
    final private String m_comment;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    LinkSetting(LinkDeclaration _associated_declaration, String _new_value,
                                       String _comment, boolean _b_is_quoted) {

            // final members
        m_associated_declaration = _associated_declaration;
        m_value = _new_value;
        mb_is_quoted = _b_is_quoted;
        m_comment = _comment;
    }

    //**************************************************************************
    //***** static methods *****************************************************

        /** Expects a string argument formatted as:
          *
          *     name
          *     {
          *     value line 1
          *     value line 2
          *     value line n
          *     }
          *
          * Returns a String[] = { name, value }
          * unless an error occurs.
          *
          * On error, the return value is String[1] = { error_msg }.
          */
    public static String[] parse(String[] _lines) {
        String name = _lines[0].trim();
        if (! name.matches("^[\\w\\.]+$")) {
            String error_msg = "LinkSetting.parse(): Invalid name in\n\n";
            for (int i = 0; i < _lines.length; i++) {
                error_msg += "    " + _lines[i] + "\n";
            }
            String[] error_response = { error_msg };
            return error_response;
        }

        if (_lines[1].trim().equals(Parser.SECTION_BEGIN_MARKER) &&
                              _lines[_lines.length - 1].trim().equals(
                                                  Parser.SECTION_END_MARKER)) {
            String value = new String();
            for (int i = 2; i < _lines.length - 1; i++) {
                value += _lines[i];
                if (i < _lines.length - 2) { value += Parser.NL; }
            }

            String[] name_and_value = { name, value };
            return name_and_value;
        }
        else { return null; }
    }

    //**************************************************************************
    //***** methods ************************************************************

    /////////////////////////////////
    // member field access //////////

    public LinkDeclaration getAssociatedDeclaration() {
        return m_associated_declaration;
    }

    public String determineName() { return m_associated_declaration.getName(); }

    public String determineType() { return m_associated_declaration.getType(); }

    public String generateParserRepresentation(String _starting_spacer) {
        String full_spacer = _starting_spacer + Parser.STANDARD_SPACER;

        String rep = _starting_spacer + determineName() + Parser.NL;
        rep += _starting_spacer + Parser.SECTION_BEGIN_MARKER + Parser.NL;

        String[] value_lines = m_value.split(Parser.NL);
        for (int i = 0; i < value_lines.length; i++) {
            rep += full_spacer + value_lines[i] + Parser.NL;
        }
        rep += _starting_spacer + Parser.SECTION_END_MARKER;

        return rep;
    }

    public String toString() {
        return "LinkSetting: " + determineName() + " = " + m_value;
    }

    /////////////////////////////////
    // NodeSettingIF methods ////////

    public String getName() { return determineName(); }

    public String getValue() { return m_value; }

    public boolean isQuoted() { return mb_is_quoted; }

    public String getType() { return determineType(); }

    public String getComment() { return m_comment; }
}
