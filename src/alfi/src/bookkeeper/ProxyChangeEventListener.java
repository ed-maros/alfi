package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;


    /**
     * The interface for objects who wish to register as listeners for
     * ProxyChangeEvents.
     */
public interface ProxyChangeEventListener extends EventListener {
    public void proxyChangePerformed(ProxyChangeEvent e);
}
