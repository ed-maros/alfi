package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /**
     * This event is created by an ALFINode on a change to its external
     * interface.  In most (if not all) cases, this means a port has been added
     * or deleted.  Other nodes containing instances of the source node may
     * need to know about this in case of now invalid connections, etc.
     */
public class NodeInterfaceChangeEvent extends EventObject {

    //**************************************************************************
    //***** constants **********************************************************

        // actions
    public final static int ELEMENT_WILL_BE_ADDED    = 0;
    public final static int ELEMENT_HAS_BEEN_ADDED   = 1;
    public final static int ELEMENT_WILL_BE_REMOVED  = 2;
    public final static int ELEMENT_HAS_BEEN_REMOVED = 3;

    private final static String[] ACTION_DESCRIPTIONS =
        {"to be added", "has been added", "to be removed", "has been removed"};

    //**************************************************************************
    //***** fields *************************************************************

         /** Element associated with the action. */
    private final Object m_associated_element;

         /** Action on the associated element. */
    private final int m_action;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public NodeInterfaceChangeEvent(Object _source, Object _associated_element,
                                                                  int _action) {
        super(_source);
        m_associated_element = _associated_element;
        m_action = _action;
    }

    //**************************************************************************
    //***** methods ************************************************************

    /////////////////////////////////
    // member field access //////////

    public int getAction() { return m_action; }

    public Object getAssociatedElement() { return m_associated_element; }

    public String toString() {
        return "NodeInterfaceChangeEvent[" + getSource() + ", " +
               m_associated_element + ", " +
               ACTION_DESCRIPTIONS[m_action] + "]";
    }
}
