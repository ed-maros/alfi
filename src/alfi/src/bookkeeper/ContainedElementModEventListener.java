package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /**
     * The interface for objects who wish to register as listeners for
     * ContainedElementModEvents.
     *
     * See the code for an skeleton of containedElementModified().
     */
public interface ContainedElementModEventListener extends EventListener {
    public void containedElementModified(ContainedElementModEvent e);
}

/*

Here is a skeleton for getting information from a ContainedElementModEvent
as fired from contained element (PortProxy, ALFINode, MemberNodeProxy, etc.):

    public void containedElementModified(ContainedElementModEvent e) {
        Object source = e.getSource();    // that's all right now...
    }
*/
