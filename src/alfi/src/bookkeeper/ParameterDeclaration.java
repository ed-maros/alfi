package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.tools.*;


    /**
     * This class represents parameters which are used to define the behavior
     * of primitives.  Parameter declarations can only be added to root
     * primitive nodes.
     */
public class ParameterDeclaration implements NodeSettingDeclarationIF {

    //**************************************************************************
    //***** constants **********************************************************

    public static final String TYPE_REAL           = "real";
    public static final String TYPE_INTEGER        = "integer";
    public static final String TYPE_STRING         = "string";
    public static final String TYPE_BOOLEAN        = "boolean";
    public static final String TYPE_COMPLEX        = "complex";
    public static final String TYPE_SIZE           = "size_t";
    public static final String TYPE_VECTOR_INTEGER = "vector_integer";
    public static final String TYPE_VECTOR_REAL    = "vector_real";
    public static final String TYPE_VECTOR_COMPLEX = "vector_complex";
    public static final String TYPE_MATRIX_REAL    = "matrix_real";
    public static final String TYPE_MATRIX_COMPLEX = "matrix_complex";

    public static final String[] PARAMETER_TYPES = { TYPE_REAL, TYPE_INTEGER,
        TYPE_STRING, TYPE_BOOLEAN, TYPE_COMPLEX, TYPE_SIZE,
        TYPE_VECTOR_INTEGER, TYPE_VECTOR_REAL, TYPE_VECTOR_COMPLEX,
        TYPE_MATRIX_REAL, TYPE_MATRIX_COMPLEX };

    public static final String FUNC_X_SHOW_INSTANCE_FLAG="showInstanceSettings";
    public static final String FILE_INCLUDE = Parser.FILE_INCLUDE_MARKER;
 
    //**************************************************************************
    //***** class methods ******************************************************

        /**
         * Class method which checks the passed string against all possible
         * types for parameters.
         */
    public static boolean typeIsValid(String _type) {
        for (int i = 0; i < PARAMETER_TYPES.length; i++) {
            if (_type.equals(PARAMETER_TYPES[i])) { return true; }
        }
        return false;
    }

        /** Parses a line which it expects to be in the format:
          *
          * type name = value
          *
          * or
          *
          * type name                 // sets value to ""
          *
          * Returns String[3] = { type, name, value }.  Returns null if
          * _declaration_string is not in the correct format.
          */
    public static String[] parse(String _declaration_string) {
        String type = null;
        String name = null;
        String value = null;

        String type_and_name = null;
        int index = _declaration_string.indexOf("=");
        if (index > 0) {
            type_and_name = _declaration_string.substring(0, index).trim();
            value = _declaration_string.substring(index + 1).trim();
        }
        else {
            type_and_name = _declaration_string;
            value = "";
        }

        String[] parts = type_and_name.split("\\s+");
        if (parts.length == 2) {
            type = parts[0];
            name = parts[1];

            if (typeIsValid(type)) {
                return new String[] { type, name, value };
            }
                // temporary code to handle the deprecated case of the "bool"
                // type string (as opposed to the string "boolean").  see also
                // code in Parser.slotSectionsIntoVectorHolders() to remove
                // when this code becomes obsolete.
            else if (type.equals("bool")) {
                return new String[] { "boolean", name, value };
            }
        }

        return null;
    }

    //**************************************************************************
    //***** member fields ******************************************************

        /**
         * Parameter name.
         */
    private final String m_name;

        /**
         * Parameter type.  Can be TYPE_REAL, TYPE_INTEGER, TYPE_STRING,
         * TYPE_BOOLEAN, TYPE_COMPLEX, TYPE_SIZE, TYPE_VECTOR_REAL,
         * TYPE_VECTOR_COMPLEX
         */
    private final String m_type;

        /**
         * The value of the parameter.
         */
    private String m_value;

        /**
         * An optional comment associated with this parameter.
         */
    private final String m_comment;

        /**
         * An optional flag associated with this parameter.
         * This parameter is not declared in the root primitive.  It is defined
         * only for that instance.  This is used mainly with FUNC_X primitives.
         */
    private final boolean mb_instance_parameter;

    //**************************************************************************
    //***** constructors *******************************************************

        /**
         * Constructor.  With comment.  Only ALFINode should ever construct
         * ParameterDeclarations.  Also now created by PrimitiveTemplates.
         */
    ParameterDeclaration(String _type, String _name, String _value,
                                String _comment, boolean _instance_parameter ) {

        mb_instance_parameter = _instance_parameter;

        // An instance_parameter does not require validation.
        if (mb_instance_parameter || (nameAndTypeAreValid(_type, _name))) {
            m_name = _name.trim();
            m_type = _type.trim();
            if (_value != null) { m_value = _value.trim(); }
            else { m_value = ""; }
            if (_comment != null) { m_comment = _comment.trim(); }
            else { m_comment = ""; }
        }
        else {
            m_type = null;
            m_name = null;
            m_value = null;
            m_comment = null;
        }
    }

        /**
         * Constructor.  Only ALFINode should ever construct
         * ParameterDeclarations.  Also now created by PrimitiveTemplates.
         */
    public ParameterDeclaration(String _type, String _name, String _value) {
        this(_type, _name, _value, "", false);
    }

        /**
         * Used by the constructor.
         */
    private boolean nameAndTypeAreValid(String _type, String _name) {
        boolean b_name_and_type_are_valid = true;

            // if needed
        String error_message =
            "ParameterDeclaration.passed_parameters_are_valid(" + _type + ", " +
            _name + ") :\n";

        if (! typeIsValid(_type) ) {
            error_message += "\tERROR: Invalid type passed.\n";
            b_name_and_type_are_valid = false;
        }
        if (_name.length() == 0) {
            error_message += "\tERROR: Blank name for parameter not allowed.\n";
            b_name_and_type_are_valid = false;
        }
        if (b_name_and_type_are_valid) { return true; }
        else {
            System.err.print(error_message);
            return false;
        }
    }

    //**************************************************************************
    //***** methods ************************************************************

    /////////////////////////////////
    // member field access //////////

    public String getType() { return m_type; }

    public String getName() { return m_name; }

    public String getValue() { return m_value; }

    public void setValue(String _value) { 
        m_value = _value; 
    }

    public String getComment() { return m_comment; }

    public boolean isQuoted() { return true; }

    public boolean isInstanceParameter() { return mb_instance_parameter; }


    public String toString() { return toString(false); }

    public String toString(boolean _b_verbose) {

        String info = "";

        if (_b_verbose) {
            info += "ParameterDeclaration: (" ;
        }
        
        info += m_type + "  " + m_name;

        if (m_value != null && m_value.length() > 0) {
         info += " = " + m_value; 
        }

        if (_b_verbose) {
            info += ")";
        }


        return info;
    }

        /** Write the object in its parser representation. */
    public String generateParserRepresentation(String _spacer) {
        StringBuffer sb = new StringBuffer(_spacer);
        
        if (!isInstanceParameter()) {

            // use quote markers any multi-lined and/or string value
        boolean b_quote = ((m_value != null) && (m_value.length() > 0)) &&
                  (m_type.equals(TYPE_STRING) || (m_value.indexOf('\n') >= 0));
        if (b_quote) {
            sb.append(Parser.QUOTE_BEGIN);
            sb.append(Parser.NL);
        }

        sb.append(m_type);
        sb.append(" ");
        sb.append(m_name);
        sb.append(" = ");

        if (m_value != null) {
            sb.append(m_value);
            if (b_quote) {
                sb.append(Parser.NL);
                sb.append(_spacer);
                sb.append(Parser.QUOTE_END);
            }
        }
        }
        
        return sb.toString();
    }
}
