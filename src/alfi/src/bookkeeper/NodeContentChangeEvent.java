package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /**
     * This event is created by an ALFINode after a call to one of its
     * add/remove element lists (e.g., junction_remove(), and is fired off
     * to interested listeners, such as an open edit window of the firing
     * node, or any ALFINodes directly derived from the firing node.  Such
     * nodes may decide to pass the event on to their directly derived nodes
     * as well.
     */
public class NodeContentChangeEvent extends EventObject {

    //**************************************************************************
    //***** constants **********************************************************

        // action codes
    public static final int MEMBER_NODE_DELETE = 0;
    public static final int MEMBER_NODE_ADD    = 1;
    public static final int CONNECTION_DELETE  = 2;
    public static final int CONNECTION_ADD     = 3;
    public static final int PORT_DELETE        = 4;
    public static final int PORT_ADD           = 5;
    public static final int JUNCTION_DELETE    = 6;
    public static final int JUNCTION_ADD       = 7;
    public static final int BUNDLER_DELETE     = 8;
    public static final int BUNDLER_ADD        = 9;
    public static final int TEXT_COMMENT_ADD   = 10;
    public static final int TEXT_COMMENT_DELETE= 11;
    public static final int COLORED_BOX_ADD    = 12;
    public static final int COLORED_BOX_DELETE = 13;
    public static final int LAYERED_GROUP_ADD  = 14;
    public static final int LAYERED_GROUP_REMOVE = 15;

    //**************************************************************************
    //***** fields *************************************************************

         /** Action which has taken place.  See action codes. */
     private final int m_action_code;

         /** Element associated with the action. */
     private final Object m_associated_element;

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public NodeContentChangeEvent(Object _source, int _action_code,
                                                  Object _associated_element) {
        super(_source);
        m_action_code = _action_code;
        m_associated_element = _associated_element;
    }

    //**************************************************************************
    //***** methods ************************************************************

    /////////////////////////////////
    // member field access //////////

    public int getAction() { return m_action_code; }

    public Object getAssociatedElement() { return m_associated_element; }

    public String toString() {
        return "NodeContentChangeEvent[" + getSource() + ", " + m_action_code +
               ", " + m_associated_element + "]";
    }
}
