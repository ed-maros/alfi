
        /** This method returns a String[4]:
          *   [0] = the local value* of this parameter or null.  (* see note)
          *   [1] = if [0] is parsed from an @@INCLUDE, the filename. else null.
          *   [2] = the default value* of this parameter or null. (* see note)
          *   [3] = name of the node where default was found or null.
          *
          *     * important note: if the true value of a parameter is an
          *       @@INCLUDE, then the content of the @@INCLUDE file is parsed to
          *       get the value from inside the @@INCLUDE file.  i.e., [0] and
          *       [2]  will never return the strings like "@@INCLUDE x.cc".
          */       
    protected String[] normalParameter_getInfo(String _parameter_name) {
    }


        /** Returns a list of String[5] elements defined as:
          *   [0] = the type of this parameter
          *   [1] = the name of this parameter
          *   [2] = the local value of this parameter or null if unchanged from
          *         setting in last MEMBER_DECL.
          *   [3] = the default value of this parameter or null if none found.
          *   [4] = the name of the node where [3] was found.
          */
    protected ArrayList endUserParameters_getInfo() {
    }


        /** If _end_user_parameter_name exists, returns a String[4]:
          *   [0] = the type of this parameter
          *   [1] = the local value of this parameter or null if unchanged from
          *         setting in last MEMBER_DECL definition.
          *   [2] = the default value of this parameter or null if none found.
          *   [3] = the name of the node where [2] was found.
          *
          * Basically this is the same information which is gotten from
          * endUserParameters_getInfo() excepth that this is for a single
          * parameter of known name.
          *
          * If _end_user_parameter_name is not recognized, null is returned.
          */
    protected String[] endUserParameters_getDetails(String 
                                                    _end_user_parameter_name) {

 
    private boolean determineInstanceParameterSetting() 
