package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.bookkeeper.*;

/** This interface defines fields and methods used by
   * NodeToBeDestroyedEvent sources.
   */
public interface NodeToBeDestroyedEventSource {
    public void addNodeToBeDestroyedEventListener(
                        NodeToBeDestroyedEventListener _listener);
    public void removeNodeToBeDestroyedEventListener(
                        NodeToBeDestroyedEventListener _listener);
}

