package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

    /** This event is created by ConnectionProxys (including BundleProxys) and
      * BundlerProxys when changes occur which may affect the current downstream
      * state of data channels reliant on these objects.
      */
public class UpstreamDataChannelChangeEvent extends EventObject {

    //**************************************************************************
    //***** constants **********************************************************

        // action codes
    // public static final int NOT_CURRENTLY USED = 0;

    //**************************************************************************
    //***** fields *************************************************************

    // none

    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor. */
    public UpstreamDataChannelChangeEvent(Object _source) {
        super(_source);
    }

    //**************************************************************************
    //***** methods ************************************************************

    public String toString() {
        return "UpstreamDataChannelChangeEvent[" + getSource() + "]";
    }
}
