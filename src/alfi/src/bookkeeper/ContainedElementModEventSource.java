package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.bookkeeper.*;

/**
  * This interface defines fields and methods used by sources of 
  * ContainedElementModEvent sources.
  */
public interface ContainedElementModEventSource {
    public void addContainedElementModListener(
                        ContainedElementModEventListener _listener);
    public void removeContainedElementModListener(
                        ContainedElementModEventListener _listener);
}

