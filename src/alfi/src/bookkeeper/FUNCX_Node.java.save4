package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;
import java.lang.Math;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;

public class FUNCX_Node extends ALFINode {

    public static final String FUNC_X_MEMBER_DECL = "MemberDecl";
    public static final String FUNC_X_MEMBER_VAR_START = "//FUNC_X_MEMBER_VAR_START";
    public static final String FUNC_X_MEMBER_VAR_END = "//FUNC_X_MEMBER_VAR_END";
    public static final String FUNC_X_MEMBER_INIT = "//**FUNC_X_INIT_VALUE";
    public static final String FUNC_X_CONSTRUCTOR = "Constructor";
    public static final String FUNC_X_CTOR_INIT_START = "//FUNC_X_MEMBER_INIT_START";
    public static final String FUNC_X_CTOR_INIT_END = "//FUNC_X_MEMBER_INIT_END";
    public static final String FUNC_X_WARNING_NO_EDIT = new String(
        "//The following code segment is generated automatically.  DO NOT EDIT" );
 
    private ArrayList m_funcx_param_decl_list;

        /**
         * ParameterDeclaration objects list.  Unlike normal ALFINode where 
         * only the primitive nodes can add parameter declarations,
         * func_x instance nodes can add parameters.
         */
    private ArrayList m_funcx_value_mod_list;

    private boolean m_instance_parameters_only;

    private int m_removed_funcx_param_index;


    //**************************************************************************
    //***** constructors *******************************************************

        /** Constructor for creating a root node only. */
    public FUNCX_Node( ALFIFile _root_node_file ) {
        super(_root_node_file);
    } 

        /** Constructor for creating a member node (i.e., not a root node.) */
    public FUNCX_Node(ALFINode _base_node, ALFINode _container_node,
                                  MemberNodeProxy _associated_MemberNodeProxy) {
        super(_base_node, _container_node, _associated_MemberNodeProxy);
    }

    //**************************************************************************
    //***** public methods *****************************************************

    ////////////////////////////////////////////
    // base and derived nodes methods //////////

    public ALFINode createNewNode( ALFINode _container_node,
                          MemberNodeProxy _associated_MemberNodeProxy) {
        FUNCX_Node new_member_node = new FUNCX_Node(this, 
                          _container_node, _associated_MemberNodeProxy);
        return (ALFINode) new_member_node;
    }

    public void initialize( ) {

        boolean instance_params_only = false; 
        ALFINode node = this;
        instance_params_only = determineInstanceParameterSetting(node);
        if (node instanceof FUNCX_Node) {   
            FUNCX_Node funcx_node = (FUNCX_Node) node;
            funcx_node.setInstanceParametersOnly(instance_params_only);
            funcx_node.initializeLocalFuncXVariable(); 
        }

        m_removed_funcx_param_index = -1;
    }


    private boolean determineInstanceParameterSetting( ALFINode _node) {
        boolean instance_params_only = false;

        ALFINode node = this;
            
        while (node != null) {
            if (node instanceof FUNCX_Node){
                FUNCX_Node funcx_node = (FUNCX_Node) node;
                ParameterValueModification[] pvm_locals = 
                    funcx_node.parameterValueMods_getLocal();

                for (int ii = 0; ii < pvm_locals.length; ii++) {
                    if (pvm_locals[ii].getName().equals (
                        ParameterDeclaration.FUNC_X_SHOW_INSTANCE_FLAG)) {
                        if (pvm_locals[ii].getValue().equals("true"))
                            return true;
                        else 
                            return false; 
                    }
                }
            }
            node = node.baseNode_getDirect();
        }

        return instance_params_only;
    }



        /**
         * Parse the parameter parameter declaration based on the
         * MemberDecl value
         */
    public void processSpecialVariables( String _value  ) {

        String name;
        String type;
        String default_value;
        int start_location = _value.indexOf(FUNC_X_MEMBER_VAR_START, 0);

        if (start_location >= 0) {
            start_location += FUNC_X_MEMBER_VAR_START.length();

            int end_location = _value.indexOf(FUNC_X_MEMBER_VAR_END, 0);

            String variable_section = 
                _value.substring(start_location, end_location);
                
            StringTokenizer line_token = new StringTokenizer(variable_section, "\n");
            String line;

            // Parse the value line by line
            while (line_token.hasMoreTokens()) {

                name = "";
                type = "";
                default_value = "";
                line = line_token.nextToken();
            
                StringTokenizer tokens = new StringTokenizer(line);
                //Parse the type
                if (tokens.hasMoreTokens()) {
                    type = tokens.nextToken();
                }

                // Parse the variable name
                if (tokens.hasMoreTokens()) {
                    // Remove semicolon at the end
                    StringBuffer name_buffer = 
                        new StringBuffer(tokens.nextToken());
                    int semicolon_index = name_buffer.indexOf(";"); 
                    if (semicolon_index >= 0) {
                        name_buffer.delete(semicolon_index, 
                            name_buffer.length());
                    }
                    name = name_buffer.toString();
                    name.trim();
                }
                    
                // Parse the default value
                StringBuffer def_val_search = new StringBuffer(line); 
                int default_val_comment_index = def_val_search.indexOf
                    (FUNC_X_MEMBER_INIT);
                if (default_val_comment_index >= 0) {
                    int start = default_val_comment_index 
                              + FUNC_X_MEMBER_INIT.length();
                    int end = def_val_search.indexOf("\n");
                    // If can't find the end-of-line character, 
                    // just get the end of the line
                    if (end < 0) {
                        end = line.length() - 1;
                    }
                    default_value = def_val_search.substring(start, end);
                    default_value = default_value.trim();
                }
                    
                parameterDeclaration_addSpecialVariables(type, name,
                                                         default_value); 
                parameterValueMod_addSpecialVariables(name, 
                                                         default_value); 
            }
        }
    }

    public void cleanUp( ) {
        clearLists();
    }

    ///////////////////////////////////////////
    // parameter declaration methods //////////

    public void initializeLocalFuncXVariable ( ) {
        clearLists();

        if (m_instance_parameters_only) {
            ALFINode node = this;
            while (node != null) {
                
                if (node.parameterValueMod_containsLocal(
                                                       FUNC_X_MEMBER_DECL)) {
                    if (node instanceof FUNCX_Node) {
                    FUNCX_Node funcx_node = (FUNCX_Node) node;
                    ParameterValueModification[] pvm_locals = 
                        funcx_node.parameterValueMods_getLocal();

                    for (int ii = 0; ii < pvm_locals.length; ii++) {
                        if (pvm_locals[ii].getName().equals (
                                FUNC_X_MEMBER_DECL)) {
                            funcx_node.processSpecialVariables(pvm_locals[ii].getValue());
                            return;
                        }
                    }
                    }
                }
                node = node.baseNode_getDirect();
            }
        }
    }
        /**
         * Add a parameter declaration to this root primitive.  Include a
         * comment.
         */
    public boolean parameterDeclaration_contains(String _parameter_name) {
        if  (baseNode_getRoot().parameterDeclaration_containsLocal(
                                                               _parameter_name))  
        { return true; }
        else if (parameterDeclaration_containsLocalFuncXVariable(
                                                               _parameter_name)) 
        { return true; }
        else if (parameterDeclaration_containsFuncXVariable(_parameter_name)) 
        { return true; }
        else 
        { return false; }

    }

    public boolean parameterDeclaration_containsLocal(String _parameter_name) {
        ParameterDeclaration[] pd_array = parameterDeclarations_getLocal();
        for (int i = 0; i < pd_array.length; i++) {
            if ( pd_array[i].getName().equals(_parameter_name) )
            { return true; }
        }
        return false;
    }

    public boolean parameterDeclaration_containsLocalFuncXVariable(
                                               String _parameter_name) {
        ParameterDeclaration[] pd_array = parameterDeclarations_getLocal();
        for (int i = 0; i < pd_array.length; i++) {
            if ( ( pd_array[i].getName().equals(_parameter_name) &&
                 (pd_array[i].isInstanceParameter()))) 
            { return true; }
        }
        return false;
    }

    public ParameterDeclaration parameterDeclaration_getFuncXVariable(
                                               String _parameter_name) {

        ALFINode node = this;
        while (node != null) {
            if (node instanceof FUNCX_Node) {
                FUNCX_Node funcx_node = (FUNCX_Node) node;
                if (funcx_node.m_funcx_param_decl_list != null)  {
                    if (funcx_node.m_funcx_param_decl_list.size() > 0) {
                        ParameterDeclaration[] pd_array =
                            new ParameterDeclaration[funcx_node.m_funcx_param_decl_list.size()];
                        funcx_node.m_funcx_param_decl_list.toArray(pd_array);
                        for (int ii = 0; ii < pd_array.length; ii++) {
                            if ( pd_array[ii].getName().equals(_parameter_name)) {
                                return pd_array[ii];
                            }
                        }
                    }
                }
            }
            node = node.baseNode_getDirect();
        }
        return null;
        
    }

    public boolean parameterDeclaration_containsFuncXVariable(
                                               String _parameter_name) {
        ALFINode node = this;
        while (node != null) {
            if (node instanceof FUNCX_Node) {
                FUNCX_Node funcx_node = (FUNCX_Node) node;
                if (funcx_node.parameterDeclaration_containsLocalFuncXVariable
                                                    (_parameter_name)) 
                { return true; }
            }
            node = node.baseNode_getDirect();
        }
        return false;
        
    }

    public void parameterDeclaration_addSpecialVariables
                 (String _type, String _parameter_name, String _value) {

        if (m_funcx_param_decl_list == null) {
             m_funcx_param_decl_list = new ArrayList();
        }

        ParameterDeclaration pd = new ParameterDeclaration(
            _type, _parameter_name, _value, "", true);
        m_funcx_param_decl_list.add(pd);
        setTimeLastModified();
    }

        /**
         * Returns an array of ParameterDeclaration objects which have been
         * added to the FUNCX_Node. 
         */
    public ParameterDeclaration[] parameterDeclarations_getLocal() {
        if (m_instance_parameters_only) {
            if (m_funcx_param_decl_list == null) {
                return new ParameterDeclaration[0];
            }
            else {  
                ParameterDeclaration[] pd_array =
                  new ParameterDeclaration[m_funcx_param_decl_list.size()];
                m_funcx_param_decl_list.toArray(pd_array);
                return pd_array;
             }
        }
        else {
            return super.parameterDeclarations_getLocal();
        }
    }


    //////////////////////////////////////////////////
    // parameter value modification methods //////////

    public boolean parameterValueMod_containsLocal(String _parameter_name) {
        ParameterValueModification[] pvm_array = parameterValueMods_getLocal();
        for (int i = 0; i < pvm_array.length; i++) {
            if (_parameter_name.equals(pvm_array[i].getName())) { return true; }
        }
        return false;
    }

        /**
         * Add a parameter value modification to this instance of a primitive.
         * Include a comment.
         *
         * [Comments not yet supported.]
         */
    public ParameterValueModification parameterValueMod_add(String _name,
                            String _value, String _comment, boolean _b_quoted) {

        boolean instance_parameter = false;
        if (parameterDeclaration_containsLocal(_name)) {
            instance_parameter = 
               parameterDeclaration_containsLocalFuncXVariable(_name);
        }
        else {
            ParameterDeclaration pd =
                parameterDeclaration_getFuncXVariable(_name);
            if (pd != null) { instance_parameter = true;}
        }
        

        if (!instance_parameter) {
            return parameterValueMod_addNormal(_name, _value, _comment, 
                        _b_quoted);
        }
        else {
            return parameterValueMod_addSpecialVariables(_name, _value);
        }

    }

    private ParameterValueModification parameterValueMod_addNormal(String _name,
                            String _value, String _comment, boolean _b_quoted) {

        if ((_name != null) && (_name.length() != 0)) {
                // remove any already existing local mod with this name
            if (parameterValueMod_containsLocal(_name)) {
                parameterValueMod_removeLocal(_name);
            }

            if (_value == null) { _value = ""; }
            if (_comment == null) { _comment = ""; }
        
            if (_name.equals(FUNC_X_MEMBER_DECL)) {
                processSpecialVariables(_value);
            }
    
            ParameterValueModification pvm =
                new ParameterValueModification(this, _name, _value, _b_quoted);
            if (pvm != null) {
                if (m_parameter_modification_list == null) {
                    m_parameter_modification_list = new ArrayList();
                }
                m_parameter_modification_list.add(pvm);
                setTimeLastModified();

                ContainedElementModEvent e = new ContainedElementModEvent(this);
                fireContainedElementModEvent(e);
            }
            return pvm;
        }
    else {
                // error conditions
            String error_message = generateFullNodePathName() +
                ".parameterValueMod_add(" + _name + ", " + _value +
                ", " + _comment + ", " + _b_quoted + ") :\n";
            if (_name == null) {
                error_message += "\tERROR: null parameter name passed.";
            }
            else if (_name.length() == 0) {
                error_message += "\tERROR: Zero length parameter name passed.";
            }
            System.err.println(error_message);
            return null;
        }
    }

        /**
         * This primitive instance contains an inherited or locally added
         * parameter change for the named parameter?
         */
    public void parameterValueMod_addAll ( ) {

        // No need to do anything if there are no instance parameters
        if (!isInstanceParametersOnly ()) { return; }
        
        ParameterValueModification[] pvm_array = parameterValueMods_getLocal();

        boolean modified_member_decl = false;
        boolean modified_constructor = false;

        for (int ii = 0; ii < pvm_array.length; ii++) {
            if (pvm_array[ii].getName().equals(FUNC_X_MEMBER_DECL)) {
                String value = pvm_array[ii].getValue();
                String comment = pvm_array[ii].getComment();
                boolean is_quoted = pvm_array[ii].isQuoted();
                String new_value = settingReplace_memberDecl(value);
                parameterValueMod_removeLocal(FUNC_X_MEMBER_DECL);
                parameterValueMod_add(FUNC_X_MEMBER_DECL, new_value,
                                        comment, is_quoted);
                modified_member_decl = true;
            }
            else if (pvm_array[ii].getName().equals(FUNC_X_CONSTRUCTOR)){
                String value = pvm_array[ii].getValue();
                String comment = pvm_array[ii].getComment();
                boolean is_quoted = pvm_array[ii].isQuoted();
                String new_value = settingReplace_constructor(value);
                parameterValueMod_removeLocal(FUNC_X_CONSTRUCTOR);
                parameterValueMod_add(FUNC_X_CONSTRUCTOR, new_value,
                                        comment, is_quoted);
                modified_constructor = true;
            }
        }

        
        if (!modified_member_decl) {
            String new_value = settingReplace_memberDecl(" ");
            parameterValueMod_add(FUNC_X_MEMBER_DECL, new_value, "", true);
        } 

        if (!modified_constructor) {
            String new_value = settingReplace_constructor(" ");
            parameterValueMod_add(FUNC_X_CONSTRUCTOR, new_value,"", true);
        } 

        clearLists();
        setTimeLastModified();

        ContainedElementModEvent e = new ContainedElementModEvent(this);
        fireContainedElementModEvent(e);
    }

    public ParameterValueModification parameterValueMod_addSpecialVariables 
                                              (String _name, String _value) {
        int index = -1;
        if ((_name != null) && (_name.length() != 0)) {
                // remove any already existing local mod with this name
            if (parameterValueMod_containsLocal(_name)) {
                index = parameterValueMod_removeSpecialVariable(_name);
            }
            else if (m_removed_funcx_param_index >= 0) {
                index = m_removed_funcx_param_index;
            }
            m_removed_funcx_param_index = -1;

            if (m_funcx_value_mod_list == null) {
                m_funcx_value_mod_list = new ArrayList();
            }

            FUNCX_ParameterValueModification pvm =
                new FUNCX_ParameterValueModification(this, _name, _value);

            if (pvm != null) {
                if (index >= 0) { 
                    m_funcx_value_mod_list.add(index, pvm); 
                } 
                else {
                    m_funcx_value_mod_list.add(pvm); 
                } 
                setTimeLastModified();

                ContainedElementModEvent e = new ContainedElementModEvent(this);
                fireContainedElementModEvent(e);
            }
            return pvm;
        }
        else {
                // error conditions
            String error_message = generateFullNodePathName() +
                ".parameterValueMod_addSpecialVariables(" + _name + ", "
                + _value + ") :\n";
            if (_name == null) {
                error_message += "\tERROR: null parameter name passed.";
            }
            else if (_name.length() == 0) {
                error_message += "\tERROR: Zero length parameter name passed.";
            }
            System.err.println(error_message);
            return null;
        }
    }


        /** 
          *
          */
    public ParameterValueModification[] parameterValueMods_get () {
        ArrayList pvm_list = new ArrayList();
        ALFINode node = this;
        while (node != null) {
            ArrayList local_pvm_list = node.m_parameter_modification_list;
            if (local_pvm_list != null) {
                pvm_list.addAll(local_pvm_list);
            }

            if (node instanceof FUNCX_Node) {
                FUNCX_Node funcx_node = (FUNCX_Node) node;
                ArrayList local_funcx_pvm_list = 
                    funcx_node.m_funcx_value_mod_list;

                if (local_funcx_pvm_list != null) {
                    pvm_list.addAll(local_funcx_pvm_list);
                }
            }
            node = node.baseNode_getDirect();
        }

        ParameterValueModification[] pvms =
                               new ParameterValueModification[pvm_list.size()];
        pvm_list.toArray(pvms);
        return pvms;
    }

        /** Returns an array of ParameterValueModification objects inherited by
          * this primitive instance.  Order returned is mods in the most recent
          * inheritance first, to those in the root base node last.
          *
          * This method is private because the results are not necessarily
          * intuitive.  A value mod for a particular name may indeed exist in a
          * base node, but if there is also a local mod to this same parameter,
          * then the inherited value is overridden, and is not really
          * "inherited" into this node.  
          */
    private ParameterValueModification[] parameterValueMods_getInherited () {
        ALFINode base = baseNode_getDirect();
        if (base != null) {
            if (base instanceof FUNCX_Node) {
                return ((FUNCX_Node) base).parameterValueMods_get();
            }
            else {
                return  base.parameterValueMods_get();
            }
        }
        else { return new ParameterValueModification[0]; }
    }

    public ParameterValueModification[] parameterValueMods_getLocal() {

        ArrayList pvm_list = new ArrayList();

        if (m_funcx_value_mod_list != null) {
            pvm_list.addAll(m_funcx_value_mod_list);
        }
            

        if (m_parameter_modification_list != null) {
                pvm_list.addAll(m_parameter_modification_list);
        }

        ParameterValueModification[] pvms =
                               new ParameterValueModification[pvm_list.size()];

        pvm_list.toArray(pvms);
        return pvms;
        
    }

        /** Returns an array of FUNCX_ParameterValueModification objects 
          * locally added to this instance.
          */
    public ParameterValueModification[] 
            parameterValueMods_getLocalFuncXVariable() {

        ArrayList pvm_list = new ArrayList();

        if (m_funcx_value_mod_list != null) {
            pvm_list.addAll(m_funcx_value_mod_list);
        }
        
        ParameterValueModification[] pvms =
                               new ParameterValueModification[pvm_list.size()];

        pvm_list.toArray(pvms);
        return pvms;
        
    }

        /** Removes a locally added ParameterValueModification from this
          * primitive instance.  Inherited mods cannot be removed, only
          * overridden by a local add.
          */
    public boolean parameterValueMod_removeLocal(String _parameter_name) {

        boolean instance_parameter = false;
        if (parameterDeclaration_containsLocal(_parameter_name)) {
            instance_parameter = 
               parameterDeclaration_containsLocalFuncXVariable(_parameter_name);
        }
        if (!instance_parameter) {
            if (parameterValueMod_contains(_parameter_name)) {
                ParameterValueModification[] pvm_locals =
                    parameterValueMods_getLocal();
                for (int i = 0; i < pvm_locals.length; i++) {
                    if (pvm_locals[i].getName().equals(_parameter_name)) {
                        ParameterValueModification pvm_to_remove = pvm_locals[i];
                        boolean b_modified =
                            m_parameter_modification_list.remove(pvm_to_remove);
                        if (b_modified) {
                            setTimeLastModified();

                            ContainedElementModEvent e =
                                    new ContainedElementModEvent(this);
                            fireContainedElementModEvent(e);

                            return true;
                        }
                    }
                }
            }
        }
        else {
            m_removed_funcx_param_index = 
                parameterValueMod_removeSpecialVariable (_parameter_name);
            return true;
        }
        return false;
    }

    public int parameterValueMod_removeSpecialVariable (
                                                      String _parameter_name) {
        ParameterValueModification[] pvm_locals =
                    parameterValueMods_getLocalFuncXVariable();
        for (int i = 0; i < pvm_locals.length; i++) {
            if (pvm_locals[i].getName().equals(_parameter_name)) {
                ParameterValueModification pvm_to_remove = pvm_locals[i];
                boolean b_modified = 
                    m_funcx_value_mod_list.remove(pvm_to_remove);
                if (b_modified) {
                    setTimeLastModified();

                    ContainedElementModEvent e =
                                new ContainedElementModEvent(this);
                    fireContainedElementModEvent(e);

                    return i;
                }
            }
        }

        // error condition
        System.err.println(generateFullNodePathName() +
                ".parameterValueMod_removeLocal(" + _parameter_name + ") :\n" +
                "\tERROR: Attempting to remove parameter value modification " +
                "not found in the local add list.");
         return -1;
    }

        /**
         * This method uses java.lang.reflect.Field to get the desired list
         * by name.  This is an auxiliary method used to streamline the code
         * for getting a node's contained objects from any of its container
         * lists (e.g., m_port_add_list/m_port_remove_list.)
         */
    protected ArrayList getListByName(String _list_name) {
        try {
                // what the heck does this do exactly? :)
                // 1. gets this object's class (class ALFINode)
                // 2. gets the Field declared in this class as name _list_name
                // 3. asks for the actual list object associated with the Field
                //    inside _this_ object (that's the "this" in ".get(this)")
                // 4. casts the procured list object as an ArrayList, returns it
           return (ArrayList) getClass().getSuperclass().getDeclaredField(_list_name).get(this);
        }
        catch(Exception e) {  
        Alfi.getMessenger().inform(e);  }

        return null;
    }

    public void setInstanceParametersOnly (boolean _flag) {
        m_instance_parameters_only = _flag; 
    }


    public boolean isInstanceParametersOnly () {
        return m_instance_parameters_only;
    }


    private String settingReplace_memberDecl (String _old_value) {

        int start_location = 0;
        int end_location = 0;
        String new_value = FUNC_X_WARNING_NO_EDIT +"\n"+
                           FUNC_X_MEMBER_VAR_START+"\n";

        if (m_funcx_value_mod_list != null) { 

            int comment_start= _old_value.indexOf(FUNC_X_WARNING_NO_EDIT, 0);
            int marker_start = _old_value.indexOf(FUNC_X_MEMBER_VAR_START, 0);
            int marker_end = _old_value.indexOf(FUNC_X_MEMBER_VAR_END, 0) +
                            FUNC_X_MEMBER_VAR_END.length() + 1;
        
            end_location = marker_end;
    
            if (comment_start < 0 && marker_start < 0) {
                // There were no previous autogenerated comments
                start_location = 0;
                end_location = 0;
            } else if (comment_start >= 0 &&  marker_start >= 0) {
                start_location = Math.min(comment_start, marker_start);    
            } else if (comment_start < 0) {
                start_location = marker_start;
            } else {
                start_location = comment_start;
            }

            // Compose the new variable section
    
            FUNCX_ParameterValueModification[] funcx_pvms =
                new FUNCX_ParameterValueModification[
                                                 m_funcx_value_mod_list.size()];
            m_funcx_value_mod_list.toArray(funcx_pvms);
 
            for (int ii = 0; ii < funcx_pvms.length; ii++) {
                String name = funcx_pvms[ii].getName();
                ParameterDeclaration funcx_decl = 
                    parameterDeclaration_getFuncXVariable(name);
                if (funcx_decl != null) {
                    new_value += funcx_decl.getType() + " " + name + ";      " ;
                    if (funcx_pvms[ii].getValue().length() > 0) {
                    new_value += FUNC_X_MEMBER_INIT + "  " + 
                                funcx_pvms[ii].getValue() +";"; 
                    }
                    new_value += "\n";
                }
            }
        }
        new_value += FUNC_X_MEMBER_VAR_END +"\n";

        StringBuffer string_buffer = new StringBuffer(_old_value);
        string_buffer = string_buffer.replace(start_location,
                    end_location, new_value);
        
        return string_buffer.toString();
    }

    private String settingReplace_constructor (String _old_value) {

        int start_location = 0;
        int end_location = 0;
        String new_value = FUNC_X_WARNING_NO_EDIT +"\n"+
                           FUNC_X_CTOR_INIT_START+"\n";

        if (m_funcx_value_mod_list != null) { 

            int comment_start= _old_value.indexOf(FUNC_X_WARNING_NO_EDIT, 0);
            int marker_start = _old_value.indexOf(FUNC_X_CTOR_INIT_START, 0);
            int marker_end = _old_value.indexOf(FUNC_X_CTOR_INIT_END, 0) +
                            FUNC_X_CTOR_INIT_END.length() + 1;
        
            end_location = marker_end;
    
            if (comment_start < 0 && marker_start < 0) {
                // There were no previous autogenerated comments
                start_location = 0;
                end_location = 0;
            } else if (comment_start >= 0 &&  marker_start >= 0) {
                start_location = Math.min(comment_start, marker_start);    
            } else if (comment_start < 0) {
                start_location = marker_start;
            } else {
                start_location = comment_start;
            }

            // Compose the new variable section
    
            FUNCX_ParameterValueModification[] funcx_pvms =
                new FUNCX_ParameterValueModification[
                                                 m_funcx_value_mod_list.size()];
            m_funcx_value_mod_list.toArray(funcx_pvms);
 
            for (int ii = 0; ii < funcx_pvms.length; ii++) {
                new_value += funcx_pvms[ii].getName();
                new_value += " = " + funcx_pvms[ii].getValue() + ";\n";
            }
        }
        new_value += FUNC_X_CTOR_INIT_END +"\n";

        StringBuffer string_buffer = new StringBuffer(_old_value);
        string_buffer = string_buffer.replace(start_location,
                    end_location, new_value);
        
        return string_buffer.toString();
    }
    
    private void clearLists ( ) {
        if (m_funcx_param_decl_list != null) {
            m_funcx_param_decl_list.clear();
        }
    
        if (m_funcx_value_mod_list != null) {
            m_funcx_value_mod_list.clear(); 
        }
    }



    //////////////////////
    // generic settings //

        /** Convenience method for dealing ParameterValueModification 
          */
    public NodeSettingIF setting_add (String _name, String _value,
                                      String _comment, Boolean _b_quoted) {

        System.out.println("FUNCX_Node setting_add");
        if (m_instance_parameters_only) {
            boolean instance_declaration = false;

            // Figure out if the setting is an instance parameter 
            if (parameterDeclaration_containsLocal(_name)) {
                instance_declaration = 
                    parameterDeclaration_containsLocalFuncXVariable(_name);
            }
            else {
                ParameterDeclaration pd =
                    parameterDeclaration_getFuncXVariable(_name);
                if (pd != null) { instance_declaration = true;}
            }
            
            if (instance_declaration) {
                // Find out if the MemberDecl ParameterValueMod exists 
                // in this instance.
                if (!parameterValueMod_containsLocal(FUNC_X_MEMBER_DECL) ) {
                    ParameterValueModification member_decl = 
                        parameterValueMod_get(FUNC_X_MEMBER_DECL);
                    processSpecialVariables(member_decl.getValue());
                }
            }
        }
        ParameterValueModification pvm = parameterValueMod_add(_name, _value, 
            _comment, _b_quoted.booleanValue());

        if (m_instance_parameters_only) {
            parameterValueMod_addAll();
        }

        return pvm;
    }

        /** Convenience method for dealing with ParameterLinkSettings (for
          * boxes) or ParameterValueModification (for primitives).
          */
    public void setting_removeLocal(String _setting_name) {
        System.out.println("FUNCX_Node::setting_removeLocal");
        boolean removed = parameterValueMod_removeLocal(_setting_name);
        
        if (removed && m_removed_funcx_param_index >= 0) {
            // Find out if the MemberDecl ParameterValueMod exists 
            // in this instance.
            if (!parameterValueMod_containsLocal(FUNC_X_MEMBER_DECL) ) {

                //if (m_member_decl_node != null) {
                //    ParameterValueModification member_decl = 
                //        m_member_decl_node.parameterValueMod_get(FUNC_X_MEMBER_DECL);
                //    processSpecialVariables(member_decl.getValue());
                //}
                //else {
                    ParameterValueModification member_decl = 
                        parameterValueMod_get(FUNC_X_MEMBER_DECL);
                    processSpecialVariables(member_decl.getValue());
                //}
            }
            parameterValueMod_addAll();
        }
    }
        

 
}
