package edu.caltech.ligo.alfi.bookkeeper;

import edu.caltech.ligo.alfi.widgets.*;

/**
  * This interface defines fields and methods used by sources of 
  * GroupElementModEvent sources.
  */
public interface GroupElementModEventSource {
    public void addGroupElementModListener(
                        GroupElementModEventListener _listener);
    public void removeGroupElementModListener(
                        GroupElementModEventListener _listener);
}

