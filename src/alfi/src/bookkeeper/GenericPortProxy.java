package edu.caltech.ligo.alfi.bookkeeper;

import java.util.*;

import edu.caltech.ligo.alfi.Alfi;


    /** GenericPortProxy is the base class for anything that a connection can
      * use as a source/sink.  It is abstract and needs several methods to be
      * implemented by the classes based upon it.
      */
public abstract class GenericPortProxy implements ProxyChangeEventSource,
                                               ContainedElementModEventSource {

    ////////////////////////////////////////////////////////////////////////////
    /////// constants //////////////////////////////////////////////////////////

    public static final int DATA_TYPE_STRING            = 0;
    public static final int DATA_TYPE_INTEGER           = 1;
    public static final int DATA_TYPE_REAL              = 2;
    public static final int DATA_TYPE_COMPLEX           = 3;
    public static final int DATA_TYPE_SINGLE_MODE_FIELD = 4;
    public static final int DATA_TYPE_FIELD             = 5;
    public static final int DATA_TYPE_VECTOR_REAL       = 6;
    public static final int DATA_TYPE_VECTOR_INTEGER    = 7;
    public static final int DATA_TYPE_VECTOR_COMPLEX    = 8;
    public static final int DATA_TYPE_MATRIX_REAL       = 9;
    public static final int DATA_TYPE_MATRIX_COMPLEX    = 10;
    public static final int DATA_TYPE_CLAMP             = 11;
    public static final int DATA_TYPE_BOOLEAN           = 12;
    public static final int DATA_TYPE_BUNDLE            = 13;
    public static final int DATA_TYPE_ALFI_BUNDLE       = 14;
    public static final int DATA_TYPE_USER_DEFINED      = 15;
        /** 1_UNKNOWN means this is an unknown single type. i.e. not a bundle.*/
    public static final int DATA_TYPE_1_UNKNOWN         = 16;
        /** Valid single alfi type, but needs resolution before modeler. */
    public static final int DATA_TYPE_1_UNRESOLVED      = 17;
        /** Invalid state.  Only used during inits. */
    public static final int DATA_TYPE_INVALID           = 18;
        /** Never valid end state.  May end up single or bundle.  Inits only. */
    public static final int DATA_TYPE_X_UNRESOLVED      = 19;
    public static final String[] DATA_TYPE_BY_STRING_ARRAY = {
        "string",            "integer",     "real",           "complex",
        "single_mode_field", "field",       "vector_real",    "vector_integer",
        "vector_complex",    "matrix_real", "matrix_complex", "clamp",
        "boolean",           "bundle",      "alfi_bundle",    "user_defined",
        "unknown",           "unresolved_1"     };

    ////////////////////////////////////////////////////////////////////////////
    /////// member fields //////////////////////////////////////////////////////

        /** This is the node in which this PortProxy was locally added. */
    protected final ALFINode m_direct_container_node;

        /** The name of the port object. */
    protected String m_name;

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected HashMap m_proxy_change_listener_map;

        /** Used in conjunction with ContainedElementModEventSource. */
    protected HashMap m_contained_element_mod_listeners_map;

    ////////////////////////////////////////////////////////////////////////////
    /////// constructors ///////////////////////////////////////////////////////

        /** Main constructor.  Should only be called from ALFINode. */
    GenericPortProxy(ALFINode _container_node, String _name) {
        assert(_container_node != null);

            // final member initializations
        m_direct_container_node = _container_node;

            // others
        m_name = (_name != null) ? new String(_name) :
                                   generateDefaultName(_container_node);

        m_proxy_change_listener_map = new HashMap();
        m_contained_element_mod_listeners_map = new HashMap();
    }

        /** Utility constructor for objects just carrying around port data. */
    public GenericPortProxy() {

            // renders this object useless other than information in fields
        m_direct_container_node = null;

            // can be set
        m_name = null;

            // won't be used
        m_proxy_change_listener_map = null;
        m_contained_element_mod_listeners_map = null;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////// class methods //////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    /////// abstract methods ///////////////////////////////////////////////////

        /** Determines the data type which is output from this port as a source
          * for connection _connection.
          */
    public abstract int determineSourceDataTypeForConnection(
                             ALFINode _container, ConnectionProxy _connection);

        /** Determines the data type which is input to this port as a sink
          * for connection _connection.
          */
    public abstract int determineSinkDataTypeForConnection(
                             ALFINode _container, ConnectionProxy _connection);

        /** If this object has upstream connections which feed it directly,
          * return them.  The returned HashMap has { connection,
          * connection_container } key/values.  Specifically, this will often
          * be the case for bundlers and junctions, and sometimes bundle ports.
          */
    protected abstract HashMap getUpstreamConnections(ALFINode _container,
                                                MemberNodeProxy _owner_member);

        /** If this object has downstream connection(s) associated with it,
          * return them.  The returned HashMap has { connection,
          * connection_container } key/values.  Specifically, this will often
          * be the case for bundlers and junctions, and sometimes bundle ports.
          */
    protected abstract HashMap getDownstreamConnections(ALFINode _container,
                                                MemberNodeProxy _owner_member);

        /** _owner_member is needed to identify external ports on member nodes
          * inside _container_node.  _owner_member should always be null for
          * some port types.
          */
    public abstract boolean hasConnections(ALFINode _container_node,
                                           MemberNodeProxy _owner_member);

        /** Returns an array of connections corresponding to each connection
          * (local or no) connected to this port in _container_node.
          */
    public abstract ConnectionProxy[] getConnections(ALFINode _container_node,
                                                 MemberNodeProxy _owner_member);

        /** Generates a default name. */
    protected abstract String generateDefaultName(ALFINode _container_node);

        /** Creates a long and short text description of itself for use in
          * status text and elsewhere.  This is a pretty version and is not
          * intended as something that may be parsed.  Therefore it may be
          * changed about without fear of breaking box files.
          */
    public abstract String generateSnippetForConnectionDescription(
                         ALFINode _container,
                         ConnectionProxy _connection, boolean _b_long_version);

        /** Does any local updating of this object and passes the update
          * signal downstream if applicable.
          */
    public abstract void updateContentAndPropogateDownstream(
                           ALFINode _container, MemberNodeProxy _owner_member);

    ////////////////////////////////////////////////////////////////////////////
    /////// implemented instance methods ///////////////////////////////////////

        /** This was originally implemented as a background thread, but there
          * are some race conditions occasionally, so the thread created here
          * is no longer placed in the background.  If speed becomes an issue,
          * this will need to be reisited.  See also
          * ConnectionProxy.doContentUpdate().
          */
    public void doContentUpdate(final ALFINode _container,
                                         final MemberNodeProxy _owner_member) {
        final GenericPortProxy gpp = this;
        final Thread background_task =
            new Thread() {
                public void run() {
                        // may need to wait for multiple ops to get finished
/*
                    int sleep_count = 0;
                    while (BundleProxy.getBundleContentUpdatesSuspended()) {
                        int sleepy_time = (sleep_count < 20) ? 250 : 1000;
                        try { Thread.currentThread().sleep(sleepy_time); }
                        catch(InterruptedException _e) { ; }

                            // in case state has changed while sleeping
                        if (_owner_member == null) {
                            if (! _container.genericPort_contains(gpp)) {
                                return;
                            }
                        }
                        else if (! _container.memberNodeProxy_contains(
                                                              _owner_member)) {
                            return;
                        }
                        else if (sleep_count++ > 3600) {    // about an hour...
                            Alfi.warn("startBackgroundContentUpdate(): " +
                                      "Too many sleeps.  Stopping thread...");
                            return;
                        }
                    }
*/
                    
                    updateContentAndPropogateDownstream(_container,
                                                                _owner_member);
                    ALFINode[] derived_nodes = _container.derivedNodes_getAll();
                    for (int i = 0; i < derived_nodes.length; i++) {
                        if ((_owner_member != null) && (! derived_nodes[i].
                                    memberNodeProxy_contains(_owner_member))) {
                            break;
                        }
                        else {
                            updateContentAndPropogateDownstream(
                                              derived_nodes[i], _owner_member);
                        }
                    }
                }
            };

        //background_task.setPriority(Thread.MIN_PRIORITY);
        background_task.start();

            // this basically moves "background_task" into the foreground
        try { background_task.join(); }
        catch(InterruptedException _e) {
            Alfi.warn("background_task interrupted!");
        }
    }

        /** Returns an array of connections corresponding to each connection
          * (local or no) connected to this port in _container_node.  This
          * only works for ports which are independent of any member node
          * ownership.
          */
    protected ConnectionProxy[] getConnections(ALFINode _container_node) {
        ConnectionProxy[] connections = _container_node.connections_get();
        ArrayList connections_list = new ArrayList();
        for (int i = 0; i < connections.length; i++) {
            if ((connections[i].getSource() == this) ||
                        (connections[i].getSink() == this)) {
                connections_list.add(connections[i]);
            }
        }

        connections = new ConnectionProxy[connections_list.size()];
        connections_list.toArray(connections);
        return connections;
    }

        /** Are there any connections to this port in _container_node?  Note
          * for some ports, more information may be required.  See
          * hasConnections(ALFINode _container_node, MemberNodeProxy _member).
          */
    protected boolean hasConnections(ALFINode _container_node) {
        ConnectionProxy[] connections = _container_node.connections_get();
        for (int i = 0; i < connections.length; i++) {
            if ((connections[i].getSource() == this) ||
                                          (connections[i].getSink() == this)) {
                return true;
            }
        }
        return false;
    }

    //////////////////
    // misc //////////

    static boolean dataTypeIdIsValid(int _data_type_id) {
        if ((_data_type_id >= 0) && (_data_type_id < DATA_TYPE_INVALID)) {
            return true;
        }
        else { return false; }
    }

    public static int getDataTypeIdFromString(String _data_type_string) {
        for (int i = 0; i < DATA_TYPE_BY_STRING_ARRAY.length; i++) {
            if (_data_type_string.equals(DATA_TYPE_BY_STRING_ARRAY[i])) {
                return i;
            }
        }
        return DATA_TYPE_INVALID;
    }

    /////////////////////////////////
    // member field access //////////

        /** @returns m_direct_container_node. */
    public ALFINode getDirectContainerNode() { return m_direct_container_node; }

        /** Returns a COPY of the name. */
    public String getName() {
        return (m_name != null) ? new String(m_name) : null;
    }

    public boolean setName(String _new_name) {
        if (! _new_name.equals(m_name)) {
            m_name = _new_name;
            m_direct_container_node.setTimeLastModified();

            ProxyChangeEvent pce = new ProxyChangeEvent(this,
                                   new PortProxy(_new_name, null, null, null));
            fireProxyChangeEvent(pce);

            ContainedElementModEvent e = new ContainedElementModEvent(this);
            fireContainedElementModEvent(e);

            return true;
        }
        else { return false; }
    }

    public String toString() { return toString(false); }

    public String toString(boolean _b_verbose) {
        String info = new String(m_name);

        if (_b_verbose) {
            info += " [added in " +
                      m_direct_container_node.generateFullNodePathName() + "]";
        }

        return info;
    }

    //////////////////////////////////////////////////////
    ////////// ProxyChangeEventSource Methods //////////

        /** ProxyChangeEventSource method. */
    public void addProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.put(_listener, null);
    }

        /** ProxyChangeEventSource method. */
    public void removeProxyChangeListener(ProxyChangeEventListener _listener) {
        m_proxy_change_listener_map.remove(_listener);
    }

        /** Used in conjunction with ProxyChangeEventSource interface. */
    protected void fireProxyChangeEvent(ProxyChangeEvent e) {
        for (Iterator i = m_proxy_change_listener_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ProxyChangeEventListener) i.next()).proxyChangePerformed(e);
        }
    }

    /////////////////////////////////////////////////////////////////
    ////////// ContainedElementModEventSource methods ///////////////

        /** ContainedElementModEventSource method. */
    public void addContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.put(_listener, null);
    }

        /** ContainedElementModEventSource method. */
    public void removeContainedElementModListener(
                        ContainedElementModEventListener _listener) {
        m_contained_element_mod_listeners_map.remove(_listener);
    }

        /** Used in conjunction with ContainedElementModEventSource. */
    protected void fireContainedElementModEvent(ContainedElementModEvent e) {
        for (Iterator i =
                m_contained_element_mod_listeners_map.keySet().iterator();
                                                              i.hasNext(); ) {
            ((ContainedElementModEventListener) i.next()).
                                                   containedElementModified(e);
        }
    }
}
