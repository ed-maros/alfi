package edu.caltech.ligo.alfi.tools;

import java.util.*;
import edu.caltech.ligo.alfi.bookkeeper.*;

    /**
     * Because it takes so long to register nodes as listeners to the
     * multitudes of ports and member node proxies which they contain down
     * to their deepest levels, we put this registration in another low
     * priority thread (this one.)  The registration will continue in
     * the background until finished.  Proxies associated with elements of
     * immediately opening edit windows will make sure nodes that are
     * supposed to be listening to them are immediately registered instead
     * of waiting for it to be done via this thread.
     */
public class ProxyListenerRegistrationTool extends Thread {
    private ArrayList m_nodes_to_register_list;
    private HashMap m_objects_to_wait_on_map;
    private boolean mb_busy;
    private int m_percent_complete;

    public ProxyListenerRegistrationTool() {
        super();

        setPriority(Thread.MIN_PRIORITY);
        setDaemon(true);
        m_nodes_to_register_list = new ArrayList(100);
        m_objects_to_wait_on_map = new HashMap();
        mb_busy = true;
        m_percent_complete = 0;
        start();
    }

    public synchronized void run() {
        while (true) {
            mb_busy = true;
            int current_count = m_nodes_to_register_list.size();
            m_percent_complete = 0;
            if ((current_count > 0) && (! interrupted())) {
                for (int i = current_count - 1; i >= 0; i--) {
                    ((ALFINode) m_nodes_to_register_list.get(i)).
                                                 registerAsProxyListener();
                    m_nodes_to_register_list.remove(i);

                    if (isInterrupted()) { break; }
                    if ((i % 100) == 0) {
                        m_percent_complete = 100 - (100 * i / current_count);
                        yield();
                    }
                }
            }
            else {
                try {
                    mb_busy = false;
                    wait();
                }
                catch(InterruptedException e) { ; }
            }
        }
    }

        /**
         * Interrupts run() if it is not currently in wait mode, and remembers
         * what object requested the interruption.  Multiple objects may have
         * called for an interruption of processing.  Processing will not
         * resume until *all* objects which have asked for an interrupt have
         * subsequently called resumeProcessingLoop().
         */
    public void interrupt(Object _object_to_wait_on) {
        m_objects_to_wait_on_map.put(_object_to_wait_on, null);
        if (mb_busy) { interrupt(); }
    }

    public synchronized void resumeProcessingLoop(Object _object_waiting_on) {
        m_objects_to_wait_on_map.remove(_object_waiting_on);
        if (m_objects_to_wait_on_map.isEmpty()) {
            if (! mb_busy) { notify(); }
        }
    }

        /** Is it busy registering nodes? */
    public boolean isBusy() { return mb_busy; }

        /** How complete is the registration since the last interrupt? */
    public float getPercentComplete() { return m_percent_complete; }

        /** Call interrupt on this thread before using this method. */
    public synchronized void addNodeToBeRegistered(ALFINode _node) {
        m_nodes_to_register_list.add(_node);
    }

        /** Call interrupt on this thread before using this method. */
    public synchronized void removeNodeToBeRegistered(ALFINode _node) {
        m_nodes_to_register_list.remove(_node);
    }
}
