package edu.caltech.ligo.alfi.tools;

import javax.swing.*;

import edu.caltech.ligo.alfi.file.*;

    /**
     * This class is the base class for all Alfi specific exceptions.  It
     * defines methods for outputting informative messages to the user and to
     * the log.
     */
public class AlfiException extends Exception {
    public static final int ALFI_INFO      = 0;
    public static final int ALFI_WARNING   = 1;
    public static final int ALFI_ERROR     = 2;
    public static final int NON_ALFI_ERROR = 3;

    protected final int m_exception_type;

        /**
         * _local_message is passed into the Exception constructor, and is
         * used in the building of a more informative message, which can
         * subsequently be gotten by the overridden getMessage() method of this
         * class and its sub-classes.  _exception_type can be ALFI_INFO (just
         * a flag), ALFI_WARNING (a more serious event which should leave
         * Alfi in a consistent state still), ALFI_ERROR (a very serious event
         * which may or may not leave Alfi in a consistent state and may or
         * may not result in corrupted Alfi files if a save is now attempted),
         * or NON_ALFI_ERROR (which usually means a programming bug exists and
         * may or may not have affected the data in the Alfi session.)
         */
    public AlfiException(String _local_message, int _exception_type) {
        super(_local_message);

        m_exception_type = _exception_type;
    }

        /**
         * Returns a short message regarding the exception, and is also
         * incorporated into the more detailed log message.  This should be
         * overridden by sub-classes to make it more meaningful for specific
         * cases.
         */
    public String getMessage() {
        String message = TextFormatter.formatMessage(super.getMessage());
        message += "\n\n\n" + TextFormatter.formatMessage(getMessageFooter());

        return message;
    }

    protected String getMessageFooter() {
        String footer = new String();
        switch (m_exception_type) {
          case ALFI_INFO:
            footer += "";
            break;
          case ALFI_WARNING:
            footer += "The invalid user action or information has been " +
                "ignored, and will not be incorporated into this session.";
            break;
          case ALFI_ERROR:
            footer += "This error may have corrupted the session's data.  " +
                "Saving any files at this time may result in a loss of file " +
                "information.  You should probably exit Alfi at this time " +
                "without saving any files.";
            break;
          default:
            footer += "This exception is due most likely to a bug in the " +
                "code- unknown effect on session.";
            break;
        }

        return footer;
    }

    public String getLogMessage() { return createLogMessage(); }

    public int getExceptionType() { return m_exception_type; }

        /** Creates a string to be written to the log file. */
    protected String createLogMessage() {
        String log_message = getClass().getName() + ":" + Parser.NL + Parser.NL;
        log_message += getMessage();
        log_message += Parser.NL + Parser.NL +
                   "        * * *  Stack Trace  * * *" + Parser.NL + Parser.NL;
        log_message += createStackTrace();
        return log_message;
    }

        /**
         * This method gets the stack trace and manipulates it to be more
         * meaningful/readable for debugging in Alfi.
         */
    protected String createStackTrace() {
        String alfi_base_package_name = "edu.caltech.ligo.alfi.";
        String trace = new String();
        StackTraceElement[] elements = getStackTrace();
        for (int i = 0; i < elements.length; i++) {
            String element = elements[i].toString();
            if (element.startsWith(alfi_base_package_name)) {
                element = element.substring(alfi_base_package_name.length());
            }
            trace += element + "\n";
        }
        return trace;
    }
}
