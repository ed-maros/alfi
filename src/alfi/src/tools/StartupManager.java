package edu.caltech.ligo.alfi.tools;

import java.io.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.summary.*;

    /**
     * This class implements most of the Alfi initialization procedure.
     */
public class StartupManager {

    JFrame m_splash_frame;

    ////////////////////////////////////////////////////////////////////////////
    ////////// Constructors ////////////////////////////////////////////////////

    public StartupManager(JFrame _splash_frame) {
        m_splash_frame = _splash_frame;
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////// Instance Methods ////////////////////////////////////////////////

    public boolean initializeAlfi() {
        File home_utility_directory = checkHomeUtilityDirectory();
        if (home_utility_directory != null) {

            Alfi.PREFERENCES = new HashMap();

            Alfi.UNIVERSAL_PREFERENCES_FILE =
                 new File(home_utility_directory, "universal_preferences.txt");
            HashMap universal_prefs_map =
                          parsePreferenceFile(Alfi.UNIVERSAL_PREFERENCES_FILE);
            Alfi.PREFERENCES.putAll(universal_prefs_map);

                // removing obsolete pref file if exists (5.5.14, 6/9/03)
            File obsolete_pref_file =
                  new File(home_utility_directory, "webstart_preferences.txt");
            if (obsolete_pref_file.exists()) {
                obsolete_pref_file.delete();
            }

                // if alfi was started via alfi_ws script, it may have created a
                // "command line file" for alfi to read on startup.  this is
                // necessary because otherwise webstart ignores any alfi
                // arguments in the command line
            File command_line_file =
                          new File(home_utility_directory, "command_line.txt");
            if (command_line_file.exists()) {
                HashMap command_line_and_env_map =
                                        parsePreferenceFile(command_line_file);
                Alfi.PREFERENCES.putAll(command_line_and_env_map);
                command_line_file.delete();
            }

            Alfi.WORKING_DIRECTORY = determineWorkingDirectory();
            System.setProperty("user.dir", Alfi.WORKING_DIRECTORY.getPath());

            Alfi.WORKING_DIRECTORY = determineWorkingDirectory();
            System.setProperty("user.dir", Alfi.WORKING_DIRECTORY.getPath());

            File log_file = new File(Alfi.WORKING_DIRECTORY, "ALFI_LOG");
            Alfi.ms_messenger = new Messenger(log_file.getAbsolutePath());


                // this is a temporary thing to remove working directories
                // from the E2E_PATH which is written at the end of session
                // unless it has been specifically added by the user
                // added at version 5.5.7 (5/12/03)
            String version = (String)
                    universal_prefs_map.get("UNIVERSAL_PREFERENCES_WRITTEN_BY");
            if (version == null) {
                String e2e_path = (String) universal_prefs_map.get("E2E_PATH");
                if (e2e_path != null) {
                    String cwd = Alfi.WORKING_DIRECTORY.getPath();
                    String[] parts = e2e_path.split(":");
                    e2e_path = new String();
                    for (int i = 0; i < parts.length; i++) {
                        if (! parts[i].equals(cwd)) {
                            e2e_path += ":" + parts[i];
                        }
                    }
                    while (e2e_path.startsWith(":")) {
                        e2e_path = e2e_path.substring(1);
                    }
                }
                universal_prefs_map.put("E2E_PATH", e2e_path);
            }


            Alfi.LOCAL_UTILITY_DIRECTORY =
                                     new File(Alfi.WORKING_DIRECTORY, ".alfi");

                // must exist
            if (! checkWorkingUtilityDirectory()) { return false; }

            Alfi.CLIPBOARD_FILE =
                  new File(Alfi.LOCAL_UTILITY_DIRECTORY, "alfi_clipboard.box");

                // add any preferences registered from the last time alfi was
                // used in this working directory
            Alfi.LOCAL_PREFERENCES_FILE =
                      new File(Alfi.LOCAL_UTILITY_DIRECTORY, "alfi_prefs.txt");
            HashMap local_prefs_map =
                              parsePreferenceFile(Alfi.LOCAL_PREFERENCES_FILE);
            Alfi.PREFERENCES.putAll(local_prefs_map);

                // finally, if alfi was started via a straight java command
                // line (usually just via development environment), then add any
                // preferences passed in the command line via the -Dxxx method
            String[] possible_command_line_Ds =
                   { "E2E_PATH", "ALFI__WINDOW_MANAGER", "WORKING_DIRECTORY" };
            for (int i = 0; i < possible_command_line_Ds.length; i++) {
                String key = possible_command_line_Ds[i];
                String value = System.getProperty(key);
                if (value != null) {
                    Alfi.PREFERENCES.put(key, value);
                }
            }
            

            Alfi.E2E_PATH = determineE2EPath();

            Alfi.WINDOW_MANAGER = determineWindowManager();
            if (Alfi.WINDOW_MANAGER.equals("fvwm")) {
                Alfi.WINDOW_MANAGER_OFFSET.translate(6, 23);
            }

            Alfi.DEFAULT_BROWSER = determineDefaultBrowser();
            universal_prefs_map.put("default.browser",Alfi.DEFAULT_BROWSER);
        }

        if (            (Alfi.UNIVERSAL_PREFERENCES_FILE != null) &&
                        (Alfi.WORKING_DIRECTORY != null) &&
                        (Alfi.LOCAL_UTILITY_DIRECTORY != null) &&
                        (Alfi.CLIPBOARD_FILE != null) &&
                        (Alfi.LOCAL_PREFERENCES_FILE != null) &&
                        (Alfi.PREFERENCES != null) &&
                        (Alfi.E2E_PATH != null) &&
                        (Alfi.WINDOW_MANAGER != null)) {
            return true;
        }
        else {
            String message = "Invalid value(s):\n" +
                "UNIVERSAL_PREFERENCES_FILE = " +
                                       Alfi.UNIVERSAL_PREFERENCES_FILE + "\n" +
                "WORKING_DIRECTORY = " + Alfi.WORKING_DIRECTORY + "\n" +
                "LOCAL_UTILITY_DIRECTORY = " +
                                          Alfi.LOCAL_UTILITY_DIRECTORY + "\n" +
                "CLIPBOARD_FILE = " + Alfi.CLIPBOARD_FILE + "\n" +
                "LOCAL_PREFERENCES_FILE = " +
                                           Alfi.LOCAL_PREFERENCES_FILE + "\n" +
                "PREFERENCES = " + Alfi.PREFERENCES + "\n" +
                "E2E_PATH = " + Alfi.E2E_PATH + "\n" +
                "DEFAULT_BROWSER = " + Alfi.DEFAULT_BROWSER + "\n" +
                "WINDOW_MANAGER = " + Alfi.WINDOW_MANAGER + "\n\nExiting.";
            String title = "Alfi Unable to Start";
            JOptionPane.showMessageDialog(m_splash_frame, message, title,
                                                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

        /** Checks that the directory ~/.alfi/ exists or attempts to create it.
          * Returns the home-alfi-utility directory if found or created, or
          * null if there was a problem.
          */
    private File checkHomeUtilityDirectory() {
        File home = new File(System.getProperty("user.home"));
        File home_util_dir = new File(home, ".alfi");
        String error_message = null;
        if (home.exists() && home.isDirectory()) {
            if (home_util_dir.exists()) {
                if (home_util_dir.isDirectory()) { return home_util_dir; }
                else {
                    error_message = "Alfi requires the use of a DIRECTORY " +
                        "having the path " + home_util_dir + ", but a FILE " +
                        "(not a directory) exists at this location in your " +
                        "system, and must be removed before Alfi can be run.";
                }
            }
            else {
                String message = TextFormatter.formatMessage(
                    "Alfi is creating a new directory in your home directory " +
                    "to store universal Alfi preference files, backup files, " +
                    "etc.  The path of this file is " + home_util_dir + ".");
                String title = "Creating Home Alfi Utility Directory";
                JOptionPane.showMessageDialog(m_splash_frame, message, title,
                                              JOptionPane.INFORMATION_MESSAGE);

                home_util_dir.mkdir();
                if (home_util_dir.isDirectory()) { return home_util_dir; }
                else {
                    error_message = "Alfi was unable to create the utility " +
                        "directory " + home_util_dir + " and is unable to " +
                        "start";
                }
            }
        }
        else {
            error_message = "Alfi is unable to access your home directory at " +
                home + ".  Alfi requires the creation of a utility directory " +
                "at " + home_util_dir + " and is unable to start.";
        }

            // if we have gotten here, there has been a problem
        error_message = TextFormatter.formatMessage(error_message);
        error_message += "\n\nAlfi will now exit.";
        String title = "Alfi Unable to Start";
        JOptionPane.showMessageDialog(m_splash_frame, error_message, title,
                                                    JOptionPane.ERROR_MESSAGE);
        System.exit(1);

        return null;
    }

        /** Checks that the directory WORKING_DIRECTORY/.alfi/ exists or
          * attempts to create it.
          */
    private boolean checkWorkingUtilityDirectory() {
        if (! Alfi.LOCAL_UTILITY_DIRECTORY.exists()) {
            String message = TextFormatter.formatMessage(
                "A local Alfi utility directory is being created in the " +
                "current working directory (path: " +
                Alfi.LOCAL_UTILITY_DIRECTORY + ").  This local utility " +
                "directory stores a few preferences like which windows were " +
                "open last session.  Not to be confused with the universal " +
                "Alfi preferences stored in your home directory.");
            String title = "Creating Alfi Utility Directory";
            JOptionPane.showMessageDialog(m_splash_frame, message, title,
                                          JOptionPane.INFORMATION_MESSAGE);

            Alfi.LOCAL_UTILITY_DIRECTORY.mkdir();
        }

        if (Alfi.LOCAL_UTILITY_DIRECTORY.exists() &&
                                   Alfi.LOCAL_UTILITY_DIRECTORY.isDirectory()) {

            if ( ! Alfi.LOCAL_UTILITY_DIRECTORY.canWrite() ) {
                String title = "Set Alfi Utility Directory";
                String message = TextFormatter.formatMessage(
                    "Write Access Failed for " +
                    Alfi.LOCAL_UTILITY_DIRECTORY + ".  Please enter " +
                    "an alternate utility directory or click Cancel to quit.");
                String file_name = JOptionPane.showInputDialog(m_splash_frame, message, title,
                                                 JOptionPane.QUESTION_MESSAGE);
                if (file_name == null) {
                    return false;
                }
                else {
                    file_name = file_name.trim();
                    if (file_name.length() == 0) 
                        return false;
                    
                    Alfi.LOCAL_UTILITY_DIRECTORY = new File(file_name);
                    if (! Alfi.LOCAL_UTILITY_DIRECTORY.exists() ||
                        ! Alfi.LOCAL_UTILITY_DIRECTORY.isDirectory()) {
                        JOptionPane.showMessageDialog(m_splash_frame, 
                         "The file is not a directory or is an " +
                         "invalid name.  Alfi is exiting...", 
                         "Alfi Unable to Start", JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }
            }
            return true;
        }
        else {
            String message = "Alfi requires the use of a local utility " +
                "DIRECTORY in your working directory called \".alfi\" .  But " +
                "there currently exists a .alfi FILE (not a directory) at " +
                "the path " + Alfi.LOCAL_UTILITY_DIRECTORY.getPath() + " .  " +
                "This file must be removed before Alfi can be started from " +
                "the current working directory ( " + Alfi.WORKING_DIRECTORY +
                " ).";
            message = TextFormatter.formatMessage(message);
            message += "\n\nExiting.";
            String title = "Alfi Unable to Start";
            JOptionPane.showMessageDialog(m_splash_frame, message, title,
                                                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);

            return false;
        }
    }

        /** First check the preferences for user.dir.  If no such preference
          * is found, then ask the user what directory they want to use as the
          * working directory.
          */
    private File determineWorkingDirectory() {
        if (Alfi.PREFERENCES.containsKey("user.dir")) {
            File cwd = new File((String) Alfi.PREFERENCES.get("user.dir"));
            if (cwd.exists()) {
                return cwd;
            }
        }

            // otherwise, ask user for a working directory
        String title = "Working Directory Selection";
        String message = "Alfi was unable to determine your default " +
            "working directory.  Please select a directory for Alfi " +
            "to use as the working (start-up) directory.";
        message = TextFormatter.formatMessage(message);
        JOptionPane.showMessageDialog(m_splash_frame, message, title,
                                                    JOptionPane.PLAIN_MESSAGE);
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int result = chooser.showDialog(m_splash_frame, "Select");
        if (result == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile();
        }

            // if all else fails, just use the system user.dir
        return new File(System.getProperty("user.dir"));
    }

        /** This method parses an Alfi preference file to determine things such
          * as selected options at last quit, E2E_PATH value, windows from last
          * session, etc.
          */
    private HashMap parsePreferenceFile(File _preferences_file) {
        HashMap pref_map = new HashMap();

        BufferedReader reader = null;
        try {
            FileReader r = new FileReader(_preferences_file);
            reader = new BufferedReader(r);
        }
        catch(FileNotFoundException _e) { return pref_map; }    // blank

        if (reader != null) {
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    String[] words = line.split("\\s+");
                    if ((words.length == 4) && words[0].equals("window")) {
   
                    }
                    else if (words.length > 1) {
                        int i = line.indexOf(words[1]);
                        String value = line.substring(i);
                        pref_map.put(words[0], value);
                    }
                    else if (words.length == 1) {
                        pref_map.put(words[0], null);
                    }
                }
                reader.close();
            }
            catch(IOException _e) {
                Alfi.warn("Problem reading " + _preferences_file + ":\n" +
                    "\t" + line);
            }
        }

        return pref_map;
    }

        /** First looks to see if an E2E_PATH property exists (i.e., passed in
          * the command line), and if not, then looks in the settings file in
          * LOCAL_UTILITY_DIRECTORY for the E2E_PATH value.  If no such file
          * exists, then the user is prompted for the value for E2E_PATH.
          *
          * After a value for E2E_PATH has been gotten, this method then removes
          * any relative path elements (such as "." or "..").  Also prepends the
          * path with the directory specified as the "working directory."
          */
    private String determineE2EPath() {
        String path_preference = (String) Alfi.PREFERENCES.get("E2E_PATH");
        boolean b_no_path_found =
                ((path_preference == null) || (path_preference.length() == 0));
        
        String path_raw = (b_no_path_found) ? "" : new String(path_preference);

        String[] parts = path_raw.split(File.pathSeparator);
        HashMap parts_map = new HashMap();
        String path = new String();
        for (int i = 0; i < parts.length; i++) {
                // absolute path elements only
            if (parts[i].startsWith(File.separator)) {
                if (! parts_map.containsKey(parts[i])) {
                    if (path.length() > 0) { path += File.pathSeparator; }
                    path += parts[i];
                }
                parts_map.put(parts[i], null);
            }
        }
        String cwd = Alfi.WORKING_DIRECTORY.getPath();
        if (parts_map.containsKey(cwd)) {
            Alfi.SAVE_WORKING_DIRECTORY_IN_E2E_PATH = true;
        }
        else {
            if (path.length() > 0) { path = File.pathSeparator + path; }
            path = cwd + path;
            Alfi.SAVE_WORKING_DIRECTORY_IN_E2E_PATH = false;
        }

        return path;
    }

        /** The ALFI__WINDOW_MANAGER preference is first set if found in the
          * UNIVERSAL_PREFERENCES_FILE, and is then possibly overridden by
          * a setting of the same name passed via the evn/command-line
          * mechanism.  If this preference is set in either of the above
          * processes, then Alfi.WINDOW_MANAGER is set accordingly.  If no such
          * setting is found, then the user is prompted for this value.
          */
    private String determineWindowManager() {
        String wm = (String) Alfi.PREFERENCES.get("ALFI__WINDOW_MANAGER");

            // if WINDOW_MANAGER is still blank, prompt user for this info
        if (wm == null) {
            String title = "Window Manager Query";
            String message = "Some window managers (e.g., FVWM) exhibit " +
                "quirks when running Java based applications such as Alfi.  " +
                "If Alfi knows which window manager it is running under, " +
                "it can mitigate these effects.";
            message = TextFormatter.formatMessage(message);
            message += "\n\nPlease select your window manager from the " +
                "following list:\n\n";
            String[] options = {"FVWM under X-Windows", "Other Window Manager"};
            Object option = JOptionPane.showInputDialog(m_splash_frame,
                message, title, JOptionPane.QUESTION_MESSAGE,
                null, options, options[0]);
            if (option == options[0]) { wm = "fvwm"; }
            else { wm = "other"; }
        }

        if (wm.equals("")) { wm = "other"; }

        return wm;
    }


        /** First check the preferences for default.browser.  If no such 
          * preference is found, just return null
          */
    private String determineDefaultBrowser() {
        if (Alfi.PREFERENCES.containsKey("default.browser")) {
            String browser = (String) Alfi.PREFERENCES.get("default.browser");
            if ((browser != null) && (browser.length() > 0)) {
                return browser;
            }
        }

        return null;
    }

        /** Loads and displays windows from a previous session. */
    public void restoreWindows() {
        ALFINodeCache cache = Alfi.getTheNodeCache();
        AlfiMainFrame main_window = Alfi.getMainWindow();

        HashMap local_prefs_map =
                              parsePreferenceFile(Alfi.LOCAL_PREFERENCES_FILE);

            // e.g., /A.box or /x/y/z/A.box
        final String file_matcher = ".+\\.box";
            // e.g., A or A.B_0.C_4
        final String node_matcher = "[\\w\\.]+";
            // e.g., 34x-3x45x567
        final String rectangle_matcher = "-?\\d+x-?\\d+x\\d+x\\d+";

        ArrayList files_to_load_list = new ArrayList();
        for (Iterator i = local_prefs_map.keySet().iterator(); i.hasNext(); ) {
            String key = (String) i.next();
            String value = (String) local_prefs_map.get(key);
            if (key.matches(file_matcher)) {
                if (! cache.containsRootNode(key)) {
                    File file = new File(key);
                    if (file.exists()) { files_to_load_list.add(file); }
                }
                i.remove();
            }
        }
        File[] files_to_load = new File[files_to_load_list.size()];
        files_to_load_list.toArray(files_to_load);

        HashMap show_map = new HashMap();
        for (Iterator i = local_prefs_map.keySet().iterator(); i.hasNext(); ) {
            String key = (String) i.next();
            String value = (String) local_prefs_map.get(key);
            if (key.matches(node_matcher) && value.matches(rectangle_matcher)) {
                Rectangle rect = null;
                String[] value_parts = value.split("x");
                try {
                    int x = Integer.parseInt(value_parts[0]);
                    int y = Integer.parseInt(value_parts[1]);
                    int w = Integer.parseInt(value_parts[2]);
                    int h = Integer.parseInt(value_parts[3]);
                    rect = new Rectangle(x, y, w ,h);
                }
                catch(NumberFormatException _e) { ; }    // already checked

                Object[] info = { rect, new Boolean(true) } ;
                show_map.put(key, info);

                i.remove();
            }
        }

        main_window.load_and_show__engine(files_to_load, show_map);
    }
}
