package edu.caltech.ligo.alfi.tools;

import java.util.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.bookkeeper.*;

    /** This class is used to update the modeler connection caches of
      * all the nodes in m_base_container_nodes in the background.
      */
public class ModelerConnectionCacheInitializer extends Thread {
    private ALFINode[] m_base_container_nodes;

        /** This constructor is used when a user adds a new member node to some
          * container node.  This constructor actually just calls the main
          * constructor, but this interface is given because the two
          * constructors are used during wholy different operations.
          */
    public ModelerConnectionCacheInitializer(ALFINode _new_member_node) {
        this(new ALFINode[] { _new_member_node });
    }

        /** This is the main constructor.  _newly_loaded_box_nodes is a set of
          * root base boxes which have just been loaded from file.
          */
    public ModelerConnectionCacheInitializer(ALFINode[] _base_container_nodes) {
        super();

        setPriority(Thread.MIN_PRIORITY);
        setDaemon(true);

        m_base_container_nodes = _base_container_nodes;
    }

    public void run() {
        for (int i = 0; i < m_base_container_nodes.length; i++) {
            initializeModelerConnectionCache(m_base_container_nodes[i]);
        }
    }

    private void initializeModelerConnectionCache(ALFINode _node) {
        ALFINode base_node = _node.baseNode_getDirect();
        if (base_node != null) {
            initializeModelerConnectionCache(base_node);
        }

        ALFINode[] member_nodes = _node.memberNodes_getDirectlyContained();
        for (int i = 0; i < member_nodes.length; i++) {
            if (member_nodes[i].isBox()) {
                initializeModelerConnectionCache(member_nodes[i]);
            }
        }

        _node.modelerConnectionCache_get();
    }
}
