package edu.caltech.ligo.alfi.tools;

import java.util.*;
import edu.caltech.ligo.alfi.bookkeeper.*;

    /** This class is used to update the content caches of bundle ports in
      * all the nodes in m_loaded_nodes in the background.
      */
public class BundlePortContentInitializer extends Thread {
    private ALFINode[] m_loaded_nodes;

    public BundlePortContentInitializer(ALFINode[] _loaded_nodes) {
        super();

        setPriority(Thread.MIN_PRIORITY);
        setDaemon(true);
        m_loaded_nodes = _loaded_nodes;
    }

    public synchronized void run() {
        HashMap support_base_node_map = new HashMap();
        for (int i = 0; i < m_loaded_nodes.length; i++) {
            ALFINode[] nodes= m_loaded_nodes[i].memberNodes_getAllGenerations();
            for (int j = 0; j < nodes.length; j++) {
                if (nodes[j].isBox()) {
                    support_base_node_map.put(nodes[j].baseNode_getRoot(),null);
                    nodes[j].bundlePorts_updateContentCache(true);
                }
            }
            m_loaded_nodes[i].bundlePorts_updateContentCache(true);
        }

        for (Iterator i = support_base_node_map.keySet().iterator();
                                                               i.hasNext(); ) {
            ((ALFINode) i.next()).bundlePorts_updateContentCache(true);
        }
    }
}
