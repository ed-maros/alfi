package edu.caltech.ligo.alfi.tools;

import java.io.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.file.*;

    /**
     * This class is used to inform users of problems and to log these messages.
     */
public class Messenger {
    private File m_log_file;
    private FileWriter m_writer;
    private boolean mb_has_been_written_to;
    private final static String ENTRY_SEPARATOR =
           Parser.NL + Parser.NL + "- - - - - - - - - - - - - - - - - - - - " +
           "- - - - - - - - - - - - - - - - - - - -" + Parser.NL + Parser.NL;

    public Messenger(String _log_file_name) {
        m_log_file = new File(_log_file_name);
        try {
            m_writer = new FileWriter(m_log_file);
            String s = "Alfi Version " + Alfi.ms_alfi_version + ENTRY_SEPARATOR;
            m_writer.write(s, 0, s.length());
            m_writer.flush();
        }
        catch(IOException _exception) {
            System.err.println("Logging Error: Unable to create log file \"" +
                m_log_file + "\".");
        }

        mb_has_been_written_to = false;
    }

        /**
         * This method sends the user a short dialog message, and writes a debug
         * message and stack trace to the log.
         */
    public void inform(Exception _exception, String _custom_message) {
        String pre = (_custom_message != null) ?
                                  _custom_message + Parser.NL + Parser.NL : "";
        if (_exception instanceof AlfiException) {
            AlfiException e = (AlfiException) _exception;
            inform_user(pre + e.getMessage(), e.getExceptionType());
            log(pre + e.createLogMessage());
        }
        else {
            inform_user(pre + createUsersJavaErrorMessage(_exception),
                                                 AlfiException.NON_ALFI_ERROR);
            log(pre + createLogsJavaErrorMessage(_exception));
        }
    }

    public void inform(Exception _exception) { inform(_exception, null); }

    public void cleanup() {
        if (m_writer == null) {
            return;
        }

        try {
            m_writer.flush();
            m_writer.close();
        }
        catch(IOException _exception) { ; }

        if (! mb_has_been_written_to) { m_log_file.delete(); }
    }

    private void inform_user(String _user_message, int _exception_type) {
        String title;
        int type;
        switch(_exception_type) {
          case AlfiException.ALFI_INFO:
            title = "ALFI User Information";
            type = JOptionPane.INFORMATION_MESSAGE;
            break;
          case AlfiException.ALFI_WARNING:
            title = "ALFI Warning";
            type = JOptionPane.WARNING_MESSAGE;
            break;
          case AlfiException.ALFI_ERROR:
            title = "ALFI Error";
            type = JOptionPane.ERROR_MESSAGE;
            break;
          default:
            title = "Non-ALFI Error";
            type = JOptionPane.ERROR_MESSAGE;
            break;
        }

        _user_message += "\n\n" + TextFormatter.formatMessage(
            "A log entry regarding this matter has been made in the log file " +
            m_log_file.getName() + ".");
        JOptionPane.showMessageDialog(null, _user_message, title, type);
    }

    private void log(String _message) {
        try {
            m_writer.write(_message, 0, _message.length());
            m_writer.write(ENTRY_SEPARATOR, 0, ENTRY_SEPARATOR.length());
            m_writer.flush();
            mb_has_been_written_to = true;
        }
        catch(IOException _exception) {
            System.err.println("Logging Error: Failed on write to log.");
        }
    }

    private String createUsersJavaErrorMessage(Exception _exception) {
        String message = "A Java Error of type " +
            _exception.getClass().getName() + " has occurred.  Such " +
            "exceptions should never occur in an ALFI session, and should " +
            "always be reported to the ALFI maintainers.  Please mail the " +
            "log file " + m_log_file.getName() + " along with a description " +
            "of what you action you took that lead to the exception, to:";
        message = TextFormatter.formatMessage(message, 40);

        message += "\n\n\talfi_bugs@ligo.caltech.edu\n\n";

        String message_2 = "You should restart ALFI without saving any files " +
            "if possible.  All files that were written over in this session " +
            "should be archived in their original form in your ALFI_BACKUP " +
            "directory.";
        message += TextFormatter.formatMessage(message_2, 40);

        return message;
    }

    private String createLogsJavaErrorMessage(Exception _exception) {
        String message = createUsersJavaErrorMessage(_exception);
        message += "\n    - - -\nStack trace:\n";
        StringWriter writer = new StringWriter();
        _exception.printStackTrace(new PrintWriter(writer));
        message += writer.getBuffer().toString();

        return message;
    }
}
