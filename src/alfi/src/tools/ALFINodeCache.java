package edu.caltech.ligo.alfi.tools;

import java.io.*;
import java.util.*;
import java.awt.*;
import javax.swing.tree.*;

import edu.caltech.ligo.alfi.Alfi;
import edu.caltech.ligo.alfi.file.*;
import edu.caltech.ligo.alfi.bookkeeper.*;
import edu.caltech.ligo.alfi.editor.*;
import edu.caltech.ligo.alfi.summary.*;
import edu.caltech.ligo.alfi.tools.*;

    /**
     * This class tracks all created ALFINodes and provides trees for display
     * of the containment and inhertance relationships between the nodes.
     */
public class ALFINodeCache {
        /** A map of root nodes vs their canonical file paths. */
    private HashMap m_root_node_map;

        /** A map of root primitive nodes vs their names. */
    private HashMap m_rootPrimitiveNode_vs_name_map;

        /** A map of root primitive templates vs their names. */
    private HashMap m_primitive_template_vs_name_map;

        /** A map of directory tree folders vs their names. */
    private HashMap m_directory_folders_vs_name_map;

        /** The container hierarchy tree.  */
    private DefaultTreeModel m_cTree;

    private DefaultMutableTreeNode m_cTree_root;

    private HashMap m_editor_vs_node_map;

    public ALFINodeCache() {
        m_root_node_map = new HashMap();
        m_rootPrimitiveNode_vs_name_map = new HashMap();
        m_primitive_template_vs_name_map = new HashMap();
        m_directory_folders_vs_name_map = new HashMap();

        m_cTree_root = new DefaultMutableTreeNode("Box Directories", true);
        m_cTree = new DefaultTreeModel(m_cTree_root, true);
        
        m_editor_vs_node_map = new HashMap();
    }

        /** Returns the_new_cTree_bullet. */
    public DefaultMutableTreeNode addNode(ALFINode _node) {
        if (_node == null) {
                // error condition
            System.err.println(
                    "ALFINodeCache.addNode(null):\n\tWARNING: Call not valid.");
            return null;
        }

            // prepare to add treeNodes
        boolean b_is_box = _node.isBox();
        DefaultMutableTreeNode the_new_cTree_bullet =
                                   new DefaultMutableTreeNode(_node, b_is_box);

        ALFINode container_node = _node.containerNode_get();
        if (_node.isRootNode()) {
            if (container_node != null) {
                    // root nodes should have no container nodes
                System.err.println("ALFINodeCache.addNode(" + _node +
                                            ", " + container_node + "):\n" +
                        "\tWARNING: Root node with a container node ?!?");
                return null;
            }

                // key for boxes is cononical path of file, for primitives, it
                // is just the primitive name
            String node_key =
                    (b_is_box) ? _node.getSourceFile().getCanonicalPath() :
                                                    _node.determineLocalName();

            if (! containsRootNode(node_key)) {
                m_root_node_map.put(node_key, _node);
                if (! b_is_box) {
                    m_rootPrimitiveNode_vs_name_map.put(node_key, _node);
                }
            }
            else {
                    // error condition
                System.err.println("ALFINodeCache.addNode(" + _node + "):\n" +
                        "\tWARNING: Node previously added.");
                return null;
            }

                // find or create the directory folder to place the box in
            if (b_is_box) {
                File dir = _node.getSourceFile().getCanonicalDirectory();
                    // don't put this one in the trees (clipboard node)
                if (! dir.equals(Alfi.LOCAL_UTILITY_DIRECTORY)) {

                    DefaultMutableTreeNode directory_folder =
                               (DefaultMutableTreeNode)
                                      m_directory_folders_vs_name_map.get(dir);
                    if (directory_folder == null) {
                        String adjusted_name = adjustName(dir.getPath());
                        directory_folder =
                               new DefaultMutableTreeNode(adjusted_name, true);
                        m_directory_folders_vs_name_map.put(dir,
                                                             directory_folder);

                            // add new directory
                        int index = determineAlphabeticalOrderIndex(
                                              m_cTree_root, directory_folder);
                        m_cTree.insertNodeInto(directory_folder,
                                                          m_cTree_root, index);
                    }

                        // add bullets into the respective hierarchy trees
                    int index = determineAlphabeticalOrderIndex(
                                       directory_folder, the_new_cTree_bullet);
                    m_cTree.insertNodeInto(the_new_cTree_bullet,
                                                      directory_folder, index);
                }
            }
        }
        else if (container_node == null) {
                // error condition
            System.err.println("ALFINodeCache.addNode(" + _node + ", " +
                                                  container_node + "):\n" +
                    "\tWARNING: Member nodes must have container nodes!");
            return null;
        }
        else {
            DefaultMutableTreeNode parent_bullet =
                  container_node.getAssociatedContainerTreeBullet();
            m_cTree.insertNodeInto(the_new_cTree_bullet, parent_bullet,
                                              parent_bullet.getChildCount());
        }

        return the_new_cTree_bullet;
    }

        /**
         * This method takes a full directory path and returns a more user
         * friendly name (e.g., /home/bsears/e2e/JAlfi/Sys/lib/Optics
         * becomes ./Sys/lib/Optics if the cwd is /home/bsears/e2e/JAlfi.)
         */
    private String adjustName(String _full_dir_path) {
        String path = new String(_full_dir_path);

        String cwd = System.getProperty("user.dir");
        if (path.startsWith(cwd)) {
            path = "." + path.substring(cwd.length());
        }
        return path;
    }

         /**
          * Determines the index to place a new entry using alphabetical order.
          */
    private int determineAlphabeticalOrderIndex(TreeNode _parent,
                                                TreeNode _child_to_add) {
        int count = _parent.getChildCount();
        String child_to_add_name = _child_to_add.toString();
        int i;
        for (i = 0; i < count; i++) {
            String child_name = _parent.getChildAt(i).toString();
            if (child_name.compareTo(child_to_add_name) > 0) {
                break;
            }
        }
        return i;
    }

    public boolean containsRootNode(String _node_key) {
        return (m_root_node_map.get(_node_key) != null);
    }

    public DefaultTreeModel getContainerTree() { return m_cTree; }

    public ALFINode getRootNode(String _canonical_file_path) {
        return ((ALFINode) m_root_node_map.get(_canonical_file_path));
    }

        /** Returns all loaded root box nodes, sorted in the order as specified
          * in getRootNodes(false, true, true).  Beware that the sorting
          * process can be time consuming.
          */
    public ALFINode[] getRootBoxNodes_sorted(boolean _b_include_clipboard) {
        return getRootNodes(false, true, true, _b_include_clipboard);
    }

        /** Returns all loaded root box nodes (in arbitrary order.) */
    public ALFINode[] getRootBoxNodes_unsorted(boolean _b_include_clipboard) {
        return getRootNodes(false, true, false, _b_include_clipboard);
    }

        /** Returns all loaded primitive nodes, sorted in the order as specified
          * in getRootNodes(true, false, true).  Beware that the sorting
          * process can be time consuming.
          */
    public ALFINode[] getRootPrimitiveNodes_sorted() {
        return getRootNodes(true, false, true, false);
    }

        /** Returns all loaded primitive nodes (in arbitrary order.) */
    public ALFINode[] getRootPrimitiveNodes_unsorted() {
        return getRootNodes(true, false, false, false);
    }

        /** Returns all loaded root nodes (both boxes and primitives), sorted
          * in the order as specified in getRootNodes(true, true, true).  Beware
          * that the sorting process can be time consuming.
          */
    public ALFINode[] getRootNodes_sorted(boolean _b_include_clipboard) {
        return getRootNodes(true, true, true, _b_include_clipboard);
    }

        /** Returns all loaded nodes (both boxes and primitives, in arbitrary
          * order.)
          */
    public ALFINode[] getRootNodes_unsorted(boolean _b_include_clipboard) {
        return getRootNodes(true, true, false, _b_include_clipboard);
    }

        /** Returns the loaded root nodes, including boxes if _b_get_boxes is
          * true, primitives if _b_get_primitives is true, and in order of
          * containment dependency if _b_sort is true (otherwise the order is
          * arbitrary.)  If sorted, to be more specific, the last nodes may (or
          * may not) include instances of the previous nodes in the array, but
          * no node in the array ever includes instances of a node which appears
          * after it in the returned array.
          */
    private ALFINode[] getRootNodes(boolean _b_get_primitives,
                                    boolean _b_get_boxes, 
                                    boolean _b_sort,
                                    boolean _b_include_clipboard) {
        ArrayList node_list = new ArrayList();

        ALFINode exclude_this_node = (_b_include_clipboard) ? null :
                       ((EditorFrame.getClipboard() != null) ?
                         EditorFrame.getClipboard().getClipboardNode() : null);

        Iterator i = m_root_node_map.values().iterator();
        while (i.hasNext()) {
            ALFINode node = (ALFINode) i.next();
            if (node == exclude_this_node) { continue; }

            if (node.isRootNode()) {
                if ( (_b_get_primitives && node.isPrimitive()) ||
                                       (_b_get_boxes && node.isBox()) ) {
                    node_list.add(node);
                }
            }
        }
        ALFINode[] nodes = new ALFINode[node_list.size()];
        node_list.toArray(nodes);

        if (_b_sort) { 
            Arrays.sort(nodes,
                new Comparator() {
                    public int compare(Object _o1, Object _o2) {
                        return ((ALFINode) _o1).determineLocalName().
                                    toUpperCase().compareTo(((ALFINode) _o2).
                                           determineLocalName().toUpperCase());
                    }
                    public boolean equals(Object _o) {
                        return (_o == this);
                    }
                });
        }

        return nodes;
    }

    public PrimitiveTemplate[] getPrimitiveTemplates() {
        ArrayList template_list =
                      new ArrayList(m_primitive_template_vs_name_map.values());
        PrimitiveTemplate[] templates =
                                   new PrimitiveTemplate[template_list.size()];
        template_list.toArray(templates);
        return templates;
    }

    public PrimitiveTemplate getPrimitiveTemplateByName(String _base_name) {
        return ((PrimitiveTemplate) m_primitive_template_vs_name_map.get(
                                                                  _base_name));
    }

    public UserDefinedPrimitive[] getUserDefinedPrimitives() {
        ArrayList udp_list = new ArrayList();

        ALFINode[] primitive_nodes = getRootPrimitiveNodes_sorted();
    
        for (int i = 0 ; i < primitive_nodes.length; i++) {
            if (primitive_nodes[i] instanceof UserDefinedPrimitive) {
                udp_list.add(primitive_nodes[i]); 
            }
        }
        
        UserDefinedPrimitive[] udp_array = 
                                      new UserDefinedPrimitive[udp_list.size()];   
    
        udp_list.toArray(udp_array);
        return udp_array;
    }

    public ALFINode getRootPrimitiveNodeByName(String _name) {
        return ((ALFINode) m_rootPrimitiveNode_vs_name_map.get(_name));
    }

    public ALFINode findTemplateGeneratedBaseNode(PrimitiveTemplate _template,
                                                        int[] _io_set_counts) {
        String base_node_name =
                             _template.generateNameForBaseNode(_io_set_counts);
        return getRootPrimitiveNodeByName(base_node_name);
    }

        /** IMPORTANT!: This method should not be used except in cases where
          * nodes are unloaded and reloaded so that the original node object is
          * no longer a good reference.  Searches all the nodes in the cache for
          * the node with the full node path of _full_node_name.
          */
    public ALFINode findNode(String _full_node_name) {
        ALFINode[] root_nodes = getRootNodes_unsorted(false);
        String[] name_parts = _full_node_name.split("\\.");
        ALFINode node = null;
        for (int i = 0; i < root_nodes.length; i++) {
            if (root_nodes[i].generateFullNodePathName().equals(name_parts[0])){
                node = root_nodes[i];
                break;
            }
        }
        if (node == null) { return null; }

        for (int i = 0; i < name_parts.length; i++) {
            ALFINode[] members = node.memberNodes_getDirectlyContained();
            for (int j = 0; j < members.length; j++) {
                String name = members[j].determineLocalName();
                if (name.equals(name_parts[i])) {
                    node = members[j];
                    break;
                }
            }
        }

        if (node.generateFullNodePathName().equals(_full_node_name)) {
            return node;
        }
        else { return null; }
    }

        /**
         * This method removes the node and all nodes contained in it from the
         * cache.
         */
    public void removeNode(ALFINode _node) {
        ALFINode[] contained_nodes = _node.memberNodes_getDirectlyContained();
        for (int i = 0; i < contained_nodes.length; i++) {
            removeNode(contained_nodes[i]);
        }

        DefaultMutableTreeNode bullet_to_remove =
                                      _node.getAssociatedContainerTreeBullet();
        TreeNode parent_bullet = bullet_to_remove.getParent();
        bullet_to_remove.removeFromParent();
        m_cTree.reload(parent_bullet);

        if (_node.isRootNode()) {
            Iterator i= m_root_node_map.keySet().iterator();
            while (i.hasNext()) {
                String key = (String) i.next();
                if (m_root_node_map.get(key) == _node) {
                    m_root_node_map.remove(key);
                    break;
                }
            }

        }

            // remove parent bullet if it is now an empty directory node
        if ((parent_bullet != null) && parent_bullet.getChildCount() == 0) {
            if (parent_bullet instanceof DefaultMutableTreeNode) {
                DefaultMutableTreeNode parent =
                                        (DefaultMutableTreeNode) parent_bullet;
                if  (m_directory_folders_vs_name_map.containsValue(parent)) {
                    Iterator i =
                           m_directory_folders_vs_name_map.keySet().iterator();
                    while (i.hasNext()) {
                        File key = (File) i.next();
                        if (m_directory_folders_vs_name_map.get(key) == parent){
                            m_directory_folders_vs_name_map.remove(key);
                            break;
                        }
                    }
                }
                Object user_object = parent.getUserObject();
                if (user_object instanceof String) {
                    TreeNode grandparent = parent.getParent();
                    parent.removeFromParent();
                    m_cTree.reload(grandparent);
                }
            }
        }
    }

        /** Clears cache of primitive nodes.  Essentially hobbles Alfi until
          * another set of primitives is loaded back in.
          */
    public void clearPrimitives() {
        for (Iterator i = m_rootPrimitiveNode_vs_name_map.values().iterator();
                                                                i.hasNext(); ) {
            ALFINode primitive = (ALFINode) i.next();
            String key = primitive.determineLocalName();
            m_root_node_map.remove(key);
        }
        m_rootPrimitiveNode_vs_name_map.clear();
    }

    /////////////////////////////////
    // primitive templates //////////

        /** Adds a primitive template to the cache. */
    public void addPrimitiveTemplate(PrimitiveTemplate _pt) {
        m_primitive_template_vs_name_map.put(_pt.getBaseName(), _pt);
    }

    /////////////////////////////////
    // edit session access //////////

        /** Adds an edit session for ease in finding it later. */
    public boolean addEditSession(ALFINode _node, EditorFrame _editor) {
        return (m_editor_vs_node_map.put(_node, _editor) != null);
    }

        /** Gets an ALFINode's edit session if one exists. */
    public EditorFrame getEditSession(ALFINode _node) {
        return ((EditorFrame) m_editor_vs_node_map.get(_node));
    }

        /** Gets all edit sessions currently open. */
    public EditorFrame[] getEditSessions() {
        Collection session_collection = m_editor_vs_node_map.values();
        EditorFrame[] sessions = new EditorFrame[session_collection.size()];
        session_collection.toArray(sessions);
        return sessions;
    }

        /** Gets all edit sessions currently open and their locations.  Edit
          * session name is key, and location is value in map returned,
          * (String, Point).
          */
    public HashMap getEditSessionInfo() {
        EditorFrame[] editors = getEditSessions();
        HashMap map = new HashMap();
        for (int i = 0; i < editors.length; i++) {
            String name = editors[i].getNodeInEdit().generateFullNodePathName();
            Rectangle rect = editors[i].getBounds();
            Boolean B_is_internal = new Boolean(editors[i].isInternalView());
            Object[] info = { rect, B_is_internal };
            map.put(name, info);
        }
        return map;
    }

        /** Removes an edit session. */
    public boolean removeEditSession(ALFINode _node) {
        return (m_editor_vs_node_map.remove(_node) != null);
    }
}
