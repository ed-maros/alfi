package edu.caltech.ligo.alfi.tools;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import edu.caltech.ligo.alfi.Alfi;

    /** This class is a PrintStream which copies a parent PrintStream and
      * creates a dialog from the copy as well as passing the data along
      * to the parent stream.
      */
public class PrintStreamDialogWriter extends PrintStream {

    protected final JFrame m_dialog;
    protected final JTextArea m_text_area;

    public PrintStreamDialogWriter(PrintStream _parent_stream) {
        super(_parent_stream);

            // this gets a reference to Alfi's main window before it is
            // initialized.  problem?
        m_dialog = new JFrame("Alfi Warning and Information Log");
        m_dialog.setVisible(false);
        m_dialog.setBounds(50, 50, 600, 500);
        m_dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        m_text_area = new JTextArea();

        JButton hide_button = new JButton("Hide");
        hide_button.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent _event) {
                    m_dialog.setVisible(false);
                }
            });

        Box south_box = Box.createHorizontalBox();
        south_box.add(Box.createHorizontalGlue());
        south_box.add(hide_button);
        south_box.add(Box.createHorizontalGlue());

        Container content_pane = m_dialog.getContentPane();
        content_pane.add(new JScrollPane(m_text_area), BorderLayout.CENTER);
        content_pane.add(south_box, BorderLayout.SOUTH);
    }

        /** I don't know what (extra) this should do if anything- (?) */
    public void write(int x) {
        super.write(x);
    }

        /** Writes to old err as well as to a dialog window. */
    public void write(byte[] _buffer, int _offset, int _length) {
        super.write(_buffer, _offset, _length);

            // Don't show dialog just for the startup message.
        if (m_text_area.getLineCount() > 2) {
            m_dialog.setState(Frame.NORMAL);
            m_dialog.setVisible(true);
        }

        m_text_area.append(new String(_buffer, _offset, _length));
    }
}
