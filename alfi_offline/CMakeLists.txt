#========================================================================
# -*- mode: cmake -*-
#------------------------------------------------------------------------
set( EXTRA_DATA
  alfi.jar
  JGo.jar
  l2fprod-common-sheet.jar
  java.policy.alfi
  )

set( BUILT_DATA_SOURCE
  alfi.jnlp
  )

foreach( file ${BUILT_DATA_SOURCE})
  configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/${file}.in
      ${CMAKE_CURRENT_BINARY_DIR}/${file}
      @ONLY )
  list( APPEND BUILT_DATA ${CMAKE_CURRENT_BINARY_DIR}/${file} )
endforeach( )

install(
  FILES ${EXTRA_DATA} ${BUILT_DATA}
  DESTINATION ${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}
  )
